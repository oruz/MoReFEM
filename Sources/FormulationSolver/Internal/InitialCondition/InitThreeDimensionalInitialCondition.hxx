/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            template<class InitialConditionT, class InputDataT>
            InitialCondition<ParameterNS::Type::vector>::unique_ptr
            InitThreeDimensionalInitialCondition(const Mesh& mesh, const InputDataT& input_data)
            {
                namespace IPL = Utilities::InputDataNS;

                decltype(auto) nature_vector = IPL::Extract<typename InitialConditionT::Nature>::Value(input_data);

                assert(nature_vector.size() == 3ul);
                const auto& nature_x = nature_vector[0];
                const auto& nature_y = nature_vector[1];
                const auto& nature_z = nature_vector[2];

                if ((nature_x == "ignore" || nature_y == "ignore" || nature_z == "ignore")
                    && (nature_x != nature_y || nature_x != nature_z))
                    throw Exception("Error for " + InitialConditionT::GetName()
                                        + ": if "
                                          "one of the item is 'ignore' the other ones should be 'ignore' as well.",
                                    __FILE__,
                                    __LINE__);

                if (nature_x == "ignore")
                    return nullptr;

                decltype(auto) value_per_component = IPL::Extract<typename InitialConditionT::Value>::Value(input_data);

                using value_per_component_type = std::decay_t<decltype(value_per_component)>;

                static_assert(Utilities::IsSpecializationOf<std::vector, value_per_component_type>());
                assert(value_per_component.size() == 3ul);

                auto&& component_x = InitScalarInitialCondition(mesh, nature_x, value_per_component[0]);

                auto&& component_y = InitScalarInitialCondition(mesh, nature_y, value_per_component[1]);

                auto&& component_z = InitScalarInitialCondition(mesh, nature_z, value_per_component[2]);

                return std::make_unique<ThreeDimensionalInitialCondition>(
                    std::move(component_x), std::move(component_y), std::move(component_z));
            }


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
