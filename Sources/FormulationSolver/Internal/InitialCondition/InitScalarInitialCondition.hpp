/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Feb 2016 14:06:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_

#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"
#include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hpp"

#include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"
#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief Init a \a InitialCondition from the content of the input data file.
             *
             * \param[in] mesh Mesh upon which the InitialCondition is applied. Initial condition
             * might be requested at each of each \a Coords.
             * \param[in] nature One of the possible nature specified in the comment of the input parameter. Currently
             * only 'constant' and 'lua_function'.
             * \param[in] value The std::variant object read from the input data file, which nature is determined by
             * \a nature.
             *
             * \return The \a InitialCondition object properly initialized.
             */
            template<class VariantT>
            InitialCondition<ParameterNS::Type::scalar>::unique_ptr
            InitScalarInitialCondition(const Mesh& mesh, const std::string& nature, const VariantT& value);


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_
