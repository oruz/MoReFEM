/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Impl
            {


                // clang-format off
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                // clang-format on
                template<typename... ConstructorArgs>
                InitialConditionInstance<TypeT, NaturePolicyT, Args...>::InitialConditionInstance(
                    const Mesh& mesh,
                    ConstructorArgs&&... arguments)
                : parent(mesh), nature_policy(std::forward<ConstructorArgs>(arguments)...)
                { }


                // clang-format off
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                // clang-format on
                inline typename InitialConditionInstance<TypeT, NaturePolicyT, Args...>::return_type
                InitialConditionInstance<TypeT, NaturePolicyT, Args...>::SupplGetValue(const SpatialPoint& coords) const
                {
                    return nature_policy::GetValueFromPolicy(coords);
                }


                // clang-format off
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                // clang-format on
                inline typename InitialConditionInstance<TypeT, NaturePolicyT, Args...>::return_type
                InitialConditionInstance<TypeT, NaturePolicyT, Args...>::SupplGetConstantValue() const
                {
                    assert(nature_policy::IsConstant());
                    return nature_policy::GetConstantValueFromPolicy();
                }


                // clang-format off
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                // clang-format on
                inline bool InitialConditionInstance<TypeT, NaturePolicyT, Args...>::IsConstant() const
                {
                    return nature_policy::IsConstant();
                }


            } // namespace Impl


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HXX_
