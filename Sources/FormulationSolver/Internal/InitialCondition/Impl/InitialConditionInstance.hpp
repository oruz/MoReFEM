/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_

#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"
#include "Parameters/Policy/LuaFunction/LuaFunction.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Impl
            {


                /*!
                 * \brief Template class that provides actual instantiation of a parameter.
                 *
                 * \tparam TypeT  Type of the parameter (real, vector, matrix).
                 * \tparam NaturePolicyT Policy that determines how to handle the parameter. Policies are enclosed in
                 * ParameterNS::Policy namespace. Policies might be for instance Constant (same value everywhere),
                 * LuaFunction (value is provided by a function defined in the input data file; additional arguments are
                 * chosen here with the variadic template argument \a Args.).
                 *
                 */
                // clang-format off
                template
                <
                    ParameterNS::Type TypeT,
                    template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                    typename... Args
                >
                // clang-format on
                class InitialConditionInstance final : public InitialCondition<TypeT>,
                                                       public NaturePolicyT<TypeT, Args...>
                {

                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = InitialConditionInstance<TypeT, NaturePolicyT, Args...>;

                    //! Alias to base class.
                    using parent = InitialCondition<TypeT>;

                    //! Alias to return type.
                    using return_type = typename parent::return_type;

                    //! Alias to traits of parent class.
                    using traits = typename parent::traits;

                    //! Alias to nature policy (constant, per quadrature point, Lua function, etc...).
                    using nature_policy = NaturePolicyT<TypeT, Args...>;

                    //! Alias to unique_ptr.
                    using unique_ptr = std::unique_ptr<self>;


                  public:
                    /// \name Special members.
                    ///@{

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] mesh \a Mesh for which the initial condition is defined.
                     * \param[in] arguments Variadic arguments for initial conditions that need extra arguments in
                     * constructor.
                     */
                    template<typename... ConstructorArgs>
                    explicit InitialConditionInstance(const Mesh& mesh, ConstructorArgs&&... arguments);

                    //! Destructor.
                    ~InitialConditionInstance() override = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    InitialConditionInstance(const InitialConditionInstance& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    InitialConditionInstance(InitialConditionInstance&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    InitialConditionInstance& operator=(const InitialConditionInstance& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    InitialConditionInstance& operator=(InitialConditionInstance&& rhs) = delete;

                    ///@}

                    //! Get the value of the initial condition at \a coords.
                    //! \param[in] coords Spetial position at which the initial condition is evaluated.
                    return_type SupplGetValue(const SpatialPoint& coords) const override;

                    //! Get the value of the initial condition when it's a constant. Disabled if it's not...
                    return_type SupplGetConstantValue() const override;

                    /*
                     * \brief Whether the parameter varies spatially or not.
                     */
                    bool IsConstant() const override;
                };


            } // namespace Impl


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_INSTANCE_HPP_
