/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Feb 2016 14:06:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            template<class VariantT>
            InitialCondition<ParameterNS::Type::scalar>::unique_ptr
            InitScalarInitialCondition(const Mesh& mesh, const std::string& nature, const VariantT& value)
            {
                namespace IPL = Utilities::InputDataNS;

                if (nature == "constant")
                {
                    using initial_condition_type =
                        Impl::InitialConditionInstance<ParameterNS::Type::scalar, Policy::Constant>;

                    return std::make_unique<initial_condition_type>(mesh, std::get<double>(value));
                } else if (nature == "lua_function")
                {
                    using initial_condition_type =
                        Impl::InitialConditionInstance<ParameterNS::Type::scalar, Policy::LuaFunction>;

                    using value_type = spatial_lua_function;

                    return std::make_unique<initial_condition_type>(mesh, std::get<value_type>(value));
                } else
                {
                    assert(false
                           && "Should not happen: all the possible choices are assumed to be checked by "
                              " LuaOptionFile Constraints.");
                }

                return nullptr;
            }


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HXX_
