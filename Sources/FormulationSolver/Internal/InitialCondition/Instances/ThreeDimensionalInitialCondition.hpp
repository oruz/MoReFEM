/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Parameters/Internal/Traits.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class SpatialPoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief Class to handle a 3D parameter (for instance a force).
             *
             * Such objects should in most if all cases be initialized with InitThreeDimensionalInitialCondition() free
             * function.
             */
            class ThreeDimensionalInitialCondition final : public InitialCondition<ParameterNS::Type::vector>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = ThreeDimensionalInitialCondition;

                //! Alias to base class.
                using parent = InitialCondition<ParameterNS::Type::vector>;

                //! Alias to return type.
                using return_type = typename parent::return_type;

                //! Alias to traits of parent class.
                using traits = typename parent::traits;

                //! Alias to scalar parameter.
                using scalar_initial_condition = InitialCondition<ParameterNS::Type::scalar>;

                //! Alias to scalar parameter unique_ptr.
                using scalar_initial_condition_ptr = scalar_initial_condition::unique_ptr;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] x_component The scalar \a InitialCondition that represents the x component.
                 * \param[in] y_component The scalar \a InitialCondition that represents the y component.
                 * \param[in] z_component The scalar \a InitialCondition that represents the z component.
                 */
                explicit ThreeDimensionalInitialCondition(scalar_initial_condition_ptr&& x_component,
                                                          scalar_initial_condition_ptr&& y_component,
                                                          scalar_initial_condition_ptr&& z_component);

                //! Destructor.
                ~ThreeDimensionalInitialCondition() override;

                //! \copydoc doxygen_hide_copy_constructor
                ThreeDimensionalInitialCondition(const ThreeDimensionalInitialCondition& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ThreeDimensionalInitialCondition(ThreeDimensionalInitialCondition&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ThreeDimensionalInitialCondition& operator=(const ThreeDimensionalInitialCondition& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ThreeDimensionalInitialCondition& operator=(ThreeDimensionalInitialCondition&& rhs) = delete;

                ///@}


              private:
                /*!
                 * \brief Returns the constant value (if the parameters is constant).
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is called by GetConstantValue() once the fact
                 * the parameter is spatially constant has been asserted.
                 * \endinternal
                 *
                 * \return Spatially constant value.
                 */
                return_type SupplGetConstantValue() const override;

                /*!
                 * \brief Get the value of the parameter.
                 *
                 * \param[in] coords \a Coords at which the initial condition must be evaluated.
                 *
                 * \return Value of the initial condition.
                 */
                return_type SupplGetValue(const SpatialPoint& coords) const override;

                //! Whether the parameter varies spatially or not.
                bool IsConstant() const override;


              private:
                //! Access to contribution of component x.
                scalar_initial_condition& GetScalarInitialConditionX() const noexcept;

                //! Access to contribution of component y.
                scalar_initial_condition& GetScalarInitialConditionY() const noexcept;

                //! Access to contribution of component z.
                scalar_initial_condition& GetScalarInitialConditionZ() const noexcept;


              private:
                //! Contribution of the x component to the vectorial parameter.
                scalar_initial_condition_ptr scalar_initial_condition_x_;

                //! Contribution of the y component to the vectorial parameter.
                scalar_initial_condition_ptr scalar_initial_condition_y_;

                //! Contribution of the z component to the vectorial parameter.
                scalar_initial_condition_ptr scalar_initial_condition_z_;

                //! Content of the parameter.
                mutable LocalVector content_;
            };


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INSTANCES_x_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
