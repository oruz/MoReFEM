/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/

#include "Geometry/Mesh/Mesh.hpp"
#include <array>
#include <cassert>
#include <memory>
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            ThreeDimensionalInitialCondition::ThreeDimensionalInitialCondition(
                scalar_initial_condition_ptr&& x_component,
                scalar_initial_condition_ptr&& y_component,
                scalar_initial_condition_ptr&& z_component)
            : parent(x_component->GetMesh()), scalar_initial_condition_x_(std::move(x_component)),
              scalar_initial_condition_y_(std::move(y_component)), scalar_initial_condition_z_(std::move(z_component))
            {
                assert(GetMesh() == GetScalarInitialConditionY().GetMesh());
                assert(GetMesh() == GetScalarInitialConditionZ().GetMesh());

                content_.resize({ 3 });

                if (IsConstant())
                {
                    content_(0) = GetScalarInitialConditionX().GetConstantValue();
                    content_(1) = GetScalarInitialConditionY().GetConstantValue();
                    content_(2) = GetScalarInitialConditionZ().GetConstantValue();
                }
            }


            ThreeDimensionalInitialCondition::~ThreeDimensionalInitialCondition() = default;


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup
