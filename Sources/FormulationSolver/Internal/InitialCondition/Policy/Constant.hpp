/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_

#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Parameters/Internal/Traits.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS::Policy
        {


            /*!
             * \brief Parameter policy when the parameter gets the same value everywhere.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             */
            template<ParameterNS::Type TypeT>
            class Constant
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = Constant<TypeT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

              private:
                //! Alias to traits class related to TypeT.
                using traits = ::MoReFEM::Internal::ParameterNS::Traits<TypeT>;

              public:
                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::decay_t<return_type>;


              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                //! \param[in] value Value of the constant initial condition.
                explicit Constant(storage_type value);

                //! Destructor.
                ~Constant() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Constant(const Constant& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Constant(Constant&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Constant& operator=(const Constant& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Constant& operator=(Constant&& rhs) = delete;

                ///@}

              public:
                /*!
                 * \brief Enables to modify the constant value of a parameter.
                 *
                 * \param[in] value Constant value.
                 */
                void SetConstantValue(double value) noexcept;

              protected:
                //! Get the value.
                return_type GetConstantValueFromPolicy() const;

                //! Provided here to make the code compile, but should never be called.
                [[noreturn]] return_type GetValueFromPolicy(const SpatialPoint&) const;

                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;

              protected:
                //! Whether the parameter varies spatially or not.
                constexpr bool IsConstant() const noexcept;


                //! Write the content of the parameter for which policy is used in a stream.
                //! \copydoc doxygen_hide_stream_inout
                void WriteFromPolicy(std::ostream& stream) const;

              private:
                //! Get the value of the parameter.
                storage_type value_;
            };


        } // namespace FormulationSolverNS::Policy


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_
