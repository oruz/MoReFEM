/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS::Policy
        {


            template<ParameterNS::Type TypeT>
            Constant<TypeT>::Constant(typename Constant<TypeT>::storage_type value) : value_(value)
            { }


            template<ParameterNS::Type TypeT>
            [[noreturn]] typename Constant<TypeT>::return_type
            Constant<TypeT>::GetValueFromPolicy(const SpatialPoint& coords) const
            {
                static_cast<void>(coords);

                assert(false && "Parameter class should have guided toward GetConstantValue()!");
                exit(-1);
            }


            template<ParameterNS::Type TypeT>
            inline typename Constant<TypeT>::return_type Constant<TypeT>::GetConstantValueFromPolicy() const
            {
                return value_;
            }


            template<ParameterNS::Type TypeT>
            inline typename Constant<TypeT>::return_type Constant<TypeT>::GetAnyValueFromPolicy() const
            {
                return GetConstantValueFromPolicy();
            }


            template<ParameterNS::Type TypeT>
            inline constexpr bool Constant<TypeT>::IsConstant() const noexcept
            {
                return true;
            }


            template<ParameterNS::Type TypeT>
            void Constant<TypeT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Homogenous value:" << std::endl;
                out << GetConstantValueFromPolicy() << std::endl;
            }


            template<ParameterNS::Type TypeT>
            inline void Constant<TypeT>::SetConstantValue(double value) noexcept
            {
                value_ = value;
            }


        } // namespace FormulationSolverNS::Policy


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HXX_
