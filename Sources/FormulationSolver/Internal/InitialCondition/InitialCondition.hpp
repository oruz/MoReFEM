/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Parameters/Internal/Traits.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief InitialCondition function provided in the input data file.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             *
             * The InputData<> template specialization matching the required
             * parameter in the input file. For instance InputDataNS::PoissonRation::LuaFunction.
             *
             * \internal <b><tt>[internal]</tt></b> This class is really the pendant of Parameter, except that the
             * values must be evaluated at \a Coords rather than at quadrature points. It is called InitialCondition for
             * the time being as it is currently the only situation in which we need this feature; if a future
             * development requires this feature as well present class should probably be renamed and moved elsewhere.
             * \endinternal
             */
            template<ParameterNS::Type TypeT>
            class InitialCondition
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = InitialCondition<TypeT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to traits.
                using traits = ::MoReFEM::Internal::ParameterNS::Traits<TypeT>;

                //! Alias to return type.
                using return_type = typename traits::return_type;

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                //! \param[in] mesh \a Mesh for which the initial condition is defined.
                explicit InitialCondition(const Mesh& mesh);

                //! Destructor.
                virtual ~InitialCondition() = default;

                //! \copydoc doxygen_hide_copy_constructor
                InitialCondition(const InitialCondition& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                InitialCondition(InitialCondition&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                InitialCondition& operator=(const InitialCondition& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                InitialCondition& operator=(InitialCondition&& rhs) = delete;

                ///@}

              public:
                /*!
                 * \brief Get the value of the parameter at a given coord point in a given \a geom_elt.
                 *
                 * \param[in] coords \a SpatialPoint at which the initial condition is evaluated.
                 *
                 * \return Value at \a coords (computed by the child through method SupplGetConstantValue()).
                 */
                return_type GetValue(const SpatialPoint& coords) const;

                /*!
                 * \brief Returns the constant value (if the parameter is constant).
                 *
                 * If not constant, an assert is raised (in debug mode).
                 *
                 * \return The spatially constant value (computed by the child through method SupplGetConstantValue()).
                 */
                return_type GetConstantValue() const;

                //! Whether the parameter varies spatially or not. If so shortcut are made in its evaluation.
                virtual bool IsConstant() const = 0;

                //! Returns the mesh upon which the parameter is defined.
                const Mesh& GetMesh() const noexcept;


              private:
                /*!
                 * \brief Returns the constant value (if the parameters is constant).
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is called by GetConstantValue() once the fact the
                 * parameter is spatially constant has been asserted.
                 * \endinternal
                 *
                 * \return The spatially constant value.
                 */
                virtual return_type SupplGetConstantValue() const = 0;


                /*!
                 * \brief Get the value of the parameter at a given quadrature point in a given \a geom_elt.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is actually called when IsConstant() yields false;
                 * if true GetConstantValue() is called instead.
                 * \endinternal
                 * \param[in] coords \a SpatialPoint at which the initial condition is evaluated.
                 * \return Value at \a coords.
                 */
                virtual return_type SupplGetValue(const SpatialPoint& coords) const = 0;


              private:
                //! Mesh upon which the parameter is defined.
                const Mesh& mesh_;
            };


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
