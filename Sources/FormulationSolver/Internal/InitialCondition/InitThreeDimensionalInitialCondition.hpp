/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hpp"
#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief Init a \a InitialCondition from the content of the input data file.
             *
             * \tparam LuaFieldT Lua field considered in the input file.
             *
             * \param[in] mesh Mesh upon which the InitialCondition is applied. Initial condition
             * might be requested at each of each \a Coords.
             * \copydoc doxygen_hide_input_data_arg
             *
             * Condition is actually splitted in three \a ScalarInitialCondition.
             *
             * \return The \a InitialCondition object properly initialized.
             */

            template<class LuaFieldT, class InputDataT>
            InitialCondition<ParameterNS::Type::vector>::unique_ptr
            InitThreeDimensionalInitialCondition(const Mesh& mesh, const InputDataT& input_data);


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
