/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            template<ParameterNS::Type TypeT>
            InitialCondition<TypeT>::InitialCondition(const Mesh& mesh) : mesh_(mesh)
            { }


            template<ParameterNS::Type TypeT>
            inline typename InitialCondition<TypeT>::return_type InitialCondition<TypeT>::GetConstantValue() const
            {
                assert(IsConstant() && "This method is relevant only for spatially constant parameters.");

                return SupplGetConstantValue();
            }


            template<ParameterNS::Type TypeT>
            inline typename InitialCondition<TypeT>::return_type
            InitialCondition<TypeT>::GetValue(const SpatialPoint& coords) const
            {
                if (IsConstant())
                    return GetConstantValue();

                return SupplGetValue(coords);
            }


            template<ParameterNS::Type TypeT>
            inline const Mesh& InitialCondition<TypeT>::GetMesh() const noexcept
            {
                return mesh_;
            }


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_
