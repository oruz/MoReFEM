/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <set>
#include <type_traits>
#include <utility>

#include "Core/NumberingSubset/Internal/FindFunctor.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace VarfNS
        {


            const GlobalVector& GlobalVectorStorage::GetVector(const NumberingSubset& numbering_subset) const
            {
                const auto& storage = GetStorage();

                const auto it =
                    std::find_if(storage.cbegin(),
                                 storage.cend(),
                                 NumberingSubsetNS::FindIfCondition<GlobalVector::unique_ptr>(numbering_subset));

                assert(it != storage.cend()
                       && "It's likely you forgot to properly allocate the global vector with"
                          "AllocateGlobalVector() or VariationalFormulation::AllocateSystemVector().");

                assert(!(!*it));
                return *(*it);
            }


            GlobalVector& GlobalVectorStorage::NewVector(const GodOfDof& god_of_dof,
                                                         const NumberingSubset& numbering_subset)
            {
                auto ptr = std::make_unique<GlobalVector>(numbering_subset);
                AllocateGlobalVector(god_of_dof, *ptr);

                auto& storage = GetNonCstStorage();
                storage.emplace_back(std::move(ptr));

#ifndef NDEBUG
                AssertNoDuplicate();
#endif // NDEBUG

                return *(storage.back());
            }


#ifndef NDEBUG
            void GlobalVectorStorage::AssertNoDuplicate() const
            {
                const auto& storage = GetStorage();

                std::set<std::size_t> id_list;
                for (const auto& ptr : storage)
                {
                    assert(!(!ptr));
                    auto check = id_list.insert(ptr->GetNumberingSubset().GetUniqueId());

                    assert(check.second && "A given unique Id should be present only once!");
                }
            }
#endif // NDEBUG


        } // namespace VarfNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup
