/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace VarfNS
        {


            inline GlobalVector& GlobalVectorStorage::GetNonCstVector(const NumberingSubset& numbering_subset)
            {
                return const_cast<GlobalVector&>(GetVector(numbering_subset));
            }


            inline const GlobalVector::vector_unique_ptr& GlobalVectorStorage ::GetStorage() const
            {
                return storage_;
            }


            inline GlobalVector::vector_unique_ptr& GlobalVectorStorage ::GetNonCstStorage()
            {
                return storage_;
            }


        } // namespace VarfNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HXX_
