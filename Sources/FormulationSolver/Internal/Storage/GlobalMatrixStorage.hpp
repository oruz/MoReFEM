/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HPP_

#include <memory>
#include <vector>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace VarfNS
        {


            /*!
             * \brief Class in charge of storing global matrices.
             *
             * \internal <b><tt>[internal]</tt></b> Actual storage use a 'flat-map': it is a STL vector upon which find
             * algorithm is used.
             * The reason for this is that we do not expect many global matrices stored, so flat map is likely to
             * be more efficient that std::map or even std::unordered_map.
             * The key for the search is the pair of numbering subsets that identify which dofs are used respectively
             * for rows and columns.
             * \endinternal
             */
            class GlobalMatrixStorage final
            {

              public:
                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const GlobalMatrixStorage>;

                //! Alias to vector of unique pointers.
                using vector_const_unique_ptr = std::vector<const_unique_ptr>;

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit GlobalMatrixStorage() = default;

                //! Destructor.
                ~GlobalMatrixStorage() = default;

                //! \copydoc doxygen_hide_copy_constructor
                GlobalMatrixStorage(const GlobalMatrixStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GlobalMatrixStorage(GlobalMatrixStorage&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GlobalMatrixStorage& operator=(const GlobalMatrixStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GlobalMatrixStorage& operator=(GlobalMatrixStorage&& rhs) = delete;

                ///@}


                /*!
                 * \brief Add the matrix described by the given numbering subsets.
                 *
                 * There can be only one for a given pair of numbering subset; however ordering matters (so
                 * { 1, 2 } and { 2, 1 } can both exist, when 1 and 2 are numbering subset unique ids).
                 *
                 * \copydetails doxygen_hide_matrix_numbering_subset_arg
                 *
                 * \return A reference to the newly created matrix.
                 */
                GlobalMatrix& NewMatrix(const NumberingSubset& row_numbering_subset,
                                        const NumberingSubset& col_numbering_subset);


                //! Access to the global matrix matching the given pair of numbering subset.
                //! \copydetails doxygen_hide_matrix_numbering_subset_arg
                const GlobalMatrix& GetMatrix(const NumberingSubset& row_numbering_subset,
                                              const NumberingSubset& col_numbering_subset) const;

                //! Non constant access to the global matrix matching the given pair of numbering subset.
                //! \copydetails doxygen_hide_matrix_numbering_subset_arg
                GlobalMatrix& GetNonCstMatrix(const NumberingSubset& row_numbering_subset,
                                              const NumberingSubset& col_numbering_subset);


              private:
                //! Access to the internal storage.
                const GlobalMatrix::vector_unique_ptr& GetStorage() const;

                //! Non constant access to the internal storage.
                GlobalMatrix::vector_unique_ptr& GetNonCstStorage();

#ifndef NDEBUG
                //! Check \a NewMatrix() is called at most once for each numbering subset.
                void AssertNoDuplicate() const;
#endif // NDEBUG


              private:
                //! List of all global matrices with the numbering subsets used to find them.
                GlobalMatrix::vector_unique_ptr storage_;
            };


        } // namespace VarfNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HPP_
