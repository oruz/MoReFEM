/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace VarfNS
        {


            inline GlobalMatrix& GlobalMatrixStorage::GetNonCstMatrix(const NumberingSubset& row_numbering_subset,
                                                                      const NumberingSubset& col_numbering_subset)
            {
                return const_cast<GlobalMatrix&>(GetMatrix(row_numbering_subset, col_numbering_subset));
            }


            inline const GlobalMatrix::vector_unique_ptr& GlobalMatrixStorage ::GetStorage() const
            {
                return storage_;
            }


            inline GlobalMatrix::vector_unique_ptr& GlobalMatrixStorage ::GetNonCstStorage()
            {
                return storage_;
            }


        } // namespace VarfNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_MATRIX_STORAGE_HXX_
