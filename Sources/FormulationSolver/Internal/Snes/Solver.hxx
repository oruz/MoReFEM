/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/Snes/Solver.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::SolverNS
{


    template<std::size_t SolverIndexT, class InputDataT>
    ::MoReFEM::Wrappers::Petsc::Snes::unique_ptr
    InitSolver(const ::MoReFEM::Wrappers::Mpi& mpi,
               const InputDataT& input_data,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESFunction snes_function,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESJacobian snes_jacobian,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESViewer snes_viewer,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function)
    {
        namespace ipl = Utilities::InputDataNS;
        using ip_petsc = ::MoReFEM::InputDataNS::Petsc<SolverIndexT>;

        decltype(auto) solver = ipl::Extract<typename ip_petsc::Solver>::Value(input_data);
        const std::string& preconditioner = ipl::Extract<typename ip_petsc::Preconditioner>::Value(input_data);

        const double absolute_tolerance = ipl::Extract<typename ip_petsc::AbsoluteTolerance>::Value(input_data);
        const double relative_tolerance = ipl::Extract<typename ip_petsc::RelativeTolerance>::Value(input_data);
        const double step_size_tolerance = ipl::Extract<typename ip_petsc::StepSizeTolerance>::Value(input_data);
        const std::size_t gmres_restart = ipl::Extract<typename ip_petsc::GmresRestart>::Value(input_data);
        const std::size_t max_iteration = ipl::Extract<typename ip_petsc::MaxIteration>::Value(input_data);

        return std::make_unique<::MoReFEM::Wrappers::Petsc::Snes>(mpi,
                                                                  solver,
                                                                  preconditioner,
                                                                  gmres_restart,
                                                                  absolute_tolerance,
                                                                  relative_tolerance,
                                                                  step_size_tolerance,
                                                                  max_iteration,
                                                                  snes_function,
                                                                  snes_jacobian,
                                                                  snes_viewer,
                                                                  snes_convergence_test_function,
                                                                  __FILE__,
                                                                  __LINE__);
    }


} // namespace MoReFEM::Internal::SolverNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_
