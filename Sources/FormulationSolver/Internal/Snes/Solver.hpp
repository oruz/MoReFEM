/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"           // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: export

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM::Internal::SolverNS
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Init Petsc solver.
     *
     *
     * \copydetails doxygen_hide_mpi_param
     * \copydoc doxygen_hide_input_data_arg
     * \copydoc doxygen_hide_snes_functions_args
     *
     * \return \a Wrappers::Petsc::Snes correctly initialized.
     */
    template<std::size_t SolverIndexT, class InputDataT>
    ::MoReFEM::Wrappers::Petsc::Snes::unique_ptr
    InitSolver(const ::MoReFEM::Wrappers::Mpi& mpi,
               const InputDataT& input_data,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESFunction snes_function = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESJacobian snes_jacobian = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESViewer snes_viewer = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function = nullptr);


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM::Internal::SolverNS


/// @} // addtogroup CoreGroup


#include "FormulationSolver/Internal/Snes/Solver.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HPP_
