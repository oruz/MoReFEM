/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Oct 2016 14:26:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/Snes/SnesInterface.hpp"

#include "ThirdParty/Wrappers/Petsc/SnesMacro.hpp" // IWYU pragma: export

#include "Core/LinearAlgebra/GlobalMatrix.hpp" // IWYU pragma: export
#include "Core/LinearAlgebra/GlobalVector.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::SolverNS
{


    template<class VariationalFormulationT>
    PetscErrorCode SnesInterface<VariationalFormulationT>::Function(SNES snes,
                                                                    Vec petsc_evaluation_state,
                                                                    Vec petsc_residual,
                                                                    void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            static_cast<void>(snes);

            // \a context_as_void is in fact the VariationalFormulation object.
            assert(!(!context_as_void));
            VariationalFormulationT* variational_formulation_ptr =
                static_cast<VariationalFormulationT*>(context_as_void);
            auto& variational_formulation = *variational_formulation_ptr;

            decltype(auto) row_numbering_subset = variational_formulation.GetRowNumberingSubset();
            decltype(auto) col_numbering_subset = variational_formulation.GetColumnNumberingSubset();

            GlobalVector residual(row_numbering_subset);
            residual.ChangeInternal(petsc_residual);
            residual.ZeroEntries(__FILE__, __LINE__);

            GlobalVector evaluation_state(col_numbering_subset);
            evaluation_state.ChangeInternal(petsc_evaluation_state);

            variational_formulation.ComputeResidual(evaluation_state, residual);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesFunction)

        return 0;
    }


    template<class VariationalFormulationT>
    PetscErrorCode SnesInterface<VariationalFormulationT>::Jacobian(SNES snes,
                                                                    Vec petsc_evaluation_state,
                                                                    Mat petsc_jacobian,
                                                                    Mat petsc_preconditioner,
                                                                    void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            static_cast<void>(snes);

            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            VariationalFormulationT* formulation_ptr = static_cast<VariationalFormulationT*>(context_as_void);
            auto& variational_formulation = *formulation_ptr;

            decltype(auto) row_numbering_subset = variational_formulation.GetRowNumberingSubset();
            decltype(auto) col_numbering_subset = variational_formulation.GetColumnNumberingSubset();
            decltype(auto) mpi = variational_formulation.GetMpi();

            GlobalVector evaluation_state(col_numbering_subset);
            evaluation_state.ChangeInternal(petsc_evaluation_state);

            GlobalMatrix tangent(row_numbering_subset, col_numbering_subset);
            tangent.ChangeInternal(mpi, petsc_jacobian);
            tangent.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrix preconditioner(row_numbering_subset, col_numbering_subset);
            preconditioner.ChangeInternal(mpi, petsc_preconditioner);
            preconditioner.ZeroEntries(__FILE__, __LINE__);

            variational_formulation.ComputeTangent(evaluation_state, tangent, preconditioner);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesJacobian)

        return 0;
    }


    template<class VariationalFormulationT>
    PetscErrorCode
    SnesInterface<VariationalFormulationT>::Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            static_cast<void>(snes);
            assert(context_as_void);

            const VariationalFormulationT* variational_formulation_ptr =
                static_cast<const VariationalFormulationT*>(context_as_void);

            assert(!(!variational_formulation_ptr));

            const auto& variational_formulation = *variational_formulation_ptr;

            variational_formulation.SnesMonitorFunction(its, norm);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesViewer)

        return 0;
    }


} // namespace MoReFEM::Internal::SolverNS


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HXX_
