/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 22 Jun 2016 16:37:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HXX_

// IWYU pragma: private, include "FormulationSolver/Crtp/HyperelasticLaw.hpp"


namespace MoReFEM
{


    namespace FormulationSolverNS
    {


        template<class DerivedT, class HyperelasticLawT>
        template<typename... Args>
        void HyperelasticLaw<DerivedT, HyperelasticLawT>::Create(Args&&... args)
        {
            assert(hyperelastic_law_ == nullptr && "Should be called only once!");

            hyperelastic_law_ = std::make_unique<HyperelasticLawT>(std::forward<decltype(args)>(args)...);
        }


        template<class DerivedT, class HyperelasticLawT>
        const HyperelasticLawT& HyperelasticLaw<DerivedT, HyperelasticLawT>::GetHyperelasticLaw() const noexcept
        {
            assert(!(!hyperelastic_law_));
            return *hyperelastic_law_;
        }


        template<class DerivedT, class HyperelasticLawT>
        const HyperelasticLawT* HyperelasticLaw<DerivedT, HyperelasticLawT>::GetHyperelasticLawPtr() const noexcept
        {
            assert(!(!hyperelastic_law_));
            return hyperelastic_law_.get();
        }


    } // namespace FormulationSolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HXX_
