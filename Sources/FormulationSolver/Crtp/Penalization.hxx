/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Mar 2018 14:33:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HXX_

// IWYU pragma: private, include "FormulationSolver/Crtp/Penalization.hpp"


namespace MoReFEM
{


    namespace FormulationSolverNS
    {


        template<class DerivedT, class PenalizationLawT>
        template<typename... Args>
        void Penalization<DerivedT, PenalizationLawT>::Create(Args&&... args)
        {
            assert(penalization_ == nullptr && "Should be called only once!");

            penalization_ = std::make_unique<PenalizationLawT>(std::forward<decltype(args)>(args)...);
        }


        template<class DerivedT, class PenalizationLawT>
        const PenalizationLawT& Penalization<DerivedT, PenalizationLawT>::GetPenalizationLaw() const noexcept
        {
            assert(!(!penalization_));
            return *penalization_;
        }


        template<class DerivedT, class PenalizationLawT>
        const PenalizationLawT* Penalization<DerivedT, PenalizationLawT>::GetPenalizationLawPtr() const noexcept
        {
            assert(!(!penalization_));
            return penalization_.get();
        }


    } // namespace FormulationSolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_PENALIZATION_HXX_
