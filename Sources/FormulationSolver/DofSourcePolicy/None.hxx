/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 13:35:14 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HXX_

// IWYU pragma: private, include "FormulationSolver/DofSourcePolicy/None.hpp"


namespace MoReFEM
{


    namespace VariationalFormulationNS
    {


        namespace DofSourcePolicyNS
        {


        } // namespace DofSourcePolicyNS


    } // namespace VariationalFormulationNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_NONE_HXX_
