/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 13:35:14 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#include "FormulationSolver/DofSourcePolicy/None.hpp"


namespace MoReFEM
{


    namespace VariationalFormulationNS
    {


        namespace DofSourcePolicyNS
        {


            void None::AddToRhs(GlobalVector& rhs)
            {
                static_cast<void>(rhs);
            }


        } // namespace DofSourcePolicyNS


    } // namespace VariationalFormulationNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup
