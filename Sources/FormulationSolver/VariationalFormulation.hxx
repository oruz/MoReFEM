/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "FormulationSolver/VariationalFormulation.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<class MoReFEMDataT>
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::VariationalFormulation(
        const MoReFEMDataT& morefem_data,
        TimeManager& time_manager,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
    : Crtp::CrtpMpi<VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>>(morefem_data.GetMpi()),
      result_directory_(morefem_data.GetResultDirectory()), time_manager_(time_manager), snes_(nullptr),
      god_of_dof_(god_of_dof), boundary_condition_list_(std::move(boundary_condition_list))
    { }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GodOfDof&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetGodOfDof() const noexcept
    {
        return god_of_dof_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<class MoReFEMDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::Init(const MoReFEMDataT& morefem_data)
    {
        auto& derived = static_cast<DerivedT&>(*this);

        decltype(auto) input_data = morefem_data.GetInputData();

        if constexpr (NonLinearSolverT == enable_non_linear_solver::yes)
        {
            snes_ = Internal::SolverNS::InitSolver<SolverIndexT>(this->GetMpi(),
                                                                 input_data,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Function,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Jacobian,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Viewer,
                                                                 derived.ImplementSnesConvergenceTestFunction());
        } else
        {
            snes_ = Internal::SolverNS::InitSolver<SolverIndexT>(this->GetMpi(), input_data);
        }

        namespace IPL = Utilities::InputDataNS;

        display_value_ = IPL::Extract<InputDataNS::Result::DisplayValue>::Value(input_data);
        derived.AllocateMatricesAndVectors();
        derived.SupplInit(morefem_data);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<IsFactorized IsFactorizedT>
    inline void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::SolveLinear(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset,
        const char* invoking_file,
        int invoking_line,
        Wrappers::Petsc::print_solver_infos do_print_solver_infos)
    {
        return SolveLinear<IsFactorizedT>(GetSystemMatrix(row_numbering_subset, col_numbering_subset),
                                          GetSystemRhs(col_numbering_subset),
                                          GetNonCstSystemSolution(col_numbering_subset),
                                          invoking_file,
                                          invoking_line,
                                          do_print_solver_infos);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<IsFactorized IsFactorizedT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::SolveLinear(
        const GlobalMatrix& matrix,
        const GlobalVector& rhs,
        GlobalVector& out,
        const char* invoking_file,
        int invoking_line,
        Wrappers::Petsc::print_solver_infos do_print_solver_infos_arg)
    {
        assert(out.GetNumberingSubset() == rhs.GetNumberingSubset());

        auto do_print_solver_info = (this->GetTimeManager().NtimeModified() % GetDisplayValue()) == 0
                                        ? Wrappers::Petsc::print_solver_infos::yes
                                        : Wrappers::Petsc::print_solver_infos::no;

        if (do_print_solver_infos_arg == Wrappers::Petsc::print_solver_infos::no)
        {
            do_print_solver_info = Wrappers::Petsc::print_solver_infos::no;
        }

        switch (IsFactorizedT)
        {
        case IsFactorized::no:
        {

            GetNonCstSnes().SolveLinear(matrix, matrix, rhs, out, invoking_file, invoking_line, do_print_solver_info);
            break;
        }
        case IsFactorized::yes:
        {
            GetNonCstSnes().SolveLinear(rhs, out, invoking_file, invoking_line, do_print_solver_info);

            break;
        }
        }

        out.UpdateGhosts(__FILE__, __LINE__);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void>
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::SolveNonLinear(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset,
        const char* invoking_file,
        int invoking_line,
        Wrappers::Petsc::check_convergence do_check_convergence,
        SNESLineSearchType line_search_type)
    {
        const auto& system_matrix = this->GetSystemMatrix(row_numbering_subset, col_numbering_subset);
        const auto& system_rhs = this->GetSystemRhs(col_numbering_subset);
        auto& system_solution = this->GetNonCstSystemSolution(col_numbering_subset);

        auto& snes = GetNonCstSnes();

        this->SetNonLinearNumberingSubset(&row_numbering_subset, &col_numbering_subset);

        snes.SetLineSearchType(line_search_type);

        snes.SolveNonLinear(static_cast<void*>(this),
                            system_rhs,
                            system_matrix,
                            system_matrix,
                            system_solution,
                            invoking_file,
                            invoking_line,
                            do_check_convergence);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<VariationalFormulationNS::On AppliedOnT, BoundaryConditionMethod BoundaryConditionMethodT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::ApplyEssentialBoundaryCondition(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        if (GetEssentialBoundaryConditionList().empty())
            return;

        auto& system_matrix = GetNonCstSystemMatrix(row_numbering_subset, col_numbering_subset);
        auto& system_rhs = GetNonCstSystemRhs(col_numbering_subset);

        const auto& god_of_dof = GetGodOfDof();

        const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));

            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(row_numbering_subset))
                continue;

            switch (AppliedOnT)
            {
            case VariationalFormulationNS::On::system_matrix:
            case VariationalFormulationNS::On::system_matrix_and_rhs:
                god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, system_matrix);
                break;
            case VariationalFormulationNS::On::system_rhs:
                break;
            }


            switch (AppliedOnT)
            {
            case VariationalFormulationNS::On::system_rhs:
            case VariationalFormulationNS::On::system_matrix_and_rhs:
                god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, system_rhs);
                break;
            case VariationalFormulationNS::On::system_matrix:
                break;
            }
        }

        // Call Petsc Assembly() properly on modified linear algebra.
        switch (AppliedOnT)
        {
        case VariationalFormulationNS::On::system_matrix:
        case VariationalFormulationNS::On::system_matrix_and_rhs:
            system_matrix.Assembly(__FILE__, __LINE__);
            break;
        case VariationalFormulationNS::On::system_rhs:
            break;
        }


        switch (AppliedOnT)
        {
        case VariationalFormulationNS::On::system_rhs:
        case VariationalFormulationNS::On::system_matrix_and_rhs:
            system_rhs.Assembly(__FILE__, __LINE__);
            break;
        case VariationalFormulationNS::On::system_matrix:
            break;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<class LinearAlgebraT, BoundaryConditionMethod BoundaryConditionMethodT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::ApplyEssentialBoundaryCondition(
        LinearAlgebraT& linear_algebra)
    {
        static_assert(std::is_same<LinearAlgebraT, GlobalMatrix>() || std::is_same<LinearAlgebraT, GlobalVector>(),
                      "Can't be another type (as GodOfDof::ApplyBoundaryCondition() should have an overload for "
                      "LinearAlgebraT.");

        if (GetEssentialBoundaryConditionList().empty())
            return;

        const auto& god_of_dof = GetGodOfDof();

        const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();

        // Trick to mimic a static ternary operator.
        auto fetch_relevant_numbering_subset = [&linear_algebra]() -> const NumberingSubset&
        {
            if constexpr (std::is_same<LinearAlgebraT, GlobalMatrix>())
                return linear_algebra.GetRowNumberingSubset();
            else
                return linear_algebra.GetNumberingSubset();
        };

        const auto& numbering_subset = fetch_relevant_numbering_subset();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));

            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(numbering_subset))
                continue;

            god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, linear_algebra);
        }

        // Call Petsc Assembly() properly on modified linear algebra.
        linear_algebra.Assembly(__FILE__, __LINE__);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::WriteSolution(
        const TimeManager& transient,
        const NumberingSubset& numbering_subset) const
    {
        std::string output_file;

        const auto& mpi = this->GetMpi();
        const auto& output_directory = GetOutputDirectory(numbering_subset);
        std::ostringstream oconv;


        oconv << "/solution_time_" << std::setfill('0') << std::setw(5) << transient.NtimeModified();

        std::string only_filename = oconv.str();
        oconv.str("");

        {
            oconv << output_directory << only_filename;

            switch (Utilities::OutputFormat::GetInstance(__FILE__, __LINE__).IsBinaryOutput())
            {
            case binary_or_ascii::ascii:
                oconv << ".hhdata";
                break;
            case binary_or_ascii::binary:
                oconv << ".binarydata";
                break;
            case binary_or_ascii::from_input_data:
                assert(false && "Should not happen");
                exit(EXIT_FAILURE);
            } // switch

            output_file = oconv.str();
        }

        GetSystemSolution(numbering_subset)
            .template Print<MpiScale::processor_wise>(mpi, output_file, __FILE__, __LINE__);

        if (mpi.IsRootProcessor())
        {
            std::ofstream inout;
            FilesystemNS::File::Append(inout, GetTimeManager().GetTimeIterationFile(), __FILE__, __LINE__);

            oconv.str("");
            oconv << GetGodOfDof()
                         .template GetOutputDirectoryForNumberingSubset<Internal::GodOfDofNS::wildcard_for_rank::yes>(
                             numbering_subset)
                  << only_filename;

            switch (Utilities::OutputFormat::GetInstance(__FILE__, __LINE__).IsBinaryOutput())
            {
            case binary_or_ascii::ascii:
                oconv << ".hhdata";
                break;
            case binary_or_ascii::binary:
                oconv << ".binarydata";
                break;
            case binary_or_ascii::from_input_data:
                assert(false && "Should not happen");
                exit(EXIT_FAILURE);
            } // switch

            inout << transient.NtimeModified() << ';' << transient.GetTime() << ';' << numbering_subset.GetUniqueId()
                  << ';' << oconv.str() << std::endl;
        }
    }


    //////////////////////////
    // PROTECTED ACCESSORS  //
    //////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalMatrix& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset) const
    {
        return GetGlobalMatrixStorage().GetMatrix(row_numbering_subset, col_numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalMatrix& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        return GetNonCstGlobalMatrixStorage().GetNonCstMatrix(row_numbering_subset, col_numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalVector& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetSystemRhs(
        const NumberingSubset& numbering_subset) const
    {
        return GetRhsVectorStorage().GetVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalVector& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstSystemRhs(
        const NumberingSubset& numbering_subset)
    {
        return GetNonCstRhsVectorStorage().GetNonCstVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalVector& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetSystemSolution(
        const NumberingSubset& numbering_subset) const
    {
        return GetSolutionVectorStorage().GetVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalVector& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstSystemSolution(
        const NumberingSubset& numbering_subset)
    {
        return GetNonCstSolutionVectorStorage().GetNonCstVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const TimeManager& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetTimeManager() const
    {
        return time_manager_;
    }


    //////////////////////////
    // PRIVATE METHODS      //
    //////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::AllocateSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        auto& global_matrix_storage = GetNonCstGlobalMatrixStorage();

        auto& matrix = global_matrix_storage.NewMatrix(row_numbering_subset, col_numbering_subset);

        AllocateGlobalMatrix(this->GetGodOfDof(), matrix);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::AllocateSystemVector(
        const NumberingSubset& numbering_subset)
    {
        {
            auto& rhs_storage = GetNonCstRhsVectorStorage();
            rhs_storage.NewVector(this->GetGodOfDof(), numbering_subset);
        }

        {
            auto& solution_storage = GetNonCstSolutionVectorStorage();
            solution_storage.NewVector(this->GetGodOfDof(), numbering_subset);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<std::size_t IndexT, class InputDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::InitializeVectorSystemSolution(
        const InputDataT& input_data,
        const NumberingSubset& numbering_subset,
        const Unknown& unknown,
        const FEltSpace& felt_space)
    {
        const auto& god_of_dof = this->GetGodOfDof();

        GlobalVector& system_solution = VariationalFormulation::GetNonCstSystemSolution(numbering_subset);

        InitializeVectorSystemSolution<IndexT>(input_data, numbering_subset, unknown, felt_space, system_solution);

        const auto& dirichlet_bc_list = this->GetEssentialBoundaryConditionList();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));
            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(numbering_subset))
                continue;

            god_of_dof.ApplyPseudoElimination(dirichlet_bc, system_solution);
        }

        system_solution.Assembly(__FILE__, __LINE__);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<std::size_t IndexT, class InputDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::InitializeVectorSystemSolution(
        const InputDataT& input_data,
        const NumberingSubset& numbering_subset,
        const Unknown& unknown,
        const FEltSpace& felt_space,
        GlobalVector& vector)
    {
        const auto& god_of_dof = this->GetGodOfDof();
        const auto& mesh = god_of_dof.GetMesh();

        using initial_condition_type = InputDataNS::InitialCondition<IndexT>;

        auto initial_condition_ptr =
            Internal::FormulationSolverNS::InitThreeDimensionalInitialCondition<initial_condition_type>(mesh,
                                                                                                        input_data);

        // Nullptr sign the case initial condition should be ignored.
        if (!initial_condition_ptr)
            return;

        const auto& initial_condition = *initial_condition_ptr;


        const auto& extended_unknown = felt_space.GetExtendedUnknown(unknown);
        const auto& extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> local_array(vector, __FILE__, __LINE__);

        const auto& felt_list_per_type =
            felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        const auto Nprocessor_wise_dof = static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(numbering_subset));

        const std::size_t mesh_dimension = felt_space.GetMeshDimension();

        const auto Ncomponent_local = static_cast<std::size_t>(Ncomponent(unknown, mesh_dimension));

        SpatialPoint node_coords;

        for (const auto& pair : felt_list_per_type)
        {
            const auto& ref_felt_space_list_ptr = pair.first;

            const auto& local_felt_space_list = pair.second;

            const auto& ref_node_list =
                ref_felt_space_list_ptr->GetRefFElt(extended_unknown).GetBasicRefFElt().GetLocalNodeList();

            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                auto& local_felt_space = *local_felt_space_ptr;

                ExtendedUnknown::vector_const_shared_ptr buf{ extended_unknown_ptr };

                local_felt_space.ComputeLocal2Global(buf, DoComputeProcessorWiseLocal2Global::yes);

                const auto& local_2_global =
                    local_felt_space.GetLocal2Global<MoReFEM::MpiScale::processor_wise>(extended_unknown);

                const auto Nlocal_node = ref_node_list.size();

                for (std::size_t i_node = 0; i_node < Nlocal_node; ++i_node)
                {
                    const auto& geometric_element = local_felt_space_ptr->GetGeometricElt();

                    const auto& local_coords = ref_node_list[i_node]->GetLocalCoords();

                    Advanced::GeomEltNS::Local2Global(geometric_element, local_coords, node_coords);

                    const auto& initial_condition_value = initial_condition.GetValue(node_coords);

                    std::size_t dof_index = i_node;

                    assert(initial_condition_value.size() >= Ncomponent_local);

                    if (local_2_global[i_node] < Nprocessor_wise_dof)
                    {
                        for (auto component = 0ul; component < Ncomponent_local; ++component, dof_index += Nlocal_node)
                        {
                            const std::size_t dof_index_int = static_cast<std::size_t>(local_2_global[dof_index]);
                            local_array[dof_index_int] = initial_condition_value(component);
                        }
                    }
                }
            }
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Wrappers::Petsc::Snes&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetSnes() const noexcept
    {
        assert(!(!snes_));
        return *snes_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Wrappers::Petsc::Snes& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstSnes() noexcept
    {
        return const_cast<Wrappers::Petsc::Snes&>(GetSnes());
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    std::string VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetOutputDirectory(
        const NumberingSubset& numbering_subset) const
    {
        return GetGodOfDof().GetOutputDirectoryForNumberingSubset(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const std::string&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetResultDirectory() const noexcept
    {
        return result_directory_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalMatrixStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstGlobalMatrixStorage()
    {
        return global_matrix_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalMatrixStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetGlobalMatrixStorage() const
    {
        return global_matrix_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetRhsVectorStorage() const
    {
        return global_rhs_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstRhsVectorStorage()
    {
        return global_rhs_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetSolutionVectorStorage() const
    {
        return global_solution_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstSolutionVectorStorage()
    {
        return global_solution_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const DirichletBoundaryCondition::vector_shared_ptr&
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetEssentialBoundaryConditionList() const noexcept
    {
        return boundary_condition_list_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    std::size_t VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetDisplayValue() const
    {
        return display_value_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline TimeManager& VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetNonCstTimeManager()
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::SnesMonitorFunction(PetscInt iteration_index,
                                                                                               PetscReal norm) const
    {
        if constexpr (NonLinearSolverT == enable_non_linear_solver::yes)
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\tAt Newton iteration %d norm is %14.12e\n",
                                                          this->GetMpi(),
                                                          __FILE__,
                                                          __LINE__,
                                                          iteration_index,
                                                          norm);
        } else
        {
            static_cast<void>(iteration_index);
            static_cast<void>(norm);

            // I would have liked to use enable_if on this method, but as I also want it virtual it wasn't possible
            // hence this crude exit here.
            assert(false && "Should not be called if non linear solver is not enabled.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetRowNumberingSubset() const noexcept
    {
        assert(!(!row_numbering_subset_));
        return *row_numbering_subset_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::GetColumnNumberingSubset() const noexcept
    {
        assert(!(!column_numbering_subset_));
        return *column_numbering_subset_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void>
    VariationalFormulation<DerivedT, SolverIndexT, NonLinearSolverT>::SetNonLinearNumberingSubset(
        const NumberingSubset* row,
        const NumberingSubset* col)
    {
        row_numbering_subset_ = row;
        column_numbering_subset_ = col;
    }


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HXX_
