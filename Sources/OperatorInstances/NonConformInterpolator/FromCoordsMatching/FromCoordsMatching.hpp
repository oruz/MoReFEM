/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp" // \todo #762 Not very clean...

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }
namespace MoReFEM::Internal::FEltSpaceNS { class CoordsMatchingInterpolator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace NonConformInterpolatorNS
    {

        /*!
         * \brief Enum class to say whether matrix pattern should be stored.
         *
         * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
         */
        enum class store_matrix_pattern
        {
            yes,
            no
        };


        //! Convenient alias to pairing.
        using pairing_type = Advanced::ConformInterpolatorNS::pairing_type;


        /*!
         * \brief Class that matches the dofs on vertices from two different meshes.
         *
         * \attention This class is a tad unwieldy to use, and currently assumes that:
         *
         * . Finite element spaces/numbering subset couples from both source and target feature the same amounts
         * of unknowns sort in the same way. In other words, (velocity_solid, pressure_solid) for source and
         * (pressure_fluid, velocity_fluid) won't be handled for the time being.
         * . Finite elements on interface must be P1.
         *
         * \internal There is room for improvement here should the construction of this interpolator proves to be a
         * bottleneck (I don't think so, but still possible): the reduction to processor-wise dofs intervenes rather
         * lately in the process and so there are some unused computation.
         * \endinternal
         */
        class FromCoordsMatching
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = FromCoordsMatching;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to unique pointer to const object.
            using const_unique_ptr = std::unique_ptr<const self>;


          public:
            /// \name Special members.
            ///@{


            /*!
             * \class doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
             *
             * \param[in] do_store_matrix_pattern Whether matrix pattern should be stored.
             * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
             */


            /*! Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             * \copydoc doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
             * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
             * \param[in] coords_matching_interpolator_index Index in the input file of the \a CoordsMatching operator.
             */
            template<class MoReFEMDataT>
            explicit FromCoordsMatching(const MoReFEMDataT& morefem_data,
                                        std::size_t coords_matching_interpolator_index,
                                        store_matrix_pattern do_store_matrix_pattern = store_matrix_pattern::no);

            //! Destructor.
            ~FromCoordsMatching() = default;

            //! \copydoc doxygen_hide_copy_constructor
            FromCoordsMatching(const FromCoordsMatching& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            FromCoordsMatching(FromCoordsMatching&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            FromCoordsMatching& operator=(const FromCoordsMatching& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            FromCoordsMatching& operator=(FromCoordsMatching&& rhs) = delete;

            ///@}

            //! Access to interpolation matrix.
            const GlobalMatrix& GetInterpolationMatrix() const noexcept;

            /*!
             * \brief Pattern of the matrix.
             *
             * \attention Should be called only if the matrix pattern is stored in the class (see constructor dedicated
             * argument).
             *
             * \return Pattern of the matrix.
             */
            const Wrappers::Petsc::MatrixPattern& GetMatrixPattern() const noexcept;

            /*!
             * \brief Keep for each processor-wise index of target the position of the related \a Dof on source.
             *
             * This information is a bit redundant with the interpolation matrix itself but is very helpful for debug
             * purposes.
             *
             * \return There is one item per row (which processor-wise index is represented by the index of the vector); the value inside is the (program-wise)
             * position of the associated source \a Dof.
             *
             */
            const std::vector<PetscInt>& GetNonZeroPositionPerRow() const noexcept;


          private:
            /*!
             * \brief Construct the object in the case of a standard run. Should not be called outside of constructor.
             *
             * \param[in] coords_matching Object which holds information about the \a geometric matching between both
             * meshes.
             * \param[in] coords_matching_interpolator Internal object used by the interpolator.
             * \copydoc doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
             * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
             */
            void
            StandardConstruct(const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                              const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
                              store_matrix_pattern do_store_matrix_pattern);

            /*!
             * \brief Construct the object in the case of a run from prepartitioned data.
             *
             *  Should not be called outside of constructor.
             *
             *  \copydetails doxygen_hide_mpi_param
             *  \param[in] coords_matching_interpolator_index Index identifying the interpolator.
             */
            void ConstructFromPrepartitionedData(const Wrappers::Mpi& mpi,
                                                 const std::size_t coords_matching_interpolator_index);


            //! Access to interpolation matrix.
            GlobalMatrix& GetNonCstInterpolationMatrix() noexcept;

            /*!
             * \brief Write in the chosen directory date required to rebuild  the interpolator.
             *
             * As the \a Coords::index_from_mesh_file is not the same in the case of a run from prepartitioned data
             * (which uses up a reduced mesh from the start), the \a MeshNS::InterpolationNS::CoordsMatching object
             * can't be used with reconstructed data. The only missing data however is the position for each row of the
             * non zero element, which is read from a file savec in \a directory.
             *
             * \attention This method does nothing if \a file_for_prepartitioned_data_ is empty (should happen only when no parallelism block in the input
             * file.
             */
            void PrintPrepartitionedData() const;


            /*!
             * \brief Set the file which includes the data for a prepartitioned run - if relevant.
             *
             * Nothing is done if the strategy chosen is Advanced::parallelism_strategy::parallel_no_write.
             *
             * \copydoc doxygen_hide_parallelism_param
             */
            void SetFileForPrepartitionedData(const Internal::Parallelism& parallelism);

            /*!
             * \brief Get the path of the file in which prepartitioned data are (or will be depending on the current parallelism strategy) stored.
             *
             * \return Path of the file if relevant, or an empty string if there are no parallelism strategy or parallelism block.
             */
            const std::string& GetFileFroPrepartitionedData() const noexcept;

          private:
            //! Global matrix used for the interpolation.
            GlobalMatrix::unique_ptr interpolation_matrix_ = nullptr;

            /*!
             * \brief Pattern of the matrix.
             *
             * \note It is stored only if constructor requires it explicitly - if not it is reset to nullptr once used.
             */
            Wrappers::Petsc::MatrixPattern::const_unique_ptr matrix_pattern_ = nullptr;

            /*!
             * \brief Keep for each processor-wise index of target the position of the related \a Dof on source.
             *
             * This information is a bit redundant with the interpolation matrix itself but is very helpful for debug
             * purposes.
             */
            std::vector<PetscInt> non_zero_position_per_row_;

            /*!
             * \brief Path to the file in which prepartitioned data will be written.
             *
             * This attribute must be set only by \a SetFileForPrepartitionedData; the directory creation if needed ois
             * handled there.
             */
            std::string file_for_prepartitioned_data_;

#ifndef NDEBUG

            //! Whether matrix pattern is kept within the class or not. If 'no', GetMatrixPattern() shouldn't be called.
            store_matrix_pattern do_store_matrix_pattern_ = store_matrix_pattern::no;
#endif // NDEBUG
        };


    } // namespace NonConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HPP_
