/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <numeric>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp" // IWYU pragma: associated

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace NonConformInterpolatorNS
    {


        namespace //  anonymous
        {


            NodeBearer::vector_shared_ptr
            ComputeNodeBearerListInNumberingSubset(const GodOfDof& god_of_dof, const NumberingSubset& numbering_subset);


            using node_bearer_per_coords_index_list_type =
                Internal::NodeBearerNS::node_bearer_per_coords_index_list_type;


            void AllocateInterpolatorMatrix(
                const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
                const Wrappers::Petsc::MatrixPattern& matrix_pattern,
                GlobalMatrix& interpolation_matrix);


            NodeBearer::vector_const_shared_ptr
            IdentifyTargetNodeBearerToProcess(const Dof::vector_shared_ptr& target_processor_wise_dof_list);


            /*!
             * \brief Compute for each row the position of the non zero element.
             *
             * We know there is exactly one per row due to the nature of the operator.
             *
             * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
             * zero elements.
             */
            std::vector<PetscInt> ComputeNonZeroPositionPerRow(
                const NodeBearer::vector_const_shared_ptr& target_node_bearer_list,
                const std::size_t Nprocessor_wise_target_dof,
                const NumberingSubset& source_numbering_subset,
                const NumberingSubset& target_numbering_subset,
                const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                const node_bearer_per_coords_index_list_type& source_node_bearer_list_per_coords);


            /*!
             * \brief "Convert" the result of \a ComputeNonZeroPositionPerRow() into the format expected by \a MatrixPattern.
             *
             *
             * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
             * zero elements.
             */
            std::vector<std::vector<PetscInt>>
            ComputeNonZeroPatternForMatrixPattern(const std::vector<PetscInt>& interpolation_pattern);


            std::vector<PetscInt> ComputeProgramWiseTargetDofIndexList(const FEltSpace& target_felt_space,
                                                                       const NumberingSubset& target_numbering_subset);


        } // namespace


        void FromCoordsMatching::PrintPrepartitionedData() const
        {
            decltype(auto) path = GetFileFroPrepartitionedData();

            if (!path.empty())
            {
                std::ofstream out;
                FilesystemNS::File::Create(out, path, __FILE__, __LINE__);

                Utilities::PrintContainer<>::Do(GetNonZeroPositionPerRow(),
                                                out,
                                                PrintNS::Delimiter::separator(","),
                                                PrintNS::Delimiter::opener("non_zero_position_per_row = {"),
                                                PrintNS::Delimiter::closer("}\n"));
            }
        }


        void FromCoordsMatching::SetFileForPrepartitionedData(const Internal::Parallelism& parallelism)
        {
            switch (parallelism.GetParallelismStrategy())
            {
            case Advanced::parallelism_strategy::none:
            case Advanced::parallelism_strategy::parallel_no_write:
            {
                assert(file_for_prepartitioned_data_.empty());
                break;
            }
            case Advanced::parallelism_strategy::precompute:
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::run_from_preprocessed:
            {
                decltype(auto) parallelism_dir = parallelism.GetDirectory();

                FilesystemNS::Directory interpolator_directory(
                    parallelism_dir, "FromCoordsMatchingInterpolator", __FILE__, __LINE__);

                file_for_prepartitioned_data_ = interpolator_directory.AddFile("non_zero_position_per_row.lua");
            }
            }
        }


        void FromCoordsMatching::StandardConstruct(
            const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
            const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
            store_matrix_pattern do_store_matrix_pattern)
        {

            decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
            decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();
            decltype(auto) source_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();
            decltype(auto) target_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

            interpolation_matrix_ = std::make_unique<GlobalMatrix>(target_numbering_subset, source_numbering_subset);

            const auto Nprocessor_wise_target_dof = target_felt_space.NprocessorWiseDof(target_numbering_subset);

#ifndef NDEBUG
            {
                const auto Nprogram_wise_source_dof = source_felt_space.NprogramWiseDof(source_numbering_subset);
                const auto Nprogram_wise_target_dof = target_felt_space.NprogramWiseDof(target_numbering_subset);
                assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);
            }
#endif // NDEBUG

            const auto target_processor_wise_dof_list =
                target_felt_space.GetProcessorWiseDofList(target_numbering_subset);

            decltype(auto) source_god_of_dof_ptr = source_felt_space.GetGodOfDofFromWeakPtr();
            assert(!(!source_god_of_dof_ptr));
            const auto& source_god_of_dof = *source_god_of_dof_ptr;

            NodeBearer::vector_shared_ptr source_node_bearer_list =
                ComputeNodeBearerListInNumberingSubset(source_god_of_dof, source_numbering_subset);

            const auto source_node_bearer_list_per_coords =
                Internal::NodeBearerNS::GenerateNodeBearerPerCoordsList(source_node_bearer_list);

            const auto target_node_bearer_list = IdentifyTargetNodeBearerToProcess(target_processor_wise_dof_list);

            non_zero_position_per_row_ = ComputeNonZeroPositionPerRow(target_node_bearer_list,
                                                                      Nprocessor_wise_target_dof,
                                                                      source_numbering_subset,
                                                                      target_numbering_subset,
                                                                      coords_matching,
                                                                      source_node_bearer_list_per_coords);

            matrix_pattern_ = std::make_unique<Wrappers::Petsc::MatrixPattern>(
                ComputeNonZeroPatternForMatrixPattern(non_zero_position_per_row_));

            decltype(auto) matrix_pattern = GetMatrixPattern();

            decltype(auto) interpolation_matrix = GetNonCstInterpolationMatrix();

            AllocateInterpolatorMatrix(coords_matching_interpolator, matrix_pattern, interpolation_matrix);

            // Fill the interpolation matrix.
            assert(non_zero_position_per_row_.size() == Nprocessor_wise_target_dof);

            decltype(auto) jCSR = matrix_pattern.GetJCsr();
            assert(jCSR.size() == Nprocessor_wise_target_dof);

            for (auto i = 0ul; i < Nprocessor_wise_target_dof; ++i)
            {
                const auto& target_processor_wise_dof_ptr = target_processor_wise_dof_list[i];
                assert(!(!target_processor_wise_dof_ptr));
                const auto program_wise_index =
                    static_cast<PetscInt>(target_processor_wise_dof_ptr->GetProgramWiseIndex(target_numbering_subset));

                interpolation_matrix.SetValue(program_wise_index, jCSR[i], 1., INSERT_VALUES, __FILE__, __LINE__);
            }

            interpolation_matrix.Assembly(__FILE__, __LINE__);

            if (do_store_matrix_pattern == store_matrix_pattern::no)
                matrix_pattern_ = nullptr;

            PrintPrepartitionedData();
        }


        void FromCoordsMatching::ConstructFromPrepartitionedData(const Wrappers::Mpi& mpi,
                                                                 const std::size_t coords_matching_interpolator_index)
        {
            decltype(auto) coords_matching_interpolator_manager =
                Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) coords_matching_interpolator =
                coords_matching_interpolator_manager.GetCoordsMatchingInterpolator(coords_matching_interpolator_index);

            LuaOptionFile lua_file(GetFileFroPrepartitionedData(), __FILE__, __LINE__);

#ifndef NDEBUG
            do_store_matrix_pattern_ = store_matrix_pattern::yes;
#endif // NDEBUG
            std::size_t Nrow_processor_wise, Ncol_processor_wise, Nrow_program_wise, Ncol_program_wise;

            decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
            decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();

            lua_file.Read("non_zero_position_per_row", "", non_zero_position_per_row_, __FILE__, __LINE__);

            decltype(auto) row_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

            decltype(auto) col_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();

            Nrow_processor_wise = target_felt_space.NprocessorWiseDof(row_numbering_subset);
            Nrow_program_wise = target_felt_space.NprogramWiseDof(row_numbering_subset);
            Ncol_processor_wise = source_felt_space.NprocessorWiseDof(col_numbering_subset);
            Ncol_program_wise = source_felt_space.NprogramWiseDof(col_numbering_subset);

            auto jCSR = non_zero_position_per_row_;

            std::vector<PetscInt> iCSR(Nrow_processor_wise + 1ul);
            std::iota(iCSR.begin(), iCSR.end(), 0);

            matrix_pattern_ = std::make_unique<Wrappers::Petsc::MatrixPattern>(std::move(iCSR), std::move(jCSR));

            interpolation_matrix_ = std::make_unique<GlobalMatrix>(row_numbering_subset, col_numbering_subset);

            interpolation_matrix_->InitParallelMatrix(Nrow_processor_wise,
                                                      Ncol_processor_wise,
                                                      Nrow_program_wise,
                                                      Ncol_program_wise,
                                                      *matrix_pattern_,
                                                      mpi,
                                                      __FILE__,
                                                      __LINE__);

            auto& interpolation_matrix = *interpolation_matrix_;

            const auto program_wise_target_dof_index_list =
                ComputeProgramWiseTargetDofIndexList(target_felt_space, row_numbering_subset);

            assert(non_zero_position_per_row_.size() == Nrow_processor_wise);
            assert(program_wise_target_dof_index_list.size() == Nrow_processor_wise);

            for (auto i = 0ul; i < Nrow_processor_wise; ++i)
                interpolation_matrix.SetValue(program_wise_target_dof_index_list[i],
                                              non_zero_position_per_row_[i],
                                              1.,
                                              INSERT_VALUES,
                                              __FILE__,
                                              __LINE__);

            interpolation_matrix.Assembly(__FILE__, __LINE__);
        }


        namespace //  anonymous
        {


            NodeBearer::vector_shared_ptr
            ComputeNodeBearerListInNumberingSubset(const GodOfDof& god_of_dof, const NumberingSubset& numbering_subset)
            {
                NodeBearer::vector_shared_ptr ret;

                decltype(auto) proc_node_bearer_list = god_of_dof.GetProcessorWiseNodeBearerList();
                decltype(auto) ghost_node_bearer_list = god_of_dof.GetGhostNodeBearerList();

                auto is_in_numbering_subset = [&numbering_subset](const NodeBearer::shared_ptr& node_bearer_ptr)
                {
                    assert(!(!node_bearer_ptr));
                    return node_bearer_ptr->IsInNumberingSubset(numbering_subset);
                };

                std::copy_if(proc_node_bearer_list.cbegin(),
                             proc_node_bearer_list.cend(),
                             std::back_inserter(ret),
                             is_in_numbering_subset);

                std::copy_if(ghost_node_bearer_list.cbegin(),
                             ghost_node_bearer_list.cend(),
                             std::back_inserter(ret),
                             is_in_numbering_subset);
                return ret;
            }


            void AllocateInterpolatorMatrix(
                const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
                const Wrappers::Petsc::MatrixPattern& matrix_pattern,
                GlobalMatrix& interpolation_matrix)
            {

                decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
                decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();
                decltype(auto) source_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();
                decltype(auto) target_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

                decltype(auto) mpi = source_felt_space.GetMpi();

                const auto Nprocessor_wise_source_dof = source_felt_space.NprocessorWiseDof(source_numbering_subset);
                const auto Nprocessor_wise_target_dof = target_felt_space.NprocessorWiseDof(target_numbering_subset);

                const auto Nprogram_wise_source_dof = source_felt_space.NprogramWiseDof(source_numbering_subset);
                const auto Nprogram_wise_target_dof = target_felt_space.NprogramWiseDof(target_numbering_subset);


                if (mpi.Nprocessor<int>() == 1)
                {
                    assert(Nprocessor_wise_source_dof == Nprocessor_wise_target_dof);

                    interpolation_matrix.InitSequentialMatrix(Nprocessor_wise_target_dof,
                                                              Nprocessor_wise_source_dof,
                                                              matrix_pattern,
                                                              mpi,
                                                              __FILE__,
                                                              __LINE__);
                } else
                {
                    assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);

                    interpolation_matrix.InitParallelMatrix(Nprocessor_wise_target_dof,
                                                            Nprocessor_wise_source_dof,
                                                            Nprogram_wise_target_dof,
                                                            Nprogram_wise_source_dof,
                                                            matrix_pattern,
                                                            mpi,
                                                            __FILE__,
                                                            __LINE__);
                }
            }


            NodeBearer::vector_const_shared_ptr
            IdentifyTargetNodeBearerToProcess(const Dof::vector_shared_ptr& target_processor_wise_dof_list)
            {
                NodeBearer::vector_const_shared_ptr ret;
                ret.reserve(target_processor_wise_dof_list.size());

                for (const auto& target_dof_ptr : target_processor_wise_dof_list)
                {
                    assert(!(!target_dof_ptr));
                    const auto& target_dof = *target_dof_ptr;

                    const auto target_node_ptr = target_dof.GetNodeFromWeakPtr();
                    assert(!(!target_node_ptr));

                    const auto target_node_bearer_ptr = target_node_ptr->GetNodeBearerFromWeakPtr();
                    assert(!(!target_node_bearer_ptr));

                    ret.push_back(target_node_bearer_ptr);
                }

                assert(ret.size() == target_processor_wise_dof_list.size());

                // A same \a NodeBearer may have been reported many times (e.g. for a vectorial unknown once per
                // component).
                Utilities::EliminateDuplicate(ret,
                                              Utilities::PointerComparison::Less<NodeBearer::const_shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::const_shared_ptr>());

                return ret;
            }


            /*!
             * \brief Compute for each row the position of the non zero element.
             *
             * We know there is exactly one per row due to the nature of the operator.
             *
             * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
             * zero elements.
             */
            std::vector<PetscInt> ComputeNonZeroPositionPerRow(
                const NodeBearer::vector_const_shared_ptr& target_node_bearer_list,
                const std::size_t Nprocessor_wise_target_dof,
                const NumberingSubset& source_numbering_subset,
                const NumberingSubset& target_numbering_subset,
                const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                const node_bearer_per_coords_index_list_type& source_node_bearer_list_per_coords)
            {
                constexpr PetscInt dumb_value = -99;
                std::vector<PetscInt> ret(Nprocessor_wise_target_dof, dumb_value);

                for (const auto& target_node_bearer_ptr : target_node_bearer_list)
                {
                    assert(!(!target_node_bearer_ptr));
                    const auto& target_node_bearer = *target_node_bearer_ptr;

                    decltype(auto) target_interface = target_node_bearer.GetInterface();

                    decltype(auto) target_interface_coords_list =
                        target_interface.ComputeVertexCoordsIndexList<CoordsNS::index_enum::from_mesh_file>();

                    const auto source_coords_index = coords_matching.FindSourceIndex(target_interface_coords_list);

                    auto it = source_node_bearer_list_per_coords.find(source_coords_index);
                    assert(it != source_node_bearer_list_per_coords.cend());

                    const auto& source_node_bearer_ptr = it->second;
                    assert(!(!source_node_bearer_ptr));
                    const auto& source_node_bearer = *source_node_bearer_ptr;

                    decltype(auto) source_node_list = source_node_bearer.GetNodeList(source_numbering_subset);

                    decltype(auto) target_node_list = target_node_bearer.GetNodeList(target_numbering_subset);

                    const auto Nnode = source_node_list.size();
                    assert(Nnode == target_node_list.size());

                    for (auto node_index = 0ul; node_index < Nnode; ++node_index)
                    {

                        const auto& source_node_ptr = source_node_list[node_index];
                        const auto& target_node_ptr = target_node_list[node_index];

                        assert(!(!source_node_ptr));
                        assert(!(!target_node_ptr));

                        decltype(auto) source_dof_list = source_node_ptr->GetDofList();
                        decltype(auto) target_dof_list = target_node_ptr->GetDofList();

                        assert(source_dof_list.size() == target_dof_list.size());

                        const auto Ndof = source_dof_list.size();

                        for (auto dof_index = 0ul; dof_index < Ndof; ++dof_index)
                        {
                            assert(dof_index < target_dof_list.size());
                            const auto& target_dof_ptr = target_dof_list[dof_index];
                            assert(!(!target_dof_ptr));
                            const auto& target_dof = *target_dof_ptr;
                            const auto processor_wise_index =
                                target_dof.GetProcessorWiseOrGhostIndex(target_numbering_subset);
                            assert(processor_wise_index < ret.size());

                            assert(ret[processor_wise_index] == dumb_value
                                   && "A given row should be handled only once!");
                            assert(!(!source_dof_list[dof_index]));

                            ret[processor_wise_index] = static_cast<PetscInt>(
                                source_dof_list[dof_index]->GetProgramWiseIndex(source_numbering_subset));
                        }
                    }
                }

                assert(std::none_of(ret.cbegin(),
                                    ret.cend(),
                                    [](const PetscInt value)
                                    {
                                        return value == dumb_value;
                                    }));

                return ret;
            }


            std::vector<std::vector<PetscInt>>
            ComputeNonZeroPatternForMatrixPattern(const std::vector<PetscInt>& interpolation_pattern)
            {
                std::vector<std::vector<PetscInt>> ret;
                ret.reserve(interpolation_pattern.size());

                for (auto value : interpolation_pattern)
                    ret.push_back({ value });

                assert(ret.size() == interpolation_pattern.size());

                return ret;
            }


            std::vector<PetscInt> ComputeProgramWiseTargetDofIndexList(const FEltSpace& target_felt_space,
                                                                       const NumberingSubset& target_numbering_subset)
            {
                decltype(auto) dof_list = target_felt_space.GetProcessorWiseDofList(target_numbering_subset);

                std::vector<PetscInt> ret(dof_list.size());
                std::transform(dof_list.cbegin(),
                               dof_list.cend(),
                               ret.begin(),
                               [&target_numbering_subset](const auto& dof_ptr)
                               {
                                   assert(!(!dof_ptr));
                                   return dof_ptr->GetProgramWiseIndex(target_numbering_subset);
                               });

                return ret;
            }


        } // namespace


    } // namespace NonConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
