/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HXX_

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hpp"

#include <tuple>
#include <unordered_map>
#include <vector>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        inline void GlobalCoordsQuadPoints::Update() const
        {
            return parent::UpdateImpl();
        }


        inline void GlobalCoordsQuadPoints ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                                                         LocalParameterOperator& local_operator,
                                                                         const std::tuple<>&&) const
        {
            static_cast<void>(local_felt_space);
            static_cast<void>(local_operator);
        }


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HXX_
