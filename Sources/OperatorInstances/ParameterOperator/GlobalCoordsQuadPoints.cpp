/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <ostream>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include "Core/Enum.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

#include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        GlobalCoordsQuadPoints ::GlobalCoordsQuadPoints(
            const FEltSpace& felt_space,
            const Unknown& unknown,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology,
            // clang-format off
            ParameterAtQuadraturePoint
            <
                ParameterNS::Type::vector,
                ParameterNS::TimeDependencyNS::None
            >& global_coords_quad_pt)
        // clang-format on
        : parent(felt_space, unknown, quadrature_rule_per_topology, AllocateGradientFEltPhi::yes, global_coords_quad_pt)
        {
            decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

            felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr), DoComputeProcessorWiseLocal2Global::yes);
        }


        const std::string& GlobalCoordsQuadPoints::ClassName()
        {
            static std::string name("GlobalCoordsQuadPoints");
            return name;
        }


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
