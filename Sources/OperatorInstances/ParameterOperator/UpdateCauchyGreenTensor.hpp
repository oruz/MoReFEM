/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_

#include <iosfwd>
#include <memory>
#include <tuple>

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: export

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        /*!
         * \brief Implementation of global \a UpdateCauchyGreenTensor operator.
         */
        class UpdateCauchyGreenTensor final
        // clang-format off
        : public GlobalParameterOperator
        <
            UpdateCauchyGreenTensor,
            LocalParameterOperatorNS::UpdateCauchyGreenTensor,
            ParameterNS::Type::vector
        >
        // clang-format on
        {

          public:
            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const UpdateCauchyGreenTensor>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Convenient alias to pinpoint the GlobalParameterOperator parent.
            // clang-format off
            using parent =
                GlobalParameterOperator
                <
                    UpdateCauchyGreenTensor,
                    LocalParameterOperatorNS::UpdateCauchyGreenTensor,
                    ParameterNS::Type::vector
                >;
            // clang-format on

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] felt_space Finite element space upon which the operator is defined.
             * \param[in] unknown Unknown considered for this operator (might be vector or vectorial).
             * \param[in,out] cauchy_green_tensor Cauchy Green tensor which is to be updated by present operator.
             * Size of a local vector inside must be 3 for a 2D mesh and 6 for a 3D one.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit UpdateCauchyGreenTensor(
                const FEltSpace& felt_space,
                const Unknown& unknown,
                ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                    cauchy_green_tensor,
                const QuadratureRulePerTopology* const quadrature_rule_per_topology);

            //! Destructor.
            ~UpdateCauchyGreenTensor() = default;

            //! \copydoc doxygen_hide_copy_constructor
            UpdateCauchyGreenTensor(const UpdateCauchyGreenTensor& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UpdateCauchyGreenTensor(UpdateCauchyGreenTensor&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            UpdateCauchyGreenTensor& operator=(const UpdateCauchyGreenTensor& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UpdateCauchyGreenTensor& operator=(UpdateCauchyGreenTensor&& rhs) = delete;

            ///@}


            /*!
             * \brief Assemble into one or several vectors.
             *
             * \param[in] current_solid_displacement The current solid displacement (iteration {n}) used to compute the
             * tensor.
             */
            void Update(const GlobalVector& current_solid_displacement) const;


          public:
            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * Here there is one:
             * - The current solid displacement (iteration {n}) used to compute the tensor.
             *
             */
            void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                             LocalParameterOperator& local_operator,
                                             const std::tuple<const GlobalVector&>& additional_arguments) const;
        };


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
