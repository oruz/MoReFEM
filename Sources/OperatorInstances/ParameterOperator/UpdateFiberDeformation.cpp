/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <ostream>
#include <unordered_map>
#include <vector>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

#include "OperatorInstances/ParameterOperator/UpdateFiberDeformation.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        UpdateFiberDeformation ::UpdateFiberDeformation(
            const FEltSpace& felt_space,
            const Unknown& unknown,
            ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&
                fiber_deformation,
            const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&
                contraction_rheology_residual,
            const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                schur_complement,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::no,
                 fiber_deformation,
                 contraction_rheology_residual,
                 schur_complement)
        { }


        const std::string& UpdateFiberDeformation::ClassName()
        {
            static std::string name("UpdateFiberDeformation");
            return name;
        }


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
