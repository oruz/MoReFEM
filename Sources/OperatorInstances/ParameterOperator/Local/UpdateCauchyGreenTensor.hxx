/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Oct 2016 14:15:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"

#include <vector>

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        inline const std::vector<double>& UpdateCauchyGreenTensor::GetIncrementLocalDisplacement() const noexcept
        {
            return increment_local_displacement_;
        }


        inline std::vector<double>& UpdateCauchyGreenTensor::GetNonCstIncrementLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetIncrementLocalDisplacement());
        }


        inline LocalMatrix& UpdateCauchyGreenTensor::GetNonCstGradientDisplacement() noexcept
        {
            return gradient_displacement_;
        }


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HXX_
