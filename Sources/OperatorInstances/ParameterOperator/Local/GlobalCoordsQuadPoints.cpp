/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <array>
#include <cassert>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"

#include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        GlobalCoordsQuadPoints ::GlobalCoordsQuadPoints(
            const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
            elementary_data_type&& a_elementary_data,
            // clang-format off
                                 ParameterAtQuadraturePoint
                                 <
                                     ParameterNS::Type::vector,
                                     ParameterNS::TimeDependencyNS::None
                                 >& global_coords_quad_pt)
        // clang-format on
        : parent(a_unknown_storage, std::move(a_elementary_data), global_coords_quad_pt)
        { }


        GlobalCoordsQuadPoints::~GlobalCoordsQuadPoints() = default;


        const std::string& GlobalCoordsQuadPoints::ClassName()
        {
            static std::string name("GlobalCoordsQuadPoints");
            return name;
        }


        void GlobalCoordsQuadPoints::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();

            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            SpatialPoint global_quad_pt_coords_as_spatial_point;

            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                auto functor =
                    [&geom_elt, &quad_pt, &global_quad_pt_coords_as_spatial_point](LocalVector& global_quad_pt_coords)
                {
                    assert(global_quad_pt_coords.size()
                           == global_quad_pt_coords_as_spatial_point.GetCoordinateList().size());

                    Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_quad_pt_coords_as_spatial_point);

                    global_quad_pt_coords(0) = global_quad_pt_coords_as_spatial_point.x();
                    global_quad_pt_coords(1) = global_quad_pt_coords_as_spatial_point.y();
                    global_quad_pt_coords(2) = global_quad_pt_coords_as_spatial_point.z();
                };

                GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
            }
        }


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
