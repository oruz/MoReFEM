/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HPP_

#include <iosfwd>
#include <memory>

#include "Core/Parameter/TypeEnum.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        /*!
         * \brief Implementation of local \a GlobalCoordsQuadPoints operator.
         * This local operator is in charge of computing the global coordinates of each quadrature point.
         */
        class GlobalCoordsQuadPoints final : public Advanced::LocalParameterOperator<ParameterNS::Type::vector>
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = GlobalCoordsQuadPoints;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to parent.
            using parent = Advanced::LocalParameterOperator<ParameterNS::Type::vector>;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity.
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in,out] global_coords_quad_pt \a Parameter to update.
             *
             * \internal This constructor must not be called manually: it is involved only in
             * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
             * \endinternal
             */
            explicit GlobalCoordsQuadPoints(
                const ExtendedUnknown::const_shared_ptr& unknown_list,
                elementary_data_type&& elementary_data,
                // clang-format off
                                            ParameterAtQuadraturePoint
                                            <
                                                ParameterNS::Type::vector,
                                                ParameterNS::TimeDependencyNS::None
                                            >& global_coords_quad_pt);
            // clang-format on

            //! Destructor.
            ~GlobalCoordsQuadPoints();

            //! \copydoc doxygen_hide_copy_constructor
            GlobalCoordsQuadPoints(const GlobalCoordsQuadPoints& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            GlobalCoordsQuadPoints(GlobalCoordsQuadPoints&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            GlobalCoordsQuadPoints& operator=(const GlobalCoordsQuadPoints& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            GlobalCoordsQuadPoints& operator=(GlobalCoordsQuadPoints&& rhs) = delete;

            ///@}


            //! Compute the elementary vector.
            void ComputeEltArray();
        };


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HPP_
