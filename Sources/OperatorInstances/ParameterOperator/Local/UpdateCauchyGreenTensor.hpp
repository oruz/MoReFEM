/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_

#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        /*!
         * \brief Implementation of local \a UpdateCauchyGreenTensor operator.
         */
        class UpdateCauchyGreenTensor final : public Advanced::LocalParameterOperator<ParameterNS::Type::vector>
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = UpdateCauchyGreenTensor;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to parent.
            using parent = Advanced::LocalParameterOperator<ParameterNS::Type::vector>;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * two unknowns (the first one vectorial and the second one vector).
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in,out] cauchy_green_tensor \a Parameter to update.
             *
             * \internal This constructor must not be called manually: it is involved only in
             * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
             * \endinternal
             */
            explicit UpdateCauchyGreenTensor(
                const ExtendedUnknown::const_shared_ptr& unknown_list,
                elementary_data_type&& elementary_data,
                ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                    cauchy_green_tensor);

            //! Destructor.
            ~UpdateCauchyGreenTensor();

            //! \copydoc doxygen_hide_copy_constructor
            UpdateCauchyGreenTensor(const UpdateCauchyGreenTensor& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UpdateCauchyGreenTensor(UpdateCauchyGreenTensor&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            UpdateCauchyGreenTensor& operator=(const UpdateCauchyGreenTensor& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UpdateCauchyGreenTensor& operator=(UpdateCauchyGreenTensor&& rhs) = delete;

            ///@}


            //! Compute the elementary vector.
            void ComputeEltArray();

            //! Constant accessor to the former increment local displacement required by ComputeEltArray().
            const std::vector<double>& GetIncrementLocalDisplacement() const noexcept;

            //! Non constant accessor to the former increment local displacement required by ComputeEltArray().
            std::vector<double>& GetNonCstIncrementLocalDisplacement() noexcept;

          private:
            //! Local matrix used to store gradient of the displacement.
            LocalMatrix& GetNonCstGradientDisplacement() noexcept;

          private:
            /*!
             * \brief Increment Displacement obtained at previous time iteration expressed at the local level.
             *
             * \internal This is a work variable that should be used only within ComputeEltArray.
             * \endinternal
             */
            std::vector<double> increment_local_displacement_;

            //! Local matrix used to store gradient of the displacement.
            LocalMatrix gradient_displacement_;
        };


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_CAUCHY_GREEN_TENSOR_HPP_
