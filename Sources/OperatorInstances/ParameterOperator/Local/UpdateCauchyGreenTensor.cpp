/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Oct 2016 14:15:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <type_traits>
#include <unordered_map>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"

#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        namespace // anonymous
        {


            /*!
             * \brief Update for a tridimensional mesh.
             *
             */
            void Update3D(const LocalMatrix& gradient_component_disp, LocalVector& value_list);

            /*!
             * \brief Update for a bidimensional mesh.
             */
            void Update2D(const LocalMatrix& gradient_component_disp, LocalVector& value_list);

            /*!
             * \brief Update for a one dimensional mesh.
             */
            void Update1D(const LocalMatrix& gradient_component_disp, LocalVector& value_list);


        } // namespace


        UpdateCauchyGreenTensor ::UpdateCauchyGreenTensor(
            const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
            elementary_data_type&& a_elementary_data,
            ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                cauchy_green_tensor)
        : parent(a_unknown_storage, std::move(a_elementary_data), cauchy_green_tensor)
        {
            const auto& elementary_data = GetElementaryData();

            increment_local_displacement_.resize(elementary_data.NdofRow());

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            gradient_displacement_.resize({ mesh_dimension, mesh_dimension });
        }


        UpdateCauchyGreenTensor::~UpdateCauchyGreenTensor() = default;


        const std::string& UpdateCauchyGreenTensor::ClassName()
        {
            static std::string name("UpdateCauchyGreenTensor");
            return name;
        }


        void UpdateCauchyGreenTensor::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();

            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            const auto& increment_local_displacement = GetIncrementLocalDisplacement();

            auto& gradient_displacement = GetNonCstGradientDisplacement();

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            const auto& ref_felt = elementary_data.GetRefFElt(GetExtendedUnknown());

            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();

                assert(increment_local_displacement.size()
                       == static_cast<std::size_t>(quad_pt_unknown_data.Nnode() * infos_at_quad_pt.GetMeshDimension()));

                Advanced::OperatorNS::ComputeGradientDisplacementMatrix(
                    quad_pt_unknown_data, ref_felt, increment_local_displacement, gradient_displacement);

                auto functor = [&gradient_displacement, mesh_dimension](LocalVector& cauchy_green_tensor)
                {
                    switch (mesh_dimension)
                    {
                    case 1:
                        Update1D(gradient_displacement, cauchy_green_tensor);
                        break;
                    case 2:
                        Update2D(gradient_displacement, cauchy_green_tensor);
                        break;
                    case 3:
                        Update3D(gradient_displacement, cauchy_green_tensor);
                        break;
                    default:
                        assert(false);
                        exit(EXIT_FAILURE);
                    }
                };

                GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
            }
        }


        namespace // anonymous
        {


            void Update3D(const LocalMatrix& gradient_component_disp, LocalVector& value_list)
            {
                using NumericNS::Square;

                assert(value_list.size() == 6ul);

                // Component Cxx
                value_list(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                + Square(gradient_component_disp(1, 0)) + Square(gradient_component_disp(2, 0));

                // Component Cyy
                value_list(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                + Square(gradient_component_disp(1, 1)) + Square(gradient_component_disp(2, 1));

                // Component Czz
                value_list(2) = 1. + 2. * gradient_component_disp(2, 2) + Square(gradient_component_disp(0, 2))
                                + Square(gradient_component_disp(1, 2)) + Square(gradient_component_disp(2, 2));

                // Component Cxy
                value_list(3) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                + gradient_component_disp(1, 0) * gradient_component_disp(1, 1)
                                + gradient_component_disp(2, 0) * gradient_component_disp(2, 1);

                // Component Cyz
                value_list(4) = gradient_component_disp(1, 2) + gradient_component_disp(2, 1)
                                + gradient_component_disp(0, 2) * gradient_component_disp(0, 1)
                                + gradient_component_disp(1, 2) * gradient_component_disp(1, 1)
                                + gradient_component_disp(2, 2) * gradient_component_disp(2, 1);

                // Component Cxz
                value_list(5) = gradient_component_disp(2, 0) + gradient_component_disp(0, 2)
                                + gradient_component_disp(0, 2) * gradient_component_disp(0, 0)
                                + gradient_component_disp(1, 2) * gradient_component_disp(1, 0)
                                + gradient_component_disp(2, 2) * gradient_component_disp(2, 0);
            }


            void Update2D(const LocalMatrix& gradient_component_disp, LocalVector& value_list)
            {
                using NumericNS::Square;

                assert(value_list.size() == 3ul);

                // Component Cxx
                value_list(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                + Square(gradient_component_disp(1, 0));

                // Component Cyy
                value_list(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                + Square(gradient_component_disp(1, 1));

                // Component Cxy
                value_list(2) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                + gradient_component_disp(1, 0) * gradient_component_disp(1, 1);
            }


            void Update1D(const LocalMatrix& gradient_component_disp, LocalVector& value_list)
            {
                using NumericNS::Square;

                assert(value_list.size() == 1ul);

                // Component Cxx
                value_list(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0));
            }


        } // namespace


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
