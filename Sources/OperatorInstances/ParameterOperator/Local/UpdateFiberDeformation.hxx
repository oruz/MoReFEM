/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HXX_

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"

#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        inline const std::vector<double>& UpdateFiberDeformation::GetIncrementLocalDisplacement() const noexcept
        {
            return increment_local_displacement_;
        }


        inline std::vector<double>& UpdateFiberDeformation::GetNonCstIncrementLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetIncrementLocalDisplacement());
        }


        inline const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&
        UpdateFiberDeformation::GetContractionRheologyResidual() const noexcept
        {
            return contraction_rheology_residual_;
        }


        inline const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
        UpdateFiberDeformation::GetSchurComplement() const noexcept
        {
            return schur_complement_;
        }


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HXX_
