/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HXX_

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_GLOBAL_COORDS_QUAD_POINTS_HXX_
