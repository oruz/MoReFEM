/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HPP_

#include <iosfwd>
#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"


namespace MoReFEM
{


    namespace LocalParameterOperatorNS
    {


        /*!
         * \brief Local operator in charge of updating fiber deformation.
         */
        class UpdateFiberDeformation final : public Advanced::LocalParameterOperator<ParameterNS::Type::scalar>
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = UpdateFiberDeformation;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to parent.
            using parent = Advanced::LocalParameterOperator<ParameterNS::Type::scalar>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_ptr Unknown considered by the operators.
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in] fiber_deformation Parameter to update.
             * \param[in] contraction_rheology_residual Parameter used to update th deformation fiber.
             * \param[in] schur_complement Parameter used to update th deformation fiber.
             *
             * \internal This constructor must not be called manually: it is involved only in
             * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
             * \endinternal
             */
            explicit UpdateFiberDeformation(
                const ExtendedUnknown::const_shared_ptr& unknown_ptr,
                elementary_data_type&& elementary_data,
                // clang-format off
                ParameterAtQuadraturePoint
                <
                    ParameterNS::Type::scalar,
                    ParameterNS::TimeDependencyNS::None
                >& fiber_deformation,
                const ParameterAtQuadraturePoint
                <
                    ParameterNS::Type::scalar,
                    ParameterNS::TimeDependencyNS::None
                >& contraction_rheology_residual,
                const ParameterAtQuadraturePoint
                <
                    ParameterNS::Type::vector,
                    ParameterNS::TimeDependencyNS::None
                >& schur_complement);
            // clang-format on

            //! Destructor.
            ~UpdateFiberDeformation();

            //! \copydoc doxygen_hide_copy_constructor
            UpdateFiberDeformation(const UpdateFiberDeformation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UpdateFiberDeformation(UpdateFiberDeformation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            UpdateFiberDeformation& operator=(const UpdateFiberDeformation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UpdateFiberDeformation& operator=(UpdateFiberDeformation&& rhs) = delete;

            ///@}


            //! Compute the elementary vector.
            void ComputeEltArray();

            //! Constant accessor to the former increment local displacement required by ComputeEltArray().
            const std::vector<double>& GetIncrementLocalDisplacement() const noexcept;

            //! Non constant accessor to the former increment local displacement required by ComputeEltArray().
            std::vector<double>& GetNonCstIncrementLocalDisplacement() noexcept;

          private:
            //! Constant access to the contraction rheologu residual.
            const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&
            GetContractionRheologyResidual() const noexcept;

            //! Constant access to the schur complement.
            const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
            GetSchurComplement() const noexcept;

          private:
            /*!
             * \brief Increment Displacement obtained at previous time iteration expressed at the local level.
             *
             * \internal This is a work variable that should be used only within ComputeEltArray.
             * \endinternal
             */
            std::vector<double> increment_local_displacement_;

          private:
            /*!
             * \brief Current residual of the contraction rheology at the newton iteration, will be used to update
             * ec_n+1. G_BS = (tau_c_n+12# + mu*ec_p_n+12)(1+2ec_n+12) - Es*(e1D_n+12# - ec_n+12)(1+2e1D_n+12#) K22 =
             * dG_BS_dec_n+1 residual = K22^(-1)*G_BS
             */
            const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>&
                contraction_rheology_residual_;

            /*!
             * \brief Current schur complement at the newton iteration, will be used to update ec_n+1.
             * K21 = dG_dyn_dy_n+1, where G_dyn is the residual of the dynamics.
             * K22 = dG_BS_dec_n+1
             * schur = K22^(-1)*K21
             */
            const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                schur_complement_;
        };


    } // namespace LocalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_LOCAL_x_UPDATE_FIBER_DEFORMATION_HPP_
