/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:35:04 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                template<>
                void ComputeNonLinearPart<1>(const LocalVector& dW, LocalMatrix& out)
                {
                    out(0, 0) = dW(0); // Term SIGMA_xx
                }


                template<>
                void ComputeNonLinearPart<2>(const LocalVector& dW, LocalMatrix& out)
                {
                    for (auto i = 0ul; i < 2; ++i)
                    {
                        auto twice = 2 * i;

                        for (auto iComp = 0ul; iComp < 2; ++iComp)
                            out(twice + iComp, twice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy

                        out(twice, twice + 1) = out(twice + 1, twice) = dW(2); // Term SIGMA_xy
                    }
                }


                template<>
                void ComputeNonLinearPart<3>(const LocalVector& dW, LocalMatrix& out)
                {
                    for (auto i = 0ul; i < 3; ++i)
                    {
                        auto thrice = 3 * i;

                        for (auto iComp = 0ul; iComp < 3; ++iComp)
                            out(thrice + iComp, thrice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy, SIGMA_zz

                        out(thrice, thrice + 1) = out(thrice + 1, thrice) = dW(3);         // Term SIGMA_xy
                        out(thrice + 1, thrice + 2) = out(thrice + 2, thrice + 1) = dW(4); // Term SIGMA_yz
                        out(thrice, thrice + 2) = out(thrice + 2, thrice) = dW(5);         // Term SIGMA_xz
                    }
                }


                void ComputeLinearPart(const LocalMatrix& De,
                                       const LocalMatrix& transposed_De,
                                       const LocalMatrix& d2W,
                                       LocalMatrix& linear_part_intermediate_matrix,
                                       LocalMatrix& linear_part)
                {
                    xt::noalias(linear_part_intermediate_matrix) = xt::linalg::dot(transposed_De, d2W);
                    xt::noalias(linear_part) = xt::linalg::dot(linear_part_intermediate_matrix, De);
                }


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
