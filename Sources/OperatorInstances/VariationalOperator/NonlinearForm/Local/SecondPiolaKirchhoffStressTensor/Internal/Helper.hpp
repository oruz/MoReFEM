/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_HELPER_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Enum.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                /*!
                 * \brief Compute the non linear contribution to the tangent matrix.
                 *
                 * \tparam MeshDimensionT Dimension of the mesh.
                 *
                 * \param[in] dW First derivates of W.
                 * \param[in,out] out Tangent matrix, to which non linear contribution is added.
                 */
                template<std::size_t MeshDimensionT>
                void ComputeNonLinearPart(const LocalVector& dW, LocalMatrix& out);


                /*!
                 * \brief Compute the linear contribution to the tangent matrix.
                 *
                 * \copydoc doxygen_hide_de_and_transpose_arg
                 * \param[in] d2W Second derivates of W.
                 * \param[in,out] linear_part_intermediate_matrix Matrix used in the computation. It is already
                 * allocated; we frankly don't care the values in it in the input or in the output of the function. A
                 * better design would make that a private member of a dedicated struct, but I do not have the time to
                 * implement this very minor change. \param[in,out] linear_part Linear contribution to the tangent
                 * matrix.
                 */
                void ComputeLinearPart(const LocalMatrix& De,
                                       const LocalMatrix& transposed_De,
                                       const LocalMatrix& d2W,
                                       LocalMatrix& linear_part_intermediate_matrix,
                                       LocalMatrix& linear_part);


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_HELPER_HPP_
