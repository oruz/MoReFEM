/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_NONE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_NONE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ParameterInstances/Compound/InternalVariable/InputNone.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class TimeManager;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace InternalVariablePolicyNS
                {


                    /*!
                     * \brief Policy to use when there are no active stress involved in the
                     * \a SecondPiolaKirchhoffStressTensor operator.
                     */
                    class None
                    {

                      public:
                        //! \copydoc doxygen_hide_alias_self
                        using self = None;

                        //! Alias to unique pointer.
                        using unique_ptr = std::unique_ptr<self>;

                        //! Alias to vector of unique pointers.
                        using vector_unique_ptr = std::vector<unique_ptr>;

                        //! Alias to the type of the input.
                        using input_internal_variable_policy_type =
                            ::MoReFEM::Advanced::ParameterInstancesNS::InternalVariablePolicyNS::InputNone;

                      public:
                        /// \name Special members.
                        ///@{

                        //! Constructor: all arguments are dummy as this policy does zilch.
                        explicit None(const std::size_t,
                                      const std::size_t,
                                      const std::size_t,
                                      const TimeManager&,
                                      input_internal_variable_policy_type*);

                        //! Destructor.
                        ~None() = default;

                        //! \copydoc doxygen_hide_copy_constructor
                        None(const None& rhs) = delete;

                        //! \copydoc doxygen_hide_move_constructor
                        None(None&& rhs) = delete;

                        //! \copydoc doxygen_hide_copy_affectation
                        None& operator=(const None& rhs) = delete;

                        //! \copydoc doxygen_hide_move_affectation
                        None& operator=(None&& rhs) = delete;

                        ///@}
                    };


                } // namespace InternalVariablePolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_NONE_HPP_
