/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    template<std::size_t FiberIndexT>
    AnalyticalPrestress<FiberIndexT>::AnalyticalPrestress(
        const std::size_t mesh_dimension,
        const std::size_t Nnode_unknown,
        const std::size_t Nquad_point,
        const TimeManager& time_manager,
        input_internal_variable_policy_type* input_internal_variable_policy)
    : vector_parent(),
      fibers_(::MoReFEM::FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                   ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__)
                  .GetFiberList(FiberIndexT)),
      time_manager_(time_manager), input_internal_variable_policy_(*input_internal_variable_policy)
    {
        static_cast<void>(Nquad_point);

        std::size_t tauxtau_init(0ul);

        switch (mesh_dimension)
        {
        case 2u:
            tauxtau_init = 3u;
            break;
        case 3u:
            tauxtau_init = 6u;
            break;
        default:
            assert(false);
            break;
        }

        this->vector_parent::InitLocalVectorStorage({ { tauxtau_init } });

        local_electrical_activation_previous_time_.resize(Nnode_unknown);
        local_electrical_activation_at_time_.resize(Nnode_unknown);
    }


    template<std::size_t FiberIndexT>
    template<std::size_t DimensionT>
    void AnalyticalPrestress<FiberIndexT>::ComputeWDerivates(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalVector& cauchy_green_tensor_value,
        const LocalMatrix& transposed_De,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        static_cast<void>(d2W);
        static_cast<void>(ref_felt);
        static_cast<void>(cauchy_green_tensor_value);
        static_cast<void>(transposed_De);

        const auto run_case = GetTimeManager().GetStaticOrDynamic();

        if (run_case == StaticOrDynamic::dynamic_)
        {
            const auto& quad_pt = quad_pt_unknown_data.GetQuadraturePoint();
            const auto& phi_ref = quad_pt_unknown_data.GetRefFEltPhi();

            auto& fibers = GetFibers();
            auto& tauxtau =
                this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tauxtau)>();
            const double dt = GetTimeManager().GetTimeStep();

            const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

            const auto dimension = DimensionT;

            double norm = 0.;

            for (auto component = 0ul; component < dimension; ++component)
                norm += NumericNS::Square(tau_interpolate(component));

            const auto size = dW.shape(0);

            // Diagonal values sigma_xx, sigma_yy and sigma_zz.
            for (auto i = 0ul; i < dimension; ++i)
                tauxtau(i) = NumericNS::Square(tau_interpolate(i));

            // Extra diagonal values sigma_xy, sigma_yz and sigma_xz.
            for (auto i = dimension; i < size; ++i)
                tauxtau(i) = tau_interpolate(i % dimension) * tau_interpolate((i + 1) % dimension);

            if (!(NumericNS::IsZero(norm)))
            {
                tauxtau /= norm;

                double new_sigma_c = 0.;

                if (do_update_sigma_c_)
                {
                    const auto& u0 = GetLocalElectricalActivationPreviousTime();
                    const auto& u1 = GetLocalElectricalActivationAtTime();

                    double u0_at_quad = 0.;
                    double u1_at_quad = 0.;

                    const double sigma_0 = GetInputActiveStress().GetContractility().GetValue(quad_pt, geom_elt);

                    const int Nnode = static_cast<int>(geom_elt.Nvertex());

                    // This loop interpolates u at the current quad point.
                    for (auto node_index = 0; node_index < Nnode; ++node_index)
                    {
                        u0_at_quad += phi_ref(node_index) * u0[static_cast<std::size_t>(node_index)];
                        u1_at_quad += phi_ref(node_index) * u1[static_cast<std::size_t>(node_index)];
                    }

                    const double u0_u1_at_quad = u0_at_quad + u1_at_quad;

                    new_sigma_c = GetNonCstSigmaC().UpdateAndGetValue(
                        quad_pt,
                        geom_elt,
                        [u0_u1_at_quad, sigma_0, dt](double& sigma_c)
                        {
                            sigma_c = (0.5 * sigma_0 * NumericNS::AbsPlus(u0_u1_at_quad)
                                       + (1. / dt - 0.5 * std::fabs(u0_u1_at_quad)) * sigma_c)
                                      / (1. / dt + 0.5 * std::fabs(u0_u1_at_quad));
                        });
                } else
                    new_sigma_c = GetNonCstSigmaC().GetValue(quad_pt, geom_elt);

                xt::noalias(dW) += new_sigma_c * tauxtau;
            }
        }
    }


    template<std::size_t FiberIndexT>
    inline const ::MoReFEM::FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>&
    AnalyticalPrestress<FiberIndexT>::GetFibers() const noexcept
    {
        return fibers_;
    }


    template<std::size_t FiberIndexT>
    inline const TimeManager& AnalyticalPrestress<FiberIndexT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<std::size_t FiberIndexT>
    inline typename AnalyticalPrestress<FiberIndexT>::ScalarParameterAtQuadPt&
    AnalyticalPrestress<FiberIndexT>::GetNonCstSigmaC() noexcept
    {
        return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(GetSigmaC());
    }


    template<std::size_t FiberIndexT>
    inline const typename AnalyticalPrestress<FiberIndexT>::ScalarParameterAtQuadPt&
    AnalyticalPrestress<FiberIndexT>::GetSigmaC() const noexcept
    {
        assert(!(!sigma_c_));
        return *sigma_c_;
    }


    template<std::size_t FiberIndexT>
    inline void
    AnalyticalPrestress<FiberIndexT>::SetSigmaC(AnalyticalPrestress<FiberIndexT>::ScalarParameterAtQuadPt* sigma_c)
    {
        sigma_c_ = sigma_c;
    }


    template<std::size_t FiberIndexT>
    inline const std::vector<double>&
    AnalyticalPrestress<FiberIndexT>::GetLocalElectricalActivationPreviousTime() const noexcept
    {
        return local_electrical_activation_previous_time_;
    }


    template<std::size_t FiberIndexT>
    inline std::vector<double>&
    AnalyticalPrestress<FiberIndexT>::GetNonCstLocalElectricalActivationPreviousTime() noexcept
    {
        return const_cast<std::vector<double>&>(GetLocalElectricalActivationPreviousTime());
    }


    template<std::size_t FiberIndexT>
    inline const std::vector<double>&
    AnalyticalPrestress<FiberIndexT>::GetLocalElectricalActivationAtTime() const noexcept
    {
        return local_electrical_activation_at_time_;
    }


    template<std::size_t FiberIndexT>
    inline std::vector<double>& AnalyticalPrestress<FiberIndexT>::GetNonCstLocalElectricalActivationAtTime() noexcept
    {
        return const_cast<std::vector<double>&>(GetLocalElectricalActivationAtTime());
    }


    template<std::size_t FiberIndexT>
    void AnalyticalPrestress<FiberIndexT>::SetDoUpdateSigmaC(const bool update)
    {
        do_update_sigma_c_ = update;
    }


    template<std::size_t FiberIndexT>
    inline const typename AnalyticalPrestress<FiberIndexT>::input_internal_variable_policy_type&
    AnalyticalPrestress<FiberIndexT>::GetInputActiveStress() const noexcept
    {
        return input_internal_variable_policy_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
