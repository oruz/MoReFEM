/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Jan 2015 10:49:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp" // IWYU pragma: export
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp" // IWYU pragma: export


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                template<std::size_t FiberIndexT>
                class AnalyticalPrestress;


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace InternalVariablePolicyNS
                {


                    /*!
                     * \brief Policy to use when \a AnalyticalPrestress is involved in \a
                     * SecondPiolaKirchhoffStressTensor.
                     *
                     * \tparam FiberIndexT Index of the relevant fiber in the \a InputDataFile.
                     *
                     * \todo #9 (Gautier) Explain difference with InputAnalyticalPrestress.
                     */
                    template<std::size_t FiberIndexT>
                    class AnalyticalPrestress : public Crtp::LocalVectorStorage<AnalyticalPrestress<FiberIndexT>, 1ul>
                    {

                      public:
                        //! \copydoc doxygen_hide_alias_self
                        using self = AnalyticalPrestress<FiberIndexT>;

                        //! Alias to unique pointer.
                        using unique_ptr = std::unique_ptr<self>;

                        //! Alias to vector of unique pointers.
                        using vector_unique_ptr = std::vector<unique_ptr>;

                        //! Type of the elementary vector.
                        using vector_type = LocalVector;

                        //! Alias to the parent that provides LocalVectorStorage.
                        using vector_parent = Crtp::LocalVectorStorage<AnalyticalPrestress<FiberIndexT>, 1ul>;

                        //! Friendship to the GlobalVariationalOperator.
                        friend ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
                            InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT>;

                        //! Alias to a scalar parameter at quadrature point.
                        using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar>;

                        //! Alias to the type of the input of the policy.
                        using input_internal_variable_policy_type = ParameterInstancesNS::
                            SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS ::InputAnalyticalPrestress;

                        //! Friendship to the class that calls SetSigmaC().
                        template<class LocalOperatorTupleT, std::size_t I, std::size_t TupleSizeT>
                        friend struct Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                            SetSigmaCHelper;

                      public:
                        /// \name Special members.
                        ///@{

                        /*!
                         * \brief Constructor.
                         *
                         * \param[in] mesh_dimension Dimension of the mesh.
                         * \param[in] Nnode_unknown Number of nodes.
                         * \param[in] Nquad_point Nunmber of quadrature point.
                         * \param[in] time_manager Object that keeps track of the time within the Model.
                         * \param[in] input_internal_variable_policy \a InputAnalyticalPrestress object.
                         */
                        explicit AnalyticalPrestress(
                            const std::size_t mesh_dimension,
                            const std::size_t Nnode_unknown,
                            const std::size_t Nquad_point,
                            const TimeManager& time_manager,
                            input_internal_variable_policy_type* input_internal_variable_policy);

                        //! Destructor.
                        ~AnalyticalPrestress() = default;

                        //! \copydoc doxygen_hide_copy_constructor
                        AnalyticalPrestress(const AnalyticalPrestress& rhs) = delete;

                        //! \copydoc doxygen_hide_move_constructor
                        AnalyticalPrestress(AnalyticalPrestress&& rhs) = delete;

                        //! \copydoc doxygen_hide_copy_affectation
                        AnalyticalPrestress& operator=(const AnalyticalPrestress& rhs) = delete;

                        //! \copydoc doxygen_hide_move_affectation
                        AnalyticalPrestress& operator=(AnalyticalPrestress&& rhs) = delete;

                        ///@}

                      public:
                        /*!
                         * \class doxygen_hide_second_piola_compute_W_derivates_internal_variable
                         *
                         * \brief Compute the active stress and its derivatives.
                         *
                         * \copydoc doxygen_hide_dW_d2W_derivates_arg
                         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                         * \param[in] ref_felt Reference finite element considered.
                         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                         * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
                         * \a QuadraturePoint.
                         * \param[in] transposed_De Transposed De matrix.
                         */

                        //! \copydoc doxygen_hide_second_piola_compute_W_derivates_internal_variable
                        template<std::size_t DimensionT>
                        void ComputeWDerivates(
                            const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList&
                                quad_pt_unknown_data,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalVector& cauchy_green_tensor_value,
                            const LocalMatrix& transposed_De,
                            LocalVector& dW,
                            LocalMatrix& d2W);

                      public:
                        //! Constant accessor to the local velocity required by ComputeEltArray().
                        const std::vector<double>& GetLocalElectricalActivationPreviousTime() const noexcept;

                        //! Non constant accessor to the local velocity required by ComputeEltArray().
                        std::vector<double>& GetNonCstLocalElectricalActivationPreviousTime() noexcept;

                        //! Constant accessor to the local velocity required by ComputeEltArray().
                        const std::vector<double>& GetLocalElectricalActivationAtTime() const noexcept;

                        //! Non constant accessor to the local velocity required by ComputeEltArray().
                        std::vector<double>& GetNonCstLocalElectricalActivationAtTime() noexcept;

                        //! Indicates if sigma_c must be advanced in time or not.
                        //! \param[in] do_update Value to set.
                        void SetDoUpdateSigmaC(const bool do_update);

                        //! Constant accessor to sigma_c.
                        const ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetSigmaC() const noexcept;

                      private:
                        //! Constant accessor to \a TimeManager.
                        const TimeManager& GetTimeManager() const noexcept;

                        //! Constant accessor to the fibers.
                        const ::MoReFEM::FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                   ParameterNS::Type::vector>&
                        GetFibers() const noexcept;

                        //! Non constant accessor to sigma_c.
                        ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetNonCstSigmaC() noexcept;

                        //! Give sigma_c to the local operator.
                        //! \param[in] sigma_c New value of sigma_c.
                        void SetSigmaC(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* sigma_c);

                      private:
                        //! Contant accessor to the input of the policy.
                        const input_internal_variable_policy_type& GetInputActiveStress() const noexcept;

                      private:
                        //! Fibers parameter.
                        const ::MoReFEM::FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                   ParameterNS::Type::vector>& fibers_;

                        //! Active stress in the fiber direction.
                        ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* sigma_c_;

                      private:
                        //! Time manager.
                        const TimeManager& time_manager_;

                      private:
                        /*!
                         * \brief U0 obtained at previous time iteration expressed at the local level.
                         *
                         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                         * ComputeEltArray.
                         * \endinternal
                         */
                        std::vector<double> local_electrical_activation_previous_time_;

                        /*!
                         * \brief U0 obtained at previous time iteration expressed at the local level.
                         *
                         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                         * ComputeEltArray.
                         * \endinternal
                         */
                        std::vector<double> local_electrical_activation_at_time_;

                      private:
                        //! Input of the policy.
                        input_internal_variable_policy_type& input_internal_variable_policy_;

                      private:
                        //! Boolean to know if it is the first newton iteration to advance sigma_c in time.
                        bool do_update_sigma_c_;

                      private:
                        /// \name Useful indexes to fetch the work matrices and vectors.
                        ///@{

                        //! Convenient enum to alias vectors.
                        enum class LocalVectorIndex : std::size_t
                        {
                            tauxtau = 0
                        };

                        ///@}
                    };


                } // namespace InternalVariablePolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
