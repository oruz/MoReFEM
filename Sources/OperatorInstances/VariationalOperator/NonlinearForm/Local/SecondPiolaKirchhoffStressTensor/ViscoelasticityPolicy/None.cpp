/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Solid; }
namespace MoReFEM { class TimeManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace ViscoelasticityPolicyNS
                {


                    None::None(const std::size_t mesh_dimension,
                               const std::size_t Ndof,
                               const std::size_t Ncomponent,
                               const Solid& solid,
                               const TimeManager& time_manager)
                    {
                        static_cast<void>(mesh_dimension);
                        static_cast<void>(time_manager);
                        static_cast<void>(Ndof);
                        static_cast<void>(solid);
                        static_cast<void>(Ncomponent);
                    }


                } // namespace ViscoelasticityPolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
