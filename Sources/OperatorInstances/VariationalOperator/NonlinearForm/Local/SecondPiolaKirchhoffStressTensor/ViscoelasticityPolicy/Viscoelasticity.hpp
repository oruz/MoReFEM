/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 13 Jan 2016 11:18:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/Parameter.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/ElementaryData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace ViscoelasticityPolicyNS
                {


                    //! Switch between the derivatives to compute in the operator.
                    enum class DerivatesWithRespectTo
                    {
                        displacement,
                        velocity,
                        displacement_and_velocity
                    };


                    /*!
                     * \brief Policy to use when visco-elasticity is involved in \a SecondPiolaKirchhoffStressTensor.
                     *
                     */
                    template<DerivatesWithRespectTo DerivatesWithRespectToT>
                    class Viscoelasticity
                    : public Crtp::LocalMatrixStorage<Viscoelasticity<DerivatesWithRespectToT>, 3ul>,
                      public Crtp::LocalVectorStorage<Viscoelasticity<DerivatesWithRespectToT>, 1ul>
                    {

                      public:
                        //! \copydoc doxygen_hide_alias_self
                        using self = Viscoelasticity;

                        //! Alias to unique pointer.
                        using unique_ptr = std::unique_ptr<self>;

                        //! Alias to vector of unique pointers.
                        using vector_unique_ptr = std::vector<unique_ptr>;


                        //! Type of the elementary matrix.
                        using matrix_type = LocalMatrix;

                        //! Type of the elementary vector.
                        using vector_type = LocalVector;

                        //! Alias to the parent that provides LocalMatrixStorage.
                        using matrix_parent = Crtp::LocalMatrixStorage<Viscoelasticity<DerivatesWithRespectToT>, 3ul>;

                        //! Alias to the parent that provides LocalVectorStorage.
                        using vector_parent = Crtp::LocalVectorStorage<Viscoelasticity<DerivatesWithRespectToT>, 1ul>;

                        //! Alias to class GreenLagrangeOrEta.
                        using GreenLagrangeOrEta = ::MoReFEM::Advanced::LocalVariationalOperatorNS::GreenLagrangeOrEta;

                        //! Alias to the type of the parameter.
                        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

                      public:
                        /// \name Special members.
                        ///@{

                        /*!
                         * \brief Constructor.
                         *
                         * \param[in] mesh_dimension Dimension of the mesh.
                         * \param[in] Ndof Number of \a Dof considered.
                         * \param[in] Ncomponent Number of components considered.
                         * \copydoc doxygen_hide_solid_arg
                         * \copydoc doxygen_hide_time_manager_arg
                         */
                        explicit Viscoelasticity(const std::size_t mesh_dimension,
                                                 const std::size_t Ndof,
                                                 const std::size_t Ncomponent,
                                                 const Solid& solid,
                                                 const TimeManager& time_manager);

                        //! Destructor.
                        ~Viscoelasticity() = default;

                        //! \copydoc doxygen_hide_copy_constructor
                        Viscoelasticity(const Viscoelasticity& rhs) = delete;

                        //! \copydoc doxygen_hide_move_constructor
                        Viscoelasticity(Viscoelasticity&& rhs) = delete;

                        //! \copydoc doxygen_hide_copy_affectation
                        Viscoelasticity& operator=(const Viscoelasticity& rhs) = delete;

                        //! \copydoc doxygen_hide_move_affectation
                        Viscoelasticity& operator=(Viscoelasticity&& rhs) = delete;

                        ///@}

                      public:
                        //! Make the type be available without its full namespace dependency.
                        using InformationsAtQuadraturePoint =
                            ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint;


                        /*!
                         * \class doxygen_hide_de_and_transpose_arg
                         *
                         * \param[in] De De matrix.
                         * \param[in] transposed_De Transposed De matrix. It is given along De to avoid recomputing it.
                         */


                        /*!
                         * \class doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
                         *
                         * \brief Compute the contribution of visco-elasticity to the derivates of W.
                         *
                         * \tparam DimensionT Dimension of the mesh.
                         *
                         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                         * \param[in] geom_elt \a GeometricElt for which the computation is performed.
                         * \param[in] ref_felt Reference finite element considered.
                         * \copydoc doxygen_hide_de_and_transpose_arg
                         * \copydoc doxygen_hide_dW_d2W_derivates_arg
                         */


                        //! \copydoc doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
                        template<std::size_t DimensionT>
                        void ComputeWDerivates(
                            const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&
                                quad_pt_unknown_data,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalMatrix& De,
                            const LocalMatrix& transposed_De,
                            LocalVector& dW,
                            LocalMatrix& d2W);

                      public:
                        //! Get the \a TimeManager.
                        const TimeManager& GetTimeManager() const noexcept;

                      private:
                        //! Reference to the \a TimeManager of the model.
                        const TimeManager& time_manager_;

                      public:
                        //! Constant accessor to the local velocity required by ComputeEltArray().
                        const std::vector<double>& GetLocalVelocity() const noexcept;

                        //! Non constant accessor to the local velocity required by ComputeEltArray().
                        std::vector<double>& GetNonCstLocalVelocity() noexcept;

                      public:
                        //! Get the tangent viscosity matrix.
                        const LocalMatrix& GetMatrixTangentVisco() const noexcept;

                      private:
                        /*!
                         * \brief Enables to put the gradient matrix in a vector ordered as
                         * (vx,x vx,y vx,z vy,x vy,y vy,z vz,x vz,y vz,z)
                         *
                         * \param[in] gradient_matrix Matrix which is to be put into a vector.
                         * \param[out] vector Vector into which the values are written. This vector is assumed to be
                         * already properly allocated.
                         */
                        void MatrixToVector(const LocalMatrix& gradient_matrix, LocalVector& vector);

                      private:
                        /*!
                         * \brief Velocity obtained at previous time iteration expressed at the local level.
                         *
                         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                         * ComputeEltArray.
                         * \endinternal
                         */
                        std::vector<double> local_velocity_;

                      private:
                        //! Helper class to manage computation of derivative of Green-Lagrange.
                        DerivativeGreenLagrange<GreenLagrangeOrEta::eta>& GetNonCstDerivativeEta() noexcept;

                        //! Helper class to manage computation of derivative of Green-Lagrange.
                        DerivativeGreenLagrange<GreenLagrangeOrEta::eta>::unique_ptr deriv_eta_ = nullptr;

                      private:
                        //! Viscosity.
                        const scalar_parameter& GetViscosity() const noexcept;

                      private:
                        //! Viscosity.
                        const scalar_parameter& viscosity_;


                      private:
                        /// \name Useful indexes to fetch the work matrices and vectors.
                        ///@{

                        //! Indexes for local matrices.
                        enum class LocalMatrixIndex : std::size_t
                        {
                            d2W_visco = 0,
                            tangent_matrix_visco,
                            gradient_velocity
                        };


                        //! Indexes for local vectors.
                        enum class LocalVectorIndex : std::size_t
                        {
                            velocity_gradient = 0
                        };

                        ///@}
                    };


                } // namespace ViscoelasticityPolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_
