/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_NONE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_NONE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Solid; }
namespace MoReFEM { class TimeManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace ViscoelasticityPolicyNS
                {


                    /*!
                     * \brief Policy to use when there are no visco-elasticity involved in the
                     * \a SecondPiolaKirchhoffStressTensor operator.
                     */
                    class None
                    {

                      public:
                        //! \copydoc doxygen_hide_alias_self
                        using self = None;

                        //! Alias to unique pointer.
                        using unique_ptr = std::unique_ptr<self>;

                        //! Alias to vector of unique pointers.
                        using vector_unique_ptr = std::vector<unique_ptr>;

                      public:
                        /// \name Special members.
                        ///@{

                        //! Constructor.
                        explicit None(const std::size_t,
                                      const std::size_t,
                                      const std::size_t,
                                      const Solid&,
                                      const TimeManager&);

                        //! Destructor.
                        ~None() = default;

                        //! \copydoc doxygen_hide_copy_constructor
                        None(const None& rhs) = delete;

                        //! \copydoc doxygen_hide_move_constructor
                        None(None&& rhs) = delete;

                        //! \copydoc doxygen_hide_copy_affectation
                        None& operator=(const None& rhs) = delete;

                        //! \copydoc doxygen_hide_move_affectation
                        None& operator=(None&& rhs) = delete;

                        ///@}

                      public:
                    };


                } // namespace ViscoelasticityPolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_NONE_HPP_
