/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp" // IWYU pragma: export
#include "Utilities/MatrixOrVector.hpp"                                 // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/ExtendedUnknown.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"           // IWYU pragma: export
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS { class ForUnknownList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Local operator for \a NonlinearMembrane.
             */
            class NonlinearMembrane final : public NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>,
                                            public Crtp::LocalMatrixStorage<NonlinearMembrane, 19ul>,
                                            public Crtp::LocalVectorStorage<NonlinearMembrane, 3ul>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = NonlinearMembrane;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Convenient alias.
                using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent = Crtp::LocalMatrixStorage<self, 19ul>;

                //! Alias to the parent that provides LocalVectorStorage.
                using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
                 * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
                 * hold exactly two unknowns (the first one vectorial and the second one scalar).
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] youngs_modulus Young's modulus of the solid.
                 * \param[in] poisson_ratio Poisson ratio of the solid.
                 * \param[in] thickness Thickness of the solid.
                 * \param[in] pretension Pretension (related computation are skipped if the underlying value is 0.).
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit NonlinearMembrane(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                           elementary_data_type&& elementary_data,
                                           const scalar_parameter& youngs_modulus,
                                           const scalar_parameter& poisson_ratio,
                                           const scalar_parameter& thickness,
                                           const scalar_parameter& pretension);

                //! Destructor.
                virtual ~NonlinearMembrane() override;

                //! \copydoc doxygen_hide_copy_constructor
                NonlinearMembrane(const NonlinearMembrane& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                NonlinearMembrane(NonlinearMembrane&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                NonlinearMembrane& operator=(const NonlinearMembrane& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                NonlinearMembrane& operator=(NonlinearMembrane&& rhs) = delete;

                ///@}

                //! Compute the elementary vector.
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }

                /*!
                 * \brief Constant accessor to the former local displacement required by ComputeEltArray().
                 *
                 * \return Reference to the former local displacement.
                 */
                const std::vector<double>& GetFormerLocalDisplacement() const noexcept;

                /*!
                 * \brief Non constant accessor to the former local displacement required by ComputeEltArray().
                 *
                 * \return Non constant reference to the former local displacement.
                 */
                std::vector<double>& GetNonCstFormerLocalDisplacement() noexcept;

              private:
                /*!
                 * \brief Compute contravariant basis.
                 *
                 * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                 * \param[out] determinant Determinant of the covariant metric tensor.
                 *
                 */
                void ComputeContravariantBasis(
                    const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&
                        quad_pt_unknown_data,
                    double& determinant);

                /*!
                 * \brief Compute displacement gradient.
                 *
                 * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                 *
                 */
                void ComputeDisplacementGradient(
                    const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&
                        quad_pt_unknown_data);

                /*!
                 * \brief Compute Green-Lagrange.
                 *
                 * \param[in] pretension Value of the pretension.
                 */
                void ComputeGreenLagrange(const double pretension);

                /*!
                 * \brief Compute locally the tangent tensor.
                 *
                 * \param[in] E Value at the quadrature point of Young's modulus.
                 * \param[in] nu Value at the quadrature point of Poisson ratio.
                 */
                void ComputeTangentTensor(double E, double nu);

                //! Compute Second Piola Kirchhoff stress tensor.
                void ComputeSecondPiolaKirchhoff();

                //! Compute matrix De.
                void ComputeDe();

                //! Compute the tangent matrix and/or the RHS (depends of the linear elgebra into which operator is
                //! assembled).
                void ComputeTangentMatrixAndRightHandSide();

              private:
                /*!
                 * \brief Accessor to Young's modulus parameter.
                 *
                 * \return Young's modulus.
                 */
                const scalar_parameter& GetYoungsModulus() const noexcept;

                /*!
                 * \brief Accessor to Poisson ratio parameter.
                 *
                 * \return Poisson ratio.
                 */
                const scalar_parameter& GetPoissonRatio() const noexcept;

                /*!
                 * \brief Accessor to solid thickness parameter.
                 *
                 * \return Thickness of the solid.
                 */
                const scalar_parameter& GetThickness() const noexcept;

                /*!
                 * \brief Accessor to pretension parameter.
                 *
                 * \return Pretension.
                 */
                const scalar_parameter& GetPretension() const noexcept;

              private:
                //! \name Material parameters.
                ///@{

                //! Young's modulus.
                const scalar_parameter& youngs_modulus_;

                //! Poisson ratio.
                const scalar_parameter& poisson_ratio_;

                //! Thickness.
                const scalar_parameter& thickness_;

                //! Pretension.
                const scalar_parameter& pretension_;

                ///@}

                //! Local displacement computed at the former time iteration.
                std::vector<double> former_local_displacement_;

              private:
                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Indexes for local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    tangent_tensor = 0,
                    De_membrane,
                    transposed_De_membrane,
                    transposed_De_mult_tangent_tensor,
                    tangent_matrix,
                    displacement_gradient,
                    covariant_basis,
                    contravariant_basis,
                    transposed_covariant_basis,
                    covariant_metric_tensor,
                    contravariant_metric_tensor,
                    gradient_based_block,
                    transposed_dphi,
                    dphi_test_mult_gradient_based_block,
                    block_contribution,
                    invert_generalized_covariant_basis,
                    test_pk_in_ref_basis,
                    test_gl_in_ref_basis,
                    //                    block_matrix1 = 0,
                    //                    block_matrix2,
                    //                    block_matrix3,
                    //                    block_matrix4,
                    //                    transposed_dphi_test,
                    //                    transposed_dpsi_test,
                    //                    dphi_sigma,
                    //                    dpsi_sigma,
                    //                    dphi_contravariant_metric_tensor,
                    //                    dpsi_contravariant_metric_tensor,
                    //                    tau_sigma,
                    //                    tau_ortho_sigma,
                    //
                };


                //! Indexes for local vectors.
                enum class LocalVectorIndex : std::size_t
                {
                    green_lagrange = 0,
                    second_PK,
                    rhs_part,
                    //                    tau_interpolate_ortho = 0,
                    //                    tau_covariant_basis,
                    //                    tau_ortho_covariant_basis
                };

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HPP_
