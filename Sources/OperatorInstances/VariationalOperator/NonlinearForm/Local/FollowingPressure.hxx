/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/FollowingPressure.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<template<ParameterNS::Type> class TimeDependencyT>
    FollowingPressure<TimeDependencyT>::FollowingPressure(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const parameter_type& pressure)
    : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), vector_parent(), pressure_(pressure)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        assert(mesh_dimension == 3 && "Operator restricted to 2D elements in a 3D mesh.");

        assert(unknown_ref_felt.GetFEltSpaceDimension() == 2 && "Operator restricted to 2D elements in a 3D mesh.");

        former_local_displacement_.resize(elementary_data.NdofRow());

        this->matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown, Nnode_for_unknown }, // phi_test_dphi_dr
            { Nnode_for_test_unknown, Nnode_for_unknown }  // phi_test_dphi_ds
        } });

        this->vector_parent::InitLocalVectorStorage({ {
            mesh_dimension, // dxdr
            mesh_dimension, // dxds
            mesh_dimension  // dxdr_cross_dxds
        } });
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    FollowingPressure<TimeDependencyT>::~FollowingPressure() = default;


    template<template<ParameterNS::Type> class TimeDependencyT>
    const std::string& FollowingPressure<TimeDependencyT>::ClassName()
    {
        static std::string name("FollowingPressure");
        return name;
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    void FollowingPressure<TimeDependencyT>::ComputeEltArray()
    {
        const auto& local_displacement = GetFormerLocalDisplacement();

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        matrix_result.fill(0.); // Is it necessary ?
        vector_result.fill(0.); // Is it necessary ?

        auto& phi_test_dphi_dr =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_test_dphi_dr)>();
        auto& phi_test_dphi_ds =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_test_dphi_ds)>();

        auto& dxdr_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr)>();
        auto& dxds_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxds)>();
        auto& dxdr_cross_dxds_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr_cross_dxds)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

        const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        // Number of component of the unknown.
        const auto Ncomponent = ComponentNS::index_type{ elementary_data.GetMeshDimension() };

        const std::size_t Nnode_for_unknown = ref_felt.Nnode();
        const std::size_t Nnode_for_test_unknown = test_ref_felt.Nnode();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        assert(geom_elt.GetDimension() == 2);

        const auto& pressure = GetPressure();

        auto& coords_in_geom_elt = GetNonCstWorkCoords();


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& phi_test = test_quad_pt_unknown_data.GetRefFEltPhi();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * pressure.GetValue(quad_pt, geom_elt); // Here no jacobian.

            dxdr_at_quad_point.fill(0.);
            dxds_at_quad_point.fill(0.);

            // Here the two indices hardcoded 0 and 1 correspond to the two variables of derivation in a 2D element
            // but the vectors are in 3D, hence Ncomponent = 3.
            for (std::size_t node_index = 0; node_index < Nnode_for_unknown; ++node_index)
            {
                auto dof_index = node_index;

                Advanced::GeomEltNS::Local2Global(
                    geom_elt, ref_felt.GetBasicRefFElt().GetLocalNode(node_index).GetLocalCoords(), coords_in_geom_elt);

                for (ComponentNS::index_type component{ 0ul }; component < Ncomponent;
                     ++component, dof_index += Nnode_for_unknown)
                {
                    const auto local_factor = coords_in_geom_elt[component.Get()] + local_displacement[dof_index];

                    dxdr_at_quad_point(component.Get()) += dphi(node_index, 0) * local_factor;
                    dxds_at_quad_point(component.Get()) += dphi(node_index, 1) * local_factor;
                }
            }

            // Same comment as previously.
            for (auto i = 0ul; i < Nnode_for_test_unknown; ++i)
            {
                for (auto j = 0ul; j < Nnode_for_unknown; ++j)
                {
                    phi_test_dphi_dr(i, j) = phi_test(i) * dphi(j, 0);
                    phi_test_dphi_ds(i, j) = phi_test(i) * dphi(j, 1);
                }
            }

            // The matrix is filled by block. Its size is (Nnode*Ncomponent)x(Nnode*Ncomponent).
            // Each block depends on a coordinate of dxdr and dxds.
            for (auto i = 0ul; i < Nnode_for_test_unknown; ++i)
            {
                for (auto j = 0ul; j < Nnode_for_unknown; ++j)
                {
                    // Block 12
                    matrix_result(i, j + Nnode_for_unknown) += factor
                                                               * (-dxdr_at_quad_point(2) * phi_test_dphi_ds(i, j)
                                                                  - (-dxds_at_quad_point(2)) * phi_test_dphi_dr(i, j));
                    // Block 13
                    matrix_result(i, j + 2 * Nnode_for_unknown) += factor
                                                                   * (dxdr_at_quad_point(1) * phi_test_dphi_ds(i, j)
                                                                      - dxds_at_quad_point(1) * phi_test_dphi_dr(i, j));
                    // Block 21
                    matrix_result(i + Nnode_for_test_unknown, j) +=
                        factor
                        * (dxdr_at_quad_point(2) * phi_test_dphi_ds(i, j)
                           - dxds_at_quad_point(2) * phi_test_dphi_dr(i, j));
                    // Block 23
                    matrix_result(i + Nnode_for_test_unknown, j + 2 * Nnode_for_unknown) +=
                        factor
                        * (-dxdr_at_quad_point(0) * phi_test_dphi_ds(i, j)
                           - (-dxds_at_quad_point(0)) * phi_test_dphi_dr(i, j));
                    // Block 31
                    matrix_result(i + 2 * Nnode_for_test_unknown, j) +=
                        factor
                        * (-dxdr_at_quad_point(1) * phi_test_dphi_ds(i, j)
                           - (-dxds_at_quad_point(1)) * phi_test_dphi_dr(i, j));
                    // Block 32
                    matrix_result(i + 2 * Nnode_for_test_unknown, j + Nnode_for_unknown) +=
                        factor
                        * (dxdr_at_quad_point(0) * phi_test_dphi_ds(i, j)
                           - dxds_at_quad_point(0) * phi_test_dphi_dr(i, j));
                }
            }

            // Cross product computation.
            dxdr_cross_dxds_at_quad_point(0) =
                dxdr_at_quad_point(1) * dxds_at_quad_point(2) - dxdr_at_quad_point(2) * dxds_at_quad_point(1);
            dxdr_cross_dxds_at_quad_point(1) =
                dxdr_at_quad_point(2) * dxds_at_quad_point(0) - dxdr_at_quad_point(0) * dxds_at_quad_point(2);
            dxdr_cross_dxds_at_quad_point(2) =
                dxdr_at_quad_point(0) * dxds_at_quad_point(1) - dxdr_at_quad_point(1) * dxds_at_quad_point(0);

            // Integration of the residual.
            for (auto node_index = 0ul; node_index < Nnode_for_test_unknown; ++node_index)
            {
                auto dof_index = node_index;

                const auto value = factor * phi_test(node_index);

                for (ComponentNS::index_type component{ 0ul }; component < Ncomponent;
                     ++component, dof_index += Nnode_for_test_unknown)
                    vector_result(dof_index) += value * dxdr_cross_dxds_at_quad_point(component.Get());
            }
        }
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    inline const typename FollowingPressure<TimeDependencyT>::parameter_type&
    FollowingPressure<TimeDependencyT>::GetPressure() const noexcept
    {
        return pressure_;
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    inline const std::vector<double>& FollowingPressure<TimeDependencyT>::GetFormerLocalDisplacement() const noexcept
    {
        return former_local_displacement_;
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    inline std::vector<double>& FollowingPressure<TimeDependencyT>::GetNonCstFormerLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
    }


    template<template<ParameterNS::Type> class TimeDependencyT>
    inline SpatialPoint& FollowingPressure<TimeDependencyT>::GetNonCstWorkCoords() noexcept
    {
        return work_coords_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_
