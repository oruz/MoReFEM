/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 May 2014 11:40:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"

#include "Operators/Enum.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"
#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/ElementaryData.hpp"                    // IWYU pragma: export
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Define the operator used in the hyperelastic problem.
             *
             * \todo Improve the comment by writing its mathematical definition!
             *
             * \tparam HyperelasticityPolicyT Policy that defines if an hyperelastic contribution is present in the
             * tensor. \tparam ViscoelasticityPolicyT Policy that defines if an viscoelastic contribution is present in
             * the tensor. \tparam InternalVariablePolicyT Policy that defines if an active stress contribution is
             * present in the tensor.
             *
             */
            template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
            class SecondPiolaKirchhoffStressTensor final
            : public HyperelasticityPolicyT,
              public ViscoelasticityPolicyT,
              public InternalVariablePolicyT,
              public NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>,
              public Crtp::LocalMatrixStorage<SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                                               ViscoelasticityPolicyT,
                                                                               InternalVariablePolicyT>,
                                              10ul>,
              public Crtp::LocalVectorStorage<SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                                               ViscoelasticityPolicyT,
                                                                               InternalVariablePolicyT>,
                                              2ul>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                              ViscoelasticityPolicyT,
                                                              InternalVariablePolicyT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Type of the elementary matrix.
                using matrix_type = LocalMatrix;

                //! Type of the elementary vector.
                using vector_type = LocalVector;

                //! Alias to variational operator parent.
                using parent = NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent = Crtp::LocalMatrixStorage<self, 10ul>;

                //! Alias to the parent that provides LocalVectorStorage.
                using vector_parent = Crtp::LocalVectorStorage<self, 2ul>;

                //! Rejuvenate alias from parent class.
                using elementary_data_type = typename parent::elementary_data_type;

                //! Alias to the type of the input of the active stress policy.
                using input_internal_variable_policy_type =
                    typename InternalVariablePolicyT::input_internal_variable_policy_type;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
                 * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
                 * hold exactly one vectorial unknown (as long as #7 is not implemented at the very least...)
                 *
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] solid Object which provides the required material parameters for the solid.
                 * \param[in] hyperelastic_law The hyperelastic law if there is an  hyperelastic part in the operator, or nullptr otherwise.
                 * Example of hyperelasticlaw is MoReFEM::HyperelasticLawNS::CiarletGeymonat.
                 * \copydoc doxygen_hide_time_manager_arg
                 * \param[in] input_internal_variable_policy Object involved with the internal variable (such as active stress)
                 * policy.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit SecondPiolaKirchhoffStressTensor(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data,
                    const Solid& solid,
                    const TimeManager& time_manager,
                    const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                    input_internal_variable_policy_type* input_internal_variable_policy);

                //! Destructor.
                virtual ~SecondPiolaKirchhoffStressTensor() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                SecondPiolaKirchhoffStressTensor(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                SecondPiolaKirchhoffStressTensor(SecondPiolaKirchhoffStressTensor&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                SecondPiolaKirchhoffStressTensor& operator=(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                SecondPiolaKirchhoffStressTensor& operator=(SecondPiolaKirchhoffStressTensor&& rhs) = delete;


                ///@}


                /*!
                 * \brief Compute the elementary matrix and/or vector.
                 */
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }


                //! Constant accessor to the former local displacement required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalDisplacement() const noexcept;

                //! Non constant accessor to the former local displacement required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalDisplacement() noexcept;

                //! Method to update internal variables of the active stress policy.
                void UpdateInternalVariableInternalVariables();

                //! Set Cauchy-Green tensor \a Parameter. Must be called only once.
                //! \param[in] param The parameter computed outside of the class.
                void SetCauchyGreenTensor(const ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                                           ParameterNS::TimeDependencyNS::None>* param);


              private:
                //! Convenient alias to drop namespace specifications.
                using InformationsAtQuadraturePoint =
                    ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint;

                /*!
                 * \brief Part of ComputeEltArray once all the dimension depending operations have been performed.
                 *
                 * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
                 * point, such as the geometric and finite element shape function values. Quadrature points related to
                 * functions are considered here.
                 * \param[in] tangent_matrix Tangent matrix.
                 * \param[in] rhs_part Rhs contribution.

                 * \param[in,out] elementary_data Objects which includes relevant elementary data. It might be modified
                 in
                 * only one way: it contains the local matrices and vectors accessible through GetNonCstMatrixResult()
                 and
                 * GetNonCstVectorResult() respectively.
                 */
                void ComputeAtQuadraturePoint(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                              const LocalMatrix& tangent_matrix,
                                              const LocalVector& rhs_part,
                                              elementary_data_type& elementary_data) const;


                /*!
                 * \class doxygen_hide_dW_d2W_derivates_arg
                 *
                 * \param[out] dW \a GlobalVector which stores the first derivates of W.
                 * \param[out] d2W \a GlobalMatrix which stores the second derivates of W.
                 */

                /*!
                 * \brief Compute the derivates of W.
                 *
                 * \copydoc doxygen_hide_dW_d2W_derivates_arg
                 * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                 * Test unknowns aren't considered at this point.
                 * \param[in] ref_felt Reference finite element considered.
                 * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                 * \param[in] gradient_displacement Gradient displacement matrix.
                 * \copydoc doxygen_hide_de_and_transpose_arg
                 */
                template<std::size_t DimensionT>
                void ComputeWDerivates(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                       const GeometricElt& geom_elt,
                                       const Advanced::RefFEltInLocalOperator& ref_felt,
                                       const LocalMatrix& gradient_displacement,
                                       const LocalMatrix& De,
                                       const LocalMatrix& transposed_De,
                                       LocalVector& dW,
                                       LocalMatrix& d2W);

                /*!
                 * \brief Compute internal data such as invariants, De, Cauchy-Green tensor for current quadrature
                 * point.
                 *
                 * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                 * \param[in] mesh_dimension Dimension of the mesh.
                 * \param[in] local_displacement Local displacement in observed within the finite element at previous
                 * time iteration. \param[in] ref_felt Reference finite element considered. \param[in] geom_elt \a
                 * GeometricElt for which the computation takes place.
                 */
                void
                PrepareInternalDataForQuadraturePoint(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                                      const GeometricElt& geom_elt,
                                                      const Advanced::RefFEltInLocalOperator& ref_felt,
                                                      const std::vector<double>& local_displacement,
                                                      const std::size_t mesh_dimension);


                //! Helper class to manage computation of derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
                GetNonCstDerivativeGreenLagrange() noexcept;


                /*!
                 * \brief Manage the ComputeEltArray part of the calculation that is dependant on the dimension
                 * considered.
                 *
                 *  \tparam MeshDimensionT Dimension of the mesh.
                 *
                 * \copydoc doxygen_hide_quad_pt_unknown_data_arg
                 * Test unknowns aren't considered at this point.
                 * \param[in] local_displacement Local displacement in observed within the finite element at previous
                 * time iteration. \param[in] ref_felt Reference finite element considered. \param[in] geom_elt \a
                 * GeometricElt for which the computation takes place.
                 */
                template<std::size_t MeshDimensionT>
                void PrepareInternalDataForQuadraturePointForDimension(
                    const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                    const GeometricElt& geom_elt,
                    const Advanced::RefFEltInLocalOperator& ref_felt,
                    const std::vector<double>& local_displacement);

                //! Get Cauchy-Green tensor \a Parameter.
                const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
                GetCauchyGreenTensor() const noexcept;


              private:
                //! Helper class to manage computation of derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr deriv_green_lagrange_ = nullptr;

                /*!
                 * \brief Pointer to the Cauchy-Green tensor \a Parameter.
                 *
                 * Do not release this pointer: it should point to an object which is already handled correctly.
                 *
                 * This must be set with SetCauchyGreenTensor().
                 */
                const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>*
                    cauchy_green_tensor_ = nullptr;

                /*!
                 * \brief Displacement obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray. \endinternal
                 */
                std::vector<double> former_local_displacement_;


                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Convenient aliases for matrix indexes.
                enum class LocalMatrixIndex : std::size_t
                {
                    tangent_matrix = 0,
                    linear_part,
                    dphi_test_mult_gradient_based_block,
                    block_contribution,
                    transposed_dphi,
                    d2W,
                    linear_part_intermediate_matrix,
                    gradient_based_block,
                    gradient_displacement,
                    deformation_gradient
                };


                //! Convenient aliases for vector indexes.
                enum class LocalVectorIndex : std::size_t
                {
                    dW = 0,
                    rhs_part
                };

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
