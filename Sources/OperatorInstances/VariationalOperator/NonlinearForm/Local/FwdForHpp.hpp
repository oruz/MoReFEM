//! \file
//
//
//  FwdForHpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 02/02/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FWD_FOR_HPP_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FWD_FOR_HPP_HPP_

#include <cstddef> // IWYU pragma: keep // IWYU pragma: export
#include <iosfwd>  // IWYU pragma: export
#include <memory>  // IWYU pragma: export

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Core/Parameter/FiberEnum.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp"  // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/ExtendedUnknown.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"           // IWYU pragma: export
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp" // IWYU pragma: export

#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FWD_FOR_HPP_HPP_
