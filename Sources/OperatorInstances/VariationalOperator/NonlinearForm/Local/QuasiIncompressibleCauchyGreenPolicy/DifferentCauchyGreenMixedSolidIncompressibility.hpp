/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Fri, 6 Mar 2020 11:47:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"
#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp"
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"

namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Mixed solid incompressibility.
             *
             * Used as penalization for incompressible hyperelasticity.
             *
             */
            template<class HydrostaticLawPolicyT>
            class DifferentCauchyGreenMixedSolidIncompressibility final
            : public NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>,
              public Crtp::LocalMatrixStorage<DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>,
                                              18ul>,
              public Crtp::LocalVectorStorage<DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>,
                                              6ul>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = DifferentCauchyGreenMixedSolidIncompressibility;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Type of the elementary matrix.
                using matrix_type = LocalMatrix;

                //! Type of the elementary vector.
                using vector_type = LocalVector;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent =
                    Crtp::LocalMatrixStorage<DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>,
                                             18ul>;

                //! Alias to the parent that provides LocalVectorStorage.
                using vector_parent =
                    Crtp::LocalVectorStorage<DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>,
                                             6ul>;

                //! Alias to CauchyGreenTensor.
                using cauchy_green_tensor_type =
                    ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>;

                //! Alias to invariant manager.
                using invariant_holder_type = typename HydrostaticLawPolicyT::invariant_holder_type;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
                 * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
                 * hold exactly two unknowns (the first one vectorial and the second one scalar).
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] hydrostatic_law Hydrostatic law policy; do not
                 * delete this pointer! \param[in] cauchy_green_tensor Cauchy-Green tensor of the related second
                 * Piola-Kirchhoff stress tensor.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit DifferentCauchyGreenMixedSolidIncompressibility(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data,
                    const cauchy_green_tensor_type& cauchy_green_tensor,
                    const HydrostaticLawPolicyT* hydrostatic_law);

                //! Destructor.
                virtual ~DifferentCauchyGreenMixedSolidIncompressibility() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                DifferentCauchyGreenMixedSolidIncompressibility(
                    const DifferentCauchyGreenMixedSolidIncompressibility& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                DifferentCauchyGreenMixedSolidIncompressibility(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) =
                    delete;

                //! \copydoc doxygen_hide_copy_affectation
                DifferentCauchyGreenMixedSolidIncompressibility&
                operator=(const DifferentCauchyGreenMixedSolidIncompressibility& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                DifferentCauchyGreenMixedSolidIncompressibility&
                operator=(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

                ///@}


                //! Compute the elementary vector.
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }

                //! Set Cauchy-Green tensor \a Parameter. Must be called only once.
                //! \param[in] param The parameter computed outside of the class.
                //! \internal <b><tt>[internal]</tt></b> Same function name as the one used by the
                //! SecondPiolaKirchhoffOperator to use the ApplySetCauchyGreenTensor helper. Here we only need to use
                //! it on the cauchy green tensor needed for the volumetric part of the computation as the deviatoric
                //! part as already been precomputed and is inherited through the \a stiffness_parent when this operator
                //! is constructed by the QuasiIncompressibleSecondPiolaKirchhoffOperator.
                void SetCauchyGreenTensor(const cauchy_green_tensor_type* param);

                //! Constant accessor to the former deviatoric local displacement required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalDisplacementDeviatoric() const noexcept;

                //! Non constant accessor to the deviatoric former local displacement required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalDisplacementDeviatoric() noexcept;

                //! Constant accessor to the former deviatoric local pressure required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalPressureDeviatoric() const noexcept;

                //! Non constant accessor to the former deviatoric local pressure required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalPressureDeviatoric() noexcept;

                //! Constant accessor to the former volumetric local displacement required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalDisplacementVolumetric() const noexcept;

                //! Non constant accessor to the former volumetric local displacement required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalDisplacementVolumetric() noexcept;

                //! Constant accessor to the former volumetric local pressure required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalPressureVolumetric() const noexcept;

                //! Non constant accessor to the former volumetric local pressure required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalPressureVolumetric() noexcept;

              private:
                //! Get Cauchy-Green tensor \a Parameter.
                const cauchy_green_tensor_type& GetCauchyGreenTensorDeviatoric() const noexcept;

                //! Get Cauchy-Green tensor \a Parameter.
                const cauchy_green_tensor_type& GetCauchyGreenTensorVolumetric() const noexcept;

                //! Helper class to manage computation of the deviatoric part of derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
                GetNonCstDerivativeGreenLagrangeDeviatoric() noexcept;

                //! Helper class to manage computation of the volumetric part of derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
                GetNonCstDerivativeGreenLagrangeVolumetric() noexcept;

                /*!
                 * \brief Compute internal data such as invariants, De, Cauchy-Green tensor for current quadrature
                 * point.
                 *
                 * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
                 * point, such as the geometric and finite element shape function values. Quadrature points related to
                 * functions are considered here.
                 * \param[in] felt_space_dimension Dimension of the \a FEltSpace.
                 * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                 * \param[in] local_displacement_deviatoric Local displacement observed within the finite element at
                 * previous time iteration. \param[in] local_pressure_deviatoric Local pressure observed within the
                 * finite element at previous time iteration. \param[in] local_displacement_volumetric Local
                 * displacement observed within the finite element at previous time iteration. \param[in]
                 * local_pressure_volumetric Local pressure observed within the finite element at previous time
                 * iteration.
                 * \param[in] displacement_ref_felt Reference finite element for displacement.
                 * \param[in] pressure_ref_felt Reference finite element for pressure.
                 * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
                 * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
                 */
                void PrepareInternalDataForQuadraturePoint(
                    const InformationsAtQuadraturePoint& infos_at_quad_pt,
                    const GeometricElt& geom_elt,
                    const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                    const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                    const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                    const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                    const std::vector<double>& local_displacement_deviatoric,
                    const std::vector<double>& local_pressure_deviatoric,
                    const std::vector<double>& local_displacement_volumetric,
                    const std::vector<double>& local_pressure_volumetric,
                    const std::size_t felt_space_dimension);

                /*!
                 * \brief Manage the ComputeEltArray part of the calculation that is dependant on the dimension
                 * considered.
                 *
                 *  \tparam MeshDimensionT Dimension of the mesh.
                 *
                 * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
                 * point, such as the geometric and finite element shape function values. Quadrature points related to
                 * functions are considered here.
                 * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                 * \param[in] local_displacement_deviatoric Local displacement observed within the finite element at
                 * previous time iteration. \param[in] local_pressure_deviatoric Local pressure observed within the
                 * finite element at previous time iteration. \param[in] local_displacement_volumetric Local
                 * displacement observed within the finite element at previous time iteration. \param[in]
                 * local_pressure_volumetric Local pressure observed within the finite element at previous time
                 * iteration.
                 * \param[in] displacement_ref_felt Reference finite element for displacement.
                 * \param[in] pressure_ref_felt Reference finite element for pressure.
                 * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
                 * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
                 */
                template<std::size_t FeltSpaceDimensionT>
                void PrepareInternalDataForQuadraturePointForDimension(
                    const InformationsAtQuadraturePoint& infos_at_quad_pt,
                    const GeometricElt& geom_elt,
                    const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                    const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                    const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                    const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                    const std::vector<double>& local_displacement_deviatoric,
                    const std::vector<double>& local_pressure_deviatoric,
                    const std::vector<double>& local_displacement_volumetric,
                    const std::vector<double>& local_pressure_volumetric);


              private:
                //! Access to the deviatoric invariant manager.
                const invariant_holder_type& GetInvariantHolderDeviatoric() const noexcept;

                //! Non constant access to the deviatoric invariant manager.
                invariant_holder_type& GetNonCstInvariantHolderDeviatoric() noexcept;

                //! Access to the volumetric invariant manager.
                const invariant_holder_type& GetInvariantHolderVolumetric() const noexcept;

                //! Non constant access to the volumetric invariant manager.
                invariant_holder_type& GetNonCstInvariantHolderVolumetric() noexcept;

                //! Underlying hydrostatic law.
                const HydrostaticLawPolicyT& GetHydrostaticLaw() const noexcept;

              private:
                //! Helper class to manage computation of the deviatoric part of the derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr
                    deriv_green_lagrange_deviatoric_ = nullptr;

                //! Helper class to manage computation of the volumetric part of the derivative of Green-Lagrange.
                DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr
                    deriv_green_lagrange_volumetric_ = nullptr;

                /*!
                 * \brief Reference to the Cauchy-Green tensor \a Parameter inherited through the \a stiffness_parent.
                 *
                 * Do not release this pointer: it should point to an object which is already handled correctly.
                 *
                 */
                const cauchy_green_tensor_type& cauchy_green_tensor_deviatoric_;

                /*!
                 * \
                 * \brief Pointer to the Cauchy-Green tensor \a Parameter.
                 *
                 * Do not release this pointer: it should point to an object which is already handled correctly.
                 *
                 * This must be set with SetCauchyGreenTensor().
                 */
                const cauchy_green_tensor_type* cauchy_green_tensor_volumetric_ = nullptr;


                /*!
                 * \brief Displacement obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray. \endinternal
                 */
                std::vector<double> former_local_displacement_deviatoric_;

                /*!
                 * \brief Displacement obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray. \endinternal
                 */
                std::vector<double> former_local_displacement_volumetric_;

                /*!
                 * \brief Pressure obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray.
                 * \endinternal
                 */
                std::vector<double> former_local_pressure_deviatoric_;

                /*!
                 * \brief Pressure obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray.
                 * \endinternal
                 */
                std::vector<double> former_local_pressure_volumetric_;

              private:
                //! Invariant manager for the deviatoric part.
                typename invariant_holder_type::unique_ptr invariant_holder_deviatoric_ = nullptr;

                //! Invariant manager for the volumetric part.
                typename invariant_holder_type::unique_ptr invariant_holder_volumetric_ = nullptr;

                //! Underlying hydrostatic law.
                const HydrostaticLawPolicyT* const hydrostatic_law_;


              private:
                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Indexes related to local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    tangent_matrix_disp_disp = 0,
                    tangent_matrix_disp_pres,
                    tangent_matrix_pres_disp,
                    transposed_dphi_displacement,
                    displacement_gradient_deviatoric,
                    displacement_gradient_volumetric,
                    hydrostatic_tangent, // pJC^-1 tangent
                    gradient_based_block,
                    block_contribution,
                    dphi_test_disp_mult_gradient_based_block,
                    linear_part,
                    linear_part_intermediate_matrix,
                    column_matrix,
                    dphi_test_disp_mult_column_matrix,
                    block_contribution_disp_pres,
                    row_matrix,
                    row_matrix_mult_transposed_dphi_disp,
                    block_contribution_pres_disp
                };


                //! Indexes related to local vectors.
                enum class LocalVectorIndex : std::size_t
                {
                    rhs_disp = 0,
                    hydrostatic_stress,                          // -pJC^-1
                    diff_hydrostatic_stress_wrt_pres_deviatoric, // -JC^-1
                    diff_hydrostatic_stress_wrt_pres_volumetric,
                    tangent_vector_disp_pres,
                    tangent_vector_pres_disp
                };
                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
