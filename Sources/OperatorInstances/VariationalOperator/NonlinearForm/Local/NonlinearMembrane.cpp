/*!
//
// \file
//
//
// Created by Dominique Chapelle <dominique.chapelle@inria.fr> on the Tue, 20 Feb 2018 12:29:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    NonlinearMembrane::NonlinearMembrane(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                         const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                         elementary_data_type&& a_elementary_data,
                                         const scalar_parameter& youngs_modulus,
                                         const scalar_parameter& poisson_ratio,
                                         const scalar_parameter& thickness,
                                         const scalar_parameter& pretension)
    : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), vector_parent(), youngs_modulus_(youngs_modulus), poisson_ratio_(poisson_ratio),
      thickness_(thickness), pretension_(pretension)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode = unknown_ref_felt.Nnode();


        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test = test_unknown_ref_felt.Nnode();

#ifndef NDEBUG
        {
            const std::size_t ref_felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

            const std::size_t euclidean_dimension = unknown_ref_felt.GetMeshDimension();

            assert(ref_felt_space_dimension < euclidean_dimension && "This operator is a surfacic operator.");
            assert((ref_felt_space_dimension + 1) == euclidean_dimension && "This operator is a surfacic operator.");
        }
#endif // NDEBUG

        former_local_displacement_.resize(elementary_data.NdofCol());

        matrix_parent::InitLocalMatrixStorage({ {
            { 3, 3 },              // tangent_tensor
            { 3, 6 },              // De_membrane
            { 6, 3 },              // transposed_De_membrane
            { 6, 3 },              // transposed_De_mult_tangent_tensor
            { 6, 6 },              // tangent_matrix
            { 3, 2 },              // displacement_gradient
            { 3, 2 },              // covariant_basis,
            { 3, 2 },              // contravariant_basis,
            { 2, 3 },              // transposed_covariant_basis,
            { 2, 2 },              // covariant_metric_tensor,
            { 2, 2 },              // contravariant_metric_tensor,
            { 2, 2 },              // gradient_based_block
            { 2, Nnode },          // transposed_dphi
            { Nnode_test, 2 },     // dphi_test_mult_gradient_based_block
            { Nnode_test, Nnode }, // block_contribution
            { 3, 3 },              // invert_generalized_covariant_basis
            { 3, 3 },              // test_pk_in_ref_basis
            { 3, 3 },              // test_gl_in_ref_basis
        } });

        vector_parent::InitLocalVectorStorage({ {
            3, // green-lagrange
            3, // second-PK
            6, // rhs_part
        } });
    }


    NonlinearMembrane::~NonlinearMembrane() = default;


    const std::string& NonlinearMembrane::ClassName()
    {
        static std::string name("NonlinearMembrane");
        return name;
    }


    void NonlinearMembrane::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.fill(0.);

        // Vector related calculation.
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        vector_result.fill(0.);

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

        const auto Nnode = unknown_ref_felt.Nnode();

        const auto Nnode_test = test_unknown_ref_felt.Nnode();

        decltype(auto) youngs_modulus = GetYoungsModulus();
        decltype(auto) poisson_ratio = GetPoissonRatio();
        decltype(auto) thickness = GetThickness();
        decltype(auto) pretension = GetPretension();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        constexpr auto Ncomponent = ComponentNS::index_type{ 3u };
        constexpr auto Nsurface_comp = 2u;

        std::size_t quad_pt_index = 0ul;

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

            double determinant;

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double youngs_modulus_value = youngs_modulus.GetValue(quad_pt, geom_elt);
            const double poisson_ratio_value = poisson_ratio.GetValue(quad_pt, geom_elt);
            const double thickness_value = thickness.GetValue(quad_pt, geom_elt);

            const auto pretension_value = pretension.GetValue(quad_pt, geom_elt);

            ComputeContravariantBasis(quad_pt_unknown_data, determinant);
            ComputeDisplacementGradient(quad_pt_unknown_data);
            ComputeGreenLagrange(pretension_value);

            ComputeTangentTensor(youngs_modulus_value, poisson_ratio_value);
            ComputeSecondPiolaKirchhoff();
            ComputeDe();
            ComputeTangentMatrixAndRightHandSide();

            const auto weight_meas = quad_pt.GetWeight() * std::sqrt(determinant) * thickness_value;
            // * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const auto& grad_felt_phi_test = test_quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& dphi_test = grad_felt_phi_test;

            assert(dphi.shape(0) == Nnode);
            assert(dphi_test.shape(0) == Nnode_test);

            if (parent::DoAssembleIntoMatrix())
            {
                auto& tangent_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();

                auto& gradient_based_block = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::gradient_based_block)>();
                auto& transposed_dphi =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();

                xt::noalias(transposed_dphi) = xt::transpose(dphi);

                // LocalMatrix dPhi_mult_gradient_based_block(dPhi.shape(0), static_cast<int>(Ncomponent));
                auto& dphi_test_mult_gradient_based_block = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::dphi_test_mult_gradient_based_block)>();
                auto& block_contribution =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

                for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
                {
                    const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                    for (ComponentNS::index_type col_component{ 0ul }; col_component < Ncomponent; ++col_component)
                    {
                        const auto col_first_index =
                            test_unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                        Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(
                            tangent_matrix, row_component, col_component, gradient_based_block);

                        xt::noalias(dphi_test_mult_gradient_based_block) =
                            xt::linalg::dot(dphi_test, gradient_based_block);

                        xt::noalias(block_contribution) =
                            weight_meas * xt::linalg::dot(dphi_test_mult_gradient_based_block, transposed_dphi);

                        for (auto row_node = 0ul; row_node < Nnode_test; ++row_node)
                        {
                            assert(row_first_index + row_node < matrix_result.shape(0));
                            assert(row_node < block_contribution.shape(0));

                            for (auto col_node = 0ul; col_node < Nnode; ++col_node)
                            {
                                assert(col_first_index + col_node < matrix_result.shape(1));
                                assert(col_node < block_contribution.shape(1));

                                matrix_result(row_first_index + row_node, col_first_index + col_node) +=
                                    block_contribution(row_node, col_node);
                            }
                        }
                    }
                }
            }

            if (parent::DoAssembleIntoVector())
            {
                auto& rhs_part = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();

                for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
                {
                    const auto dof_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                    const auto component_first_index = row_component.Get() * Nsurface_comp;

                    // Compute the new contribution to vector_result here.
                    // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                    for (auto row_node = 0ul; row_node < Nnode_test; ++row_node)
                    {
                        double value = 0.;

                        assert(row_node < dphi.shape(0));
                        assert(dphi.shape(1) == Nsurface_comp);

                        for (auto col = 0ul; col < Nsurface_comp; ++col)
                        {
                            assert(col + component_first_index < rhs_part.size());
                            value += dphi(row_node, col) * rhs_part(col + component_first_index);
                        }

                        assert(dof_first_index + row_node < vector_result.size());

                        vector_result(dof_first_index + row_node) += value * weight_meas;
                    }
                }
            }

            ++quad_pt_index;
        } // loop over quadrature points
    }


    void NonlinearMembrane ::ComputeContravariantBasis(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        double& determinant)
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
        covariant_basis.fill(0.);

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();
        const auto& dphi_geo = quad_pt_unknown_data.GetGradientRefGeometricPhi();

        constexpr auto Nsurface_comp = ComponentNS::index_type{ 2ul };
        constexpr auto euclidean_dimension = 3;
        const std::size_t Nshape_function = dphi_geo.shape(0);

        for (std::size_t shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (ComponentNS::index_type component_shape_function{ 0ul }; component_shape_function < Nsurface_comp;
                 ++component_shape_function)
            {
                for (std::size_t coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                {
                    covariant_basis(coord_index, component_shape_function.Get()) +=
                        coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function.Get());
                }
            }
        }


        auto& covariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
        auto& contravariant_metric_tensor =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

        auto& transposed_covariant_basis =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
        xt::noalias(transposed_covariant_basis) = xt::transpose(covariant_basis);
        xt::noalias(covariant_metric_tensor) = xt::linalg::dot(transposed_covariant_basis, covariant_basis);


        Wrappers::Xtensor::ComputeInverseSquareMatrix(
            covariant_metric_tensor, contravariant_metric_tensor, determinant);
    }


    void NonlinearMembrane ::ComputeDisplacementGradient(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data)
    {
        auto& displacement_gradient = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        displacement_gradient.fill(0.);

        auto& local_displacement = GetFormerLocalDisplacement();

        const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

        constexpr auto Nsurface_comp = ComponentNS::index_type{ 2ul };
        constexpr auto euclidean_dimension = 3ul;

        const std::size_t Nshape_function = dphi.shape(0);

        for (auto shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            for (ComponentNS::index_type component_shape_function{ 0ul }; component_shape_function < Nsurface_comp;
                 ++component_shape_function)
            {
                for (auto coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                {
                    const auto local_displacement_index = Nshape_function * coord_index + shape_fct_index;
                    assert(local_displacement_index < local_displacement.size());

                    displacement_gradient(coord_index, component_shape_function.Get()) +=
                        local_displacement[local_displacement_index]
                        * dphi(shape_fct_index, component_shape_function.Get());
                }
            }
        }
    }


    void NonlinearMembrane::ComputeGreenLagrange(const double pretension)
    {
        auto& green_lagrange = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
        const auto& displacement_gradient =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        const auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

        assert(green_lagrange.size() == 3);
        green_lagrange.fill(0.);

        // Green-Lagrange strain tensor entries
        for (auto i = 0ul; i < 3ul; ++i)
        {
            const auto disp_grad_0 = displacement_gradient(i, 0);
            const auto disp_grad_1 = displacement_gradient(i, 1);
            const auto cov_basis_0 = covariant_basis(i, 0);
            const auto cov_basis_1 = covariant_basis(i, 1);

            green_lagrange(0) += disp_grad_0 * (cov_basis_0 + 0.5 * disp_grad_0);
            green_lagrange(1) += disp_grad_1 * (cov_basis_1 + 0.5 * disp_grad_1);
            green_lagrange(2) += disp_grad_0 * cov_basis_1 + disp_grad_1 * cov_basis_0 + disp_grad_0 * disp_grad_1;
        }

        // PRETENSION

        if (!NumericNS::IsZero(pretension))
        {
            const auto& covariant_metric_tensor =
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
            green_lagrange(0) += pretension * covariant_metric_tensor(0, 0);
            green_lagrange(1) += pretension * covariant_metric_tensor(1, 1);
            green_lagrange(2) += 2. * pretension * covariant_metric_tensor(0, 1);
        }
    }


    void NonlinearMembrane::ComputeTangentTensor(double E, double nu)
    {
        auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
        const auto& contravariant_metric_tensor =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

        assert(!NumericNS::IsZero(nu));

        const double factor = E / (1. - NumericNS::Square(nu));

        tangent_tensor.fill(0.);

        const auto contravariant_metric_tensor_0_0 = contravariant_metric_tensor(0, 0);
        const auto contravariant_metric_tensor_0_1 = contravariant_metric_tensor(0, 1);
        const auto contravariant_metric_tensor_1_1 = contravariant_metric_tensor(1, 1);

        // Tangent tensor entries (upper triangular)
        tangent_tensor(0, 0) = factor * NumericNS::Square(contravariant_metric_tensor_0_0);
        tangent_tensor(0, 1) = factor
                               * ((1. - nu) * NumericNS::Square(contravariant_metric_tensor_0_1)
                                  + nu * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1);

        tangent_tensor(0, 2) = factor * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_0_1;
        tangent_tensor(1, 1) = factor * NumericNS::Square(contravariant_metric_tensor_1_1);
        tangent_tensor(1, 2) = factor * contravariant_metric_tensor_1_1 * contravariant_metric_tensor_0_1;
        tangent_tensor(2, 2) = 0.5 * factor
                               * ((1. - nu) * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1
                                  + (1. + nu) * NumericNS::Square(contravariant_metric_tensor_0_1));

        // Symmetry
        tangent_tensor(1, 0) = tangent_tensor(0, 1);
        tangent_tensor(2, 0) = tangent_tensor(0, 2);
        tangent_tensor(2, 1) = tangent_tensor(1, 2);
    }


    void NonlinearMembrane::ComputeSecondPiolaKirchhoff()
    {
        auto& second_PK = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();
        const auto& green_lagrange = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
        const auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();

        xt::noalias(second_PK) = xt::linalg::dot(tangent_tensor, green_lagrange);
    }


    void NonlinearMembrane::ComputeDe()
    {
        auto& De_membrane = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
        auto& transposed_De_membrane = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
        const auto& displacement_gradient =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        const auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

        De_membrane.fill(0.);

        // De/Dgrady matrix entries
        for (auto i = 0ul; i < 3ul; ++i)
        {
            const auto cov_basis_0 = covariant_basis(i, 0);
            const auto cov_basis_1 = covariant_basis(i, 1);
            const auto disp_grad_0 = displacement_gradient(i, 0);
            const auto disp_grad_1 = displacement_gradient(i, 1);

            De_membrane(0, 2 * i) = cov_basis_0 + disp_grad_0;
            De_membrane(1, 2 * i + 1) = cov_basis_1 + disp_grad_1;
            De_membrane(2, 2 * i) = cov_basis_1 + disp_grad_1;
            De_membrane(2, 2 * i + 1) = cov_basis_0 + disp_grad_0;
        }

        // Transposed
        xt::noalias(transposed_De_membrane) = xt::transpose(De_membrane);
    }


    void NonlinearMembrane::ComputeTangentMatrixAndRightHandSide()
    {
        auto& tangent_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
        auto& tangent_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
        const auto& De_membrane = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
        const auto& transposed_De_membrane =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
        auto& transposed_De_mult_tangent_tensor =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_mult_tangent_tensor)>();
        auto& rhs_part = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
        const auto& second_PK = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();

        if (parent::DoAssembleIntoMatrix())
        {
            transposed_De_mult_tangent_tensor.fill(0.);
            tangent_matrix.fill(0.);

            // Tangent: Rigidity part
            xt::noalias(transposed_De_mult_tangent_tensor) = xt::linalg::dot(transposed_De_membrane, tangent_tensor);
            xt::noalias(tangent_matrix) = xt::linalg::dot(transposed_De_mult_tangent_tensor, De_membrane);

            // Tangent: Geometric part
            for (auto i = 0ul; i < 3ul; ++i)
            {
                const double second_pk_2 = second_PK(2);

                tangent_matrix(2 * i, 2 * i) += second_PK(0);
                tangent_matrix(2 * i + 1, 2 * i + 1) += second_PK(1);
                tangent_matrix(2 * i, 2 * i + 1) += second_pk_2;
                tangent_matrix(2 * i + 1, 2 * i) += second_pk_2;
            }
        }

        if (parent::DoAssembleIntoVector())
        {
            rhs_part.fill(0.);
            xt::noalias(rhs_part) = xt::linalg::dot(transposed_De_membrane, second_PK);
        }
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup
