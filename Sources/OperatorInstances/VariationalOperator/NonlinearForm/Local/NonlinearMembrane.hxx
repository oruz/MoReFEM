/*!
//
// \file
//
//
// Created by Dominique Chapelle <dominique.chapelle@inria.fr> on the Tue, 20 Feb 2018 12:29:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp"

#include <vector>


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const NonlinearMembrane::scalar_parameter& NonlinearMembrane ::GetYoungsModulus() const noexcept
            {
                return youngs_modulus_;
            }

            inline const NonlinearMembrane::scalar_parameter& NonlinearMembrane ::GetPoissonRatio() const noexcept
            {
                return poisson_ratio_;
            }


            inline const NonlinearMembrane::scalar_parameter& NonlinearMembrane ::GetThickness() const noexcept
            {
                return thickness_;
            }

            inline const NonlinearMembrane::scalar_parameter& NonlinearMembrane ::GetPretension() const noexcept
            {
                return pretension_;
            }


            inline const std::vector<double>& NonlinearMembrane::GetFormerLocalDisplacement() const noexcept
            {
                return former_local_displacement_;
            }


            inline std::vector<double>& NonlinearMembrane::GetNonCstFormerLocalDisplacement() noexcept
            {
                return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_MEMBRANE_HXX_
