//! \file
//
//
//  FwdForHpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 02/02/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FWD_FOR_CPP_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FWD_FOR_CPP_HPP_

#include <cassert>     // IWYU pragma: export
#include <cmath>       // IWYU pragma: export
#include <map>         // IWYU pragma: export
#include <ostream>     // IWYU pragma: export
#include <type_traits> // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp" // IWYU pragma: export

#include "Core/Enum.hpp"                            // IWYU pragma: export
#include "Core/NumberingSubset/NumberingSubset.hpp" // IWYU pragma: export

#include "Geometry/Mesh/Mesh.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp" // IWYU pragma: export
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"      // IWYU pragma: export

#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FWD_FOR_CPP_HPP_
