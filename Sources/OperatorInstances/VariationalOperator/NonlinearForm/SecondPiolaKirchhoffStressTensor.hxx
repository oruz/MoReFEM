/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:40:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"

#include <cstddef> // IWYU pragma: keep

namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SecondPiolaKirchhoffStressTensor(const FEltSpace& felt_space,
                                         const Unknown::const_shared_ptr unknown_ptr,
                                         const Unknown::const_shared_ptr test_unknown_ptr,
                                         const Solid& solid,
                                         const TimeManager& time_manager,
                                         const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                         const QuadratureRulePerTopology* const a_quadrature_rule_per_topology,
                                         input_internal_variable_policy_type* input_internal_variable_policy)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             std::move(a_quadrature_rule_per_topology),
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             solid,
             time_manager,
             hyperelastic_law,
             input_internal_variable_policy)
    {
        assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        const auto& mesh = god_of_dof.GetMesh();
        decltype(auto) local_operator_storage = parent::GetLocalOperatorPerRefGeomElt();

        const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
        auto& internal_variable = *internal_variable_ptr;

        Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariableHelper<
            InternalVariablePolicyT>::InitInternalVariable(felt_space.GetDomain(),
                                                           parent::GetQuadratureRulePerTopology(),
                                                           time_manager,
                                                           local_operator_storage,
                                                           input_internal_variable_policy,
                                                           internal_variable);

        LocalVector initial_value;

        switch (mesh.GetDimension())
        {
        case 1ul:
            initial_value.resize({ 1ul });
            break;
        case 2ul:
            initial_value.resize({ 3ul });
            break;
        case 3ul:
            initial_value.resize({ 6ul });
            break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        decltype(auto) quadrature_rule_per_topology = parent::GetQuadratureRulePerTopology();

        cauchy_green_tensor_ = std::make_unique<cauchy_green_tensor_type>(
            "Cauchy-Green tensor", felt_space.GetDomain(), quadrature_rule_per_topology, initial_value, time_manager);

        cauchy_green_tensor_operator_ = std::make_unique<GlobalParameterOperatorNS::UpdateCauchyGreenTensor>(
            felt_space, *unknown_ptr, *cauchy_green_tensor_, &quadrature_rule_per_topology);

        // Feed CauchyGreen param to each local operator.

        decltype(auto) cauchy_green_tensor_raw = cauchy_green_tensor_.get();

        Internal::GlobalVariationalOperatorNS::ApplySetCauchyGreenTensor<
            typename parent::local_operator_storage_type,
            0ul,
            std::tuple_size<typename parent::local_operator_storage_type>::value>::Perform(local_operator_storage,
                                                                                           cauchy_green_tensor_raw);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    const std::string& SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                        ViscoelasticityPolicyT,
                                                        InternalVariablePolicyT>::ClassName()
    {
        static std::string name("SecondPiolaKirchhoffStressTensor");
        return name;
    }

    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    const typename SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                    ViscoelasticityPolicyT,
                                                    InternalVariablePolicyT>::cauchy_green_tensor_type&
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        GetCauchyGreenTensor() const noexcept
    {
        assert(!(!cauchy_green_tensor_));
        return *cauchy_green_tensor_;
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(
            std::move(linear_algebra_tuple), domain, displacement_previous_iteration);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        ConstRefVelocityGlobalVector velocity_previous_iteration,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(
            std::move(linear_algebra_tuple), domain, displacement_previous_iteration, velocity_previous_iteration);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        ConstRefVelocityGlobalVector velocity_previous_iteration,
        ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
        ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
        const bool do_update,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                               domain,
                                               displacement_previous_iteration,
                                               velocity_previous_iteration,
                                               electrical_activation_previous_time,
                                               electrical_activation_at_time,
                                               do_update);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        ConstRefVelocityGlobalVector velocity_previous_iteration,
        ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
        ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                               domain,
                                               displacement_previous_iteration,
                                               velocity_previous_iteration,
                                               electrical_activation_previous_time,
                                               electrical_activation_at_time);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
        ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
        const bool do_update,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                               domain,
                                               displacement_previous_iteration,
                                               electrical_activation_previous_time,
                                               electrical_activation_at_time,
                                               do_update);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefDisplacementGlobalVector displacement_previous_iteration,
        ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
        ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperator().Update(displacement_previous_iteration.Get());

        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                               domain,
                                               displacement_previous_iteration,
                                               electrical_activation_previous_time,
                                               electrical_activation_at_time);
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple<ConstRefDisplacementGlobalVector>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    // clang-format off
                                  const std::tuple
                                  <
                                      ConstRefDisplacementGlobalVector,
                                      ConstRefVelocityGlobalVector
                                  >& additional_arguments) const
    // clang-format on
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<1>(additional_arguments).Get(),
                              local_operator.GetNonCstLocalVelocity());
    }

    namespace // anonymous
    {


        template<class LocalVariationalOperatorT>
        [[maybe_unused]] void
        SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                const LocalFEltSpace& local_felt_space,
                                                const GlobalVector& a_electrical_activation_previous_time,
                                                const GlobalVector& a_electrical_activation_at_time,
                                                const bool do_update);


        template<class LocalVariationalOperatorT>
        [[maybe_unused]] void
        SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                const LocalFEltSpace& local_felt_space,
                                                const GlobalVector& a_electrical_activation_previous_time,
                                                const GlobalVector& a_electrical_activation_at_time);


    } // namespace


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    // clang-format off
                                  const std::tuple
                                  <
                                      ConstRefDisplacementGlobalVector,
                                      ConstRefVelocityGlobalVector,
                                      ConstRefPreviousElectricalActivationGlobalVector,
                                      ConstRefCurrentElectricalActivationGlobalVector,
                                      const bool
                                  >& additional_arguments) const
                                  // clang-format off
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<1>(additional_arguments).Get(),
                              local_operator.GetNonCstLocalVelocity());

        SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                local_felt_space,
                                                std::get<2>(additional_arguments).Get(),
                                                std::get<3>(additional_arguments).Get(),
                                                std::get<4>(additional_arguments));
    }


    template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
    ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                  LocalOperatorTypeT& local_operator,
                                  // clang-format off
                                  const std::tuple
                                  <
                                      ConstRefDisplacementGlobalVector,
                                      ConstRefVelocityGlobalVector,
                                      ConstRefPreviousElectricalActivationGlobalVector,
                                      ConstRefCurrentElectricalActivationGlobalVector
                                  >& additional_arguments) const
    // clang-format on
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<1>(additional_arguments).Get(),
                              local_operator.GetNonCstLocalVelocity());

        SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                local_felt_space,
                                                std::get<2>(additional_arguments).Get(),
                                                std::get<3>(additional_arguments).Get());
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    // clang-format off
                                  const std::tuple
                                  <
                                      ConstRefDisplacementGlobalVector,
                                      ConstRefPreviousElectricalActivationGlobalVector,
                                      ConstRefCurrentElectricalActivationGlobalVector,
                                      const bool
                                  >& additional_arguments) const
    // clang-format on
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                local_felt_space,
                                                std::get<1>(additional_arguments).Get(),
                                                std::get<2>(additional_arguments).Get(),
                                                std::get<3>(additional_arguments));
    }


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    template<class LocalOperatorTypeT>
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            // clang-format off
                                  const std::tuple
                                  <
                                      ConstRefDisplacementGlobalVector,
                                      ConstRefPreviousElectricalActivationGlobalVector,
                                      ConstRefCurrentElectricalActivationGlobalVector
                                  >& additional_arguments) const
    // clang-format on
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                local_felt_space,
                                                std::get<1>(additional_arguments).Get(),
                                                std::get<2>(additional_arguments).Get());
    }


    namespace // anonymous
    {


        template<class LocalVariationalOperatorT>
        void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                     const LocalFEltSpace& local_felt_space,
                                                     const GlobalVector& a_electrical_activation_previous_time,
                                                     const GlobalVector& a_electrical_activation_at_time,
                                                     const bool do_update_sigma_c)
        {
            SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                    local_felt_space,
                                                    a_electrical_activation_previous_time,
                                                    a_electrical_activation_at_time);

            local_operator.SetDoUpdateSigmaC(do_update_sigma_c);
        }


        template<class LocalVariationalOperatorT>
        void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                     const LocalFEltSpace& local_felt_space,
                                                     const GlobalVector& a_electrical_activation_previous_time,
                                                     const GlobalVector& a_electrical_activation_at_time)
        {
            auto& u0 = local_operator.GetNonCstLocalElectricalActivationPreviousTime();
            auto& u1 = local_operator.GetNonCstLocalElectricalActivationAtTime();

            {
                Wrappers::Petsc::AccessGhostContent ghost_vector_u0(
                    a_electrical_activation_previous_time, __FILE__, __LINE__);
                Wrappers::Petsc::AccessGhostContent ghost_vector_u1(
                    a_electrical_activation_at_time, __FILE__, __LINE__);

                const auto& vector_with_ghost_u0 = ghost_vector_u0.GetVectorWithGhost();
                const auto& vector_with_ghost_u1 = ghost_vector_u1.GetVectorWithGhost();

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> ghost_vector_content_u0(
                    vector_with_ghost_u0, __FILE__, __LINE__);
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> ghost_vector_content_u1(
                    vector_with_ghost_u1, __FILE__, __LINE__);

                const auto& electrical_activation_numbering_subset =
                    a_electrical_activation_previous_time.GetNumberingSubset();

                const auto& node_bearer_list = local_felt_space.GetNodeBearerList();

                const auto node_bearer_list_size = node_bearer_list.size();

                assert(u0.size() == node_bearer_list_size);
                assert(u1.size() == node_bearer_list_size);

                for (std::size_t i = 0; i < node_bearer_list_size; ++i)
                {
                    const auto& node_bearer_ptr = node_bearer_list[i];
                    assert(!(!node_bearer_ptr));
                    const auto& node_bearer = *node_bearer_ptr;

                    const auto& node_list = node_bearer.GetNodeList();

                    for (const auto& node_ptr : node_list)
                    {
                        assert(!(!node_ptr));

                        if (node_ptr->IsInNumberingSubset(electrical_activation_numbering_subset))
                        {
                            const auto& dof_list = node_ptr->GetDofList();

                            assert(dof_list.size() == 1 && "The node should contain a single dof of a scalar unknown.");

                            const auto& dof = *dof_list[0];

                            u0[i] = ghost_vector_content_u0.GetValue(
                                dof.GetProcessorWiseOrGhostIndex(electrical_activation_numbering_subset));
                            u1[i] = ghost_vector_content_u1.GetValue(
                                dof.GetProcessorWiseOrGhostIndex(electrical_activation_numbering_subset));
                        }
                    }
                }
            }
        }


    } // namespace


    template<class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
    inline const GlobalParameterOperatorNS::UpdateCauchyGreenTensor&
    SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::
        GetCauchyGreenTensorOperator() const noexcept
    {
        assert(!(!cauchy_green_tensor_operator_));
        return *cauchy_green_tensor_operator_;
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
