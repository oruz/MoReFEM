/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Sep 2014 14:19:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"

#include "Operators/GlobalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"
#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include <tuple>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class LocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<class LinearAlgebraTupleT>
        inline void NonlinearMembrane ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                 const GlobalVector& input_vector,
                                                 const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
        }


        template<class LocalOperatorTypeT>
        inline void NonlinearMembrane ::SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HXX_
