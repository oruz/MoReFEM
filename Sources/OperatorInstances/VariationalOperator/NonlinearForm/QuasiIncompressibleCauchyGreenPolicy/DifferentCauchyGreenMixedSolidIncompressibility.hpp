/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Fri, 6 Mar 2020 11:47:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{

    /*!
     * \brief DifferentCauchyGreenMixedSolidIncompressibility description.
     *
     * \todo #9 Describe operator!
     */
    template<class HydrostaticLawPolicyT>
    class DifferentCauchyGreenMixedSolidIncompressibility
    // clang-format off
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>,
        Advanced::OperatorNS::Nature::nonlinear,
        typename Advanced::LocalVariationalOperatorNS::DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Const Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_variational_operator_type =
            Advanced::LocalVariationalOperatorNS::DifferentCauchyGreenMixedSolidIncompressibility<
                HydrostaticLawPolicyT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::nonlinear,
            local_variational_operator_type
        >;
        // clang-format on

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to CauchyGreenTensor.
        using cauchy_green_tensor_type =
            ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>;

        //! Strong type for the deviatoric part of monolithic (displacement, pressure) vectors.
        using ConstRefMonolithicDeviatoricGlobalVector =
            StrongType<const GlobalVector&, struct MonolithicDeviatoricTag>;

        //! Strong type for the volumetric part of monolithic (displacement, pressure) vectors.
        using ConstRefMonolithicVolumetricGlobalVector =
            StrongType<const GlobalVector&, struct MonolithicVolumetricTag>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         * \param[in] unknown_list Container with vectorial then scalar unknown.
         * \copydoc doxygen_hide_test_unknown_list_param
         * \param[in] cauchy_green_tensor Cauchy-Green tensor used in the SecondPiolaKirchoffStressTensor operator.
         *  \param[in] time_manager \a TimeManager need for Cauchy Green parameter, Viscoelasticity and Active Stress.
         * \param[in] hydrostatic_law Policy concerning hydrostatic law.
         */
        explicit DifferentCauchyGreenMixedSolidIncompressibility(
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const cauchy_green_tensor_type& cauchy_green_tensor,
            const TimeManager& time_manager,
            const HydrostaticLawPolicyT* hydrostatic_law,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~DifferentCauchyGreenMixedSolidIncompressibility() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DifferentCauchyGreenMixedSolidIncompressibility(const DifferentCauchyGreenMixedSolidIncompressibility& rhs) =
            delete;

        //! \copydoc doxygen_hide_move_constructor
        DifferentCauchyGreenMixedSolidIncompressibility(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DifferentCauchyGreenMixedSolidIncompressibility&
        operator=(const DifferentCauchyGreenMixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DifferentCauchyGreenMixedSolidIncompressibility&
        operator=(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] previous_iteration_data Value from previous time iteration.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefMonolithicDeviatoricGlobalVector previous_iteration_data,
                      const Domain& domain = Domain()) const;

        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] deviatoric_vector Monolithic vector used to compute the deviatoric contribution.
         * \param[in] volumetric_vector Monolithic vector used to compute the volumetric contribution.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefMonolithicDeviatoricGlobalVector deviatoric_vector,
                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                      const Domain& domain = Domain()) const;

        //! Share Access to the Cauchy Green Tensor "Global Parameter"
        const cauchy_green_tensor_type& GetCauchyGreenTensorVolumetric() const noexcept;


      private:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            const std::tuple<ConstRefMonolithicDeviatoricGlobalVector>& additional_arguments) const;

        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            // clang-format off
                                    const std::tuple
                                    <
                                        ConstRefMonolithicDeviatoricGlobalVector,
                                        ConstRefMonolithicVolumetricGlobalVector
                                    >& additional_arguments) const;
        // clang-format on

      private:
        //! Accessor to Cauchy Green tensor operator.
        const GlobalParameterOperatorNS::UpdateCauchyGreenTensor&
        GetCauchyGreenTensorOperatorVolumetric() const noexcept;

      private:
        //! CauchyGreen tensor.
        typename cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_volumetric_ = nullptr;

        //! Cauchy Green operator.
        GlobalParameterOperatorNS::UpdateCauchyGreenTensor::const_unique_ptr cauchy_green_tensor_operator_volumetric_ =
            nullptr;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_DIFFERENT_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
