/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HPP_

#include <iosfwd>
#include <memory>
#include <tuple>

#include "Geometry/Domain/Domain.hpp"

#include "Operators/Enum.hpp"                                                // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        // clang-format off
        class NonlinearMembrane final
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            NonlinearMembrane,
            Advanced::OperatorNS::Nature::nonlinear,
            Advanced::LocalVariationalOperatorNS::NonlinearMembrane
        >
        // clang-format on
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = NonlinearMembrane;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::NonlinearMembrane;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::nonlinear,
                local_operator_type
            >;
            // clang-format on

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const NonlinearMembrane>;

            //! Alias to relevant scalar parameter type used in this operator.
            using scalar_parameter = local_operator_type::scalar_parameter;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * two unknowns (the first one vectorial and the second one scalar).
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] youngs_modulus Young's modulus of the solid.
             * \param[in] poisson_ratio Poisson ratio of the solid.
             * \param[in] thickness Thickness of the solid.
             * \param[in] pretension Pretension (related computation are skipped if the underlying value is 0.).
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit NonlinearMembrane(const FEltSpace& felt_space,
                                       Unknown::const_shared_ptr unknown_list,
                                       Unknown::const_shared_ptr test_unknown_list,
                                       const scalar_parameter& youngs_modulus,
                                       const scalar_parameter& poisson_ratio,
                                       const scalar_parameter& thickness,
                                       const scalar_parameter& pretension,
                                       const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~NonlinearMembrane() = default;

            //! \copydoc doxygen_hide_copy_constructor
            NonlinearMembrane(const NonlinearMembrane& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            NonlinearMembrane(NonlinearMembrane&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            NonlinearMembrane& operator=(const NonlinearMembrane& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            NonlinearMembrane& operator=(NonlinearMembrane&& rhs) = delete;

            ///@}


          public:
            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
             * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] previous_iteration_data Value from previous time iteration.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& previous_iteration_data,
                          const Domain& domain = Domain()) const;


          public:
            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally.
             */
            template<class LocalOperatorTypeT>
            void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                             LocalOperatorTypeT& local_operator,
                                             const std::tuple<const GlobalVector&>& additional_arguments) const;
        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_MEMBRANE_HPP_
