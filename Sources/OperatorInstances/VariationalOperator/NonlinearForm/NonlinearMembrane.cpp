/*!
//
// \file
//
//
// Created by Dominique Chapelle <dominique.chapelle@inria.fr> on the Tue, 20 Feb 2018 12:29:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <map>
#include <ostream>
#include <vector>

#include "Core/Enum.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM
{
    namespace GlobalVariationalOperatorNS
    {


        NonlinearMembrane::NonlinearMembrane(const FEltSpace& felt_space,
                                             Unknown::const_shared_ptr unknown_list,
                                             Unknown::const_shared_ptr test_unknown_list,
                                             const scalar_parameter& youngs_modulus,
                                             const scalar_parameter& poisson_ratio,
                                             const scalar_parameter& thickness,
                                             const scalar_parameter& pretension,
                                             const QuadratureRulePerTopology* const quadrature_rule_per_topology)

        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::yes,
                 youngs_modulus,
                 poisson_ratio,
                 thickness,
                 pretension)
        { }


        const std::string& NonlinearMembrane::ClassName()
        {
            static std::string name("NonlinearMembrane");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
