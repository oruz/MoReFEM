/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace HyperelasticityPolicyNS
            {


                /*!
                 * \brief Policy to use when there is an hyperelastic law considered in the operator.
                 *
                 * \tparam HyperelasticLawT Hyperelastic law to use; some are defined in Operators/HyperelasticLaw
                 * folder (and HyperelasticLawNS namespace). You may use more specific one; see Poromechanics model in
                 * which an extended version of StVenantKirchhoff is used.
                 *
                 * \attention Current class only stores a reference to the hyperelastic law object to use; it is assumed
                 * its lifespan exceeds the operator's one.
                 */
                template<class HyperelasticLawT>
                class Hyperelasticity
                {
                  public:
                    //! \copydoc doxygen_hide_gvo_local_policy_alias
                    using local_policy = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                        HyperelasticityPolicyNS::Hyperelasticity<HyperelasticLawT>;

                    //! Convenient alias upon template parameter.
                    using law_type = HyperelasticLawT;

                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = Hyperelasticity<HyperelasticLawT>;

                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Hyperelasticity() = default;

                    //! Destructor.
                    ~Hyperelasticity() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    Hyperelasticity(const Hyperelasticity& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Hyperelasticity(Hyperelasticity&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Hyperelasticity& operator=(const Hyperelasticity& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Hyperelasticity& operator=(Hyperelasticity&& rhs) = delete;

                    ///@}

                  private:
                };


            } // namespace HyperelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_
