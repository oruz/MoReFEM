/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace HyperelasticityPolicyNS
            {


                /*!
                 * \brief Policy to choose when there is no hyperelasticity
                 */
                class None
                {
                  public:
                    //! \copydoc doxygen_hide_gvo_local_policy_alias
                    using local_policy = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                        HyperelasticityPolicyNS::None;

                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = None;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                    //! Expected alias.
                    using law_type = std::nullptr_t;

                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit None() = default;

                    //! Destructor.
                    ~None() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    None(const None& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    None(None&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    None& operator=(const None& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    None& operator=(None&& rhs) = delete;

                    ///@}

                  private:
                };


            } // namespace HyperelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_
