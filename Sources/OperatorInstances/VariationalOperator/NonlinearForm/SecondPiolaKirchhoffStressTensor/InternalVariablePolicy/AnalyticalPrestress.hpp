/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Jan 2015 10:49:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                //! Implements an active stress Sigma_c*Tau*Tau, with dSigma_c/dt = sigma0 + |u|+ - |u|dSigma_c.
                template<std::size_t FiberIndexT>
                class AnalyticalPrestress
                {
                  public:
                    //! \copydoc doxygen_hide_gvo_local_policy_alias
                    using local_policy = ::MoReFEM::Advanced::LocalVariationalOperatorNS::
                        SecondPiolaKirchhoffStressTensorNS ::InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT>;

                    //! Alias to the type of the input.
                    using input_internal_variable_policy_type =
                        typename local_policy::input_internal_variable_policy_type;

                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = AnalyticalPrestress<FiberIndexT>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit AnalyticalPrestress() = default;

                    //! Destructor.
                    ~AnalyticalPrestress() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    AnalyticalPrestress(const AnalyticalPrestress& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    AnalyticalPrestress(AnalyticalPrestress&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    AnalyticalPrestress& operator=(const AnalyticalPrestress& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    AnalyticalPrestress& operator=(AnalyticalPrestress&& rhs) = delete;

                    ///@}

                  public:
                    /*!
                     * \brief Initialize Sigma_c.
                     *
                     * \param[in] domain \a Domain.
                     * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
                     * \param[in] time_manager \a TimeManager of the model.
                     * \param[in] local_operator_storage Tuple with all local operators considered.
                     * \param[in] input_internal_variable_policy Object with relevant input data.
                     */
                    template<class LocalOperatorStorageT>
                    void InitializeInternalVariablePolicy(
                        const Domain& domain,
                        const QuadratureRulePerTopology& quadrature_rule_per_topology,
                        const TimeManager& time_manager,
                        const LocalOperatorStorageT& local_operator_storage,
                        input_internal_variable_policy_type* input_internal_variable_policy);

                  private:
                    //! Accessor.
                    const input_internal_variable_policy_type& GetInputActiveStress() const noexcept;

                    //! Accessor.
                    input_internal_variable_policy_type& GetNonCstInputActiveStress() noexcept;

                  private:
                    //! Object with relevant data for internal variable.
                    input_internal_variable_policy_type* input_internal_variable_policy_ = nullptr;

                  private:
                    //! Sigma_c.
                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>::unique_ptr sigma_c_ = nullptr;
                };


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
