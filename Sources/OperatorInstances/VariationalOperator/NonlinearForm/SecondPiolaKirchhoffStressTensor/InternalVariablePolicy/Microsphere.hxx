/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 16:35:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T>
                template<class LocalOperatorStorageT>
                void Microsphere<FiberIndexI4T, FiberIndexI6T>::InitializeInternalVariablePolicy(
                    const Domain& domain,
                    const QuadratureRulePerTopology& quadrature_rule_per_topology,
                    const TimeManager& time_manager,
                    const LocalOperatorStorageT& local_operator_storage,
                    input_internal_variable_policy_type* input_internal_variable_policy)
                {
                    static_cast<void>(domain);
                    static_cast<void>(quadrature_rule_per_topology);
                    static_cast<void>(time_manager);
                    static_cast<void>(local_operator_storage);

                    assert(!(!input_internal_variable_policy));

                    input_internal_variable_policy_ = input_internal_variable_policy;
                }


                template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T>
                inline const typename Microsphere<FiberIndexI4T, FiberIndexI6T>::input_internal_variable_policy_type&
                Microsphere<FiberIndexI4T, FiberIndexI6T>::GetInputMicrosphere() const noexcept
                {
                    assert(!(!input_internal_variable_policy_));
                    return *input_internal_variable_policy_;
                }


                template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T>
                inline typename Microsphere<FiberIndexI4T, FiberIndexI6T>::input_internal_variable_policy_type&
                Microsphere<FiberIndexI4T, FiberIndexI6T>::GetNonCstInputMicrosphere() noexcept
                {
                    return const_cast<input_internal_variable_policy_type&>(GetInputMicrosphere());
                }


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_
