/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Mar 2016 18:13:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_INTERNAL_x_HELPER_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_INTERNAL_x_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                /*!
                 * \class doxygen_hide_internal_variable_helper_init_function
                 *
                 * \brief Static method that inits properly internal variable (such as active stress).

                 * \param[in] domain \a Domain upon which the SecondPiolaKirchhoffStressTensor operator is defined.
                 * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
                 * \copydetails doxygen_hide_time_manager_arg
                 * \param[in] local_operator_storage List of \a LocalVariationalOperator considered in the
                 * SecondPiolaKirchhoffStressTensor sort per \a RefGeomElt (container is an associative one).
                 * \param[in] input_internal_variable_policy Input internal policy.
                 * \param[in,out] internal_variable Internal variable to be initialized.
                 */

                /*!
                 * \class doxygen_hide_internal_variable_helper_match_dof_function
                 *
                 * \brief Static method that match dofs between the scalar unknown underneath the internal variable and
                 * the vectorial one considered in the operator.
                 *
                 * \param[in] internal_variable Internal variable for which the matching is performed.
                 *
                 * \return The object that store the pairings found.
                 */


                /*!
                 * \brief Helper struct used to call
                 * SecondPiolaKirchhoffStressTensor::InitializeInternalVariablePolicy() if the InternalVariablePolicyT
                 * is not InternalVariablePolicyNS::None.
                 *
                 * \tparam InternalVariablePolicyT Internal variable policy.
                 */
                template<class InternalVariablePolicyT>
                struct InternalVariableHelper
                {


                    //! \copydoc doxygen_hide_internal_variable_helper_init_function
                    template<class LocalOperatorStorageT>
                    static void
                    InitInternalVariable(const Domain& domain,
                                         const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                         const TimeManager& time_manager,
                                         const LocalOperatorStorageT& local_operator_storage,
                                         typename InternalVariablePolicyT::input_internal_variable_policy_type*
                                             input_internal_variable_policy,
                                         InternalVariablePolicyT& internal_variable);
                };


                /*!
                 * \brief Helper struct used to call
                 * SecondPiolaKirchhoffStressTensor::InitializeInternalVariablePolicy() if the InternalVariablePolicyT
                 * is InternalVariablePolicyNS::None.
                 *
                 * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
                 * InternalVariablePolicyNS::None policy in which all the methods would be asked to do nothing.
                 * \endinternal
                 *
                 */
                template<>
                struct InternalVariableHelper<::MoReFEM::GlobalVariationalOperatorNS::
                                                  SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None>
                {

                    //! Convenient alias.
                    using none_type = ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                        InternalVariablePolicyNS::None;


                    //! \copydoc doxygen_hide_internal_variable_helper_init_function
                    template<class LocalOperatorStorageT>
                    static void InitInternalVariable(
                        const Domain& domain,
                        const QuadratureRulePerTopology& quadrature_rule_per_topology,
                        const TimeManager& time_manager,
                        const LocalOperatorStorageT& local_operator_storage,
                        typename none_type::input_internal_variable_policy_type* input_internal_variable_policy,
                        none_type& internal_variable);
                };


                /*!
                 * \brief Metaprogramming struct to call SetSigmaC for all elements of a local operator tuple.
                 *
                 * \copydoc doxygen_hide_global_operator_local_operator_tuple_type
                 * \tparam I Position of the element being considered currently in the recursive call.
                 * \tparam TupleSizeT Must be std::tuple_size<LocalOperatorTupleT>::value; used for the stopping
                 * condition of the recursive call.
                 */
                template<class LocalOperatorTupleT, std::size_t I, std::size_t TupleSizeT>
                struct SetSigmaCHelper
                {


                    static_assert(TupleSizeT == std::tuple_size<LocalOperatorTupleT>::value, "");

                    /*!
                     * \brief Static method that does the actual work.
                     *
                     * \param[in] raw_sigma_c Raw pointer to an adequate \a Parameter for sigma_c value.
                     * \copydoc doxygen_hide_global_operator_local_operator_tuple_in
                     */
                    static void Perform(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* raw_sigma_c,
                                        const LocalOperatorTupleT& local_operator_tuple);
                };


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Stopping condition for the recursive call.
                // ============================


                template<class LocalOperatorTupleT, std::size_t TupleSizeT>
                struct SetSigmaCHelper<LocalOperatorTupleT, TupleSizeT, TupleSizeT>
                {


                    static void Perform(ParameterAtQuadraturePoint<ParameterNS::Type::scalar>*,
                                        const LocalOperatorTupleT&);
                };


                // ============================
                // Stopping condition for the recursive call.
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_INTERNAL_x_HELPER_HPP_
