/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 16:35:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                //! Implements a microsphere model for the two families of fibers with in and out plane dispersions.
                template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T>
                class Microsphere
                {
                  public:
                    //! \copydoc doxygen_hide_gvo_local_policy_alias
                    using local_policy =
                        ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
                            InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T>;

                    //! Alias to the type of the input.
                    using input_internal_variable_policy_type =
                        typename local_policy::input_internal_variable_policy_type;

                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = Microsphere<FiberIndexI4T, FiberIndexI6T>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Microsphere() = default;

                    //! Destructor.
                    ~Microsphere() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    Microsphere(const Microsphere& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Microsphere(Microsphere&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Microsphere& operator=(const Microsphere& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Microsphere& operator=(Microsphere&& rhs) = delete;

                    ///@}

                  public:
                    /*!
                     * \brief Initialize Sigma_c.
                     *
                     * \param[in] domain \a Domain.
                     * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
                     * \param[in] time_manager \a TimeManager of the model.
                     * \param[in] local_operator_storage Tuple with all local operators considered.
                     * \param[in] input_internal_variable_policy Object with relevant input data.
                     */
                    template<class LocalOperatorStorageT>
                    void InitializeInternalVariablePolicy(
                        const Domain& domain,
                        const QuadratureRulePerTopology& quadrature_rule_per_topology,
                        const TimeManager& time_manager,
                        const LocalOperatorStorageT& local_operator_storage,
                        input_internal_variable_policy_type* input_internal_variable_policy);

                  private:
                    //! Accessor.
                    const input_internal_variable_policy_type& GetInputMicrosphere() const noexcept;

                    //! Accessor.
                    input_internal_variable_policy_type& GetNonCstInputMicrosphere() noexcept;

                  private:
                    //! Object with relevant data for internal variable.
                    input_internal_variable_policy_type* input_internal_variable_policy_ = nullptr;
                };


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_
