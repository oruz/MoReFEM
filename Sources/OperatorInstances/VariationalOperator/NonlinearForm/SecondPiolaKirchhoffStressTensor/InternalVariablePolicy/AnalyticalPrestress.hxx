/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 22 Jan 2016 12:33:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                template<std::size_t FiberIndexT>
                template<class LocalOperatorStorageT>
                void AnalyticalPrestress<FiberIndexT>::InitializeInternalVariablePolicy(
                    const Domain& domain,
                    const QuadratureRulePerTopology& quadrature_rule_per_topology,
                    const TimeManager& time_manager,
                    const LocalOperatorStorageT& local_operator_storage,
                    input_internal_variable_policy_type* input_internal_variable_policy)
                {
                    assert(!(!input_internal_variable_policy));

                    input_internal_variable_policy_ = input_internal_variable_policy;

                    sigma_c_ = std::make_unique<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>>(
                        "SigmaC",
                        domain,
                        quadrature_rule_per_topology,
                        GetInputActiveStress().GetInitialValueInternalVariable(),
                        time_manager);

                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>* raw_sigma_c = sigma_c_.get();


                    ::MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
                        SetSigmaCHelper<LocalOperatorStorageT, 0, std::tuple_size<LocalOperatorStorageT>::value>::
                            Perform(raw_sigma_c, local_operator_storage);
                }


                template<std::size_t FiberIndexT>
                inline const typename AnalyticalPrestress<FiberIndexT>::input_internal_variable_policy_type&
                AnalyticalPrestress<FiberIndexT>::GetInputActiveStress() const noexcept
                {
                    assert(!(!input_internal_variable_policy_));
                    return *input_internal_variable_policy_;
                }


                template<std::size_t FiberIndexT>
                inline typename AnalyticalPrestress<FiberIndexT>::input_internal_variable_policy_type&
                AnalyticalPrestress<FiberIndexT>::GetNonCstInputActiveStress() noexcept
                {
                    return const_cast<input_internal_variable_policy_type&>(GetInputActiveStress());
                }


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_ANALYTICAL_PRESTRESS_HXX_
