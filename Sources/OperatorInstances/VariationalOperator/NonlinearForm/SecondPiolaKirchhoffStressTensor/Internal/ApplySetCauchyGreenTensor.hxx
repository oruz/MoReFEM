/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:23:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            template<class LocalOperatorTupleT, std::size_t I, std::size_t SizeTupleT>
            template<class CauchyGreenTypeT>
            void ApplySetCauchyGreenTensor<LocalOperatorTupleT, I, SizeTupleT>::Perform(
                const LocalOperatorTupleT& local_operator_tuple,
                const CauchyGreenTypeT& cauchy_green_tensor_raw_ptr)
            {
                const auto& local_operator_item = std::get<I>(local_operator_tuple);

                if (local_operator_item.IsRelevant())
                    local_operator_item.GetNonCstLocalOperator().SetCauchyGreenTensor(cauchy_green_tensor_raw_ptr);

                ApplySetCauchyGreenTensor<LocalOperatorTupleT, I + 1, SizeTupleT>::Perform(local_operator_tuple,
                                                                                           cauchy_green_tensor_raw_ptr);
            }


            template<class LocalOperatorTupleT, std::size_t SizeTupleT>
            template<class CauchyGreenTypeT>
            void
            ApplySetCauchyGreenTensor<LocalOperatorTupleT, SizeTupleT, SizeTupleT>::Perform(const LocalOperatorTupleT&,
                                                                                            const CauchyGreenTypeT&)
            { }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_APPLY_SET_CAUCHY_GREEN_TENSOR_HXX_
