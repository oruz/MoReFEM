/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 15 Jun 2015 11:55:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_NON_LINEAR_SOURCE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_NON_LINEAR_SOURCE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/NonLinearSource.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
        NonLinearSource<ReactionLawNameT>::NonLinearSource(
            const FEltSpace& felt_space,
            const Unknown::const_shared_ptr unknown_ptr,
            reaction_law_type& reaction_law,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)

        : parent(felt_space,
                 unknown_ptr,
                 unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::no,
                 DoComputeProcessorWiseLocal2Global::yes,
                 reaction_law)
        { }


        template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
        const std::string& NonLinearSource<ReactionLawNameT>::ClassName()
        {
            static std::string name("NonLinearSource");
            return name;
        }


        template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
        template<class LinearAlgebraTupleT>
        inline void NonLinearSource<ReactionLawNameT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                const GlobalVector& input_vector,
                                                                const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
        }


        template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
        template<class LocalOperatorTypeT>
        inline void NonLinearSource<ReactionLawNameT>::SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalSolution());
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_NON_LINEAR_SOURCE_HXX_
