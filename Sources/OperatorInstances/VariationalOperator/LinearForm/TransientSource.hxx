/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 3 Feb 2015 14:05:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_TRANSIENT_SOURCE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_TRANSIENT_SOURCE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT>
        TransientSource<TypeT, TimeDependencyT>::TransientSource(
            const FEltSpace& felt_space,
            const Unknown::const_shared_ptr unknown_ptr,
            const parameter_type& source,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::no,
                 DoComputeProcessorWiseLocal2Global::no,
                 source)
        { }


        template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT>
        const std::string& TransientSource<TypeT, TimeDependencyT>::ClassName()
        {
            static std::string name("Transient source");
            return name;
        }


        template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT>
        template<class LinearAlgebraTupleT>
        inline void
        TransientSource<TypeT, TimeDependencyT>::Assemble(LinearAlgebraTupleT&& global_vector_with_coeff_tuple,
                                                          double time,
                                                          const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(global_vector_with_coeff_tuple), domain, time);
        }


        template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT>
        template<class LocalOperatorTypeT>
        inline void TransientSource<TypeT, TimeDependencyT>::SetComputeEltArrayArguments(
            const LocalFEltSpace& local_felt_space,
            LocalOperatorTypeT& local_operator,
            const std::tuple<double>& additional_arguments) const
        {
            static_cast<void>(local_felt_space);

            local_operator.SetTime(std::get<0>(additional_arguments));
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_TRANSIENT_SOURCE_HXX_
