/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 15 Jun 2015 11:55:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            NonLinearSource<ReactionLawNameT>::NonLinearSource(
                const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                elementary_data_type&& a_elementary_data,
                reaction_law_type& reaction_law)
            : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)), reaction_law_(reaction_law)
            {
                const auto& elementary_data = GetElementaryData();
                former_local_solution_.resize(elementary_data.NdofRow());
            }


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            NonLinearSource<ReactionLawNameT>::~NonLinearSource() = default;


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            const std::string& NonLinearSource<ReactionLawNameT>::ClassName()
            {
                static std::string name("NonLinearSource");
                return name;
            }


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            void NonLinearSource<ReactionLawNameT>::ComputeEltArray()
            {
                auto& elementary_data = parent::GetNonCstElementaryData();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

                const auto& local_solution = GetFormerLocalSolution();

                assert(parent::GetNthUnknown().GetNature() == UnknownNS::Nature::scalar
                       && "NonLinearSource is limited to a scalar unknown. It could be extended to a vectorial one but "
                          "not in a generic way.");

                auto& vector_result = elementary_data.GetNonCstVectorResult();

                const auto& test_ref_felt = elementary_data.GetTestRefFElt(parent::GetNthTestUnknown());

                const auto Nnode_test = static_cast<int>(test_ref_felt.Nnode());

                auto& unknown_interpolate_at_quad_point = GetNonCstReactionLaw().GetNonCstLocalPotential();

                const auto& geom_elt = elementary_data.GetCurrentGeomElt();


                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    const auto& phi_test = test_quad_pt_unknown_data.GetRefFEltPhi();
                    const auto& phi = quad_pt_unknown_data.GetRefFEltPhi();

                    const auto& quad_pt = test_quad_pt_unknown_data.GetQuadraturePoint();

                    const double quad_pt_factor =
                        quad_pt.GetWeight() * test_quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

                    unknown_interpolate_at_quad_point = 0;

                    // This loop interpolates the unknown at the current quad point. It is needed to forward gate.
                    for (auto node_index = 0; node_index < Nnode_test; ++node_index)
                    {
                        const auto unsigned_node_index = static_cast<std::size_t>(node_index);

                        unknown_interpolate_at_quad_point += phi(node_index) * local_solution[unsigned_node_index];
                    }

                    const double i_ion = GetNonCstReactionLaw().ReactionLawFunction(
                        unknown_interpolate_at_quad_point, quad_pt, geom_elt);

                    // This loop integrates the source at the quad point.
                    for (auto node_index = 0; node_index < Nnode_test; ++node_index)
                    {
                        vector_result(node_index) += quad_pt_factor * phi_test(node_index) * i_ion;
                    }
                }
            }


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            inline const typename NonLinearSource<ReactionLawNameT>::reaction_law_type&
            NonLinearSource<ReactionLawNameT>::GetReactionLaw() const noexcept
            {
                return reaction_law_;
            }

            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            inline typename NonLinearSource<ReactionLawNameT>::reaction_law_type&
            NonLinearSource<ReactionLawNameT>::GetNonCstReactionLaw() noexcept
            {
                return reaction_law_;
            }


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            inline const std::vector<double>& NonLinearSource<ReactionLawNameT>::GetFormerLocalSolution() const noexcept
            {
                return former_local_solution_;
            }


            template<Advanced::ReactionLawNS::ReactionLawName ReactionLawNameT>
            inline std::vector<double>& NonLinearSource<ReactionLawNameT>::GetNonCstFormerLocalSolution() noexcept
            {
                return const_cast<std::vector<double>&>(GetFormerLocalSolution());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_HXX_
