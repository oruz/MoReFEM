/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 14:28:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_REACTION_LAW_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_REACTION_LAW_HPP_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {


            //! Enum of the reaction laws implemented.
            enum class ReactionLawName
            {
                FitzHughNagumo,
                MitchellSchaeffer,
                CourtemancheRamirezNattel
            };

            //! Declaration of template class ReactionLaw.
            template<ReactionLawName>
            class ReactionLaw;


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/CourtemancheRamirezNattel.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/FitzHughNagumo.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/MitchellSchaeffer.hpp"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_REACTION_LAW_HPP_
