/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <cassert>
#include <unordered_map>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/MitchellSchaeffer.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/ReactionLaw.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {


            const std::string& ReactionLaw<ReactionLawName::MitchellSchaeffer>::ClassName()
            {
                static std::string name("MitchellSchaeffer");
                return name;
            }


            void ReactionLaw<ReactionLawName::MitchellSchaeffer>::GateLawFunction(const QuadraturePoint& quad_pt,
                                                                                  const GeometricElt& geom_elt,
                                                                                  double& gate) const
            {
                const auto& local_potential = GetLocalPotential();

                if (local_potential < GetUGate())
                {
                    assert(!NumericNS::IsZero(NumericNS::Square(GetUMax() - GetUMin())));
                    assert(!NumericNS::IsZero(GetTauOpen()));

                    gate += GetTimeManager().GetTimeStep() * (1. / NumericNS::Square(GetUMax() - GetUMin()) - gate)
                            / GetTauOpen();
                } else
                {
                    gate += GetTimeManager().GetTimeStep() * (-gate / GetTauClose().GetValue(quad_pt, geom_elt));
                }
            }


            double ReactionLaw<ReactionLawName::MitchellSchaeffer>::ReactionLawFunction(const double local_potential,
                                                                                        const QuadraturePoint& quad_pt,
                                                                                        const GeometricElt& geom_elt)
            {
                auto functor = [&quad_pt, &geom_elt, this](double& gate)
                {
                    return GateLawFunction(quad_pt, geom_elt, gate);
                };

                const double new_gate = GetNonCstGate().UpdateAndGetValue(quad_pt, geom_elt, functor);

                assert(!NumericNS::IsZero(GetTauIn() * (GetUMax() - GetUMin())));
                assert(!NumericNS::IsZero(GetTauOut() * (GetUMax() - GetUMin())));

                return new_gate * NumericNS::Square(local_potential - GetUMin()) * (GetUMax() - local_potential)
                           / (GetTauIn() * (GetUMax() - GetUMin()))
                       - (local_potential - GetUMin()) / (GetTauOut() * (GetUMax() - GetUMin()));
            }

            void ReactionLaw<ReactionLawName::MitchellSchaeffer>::WriteGate(const std::string& filename) const
            {
                GetGate().Write(filename);
            }


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
