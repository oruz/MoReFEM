/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 5 Oct 2015 10:59:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cmath>
#include <unordered_map>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/CourtemancheRamirezNattel.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/ReactionLaw.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{


    const std::string& ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ClassName()
    {
        static std::string name("CourtemancheRamirezNattel");
        return name;
    }


    double ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ReactionLawFunction(const double v,
                                                                                        const QuadraturePoint& quad_pt,
                                                                                        const GeometricElt& geom_elt)
    {
        const double R = 8.3143;
        const double temp = 310.;
        const double frdy = 96.4867;
        const double dt = GetTimeManager().GetTimeStep();

        // Compute INa: ina.
        const double gna = 7.8;
        const double old_value_nao = GetNonCstParameter(parameter_index::nao).GetValue(quad_pt, geom_elt);

        const double old_value_nai = GetNonCstParameter(parameter_index::nai).GetValue(quad_pt, geom_elt);

        const double ena = ((R * temp) / frdy) * std::log(old_value_nao / old_value_nai);
        double am = 3.2;

        if (std::fabs(v - 47.13) < NumericNS::DefaultEpsilon<double>())
            am = 0.32 * (v + 47.13) / (1. - std::exp(-0.1 * (v + 47.13)));

        const double bm = 0.08 * std::exp(-v / 11.);
        double ah, bh, aj, bj;

        if (v < -40.)
        {
            ah = 0.135 * std::exp((80. + v) / -6.8);
            bh = 3.56 * std::exp(0.079 * v) + 310000. * std::exp(0.35 * v);
            aj = (-127140. * std::exp(0.2444 * v) - 0.00003474 * std::exp(-0.04391 * v))
                 * ((v + 37.78) / (1. + std::exp(0.311 * (v + 79.23))));
            bj = (0.1212 * std::exp(-0.01052 * v)) / (1. + std::exp(-0.1378 * (v + 40.14)));
        } else
        {
            ah = 0.;
            bh = 1. / (0.13 * (1. + std::exp((v + 10.66) / -11.1)));
            aj = 0.;
            bj = (0.3 * std::exp(-0.0000002535 * v)) / (1. + std::exp(-0.1 * (v + 32.)));
        }

        auto update_function_h = [ah, dt, bh](double& value_h)
        {
            value_h = ah / (ah + bh) - ((ah / (ah + bh)) - value_h) * std::exp(-dt / (1. / (ah + bh)));
        };


        auto update_function_j = [aj, dt, bj](double& value_j)
        {
            const double aj_bj = 1. / (aj + bj);
            value_j = aj * aj_bj - ((aj * aj_bj) - value_j) * std::exp(-dt / aj_bj);
        };

        auto update_function_m = [am, dt, bm](double& value_m)
        {
            value_m = am / (am + bm) - ((am / (am + bm)) - value_m) * std::exp(-dt / (1. / (am + bm)));
        };


        const double new_value_h =
            GetNonCstParameter(parameter_index::h).UpdateAndGetValue(quad_pt, geom_elt, update_function_h);

        const double new_value_j =
            GetNonCstParameter(parameter_index::j).UpdateAndGetValue(quad_pt, geom_elt, update_function_j);

        const double new_value_m =
            GetNonCstParameter(parameter_index::m).UpdateAndGetValue(quad_pt, geom_elt, update_function_m);

        double ina = 0.;
        // #622 heterogeneity of parameters not handled for the moment.
        // if (!m_fsparam->hasHeteroCondAtria) {
        //    ina = gna*m_value_m*m_value_m*m_value_m*m_value_h*m_value_j*(v-ena)*m_courtCondMultCoeff[pos];
        //} else {
        ina = gna * new_value_m * new_value_m * new_value_m * new_value_h * new_value_j * (v - ena);
        // }

        // Compute ICaL: ical
        const double dss = 1. / (1. + std::exp(-(v + 10.) / 8.));
        const double taud =
            (1. - std::exp((v + 10.) / -6.24)) / (0.035 * (v + 10.) * (1. + std::exp((v + 10.) / -6.24)));
        const double fss = 1. / (1. + std::exp((v + 28.) / 6.9));
        const double tauf = 9. / (0.0197 * std::exp(-NumericNS::Square(0.0337 * (v + 10.))) + 0.02);
        const double old_value_cai = GetNonCstParameter(parameter_index::cai).GetValue(quad_pt, geom_elt);
        const double fcass = 1. / (1. + old_value_cai / 0.00035);
        const double taufca = 2.;

        auto update_function_d = [dt, dss, taud](double& value_d)
        {
            value_d = dss - (dss - value_d) * std::exp(-dt / taud);
        };

        auto update_function_f = [dt, fss, tauf](double& value_f)
        {
            value_f = fss - (fss - value_f) * std::exp(-dt / tauf);
        };

        auto update_function_fca = [dt, fcass, taufca](double& value_fca)
        {
            value_fca = fcass - (fcass - value_fca) * std::exp(-dt / taufca);
        };

        const double new_value_d =
            GetNonCstParameter(parameter_index::d).UpdateAndGetValue(quad_pt, geom_elt, update_function_d);

        const double new_value_f =
            GetNonCstParameter(parameter_index::f).UpdateAndGetValue(quad_pt, geom_elt, update_function_f);

        const double new_value_fca =
            GetNonCstParameter(parameter_index::fca).UpdateAndGetValue(quad_pt, geom_elt, update_function_fca);

        double gcalbar = 0.1238;
        // #622 heterogeneity of parameters not handled for the moment.
        // if (m_fsparam->hasHeteroCourtPar) {
        //    gcalbar = m_courtCondICaL[pos];
        //}

        const double ical = gcalbar * new_value_d * new_value_f * new_value_fca * (v - 65.);

        // Compute IKr: ikr
        const double gkr = 0.0294; //*sqrt(ko/5.4);
        const double old_value_ko = GetNonCstParameter(parameter_index::ko).GetValue(quad_pt, geom_elt);
        const double old_value_ki = GetNonCstParameter(parameter_index::ki).GetValue(quad_pt, geom_elt);
        const double ekr = ((R * temp) / frdy) * std::log(old_value_ko / old_value_ki);

        const double xrss = 1. / (1. + std::exp(-(v + 14.1) / 6.5));
        const double tauxr = 1.
                             / (0.0003 * (v + 14.1) / (1. - std::exp(-(v + 14.1) / 5.))
                                + 0.000073898 * (v - 3.3328) / (std::exp((v - 3.3328) / 5.1237) - 1.));

        auto update_function_xr = [dt, xrss, tauxr](double& value_xr)
        {
            value_xr = xrss - (xrss - value_xr) * std::exp(-dt / tauxr);
        };


        const double new_value_xr =
            GetNonCstParameter(parameter_index::xr).UpdateAndGetValue(quad_pt, geom_elt, update_function_xr);

        const double r = 1. / (1. + std::exp((v + 15.) / 22.4));

        const double ikr = gkr * new_value_xr * r * (v - ekr);

        // Compute IKs: iks
        const double gks = 0.129;
        const double eks = ((R * temp) / frdy) * std::log(old_value_ko / old_value_ki);
        const double axs = 4.0e-5 * (v - 19.9) / (1. - std::exp(-(v - 19.9) / 17.));
        const double bxs = 3.5e-5 * (v - 19.9) / (std::exp((v - 19.9) / 9.) - 1.);
        const double tauxs = 1. / (2. * (axs + bxs));
        const double xsss = 1. / std::pow((1. + std::exp(-(v - 19.9) / 12.7)), 0.5);

        auto update_function_xs = [dt, xsss, tauxs](double& value_xs)
        {
            value_xs = xsss - (xsss - value_xs) * std::exp(-dt / tauxs);
        };

        const double new_value_xs =
            GetNonCstParameter(parameter_index::xs).UpdateAndGetValue(quad_pt, geom_elt, update_function_xs);

        double iks = 0.0;
        // #622 heterogeneity of parameters not handled for the moment.
        // Warning ! The parameter of the CRN model gks becomes 5.0*gks in order to accelerate the atrial
        // repolarization.
        // if (m_fsparam->torsade) {
        //     if ( (m_fstransient->time > m_fsparam->torsadeTimeBegin) &&
        // (m_fstransient->time < m_fsparam->torsadeTimeEnd) ) {
        //         iks = gks*m_value_xs*m_value_xs*(v-eks);
        //     }
        // } else {
        iks = /*5.3.0*/ gks * new_value_xs * new_value_xs * (v - eks);
        // }

        // Compute IKi: iki
        const double gki = 0.09; //*pow(ko/5.4,0.4);
        const double eki = ((R * temp) / frdy) * std::log(old_value_ko / old_value_ki);

        const double kin = 1. / (1. + std::exp(0.07 * (v + 80.)));

        const double iki = gki * kin * (v - eki);

        // Compute Ikur: ikur
        const double gkur = 0.005 + 0.05 / (1 + std::exp(-(v - 15.) / 13.));
        const double ekur = ((R * temp) / frdy) * std::log(old_value_ko / old_value_ki);
        const double alphauakur = 0.65 / (std::exp(-(v + 10.) / 8.5) + std::exp(-(v - 30.) / 59.));
        const double betauakur = 0.65 / (2.5 + std::exp((v + 82.) / 17.));
        const double tauuakur = 1. / (3. * (alphauakur + betauakur));
        const double uakurss = 1. / (1. + std::exp(-(v + 30.3) / 9.6));
        const double alphauikur = 1. / (21. + std::exp(-(v - 185.) / 28.));
        const double betauikur = std::exp((v - 158.) / 16.);
        const double tauuikur = 1. / (3. * (alphauikur + betauikur));
        const double uikurss = 1. / (1. + std::exp((v - 99.45) / 27.48));

        auto update_function_ua = [dt, uakurss, tauuakur](double& value_ua)
        {
            value_ua = uakurss - (uakurss - value_ua) * std::exp(-dt / tauuakur);
        };

        auto update_function_ui = [dt, uikurss, tauuikur](double& value_ui)
        {
            value_ui = uikurss - (uikurss - value_ui) * std::exp(-dt / tauuikur);
        };

        const double new_value_ua =
            GetNonCstParameter(parameter_index::ua).UpdateAndGetValue(quad_pt, geom_elt, update_function_ua);

        const double new_value_ui =
            GetNonCstParameter(parameter_index::ui).UpdateAndGetValue(quad_pt, geom_elt, update_function_ui);

        const double ikur = gkur * new_value_ua * new_value_ua * new_value_ua * new_value_ui * (v - ekur);

        // Compute ITo: ito
        double gito = 0.1652;
        // #622 heterogeneity of parameters not handled for the moment.
        // if (m_fsparam->hasHeteroCourtPar) {
        //    gito = m_courtCondIto[pos];
        //}

        const double erevto = ((R * temp) / frdy) * std::log(old_value_ko / old_value_ki);

        const double alphaato = 0.65 / (std::exp(-(v + 10.) / 8.5) + std::exp(-(v - 30.) / 59.));
        const double betaato = 0.65 / (2.5 + std::exp((v + 82.) / 17.));
        const double tauato = 1. / (3. * (alphaato + betaato));
        const double atoss = 1. / (1. + std::exp(-(v + 20.47) / 17.54));

        auto update_function_ao = [dt, atoss, tauato](double& valuem_ao)
        {
            valuem_ao = atoss - (atoss - valuem_ao) * std::exp(-dt / tauato);
        };

        const double new_value_ao =
            GetNonCstParameter(parameter_index::ao).UpdateAndGetValue(quad_pt, geom_elt, update_function_ao);

        const double alphaiito = 1. / (18.53 + std::exp((v + 113.7) / 10.95));
        const double betaiito = 1. / (35.56 + std::exp(-(v + 1.26) / 7.44));
        const double tauiito = 1. / (3. * (alphaiito + betaiito));
        const double iitoss = 1. / (1. + std::exp((v + 43.1) / 5.3));

        auto update_function_io = [dt, iitoss, tauiito](double& value_io)
        {
            value_io = iitoss - (iitoss - value_io) * std::exp(-dt / tauiito);
        };

        const double new_value_io =
            GetNonCstParameter(parameter_index::io).UpdateAndGetValue(quad_pt, geom_elt, update_function_io);

        const double ito = gito * new_value_ao * new_value_ao * new_value_ao * new_value_io * (v - erevto);

        // Compute INaCa: inaca
        const double gammas = 0.35;
        const double kmnancx = 87.5;
        const double kmcancx = 1.38;
        const double ksatncx = 0.1;

        const double old_value_cao = GetNonCstParameter(parameter_index::cao).GetValue(quad_pt, geom_elt);

        const double inaca =
            1600.
            * (std::exp(gammas * frdy * v / (R * temp)) * old_value_nai * old_value_nai * old_value_nai * old_value_cao
               - std::exp((gammas - 1.) * frdy * v / (R * temp)) * old_value_nao * old_value_nao * old_value_nao
                     * old_value_cai)
            / ((NumericNS::Cube(kmnancx) + NumericNS::Cube(old_value_nao)) * (kmcancx + old_value_cao)
               * (1. + ksatncx * std::exp((gammas - 1.) * frdy * v / (R * temp))));

        // Compute INaK: inak
        const double sigma = (std::exp(old_value_nao / 67.3) - 1.) / 7.;
        const double ibarnak = 0.6;
        const double kmko = 1.5;
        const double kmnai = 10.;

        const double fnak = 1.
                            / (1. + 0.1245 * std::exp((-0.1 * v * frdy) / (R * temp))
                               + 0.0365 * sigma * std::exp((-v * frdy) / (R * temp)));

        const double inak = ibarnak * fnak * (1. / (1. + std::pow((kmnai / old_value_nai), 1.5)))
                            * (old_value_ko / (old_value_ko + kmko));

        // Compute IPCa: ipca
        const double ibarpca = 0.275;
        const double kmpca = 0.0005;

        const double ipca = (ibarpca * old_value_cai) / (kmpca + old_value_cai);

        // Compute IbCa: ibca
        const double gcab = 0.00113;
        const double ecan = ((R * temp) / frdy / 2.) * std::log(old_value_cao / old_value_cai);

        const double ibca = gcab * (v - ecan);

        // Compute IbNa: ibna
        const double gnab = 0.000674;
        const double enan = ((R * temp) / frdy) * std::log(old_value_nao / old_value_nai);

        const double ibna = gnab * (v - enan);

        // Compute ILeak: ileak
        const double caupmax = 15.;
        const double iupmax = 0.005;

        const double old_value_nsr = GetNonCstParameter(parameter_index::nsr).GetValue(quad_pt, geom_elt);

        GetNonCstParameter(parameter_index::ileak)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [iupmax, caupmax, old_value_nsr](double& value_ileak)
                         {
                             value_ileak = old_value_nsr * iupmax / caupmax;
                         });

        // Compute Iup: iup
        const double kup = 0.00092;

        GetNonCstParameter(parameter_index::iup)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [iupmax, kup, old_value_cai](double& value_iup)
                         {
                             value_iup = iupmax / (1. + (kup / old_value_cai));
                         });
        // Compute Itr: itr
        const double tautr = 180.;

        const double old_value_jsr = GetNonCstParameter(parameter_index::jsr).GetValue(quad_pt, geom_elt);

        GetNonCstParameter(parameter_index::itr)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [tautr, old_value_nsr, old_value_jsr](double& value_itr)
                         {
                             value_itr = (old_value_nsr - old_value_jsr) / tautr;
                         });

        // Compute Irel: irel
        const double krel = 30.;
        const double constvrel = 96.48;
        const double old_value_irel = GetNonCstParameter(parameter_index::irel).GetValue(quad_pt, geom_elt);
        const double fn = constvrel * (1.e-12) * old_value_irel - (5.e-13) * (ical / 2. - inaca / 5.) / frdy;

        const double tauurel = 8.;
        const double urelss = 1. / (1. + std::exp(-(fn - 3.4175e-13) / 13.67e-16));

        const double tauvrel = 1.91 + 2.09 / (1. + std::exp(-(fn - 3.4175e-13) / 13.67e-16));
        const double vrelss = 1. - 1. / (1. + std::exp(-(fn - 6.835e-14) / 13.67e-16));

        const double tauwrel =
            6. * (1. - std::exp(-(v - 7.9) / 5.)) / ((1. + 0.3 * std::exp(-(v - 7.9) / 5.)) * (v - 7.9));
        const double wrelss = 1. - 1. / (1. + std::exp(-(v - 40.) / 17.));

        auto update_function_urel = [dt, urelss, tauurel](double& value_urel)
        {
            value_urel = urelss - (urelss - value_urel) * std::exp(-dt / tauurel);
        };

        auto update_function_vrel = [dt, vrelss, tauvrel](double& value_vrel)
        {
            value_vrel = vrelss - (vrelss - value_vrel) * std::exp(-dt / tauvrel);
        };

        auto update_function_wrel = [dt, wrelss, tauwrel](double& value_wrel)
        {
            value_wrel = wrelss - (wrelss - value_wrel) * std::exp(-dt / tauwrel);
        };

        const double new_value_urel =
            GetNonCstParameter(parameter_index::urel).UpdateAndGetValue(quad_pt, geom_elt, update_function_urel);

        const double new_value_vrel =
            GetNonCstParameter(parameter_index::vrel).UpdateAndGetValue(quad_pt, geom_elt, update_function_vrel);

        const double new_value_wrel =
            GetNonCstParameter(parameter_index::wrel).UpdateAndGetValue(quad_pt, geom_elt, update_function_wrel);

        auto update_function_irel =
            [krel, new_value_urel, new_value_vrel, new_value_wrel, old_value_jsr, old_value_cai](double& value_irel)
        {
            value_irel = krel * new_value_urel * new_value_urel * new_value_vrel * new_value_wrel
                         * (old_value_jsr - old_value_cai);
        };


        GetNonCstParameter(parameter_index::irel).UpdateValue(quad_pt, geom_elt, update_function_irel);

        // Compute total current for Na:
        GetNonCstParameter(parameter_index::naiont)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [ina, ibna, inak, inaca](double& value_naiont)
                         {
                             value_naiont = ina + ibna + 3. * inak + 3. * inaca;
                         });

        // Compute total current for K:
        GetNonCstParameter(parameter_index::kiont)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [ikr, iks, iki, inak, ito, ikur](double& value_kiont)
                         {
                             value_kiont = ikr + iks + iki - 2. * inak + ito + ikur;
                         });

        // Compute total current for Ca:
        GetNonCstParameter(parameter_index::caiont)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [ical, ibca, ipca, inaca](double& value_caiont)
                         {
                             value_caiont = ical + ibca + ipca - 2. * inaca;
                         });

        const double value_ion = ical + ibca + ibna + inaca + ipca + inak + ikr + iks + ikur + ito + iki + ina;

        ComputeConcentrations(quad_pt, geom_elt);

        return value_ion;
    }


    void ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ComputeConcentrations(const QuadraturePoint& quad_pt,
                                                                                        const GeometricElt& geom_elt)
    {
        const double dt = GetTimeManager().GetTimeStep();
        const double frdy = 96.4867;
        const double vi = 13668.;

        // Update Na concentration
        const double value_naiont = GetNonCstParameter(parameter_index::naiont).GetValue(quad_pt, geom_elt);
        const double dnai = -dt * value_naiont / (vi * frdy);

        GetNonCstParameter(parameter_index::nai)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [dnai](double& value_nai)
                         {
                             value_nai = dnai + value_nai;
                         });

        // Update K concentration
        const double value_kiont = GetNonCstParameter(parameter_index::kiont).GetValue(quad_pt, geom_elt);
        const double dki = -dt * value_kiont / (vi * frdy);
        GetNonCstParameter(parameter_index::ki)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [dki](double& value_ki)
                         {
                             value_ki = dki + value_ki;
                         });

        // Update Ca concentration
        const double vup = 1109.52;
        const double constvrel = 96.48;

        const double value_ileak = GetNonCstParameter(parameter_index::ileak).GetValue(quad_pt, geom_elt);
        const double value_iup = GetNonCstParameter(parameter_index::iup).GetValue(quad_pt, geom_elt);

        const double value_irel = GetNonCstParameter(parameter_index::irel).GetValue(quad_pt, geom_elt);

        const double value_caiont = GetNonCstParameter(parameter_index::caiont).GetValue(quad_pt, geom_elt);
        const double b1cai =
            -value_caiont / (2. * frdy * vi) + (vup * (value_ileak - value_iup) + constvrel * value_irel) / vi;

        const double cmdnmax = 0.05;
        const double trpnmax = 0.07;
        const double kmcmdn = 0.00238;
        const double kmtrpn = 0.0005;
        const double old_value_cai = GetNonCstParameter(parameter_index::cai).GetValue(quad_pt, geom_elt);
        const double b2cai = 1. + trpnmax * kmtrpn / NumericNS::Square((old_value_cai + kmtrpn))
                             + cmdnmax * kmcmdn / NumericNS::Square((old_value_cai + kmcmdn));

        const double dcai = dt * b1cai / b2cai;

        GetNonCstParameter(parameter_index::cai)
            .UpdateValue(quad_pt,
                         geom_elt,
                         [dcai](double& value_cai)
                         {
                             value_cai = dcai + value_cai;
                         });
    }


    void ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::WriteGate(const std::string& filename) const
    {
        GetParameter(parameter_index::m).Write(filename);
    }


} // namespace MoReFEM::Advanced::ReactionLawNS


/// @} // addtogroup OperatorInstancesGroup
