/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_FITZ_HUGH_NAGUMO_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_FITZ_HUGH_NAGUMO_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/FitzHughNagumo.hpp"

#include <cassert>
#include <memory>

#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
//#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class TimeManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {


            template<class InputDataT>
            ReactionLaw<ReactionLawName::FitzHughNagumo>::ReactionLaw(
                const InputDataT& input_data,
                const Domain& domain,
                const TimeManager& time_manager,
                const QuadratureRulePerTopology& default_quadrature_rule_set)
            : a_(Utilities::InputDataNS::Extract<input_data_fhn::ACoefficient>::Value(input_data)),
              b_(Utilities::InputDataNS::Extract<input_data_fhn::BCoefficient>::Value(input_data)),
              c_(Utilities::InputDataNS::Extract<input_data_fhn::CCoefficient>::Value(input_data)),
              time_manager_(time_manager)
            {
                using InitialConditionGate = ::MoReFEM::InputDataNS::InitialConditionGate;
                namespace ipl = Utilities::InputDataNS;


                const double initial_condition_gate = ipl::Extract<InitialConditionGate::Value>::Value(input_data);

                gate_ = std::make_unique<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>>(
                    "Gate", domain, default_quadrature_rule_set, initial_condition_gate, this->GetTimeManager());
            }


            inline double ReactionLaw<ReactionLawName::FitzHughNagumo>::GetLocalPotential() const noexcept
            {
                return local_potential_;
            }


            inline double& ReactionLaw<ReactionLawName::FitzHughNagumo>::GetNonCstLocalPotential() noexcept
            {
                return local_potential_;
            }


            inline const TimeManager& ReactionLaw<ReactionLawName::FitzHughNagumo>::GetTimeManager() const noexcept
            {
                return time_manager_;
            }

            inline double ReactionLaw<ReactionLawName::FitzHughNagumo>::GetA() const noexcept
            {
                return a_;
            }


            inline double ReactionLaw<ReactionLawName::FitzHughNagumo>::GetB() const noexcept
            {
                return b_;
            }


            inline double ReactionLaw<ReactionLawName::FitzHughNagumo>::GetC() const noexcept
            {
                return c_;
            }

            inline ReactionLaw<ReactionLawName::FitzHughNagumo>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::FitzHughNagumo>::GetNonCstGate() noexcept
            {
                return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(this->GetGate());
            }


            inline const ReactionLaw<ReactionLawName::FitzHughNagumo>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::FitzHughNagumo>::GetGate() const noexcept
            {
                assert(!(!gate_));
                return *gate_;
            }


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_FITZ_HUGH_NAGUMO_HXX_
