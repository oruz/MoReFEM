/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HPP_


#include <iosfwd>
#include <limits>
#include <memory>

#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/ReactionLaw.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class TimeManager; }
namespace MoReFEM::Advanced::ReactionLawNS { template <ReactionLawName> class ReactionLaw; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {

            /*!
             * \brief Implementation of the MitchellSchaeffer reaction law. Defines f and g for a ReactionDiffusion
             * model.
             */
            template<>
            class ReactionLaw<ReactionLawName::MitchellSchaeffer>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = ReactionLaw;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the reaction law.
                static const std::string& ClassName();

              private:
                //! Alias to the matching input parameter.
                using input_data_ms = ::MoReFEM::InputDataNS::ReactionNS::MitchellSchaeffer;

                //! Alias to a scalar parameter at quadrature point.
                using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<
                    ParameterNS::Type::scalar,
                    ParameterNS::TimeDependencyNS::None // There is a time dependency but it is much more sophisticated
                                                        // that what this template parameter is able to cope with,
                                                        // namely only something like f(x) * g(t).
                    >;

                //! Alias to a scalar parameter.
                using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_input_data_arg
                 * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
                 * \param[in] domain \a Domain upon which the \a Parameters are defined.
                 * \copydoc doxygen_hide_time_manager_arg
                 */
                template<class InputDataT>
                explicit ReactionLaw(const InputDataT& input_data,
                                     const Domain& domain,
                                     const TimeManager& time_manager,
                                     const QuadratureRulePerTopology& quadrature_rule_per_topology);

                //! Destructor.
                ~ReactionLaw() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ReactionLaw(const ReactionLaw& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ReactionLaw(ReactionLaw&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ReactionLaw& operator=(const ReactionLaw& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ReactionLaw& operator=(ReactionLaw&& rhs) = delete;

                ///@}

                /*!
                 *
                 * \brief Defines f(u,w) = w(u-u_min)^2(u_max-u)/(tau_in(u_max-u_min)) - (u-u_min)/(tau_out(u_max -
                 * u_min)) See NonLinearSource
                 *
                 * \todo #9 (Gautier) Improve this comment!
                 *
                 * \param[in] local_potential Local potential.
                 * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
                 * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
                 *
                 * \return Value of the reaction law at the requested location.
                 */
                double ReactionLawFunction(const double local_potential,
                                           const QuadraturePoint& quad_pt,
                                           const GeometricElt& geom_elt);

                //! Accessor to the local potential.
                double GetLocalPotential() const noexcept;

                //! Non constant accessor to the local potential.
                double& GetNonCstLocalPotential() noexcept;

                /*!
                 * \brief Write gate into a file.
                 *
                 * \param[in] filename Path of the file into which gate must be written.
                 */
                void WriteGate(const std::string& filename) const;

                //! Non constant accessor the the gate \a Parameter.
                ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetNonCstGate() noexcept;

                //! Accessor the the gate \a Parameter.
                const ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetGate() const noexcept;

              private:
                /*!
                 * \brief Defines g = (1/(u_max-u_min)^2 - w)/tau_open if u < u_gate
                 *                                       - w/tau_close if u > u_gate
                 * See NonLinearSource
                 *
                 * \copydoc doxygen_hide_reaction_diffusion_gate_law_function_args
                 */
                void GateLawFunction(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt, double& gate) const;

                //! Accessor to the \a TimeManager.
                const TimeManager& GetTimeManager() const noexcept;

                //! tau_in.
                // .
                // \todo #9 Improve this comment (Gautier)
                double GetTauIn() const noexcept;

                //! Tau out .
                // \todo #9 Improve this comment (Gautier)
                double GetTauOut() const noexcept;

                //! Tau open .
                // \todo #9 Improve this comment (Gautier)
                double GetTauOpen() const noexcept;

                //! Tau close .
                // \todo #9 Improve this comment (Gautier)
                const scalar_parameter& GetTauClose() const;

                //! Ugate .
                // \todo #9 Improve this comment (Gautier)
                double GetUGate() const noexcept;

                //! Umin .
                // \todo #9 Improve this comment (Gautier)
                double GetUMin() const noexcept;

                //! Umax .
                // \todo #9 Improve this comment (Gautier)
                double GetUMax() const noexcept;


              private:
                //! Gate.
                ParameterAtQuadraturePoint<ParameterNS::Type::scalar>::unique_ptr gate_ = nullptr;

              private:
                //! \name MitchellSchaeffer parameters.
                ///@{

                //! tau_in .
                // \todo #9 Improve this comment (Gautier)
                const double tau_in_;

                //! tau_out_ .
                // \todo #9 Improve this comment (Gautier)
                const double tau_out_;

                //! tau_open_ .
                // \todo #9 Improve this comment (Gautier)
                const double tau_open_;

                //! tau_close_ .
                // \todo #9 Improve this comment (Gautier)
                scalar_parameter::unique_ptr tau_close_ = nullptr;

                //! u_gate_ .
                // \todo #9 Improve this comment (Gautier)
                const double u_gate_;

                //! u_min_ .
                // \todo #9 Improve this comment (Gautier)
                const double u_min_;

                //! u_max_ .
                // \todo #9 Improve this comment (Gautier)
                const double u_max_;

                ///@}

                //! \a TimeManager of the model.
                const TimeManager& time_manager_;

                //! Local potential.
                double local_potential_ = std::numeric_limits<double>::lowest();
            };


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/MitchellSchaeffer.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HPP_
