/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 5 Oct 2015 10:59:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/CourtemancheRamirezNattel.hpp"

#include <array>
#include <cassert>
#include <memory>

#include "Utilities/Containers/EnumClass.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class TimeManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {

            template<class InputDataT>
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ReactionLaw(
                const InputDataT& input_data,
                const Domain& domain,
                const TimeManager& time_manager,
                const QuadratureRulePerTopology& default_quadrature_rule_set)
            : time_manager_(time_manager)
            {
                static_cast<void>(input_data); // avoid warning in release mode.

                constexpr const double value_at_quad_m = 0.00291;
                constexpr const double value_at_quad_h = 0.965;
                constexpr const double value_at_quad_j = 0.978;
                constexpr const double value_at_quad_ao = 0.0304;
                constexpr const double value_at_quad_io = 0.999;
                constexpr const double value_at_quad_ua = 0.00496;
                constexpr const double value_at_quad_ui = 0.999;
                constexpr const double value_at_quad_xr = 0.0000329;
                constexpr const double value_at_quad_xs = 0.0187;
                constexpr const double value_at_quad_d = 0.000137;
                constexpr const double value_at_quad_f = 0.999837;
                constexpr const double value_at_quad_fca = 0.775;
                constexpr const double value_at_quad_urel = 0.;
                constexpr const double value_at_quad_vrel = 1.00;
                constexpr const double value_at_quad_wrel = 0.999;
                constexpr const double value_at_quad_nai = 11.2;
                constexpr const double value_at_quad_nao = 140.0;
                constexpr const double value_at_quad_cao = 1.8;
                constexpr const double value_at_quad_ki = 139.0;
                constexpr const double value_at_quad_ko = 4.5;
                constexpr const double value_at_quad_cai = 0.000102;
                constexpr const double value_at_quad_naiont = 0.;
                constexpr const double value_at_quad_kiont = 0.;
                constexpr const double value_at_quad_caiont = 0.;
                constexpr const double value_at_quad_ileak = 0.;
                constexpr const double value_at_quad_iup = 0.;
                constexpr const double value_at_quad_itr = 0.;
                constexpr const double value_at_quad_irel = 0.;
                constexpr const double value_at_quad_nsr = 1.49;
                constexpr const double value_at_quad_jsr = 1.49;


                parameter_list_[EnumUnderlyingType(parameter_index::m)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "m", domain, default_quadrature_rule_set, value_at_quad_m, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::h)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "h", domain, default_quadrature_rule_set, value_at_quad_h, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::j)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "j", domain, default_quadrature_rule_set, value_at_quad_j, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ao)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ao", domain, default_quadrature_rule_set, value_at_quad_ao, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::io)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "io", domain, default_quadrature_rule_set, value_at_quad_io, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ua)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ua", domain, default_quadrature_rule_set, value_at_quad_ua, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ui)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ui", domain, default_quadrature_rule_set, value_at_quad_ui, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::xr)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "xr", domain, default_quadrature_rule_set, value_at_quad_xr, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::xs)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "xs", domain, default_quadrature_rule_set, value_at_quad_xs, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::d)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "d", domain, default_quadrature_rule_set, value_at_quad_d, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::f)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "f", domain, default_quadrature_rule_set, value_at_quad_f, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::fca)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "fca", domain, default_quadrature_rule_set, value_at_quad_fca, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::urel)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "urel", domain, default_quadrature_rule_set, value_at_quad_urel, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::vrel)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "vrel", domain, default_quadrature_rule_set, value_at_quad_vrel, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::wrel)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "wrel", domain, default_quadrature_rule_set, value_at_quad_wrel, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::nai)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "nai", domain, default_quadrature_rule_set, value_at_quad_nai, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::nao)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "nao", domain, default_quadrature_rule_set, value_at_quad_nao, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::cao)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "cao", domain, default_quadrature_rule_set, value_at_quad_cao, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ki)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ki", domain, default_quadrature_rule_set, value_at_quad_ki, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ko)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ko", domain, default_quadrature_rule_set, value_at_quad_ko, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::cai)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "cai", domain, default_quadrature_rule_set, value_at_quad_cai, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::naiont)] =
                    std::make_unique<ScalarParameterAtQuadPt>(
                        "naiont", domain, default_quadrature_rule_set, value_at_quad_naiont, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::kiont)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "kiont", domain, default_quadrature_rule_set, value_at_quad_kiont, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::caiont)] =
                    std::make_unique<ScalarParameterAtQuadPt>(
                        "caiont", domain, default_quadrature_rule_set, value_at_quad_caiont, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::ileak)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "ileak", domain, default_quadrature_rule_set, value_at_quad_ileak, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::iup)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "iup", domain, default_quadrature_rule_set, value_at_quad_iup, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::itr)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "itr", domain, default_quadrature_rule_set, value_at_quad_itr, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::irel)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "irel", domain, default_quadrature_rule_set, value_at_quad_irel, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::nsr)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "nsr", domain, default_quadrature_rule_set, value_at_quad_nsr, this->GetTimeManager());

                parameter_list_[EnumUnderlyingType(parameter_index::jsr)] = std::make_unique<ScalarParameterAtQuadPt>(
                    "jsr", domain, default_quadrature_rule_set, value_at_quad_jsr, this->GetTimeManager());
            }


            inline ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetNonCstParameter(parameter_index index) noexcept
            {
                return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(GetParameter(index));
            }


            inline const ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetParameter(parameter_index index) const noexcept
            {
                assert(EnumUnderlyingType(index) < parameter_list_.size());
                assert(!(!parameter_list_[EnumUnderlyingType(index)]));
                return *parameter_list_[EnumUnderlyingType(index)];
            }


            inline double ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetLocalPotential() const noexcept
            {
                return local_potential_;
            }


            inline double& ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetNonCstLocalPotential() noexcept
            {
                return local_potential_;
            }


            inline const TimeManager&
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetTimeManager() const noexcept
            {
                return time_manager_;
            }


            inline ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetNonCstGate() noexcept
            {
                return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(this->GetGate());
            }


            inline const ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::ScalarParameterAtQuadPt&
            ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>::GetGate() const noexcept
            {
                return GetParameter(parameter_index::m);
            }


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HXX_
