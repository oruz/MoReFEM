### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FwdForHpp.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NonLinearSource.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NonLinearSource.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/NonLinearSource/SourceList.cmake)
