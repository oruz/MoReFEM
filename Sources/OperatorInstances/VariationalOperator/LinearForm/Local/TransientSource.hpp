/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_
//
//#include <memory>
//#include <vector>
//
//#include "Core/InputData/InputData.hpp"
//
//#include "Core/Parameter/TypeEnum.hpp"
//#include "Parameters/Parameter.hpp"
//
//#include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Implementation of local TransientSource operator.
             *
             * \todo Improve the comment by writing its mathematical definition!
             */
            // clang-format off
            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            // clang-format on
            class TransientSource final : public LinearLocalVariationalOperator<LocalVector>
            {


              public:
                //! \copydoc doxygen_hide_alias_self
                using self = TransientSource<TypeT, TimeDependencyT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to parameter type.
                using parameter_type = Parameter<TypeT, LocalCoords, TimeDependencyT>;

                //! Alias to parent.
                using parent = LinearLocalVariationalOperator<LocalVector>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
                 * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
                 * hold exactly one vectorial unknown. \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] source Lua function describing the source applied.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit TransientSource(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                         const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                         typename parent::elementary_data_type&& elementary_data,
                                         const parameter_type& source);


                //! Destructor.
                virtual ~TransientSource() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                TransientSource(const TransientSource& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                TransientSource(TransientSource&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                TransientSource& operator=(const TransientSource& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                TransientSource& operator=(TransientSource&& rhs) = delete;

                ///@}


                /*!
                 * \brief Compute the elementary \a OperatorNatureT.
                 *
                 * For current operator, only vector makes sense; so only this specialization is actually defined.
                 *
                 * \internal <b><tt>[internal]</tt></b> This parameter is computed by
                 * GlobalVariationalOperatorNS::TransientSource::SetComputeEltArrayArguments() method.
                 * \endinternal
                 *
                 */
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }


                //! Set the time.
                //! \param[in] time New time.
                void SetTime(double time) noexcept;


              private:
                /*!
                 * \brief Access to the transient source parameter.
                 */
                const parameter_type& GetSource() const;


              private:
                //! Source  considered.
                const parameter_type& source_;

                //! Current time in seconds.
                double time_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/LinearForm/Local/TransientSource.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_
