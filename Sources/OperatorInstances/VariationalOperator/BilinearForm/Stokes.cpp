/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 May 2015 10:21:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        Stokes::Stokes(const FEltSpace& felt_space,
                       const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                       const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                       const scalar_parameter& fluid_viscosity,
                       const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 fluid_viscosity)
        {
            assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

            assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
        }


        const std::string& Stokes::ClassName()
        {
            static std::string name("Stokes");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
