//! \file
//
//
//  FwdForHpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 02/02/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_FWD_FOR_HPP_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_FWD_FOR_HPP_HPP_

#include <array>  // IWYU pragma: export
#include <iosfwd> // IWYU pragma: export
#include <memory> // IWYU pragma: export
#include <tuple>  // IWYU pragma: export

#include "Core/Parameter/FiberEnum.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp"  // IWYU pragma: export

#include "Geometry/Domain/Domain.hpp"                          // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: export

#include "Operators/Enum.hpp"                                                      // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"       // IWYU pragma: export
#include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp" // IWYU pragma: export

// namespace MoReFEM { class GlobalVector; } // export not supported yet by IWYU

#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_FWD_FOR_HPP_HPP_
