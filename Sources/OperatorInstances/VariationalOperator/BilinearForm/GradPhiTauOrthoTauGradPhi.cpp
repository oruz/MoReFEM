/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 25 Jan 2017 10:21:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauOrthoTauGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    GradPhiTauOrthoTauGradPhi ::GradPhiTauOrthoTauGradPhi(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr unknown_ptr,
        const Unknown::const_shared_ptr test_unknown_ptr,
        const scalar_parameter& transverse_diffusion_tensor,
        const scalar_parameter& fiber_diffusion_tensor,
        const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers,
        const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>& angles,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)

    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             std::move(quadrature_rule_per_topology),
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::no,
             transverse_diffusion_tensor,
             fiber_diffusion_tensor,
             fibers,
             angles)
    { }


    const std::string& GradPhiTauOrthoTauGradPhi::ClassName()
    {
        static std::string name("GradPhiTauOrthoTauGradPhi");
        return name;
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup
