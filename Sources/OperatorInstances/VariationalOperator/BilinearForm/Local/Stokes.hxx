/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_STOKES_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_STOKES_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const Stokes::scalar_parameter& Stokes::GetFluidViscosity() const
            {
                return fluid_viscosity_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_STOKES_HXX_
