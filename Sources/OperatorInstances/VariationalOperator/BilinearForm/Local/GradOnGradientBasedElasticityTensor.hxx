/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 09:39:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp"

#include <cassert>
// IWYU pragma: no_include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline GradOnGradientBasedElasticityTensor::matrix_parameter&
            GradOnGradientBasedElasticityTensor::GetNonCstGradientBasedElasticityTensor()
            {
                assert(!(!gradient_based_elasticity_tensor_parameter_));
                return *gradient_based_elasticity_tensor_parameter_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
