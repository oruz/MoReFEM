/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hpp"


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const SurfacicBidomain::scalar_parameter&
            SurfacicBidomain ::GetIntracelluarTransDiffusionTensor() const noexcept
            {
                return intracellular_trans_diffusion_tensor_;
            }

            inline const SurfacicBidomain::scalar_parameter&
            SurfacicBidomain ::GetExtracelluarTransDiffusionTensor() const noexcept
            {
                return extracellular_trans_diffusion_tensor_;
            }


            inline const SurfacicBidomain::scalar_parameter&
            SurfacicBidomain ::GetIntracelluarFiberDiffusionTensor() const noexcept
            {
                return intracellular_fiber_diffusion_tensor_;
            }

            inline const SurfacicBidomain::scalar_parameter&
            SurfacicBidomain ::GetExtracelluarFiberDiffusionTensor() const noexcept
            {
                return extracellular_fiber_diffusion_tensor_;
            }

            inline const SurfacicBidomain::scalar_parameter&
            SurfacicBidomain ::GetHeterogeneousConductivityCoefficient() const noexcept
            {
                return heterogeneous_conductivity_coefficient_;
            }

            inline const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>&
            SurfacicBidomain::GetFibers() const noexcept
            {
                return fibers_;
            }

            inline const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>&
            SurfacicBidomain::GetAngles() const noexcept
            {
                return angles_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HXX_
