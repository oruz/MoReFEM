/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HPP_

#include <vector>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Local operator for \a Ale.
             */
            class Ale final : public BilinearLocalVariationalOperator<LocalMatrix>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = Ale;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Convenient alias.
                using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operator. Its type (vector_shared_ptr)
                 * is due to constraints from genericity; for current operator it is expected to hold exactly
                 * two unknowns (the first one vectorial and the second one scalar).
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] density Density as a \a Parameter.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit Ale(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                             const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                             elementary_data_type&& elementary_data,
                             const scalar_parameter& density);

                //! Destructor.
                virtual ~Ale() override;

                //! \copydoc doxygen_hide_copy_constructor
                Ale(const Ale& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Ale(Ale&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Ale& operator=(const Ale& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Ale& operator=(Ale&& rhs) = delete;

                ///@}


                //! Compute the elementary vector.
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }


                //! Constant accessor to the former local velocity required by ComputeEltArray().
                const std::vector<double>& GetFormerLocalVelocity() const noexcept;

                //! Non constant accessor to the former local velocity required by ComputeEltArray().
                std::vector<double>& GetNonCstFormerLocalVelocity() noexcept;


              private:
                //! Access to the density.
                const scalar_parameter& GetDensity() const noexcept;

              private:
                //! Density.
                const scalar_parameter& density_;

                /*!
                 * \brief Velocity obtained at previous time iteration expressed at the local level.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
                 * ComputeEltArray.
                 * \endinternal
                 */
                std::vector<double> former_local_velocity_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HPP_
