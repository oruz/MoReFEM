/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 25 Jan 2017 10:21:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HPP_

#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Local operator for \a GradPhiTauTauGradPhi.
             */
            class GradPhiTauTauGradPhi final : public BilinearLocalVariationalOperator<LocalMatrix>,
                                               public Crtp::LocalMatrixStorage<GradPhiTauTauGradPhi, 4ul>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = GradPhiTauTauGradPhi;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Convenient typedef.
                using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent = Crtp::LocalMatrixStorage<self, 4ul>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
                 * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
                 * hold exactly two unknowns (the first one vectorial and the second one scalar).
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] transverse_diffusion_tensor Transverse diffusion tensor.
                 * \param[in] fiber_diffusion_tensor Fiber diffusion tensor.
                 * \param[in] fibers Vectorial fibers.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit GradPhiTauTauGradPhi(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data,
                    const scalar_parameter& transverse_diffusion_tensor,
                    const scalar_parameter& fiber_diffusion_tensor,
                    const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers);

                //! Destructor.
                virtual ~GradPhiTauTauGradPhi() override;

                //! \copydoc doxygen_hide_copy_constructor
                GradPhiTauTauGradPhi(const GradPhiTauTauGradPhi& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GradPhiTauTauGradPhi(GradPhiTauTauGradPhi&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GradPhiTauTauGradPhi& operator=(const GradPhiTauTauGradPhi& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GradPhiTauTauGradPhi& operator=(GradPhiTauTauGradPhi&& rhs) = delete;

                ///@}


                //! Compute the elementary vector.
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }


              private:
                //! Get the transverse diffusion tensor.
                const scalar_parameter& GetTransverseDiffusionTensor() const noexcept;

                //! Get the fiber diffusion tensor.
                const scalar_parameter& GetFiberDiffusionTensor() const noexcept;

                //! Get the fiber.
                const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>&
                GetFibers() const noexcept;

              private:
                //! \name Material parameters.
                ///@{

                //! First diffusion tensor = sigma_t.
                const scalar_parameter& transverse_diffusion_tensor_;

                //! Second diffusion tensor = sigma_l. (longitudinal)
                const scalar_parameter& fiber_diffusion_tensor_;

                //! Fibers parameter.
                const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers_;

                ///@}

              private:
                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Indexes of local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    block_matrix = 0,
                    transposed_dphi,
                    dphi_test_sigma,
                    tau_sigma
                };

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauTauGradPhi.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HPP_
