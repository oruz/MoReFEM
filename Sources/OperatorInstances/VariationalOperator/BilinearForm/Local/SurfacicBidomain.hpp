/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_

#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a SurfacicBidomain.
     */
    class SurfacicBidomain final
    // clang-format off
    : public BilinearLocalVariationalOperator<LocalMatrix>,
    public Crtp::LocalMatrixStorage<SurfacicBidomain, 18u>,
    public Crtp::LocalVectorStorage<SurfacicBidomain, 3ul>
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SurfacicBidomain;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Convenient alias.
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 18ul>;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] intracellular_trans_diffusion_tensor Intracellular trans diffusion tensor.
         * \param[in] extracellular_trans_diffusion_tensor Extracellular trans diffusion tensor.
         * \param[in] intracellular_fiber_diffusion_tensor Intracellular fiber diffusion tensor.
         * \param[in] extracellular_fiber_diffusion_tensor Extracellular fiber diffusion tensor.
         * \param[in] heterogeneous_conductivity_coefficient Heterogeneous conductivity coefficient.
         * \param[in] fibers Fibers.
         * \param[in] angles Angles. \todo #9 Should be completed.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
         * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit SurfacicBidomain(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data,
            const scalar_parameter& intracellular_trans_diffusion_tensor,
            const scalar_parameter& extracellular_trans_diffusion_tensor,
            const scalar_parameter& intracellular_fiber_diffusion_tensor,
            const scalar_parameter& extracellular_fiber_diffusion_tensor,
            const scalar_parameter& heterogeneous_conductivity_coefficient,
            const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers,
            const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>& angles);

        //! Destructor.
        virtual ~SurfacicBidomain() override;

        //! \copydoc doxygen_hide_copy_constructor
        SurfacicBidomain(const SurfacicBidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SurfacicBidomain(SurfacicBidomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SurfacicBidomain& operator=(const SurfacicBidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SurfacicBidomain& operator=(SurfacicBidomain&& rhs) = delete;

        ///@}

        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        /*!
         * \brief Compute contravariant basis.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         */
        void ComputeContravariantBasis(
            const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data);


      private:
        //! Get the first diffusion tensor.
        const scalar_parameter& GetIntracelluarTransDiffusionTensor() const noexcept;

        //! Get the second diffusion tensor.
        const scalar_parameter& GetExtracelluarTransDiffusionTensor() const noexcept;

        //! Get the thrid diffusion tensor.
        const scalar_parameter& GetIntracelluarFiberDiffusionTensor() const noexcept;

        //! Get the fourth diffusion tensor.
        const scalar_parameter& GetExtracelluarFiberDiffusionTensor() const noexcept;

        //! Get the coefficient for Heterogeneous Conductivity.
        const scalar_parameter& GetHeterogeneousConductivityCoefficient() const noexcept;

        //! Get the fiber.
        const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>&
        GetFibers() const noexcept;

        //! Get the angles.
        const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>&
        GetAngles() const noexcept;

      private:
        //! \name Material parameters.
        ///@{

        //! First diffusion tensor = sigma_i_t.
        const scalar_parameter& intracellular_trans_diffusion_tensor_;

        //! Second diffusion tensor = sigma_e_t.
        const scalar_parameter& extracellular_trans_diffusion_tensor_;

        //! Third diffusion tensor = sigma_i_l.
        const scalar_parameter& intracellular_fiber_diffusion_tensor_;

        //! Fourth diffusion tensor = sigma_e_l.
        const scalar_parameter& extracellular_fiber_diffusion_tensor_;

        //! Coefficient for heterogeneous_conductivity.
        const scalar_parameter& heterogeneous_conductivity_coefficient_;

        //! Fibers parameter.
        const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers_;

        //! Angles parameter.
        const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>& angles_;

        ///@}

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t
        {
            block_matrix1 = 0,
            block_matrix2,
            block_matrix3,
            block_matrix4,
            transposed_dphi,
            transposed_dpsi,
            dphi_test_sigma,
            dpsi_test_sigma,
            dphi_test_contravariant_metric_tensor,
            dpsi_test_contravariant_metric_tensor,
            tau_sigma,
            tau_ortho_sigma,
            covariant_basis,
            contravariant_basis,
            transposed_covariant_basis,
            covariant_metric_tensor,
            contravariant_metric_tensor,
            reduced_contravariant_metric_tensor
        };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t
        {
            tau_interpolate_ortho = 0,
            tau_covariant_basis,
            tau_ortho_covariant_basis
        };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_
