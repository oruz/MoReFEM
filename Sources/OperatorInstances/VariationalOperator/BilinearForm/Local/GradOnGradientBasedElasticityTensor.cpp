/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 09:39:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp" // IWYU pragma: keep
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            GradOnGradientBasedElasticityTensor ::GradOnGradientBasedElasticityTensor(
                const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                elementary_data_type&& a_elementary_data,
                const scalar_parameter& young_modulus,
                const scalar_parameter& poisson_ratio,
                const ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
            : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)),
              matrix_parent()
            {
                assert(unknown_list.size() == 1);
                assert(test_unknown_list.size() == 1);

                const auto& elementary_data = GetElementaryData();

                gradient_based_elasticity_tensor_parameter_ =
                    Internal::LocalVariationalOperatorNS::InitGradientBasedElasticityTensor(
                        young_modulus, poisson_ratio, configuration);

                const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

                const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
                const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

                const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

                InitLocalMatrixStorage({ {
                    { felt_space_dimension, felt_space_dimension },  // gradient_based_block
                    { Nnode_for_test_unknown, Nnode_for_unknown },   // block_contribution
                    { felt_space_dimension, Nnode_for_unknown },     // transposed dphi
                    { Nnode_for_test_unknown, felt_space_dimension } // dphi_test x gradient_based_block
                } });
            }


            GradOnGradientBasedElasticityTensor::~GradOnGradientBasedElasticityTensor() = default;


            const std::string& GradOnGradientBasedElasticityTensor::ClassName()
            {
                static std::string name("GradOnGradientBasedElasticityTensor");
                return name;
            }


            void GradOnGradientBasedElasticityTensor::ComputeEltArray()
            {
                auto& elementary_data = GetNonCstElementaryData();

                auto& matrix_result = elementary_data.GetNonCstMatrixResult();

                auto& gradient_based_block =
                    GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
                auto& block_contribution = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
                auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
                auto& dphi_test_mult_gradient_based_block =
                    GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_mult_gradient_based_block)>();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

                const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

                const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
                const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

                const auto Ncomponent = unknown_ref_felt.Ncomponent();

                assert(Ncomponent == test_unknown_ref_felt.Ncomponent());

                const auto& geom_elt = elementary_data.GetCurrentGeomElt();

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                    decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    const auto& gradient_based_elasticity_tensor =
                        GetNonCstGradientBasedElasticityTensor().GetValue(quad_pt, geom_elt);

                    const double factor =
                        quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant() * quad_pt.GetWeight();

                    const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();

                    const auto& dphi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

                    xt::noalias(transposed_dphi) = xt::transpose(dphi);


                    for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
                    {
                        const auto row_first_index =
                            test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                        for (ComponentNS::index_type col_component{ 0ul }; col_component < Ncomponent; ++col_component)
                        {
                            const auto col_first_index =
                                unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                            Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(
                                gradient_based_elasticity_tensor, row_component, col_component, gradient_based_block);

                            xt::noalias(dphi_test_mult_gradient_based_block) =
                                xt::linalg::dot(dphi_test, gradient_based_block);

                            xt::noalias(block_contribution) =
                                factor * xt::linalg::dot(dphi_test_mult_gradient_based_block, transposed_dphi);

                            for (auto row_node = 0ul; row_node < Nnode_for_test_unknown; ++row_node)
                            {
                                for (auto col_node = 0ul; col_node < Nnode_for_unknown; ++col_node)
                                    matrix_result(row_first_index + row_node, col_first_index + col_node) +=
                                        block_contribution(row_node, col_node);
                            }
                        }
                    }
                }
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
