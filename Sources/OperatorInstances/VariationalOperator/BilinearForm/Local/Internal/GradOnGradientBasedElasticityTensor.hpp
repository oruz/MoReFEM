/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 15:58:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_INTERNAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_INTERNAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_


#include "Geometry/Coords/LocalCoords.hpp"

#include "Parameters/Internal/Alias.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace ParameterNS
    {


        enum class GradientBasedElasticityTensorConfiguration;


    } // namespace ParameterNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Init the gradient based elasticity tensor depending on the configuration chosen in the input
             * settings.
             *
             * \param[in] young_modulus Young's modulus
             * \param[in] poisson_ratio Poisson ratio
             * \param[in] configuration Configuration to use: for instance in 2D whether we're considering
             * plane stress or plane strain.
             */
            // clang-format off
            Parameter
            <
                ParameterNS::Type::matrix,
                LocalCoords,
                ::MoReFEM::ParameterNS::TimeDependencyNS::None
            >::unique_ptr
                // clang-format on
                InitGradientBasedElasticityTensor(
                    const ScalarParameter<>& young_modulus,
                    const ScalarParameter<>& poisson_ratio,
                    const ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration configuration);


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_INTERNAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
