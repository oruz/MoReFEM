/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_GRAD_PHI_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_GRAD_PHI_HPP_

#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Implementation of operator grad-grad.
             */
            class GradPhiGradPhi final : public BilinearLocalVariationalOperator<LocalMatrix>,
                                         public Crtp::LocalMatrixStorage<GradPhiGradPhi, 2ul>
            {

              public:
                //! Alias to self.
                using self = GradPhiGradPhi;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<GradPhiGradPhi>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Alias to parent.
                using parent = BilinearLocalVariationalOperator<LocalMatrix>;

                //! Rejuvenate alias from parent.
                using elementary_data_type = parent::elementary_data_type;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent = Crtp::LocalMatrixStorage<self, 2ul>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (STL vector)
                 * is due to constraints from genericity; for current operator it is expected to hold exactly
                 * one vectorial unknown.
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
                 * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
                 * method. \endinternal
                 */
                explicit GradPhiGradPhi(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                        const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                        elementary_data_type&& elementary_data);

                //! Destructor.
                virtual ~GradPhiGradPhi() override;

                //! \copydoc doxygen_hide_copy_constructor
                GradPhiGradPhi(const GradPhiGradPhi& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GradPhiGradPhi(GradPhiGradPhi&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GradPhiGradPhi& operator=(const GradPhiGradPhi& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GradPhiGradPhi& operator=(GradPhiGradPhi&& rhs) = delete;


                ///@}


                /*!
                 * \brief Compute the elementary matrix.
                 *
                 * Here we consider the case of the operator for one unknown, so the structure of the matrix is (for 3d
                 * case):
                 *
                 *
                 *
                 *    vx1, vx2, ..., vxm, vy1, vy2, ..., vym, vz1, vz2, ..., vzm
                 * (                                                              ) // Same ordering for rows.
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 * (                                                              )
                 */
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation()
                { }

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation()
                { }

              private:
                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Indexes of local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    block_matrix = 0,
                    transposed_dphi
                };

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiGradPhi.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_GRAD_PHI_HPP_
