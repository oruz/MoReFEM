/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            GradPhiGradPhi::GradPhiGradPhi(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                           elementary_data_type&& a_elementary_data)
            : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent()
            {
                assert(unknown_list.size() == 1);
                assert(test_unknown_list.size() == 1);

                const auto& elementary_data = parent::GetElementaryData();

                const auto& unknown = parent::GetNthUnknown(0);
                const auto& test_unknown = parent::GetNthTestUnknown(0);

                const auto& ref_felt = elementary_data.GetRefFElt(unknown);
                const auto& test_ref_felt = elementary_data.GetTestRefFElt(test_unknown);

                const auto Nnode_for_unknown = ref_felt.Nnode();
                const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

#ifndef NDEBUG
                {
                    const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();
                    assert(!infos_at_quad_pt_list.empty());
                    const auto& last_dphi = infos_at_quad_pt_list.back().GetUnknownData().GetGradientFEltPhi();
                    assert(last_dphi.shape(0) == Nnode_for_unknown);
                }
#endif // NDEBUG

                const auto felt_space_dimension = ref_felt.GetFEltSpaceDimension();

                InitLocalMatrixStorage({ {
                    { Nnode_for_test_unknown, Nnode_for_unknown }, // block matrix
                    { felt_space_dimension, Nnode_for_unknown }    // transposed dPhi
                } });
            }


            GradPhiGradPhi::~GradPhiGradPhi() = default;


            const std::string& GradPhiGradPhi::ClassName()
            {
                static std::string name("GradPhiGradPhi");
                return name;
            }


            void GradPhiGradPhi::ComputeEltArray()
            {
                auto& elementary_data = parent::GetNonCstElementaryData();

                const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

                // Current operator yields in fact a diagonal per block matrix where each block is the same.
                auto& block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();

                auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                matrix_result.fill(0.);

                auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();


                const auto Nnode_for_unknown = ref_felt.Nnode();
                const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

                const auto Ncomponent = ref_felt.Ncomponent();
                assert(Ncomponent == test_ref_felt.Ncomponent());

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    block_matrix.fill(0.);

                    decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    // First compute the content of the block matrix.
                    const double factor = infos_at_quad_pt.GetQuadraturePoint().GetWeight()
                                          * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

                    const auto& dphi = ExtractSubMatrix(quad_pt_unknown_data.GetGradientFEltPhi(), ref_felt);

                    const auto& dphi_test =
                        ExtractSubMatrix(test_quad_pt_unknown_data.GetGradientFEltPhi(), test_ref_felt);

                    assert(dphi.shape(0) == Nnode_for_unknown);
                    assert(dphi_test.shape(0) == Nnode_for_test_unknown);

                    xt::noalias(transposed_dphi) = xt::transpose(dphi);

                    xt::noalias(block_matrix) = factor * xt::linalg::dot(dphi_test, transposed_dphi);


                    // Then report it into the elementary matrix.
                    for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                    {
                        for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                        {
                            const double value = block_matrix(m, n);

                            for (auto deriv_component = Advanced::ComponentNS::index_type{ 0ul };
                                 deriv_component < Ncomponent;
                                 ++deriv_component)
                            {
                                const auto shift = Nnode_for_test_unknown * deriv_component.Get();
                                matrix_result(m + shift, n + shift) += value;
                            }
                        }
                    }
                }
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
