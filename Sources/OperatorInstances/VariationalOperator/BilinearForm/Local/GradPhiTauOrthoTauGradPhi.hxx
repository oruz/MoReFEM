/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 25 Jan 2017 10:21:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauOrthoTauGradPhi.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const GradPhiTauOrthoTauGradPhi::scalar_parameter&
            GradPhiTauOrthoTauGradPhi ::GetTransverseDiffusionTensor() const noexcept
            {
                return transverse_diffusion_tensor_;
            }


            inline const GradPhiTauOrthoTauGradPhi::scalar_parameter&
            GradPhiTauOrthoTauGradPhi ::GetFiberDiffusionTensor() const noexcept
            {
                return fiber_diffusion_tensor_;
            }

            inline const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>&
            GradPhiTauOrthoTauGradPhi::GetFibers() const noexcept
            {
                return fibers_;
            }

            inline const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>&
            GradPhiTauOrthoTauGradPhi::GetAngles() const noexcept
            {
                return angles_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HXX_
