/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Jan 2016 10:30:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            Ale::Ale(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                     const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                     elementary_data_type&& a_elementary_data,
                     const scalar_parameter& density)
            : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
              density_(density)
            {
                assert(a_unknown_storage.size() == 1 && "Operator currently written to be used with one unknown only!");
                assert(!(!a_unknown_storage.back()));
                assert(a_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);

                assert(a_test_unknown_storage.size() == 1
                       && "Operator currently written to be used with one unknown only!");
                assert(!(!a_test_unknown_storage.back()));
                assert(a_test_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);

                const auto& elementary_data = GetElementaryData();

                former_local_velocity_.resize(elementary_data.NdofRow());
            }


            Ale::~Ale() = default;


            const std::string& Ale::ClassName()
            {
                static std::string name("Ale");
                return name;
            }


            void Ale::ComputeEltArray()
            {
                auto& elementary_data = GetNonCstElementaryData();
                const auto& former_local_velocity = GetFormerLocalVelocity();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

                const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto Ncomponent = ref_felt.Ncomponent();

                const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

                assert(Ncomponent == test_ref_felt.Ncomponent());

                auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                matrix_result.fill(0.);

                const auto Nnode_for_unknown = ref_felt.Nnode();

                const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

                decltype(auto) geom_elt = elementary_data.GetCurrentGeomElt();

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    const auto& phi = quad_pt_unknown_data.GetFEltPhi();

                    const auto& phi_test = test_quad_pt_unknown_data.GetFEltPhi();
                    const auto& grad_phi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

                    decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                    const double geometric_factor =
                        quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

                    const double factor = geometric_factor * GetDensity().GetValue(quad_pt, geom_elt);

                    {
                        // ===========================================
                        // First add the modified_v * gradV * Vtest contribution.
                        // Only the block for the first component is filled here.
                        // ===========================================
                        for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                        {
                            double modified_velocity_at_quad_pt = 0.;

                            // Compute the \a component for velocity at quadrature point.
                            for (auto node_test_index = 0ul; node_test_index < Nnode_for_test_unknown;
                                 ++node_test_index)
                            {
                                assert(node_test_index + component.Get() * Nnode_for_test_unknown
                                       < former_local_velocity.size());
                                modified_velocity_at_quad_pt +=
                                    former_local_velocity[node_test_index + component.Get() * Nnode_for_test_unknown]
                                    * phi_test(node_test_index);
                            }

                            for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                            {
                                for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                                {
                                    matrix_result(m, n) += factor * modified_velocity_at_quad_pt
                                                           * grad_phi_test(m, component.Get()) * phi(n);
                                }
                            }
                        }
                    }

                    {
                        // ===========================================
                        // Then add the div (modified_v) * (Vx Vtextx + Vy Vtesty) contribution.
                        // Likewise, only the block for the first component is filled here.
                        // ===========================================
                        double div_modified_velocity_at_quad_pt = 0.;

                        for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                        {
                            // Compute the \a component for velocity at quadrature point.
                            for (auto node_index = 0ul; node_index < Nnode_for_test_unknown; ++node_index)
                                div_modified_velocity_at_quad_pt +=
                                    former_local_velocity[node_index + component.Get() * Nnode_for_test_unknown]
                                    * grad_phi_test(node_index, component.Get());
                        }

                        for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                        {
                            const double m_value = factor * div_modified_velocity_at_quad_pt * phi_test(m);

                            for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                                matrix_result(m, n) += m_value * phi(n);
                        }
                    }


                    {
                        // ===========================================
                        // Duplicate the block for each component.
                        // ===========================================
                        for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                        {
                            for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                            {
                                for (auto component_block = ComponentNS::index_type{ 1ul };
                                     component_block < Ncomponent;
                                     ++component_block)
                                {
                                    const auto shift_row = Nnode_for_test_unknown * component_block.Get();
                                    const auto shift_col = Nnode_for_unknown * component_block.Get();
                                    matrix_result(m + shift_row, n + shift_col) = matrix_result(m, n);
                                }
                            }
                        }
                    }
                }
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
