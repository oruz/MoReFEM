/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hpp"

#include <vector>


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const Ale::scalar_parameter& Ale::GetDensity() const noexcept
            {
                return density_;
            }


            inline const std::vector<double>& Ale::GetFormerLocalVelocity() const noexcept
            {
                return former_local_velocity_;
            }


            inline std::vector<double>& Ale::GetNonCstFormerLocalVelocity() noexcept
            {
                return const_cast<std::vector<double>&>(GetFormerLocalVelocity());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_ALE_HXX_
