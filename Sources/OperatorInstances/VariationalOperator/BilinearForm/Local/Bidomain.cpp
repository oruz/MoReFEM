/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 1 Sep 2015 14:09:59 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            Bidomain::Bidomain(
                const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                elementary_data_type&& a_elementary_data,
                const scalar_parameter& intracellular_trans_diffusion_tensor,
                const scalar_parameter& extracellular_trans_diffusion_tensor,
                const scalar_parameter& intracellular_fiber_diffusion_tensor,
                const scalar_parameter& extracellular_fiber_diffusion_tensor,
                const FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers)
            : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
              matrix_parent(), intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
              extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
              intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
              extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor), fibers_(fibers)
            {
                const auto& elementary_data = GetElementaryData();

                const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();

                const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
                const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

                const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
                const int Nnode_for_test_unknown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());

                const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
                const int Nnode_for_test_unknown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());

                const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());

                InitLocalMatrixStorage({ {
                    { Nnode_for_test_unknown1, Nnode_for_unknown1 },   // block matrix 1 (Vm,Vm)
                    { Nnode_for_test_unknown2, Nnode_for_unknown2 },   // block matrix 2 (Ue,Ue)
                    { Nnode_for_test_unknown1, Nnode_for_unknown2 },   // block matrix 3 (Vm, Ue)
                    { Nnode_for_test_unknown2, Nnode_for_unknown1 },   // block matrix 4 (Ue, Vm)
                    { felt_space_dimension, Nnode_for_unknown1 },      // transposed dPhi
                    { felt_space_dimension, Nnode_for_unknown2 },      // transposed dPsi
                    { Nnode_for_test_unknown1, felt_space_dimension }, // dPhi_test*sigma
                    { Nnode_for_test_unknown2, felt_space_dimension }, // dPsi_test*sigma
                    { felt_space_dimension, felt_space_dimension }     // tau_X_tau
                } });
            }


            Bidomain::~Bidomain() = default;


            const std::string& Bidomain::ClassName()
            {
                static std::string name("Bidomain");
                return name;
            }


            void Bidomain::ComputeEltArray()
            {
                auto& elementary_data = GetNonCstElementaryData();

                auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                matrix_result.fill(0.);

                const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

                const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
                const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));

                // Current operator yields in fact a diagonal per block matrix where each block is the same.
                auto& block_matrix1 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
                auto& block_matrix2 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
                auto& block_matrix3 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
                auto& block_matrix4 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
                auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
                auto& transposed_dpsi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dpsi)>();
                auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
                auto& dpsi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_sigma)>();
                auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

                const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();
                const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

                assert(unknown1_ref_felt.Ncomponent().Get() == 1u && "Bidomain operator limited to scalar unknowns.");
                assert(unknown2_ref_felt.Ncomponent().Get() == 1u && "Bidomain operator limited to scalar unknowns.");

                const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode();
                const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode();

                assert(test_unknown1_ref_felt.Ncomponent().Get() == 1u
                       && "Bidomain operator limited to scalar unknowns.");
                assert(test_unknown2_ref_felt.Ncomponent().Get() == 1u
                       && "Bidomain operator limited to scalar unknowns.");

                const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
                const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
                const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
                const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();

                const auto& geom_elt = elementary_data.GetCurrentGeomElt();

                const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());

                auto& fibers = GetFibers();

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    // First compute the content of the block matrix.
                    const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                    const double factor =
                        quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

                    const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const auto& grad_felt_phi_for_unknown = quad_pt_unknown_data.GetGradientFEltPhi();

                    const auto& dphi = ExtractSubMatrix(grad_felt_phi_for_unknown, unknown1_ref_felt);

                    const auto& dpsi = ExtractSubMatrix(grad_felt_phi_for_unknown, unknown2_ref_felt);

                    const auto& grad_felt_phi_for_test_unknown = test_quad_pt_unknown_data.GetGradientFEltPhi();

                    const auto& dphi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown, test_unknown1_ref_felt);

                    const auto& dpsi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown, test_unknown2_ref_felt);

                    assert(dphi.shape(0) == Nnode_for_unknown1);
                    assert(dpsi.shape(0) == Nnode_for_unknown2);
                    assert(dphi_test.shape(0) == Nnode_for_test_unknown1);
                    assert(dpsi_test.shape(0) == Nnode_for_test_unknown2);

                    xt::noalias(transposed_dphi) = xt::transpose(dphi);
                    xt::noalias(transposed_dpsi) = xt::transpose(dpsi);

                    block_matrix1.fill(0.);
                    block_matrix2.fill(0.);
                    block_matrix3.fill(0.);
                    block_matrix4.fill(0.);

                    xt::noalias(block_matrix1) = factor1 * xt::linalg::dot(dphi_test, transposed_dphi);

                    xt::noalias(block_matrix2) = (factor1 + factor2) * xt::linalg::dot(dpsi_test, transposed_dpsi);

                    xt::noalias(block_matrix3) = factor1 * xt::linalg::dot(dphi_test, transposed_dpsi);

                    xt::noalias(block_matrix4) = factor1 * xt::linalg::dot(dpsi_test, transposed_dphi);

                    const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

                    double norm = 0.;
                    for (int component = 0; component < felt_space_dimension; ++component)
                        norm += NumericNS::Square(tau_interpolate(component));

                    tau_sigma.fill(0.);

                    if (!(NumericNS::IsZero(norm)))
                    {
                        xt::noalias(tau_sigma) = xt::linalg::outer(tau_interpolate, tau_interpolate) / norm;

                        xt::noalias(dphi_test_sigma) = (factor3 - factor1) * xt::linalg::dot(dphi_test, tau_sigma);

                        xt::noalias(block_matrix1) += xt::linalg::dot(dphi_test_sigma, transposed_dphi);

                        xt::noalias(dphi_test_sigma) = (factor3 - factor1) * xt::linalg::dot(dphi_test, tau_sigma);

                        xt::noalias(block_matrix3) += xt::linalg::dot(dphi_test_sigma, transposed_dpsi);

                        xt::noalias(dpsi_test_sigma) = (factor3 - factor1) * xt::linalg::dot(dpsi_test, tau_sigma);

                        xt::noalias(block_matrix4) += xt::linalg::dot(dpsi_test_sigma, transposed_dphi);

                        xt::noalias(dpsi_test_sigma) =
                            (factor3 - factor1 + factor4 - factor2) * xt::linalg::dot(dpsi_test, tau_sigma);

                        xt::noalias(block_matrix2) += xt::linalg::dot(dpsi_test_sigma, transposed_dpsi);
                    }

                    // Then report it into the elementary matrix.
                    for (auto node_test_unknown1_index = 0ul; node_test_unknown1_index < Nnode_for_test_unknown1;
                         ++node_test_unknown1_index)
                    {
                        for (auto node_unknown1_index = 0ul; node_unknown1_index < Nnode_for_unknown1;
                             ++node_unknown1_index)
                        {
                            const double value1 = block_matrix1(node_test_unknown1_index, node_unknown1_index);

                            matrix_result(node_test_unknown1_index, node_unknown1_index) += value1;
                        }
                    }

                    for (auto node_test_unknown2_index = 0ul; node_test_unknown2_index < Nnode_for_test_unknown2;
                         ++node_test_unknown2_index)
                    {
                        for (auto node_unknown2_index = 0ul; node_unknown2_index < Nnode_for_unknown2;
                             ++node_unknown2_index)
                        {
                            const double value2 = block_matrix2(node_test_unknown2_index, node_unknown2_index);

                            matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1,
                                          node_unknown2_index + Nnode_for_unknown1) += value2;
                        }
                    }

                    for (auto node_test_unknown2_index = 0ul; node_test_unknown2_index < Nnode_for_test_unknown2;
                         ++node_test_unknown2_index)
                    {
                        for (auto node_unknown1_index = 0ul; node_unknown1_index < Nnode_for_unknown1;
                             ++node_unknown1_index)
                        {
                            const double value4 = block_matrix4(node_test_unknown2_index, node_unknown1_index);

                            matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1, node_unknown1_index) +=
                                value4;
                        }
                    }

                    for (auto node_test_unknown1_index = 0ul; node_test_unknown1_index < Nnode_for_test_unknown1;
                         ++node_test_unknown1_index)
                    {
                        for (auto node_unknown2_index = 0ul; node_unknown2_index < Nnode_for_unknown2;
                             ++node_unknown2_index)
                        {
                            const double value3 = block_matrix3(node_test_unknown1_index, node_unknown2_index);

                            matrix_result(node_test_unknown1_index, node_unknown2_index + Nnode_for_unknown1) += value3;
                        }
                    }
                }
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
