/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 May 2015 10:21:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    Stokes::Stokes(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                   elementary_data_type&& a_elementary_data,
                   const scalar_parameter& fluid_viscosity)
    : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent(),
      fluid_viscosity_(fluid_viscosity)
    {

#ifndef NDEBUG
        {
            const auto& unknown_storage = this->GetExtendedUnknownList();
            assert(unknown_storage.size() == 2ul
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                   && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                   && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);

            const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
            assert(test_unknown_storage.size() == 2ul
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!test_unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                   && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!test_unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                   && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
        }
#endif // NDEBUG

        const auto& elementary_data = GetElementaryData();
        const auto& velocity_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
        const int Nnode_for_velocity = static_cast<int>(velocity_ref_felt.Nnode());

        const auto& test_velocity_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
        const int Nnode_for_test_velocity = static_cast<int>(test_velocity_ref_felt.Nnode());

        const int felt_space_dimension = static_cast<int>(velocity_ref_felt.GetFEltSpaceDimension());

        InitLocalMatrixStorage({ {
            { Nnode_for_test_velocity, Nnode_for_velocity }, // velocity_block_matrix
            { felt_space_dimension, Nnode_for_velocity }     // transposed_dphi_velocity_block
        } });
    }


    Stokes::~Stokes() = default;


    const std::string& Stokes::ClassName()
    {
        static std::string name("Stokes");
        return name;
    }


    void Stokes::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.fill(0.);

        const auto& scalar_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
        const auto& vectorial_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));

        const auto& scalar_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
        const auto& vectorial_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));

        const auto Nnode_velocity = vectorial_ref_felt.Nnode();
        const auto Ndof_velocity = vectorial_ref_felt.Ndof();
        const auto Nnode_pressure = scalar_ref_felt.Nnode();

        const auto Nnode_test_velocity = vectorial_test_ref_felt.Nnode();
        const auto Nnode_test_pressure = scalar_test_ref_felt.Nnode();
        const auto Ndof_test_velocity = vectorial_test_ref_felt.Ndof();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

        auto& velocity_block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::velocity_block_matrix)>();
        // < Used to store factor * dPhi * transp(dPhi_test).

        auto& transposed_dphi_velocity =
            GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_velocity)>();
        // < Helper matrix to store transp(dPhi_test).

        const auto& fluid_viscosity = GetFluidViscosity();
        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto Ncomponent = vectorial_ref_felt.Ncomponent();
        assert(Ncomponent == vectorial_test_ref_felt.Ncomponent());

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant()
                                  * fluid_viscosity.GetValue(quad_pt, geom_elt);


            // First the scalar div vectorial contribution, which should yield a result similar to operator
            // ScalarDivVectorial.
            {
                const auto& felt_phi = quad_pt_unknown_data.GetFEltPhi();
                const auto& test_felt_phi = test_quad_pt_unknown_data.GetFEltPhi();

                const auto& gradient_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi();
                const auto& test_gradient_felt_phi = test_quad_pt_unknown_data.GetGradientFEltPhi();

                const auto& pressure_felt_phi = ExtractSubVector(felt_phi, scalar_ref_felt); // only on p
                const auto& test_pressure_felt_phi = ExtractSubVector(test_felt_phi,
                                                                      scalar_test_ref_felt); // only on p*

                // Part in (v* p)
                for (auto node_test_velocity_index = 0ul; node_test_velocity_index < Nnode_test_velocity;
                     ++node_test_velocity_index)
                {
                    for (auto node_pressure_index = 0ul; node_pressure_index < Nnode_pressure; ++node_pressure_index)
                    {
                        const auto dof_pressure = node_pressure_index; // alias

                        // The two terms below give the same result but in a different manner.
                        // It is just here to show how to use them.
                        // const auto test_pressure_term =
                        //   test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                        const auto pressure_term = pressure_felt_phi(node_pressure_index);

                        for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                        {
                            auto dof_test_velocity_index =
                                node_test_velocity_index + Nnode_test_velocity * component.Get();

                            const double product = factor * pressure_term
                                                   * test_gradient_felt_phi(node_test_velocity_index, component.Get());

                            assert(dof_test_velocity_index < matrix_result.shape(0));
                            assert(Ndof_velocity + dof_pressure < matrix_result.shape(1));
                            matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) -= product;
                        }
                    }
                }

                // Part in (p* v)
                for (auto node_test_pressure_index = 0ul; node_test_pressure_index < Nnode_test_pressure;
                     ++node_test_pressure_index)
                {
                    const auto dof_test_pressure = node_test_pressure_index;

                    // The two terms below give the same result but in a different manner. It is just here to show
                    // how to use them.
                    // const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                    const auto test_pressure_term = test_pressure_felt_phi(node_test_pressure_index);

                    for (auto node_velocity_index = 0ul; node_velocity_index < Nnode_velocity; ++node_velocity_index)
                    {
                        auto dof_velocity_index = node_velocity_index;

                        for (auto component = ComponentNS::index_type{ 0ul }; component < Ncomponent;
                             ++component, dof_velocity_index += Nnode_velocity)
                        {
                            const double product =
                                factor * test_pressure_term * gradient_felt_phi(node_velocity_index, component.Get());

                            assert(Ndof_test_velocity + dof_test_pressure < matrix_result.shape(0));
                            assert(dof_velocity_index < matrix_result.shape(1));

                            matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) -= product;
                        }
                    }
                }
            }

            // Then here we perform grad-grad operator on the velocity-velocity block.
            {
                const auto& dphi_velocity =
                    ExtractSubMatrix(quad_pt_unknown_data.GetGradientFEltPhi(), vectorial_ref_felt);

                const auto& dphi_test_velocity =
                    ExtractSubMatrix(test_quad_pt_unknown_data.GetGradientFEltPhi(), vectorial_test_ref_felt);

                velocity_block_matrix.fill(0.);

                // First compute the content of the block matrix.
                assert(dphi_velocity.shape(0) == Nnode_velocity);
                assert(dphi_velocity.shape(1) == Ncomponent.Get());

                assert(dphi_test_velocity.shape(0) == Nnode_test_velocity);
                assert(dphi_test_velocity.shape(1) == Ncomponent.Get());

                xt::noalias(transposed_dphi_velocity) = xt::transpose(dphi_velocity);

                xt::noalias(velocity_block_matrix) =
                    factor * xt::linalg::dot(dphi_test_velocity, transposed_dphi_velocity);

                // Then report it into the elementary matrix.
                for (auto m = 0ul; m < Nnode_test_velocity; ++m)
                {
                    for (auto n = 0ul; n < Nnode_velocity; ++n)
                    {
                        const double value = velocity_block_matrix(m, n);

                        for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                        {
                            const auto shift_row = Nnode_test_velocity * component.Get();
                            const auto shift_col = Nnode_velocity * component.Get();
                            matrix_result(m + shift_row, n + shift_col) += value;
                        }
                    }
                }
            }
        }
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup
