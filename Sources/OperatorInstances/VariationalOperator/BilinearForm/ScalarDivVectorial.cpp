/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Sep 2014 14:19:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        ScalarDivVectorial::ScalarDivVectorial(const FEltSpace& felt_space,
                                               const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                               const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                               const double alpha,
                                               const double beta,
                                               const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 alpha,
                 beta)
        {
            assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

            assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
        }


        const std::string& ScalarDivVectorial::ClassName()
        {
            static std::string name("ScalarDivVectorial");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
