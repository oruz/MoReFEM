/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HPP_

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp"                // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/ScalarDivVectorial.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Implementation of global Stokes operator.
         *
         * \todo Improve the comment by writing its mathematical definition!
         */
        class ScalarDivVectorial final
        // clang-format off
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            ScalarDivVectorial,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::ScalarDivVectorial
        >
        // clang-format on
        {

          public:
            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_alias_self
            using self = ScalarDivVectorial;

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::ScalarDivVectorial;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;
            // clang-format on


            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;


            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const ScalarDivVectorial>;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydetails doxygen_hide_scalar_div_vectorial_formula
             *
             * \param[in] unknown_list Container with vectorial then scalar unknown.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \param[in] alpha See its meaning in the formula above.
             * \param[in] beta See its meaning in the formula above.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit ScalarDivVectorial(const FEltSpace& felt_space,
                                        const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                        const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                        double alpha,
                                        double beta,
                                        const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~ScalarDivVectorial() = default;

            //! \copydoc doxygen_hide_move_constructor
            ScalarDivVectorial(ScalarDivVectorial&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_constructor
            ScalarDivVectorial(const ScalarDivVectorial& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            ScalarDivVectorial& operator=(const ScalarDivVectorial& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            ScalarDivVectorial& operator=(ScalarDivVectorial&& rhs) = delete;

            ///@}


            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;
        };


    } //  namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HPP_
