/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Jun 2014 16:41:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        GradOnGradientBasedElasticityTensor ::GradOnGradientBasedElasticityTensor(
            const FEltSpace& felt_space,
            const Unknown::const_shared_ptr unknown_ptr,
            const Unknown::const_shared_ptr test_unknown_ptr,
            const scalar_parameter& young_modulus,
            const scalar_parameter& poisson_ratio,
            const ParameterNS::GradientBasedElasticityTensorConfiguration configuration,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 young_modulus,
                 poisson_ratio,
                 configuration)
        {
            assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
        }


        const std::string& GradOnGradientBasedElasticityTensor::ClassName()
        {
            static std::string name("GradOnGradientBasedElasticityTensor");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
