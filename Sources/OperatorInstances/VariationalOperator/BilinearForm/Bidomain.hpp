/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_BIDOMAIN_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_BIDOMAIN_HPP_

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        class Bidomain final
        // clang-format off
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            Bidomain,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::Bidomain
        >
        // clang-format on
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = Bidomain;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::Bidomain;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                Bidomain,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;
            // clang-format on

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const Bidomain>;

            //! Alias to relevant scalar parameter type used in this operator.
            using scalar_parameter = local_operator_type::scalar_parameter;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list Two scalar unknowns considered: first one is transcellular potential,
             * second one is extracellular one.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] intracellular_trans_diffusion_tensor intracellular_trans_diffusion_tensor
             * \param[in] extracellular_trans_diffusion_tensor extracellular_trans_diffusion_tensor
             * \param[in] intracellular_fiber_diffusion_tensor intracellular_fiber_diffusion_tensor
             * \param[in] extracellular_fiber_diffusion_tensor extracellular_fiber_diffusion_tensor
             * \param[in] fibers Fibers.
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit Bidomain(const FEltSpace& felt_space,
                              const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                              const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                              const scalar_parameter& intracellular_trans_diffusion_tensor,
                              const scalar_parameter& extracellular_trans_diffusion_tensor,
                              const scalar_parameter& intracellular_fiber_diffusion_tensor,
                              const scalar_parameter& extracellular_fiber_diffusion_tensor,
                              const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers,
                              const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);


            //! Destructor.
            ~Bidomain() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Bidomain(const Bidomain& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Bidomain(Bidomain&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Bidomain& operator=(const Bidomain& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Bidomain& operator=(Bidomain&& rhs) = delete;

            ///@}


          public:
            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;
        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_BIDOMAIN_HPP_
