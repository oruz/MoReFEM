/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:40:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Instantiation of the Operator related to elastic matrix.
         *
         */
        class GradOnGradientBasedElasticityTensor final
        : public GlobalVariationalOperatorNS::DependsOnRefGeomElt<
              GradOnGradientBasedElasticityTensor,
              Advanced::OperatorNS::Nature::bilinear,
              std::tuple<MOREFEM_GVO_LOCAL_TUPLE_ITEM_IGNORE(Advanced::GeometricEltEnum::Point1),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Segment2,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Segment3,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Triangle3,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Triangle6,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Quadrangle4,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Quadrangle8,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Quadrangle9,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Tetrahedron4,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Tetrahedron10,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Hexahedron8,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Hexahedron20,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                         MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                             Advanced::GeometricEltEnum::Hexahedron27,
                             Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor)>>
        {

          public:
            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_alias_self
            using self = GradOnGradientBasedElasticityTensor;

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::DependsOnRefGeomElt<
                GradOnGradientBasedElasticityTensor,
                Advanced::OperatorNS::Nature::bilinear,
                std::tuple<MOREFEM_GVO_LOCAL_TUPLE_ITEM_IGNORE(Advanced::GeometricEltEnum::Point1),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Segment2,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Segment3,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Triangle3,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Triangle6,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Quadrangle4,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Quadrangle8,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Quadrangle9,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Tetrahedron4,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Tetrahedron10,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Hexahedron8,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Hexahedron20,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor),
                           MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(
                               Advanced::GeometricEltEnum::Hexahedron27,
                               Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor)>>;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const GradOnGradientBasedElasticityTensor>;

            //! Young modulus parameter type.
            using scalar_parameter = typename local_operator_type::scalar_parameter;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \param[in] unknown_ptr Vectorial unknown considered (should be a displacement).
             * \copydoc doxygen_hide_test_unknown_ptr_param
             * \param[in] young_modulus Young modulus of the solid.
             * \param[in] poisson_ratio Poisson coefficient of the solid.
             * \param[in] configuration Whether we consider 2D/plane_strain, 2D/plane_stress operator or 3D operator.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit GradOnGradientBasedElasticityTensor(
                const FEltSpace& felt_space,
                const Unknown::const_shared_ptr unknown_ptr,
                const Unknown::const_shared_ptr test_unknown_ptr,
                const scalar_parameter& young_modulus,
                const scalar_parameter& poisson_ratio,
                const ParameterNS::GradientBasedElasticityTensorConfiguration configuration,
                const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~GradOnGradientBasedElasticityTensor() = default;

            //! \copydoc doxygen_hide_move_constructor
            GradOnGradientBasedElasticityTensor(GradOnGradientBasedElasticityTensor&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_constructor
            GradOnGradientBasedElasticityTensor(const GradOnGradientBasedElasticityTensor& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            GradOnGradientBasedElasticityTensor& operator=(const GradOnGradientBasedElasticityTensor& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            GradOnGradientBasedElasticityTensor& operator=(GradOnGradientBasedElasticityTensor&& rhs) = delete;

            ///@}


            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_tuple List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_tuple, const Domain& domain = Domain()) const;
        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
