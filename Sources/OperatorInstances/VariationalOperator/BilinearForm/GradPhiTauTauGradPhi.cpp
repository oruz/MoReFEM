/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 9 Aug 2017 17:28:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauTauGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        GradPhiTauTauGradPhi::GradPhiTauTauGradPhi(
            const FEltSpace& felt_space,
            const Unknown::const_shared_ptr unknown_ptr,
            const Unknown::const_shared_ptr test_unknown_ptr,
            const scalar_parameter& transverse_diffusion_tensor,
            const scalar_parameter& fiber_diffusion_tensor,
            const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 transverse_diffusion_tensor,
                 fiber_diffusion_tensor,
                 fibers)
        { }


        const std::string& GradPhiTauTauGradPhi::ClassName()
        {
            static std::string name("GradPhiTauTauGradPhi");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
