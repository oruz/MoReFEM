/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Jun 2014 13:58:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        Mass::Mass(const FEltSpace& felt_space,
                   const Unknown::const_shared_ptr unknown_ptr,
                   const Unknown::const_shared_ptr test_unknown_ptr,
                   const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::no,
                 DoComputeProcessorWiseLocal2Global::no)
        { }


        const std::string& Mass::ClassName()
        {
            static std::string name("Mass");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
