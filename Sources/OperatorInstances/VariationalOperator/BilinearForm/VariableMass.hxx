/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Sep 2016 18:31:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/VariableMass.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<class ParameterT>
        VariableMass<ParameterT>::VariableMass(const FEltSpace& felt_space,
                                               const Unknown::const_shared_ptr unknown_ptr,
                                               const Unknown::const_shared_ptr test_unknown_ptr,
                                               const ParameterT& parameter_factor,
                                               const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::no,
                 DoComputeProcessorWiseLocal2Global::no,
                 parameter_factor)
        { }


        template<class ParameterT>
        const std::string& VariableMass<ParameterT>::ClassName()
        {
            static std::string name("VariableMass");
            return name;
        }


        template<class ParameterT>
        template<class LinearAlgebraTupleT>
        inline void VariableMass<ParameterT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                       const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
        }


    } //  namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HXX_
