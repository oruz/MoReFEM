/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HPP_

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/VariableMass.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Implementation of global mass operator for poromechanics.
         *
         * \todo Improve the comment by writing its mathematical definition!
         * \todo #971 Probably merge it with 'usual' mass operator through a policy.
         */
        template<class ParameterT>
        class VariableMass final
        // clang-format off
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            VariableMass<ParameterT>,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::VariableMass<ParameterT>
        >
        // clang-format on
        {

          public:
            //! Self.
            using self = VariableMass<ParameterT>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::VariableMass<ParameterT>;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;
            // clang-format on

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const self>;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_ptr Unknown onto which the operator works.
             * \copydoc doxygen_hide_test_unknown_ptr_param
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \param[in] parameter_factor Spatial factor to consider expressed at quadrature points.
             */
            explicit VariableMass(const FEltSpace& felt_space,
                                  const Unknown::const_shared_ptr unknown_ptr,
                                  const Unknown::const_shared_ptr test_unknown_ptr,
                                  const ParameterT& parameter_factor,
                                  const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~VariableMass() = default;

            //! \copydoc doxygen_hide_move_constructor
            VariableMass(VariableMass&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_constructor
            VariableMass(const VariableMass& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariableMass& operator=(const VariableMass& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariableMass& operator=(VariableMass&& rhs) = delete;

            ///@}


            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_tuple List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_tuple, const Domain& domain = Domain()) const;
        };


    } //  namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/BilinearForm/VariableMass.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_VARIABLE_MASS_HPP_
