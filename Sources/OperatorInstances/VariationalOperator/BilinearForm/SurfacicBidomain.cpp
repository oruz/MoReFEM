/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "Core/Enum.hpp"
#include <map>
#include <ostream>
#include <vector>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp" // IWYU pragma: keep
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"      // IWYU pragma: keep

#include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp" // IWYU pragma: keep
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp" // IWYU pragma: keep
#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp" // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/BilinearForm/SurfacicBidomain.hpp" // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        SurfacicBidomain::SurfacicBidomain(
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const scalar_parameter& intracellular_trans_diffusion_tensor,
            const scalar_parameter& extracellular_trans_diffusion_tensor,
            const scalar_parameter& intracellular_fiber_diffusion_tensor,
            const scalar_parameter& extracellular_fiber_diffusion_tensor,
            const scalar_parameter& heterogeneous_conductivity_coefficient,
            const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>& fibers,
            const FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>& angles,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)

        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 intracellular_trans_diffusion_tensor,
                 extracellular_trans_diffusion_tensor,
                 intracellular_fiber_diffusion_tensor,
                 extracellular_fiber_diffusion_tensor,
                 heterogeneous_conductivity_coefficient,
                 fibers,
                 angles)
        { }


        const std::string& SurfacicBidomain::ClassName()
        {
            static std::string name("SurfacicBidomain");
            return name;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
