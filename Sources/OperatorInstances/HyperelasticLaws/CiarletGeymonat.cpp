/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


/*!
 \file CiarletGeymonat.h
 \authors S. Gilles
 \date 25/03/2013
 \brief Class in charge of Ciarlet-Geymonat laws
 */

#include <cmath>
#include <string> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        const std::string& CiarletGeymonat::ClassName()
        {
            static std::string ret("Ciarlet-Geymonat");
            return ret;
        }


        CiarletGeymonat::CiarletGeymonat(const Solid& solid)
        : kappa1_(solid.GetKappa1()), kappa2_(solid.GetKappa2()), bulk_(solid.GetHyperelasticBulk())
        { }


        double CiarletGeymonat::W(const invariant_holder_type& invariant_holder,
                                  const QuadraturePoint& quad_pt,
                                  const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            const double sqrt_I3 = std::sqrt(I3);

            return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
                   + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
        }


        double CiarletGeymonat::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                               const QuadraturePoint& quad_pt,
                                                               const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);

            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third * std::pow(I3, 4. * minus_one_third)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third
                         * std::pow(I3, 5. * minus_one_third)
                   + GetBulk().GetValue(quad_pt, geom_elt) * (0.5 * std::pow(I3, -0.5) - 0.5 / I3);
        }


        double CiarletGeymonat::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double one_ninth = 1. / 9.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth * std::pow(I3, 7. * minus_one_third)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth * std::pow(I3, 8. * minus_one_third)
                   + GetBulk().GetValue(quad_pt, geom_elt) * (-0.25 * std::pow(I3, -1.5) + 0.5 * std::pow(I3, -2.));
        }


        double CiarletGeymonat::SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                        const QuadraturePoint& quad_pt,
                                                                        const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third * std::pow(I3, 4. * minus_one_third);
        }


        double CiarletGeymonat::SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quad_pt,
                                                                         const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third);
        }


        double CiarletGeymonat::FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                               const QuadraturePoint& quad_pt,
                                                               const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa1().GetValue(quad_pt, geom_elt) * std::pow(I3, -1. / 3.);
        }


        double CiarletGeymonat::FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa2().GetValue(quad_pt, geom_elt) * std::pow(I3, -2. / 3.);
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
