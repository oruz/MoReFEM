/*!
//
// \file
//
//
// Created by Philippe Moireau <philippe.moireau@inria.fr> on the Fri, 23 Feb 2018 10:31:15 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {

        inline const LogI3Penalization::scalar_parameter& LogI3Penalization::GetBulk() const noexcept
        {
            return bulk_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
