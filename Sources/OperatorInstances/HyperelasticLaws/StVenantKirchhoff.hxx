/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        inline constexpr double
        StVenantKirchhoff ::SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);

            return 0.;
        }


        inline double StVenantKirchhoff ::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                          const QuadraturePoint& quad_pt,
                                                                          const GeometricElt& geom_elt) const noexcept
        {
            static_cast<void>(invariant_holder);

            return GetBulk().GetValue(quad_pt, geom_elt);
        }


        inline constexpr double
        StVenantKirchhoff ::SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                    const QuadraturePoint& quad_pt,
                                                                    const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);

            return 0.;
        }


        inline constexpr double
        StVenantKirchhoff ::SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quad_pt,
                                                                     const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);

            return 0.;
        }


        inline constexpr double
        StVenantKirchhoff ::SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quad_pt,
                                                                     const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);

            return 0.;
        }


        inline const StVenantKirchhoff::scalar_parameter& StVenantKirchhoff::GetLameLambda() const noexcept
        {
            return lame_lambda_;
        }


        inline const StVenantKirchhoff::scalar_parameter& StVenantKirchhoff::GetLameMu() const noexcept
        {
            return lame_mu_;
        }


        inline const StVenantKirchhoff::scalar_parameter& StVenantKirchhoff::GetBulk() const noexcept
        {
            return bulk_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_
