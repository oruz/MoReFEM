/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        inline constexpr double
        CiarletGeymonatDeviatoric::SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                   const QuadraturePoint& quad_pt,
                                                                   const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double
        CiarletGeymonatDeviatoric::SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                    const QuadraturePoint& quad_pt,
                                                                    const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double CiarletGeymonatDeviatoric::SecondDerivativeWFirstAndSecondInvariant(
            const invariant_holder_type& invariant_holder,
            const QuadraturePoint& quad_pt,
            const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline const CiarletGeymonatDeviatoric::scalar_parameter& CiarletGeymonatDeviatoric::GetKappa1() const noexcept
        {
            return kappa1_;
        }


        inline const CiarletGeymonatDeviatoric::scalar_parameter& CiarletGeymonatDeviatoric::GetKappa2() const noexcept
        {
            return kappa2_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_
