/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


/*!
 \file StVenantKirchhoff.cpp
 \authors S. Gilles
 \date 05/07/2013
 \brief Class in charge of StVenant Kirchhoff laws
 */

#include <string> // IWYU pragma: keep

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        const std::string& StVenantKirchhoff::ClassName()
        {
            static std::string ret("StVenant-Kirchhoff");
            return ret;
        }


        StVenantKirchhoff::StVenantKirchhoff(const Solid& solid)
        : lame_lambda_(solid.GetLameLambda()), lame_mu_(solid.GetLameMu()), bulk_(solid.GetHyperelasticBulk())
        { }


        double StVenantKirchhoff::W(const invariant_holder_type& invariant_holder,
                                    const QuadraturePoint& quad_pt,
                                    const GeometricElt& geom_elt) const
        {

            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);

            const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
            const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);
            const double bulk = GetBulk().GetValue(quad_pt, geom_elt);

            return (0.125 * lame_lambda_value + 0.25 * lame_mu_value) * NumericNS::Square(I1)
                   - (0.75 * lame_lambda_value + 0.5 * lame_mu_value) * I1 - 0.5 * lame_mu_value * I2
                   + 1.125 * lame_lambda_value + 0.75 * lame_mu_value + 0.5 * bulk * NumericNS::Square(I3 - 1.);
        }


        double StVenantKirchhoff::FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                 const QuadraturePoint& quad_pt,
                                                                 const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);

            const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
            const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

            return (0.25 * lame_lambda_value + 0.5 * lame_mu_value) * I1
                   - (0.75 * lame_lambda_value + 0.5 * lame_mu_value);
        }


        double StVenantKirchhoff::FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                  const QuadraturePoint& quad_pt,
                                                                  const GeometricElt& geom_elt) const
        {
            static_cast<void>(invariant_holder);
            const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

            return -0.5 * lame_mu_value;
        }


        double StVenantKirchhoff::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                 const QuadraturePoint& quad_pt,
                                                                 const GeometricElt& geom_elt) const
        {
            const double bulk = GetBulk().GetValue(quad_pt, geom_elt);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);

            return bulk * (I3 - 1.);
        }


        double StVenantKirchhoff::SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                  const QuadraturePoint& quad_pt,
                                                                  const GeometricElt& geom_elt) const
        {
            static_cast<void>(invariant_holder);
            const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
            const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

            return (0.25 * lame_lambda_value + 0.5 * lame_mu_value);
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
