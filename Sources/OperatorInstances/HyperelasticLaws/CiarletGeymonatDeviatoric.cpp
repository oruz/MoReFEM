/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


/*!
 \file CiarletGeymonatDeviatoric.h
 \authors S. Gilles
 \date 25/03/2013
 \brief Class in charge of Ciarlet-Geymonat laws
 */

#include <cmath>
#include <string> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        const std::string& CiarletGeymonatDeviatoric::ClassName()
        {
            static std::string ret("Ciarlet-Geymonat");
            return ret;
        }


        CiarletGeymonatDeviatoric::CiarletGeymonatDeviatoric(const Solid& solid)
        : kappa1_(solid.GetKappa1()), kappa2_(solid.GetKappa2())
        { }


        double CiarletGeymonatDeviatoric::W(const invariant_holder_type& invariant_holder,
                                            const QuadraturePoint& quad_pt,
                                            const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);

            return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.);
        }


        double CiarletGeymonatDeviatoric ::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                          const QuadraturePoint& quad_pt,
                                                                          const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);

            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third * std::pow(I3, 4. * minus_one_third)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third
                         * std::pow(I3, 5. * minus_one_third);
        }


        double
        CiarletGeymonatDeviatoric ::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                    const QuadraturePoint& quad_pt,
                                                                    const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double one_ninth = 1. / 9.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth * std::pow(I3, 7. * minus_one_third)
                   + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth
                         * std::pow(I3, 8. * minus_one_third);
        }


        double CiarletGeymonatDeviatoric ::SecondDerivativeWFirstAndThirdInvariant(
            const invariant_holder_type& invariant_holder,
            const QuadraturePoint& quad_pt,
            const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third * std::pow(I3, 4. * minus_one_third);
        }


        double CiarletGeymonatDeviatoric ::SecondDerivativeWSecondAndThirdInvariant(
            const invariant_holder_type& invariant_holder,
            const QuadraturePoint& quad_pt,
            const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;

            return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third);
        }


        double CiarletGeymonatDeviatoric ::FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                          const QuadraturePoint& quad_pt,
                                                                          const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa1().GetValue(quad_pt, geom_elt) * std::pow(I3, -1. / 3.);
        }


        double
        CiarletGeymonatDeviatoric ::FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                    const QuadraturePoint& quad_pt,
                                                                    const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa2().GetValue(quad_pt, geom_elt) * std::pow(I3, -2. / 3.);
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
