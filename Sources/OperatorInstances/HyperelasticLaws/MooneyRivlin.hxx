/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        inline constexpr double
        MooneyRivlin ::SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                       const QuadraturePoint& quad_pt,
                                                       const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double
        MooneyRivlin ::SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                        const QuadraturePoint& quad_pt,
                                                        const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double
        MooneyRivlin ::SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline const MooneyRivlin::scalar_parameter& MooneyRivlin::GetKappa1() const noexcept
        {
            return kappa1_;
        }


        inline const MooneyRivlin::scalar_parameter& MooneyRivlin::GetKappa2() const noexcept
        {
            return kappa2_;
        }


        inline const MooneyRivlin::scalar_parameter& MooneyRivlin::GetBulk() const noexcept
        {
            return bulk_;
        }


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HXX_
