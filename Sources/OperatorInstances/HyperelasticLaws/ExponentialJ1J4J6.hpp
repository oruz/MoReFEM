/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 28 Jan 2019 19:12:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HPP_

#include <array>
#include <bitset>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <exception>
#include <vector>

#include "Core/InputData/InputData.hpp"

#include "Parameters/Parameter.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class Solid; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        /*!
         * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
         */
        template<FiberNS::AtNodeOrAtQuadPt PolicyT>
        class ExponentialJ1J4J6
        {
          public:
            //! Return the name of the hyperelastic law.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_operator_alias_scalar_parameter
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! \copydoc doxygen_hide_alias_self
            using self = ExponentialJ1J4J6;

            //! \copydoc doxygen_hide_alias_const_unique_ptr
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias on the type of the invariant holder for the current law. 5 is the number of invariants.
            using invariant_holder_type = InvariantHolder<5, PolicyT>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] solid Object which provides the required material parameters for the solid.
             * \param[in] fibersI4 Fibers to define I4 in the law.
             * \param[in] fibersI6 Fibers to define I6 in the law.
             */
            explicit ExponentialJ1J4J6(const Solid& solid,
                                       const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI4,
                                       const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI6);

            //! Destructor.
            ~ExponentialJ1J4J6() = default;

            //! Copy constructor.
            ExponentialJ1J4J6(const ExponentialJ1J4J6&) = delete;

            //! Move constructor.
            ExponentialJ1J4J6(ExponentialJ1J4J6&&) = delete;

            //! Copy affectation.
            ExponentialJ1J4J6& operator=(const ExponentialJ1J4J6&) = delete;

            //! Move affectation.
            ExponentialJ1J4J6& operator=(ExponentialJ1J4J6&&) = delete;

            ///@}

          public:
            //! Number of invariants of the law.
            static constexpr std::size_t Ninvariants()
            {
                return 5;
            }

            //! If the law needs I4 or not.
            static constexpr bool DoI4Activate()
            {
                return true;
            }

            //! If the law needs I6 or not.
            static constexpr bool DoI6Activate()
            {
                return true;
            }

          public:
            //! Function W.
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double W(const invariant_holder_type& invariant_holder,
                     const QuadraturePoint& quadrature_point,
                     const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of first invariant (dWdI1)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of second invariant (dWdI2)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI3)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI4)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double FirstDerivativeWFourthInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI6)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double FirstDerivativeWSixthInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of first invariant (d2WdI1dI1)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second invariant (d2WdI2dI2)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quadrature_point,
                                                                     const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of third invariant (d2WdI3dI3)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of third invariant (d2WdI4dI4)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWFourthInvariant(const invariant_holder_type& invariant_holder,
                                                    const QuadraturePoint& quadrature_point,
                                                    const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of third invariant (d2WdI6dI6)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWSixthInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double
            SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                     const QuadraturePoint& quadrature_point,
                                                     const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                           const QuadraturePoint& quadrature_point,
                                                           const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second and third invariant (d2WdI1dI4)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double
            SecondDerivativeWFirstAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                     const QuadraturePoint& quadrature_point,
                                                     const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second and third invariant (d2WdI1dI6)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double
            SecondDerivativeWFirstAndSixthInvariant(const invariant_holder_type& invariant_holder,
                                                    const QuadraturePoint& quadrature_point,
                                                    const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                            const QuadraturePoint& quadrature_point,
                                                            const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second and third invariant (d2WdI2dI4)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double
            SecondDerivativeWSecondAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                      const QuadraturePoint& quadrature_point,
                                                      const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second and third invariant (d2WdI2dI6)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            static constexpr double
            SecondDerivativeWSecondAndSixthInvariant(const invariant_holder_type& invariant_holder,
                                                     const QuadraturePoint& quadrature_point,
                                                     const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second and third invariant (d2WdI3dI4)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWThirdAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                            const QuadraturePoint& quadrature_point,
                                                            const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second and third invariant (d2WdI3dI6)
            //! \copydoc doxygen_hide_hyperelastic_law_parameters
            double SecondDerivativeWThirdAndSixthInvariant(const invariant_holder_type& invariant_holder,
                                                           const QuadraturePoint& quadrature_point,
                                                           const GeometricElt& geom_elt) const;

          public:
            //! Constant accessors to the fibers in case of I4 is activate.
            const FiberList<PolicyT, ParameterNS::Type::vector>* GetFibersI4() const noexcept;

            //! Constant accessors to the fibers in case of I6 is activate.
            const FiberList<PolicyT, ParameterNS::Type::vector>* GetFibersI6() const noexcept;

          private:
            //! Mu1.
            const scalar_parameter& GetMu1() const noexcept;

            //! Mu2.
            const scalar_parameter& GetMu2() const noexcept;

            //! C0.
            const scalar_parameter& GetC0() const noexcept;

            //! C1.
            const scalar_parameter& GetC1() const noexcept;

            //! C2.
            const scalar_parameter& GetC2() const noexcept;

            //! C3.
            const scalar_parameter& GetC3() const noexcept;

            //! C4.
            const scalar_parameter& GetC4() const noexcept;

            //! C5.
            const scalar_parameter& GetC5() const noexcept;

            //! Hyperelastic bulk.
            const scalar_parameter& GetBulk() const noexcept;

          private:
            //! Mu1.
            const scalar_parameter& mu_1_;

            //! Mu2.
            const scalar_parameter& mu_2_;

            //! C0.
            const scalar_parameter& c_0_;

            //! C1.
            const scalar_parameter& c_1_;

            //! C2.
            const scalar_parameter& c_2_;

            //! C3.
            const scalar_parameter& c_3_;

            //! C4.
            const scalar_parameter& c_4_;

            //! C5.
            const scalar_parameter& c_5_;

            //! Bulk.
            const scalar_parameter& bulk_;

          private:
            //! Fibers parameter for I4.
            const FiberList<PolicyT, ParameterNS::Type::vector>* fibers_I4_ = nullptr;

            //! Fibers parameter for I6.
            const FiberList<PolicyT, ParameterNS::Type::vector>* fibers_I6_ = nullptr;
        };


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HPP_
