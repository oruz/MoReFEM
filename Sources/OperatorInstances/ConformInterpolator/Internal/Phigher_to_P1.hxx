/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 12:06:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_

// IWYU pragma: private, include "OperatorInstances/ConformInterpolator/Internal/Phigher_to_P1.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            template<class LocalInterpolatorT>
            const std::string& Phigher_to_P1<LocalInterpolatorT>::ClassName()
            {
                static std::string ret = "P" + LocalInterpolatorT::GetSourceShapeFunctionLabel() + "_to_P1";
                return ret;
            }


            template<class LocalInterpolatorT>
            Phigher_to_P1<LocalInterpolatorT>::Phigher_to_P1(const FEltSpace& source_felt_space,
                                                             const NumberingSubset& source_numbering_subset,
                                                             const FEltSpace& target_felt_space,
                                                             const NumberingSubset& target_numbering_subset,
                                                             pairing_type&& pairing)
            : parent(source_felt_space,
                     source_numbering_subset,
                     target_felt_space,
                     target_numbering_subset,
                     std::move(pairing))
            {
#ifndef NDEBUG

                const auto& interpolation_data = parent::GetInterpolationData();
                const auto& source_unknown_storage = interpolation_data.GetSourceData().GetExtendedUnknownList();

                assert(std::all_of(source_unknown_storage.cbegin(),
                                   source_unknown_storage.cend(),
                                   [](const auto& extended_unknown_ptr)
                                   {
                                       assert(!(!extended_unknown_ptr));

                                       return extended_unknown_ptr->GetShapeFunctionLabel()
                                              == LocalInterpolatorT::GetSourceShapeFunctionLabel();
                                   }));

                const auto& target_unknown_storage = interpolation_data.GetTargetData().GetExtendedUnknownList();

                assert(std::all_of(target_unknown_storage.cbegin(),
                                   target_unknown_storage.cend(),
                                   [](const auto& extended_unknown_ptr)
                                   {
                                       if (!extended_unknown_ptr)
                                           return true; // some unknowns may be dropped.

                                       return extended_unknown_ptr->GetShapeFunctionLabel() == "P1";
                                   }));
#endif // NDEBUG
            }


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_
