/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 15:21:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P2_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P2_HPP_

#include "OperatorInstances/ConformInterpolator/Internal/P1_to_Phigher.hpp"
#include "OperatorInstances/ConformInterpolator/Local/P1_to_P2.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        //! Alias that defines P1 -> P2 interpolator.
        using P1_to_P2 = Internal::ConformInterpolatorNS::P1_to_Phigher<Local::P1_to_P2>;


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P2_HPP_
