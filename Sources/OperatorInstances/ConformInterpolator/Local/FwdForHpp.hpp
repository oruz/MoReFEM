//! \file
//
//
//  FwdForHpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 01/02/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_FWD_FOR_HPP_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_FWD_FOR_HPP_HPP_

#include <iosfwd> // IWYU pragma: export
#include <memory> // IWYU pragma: export
#include <vector> // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Not supported yet: https://github.com/include-what-you-use/include-what-you-use/issues/608
// namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
// namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
// namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: export

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_FWD_FOR_HPP_HPP_
