/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 17:20:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"

#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"
#include "OperatorInstances/ConformInterpolator/Local/P1_to_P1b.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            const std::string& P1_to_P1b::GetTargetShapeFunctionLabel()
            {
                static std::string ret("P1b");
                return ret;
            }


            P1_to_P1b::~P1_to_P1b() = default;


            P1_to_P1b::P1_to_P1b(const FEltSpace& p1_felt_space,
                                 const Internal::RefFEltNS::RefLocalFEltSpace& p1b_ref_local_felt_space,
                                 const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
            : parent(p1b_ref_local_felt_space.GetRefGeomElt(), interpolation_data)
            {
                const auto& ref_geom_elt = GetRefGeomElt();

                const auto& p1_ref_local_felt_space = parent::GetRefLocalFEltSpace(p1_felt_space, ref_geom_elt);

                auto& local_projection_matrix = GetNonCstProjectionMatrix();

                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();


                const auto& p1_ref_felt = source_data.GetCommonBasicRefFElt(p1_ref_local_felt_space);

                const auto& p1b_ref_felt = target_data.GetCommonBasicRefFElt(p1b_ref_local_felt_space);

                const auto& p1_local_node_list = p1_ref_felt.GetLocalNodeList();
                const auto& p1b_local_node_list = p1b_ref_felt.GetLocalNodeList();

#ifndef NDEBUG
                Internal::ConformInterpolatorNS::Local::Impl::AssertLocalNodeConsistency(p1_local_node_list,
                                                                                         p1b_local_node_list);
#endif // NDEBUG

                local_projection_matrix.resize({ p1b_local_node_list.size() * target_data.NunknownComponent(),
                                                 p1_local_node_list.size() * source_data.NunknownComponent() });

                local_projection_matrix.fill(0.);

                const auto Nnode_in_col = p1_local_node_list.size();
                const auto Nnode_in_row = p1b_local_node_list.size();

                assert(Nnode_in_row >= Nnode_in_col);

                // Prepare the content of the block that will be repeated for each unknown component.
                LocalMatrix block({ Nnode_in_row, Nnode_in_col });
                block.fill(0.);

                // \todo #1491 Xtensor probably has something better for that...
                for (auto i = 0ul; i < Nnode_in_col; ++i)
                    block(i, i) = 1.;

                const auto Nvertex = ref_geom_elt.Nvertex();
                assert(Nvertex > 0);
                const double value_to_set = 1. / static_cast<double>(Nvertex);

                for (auto i = Nnode_in_col; i < Nnode_in_row; ++i)
                {
#ifndef NDEBUG
                    {
                        const auto& local_node_ptr = p1b_local_node_list[i];
                        assert(!(!local_node_ptr));

                        decltype(auto) local_interface = local_node_ptr->GetLocalInterface();

                        assert(local_interface.GetNature() == ref_geom_elt.GetInteriorInterfaceNature());
                    }
#endif // NDEBUG

                    for (auto j = 0ul; j < Nvertex; ++j)
                        block(i, j) = value_to_set;
                }

                // Now repeat the block for all the relevant unknowns or components.
                FillMatrixFromNodeBlock(std::move(block));
            }


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
