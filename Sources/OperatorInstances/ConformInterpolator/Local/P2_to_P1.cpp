/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 14:59:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <string>

#include "OperatorInstances/ConformInterpolator/Local/P2_to_P1.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            const std::string& P2_to_P1::ClassName()
            {
                static std::string ret = GetSourceShapeFunctionLabel() + "_to_P1";
                return ret;
            }


            const std::string& P2_to_P1::GetSourceShapeFunctionLabel()
            {
                static std::string ret("P2");
                return ret;
            }


            P2_to_P1::P2_to_P1(const FEltSpace& source_felt_space,
                               const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                               const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
            : parent(source_felt_space, target_ref_local_felt_space, interpolation_data)
            { }


            P2_to_P1::~P2_to_P1() = default;


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
