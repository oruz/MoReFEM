/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Oct 2015 11:54:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HXX_

// IWYU pragma: private, include "OperatorInstances/ConformInterpolator/Local/P1_to_P1b.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P1B_HXX_
