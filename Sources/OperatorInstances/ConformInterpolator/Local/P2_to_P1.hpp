/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 17:20:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P2_xTO_x_P1_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P2_xTO_x_P1_HPP_

#include "OperatorInstances/ConformInterpolator/Local/FwdForHpp.hpp"
#include "OperatorInstances/ConformInterpolator/Local/Internal/Phigher_to_P1.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Ideally should be in FwdForHpp.hpp, but not yet supported:
// https://github.com/include-what-you-use/include-what-you-use/issues/608
namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: export

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            /*!
             * \brief Local operator that defines P1 -> P2 interpolation.
             */
            class P2_to_P1 : public ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = P2_to_P1;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Class name.
                static const std::string& ClassName();

                //! Label of the source shape function label.
                static const std::string& GetSourceShapeFunctionLabel();

                //! Parent.
                using parent = ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] source_felt_space \a FEltSpace with the P2 unknown.
                 * \param[in] target_ref_local_felt_space \a RefLocalFEltSpace for the P1 unknown.
                 * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
                 *
                 */
                explicit P2_to_P1(const FEltSpace& source_felt_space,
                                  const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                                  const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);


                //! Destructor.
                virtual ~P2_to_P1() override;

                //! \copydoc doxygen_hide_copy_constructor
                P2_to_P1(const P2_to_P1& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                P2_to_P1(P2_to_P1&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                P2_to_P1& operator=(const P2_to_P1& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                P2_to_P1& operator=(P2_to_P1&& rhs) = delete;

                ///@}
            };


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P2_xTO_x_P1_HPP_
