/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 15:29:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <cassert>
#include <memory>
#include <type_traits>
#include <vector>

#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace Local
            {


                namespace Impl
                {


#ifndef NDEBUG
                    void AssertLocalNodeConsistency(
                        const Advanced::LocalNode::vector_const_shared_ptr& p1_local_node_list,
                        const Advanced::LocalNode::vector_const_shared_ptr& broader_local_node_list)
                    {
                        const auto Np1_node = p1_local_node_list.size();

                        for (auto i = 0ul; i < Np1_node; ++i)
                        {
                            const auto& p1_local_node_ptr = p1_local_node_list[i];
                            assert(!(!p1_local_node_ptr));
                            const auto& local_interface = p1_local_node_ptr->GetLocalInterface();
                            const auto& p1_vertex_list = local_interface.GetVertexIndexList();
                            assert(p1_vertex_list.size() == 1ul);
                            assert(p1_vertex_list.back() == i);

                            const auto& broader_local_node_ptr = broader_local_node_list[i];
                            assert(!(!broader_local_node_ptr));
                            const auto& broader_vertex_list =
                                broader_local_node_ptr->GetLocalInterface().GetVertexIndexList();
                            assert(broader_vertex_list.size() == 1ul);
                            assert(broader_vertex_list.back() == i);
                        }
                    }
#endif // NDEBUG


                } // namespace Impl


            } // namespace Local


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
