/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 15:29:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_CHECK_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_CHECK_HPP_


#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace Local
            {


                namespace Impl
                {


#ifndef NDEBUG
                    /*!
                     * \brief Assert the local node lists are consistent, i.e. that broader one is an extension of p1
                     * one.
                     *
                     * \param[in] p1_local_node_list List of P1 local nodes.
                     * \param[in] broader_local_node_list List of all local nodes.
                     */
                    void AssertLocalNodeConsistency(
                        const Advanced::LocalNode::vector_const_shared_ptr& p1_local_node_list,
                        const Advanced::LocalNode::vector_const_shared_ptr& broader_local_node_list);
#endif // NDEBUG


                } // namespace Impl


            } // namespace Local


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_CHECK_HPP_
