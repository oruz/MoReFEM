/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 12:06:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HPP_

#include "OperatorInstances/ConformInterpolator/Local/FwdForHpp.hpp"
#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Ideally should be in FwdForHpp.hpp, but not yet supported:
// https://github.com/include-what-you-use/include-what-you-use/issues/608
namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: export

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace Local
            {


                /*!
                 * \brief Local interpolator from  P{n} to P1 when N > 1.
                 */
                class Phigher_to_P1 : public ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LocalLagrangianInterpolator
                {

                  private:
                    //! Alias to parent.
                    using parent = ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LocalLagrangianInterpolator;


                  public:
                    //! \copydoc doxygen_hide_alias_self
                    using self = Phigher_to_P1;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                  public:
                    /// \name Special members.
                    ///@{

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] source_felt_space \a FEltSpace of the source.
                     * \param[in] target_ref_local_felt_space \a RefLocalFEltSpace considered for the target.
                     * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
                     */
                    explicit Phigher_to_P1(
                        const FEltSpace& source_felt_space,
                        const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                        const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

                    //! Destructor.
                    virtual ~Phigher_to_P1() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    Phigher_to_P1(const Phigher_to_P1& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Phigher_to_P1(Phigher_to_P1&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Phigher_to_P1& operator=(const Phigher_to_P1& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Phigher_to_P1& operator=(Phigher_to_P1&& rhs) = delete;

                    ///@}
                };


            } // namespace Local


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HPP_
