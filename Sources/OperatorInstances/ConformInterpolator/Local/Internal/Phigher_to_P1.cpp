/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 12:06:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "Utilities/MatrixOrVector.hpp"
#include <cassert>
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"

#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"
#include "OperatorInstances/ConformInterpolator/Local/Internal/Phigher_to_P1.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace Local
            {


                Phigher_to_P1::~Phigher_to_P1() = default;


                Phigher_to_P1::Phigher_to_P1(
                    const FEltSpace& p_higher_felt_space,
                    const Internal::RefFEltNS::RefLocalFEltSpace& p1_ref_local_felt_space,
                    const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
                : parent(p1_ref_local_felt_space.GetRefGeomElt(), interpolation_data)
                {
                    const auto& ref_geom_elt = GetRefGeomElt();

                    const auto& p_higher_ref_local_felt_space =
                        parent::GetRefLocalFEltSpace(p_higher_felt_space, ref_geom_elt);

                    auto& local_projection_matrix = GetNonCstProjectionMatrix();

                    const auto& source_data = interpolation_data.GetSourceData();
                    const auto& target_data = interpolation_data.GetTargetData();

                    const auto& p1_ref_felt = target_data.GetCommonBasicRefFElt(p1_ref_local_felt_space);

                    const auto& p_higher_ref_felt = source_data.GetCommonBasicRefFElt(p_higher_ref_local_felt_space);

                    const auto& p_higher_local_node_list = p_higher_ref_felt.GetLocalNodeList();
                    const auto& p1_local_node_list = p1_ref_felt.GetLocalNodeList();

#ifndef NDEBUG
                    Impl::AssertLocalNodeConsistency(p1_local_node_list, p_higher_local_node_list);
#endif // NDEBUG

                    local_projection_matrix.resize(
                        { p1_local_node_list.size() * target_data.NunknownComponent(),
                          p_higher_local_node_list.size() * source_data.NunknownComponent() });
                    local_projection_matrix.fill(0.);

                    const auto Nnode_in_col = p_higher_local_node_list.size();
                    const auto Nnode_in_row = p1_local_node_list.size();
                    assert(Nnode_in_col > Nnode_in_row);

                    // Prepare the content of the block that will be repeated for each unknown component.
                    LocalMatrix block({ Nnode_in_row, Nnode_in_col });
                    block.fill(0.);

                    for (auto i = 0ul; i < Nnode_in_row; ++i)
                        block(i, i) = 1.;


                    // Now repeat the block for the others unknowns or components.
                    FillMatrixFromNodeBlock(std::move(block));
                }


            } // namespace Local


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
