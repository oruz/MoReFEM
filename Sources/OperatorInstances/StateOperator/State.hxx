/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Feb 2015 10:39:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_

// IWYU pragma: private, include "OperatorInstances/StateOperator/State.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace InterpolationOperatorNS
    {


        inline const State::DirichletDofListType& State::GetProcessorWiseDirichletDofList() const
        {
            return processor_wise_dirichlet_dof_list_;
        }


        inline bool State::AreProcessorWiseDirichletDof() const
        {
            return !processor_wise_dirichlet_dof_list_.empty();
        }


#ifndef NDEBUG


        inline std::size_t State::NexpectedProgramWiseDofIncludingDirichlet() const noexcept
        {
            assert(Nexpected_program_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nexpected_program_wise_dof_including_dirichlet_;
        }


        inline std::size_t State::NexpectedProgramWiseDofExcludingDirichlet() const noexcept
        {
            assert(Nexpected_program_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nexpected_program_wise_dof_excluding_dirichlet_;
        }


        inline std::size_t State::NexpectedProcessorWiseDofIncludingDirichlet() const noexcept
        {
            assert(Nexpected_processor_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nexpected_processor_wise_dof_including_dirichlet_;
        }


        inline std::size_t State::NexpectedProcessorWiseDofExcludingDirichlet() const noexcept
        {
            assert(Nexpected_processor_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nexpected_processor_wise_dof_excluding_dirichlet_;
        }

#endif // NDEBUG


    } // namespace InterpolationOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_
