/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Feb 2015 10:39:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <type_traits>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "OperatorInstances/StateOperator/State.hpp"


namespace MoReFEM
{


    namespace InterpolationOperatorNS
    {


        State::State(const GodOfDof& god_of_dof,
                     const NumberingSubset& numbering_subset,
                     GlobalVector& target_vector_pattern)
        {
            Construct(god_of_dof, numbering_subset, god_of_dof.GetProcessorWiseDofList(), target_vector_pattern);

#ifndef NDEBUG
            Nexpected_processor_wise_dof_including_dirichlet_ = god_of_dof.NprocessorWiseDof();
            Nexpected_program_wise_dof_including_dirichlet_ = god_of_dof.NprogramWiseDof();
#endif // NDEBUG
        }


        State::State(FEltSpace& felt_space,
                     const NumberingSubset& numbering_subset,
                     GlobalVector& target_vector_pattern)
        {
            auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
            assert((!(!god_of_dof_ptr)));
            const auto& god_of_dof = *god_of_dof_ptr;

            // From felt space we get the list of processor-wise and ghost dofs; filter the latter out.
            Dof::vector_shared_ptr processor_wise_dof_list = felt_space.GetProcessorWiseDofList();

            Construct(god_of_dof, numbering_subset, processor_wise_dof_list, target_vector_pattern);

#ifndef NDEBUG
            Nexpected_processor_wise_dof_including_dirichlet_ = processor_wise_dof_list.size();
            Nexpected_program_wise_dof_including_dirichlet_ = god_of_dof.GetMpi().AllReduce(
                Nexpected_processor_wise_dof_including_dirichlet_, Wrappers::MpiNS::Op::Sum);
#endif // NDEBUG
        }


        void State::ToState(const GlobalVector& vector_with_dirichlet_dof,
                            GlobalVector& vector_without_dirichlet_dof) const
        {
            assert(static_cast<std::size_t>(vector_with_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__))
                   == NexpectedProcessorWiseDofIncludingDirichlet());
            assert(static_cast<std::size_t>(vector_without_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__))
                   == NexpectedProcessorWiseDofExcludingDirichlet());
            assert(static_cast<std::size_t>(vector_with_dirichlet_dof.GetProgramWiseSize(__FILE__, __LINE__))
                   == NexpectedProgramWiseDofIncludingDirichlet());
            assert(static_cast<std::size_t>(vector_without_dirichlet_dof.GetProgramWiseSize(__FILE__, __LINE__))
                   == NexpectedProgramWiseDofExcludingDirichlet());

            // If there are no Dirichlet dofs on the local processor, a mere copy is enough.
            if (!AreProcessorWiseDirichletDof())
            {
                assert(vector_with_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__)
                       == vector_without_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__));

                vector_without_dirichlet_dof.Copy(vector_with_dirichlet_dof, __FILE__, __LINE__);
            } else
            {
                const auto& processor_wise_dirichlet_dof_list = GetProcessorWiseDirichletDofList();
                assert(!processor_wise_dirichlet_dof_list.empty() && "Empty case is handled separately!");

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> complete(
                    vector_with_dirichlet_dof, __FILE__, __LINE__);

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> reduced(
                    vector_without_dirichlet_dof, __FILE__, __LINE__);

                const auto Ndof_in_complete = complete.GetSize(__FILE__, __LINE__);

                auto it = processor_wise_dirichlet_dof_list.cbegin();
                auto end = processor_wise_dirichlet_dof_list.cend();

                assert(it != end && "If no processor-wise Dirichlet dofs, we shouldn't be in this else block!");

                auto next_dirichlet_pos = it->second;

                for (std::size_t i = 0ul, current_reduced_index = 0ul; i < Ndof_in_complete; ++i)
                {
                    // If current dof is not on a Dirichlet condition, keep it.
                    if (next_dirichlet_pos != i)
                        reduced[current_reduced_index++] = complete.GetValue(i);
                    // Otherwise skip it and prepare the next iteration.
                    else
                    {
                        ++it;

                        if (it == end) // all Dirichlet dofs have been encountered; set the value so that test
                                       // above will never fail.
                        {
                            next_dirichlet_pos = NumericNS::UninitializedIndex<std::size_t>();
                        } else
                            next_dirichlet_pos = it->second;
                    }
                }

                assert(it == end && "All Dirichlet dofs should have been iterated through!");
            }
        }


        void State::FromState(const GlobalVector& vector_without_dirichlet_dof,
                              GlobalVector& vector_with_dirichlet_dof) const
        {
            assert(static_cast<std::size_t>(vector_with_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__))
                   == NexpectedProcessorWiseDofIncludingDirichlet());
            assert(static_cast<std::size_t>(vector_without_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__))
                   == NexpectedProcessorWiseDofExcludingDirichlet());
            assert(static_cast<std::size_t>(vector_with_dirichlet_dof.GetProgramWiseSize(__FILE__, __LINE__))
                   == NexpectedProgramWiseDofIncludingDirichlet());
            assert(static_cast<std::size_t>(vector_without_dirichlet_dof.GetProgramWiseSize(__FILE__, __LINE__))
                   == NexpectedProgramWiseDofExcludingDirichlet());

            // If there are no Dirichlet dofs on the local processor, a mere copy is enough.
            if (vector_with_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__)
                == vector_without_dirichlet_dof.GetProcessorWiseSize(__FILE__, __LINE__))
                vector_with_dirichlet_dof.Copy(vector_without_dirichlet_dof, __FILE__, __LINE__);
            else
            {
                const auto& processor_wise_dirichlet_dof_list = GetProcessorWiseDirichletDofList();
                assert(!processor_wise_dirichlet_dof_list.empty() && "Empty case is handled separately!");

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> complete(
                    vector_with_dirichlet_dof, __FILE__, __LINE__);

                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> reduced(
                    vector_without_dirichlet_dof, __FILE__, __LINE__);

                const auto Ndof_in_complete = complete.GetSize(__FILE__, __LINE__);

                auto it = processor_wise_dirichlet_dof_list.cbegin();
                auto end = processor_wise_dirichlet_dof_list.cend();

                assert(it != end && "If no processor-wise Dirichlet dofs, we shouldn't be in this else block!");

                std::size_t next_dirichlet_pos = it->second;

                for (std::size_t i = 0ul, current_reduced_index = 0ul; i < Ndof_in_complete; ++i)
                {
                    // If current dof is not on a Dirichlet condition, keep it.
                    if (next_dirichlet_pos != i)
                        complete[i] = reduced.GetValue(current_reduced_index++);
                    // Otherwise fix its value to 0.
                    else
                    {
                        complete[i] = 0.;
                        ++it;

                        if (it == end) // all Dirichlet dofs have been encountered; set the value so that test
                                       // above will never fail.
                        {
                            next_dirichlet_pos = NumericNS::UninitializedIndex<std::size_t>();
                        } else
                            next_dirichlet_pos = it->second;
                    }
                }

                assert(it == end && "All Dirichlet dofs should have been iterated through!");
            }

            // Also update the ghost: state vector follows a different pattern than complete one.
            vector_with_dirichlet_dof.UpdateGhosts(__FILE__, __LINE__);
        }


        void State::Construct(const GodOfDof& god_of_dof,
                              const NumberingSubset& numbering_subset,
                              const Dof::vector_shared_ptr& processor_wise_dof_list,
                              GlobalVector& target_vector_pattern)
        {
            decltype(auto) processor_wise_or_ghost_dirichlet_dof_list =
                god_of_dof.GetBoundaryConditionDofList(numbering_subset);

            // Create here the list of processor-wise dofs once Dirichlet ones has been removed.
            DirichletDofListType processor_wise_dirichlet_dof_list;
            Dof::vector_shared_ptr ghost_dirichlet_dof_list;

            auto dirichlet_begin = processor_wise_or_ghost_dirichlet_dof_list.cbegin();
            auto dirichlet_end = processor_wise_or_ghost_dirichlet_dof_list.cend();
            assert(std::is_sorted(dirichlet_begin, dirichlet_end, dof_ordering_comp()));
            assert(
                std::is_sorted(processor_wise_dof_list.cbegin(), processor_wise_dof_list.cend(), dof_ordering_comp()));

            const auto Nprocessor_wise_dof = processor_wise_dof_list.size();

            // Iterate through all processor-wise dofs considered and mark those that are related to a
            // Dirichlet boundary condition.
            for (std::size_t i = 0ul; i < Nprocessor_wise_dof; ++i)
            {
                const auto& dof_ptr = processor_wise_dof_list[i];
                assert(!(!dof_ptr));

                if (std::binary_search(dirichlet_begin, dirichlet_end, dof_ptr, dof_ordering_comp()))
                {
                    auto pair = processor_wise_dirichlet_dof_list.insert({ dof_ptr, i });
                    assert(pair.second && "One dof should be inserted only once!");
                    static_cast<void>(pair);
                }
            }

            const auto Nprocessor_wise_dof_without_dirichlet =
                processor_wise_dof_list.size() - processor_wise_dirichlet_dof_list.size();

            SetProcessorWiseDirichletDofList(std::move(processor_wise_dirichlet_dof_list));

            const auto& mpi = god_of_dof.GetMpi();

            const auto Nprogram_wise_dof_without_dirichlet =
                mpi.AllReduce(Nprocessor_wise_dof_without_dirichlet, Wrappers::MpiNS::Op::Sum);

            if (mpi.Nprocessor<int>() == 1)
            {
                assert(Nprogram_wise_dof_without_dirichlet == Nprocessor_wise_dof_without_dirichlet);
                target_vector_pattern.InitSequentialVector(
                    mpi, Nprogram_wise_dof_without_dirichlet, __FILE__, __LINE__);
            } else
            {
                target_vector_pattern.InitMpiVector(mpi,
                                                    Nprocessor_wise_dof_without_dirichlet,
                                                    Nprogram_wise_dof_without_dirichlet,
                                                    __FILE__,
                                                    __LINE__);
            }

#ifndef NDEBUG
            Nexpected_program_wise_dof_excluding_dirichlet_ = Nprogram_wise_dof_without_dirichlet;
            Nexpected_processor_wise_dof_excluding_dirichlet_ = Nprocessor_wise_dof_without_dirichlet;
#endif // NDEBUG
        }


        void State::SetProcessorWiseDirichletDofList(DirichletDofListType&& processor_wise_dirichlet_dof_list)
        {
            processor_wise_dirichlet_dof_list_ = std::move(processor_wise_dirichlet_dof_list);
        }


    } // namespace InterpolationOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
