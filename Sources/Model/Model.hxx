/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 30 Oct 2013 17:29:19 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_MODEL_HXX_
#define MOREFEM_x_MODEL_x_MODEL_HXX_

// IWYU pragma: private, include "Model/Model.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <ctime>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Model/Internal/CreateMeshDataDirectory.hpp"
#include "Model/Internal/InitializeHelper.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM
{
    
    namespace FilesystemNS { class Directory; }
    namespace InputDataNS { struct TimeManager; }
    namespace TimeManagerNS::Policy { class ConstantTimeStep; }
    namespace TimeManagerNS::Policy { class VariableTimeStep; }

} // namespace MoReFEM
    
// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::Model(
        const morefem_data_type& morefem_data,
        create_domain_list_for_coords a_create_domain_list_for_coords,
        print_banner do_print_banner)
    : Crtp::CrtpMpi<Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>>(
        morefem_data.GetMpi()),
      morefem_data_(morefem_data), do_print_banner_(do_print_banner)
    {
        if (a_create_domain_list_for_coords == create_domain_list_for_coords::yes)
            Coords::SetCreateDomainListForCoords();

        // current date and time system
        time_t now = std::time(nullptr);
        char* date_time = ctime(&now);

        namespace IPL = Utilities::InputDataNS;
        using Result = InputDataNS::Result;

        static_assert(MoReFEMDataT::input_data_type::template Find<InputDataNS::TimeManager>(),
                      "A model assumes a field TimeManager is present in InputData tuple.");

        if constexpr (std::is_same<TimeManagerPolicyT, TimeManagerNS::Policy::VariableTimeStep>())
            time_manager_ =
                std::make_unique<TimeManagerInstance<TimeManagerNS::Policy::VariableTimeStep>>(morefem_data);
        else if constexpr (std::is_same<TimeManagerPolicyT, TimeManagerNS::Policy::ConstantTimeStep>())
            time_manager_ =
                std::make_unique<TimeManagerInstance<TimeManagerNS::Policy::ConstantTimeStep>>(morefem_data);
        else
        {
            assert(false && "The policy is not known!");
            exit(EXIT_FAILURE);
        }

        decltype(auto) input_data = morefem_data.GetInputData();

        display_value_ = IPL::Extract<Result::DisplayValue>::Value(input_data);

        const auto& mpi = this->GetMpi();

        if (mpi.IsRootProcessor())
        {
            decltype(auto) output_directory = GetOutputDirectory();

            {
                std::string target(output_directory);
                target += "/input_data.lua";

                FilesystemNS::File::Copy(input_data.GetInputFile(),
                                         target,
                                         FilesystemNS::File::fail_if_already_exist::no,
                                         FilesystemNS::File::autocopy::yes,
                                         __FILE__,
                                         __LINE__);
            }

            // Specify in output directory how many processors are involved.

            {
                std::string target(output_directory);
                target += "/mpi.hhdata";

                std::ofstream out;
                FilesystemNS::File::Create(out, target, __FILE__, __LINE__);

                out << "Nprocessor: " << mpi.template Nprocessor<int>() << std::endl;
            }

            // Specify the name of the model. It might be useful as same Lua file might be used with different models
            // (e.g. different time scheme or hyperelastic law for hyperelastic model).
            {
                std::string target(output_directory);
                target += "/model_name.hhdata";

                std::ofstream out;
                FilesystemNS::File::Create(out, target, __FILE__, __LINE__);

                out << DerivedT::ClassName() << std::endl;
            }
        }

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n================================================================\n", mpi, __FILE__, __LINE__);

            std::ostringstream oconv;
            oconv << "MoReFEM ";
            oconv << 8 * sizeof(void*);
            oconv << " bits ";
            Wrappers::Petsc::PrintMessageOnFirstProcessor(oconv.str().c_str(), mpi, __FILE__, __LINE__);

            if (mpi.template Nprocessor<int>() == 1)
                Wrappers::Petsc::PrintMessageOnFirstProcessor("on 1 processor \n", mpi, __FILE__, __LINE__);
            else
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "on %d processors \n", mpi, __FILE__, __LINE__, mpi.template Nprocessor<int>());

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "%s=================================================================\n",
                mpi,
                __FILE__,
                __LINE__,
                date_time);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::~Model()
    {
        const auto& mpi = this->GetMpi();

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\nIf no exception, all results have been printed in %s.\n",
                                                          mpi,
                                                          __FILE__,
                                                          __LINE__,
                                                          GetOutputDirectory().GetPath().c_str());

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n==============================================================\n", mpi, __FILE__, __LINE__);
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "MoReFEM %s ended (if exception thrown it will appear afterwards).\n",
                mpi,
                __FILE__,
                __LINE__,
                DerivedT::ClassName().c_str());

            // current date and time system
            time_t now = time(nullptr);
            char* date_time = ctime(&now);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "%s==============================================================\n",
                mpi,
                __FILE__,
                __LINE__,
                date_time);
        }


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
        if (mpi.template Nprocessor<int>() > 1 && mpi.IsRootProcessor())
            ::MoReFEM::Internal::Wrappers::Petsc::CheckUpdateGhostManager::GetInstance(__FILE__, __LINE__).Print();
#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
    }


    template<class DerivedT,
             class MoReFEMDataT,
             DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
             class TimeManagerPolicyT>
    bool Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::HasFinished()
    {
        auto& time_manager = GetNonCstTimeManager();

        if (time_manager.HasFinished())
            return true;

        if (static_cast<const DerivedT&>(*this).SupplHasFinishedConditions())
            return true;

        return false;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::Initialize()
    {
        const auto& mpi = this->GetMpi();

        decltype(auto) morefem_data = this->GetMoReFEMData();
        decltype(auto) input_data = morefem_data.GetInputData();

        const bool is_root_processor = mpi.IsRootProcessor();

        const auto parallelism = morefem_data.GetParallelismPtr();

        // Init all the relevant objects found in the input data file.
        // Ordering is important here: do not modify it lightly!

        {
            auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);

            if (!(!parallelism)
                && parallelism->GetParallelismStrategy() == Advanced::parallelism_strategy::run_from_preprocessed)
                Advanced::SetFromPreprocessedData<>(morefem_data, manager);
            else
                Advanced::SetFromInputData<>(input_data, manager);
        }

        auto mesh_directory_storage = Internal::ModelNS::CreateMeshDataDirectory(GetOutputDirectory());
        mpi.Barrier();

        {
            auto& manager = DomainManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager);
        }


        {
            auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        if (Coords::GetCreateDomainListForCoords() == create_domain_list_for_coords::yes)
            CreateDomainListForCoords();

        // Advanced::SetFromInputData<Internal::PseudoNormalsManager>(input_data); #938 not activated for the moment.
        {
            auto& manager = UnknownManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager);
        }


        // Write the list of unknowns.
        // #289 We assume here there is one model, as the target file gets a hardcoded name...
        if (is_root_processor)
            WriteUnknownList(GetOutputDirectory());

        {
            auto& manager =
                Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager = GodOfDofManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, manager, mpi);
        }

        Internal::ModelNS::InitAllGodOfDofs(
            morefem_data, DoConsiderProcessorWiseLocal2GlobalT, std::move(mesh_directory_storage));

#ifndef NDEBUG
        {
            AssertNCoordsConsistency(mpi);
        }
#endif // NDEBUG

        Internal::MeshNS::WriteInterfaceListForEachMesh(mesh_directory_storage);

        // As FiberListManager gets a constructor with arguments, call it explicitly there.
        decltype(auto) time_manager = GetTimeManager();

        {
            auto& manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::scalar>::CreateOrGetInstance(__FILE__,
                                                                                                      __LINE__,
                                                                                                      time_manager);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                      ParameterNS::Type::scalar>::CreateOrGetInstance(__FILE__,
                                                                                                      __LINE__,
                                                                                                      time_manager);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::vector>::CreateOrGetInstance(__FILE__,
                                                                                                      __LINE__,
                                                                                                      time_manager);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                      ParameterNS::Type::vector>::CreateOrGetInstance(__FILE__,
                                                                                                      __LINE__,
                                                                                                      time_manager);
            Advanced::SetFromInputData<>(input_data, manager);
        }

        {
            auto& manager =
                Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(
                input_data, manager, Internal::ModelNS::ComputeCoordsMatchingInformations(input_data));
        }

        // If the point was just to write preprocessed data; stop the program here.
        // I put this before the eventual additional steps required by the Model: all the partition is complete
        // at this point so there should be no need to run SupplInitialize() first.
        // In the case I'm wrong, the call may be put later but in this case in derived model one should be put inside
        // SupplInitialize(): we do not want for instance intialization step such as the computing of the static
        // case to occur.
        PrecomputeExit(morefem_data);

        static_cast<DerivedT&>(*this).SupplInitialize();

        if (do_clear_god_of_dof_temporary_data_after_initialize_)
            ClearGodOfDofTemporaryData();

        ClearAllBoundaryConditionInitialValueList();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::InitializeStep()
    {
        // Update time for current time step.
        UpdateTime();

        // Print time information
        if (DoPrintBanner() && do_print_new_time_iteration_banner_)
        {
            if ((this->GetTimeManager().NtimeModified() % GetDisplayValue()) == 0)
            {
                PrintNewTimeIterationBanner();
            }
        }

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplInitializeStep();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::FinalizeStep()
    {
        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalizeStep();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::Finalize()
    {
        const auto& mpi = this->GetMpi();

        decltype(auto) time_keep = TimeKeep::GetInstance(__FILE__, __LINE__);

        std::string time_end = time_keep.TimeElapsedSinceBeginning();

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "Time of execution : %s.\n", mpi, __FILE__, __LINE__, time_end.c_str());
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "----------------------------------------------\n", mpi, __FILE__, __LINE__);
        }

        // Destroy manually some singletons that would blow up at the end of the program: due to a dependancy
        // their content must be destroyed before the call to Mpi::Finalize(), which occurs before the natural end of
        // singletons.
        FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::GetInstance(__FILE__,
                                                                                                              __LINE__)
            .Destroy();
        FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::GetInstance(__FILE__,
                                                                                                              __LINE__)
            .Destroy();

        FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::scalar>::GetInstance(
            __FILE__, __LINE__)
            .Destroy();
        FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::vector>::GetInstance(
            __FILE__, __LINE__)
            .Destroy();


        time_keep.PrintEndProgram();

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalize();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::Run()
    {
        auto& crtp_helper = static_cast<DerivedT&>(*this);

        crtp_helper.Initialize();

        while (!crtp_helper.HasFinished())
        {
            crtp_helper.InitializeStep();
            crtp_helper.Forward();
            crtp_helper.FinalizeStep();
        }

        crtp_helper.Finalize();
    }


    ////////////////////////
    // ACCESSORS          //
    ////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline const Mesh& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetMesh(
        std::size_t mesh_index) const
    {
        return Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetMesh(mesh_index);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline Mesh& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetNonCstMesh(
        std::size_t mesh_index) const
    {
        return Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetNonCstMesh(mesh_index);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    const GodOfDof&
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetGodOfDof(
        std::size_t unique_id) const
    {
        return GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDof(unique_id);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline TimeManager& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::
        GetNonCstTimeManager() noexcept
    {
        assert(!(!time_manager_));
        return *time_manager_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline const TimeManager&
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetTimeManager()
        const noexcept
    {
        assert(!(!time_manager_));
        return *time_manager_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    const FilesystemNS::Directory&
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetOutputDirectory()
        const noexcept
    {
        return GetMoReFEMData().GetResultDirectory();
    }


    ////////////////////////
    // PRIVATE METHODS    //
    ////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::UpdateTime()
    {
        time_manager_->IncrementTime();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::
        PrintNewTimeIterationBanner() const
    {
        const auto& mpi = this->GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "\n---------------------------------------------------\n", mpi, __FILE__, __LINE__);
        Wrappers::Petsc::PrintMessageOnFirstProcessor("%s Iteration: %d, Time: %f -> %f \n",
                                                      mpi,
                                                      __FILE__,
                                                      __LINE__,
                                                      DerivedT::ClassName().c_str(),
                                                      time_manager_->NtimeModified(),
                                                      time_manager_->GetTime() - time_manager_->GetTimeStep(),
                                                      time_manager_->GetTime());
        Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "---------------------------------------------------\n", mpi, __FILE__, __LINE__);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    std::size_t
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetDisplayValue() const
    {
        return display_value_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::
        SetClearGodOfDofTemporaryDataToFalse()
    {
        do_clear_god_of_dof_temporary_data_after_initialize_ = false;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::
        SetDoPrintNewTimeIterationBanner(bool do_print) noexcept
    {
        do_print_new_time_iteration_banner_ = do_print;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline void
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::CreateDomainListForCoords()
    {
        auto& domain_manager = DomainManager::CreateOrGetInstance(__FILE__, __LINE__);

        const auto& domain_list = domain_manager.GetDomainList();

        for (const auto& domain_and_id : domain_list)
        {
            const auto& domain_ptr = domain_and_id.second;

            assert(!(!domain_ptr));

            const auto& domain = *domain_ptr;

            const auto domain_unique_id = domain_and_id.first;

            const auto& mesh = domain.GetMesh();

            const auto& geometric_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

            for (const auto& geometric_elt_ptr : geometric_elt_list)
            {
                assert(!(!geometric_elt_ptr));

                const auto& geom_elt = *geometric_elt_ptr;

                if (domain.IsGeometricEltInside(geom_elt))
                {
                    auto& coords_list_elem = geom_elt.GetCoordsList();

                    for (auto& coords_ptr : coords_list_elem)
                    {
                        assert(!(!coords_ptr));

                        auto& coords = *coords_ptr;

                        coords.AddDomainContainingCoords(domain_unique_id);
                    }
                }
            }
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    bool Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::DoPrintBanner()
        const noexcept
    {
        switch (do_print_banner_)
        {
        case print_banner::yes:
            return true;
        case print_banner::no:
            return false;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT
    >
    // clang-format on
    inline const typename Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::
        morefem_data_type&
        Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>::GetMoReFEMData()
            const noexcept
    {
        return morefem_data_;
    }


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_MODEL_x_MODEL_HXX_
