/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Jan 2018 16:52:01 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


namespace MoReFEM
{

    // Empty file created to help CMake!


} // namespace MoReFEM


/// @} // addtogroup ModelGroup
