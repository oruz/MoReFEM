/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
#define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_

// IWYU pragma: private, include "Model/Internal/InitializeHelper.hpp"

#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ModelNS
        {


            template<class MoReFEMDataT>
            void
            InitGodOfDof(const MoReFEMDataT& morefem_data,
                         std::map<std::size_t, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                         DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                         const std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&
                             mesh_output_directory_storage,
                         const std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>& coords_matching_infos,
                         GodOfDof& god_of_dof)
            {
                auto it = felt_space_list_per_god_of_dof_index.find(god_of_dof.GetUniqueId());

                if (it == felt_space_list_per_god_of_dof_index.cend())
                    throw Exception("No finite element space in GodOfDof " + std::to_string(god_of_dof.GetUniqueId()),
                                    __FILE__,
                                    __LINE__);

                auto it_output_dir = mesh_output_directory_storage.find(god_of_dof.GetUniqueId());

                assert(it_output_dir != mesh_output_directory_storage.cend());
                assert(it_output_dir->second != nullptr);

                god_of_dof.Init(morefem_data,
                                std::move(it->second),
                                do_consider_proc_wise_local_2_global,
                                *(it_output_dir->second),
                                coords_matching_infos);
            }


            template<class MoReFEMDataT>
            void
            InitEachGodOfDof(const MoReFEMDataT& morefem_data,
                             std::map<std::size_t, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                             DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                             std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                                 mesh_output_directory_storage,
                             std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>&& coords_matching_infos)
            {
                const auto& god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

                // Process the \a GodOfDof in any order... except in the case some are involved in a \a CoordsMatching.
                // In this case, make sure the target one is processed before the source one (as the latter will
                // need to keep supplementary \a NodeBearer as ghosts - and which one requires informations from the
                // former).
                std::optional<std::size_t> source_coords_matching_god_of_dof{ std::nullopt };

                if (coords_matching_infos)
                    source_coords_matching_god_of_dof = coords_matching_infos.value().GetSourceMeshId();

                for (const auto& [god_of_dof_index, god_of_dof_ptr] : god_of_dof_storage)
                {
                    if (source_coords_matching_god_of_dof && source_coords_matching_god_of_dof == god_of_dof_index)
                        continue;

                    assert(!(!god_of_dof_ptr));
                    InitGodOfDof(morefem_data,
                                 felt_space_list_per_god_of_dof_index,
                                 do_consider_proc_wise_local_2_global,
                                 mesh_output_directory_storage,
                                 std::nullopt, // no need for coords_matchings infos here
                                 *god_of_dof_ptr);
                }

                if (source_coords_matching_god_of_dof)
                {
                    auto it = god_of_dof_storage.find(source_coords_matching_god_of_dof.value());
                    assert(it != god_of_dof_storage.cend());
                    auto& god_of_dof_ptr = it->second;
                    assert(!(!god_of_dof_ptr));

                    InitGodOfDof(morefem_data,
                                 felt_space_list_per_god_of_dof_index,
                                 do_consider_proc_wise_local_2_global,
                                 mesh_output_directory_storage,
                                 coords_matching_infos,
                                 *god_of_dof_ptr);
                }
            }


            template<class MoReFEMDataT>
            void InitMostSingletonManager(const MoReFEMDataT& morefem_data)
            {
                decltype(auto) input_data = morefem_data.GetInputData();
                decltype(auto) mpi = morefem_data.GetMpi();

                {
                    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager =
                        Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager = UnknownManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager = DomainManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    auto& manager = GodOfDofManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager, mpi);
                }
            }


            template<class InputDataT>
            std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>
            ComputeCoordsMatchingInformations(const InputDataT& input_data)
            {
                if constexpr (InputDataT::template Find<::MoReFEM::InputDataNS::InterpolationFile>())
                    return std::make_optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>(input_data);
                else
                    return std::nullopt;
            }


            template<class MoReFEMDataT>
            void InitAllGodOfDofs(const MoReFEMDataT& morefem_data,
                                  DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                                  std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                                      mesh_output_directory_storage)
            {
                decltype(auto) input_data = morefem_data.GetInputData();

                auto felt_space_list_per_god_of_dof_index = FEltSpaceNS::CreateFEltSpaceList(input_data);

                InitEachGodOfDof(morefem_data,
                                 felt_space_list_per_god_of_dof_index,
                                 do_consider_proc_wise_local_2_global,
                                 std::move(mesh_output_directory_storage),
                                 ComputeCoordsMatchingInformations(input_data));
            }


        } // namespace ModelNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
