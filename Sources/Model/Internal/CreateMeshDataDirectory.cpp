//! \file
//
//
//  CreateMeshDataDirectory.cpp
//  MoReFEM
//
//  Created by sebastien on 09/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <set>
#include <string>
#include <type_traits>

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Model/Internal/CreateMeshDataDirectory.hpp"


namespace MoReFEM::Internal::ModelNS
{


    std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>
    CreateMeshDataDirectory(const ::MoReFEM::FilesystemNS::Directory& output_directory)
    {
        std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr> ret;

        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) mesh_id_list = mesh_manager.GetUniqueIdList();

        for (const auto mesh_id : mesh_id_list)
        {
            auto new_subdir = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                output_directory, std::string("Mesh_") + std::to_string(mesh_id), __FILE__, __LINE__);

            ret.insert({ mesh_id, std::move(new_subdir) });
        }

        return ret;
    }


} // namespace MoReFEM::Internal::ModelNS
