/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
#define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>
#include <vector>

#include "Core/InputData/Advanced/SetFromInputData.hpp" // IWYU pragma: keep

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ModelNS
        {


            /*!
             * \brief Create the finite element spaces and init the god of dofs with them.
             *
             * \copydoc doxygen_hide_morefem_data_param
             * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
             * \param[in] output_directory_per_mesh_index Key is the unique id of the \a Mesh (same
             * as the one for the \a GodOfDof), value the path to the associated output directory (which should have
             * already been created by \a Model class).
             */
            template<class MoReFEMDataT>
            void InitAllGodOfDofs(const MoReFEMDataT& morefem_data,
                                  DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                                  std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                                      output_directory_per_mesh_index);


            /*!
             * \brief Init one \a GodOfDof.
             *
             * \attention This function is a helper for \a InitEachGodOfDof and should not be called outside this context.
             *
             * \copydoc doxygen_hide_morefem_data_param
             * \param[in] felt_space_list_per_god_of_dof_index Key is the \a GodOfDof (or \a Mesh - there's a bijection here) index,
             *  value if the list of \a FEltSpace within the \a GodOfDof.
             * \param[in] mesh_output_directory_storage  Key is the \a GodOfDof (or \a Mesh - there's a bijection here) index, value is
             * the \a Directory related to this \a GodOfDof (or \a Mesh).
             * \copydoc doxygen_hide_coords_matching_arg
             * \param[in,out] god_of_dof The \a GodOfDof to be initialized.
             * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
             *
             */
            template<class MoReFEMDataT>
            void InitGodOfDof(const MoReFEMDataT& morefem_data,
                              std::map<std::size_t, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                              DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                              const std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&
                                  mesh_output_directory_storage,
                              const std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>& coords_matching,
                              GodOfDof& god_of_dof);


            /*!
             * \brief Init most of the singleton managers, using the data from the input data file,
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            template<class MoReFEMDataT>
            void InitMostSingletonManager(const MoReFEMDataT& morefem_data);


            /*!
             * \brief Init each god of dof with its finite element spaces.
             *
             * \internal <b><tt>[internal]</tt></b> Should not be called outside of InitAllGodOfDofs().
             * \endinternal
             *
             * \copydoc doxygen_hide_morefem_data_param
             * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
             *
             * \param[in] felt_space_list_per_god_of_dof_index List of all \a FEltSpace sort \a GodOfDof index.
             * FEltSpace have already been instantiated during this call, and are given to their proper \a GodOfDof. The
             * reference is not constant due to move semantics inside, but the output value shouldn't be considered.
             * \param[in] coords_matching_infos Specific informations required when there is a \a FromCoordsMatching operator - in these cases
             * we need to keep slightly more ghost \a Coords than usual.
             * \param[in] mesh_output_directory_storage  Key is the \a GodOfDof (or \a Mesh - there's a bijection here) index, value is
             * the \a Directory related to this \a GodOfDof (or \a Mesh).
             */
            template<class MoReFEMDataT>
            void
            InitEachGodOfDof(const MoReFEMDataT& morefem_data,
                             std::map<std::size_t, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                             DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                             std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                                 mesh_output_directory_storage,
                             std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>&& coords_matching_infos);


            /*!
             * \brief Helper function which computes the \a CoordsMatching object if required.
             *
             * \copydoc doxygen_hide_input_data_arg
             *
             * If an operator \a FromCoordsMatching is used in the model, additional \a Coords objects have to be
             * kept as ghost in both \a GodOfDof involved; they are computed here to be transmitted for the
             * \a GodOfDof reduction process.
             *
             * \return std::nullopt if no \a CoordsMatching involved, or the \a CoordsMatching to be used otherwise.
             */
            template<class InputDataT>
            std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>
            ComputeCoordsMatchingInformations(const InputDataT& input_data);


        } // namespace ModelNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#include "Model/Internal/InitializeHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
