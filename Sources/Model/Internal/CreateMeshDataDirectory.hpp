//! \file
//
//
//  CreateMeshDataDirectory.hpp
//  MoReFEM
//
//  Created by sebastien on 09/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_CREATE_MESH_DATA_DIRECTORY_HPP_
#define MOREFEM_x_MODEL_x_INTERNAL_x_CREATE_MESH_DATA_DIRECTORY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>

#include "Utilities/Filesystem/Directory.hpp"


namespace MoReFEM::Internal::ModelNS
{


    /*!
     * \brief Create on disk an output directory for each mesh.
     *
     * \param[in] output_directory Output directory in which all the model outputs are written (it is the root directory
     * of all outputs).
     *
     * \return Key is the unique id of the mesh, value the path to it.
     */
    std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>
    CreateMeshDataDirectory(const ::MoReFEM::FilesystemNS::Directory& output_directory);


} // namespace MoReFEM::Internal::ModelNS


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_CREATE_MESH_DATA_DIRECTORY_HPP_
