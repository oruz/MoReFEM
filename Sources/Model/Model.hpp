/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 16:38:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_MODEL_HPP_
#define MOREFEM_x_MODEL_x_MODEL_HPP_


#include <cstddef> // IWYU pragma: keep
#include <map>
#include <string>
#include <vector>

#include "Utilities/Containers/Tuple.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"
#include "Core/TimeManager/Policy/ConstantTimeStep.hpp" // IWYU pragma: export
#include "Core/TimeManager/Policy/VariableTimeStep.hpp" // IWYU pragma: export
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp" // only for #740...
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"                            // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp" // only for #740...

#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "FormulationSolver/VariationalFormulation.hpp" // IWYU pragma: export

#include "Model/Internal/CreateMeshDataDirectory.hpp"
#include "Model/Internal/InitializeHelper.hpp"


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_do_create_output_dir_arg
     *
     * \param[in] do_create_output_dir If yes, create the new output directory (and remove any preexisting one).
     * This is the exepcted setting for a Model. 'no' is mostly useful for PostProcessing purposes: Model classes
     * defined in PostProcessing usually use the same input data file as the Model they cover but we do not
     * want to erase the results of the computation...
     */


    //! Enum class to say whether banners are printed or not.
    enum class print_banner
    {
        yes,
        no
    };


    /*!
     * \brief Class that drives the finite element resolution.
     *
     * A main can consist only on the instantiation of a Model and a call to its Run() method.
     *
     * \tparam DerivedT Model is used as a CRTP class.
     * \tparam DoConsiderProcessorWiseLocal2GlobalT Whether the local-2-global for processor-wise data might be required
     * by at least one of the operator. In some models processor-wise is never considered (most linear models); in that
     * case it's irrelevant to use up loads of memory to store them so 'no'should be given.
     * \tparam TimeManagerPolicyT The time manager policy to use. Currently two options are open:
     * - TimeManagerNS::Policy::ConstantTimeStep
     * - TimeManagerNS::Policy::VariableTimeStep
     *
     * DerivedT is expected to define the following methods (otherwise the compilation will fail):
     * - void InitializeStep()
     * - void Forward();
     * - void SupplFinalizeStep()
     * - void SupplFinalize()
     * - void SupplInitialize();
     * - bool SupplHasFinishedConditions() const;
     *
     */
    // clang-format off
    template
    <
        class DerivedT,
        class MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT,
        class TimeManagerPolicyT = TimeManagerNS::Policy::ConstantTimeStep
    >
    // clang-format on
    class Model
    : public Crtp::CrtpMpi<Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>>
    {

      public:
        //! Alias to self.
        using self = Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT, TimeManagerPolicyT>;

        //! Alias to MoReFEMData.
        using morefem_data_type = MoReFEMDataT;


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Model drives most of MoReFEM; so its constructor is responsible for a lot the initialisation
         * steps (for instance VariationalFormulation or dof numebering are fully built there).
         *
         * \copydoc doxygen_hide_init_morefem_param
         * \param[in] a_create_domain_list_for_coords Whether the model will compute the list of domains a coord is in
         * or not. \param[in] do_print_banner If True, print text at the beginning and the end of the program. False has
         * been introduced only for the integration tests.
         */
        explicit Model(
            const morefem_data_type& morefem_data,
            create_domain_list_for_coords a_create_domain_list_for_coords = create_domain_list_for_coords::no,
            print_banner do_print_banner = print_banner::yes);


        //! Destructor.
        ~Model();

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor - deactivated.
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /*!
         * \brief Whether the model has finished all its dynamic iterations.
         *
         * \internal <b><tt>[internal]</tt></b> Part of its implementation is the responsability of DerivedT: the base
         * class just checks whether maximum time has been hit, but it is possible for some model to add additional
         * criteria through SupplHasFinishedConditions().
         * \endinternal
         *
         * \return True if all time iterations are done.
         */
        bool HasFinished();


        /*!
         * \brief Initialize the model.
         *
         * Base class only creates the GodOfDof and finite element spaces; additional functionalities
         * must be implemented in DerivedT::SupplInitialize() (this method must be defined even
         * in cases its content is null).
         *
         */
        void Initialize();


        /*!
         * \brief Initialize the dynamic step.
         *
         * Base class only print informations about time; additional functionalities
         * must be implemented in DerivedT::SupplInitializeStep() (this method must be defined even
         * in cases its content is null).
         *
         */
        void InitializeStep();


        /*!
         * \brief Finalize the dynamic step.
         *
         * Base class only asks the TimeManager object to update the time; additional functionalities
         * must be implemented in DerivedT::SupplFinalizeStep() (this method must be defined even
         * in cases its content is null).
         *
         */
        void FinalizeStep();


        /*!
         * \brief Finalize the model once the time loop is done.
         *
         *
         */
        void Finalize();


        /*!
         * \brief A convenient wrapper over the usual way a Model is used.
         *
         * A main could be just the instantiation of a Model and subsequently the call to this method.
         *
         * This method is equivalent to what is usually in a main for Verdandi:
         *
         * \code

         * model.Initialize();
         *
         * while (!HasFinished())
         * {
         *     model.InitializeStep();
         *     model.Forward();
         *     model.SupplFinalizeStep();
         * }
         *
         * model.Finalize();
         *
         * \endcode
         *
         */
        void Run();

        //! Access to the object in charge of transient data.
        const TimeManager& GetTimeManager() const noexcept;

        /*!
         * \brief Access to output directory.
         *
         */
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        //! Setter on do_print_new_time_iteration_banner_.
        //! \param[in] do_print True if you want to print the banner.
        void SetDoPrintNewTimeIterationBanner(bool do_print) noexcept;

        //! Set do_clear_god_of_dof_temporary_data_after_initialize_ to false.
        void SetClearGodOfDofTemporaryDataToFalse();

        //! Access a GodOfDof object using its unique id.
        //! \unique_id_param_in_accessor{GodOfDof}
        const GodOfDof& GetGodOfDof(std::size_t unique_id) const;

        //! Non constant access to the object in charge of transient data.
        TimeManager& GetNonCstTimeManager() noexcept;


      protected:
        //! \non_cst_accessor{Mesh which unique identifier is \a unique_id}
        //! \unique_id_param_in_accessor{Mesh}.
        const Mesh& GetMesh(std::size_t unique_id) const;

        //! \accessor{Mesh which unique identifier is \a unique_id}
        //! \unique_id_param_in_accessor{Mesh}.
        Mesh& GetNonCstMesh(std::size_t unique_id) const;


        //! Files will be written every \a GetDisplayValue() time iterations.
        std::size_t GetDisplayValue() const;

        /*!
         * \brief Accessor to the underlying \a MoReFEMData object.
         *
         * \return Constant reference to the \a MoReFEMData object.
         */
        const morefem_data_type& GetMoReFEMData() const noexcept;

      private:
        /*!
         * \brief Go the the next iteration step.
         *
         */
        void UpdateTime();

        //! Print a banner with text, the time and the iteration number
        void PrintNewTimeIterationBanner() const;

        //! Constant accessor to create_domain_list_for_coords_.
        create_domain_list_for_coords GetCreateDomainListForCoords() const noexcept;

        //! Coords will get the information of which domain they are in.
        void CreateDomainListForCoords();

        /*!
         * \brief Whether banners should be printed or not.
         *
         * \return Boolean...
         */
        bool DoPrintBanner() const noexcept;


      private:
        //! Reference to the \a MoReFEMData object.
        const morefem_data_type& morefem_data_;

        //! Transient parameters.
        TimeManager::unique_ptr time_manager_ = nullptr;

        //! Files will be written every \a display_value_ time iteration. Choose 1 to write all of them.
        std::size_t display_value_;

        /*!
         * \brief Enables to postpone the ClearGodOfDofTemporaryData to be able to create operators after the
         * initialize.
         *
         * \attention If set to false you will need to call ClearGodOfDofTemporaryData manually once you have initialize
         * all your operators. When writing a simple MoReFEM model one should not set this attribute to false it is
         * designed to be used when interfacong with another code.
         */
        bool do_clear_god_of_dof_temporary_data_after_initialize_ = true;

        /*!
         * \brief If false, the banner for each new time iteration won't be printed.
         *
         * Even if true it is second to \a do_print_banner_.
         */
        bool do_print_new_time_iteration_banner_ = true;

        //! If false, no banner at all will be printed. False is useful for test framework.
        print_banner do_print_banner_ = print_banner::yes;
    };


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#include "Model/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_MODEL_HPP_
