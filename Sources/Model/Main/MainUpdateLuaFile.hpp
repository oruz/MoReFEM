//! \file
//
//
//  Main.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HPP_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HPP_

#include <cstdlib> // IWYU pragma: export
#include <map>     // IWYU pragma: export
#include <vector>  // IWYU pragma: export

#include "Utilities/Containers/Print.hpp"               // IWYU pragma: export
#include "Utilities/Exceptions/GracefulExit.hpp"        // IWYU pragma: export
#include "Utilities/Exceptions/PrintAndAbort.hpp"       // IWYU pragma: export
#include "Utilities/InputData/RewriteInputDataFile.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    /*!
     * \brief The typical main to update Lua file for a model; its main() function should look like:
     *
     \code
     int main(int argc, char** argv)
     {
         return ModelNS::MainUpdateLuaFile<**model class**>(argc, argv);
     }
     \endcode
     *
     * \tparam ModelT The \a Model class considered.
     *
     * \param[in] argc The first argument from main() function.
     * \param[in] argv The second argument from main() function.
     *
     * \return The error code for the main.
     */
    template<class ModelT>
    int MainUpdateLuaFile(int argc, char** argv);


} // namespace MoReFEM::ModelNS


#include "Model/Main/MainUpdateLuaFile.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HPP_
