//! \file
//
//
//  Main.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_

// IWYU pragma: private, include "Model/Main/MainUpdateLuaFile.hpp"

#include "Core/InputData/InputData.hpp"

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int MainUpdateLuaFile(int argc, char** argv)
    {
        try
        {
            // We're not using the model input_data_type directly as we want to tolerate missing fields in the Lua file
            // (e.g. if a new one was introduced in the tuple after the first version of the Lua file was generated).
            using tuple = typename ModelT::morefem_data_type::input_data_type::tuple;
            using input_data_type = MoReFEM::InputData<tuple, Utilities::InputDataNS::allow_invalid_lua_file::yes>;
            using morefem_data_type = MoReFEMData<input_data_type, program_type::update_lua_file>;

            morefem_data_type morefem_data(argc, argv);
            RewriteInputDataFile(morefem_data.GetInputData());
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData<InputData> - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_
