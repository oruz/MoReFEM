//! \file
//
//
//  Main.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HPP_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HPP_

#include <cstddef> // IWYU pragma: keep // IWYU pragma: export
#include <cstdlib> // IWYU pragma: export
#include <iosfwd>  // IWYU pragma: export
#include <map>     // IWYU pragma: export
#include <string>  // IWYU pragma: export
#include <vector>  // IWYU pragma: export

#include "Utilities/Exceptions/GracefulExit.hpp"  // IWYU pragma: export
#include "Utilities/Exceptions/PrintAndAbort.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp" // IWYU pragma: export

#include "Core/InputData/Advanced/SetFromInputData.hpp"             // IWYU pragma: export
#include "Core/InputData/Instances/Crtp/Section.hpp"                // IWYU pragma: export
#include "Core/MoReFEMData/MoReFEMData.hpp"                         // IWYU pragma: export
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp" // IWYU pragma: export

#include "Geometry/Mesh/Internal/MeshManager.hpp" // IWYU pragma: export

#include "PostProcessing/OutputFormat/Ensight6.hpp" // IWYU pragma: export
#include "PostProcessing/PostProcessing.hpp"        // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    /*!
     * \brief The typical main for the Ensight output executable related to model; its main() function should look like:
     *
     \code
     int main(int argc, char** argv)
     {
         std::vector<std::size_t> numbering_subset_id_list
         {
             EnumUnderlyingType(NumberingSubsetIndex::monolithic)
         };

         std::vector<std::string> unknown_list
         {
             "solid_displacement"
         };

        return ModelNS::MainEnsightOutput<**your model class**>(argc, argv,
                                                                mesh_index,
                                                                numbering_subset_id_list,
                                                                unknown_list);
     }
     \endcode
     *
     * \note The function is called for one \a Mesh; it is trivial of course to call in one main one instance of current
     function per \a Mesh.
     *
     * \tparam InputDataTypeT The \a input data type considered (typically an instantiation of
     MoReFEM::InputData<TupleT> class)
     *
     * \param[in] argc The first argument from main() function.
     * \param[in] argv The second argument from main() function.
     * \param[in] mesh_index Unique of of the \a Mesh considered.
     * \param[in] numbering_subset_id_list List of \a NumberingSubset considered.
     * \param[in] unknown_list List of \a Unknown considered. This must be the same size as \a numbering_subset_id_list:
     the i-th element of one is
     * related to the i-th element of the other (for instance with the code abbove \a NumberingSubset associated to
     'solid_displacement' is
     * 'NumberingSubsetIndex::monolithic'.
     *
     * \return The error code for the main.
     */
    template<class InputDataTypeT>
    int MainEnsightOutput(int argc,
                          char** argv,
                          std::size_t mesh_index,
                          const std::vector<std::size_t>& numbering_subset_id_list,
                          const std::vector<std::string>& unknown_list);


} // namespace MoReFEM::ModelNS


#include "Model/Main/MainEnsightOutput.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HPP_
