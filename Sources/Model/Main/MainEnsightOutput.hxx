//! \file
//
//
//  Main.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_

// IWYU pragma: private, include "Model/Main/MainEnsightOutput.hpp"

#include <cstddef>   // IWYU pragma: keep // IWYU pragma: export
#include <exception> // IWYU pragma: export

namespace MoReFEM::ModelNS
{


    template<class InputDataTypeT>
    int MainEnsightOutput(int argc,
                          char** argv,
                          std::size_t mesh_index,
                          const std::vector<std::size_t>& numbering_subset_id_list,
                          const std::vector<std::string>& unknown_list)

    {
        assert(numbering_subset_id_list.size() == unknown_list.size());

        try
        {
            MoReFEMData<InputDataTypeT, program_type::post_processing, Utilities::InputDataNS::DoTrackUnusedFields::no>
                morefem_data(argc, argv);

            decltype(auto) input_data = morefem_data.GetInputData();
            decltype(auto) mpi = morefem_data.GetMpi();

            try
            {
                namespace ipl = Utilities::InputDataNS;

                using Result = InputDataNS::Result;
                decltype(auto) result_directory_path = ipl::Extract<Result::OutputDirectory>::Path(input_data);

                FilesystemNS::Directory result_directory(
                    mpi, result_directory_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);
                {
                    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);

                const Mesh& mesh = mesh_manager.GetMesh(mesh_index);

                {
                    auto& manager =
                        Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputData<>(input_data, manager);
                }

                {
                    PostProcessingNS::OutputFormat::Ensight6 ensight_output(
                        result_directory, unknown_list, numbering_subset_id_list, mesh);
                }

                std::cout << "End of Post-Processing." << std::endl;
                std::cout << TimeKeep::GetInstance(__FILE__, __LINE__).TimeElapsedSinceBeginning() << std::endl;
            }
            catch (const ExceptionNS::GracefulExit&)
            {
                return EXIT_SUCCESS;
            }
            catch (const std::exception& e)
            {
                ExceptionNS::PrintAndAbort(mpi, e.what());
            }
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData<InputData> - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }


        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_
