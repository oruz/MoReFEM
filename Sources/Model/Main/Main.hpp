//! \file
//
//
//  Main.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_HPP_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_HPP_

#include <cstdlib>

#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    /*!
     * \brief The typical main for a model; its main() function should look like:
     *
     \code
     int main(int argc, char** argv)
     {
         return ModelNS::Main<**model class**>(argc, argv);
     }
     \endcode
     *
     * \tparam ModelT The \a Model class considered.
     *
     * \param[in] argc The first argument from main() function.
     * \param[in] argv The second argument from main() function.
     *
     * \return The error code for the main.
     */
    template<class ModelT>
    int Main(int argc, char** argv);


} // namespace MoReFEM::ModelNS


#include "Model/Main/Main.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_HPP_
