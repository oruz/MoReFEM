/*!
 * \defgroup ModelGroup Model
 * 
 * \brief Module that encompass the model, which drives a MoReFEM program.
 *
 * \attention Currently model is a header-only library.
 *
 */


/// \addtogroup ModelGroup
///@{


/// \namespace MoReFEM::Internal::ModelNS
/// \brief Namespace that enclose helper classes of Model.


///@}

/*!
 * \class doxygen_hide_model_SupplHasFinishedConditions_common
 *
 * \brief Whether the model wants to add additional cases in which the Model stops (besides the reach of
 * maximum time).
 *
 */

/*!
* \class doxygen_hide_model_SupplHasFinishedConditions
*
* \copydoc doxygen_hide_model_SupplHasFinishedConditions_common
*
* \return True if one of the additional finish condition defined specifically in current \a Model is reached.
*/

/*!
 * \class doxygen_hide_model_SupplHasFinishedConditions_always_true
 *
 * \copydoc doxygen_hide_model_SupplHasFinishedConditions_common
 *
 * \return Always true (no such additional condition in this Model).
 */

