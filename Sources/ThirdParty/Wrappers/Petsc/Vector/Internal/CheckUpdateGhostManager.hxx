/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Sep 2016 22:55:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                inline const CheckUpdateGhostManager::log_type& CheckUpdateGhostManager ::GetLog() const noexcept
                {
                    return log_;
                }


                inline CheckUpdateGhostManager::log_type& CheckUpdateGhostManager ::GetNonCstLog() noexcept
                {
                    return log_;
                }


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HXX_
