/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 11:36:08 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <cstring> // required by gcc for std::memcpy.
#include <iomanip>
#include <ostream>
#include <vector>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                void PrintPerProcessor(const Vector& vector,
                                       const std::string& output_file,
                                       const char* invoking_file,
                                       int invoking_line,
                                       binary_or_ascii binary_or_ascii_choice)
                {
                    ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_only> content(
                        vector, invoking_file, invoking_line);

                    const std::size_t Nvalue = content.GetSize(invoking_file, invoking_line);

                    switch (binary_or_ascii_choice)
                    {
                    case binary_or_ascii::binary:
                    {
                        std::ofstream out;
                        FilesystemNS::File::Create(
                            out, output_file, invoking_file, invoking_line, std::ofstream::binary);

                        const auto array = content.GetArray();
                        const auto Nbits = Nvalue * sizeof(double);
                        std::vector<char> binary_values(Nbits);
                        std::memcpy(binary_values.data(), array, Nbits);
                        out.write(binary_values.data(), static_cast<std::streamsize>(Nbits));
                        out.close();
                        break;
                    }
                    case binary_or_ascii::ascii:
                    {
                        std::ofstream out;
                        FilesystemNS::File::Create(out, output_file, invoking_file, invoking_line);

                        for (std::size_t i = 0ul; i < Nvalue; ++i)
                        {
                            if (std::fabs(content.GetValue(i)) <= NumericNS::DefaultEpsilon<double>())
                                out << std::setw(12) << std::scientific << 0. << std::endl;
                            else
                                out << std::setw(12) << std::scientific << std::setprecision(20) << content.GetValue(i)
                                    << std::endl;
                        }
                        break;
                    }
                    case binary_or_ascii::from_input_data:
                    {
                        assert(false
                               && "This function should be called from Vector class, and this specific case "
                                  "should have been addressed prior to this call (see Vector::Print() for instance "
                                  "to see how).");
                        exit(EXIT_FAILURE);
                    }
                    } // switch
                }


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
