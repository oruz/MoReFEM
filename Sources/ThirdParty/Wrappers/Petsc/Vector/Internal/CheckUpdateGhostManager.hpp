/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Sep 2016 22:55:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HPP_

#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

#include <map>
#include <memory>
#include <vector>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                /*!
                 * \brief Helper object used to detect unneeded calls to UpdateGhosts().
                 *
                 * This object is to use only in debug mode when MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE macro
                 * is defined. Its purpose is to check whether there are some undue calls to
                 * Wrappers::Petsc::Vector::UpdateGhosts() somewhere down the line. It works only in parallel, as
                 * there are no ghosts ar all in sequential to detect.
                 *
                 * Whenever a useless call to Wrappers::Petsc::Vector::UpdateGhosts() is issued, there is a check
                 * whether the ghosts are actually modified or not. If not, the file and line of calls gets registered
                 * here; if a subsequent call modify the ghost values it is removed.
                 *
                 * \warning The output is produced only in root processor with data produced there; it is assumed all
                 * processors exhibit the same behaviour. This seems a rather same assumption, but you should anyway
                 * check your output isn't changed by removing one of the flagged UpdateGhosts.
                 *
                 */
                class CheckUpdateGhostManager : public Utilities::Singleton<CheckUpdateGhostManager>
                {

                  public:
                    /*!
                     * \brief Returns the name of the class (required for some Singleton-related errors).
                     *
                     * \return Name of the class.
                     */
                    static const std::string& ClassName();

                    //! Convenient alias.
                    using log_type = std::map<std::pair<std::string, int>, bool>;


                  public:
                    /*!
                     * \brief Add a new entry.
                     */
                    void UnneededCall(const std::string& file, int line);

                    /*!
                     * \brief Remove if necessary an entry.
                     */
                    void NeededCall(const std::string& file, int line);

                    //! Print the content (to call at Model destruction).
                    void Print() const;

                  private:
                    /*!
                     * \class doxygen_hide_check_update_ghost_manager_log
                     *
                     * \brief Store for each (file, line) pair in which calls to UpdateGhosts() whether they modify
                     * or not the ghost values.
                     *
                     * Key is the pair (file, line)
                     * Value is true if the call is required, false otherwise.
                     */


                    //! \copydoc doxygen_hide_check_update_ghost_manager_log
                    log_type& GetNonCstLog() noexcept;


                    //! \copydoc doxygen_hide_check_update_ghost_manager_log
                    const log_type& GetLog() const noexcept;


                  private:
                    //! \name Singleton requirements.
                    ///@{
                    //! Constructor.
                    CheckUpdateGhostManager() = default;

                    //! Destructor.
                    virtual ~CheckUpdateGhostManager() override;

                    //! Friendship declaration to Singleton template class (to enable call to constructor).
                    friend class Utilities::Singleton<CheckUpdateGhostManager>;
                    ///@}


                  private:
                    /*!
                     * \brief Store for each (file, line) pair in which calls to UpdateGhosts() whether they modify
                     * or not the ghost values.
                     *
                     * Key is the pair (file, line)
                     * Value is true if the call is required, false otherwise.
                     */
                    log_type log_;
                };


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_CHECK_UPDATE_GHOST_MANAGER_HPP_
