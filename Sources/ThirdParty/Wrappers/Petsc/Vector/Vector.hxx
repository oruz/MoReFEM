/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:00:51 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include <cassert>
#include <cstdlib>
#include <iosfwd>
#include <vector>

#include "Utilities/Miscellaneous.hpp"
#include "Utilities/OutputFormat/OutputFormat.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            template<Vector::check_non_null_ptr do_check_non_null_ptr>
            inline Vec Vector::Internal() noexcept
            {
#ifndef NDEBUG
                {
                    if constexpr (do_check_non_null_ptr == check_non_null_ptr::yes)
                        assert(petsc_vector_ != PETSC_NULL);
                }
#endif // NDEBUG

                return petsc_vector_;
            }


            template<Vector::check_non_null_ptr do_check_non_null_ptr>
            inline Vec Vector::InternalForReadOnly() const noexcept
            {
#ifndef NDEBUG
                {
                    if constexpr (do_check_non_null_ptr == check_non_null_ptr::yes)
                        assert(petsc_vector_ != PETSC_NULL);
                }
#endif // NDEBUG

                return petsc_vector_;
            }


            template<Utilities::Access AccessT>
            inline void Vector::SetValues(const std::vector<PetscInt>& indexing,
                                          const AccessVectorContent<AccessT>& local_vec,
                                          InsertMode insertOrAppend,
                                          const char* invoking_file,
                                          int invoking_line)
            {
                this->SetValues(indexing, local_vec.GetArray(), insertOrAppend, invoking_file, invoking_line);
            }


            template<MpiScale MpiScaleT>
            void Vector::Print(const Mpi& mpi,
                               const std::string& output_file,
                               const char* invoking_file,
                               int invoking_line,
                               binary_or_ascii binary_or_ascii_choice) const
            {
                if (binary_or_ascii_choice == binary_or_ascii::from_input_data)
                    binary_or_ascii_choice = Utilities::OutputFormat::GetInstance(__FILE__, __LINE__).IsBinaryOutput();

                switch (MpiScaleT)
                {
                case MpiScale::program_wise:
                {
                    switch (binary_or_ascii_choice)
                    {
                    case binary_or_ascii::ascii:
                        View(mpi, output_file, invoking_file, invoking_line, PETSC_VIEWER_ASCII_MATLAB);
                        break;
                    case binary_or_ascii::binary:
                        ViewBinary(mpi, output_file, invoking_file, invoking_line);
                        break;
                    case binary_or_ascii::from_input_data:
                    {
                        assert(false && "SHould have been handled at the beginning of current method.");
                        exit(EXIT_FAILURE);
                    }
                    }
                    break;
                }
                case MpiScale::processor_wise:
                    Internal::Wrappers::Petsc::PrintPerProcessor(
                        *this, output_file, invoking_file, invoking_line, binary_or_ascii_choice);
                    break;
                } // switch
            }


            inline void Vector::UpdateGhosts(const char* invoking_file, int invoking_line, update_ghost do_update_ghost)
            {
                switch (do_update_ghost)
                {
                case update_ghost::yes:
                    UpdateGhosts(invoking_file, invoking_line);
                    break;
                case update_ghost::no:
                    break;
                }
            }


            inline const std::vector<PetscInt>& Vector::GetGhostPadding() const noexcept
            {
                assert(!ghost_padding_.empty() && "Should only be called for Vector initialized with ghosts!");
                return ghost_padding_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_VECTOR_HXX_
