/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Jul 2014 15:37:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            template<Utilities::Access AccessT>
            inline typename VectorForAccess<AccessT>::scalar_array_type AccessVectorContent<AccessT>::GetArray() const
            {
                return values_;
            }


            template<Utilities::Access AccessT>
            inline PetscScalar AccessVectorContent<AccessT>::GetValue(std::size_t i) const
            {
                assert(values_ != NULL);
                assert(i <= static_cast<std::size_t>(vector_.GetProcessorWiseSize(__FILE__, __LINE__)));
                return values_[i];
            }


            template<Utilities::Access AccessT>
            inline std::size_t AccessVectorContent<AccessT>::GetSize(const char* invoking_file, int invoking_line) const
            {
                assert(vector_.template InternalForReadOnly<Vector::check_non_null_ptr::no>() != PETSC_NULL);
                return static_cast<std::size_t>(vector_.GetProcessorWiseSize(invoking_file, invoking_line));
            }


            template<Utilities::Access AccessT>
            PetscScalar& AccessVectorContent<AccessT>::operator[](std::size_t i)
            {
                static_assert(AccessT != Utilities::Access::read_only,
                              "This method should only be called when read/write rights are given!");
                assert(values_ != NULL);
                assert(i <= static_cast<std::size_t>(vector_.GetProcessorWiseSize(__FILE__, __LINE__)));
                return values_[i];
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HXX_
