### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Vector.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/VectorInitMethods.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/BinaryOrAscii.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
