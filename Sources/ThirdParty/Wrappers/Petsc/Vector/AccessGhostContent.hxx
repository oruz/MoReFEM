/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 12 Jun 2015 09:53:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"

#include <cassert>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            inline Vector AccessGhostContent::GetVectorWithGhost() const
            {
                assert(petsc_vector_with_ghost_ != PETSC_NULL);
                return Vector(petsc_vector_with_ghost_, false);
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HXX_
