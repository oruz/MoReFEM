/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Jul 2014 15:37:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Class used to get a glimpse over the local content of a Vector, and possibly modify them.
             *
             * \tparam AccessT Whether there are read-only or read/write rights to the content of the vector.
             * The point of it is that Petsc itself doesn't enforce constness at all; so
             *
             * In Petsc you can reach what is in the local processor by:
             *  \code
             * Vec vector;
             * ...
             * PetscScalar* values;
             * VecGetArray(vector, &values);
             * ...
             * (you can modify the values there if you like)
             * ...
             * // Do not forget to call this line at the end!!!
             * VecRestoreArray(vector, &values);
             * \endcode
             *
             * Current class is a RAII around what is above.
             *
             * \attention make sure to create object of this class locally, typically in a block, to make sure
             * VecRestoreArray is called as soon as possible!
             *
             * For instance,
             * \code
             * {  // very important brace!!!
             *
             *    AccessVectorContent<Utilities::Access::read_only> local_array(vector);
             *    PetscScalar* values = local_array.values();
             *    ...
             * }
             * \endcode
             */
            template<Utilities::Access AccessT>
            class AccessVectorContent
            {
              public:
                //! Alias over const unique_ptr.
                using const_unique_ptr = std::unique_ptr<const AccessVectorContent>;

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor
                 *
                 * \param[in] vector Vector for which local access is granted.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * NOTE: Vector is accessed by value instead of reference because it is an alias over a pointer...
                 */
                explicit AccessVectorContent(typename VectorForAccess<AccessT>::Type& vector,
                                             const char* invoking_file,
                                             int invoking_line);

                //! Destructor
                ~AccessVectorContent();

                //! \copydoc doxygen_hide_copy_constructor
                AccessVectorContent(const AccessVectorContent& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                AccessVectorContent(AccessVectorContent&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                AccessVectorContent& operator=(const AccessVectorContent& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                AccessVectorContent& operator=(AccessVectorContent&& rhs) = delete;

                ///@}


                //! Get a pointer to the underlying local array.
                typename VectorForAccess<AccessT>::scalar_array_type GetArray() const;

                //! Get a given value of the underlying local array.
                //! \param[in] i Index of the requested value.
                PetscScalar GetValue(std::size_t i) const;

                //! Get the number of elements in the array.
                //! \copydoc doxygen_hide_invoking_file_and_line
                std::size_t GetSize(const char* invoking_file, int invoking_line) const;

                /*!
                 * \copydoc doxygen_hide_non_const_subscript_operator
                 *
                 * \internal <b><tt>[internal]</tt></b> Compilation will fail if this method is called with
                 * AccessT set as read_only.
                 * \endinternal
                 *
                 * \note Index \a must be in [0, N[ where N is the (processor-wise) size of the underlying vector.
                 *
                 */
                PetscScalar& operator[](std::size_t i);


              private:
                /*!
                 * \brief Vector from which the value will be read.
                 *
                 */
                typename VectorForAccess<AccessT>::Type& vector_;

                /*!
                 * \brief Underlying local array. Do not free it: Petsc is in charge of doing so!
                 *
                 * Might be NULL is the vector used in constructor is empty.
                 */
                typename VectorForAccess<AccessT>::scalar_array_type values_;
            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_VECTOR_CONTENT_HPP_
