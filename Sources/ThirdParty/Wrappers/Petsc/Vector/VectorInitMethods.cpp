/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <istream>
#include <string>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/OutputFormat/ReadBinaryFile.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp" // IWYU pragma: associated
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    namespace // anonymous
    {


        //! Basic function to read content of a simple file and stores the values into a vector of double.
        //! Two different types of files are processable by this function:
        //! - Ad hoc files generated by Internal::Wrappers::Petsc::PrintPerProcessor()
        //! - Matlab files generated directly by Petsc.
        std::vector<double> ReadAsciiFileContent(const std::string& file, const char* invoking_file, int invoking_line);


    } // namespace


    Vector::Vector() : petsc_vector_(PETSC_NULL), do_petsc_destroy_(false) // Nothing is assigned yet!
    { }


    Vector::Vector(const Vec& petsc_vector, bool do_destroy_petsc)
    : petsc_vector_(petsc_vector), do_petsc_destroy_(do_destroy_petsc)
    {
        assert(petsc_vector != PETSC_NULL);
    }


    Vector::Vector(const Vector& rhs) : petsc_vector_(PETSC_NULL), do_petsc_destroy_(rhs.do_petsc_destroy_)
    {
        CompleteCopy(rhs, __FILE__, __LINE__);
    }


    void Vector::InitFromProgramWiseBinaryFile(const Mpi& mpi,
                                               std::size_t processor_wise_size,
                                               std::size_t program_wise_size,
                                               const std::vector<PetscInt>& ghost_padding,
                                               const std::string& binary_file,
                                               const char* invoking_file,
                                               int invoking_line)
    {
        assert(petsc_vector_ == PETSC_NULL);
        assert(processor_wise_size <= program_wise_size && "Detect easily invalid order in arguments...");
        do_petsc_destroy_ = true;

        Viewer viewer(mpi, binary_file, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_READ, invoking_file, invoking_line);

        if (mpi.Nprocessor<int>() > 1)
        {
            InitMpiVectorWithGhost(
                mpi, processor_wise_size, program_wise_size, ghost_padding, invoking_file, invoking_line);

            int error_code = VecLoad(petsc_vector_, viewer.GetUnderlyingPetscObject());

            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecLoad", invoking_file, invoking_line);

            mpi.Barrier();
            UpdateGhosts(invoking_file, invoking_line);
        } else
        {
            assert(processor_wise_size == program_wise_size);
            assert(ghost_padding.empty());

            InitSequentialVector(mpi, processor_wise_size, invoking_file, invoking_line);

            int error_code = VecLoad(petsc_vector_, viewer.GetUnderlyingPetscObject());

            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecLoad", invoking_file, invoking_line);
        }
    }


    Vector::~Vector()
    {
        if (do_petsc_destroy_)
        {
            assert(petsc_vector_ != PETSC_NULL);
            int error_code = VecDestroy(&petsc_vector_);
            assert(!error_code && "Error in Vec destruction."); // no exception in destructors!
            static_cast<void>(error_code);                      // to avoid arning in release compilation.
        }
    }


    void Vector::InitSequentialVector(const Mpi& mpi, std::size_t size, const char* invoking_file, int invoking_line)
    {
        assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
        int error_code = VecCreateSeq(mpi.GetCommunicator(), static_cast<PetscInt>(size), &petsc_vector_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecCreateSeq", invoking_file, invoking_line);

        do_petsc_destroy_ = true;
    }


    void Vector::InitMpiVector(const Mpi& mpi,
                               std::size_t local_size,
                               std::size_t global_size,
                               const char* invoking_file,
                               int invoking_line)
    {
        assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
        assert(local_size <= global_size
               && "If not, either sequential mode or the local and global were "
                  "provided in the wrong order.");

        int error_code = VecCreateMPI(mpi.GetCommunicator(),
                                      static_cast<PetscInt>(local_size),
                                      static_cast<PetscInt>(global_size),
                                      &petsc_vector_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecCreateMPI", invoking_file, invoking_line);

        do_petsc_destroy_ = true;
        mpi.Barrier();
    }


    void Vector::InitMpiVectorWithGhost(const Mpi& mpi,
                                        std::size_t local_size,
                                        std::size_t global_size,
                                        const std::vector<PetscInt>& ghost_padding,
                                        const char* invoking_file,
                                        int invoking_line)
    {
        assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
        assert(local_size <= global_size
               && "If not, either sequential mode or the local and global were "
                  "provided in the wrong order.");

        const PetscInt Nghost = static_cast<PetscInt>(ghost_padding.size());

        ghost_padding_ = ghost_padding;

        int error_code = VecCreateGhost(mpi.GetCommunicator(),
                                        static_cast<PetscInt>(local_size),
                                        static_cast<PetscInt>(global_size),
                                        Nghost,
                                        ghost_padding.data(),
                                        &petsc_vector_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecCreateGhost", invoking_file, invoking_line);

        do_petsc_destroy_ = true;
        mpi.Barrier();
    }


    void Vector::InitSequentialFromFile(const Mpi& mpi,
                                        const std::string& file,
                                        const char* invoking_file,
                                        int invoking_line)
    {
        assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
        assert(mpi.Nprocessor<int>() == 1 && "This method assumes sequential case!");

        std::vector<double> value_list = ReadAsciiFileContent(file, invoking_file, invoking_line);

        // Now Init the vector with the appropriate size.
        const std::size_t Nvalue = value_list.size();

        InitSequentialVector(mpi, Nvalue, invoking_file, invoking_line);

        // And fill it with the values.
        AccessVectorContent<Utilities::Access::read_and_write> content(*this, invoking_file, invoking_line);

        for (auto i = 0ul; i < Nvalue; ++i)
            content[i] = value_list[i];
    }


    void Vector::InitParallelFromProcessorWiseAsciiFile(const Mpi& mpi,
                                                        std::size_t processor_wise_size,
                                                        std::size_t program_wise_size,
                                                        const std::vector<PetscInt>& ghost_padding,
                                                        const std::string& file,
                                                        const char* invoking_file,
                                                        int invoking_line)
    {
        assert(processor_wise_size <= program_wise_size);

        InitMpiVectorWithGhost(
            mpi, processor_wise_size, program_wise_size, ghost_padding, invoking_file, invoking_line);

        const auto value_list = ReadAsciiFileContent(file, invoking_file, invoking_line);

        assert(value_list.size() == processor_wise_size);

        {
            AccessVectorContent<Utilities::Access::read_and_write> content(*this, invoking_file, invoking_line);

            const auto size = content.GetSize(invoking_file, invoking_line);
            assert(size == processor_wise_size);

            for (auto i = 0ul; i < size; ++i)
                content[i] = value_list[i];
        }

        mpi.Barrier();
        UpdateGhosts(invoking_file, invoking_line);
    }


    void Vector::InitParallelFromProcessorWiseBinaryFile(const Mpi& mpi,
                                                         std::size_t processor_wise_size,
                                                         std::size_t program_wise_size,
                                                         const std::vector<PetscInt>& ghost_padding,
                                                         const std::string& binary_file,
                                                         const char* invoking_file,
                                                         int invoking_line)
    {
        assert(processor_wise_size <= program_wise_size);

        InitMpiVectorWithGhost(
            mpi, processor_wise_size, program_wise_size, ghost_padding, invoking_file, invoking_line);

        const auto value_list = Advanced::ReadSimpleBinaryFile(binary_file, invoking_file, invoking_line);

        assert(value_list.size() == processor_wise_size);

        {
            AccessVectorContent<Utilities::Access::read_and_write> content(*this, invoking_file, invoking_line);

            const auto size = content.GetSize(invoking_file, invoking_line);
            assert(size == processor_wise_size);

            for (auto i = 0ul; i < size; ++i)
                content[i] = value_list[i];
        }

        mpi.Barrier();
        UpdateGhosts(invoking_file, invoking_line);
    }


    namespace // anonymous
    {


        std::vector<double> ReadAsciiFileContent(const std::string& file, const char* invoking_file, int invoking_line)
        {
            std::ifstream stream;
            FilesystemNS::File::Read(stream, file, invoking_file, invoking_line);

            std::string line;

            std::vector<double> ret;

            while (getline(stream, line))
            {
                // 4 lines to ignore in PETSc Matlab format.
                if (Utilities::String::StartsWith(line, "%") || Utilities::String::StartsWith(line, "Vec_")
                    || Utilities::String::StartsWith(line, "];"))
                    continue;

                // All lines are expected to be one value.
                ret.push_back(std::stod(line));
            }

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup
