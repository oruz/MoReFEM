/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 12 Nov 2013 11:55:34 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HPP_


#include <cstdio>
#include <iosfwd>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"                // IWYU pragma: export // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Print a message on the first processor only.
             *
             * \param[in] message Message to be printed on screen.
             * \copydetails doxygen_hide_mpi_param
             * \param[in] invoking_file Usually __FILE__; useful for tracking when an exception is thrown.
             * \param[in] invoking_line Usually __LINE__; useful for tracking when an exception is thrown.
             * \param[in] arguments Variadic arguments that fill the same role as printf variadic arguments.
             * So for instance if there is a '%d' in \a message an integer is expected here.
             */
            template<typename... Args>
            void PrintMessageOnFirstProcessor(const std::string& message,
                                              const Mpi& mpi,
                                              const char* invoking_file,
                                              int invoking_line,
                                              Args&&... arguments);


            /*!
             * \brief Print messages from several processors with synchronization (to prevent interleaving).
             *
             * A call to SynchronizedFlush() is mandatory to ensure all messages are printed.
             *
             * \param[in] message Message to be printed on screen.
             * \copydetails doxygen_hide_mpi_param
             * \param[in] C_file FILE object used in C. No conversion with C++ streams; however as sync_with_stdio
             * is true for std::cout, std::cerr and std::clog in all MoReFEM, you can provide here stdout, stderr
             * and stdlog respectively.
             * \param[in] invoking_file Usually __FILE__; useful for tracking when an exception is thrown.
             * \param[in] invoking_line Usually __LINE__; useful for tracking when an exception is thrown.
             * \param[in] arguments Variadic arguments that fill the same role as printf variadic arguments.
             * So for instance if there is a '%d' in \a message an integer is expected here.
             */
            template<typename... Args>
            void PrintSynchronizedMessage(const std::string& message,
                                          const Mpi& mpi,
                                          FILE* C_file,
                                          const char* invoking_file,
                                          int invoking_line,
                                          Args&&... arguments);


            /*!
             * \brief Flush synchronized messages.
             *
             * Must be called to ensure all syncrhonized messages are printed.
             *
             * \internal <b><tt>[internal]</tt></b> This function is called in Petsc RAII for all standard outputs to
             * ensure it's called at least once; however developers may need to call it more often than that.
             * \endinternal
             *
             * \copydetails doxygen_hide_mpi_param
             * \param[in] C_file FILE object used in C. No conversion with C++ streams; however as sync_with_stdio
             * is true for std::cout, std::cerr and std::clog in all MoReFEM, you can provide here stdout, stderr
             * and stdlog respectively.
             * \param[in] invoking_file Usually __FILE__; useful for tracking when an exception is thrown.
             * \param[in] invoking_line Usually __LINE__; useful for tracking when an exception is thrown.
             */
            void SynchronizedFlush(const Mpi& mpi, FILE* C_file, const char* invoking_file, int invoking_line);


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Print.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HPP_
