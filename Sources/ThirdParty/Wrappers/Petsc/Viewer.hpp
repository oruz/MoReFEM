/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Feb 2014 11:56:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_


#include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class Mpi;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        namespace Petsc
        {


            /*!
             * \brief RAII over PetscViewer object.
             */
            class Viewer
            {
              public:
                /// \name Special members.
                ///@{

                /*!
                 * \class doxygen_hide_petsc_file_mode
                 *
                 * \param[in] file_mode Same modes as in
                 * [Petsc
                 * documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscFileMode.html)
                 * , i.e.:
                 * - FILE_MODE_WRITE - create new file for binary output.
                 * - FILE_MODE_READ - open existing file for binary input.
                 * - FILE_MODE_APPEND - open existing file for binary output.
                 * Note: this field is actually not used if the format is an ascii one.
                 */


                /*!
                 * \class doxygen_hide_petsc_viewer_format
                 *
                 * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
                 * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT and PETSC_VIEWER_ASCII_* ones and
                 * PETSC_VIEWER_BINARY_MATLAB.
                 */


                /*!
                 * \brief Constructor for an ascii file.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] file Path to the file to which the Petsc object will be associated.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \copydoc doxygen_hide_petsc_viewer_format
                 * \copydoc doxygen_hide_petsc_file_mode
                 *
                 */
                Viewer(const Mpi& mpi,
                       const std::string& file,
                       PetscViewerFormat format,
                       PetscFileMode file_mode,
                       const char* invoking_file,
                       int invoking_line);


                //! Destructor.
                ~Viewer();

                //! \copydoc doxygen_hide_copy_constructor
                Viewer(const Viewer& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Viewer(Viewer&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Viewer& operator=(const Viewer& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Viewer& operator=(Viewer&& rhs) = delete;


                ///@}


                //! Access to the underlying viewer.
                PetscViewer& GetUnderlyingPetscObject();

              private:
                /*!
                 * \brief Method called by the constructor if the format is an ascii one.
                 *
                 * \param[in] file Path to the file to which the Petsc object will be associated.
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_petsc_viewer_format
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void AsciiCase(const Mpi& mpi,
                               const std::string& file,
                               PetscViewerFormat format,
                               const char* invoking_file,
                               int invoking_line);


                /*!
                 * \brief Method called by the constructor if the format is a binary one.
                 *
                 *  \param[in] file Path to the file to which the Petsc object will be associated.
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_petsc_file_mode
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void BinaryCase(const Mpi& mpi,
                                const std::string& file,
                                PetscFileMode file_mode,
                                const char* invoking_file,
                                int invoking_line);

              private:
                //! Underlying PetscViewer object (it is truly a pointer over an alias);
                PetscViewer viewer_;
            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_
