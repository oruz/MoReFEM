/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 Apr 2016 09:31:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                template<MoReFEM::Wrappers::Petsc::NonZeroPattern NonZeroPatternT>
                constexpr ::MatStructure NonZeroPatternPetsc()
                {
                    using enum_nnz = MoReFEM::Wrappers::Petsc::NonZeroPattern;

                    switch (NonZeroPatternT)
                    {
                    case enum_nnz::same:
                        return SAME_NONZERO_PATTERN;
                    case enum_nnz::subset:
                        return SUBSET_NONZERO_PATTERN;
                    case enum_nnz::different:
                        return DIFFERENT_NONZERO_PATTERN;
                    }

                    assert(false);
                    exit(EXIT_FAILURE);
                }


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HXX_
