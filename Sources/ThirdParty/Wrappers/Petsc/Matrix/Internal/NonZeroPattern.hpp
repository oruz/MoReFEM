/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 Apr 2016 09:31:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HPP_

#include <cassert>


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                /*!
                 * \brief Associate the Petsc value that match the value of MoReFEM enum class.
                 *
                 * \tparam NonZeroPatternT Value of the enum class in MoReFEM.
                 * \return The corresponding MatStructure object from Petsc.
                 */
                template<MoReFEM::Wrappers::Petsc::NonZeroPattern NonZeroPatternT>
                constexpr ::MatStructure NonZeroPatternPetsc();


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_NON_ZERO_PATTERN_HPP_
