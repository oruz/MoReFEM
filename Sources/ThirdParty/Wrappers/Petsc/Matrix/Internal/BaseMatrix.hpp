/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 12:41:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_BASE_MATRIX_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_BASE_MATRIX_HPP_


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                /*!
                 * \brief A very shallow struct from which both Matrix and ShellMatrix should inherit.
                 *
                 * \internal <b><tt>[internal]</tt></b> It is there for a unique purpose: operations such as AXPY exist
                 * for both vectors and matrices.
                 * I want to define them once for Matrix and ShellMatrix, but if I use to do so a template classes
                 * inherited from Vector should choose the template rather than the prototype featuring Vector.
                 * \endinternal
                 */

                struct BaseMatrix
                {

                  protected:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit BaseMatrix() = default;

                    //! Destructor.
                    virtual ~BaseMatrix();

                    //! \copydoc doxygen_hide_copy_constructor
                    BaseMatrix(const BaseMatrix& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    BaseMatrix(BaseMatrix&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    BaseMatrix& operator=(const BaseMatrix& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    BaseMatrix& operator=(BaseMatrix&& rhs) = default;

                    ///@}
                };


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_INTERNAL_x_BASE_MATRIX_HPP_
