/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 May 2014 14:01:04 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            inline std::size_t MatrixPattern::Nrow() const noexcept
            {
                assert(!iCSR_.empty());
                return iCSR_.size() - 1u;
            }


            inline const std::vector<PetscInt>& MatrixPattern::GetICsr() const noexcept
            {
                assert(!iCSR_.empty());
                return iCSR_;
            }

            inline const std::vector<PetscInt>& MatrixPattern::GetJCsr() const noexcept
            {
                // I used to put a condition upon jCSR_'s emptiness here, but it could hurt degenerate cases in which
                // one of the processor got very few or even no dofs to handle.
                return jCSR_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HXX_
