/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include <cassert>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            inline Mat Matrix::Internal() noexcept
            {
                assert(petsc_matrix_ != PETSC_NULL);
                return petsc_matrix_;
            }


            inline Mat Matrix::InternalForReadOnly() const noexcept
            {
                assert(petsc_matrix_ != PETSC_NULL);
                return petsc_matrix_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HXX_
