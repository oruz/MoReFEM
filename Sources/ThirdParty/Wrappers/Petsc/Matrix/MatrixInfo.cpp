/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 Apr 2016 16:09:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixInfo.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


#ifndef NDEBUG


            void MatrixInfo(const std::string& tag,
                            const Matrix& matrix,
                            const char* invoking_file,
                            int invoking_line,
                            std::ostream& stream)
            {
                MatInfo infos;

                int error_code = MatGetInfo(matrix.InternalForReadOnly(), MAT_LOCAL, &infos);


                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetInfo", invoking_file, invoking_line);

                MatType type;
                error_code = MatGetType(matrix.InternalForReadOnly(), &type);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetType", invoking_file, invoking_line);


                stream << "Informations about matrix " << tag << ':' << std::endl;

                stream << "\t- Type = " << type << std::endl;
                stream << "\t- Block size = " << infos.block_size << std::endl;
                stream << "\t- Number of non zeros (allocated | used | unneeded) = " << infos.nz_allocated << " | "
                       << infos.nz_used << " | " << infos.nz_unneeded << std::endl;
                stream << "\t- Memory allocated = " << infos.memory << std::endl;
                stream << "\t- Number of Assemblies called = " << infos.assemblies << std::endl;
                stream << "\t- Number of mallocs during MatSetValues() = " << infos.mallocs << std::endl;
                stream << "\t- Fill ratio for LU/ILU = " << infos.fill_ratio_given << " | " << infos.fill_ratio_needed
                       << std::endl;
                stream << "\t- Number of mallocs during factorization = " << infos.factor_mallocs << std::endl;
            }


#endif // NDEBUG


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
