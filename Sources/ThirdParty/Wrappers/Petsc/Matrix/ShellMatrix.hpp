/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 13:37:58 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HPP_

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/BaseMatrix.hpp"

namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Wrappers over a Petsc shell matrix.
             *
             * A shell matrix is described in Petsc as a user-defined private data storage format.
             * It is used to define indirectly a matrix used in a linear system; please have a look at FSI model to
             * see how it may be used.
             *
             * \attention Context is not stored explicitly in current class as Petsc already stores it in its matrix;
             * the type is given to enable interpretation of the void* Petsc is actually storing.
             */
            template<class ContextT>
            class ShellMatrix : private Internal::Wrappers::Petsc::BaseMatrix
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = ShellMatrix;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to context.
                using context_type = ContextT;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_parallel_matrix_args
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] mat_op MatOperation object which specifies which operation is to be redefined for the
                 * shell matrix. So far only MATOP_MULT has been redefined this way in FSI model.
                 * \param[in] context Pointer to data needed by the shell matrix routines.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                explicit ShellMatrix(const Wrappers::Mpi& mpi,
                                     std::size_t Nlocal_row,
                                     std::size_t Nlocal_column,
                                     std::size_t Nglobal_row,
                                     std::size_t Nglobal_column,
                                     ContextT* context,
                                     MatOperation mat_op,
                                     const char* invoking_file,
                                     int invoking_line);

                //! Destructor.
                ~ShellMatrix() override;

                //! \copydoc doxygen_hide_copy_constructor
                ShellMatrix(const ShellMatrix& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ShellMatrix(ShellMatrix&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ShellMatrix& operator=(const ShellMatrix& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ShellMatrix& operator=(ShellMatrix&& rhs) = delete;

                ///@}


                /*!
                 * \brief Handle over the internal Mat object.
                 *
                 * \return Internal Mat object, which is indeed a pointer in Petsc.
                 *
                 * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
                 * method should be implemented over the function that might need access to the \a Mat internal object.
                 */
                Mat Internal() noexcept;

                /*!
                 * \brief Handle over the internal Mat object - when you can guarantee the call is only to read the
                 * value, not act upon it.
                 *
                 * \return Internal Mat object, which is indeed a pointer in Petsc.
                 *
                 * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
                 * method should be implemented over the function that might need access to the \a Mat internal object.
                 */
                Mat InternalForReadOnly() const noexcept;

              private:
                /*!
                 * \brief Thin wrapper over MatShellSetOperation.
                 *
                 * \param[in] mat_op MatOperation object which specifies which operation is to be redefined for the
                 * shell matrix. So far only MATOP_MULT has been redefined this way in FSI model.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * The implementation itself is expected to be given by ContextT::ShellMatrixOperation(). This one must
                 * respect the prototype expected by Petsc (e.g. int (Mat, Vec, Vec) for MATOP_MULT) and return 0 if
                 * all is fine.
                 *
                 */
                void SetOperation(MatOperation mat_op, const char* invoking_file, int invoking_line);


              private:
                //! Underlying Petsc matrix.
                Mat petsc_matrix_;
            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Matrix/ShellMatrix.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HPP_
