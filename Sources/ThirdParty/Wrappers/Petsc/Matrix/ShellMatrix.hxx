/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 13:37:58 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/ShellMatrix.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            template<class ContextT>
            ShellMatrix<ContextT>::~ShellMatrix()
            {
                assert(petsc_matrix_ != PETSC_NULL);
                auto error_code = MatDestroy(&petsc_matrix_);
                assert(!error_code && "Error in Mat destruction."); // no exception in destructors!
                static_cast<void>(error_code);                      // to avoid warning in release compilation.
            }


            template<class ContextT>
            ShellMatrix<ContextT>::ShellMatrix(const Wrappers::Mpi& mpi,
                                               std::size_t Nlocal_row,
                                               std::size_t Nlocal_column,
                                               std::size_t Nglobal_row,
                                               std::size_t Nglobal_column,
                                               ContextT* context,
                                               MatOperation mat_op,
                                               const char* invoking_file,
                                               int invoking_line)
            {
                int error_code = MatCreateShell(mpi.GetCommunicator(),
                                                static_cast<PetscInt>(Nlocal_row),
                                                static_cast<PetscInt>(Nlocal_column),
                                                static_cast<PetscInt>(Nglobal_row),
                                                static_cast<PetscInt>(Nglobal_column),
                                                context,
                                                &petsc_matrix_);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreateShell", invoking_file, invoking_line);

                SetOperation(mat_op, invoking_file, invoking_line);
            }


            template<class ContextT>
            inline PetscInt ApplyOperation(Mat shell_matrix, Vec x, Vec y)
            {
                ContextT* context;

                auto error_code = MatShellGetContext(shell_matrix, &context);

                if (error_code)
                    throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatShellGetContext", __FILE__, __LINE__);

                context->ShellMatrixOperation(x, y);

                return 0;
            }


            template<class ContextT>
            void ShellMatrix<ContextT>::SetOperation(MatOperation mat_op, const char* invoking_file, int invoking_line)
            {
                int error_code = MatShellSetOperation(
                    Internal(), mat_op, reinterpret_cast<void (*)(void)>(ApplyOperation<ContextT>));

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatShellSetOperation", invoking_file, invoking_line);
            }


            template<class ContextT>
            inline Mat ShellMatrix<ContextT>::Internal() noexcept
            {
                assert(petsc_matrix_ != PETSC_NULL);
                return petsc_matrix_;
            }


            template<class ContextT>
            inline Mat ShellMatrix<ContextT>::InternalForReadOnly() const noexcept
            {
                assert(petsc_matrix_ != PETSC_NULL);
                return petsc_matrix_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_SHELL_MATRIX_HXX_
