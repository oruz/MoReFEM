/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 Apr 2016 09:31:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_NON_ZERO_PATTERN_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_NON_ZERO_PATTERN_HPP_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Convenient enum used to typify more strongly Petsc macro values.
             */
            enum class NonZeroPattern
            {
                same,
                different,
                subset
            };


        } // namespace Petsc


    } // namespace Wrappers


    /*!
     * \brief Convenient alias to avoid repeating the namespaces in each call.
     *
     * It should be used anyway only as template arguments of functions within this namespaces, such as
     * Wrappers::Petsc::AXPY.
     */
    using NonZeroPattern = Wrappers::Petsc::NonZeroPattern;


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hpp"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_NON_ZERO_PATTERN_HPP_
