/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 Apr 2016 16:09:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HPP_

#include <iostream>


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


#ifndef NDEBUG


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Forward declarations.
            // ============================


            class Matrix;


            // ============================
            // End of forward declarations.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            /*!
             * \brief Wrapper over MatGetInfo(), which provides informations about the structure of the matrix.
             *
             * This is intended to be used only in debug mode!
             *
             * \param[in] tag String that identifies the matrix in the output stream (useful if several calls to
             * this function are present in the same code).
             * \param[in] matrix Matrix for which informations are requested.
             * \copydoc doxygen_hide_stream_inout
             * \copydetails doxygen_hide_invoking_file_and_line
             */
            void MatrixInfo(const std::string& tag,
                            const Matrix& matrix,
                            const char* invoking_file,
                            int invoking_line,
                            std::ostream& stream = std::cout);


#endif // NDEBUG


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixInfo.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HPP_
