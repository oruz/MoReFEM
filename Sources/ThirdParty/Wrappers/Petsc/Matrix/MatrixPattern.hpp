/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Jan 2014 14:15:48 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief This class helps to create Petsc parallel matrices.
             *
             * It stores the CSR format that defines the skeleton of the matrix.
             *
             * The storage is really processor-wise per nature: each processor is only aware on the rows handled
             * directly by the local processor. However, the rows may include position for program-wise indexes handled
             * by different processors (this choice is made by PETSc).
             *
             */
            class MatrixPattern final
            {
              public:
                //! Alias to unique_ptr.
                using const_unique_ptr = std::unique_ptr<const MatrixPattern>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] non_zero_slots_per_local_row This vector yields for each local (i.e. processor-wise)
                 * row the indexes of the columns that hold non-zero terms. For instance non_zero_slots_per_local_row[0]
                 * will tell which columns are non-zero. Beware: the columns are pointed out using the PROGRAM-WISE
                 * indexes.
                 */
                explicit MatrixPattern(const std::vector<std::vector<PetscInt>>& non_zero_slots_per_local_row);


                /*!
                 * \brief Constructor from existing pattern,
                 *
                 * This is useful when loading from prepartitioned data.
                 *
                 * \param[in] iCSR i-part of the CSR pattern.
                 * \param[in] jCSR j-part of the CSR pattern.
                 */
                explicit MatrixPattern(std::vector<PetscInt>&& iCSR, std::vector<PetscInt>&& jCSR);

                //! Destructor.
                ~MatrixPattern() = default;

                //! \copydoc doxygen_hide_move_constructor
                MatrixPattern(MatrixPattern&& rhs) = default;

                //! \copydoc doxygen_hide_copy_constructor
                MatrixPattern(const MatrixPattern& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                MatrixPattern& operator=(const MatrixPattern& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                MatrixPattern& operator=(MatrixPattern&& rhs) = default;

                ///@}


                //! Get the number of (processor-wise) rows.
                std::size_t Nrow() const noexcept;

                //! Get iCSR part of the CSR pattern (see iCSR_ for more details).
                const std::vector<PetscInt>& GetICsr() const noexcept;

                //! Get jCSR part of the CSR pattern (see iCSR_ for more details).
                const std::vector<PetscInt>& GetJCsr() const noexcept;

              private:
                /*!
                 * \brief iCSR part of the CSR pattern.
                 *
                 * iCSR matrix begins with a 0, and then for each row yields the cumulative number of non-zero terms
                 * found.
                 *
                 * Hereafter is the example currently displayed in Wikipedia:
                 *
                 * [ 10 20  0  0  0  0 ]
                 * [  0 30  0 40  0  0 ]
                 * [  0  0 50 60 70  0 ]
                 * [  0  0  0  0  0 80 ]
                 *
                 * is represented by:
                 *
                 * iCSR = [ 0  2  4  7  8 ]
                 * jCSR = [ 0  1  1  3  2  3  4  5 ]
                 *
                 */
                std::vector<PetscInt> iCSR_;

                //! jCSR part of the CSR pattern (see iCSR_ for more details).
                std::vector<PetscInt> jCSR_;
            };


            /*!
             * \copydoc doxygen_hide_operator_equal
             *
             * To make two \a MatrixPattern equal, both iCSR and jCSR should match.
             */
            bool operator==(const MatrixPattern& lhs, const MatrixPattern& rhs);


            /*!
             * \copydoc doxygen_hide_std_stream_out_overload
             *
             * Indicates for each (processor-wise) row the (program-wise) positions of non--zero elements.
             */
            std::ostream& operator<<(std::ostream& stream, const MatrixPattern& rhs);


        } // namespace Petsc


    } // namespace Wrappers


    /*!
     * \brief Print the pattern in a dense matrix.
     *
     * This function is mostly for debug purposes.
     *
     * \param[in] pattern The \a MatrixPattern we want to peruse.
     *
     * \return For each (processor-wise) row, give the indexes of the non-zero elements.
     */
    std::vector<std::vector<PetscInt>> ExtractNonZeroPositionsPerRow(const Wrappers::Petsc::MatrixPattern& pattern);


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_PATTERN_HPP_
