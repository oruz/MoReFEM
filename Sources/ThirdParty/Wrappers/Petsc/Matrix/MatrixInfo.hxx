/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/MatrixInfo.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_INFO_HXX_
