//! \file
//
//
//  RAII.cpp
//  MoReFEM
//
//  Created by sebastien on 22/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
// IWYU pragma: no_include <iosfwd>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"


namespace MoReFEM::Internal::PetscNS
{


    RAII::RAII(int argc, char** argv)
    {
        ::MoReFEM::Wrappers::Mpi::InitEnvironment(argc, argv);

        mpi_ = std::make_unique<::MoReFEM::Wrappers::Mpi>(0, ::MoReFEM::Wrappers::MpiNS::Comm::World);

        // PetscOptionsSetValue(nullptr, "-snes_linesearch_type", "basic");

        int error_code = PetscInitialize(PETSC_NULL, PETSC_NULL, PETSC_NULL, "");

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "PetscInitialize", __FILE__, __LINE__);
    }


    RAII::~RAII()
    {
        int error_code = PetscFinalize();
        assert(!error_code && "Error in PetscFinalize call!"); // No exception in destructors...
        static_cast<void>(error_code);                         // avoid warning in release compilation.
    }


    const std::string& RAII::ClassName()
    {
        static std::string ret("Internal::PetscNS::RAII");
        return ret;
    }


} // namespace MoReFEM::Internal::PetscNS
