//! \file
//
//
//  RAII.hpp
//  MoReFEM
//
//  Created by sebastien on 22/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HPP_

#include <iosfwd>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::PetscNS
{


    /*!
     * \brief RAII class to initialize / close properly PETSc and mpi.
     */
    class RAII final : public Utilities::Singleton<RAII>
    {

      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] argc The first argument from main() function.
         * \param[in] argv The second argument from main() function.
         */
        explicit RAII(int argc, char** argv);

        //! Destructor.
        virtual ~RAII() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<RAII>;

        //! Name of the class.
        static const std::string& ClassName();

        ///@}

      public:
        //! Accessor to mpi.
        const ::MoReFEM::Wrappers::Mpi& GetMpi() const noexcept;

      private:
        //! Holds Mpi object.
        ::MoReFEM::Wrappers::Mpi::const_unique_ptr mpi_ = nullptr;
    };


} // namespace MoReFEM::Internal::PetscNS


#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HPP_
