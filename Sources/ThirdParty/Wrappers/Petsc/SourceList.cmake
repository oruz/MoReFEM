### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Print.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Viewer.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Print.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Print.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SnesMacro.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Viewer.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Matrix/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solver/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Vector/SourceList.cmake)
