/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Feb 2014 11:56:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <cassert>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"

#include "Utilities/String/String.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            Viewer::Viewer(const Mpi& mpi,
                           const std::string& file,
                           PetscViewerFormat format,
                           PetscFileMode file_mode,
                           const char* invoking_file,
                           int invoking_line)
            : viewer_(nullptr)
            {
                switch (format)
                {
                case PETSC_VIEWER_DEFAULT:
                case PETSC_VIEWER_ASCII_MATLAB:
                case PETSC_VIEWER_ASCII_MATHEMATICA:
                case PETSC_VIEWER_ASCII_IMPL:
                case PETSC_VIEWER_ASCII_INFO:
                case PETSC_VIEWER_ASCII_INFO_DETAIL:
                case PETSC_VIEWER_ASCII_COMMON:
                case PETSC_VIEWER_ASCII_SYMMODU:
                case PETSC_VIEWER_ASCII_INDEX:
                case PETSC_VIEWER_ASCII_DENSE:
                case PETSC_VIEWER_ASCII_MATRIXMARKET:
#if PETSC_VERSION_LT(3, 14, 0)
                case PETSC_VIEWER_ASCII_VTK:
                case PETSC_VIEWER_ASCII_VTK_CELL:
                case PETSC_VIEWER_ASCII_VTK_COORDS:
#else
                case PETSC_VIEWER_ASCII_VTK_DEPRECATED:
                case PETSC_VIEWER_ASCII_VTK_CELL_DEPRECATED:
                case PETSC_VIEWER_ASCII_VTK_COORDS_DEPRECATED:
#endif
                case PETSC_VIEWER_ASCII_PCICE:
                case PETSC_VIEWER_ASCII_PYTHON:
                case PETSC_VIEWER_ASCII_FACTOR_INFO:
                case PETSC_VIEWER_ASCII_LATEX:
                case PETSC_VIEWER_ASCII_XML:
                case PETSC_VIEWER_ASCII_GLVIS:
                case PETSC_VIEWER_ASCII_CSV:
#if PETSC_VERSION_GE(3, 15, 0)
                case PETSC_VIEWER_ASCII_FLAMEGRAPH:
#endif
                    AsciiCase(mpi, file, format, invoking_file, invoking_line);
                    break;
                case PETSC_VIEWER_BINARY_MATLAB:
                    BinaryCase(mpi, file, file_mode, invoking_file, invoking_line);
                    break;
                case PETSC_VIEWER_HDF5_PETSC:
                case PETSC_VIEWER_HDF5_VIZ:
                case PETSC_VIEWER_HDF5_XDMF:
                case PETSC_VIEWER_HDF5_MAT:
                case PETSC_VIEWER_DRAW_BASIC:
                case PETSC_VIEWER_DRAW_LG:
                case PETSC_VIEWER_DRAW_LG_XRANGE:
                case PETSC_VIEWER_DRAW_CONTOUR:
                case PETSC_VIEWER_DRAW_PORTS:
                case PETSC_VIEWER_VTK_VTS:
                case PETSC_VIEWER_VTK_VTR:
                case PETSC_VIEWER_VTK_VTU:
                case PETSC_VIEWER_NATIVE:
                case PETSC_VIEWER_NOFORMAT:
                case PETSC_VIEWER_LOAD_BALANCE:
#if PETSC_VERSION_GE(3, 14, 0)
                case PETSC_VIEWER_FAILED:
#endif
                    throw Exception("The required Petsc Viewer format is not supported yet; if you need it "
                                    "please contact the maintainers of MoReFEM library.",
                                    __FILE__,
                                    __LINE__);
                }
            }


            void Viewer::AsciiCase(const Mpi& mpi,
                                   const std::string& ascii_file,
                                   PetscViewerFormat format,
                                   const char* invoking_file,
                                   int invoking_line)
            {
                int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerCreate", invoking_file, invoking_line);

                if (format == PETSC_VIEWER_ASCII_MATLAB && !Utilities::String::EndsWith(ascii_file, ".m"))
                    throw ExceptionNS::WrongMatlabExtension(ascii_file, invoking_file, invoking_line);

                error_code = PetscViewerASCIIOpen(mpi.GetCommunicator(), ascii_file.c_str(), &viewer_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerASCIIOpen", invoking_file, invoking_line);

                error_code = PetscViewerPushFormat(viewer_, format);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerPushFormat", invoking_file, invoking_line);
            }


            void Viewer::BinaryCase(const Mpi& mpi,
                                    const std::string& file,
                                    PetscFileMode file_mode,
                                    const char* invoking_file,
                                    int invoking_line)
            {
                int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerCreate", invoking_file, invoking_line);

                error_code = PetscViewerBinaryOpen(mpi.GetCommunicator(), file.c_str(), file_mode, &viewer_);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerBinaryOpen", invoking_file, invoking_line);
            }


            Viewer::~Viewer()
            {
                assert(!(!viewer_));
                int error_code = PetscViewerDestroy(&viewer_);
                static_cast<void>(error_code); // to avoid warning in release compilation.
                assert(!error_code);
            }


            PetscViewer& Viewer::GetUnderlyingPetscObject()
            {
                assert(!(!viewer_));
                return viewer_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
