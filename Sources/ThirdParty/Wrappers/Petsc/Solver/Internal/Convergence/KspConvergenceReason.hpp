/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Sep 2016 12:01:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HPP_

#include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                //! \copydoc doxygen_hide_namespace_cluttering
                using convergence_status = ::MoReFEM::Wrappers::Petsc::convergence_status;


                /*!
                 * \brief The purpose of this family of class is to provide two informations about Ksp: whether
                 * it converged or not, and a small text that explain exactly the reason.
                 *
                 * This text might be very short: it is taken from Petsc documentation.
                 *
                 * Each specialization must provide the method:
                 *
                 * \code
                 * static constexpr convergence_status GetConvergenceStatus() noexcept;
                 * static const std::string& Explanation();
                 * \endcode
                 *
                 * \attention After updating Petsc version, you might have to
                 */
                template<KSPConvergedReason ReasonT>
                struct KspConvergenceReason;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_RTOL_NORMAL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_ATOL_NORMAL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_RTOL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_ATOL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_ITS>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_CG_NEG_CURVE>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_CG_CONSTRAINED>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_STEP_LENGTH>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_HAPPY_BREAKDOWN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_NULL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_ITS>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_DTOL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_BREAKDOWN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_BREAKDOWN_BICG>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_NONSYMMETRIC>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_INDEFINITE_PC>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_NANORINF>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct KspConvergenceReason<KSP_DIVERGED_INDEFINITE_MAT>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


#if PETSC_VERSION_LT(3, 11, 0)
                template<>
                struct KspConvergenceReason<KSP_DIVERGED_PCSETUP_FAILED>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif


#if PETSC_VERSION_GE(3, 11, 0)
                template<>
                struct KspConvergenceReason<KSP_DIVERGED_PC_FAILED>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif // PETSC_VERSION_GE(3, 11, 0)


                template<>
                struct KspConvergenceReason<KSP_CONVERGED_ITERATING>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HPP_
