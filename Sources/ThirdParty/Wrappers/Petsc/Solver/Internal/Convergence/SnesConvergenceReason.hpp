/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Sep 2016 12:01:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HPP_

#include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                //! \copydoc doxygen_hide_namespace_cluttering
                using convergence_status = ::MoReFEM::Wrappers::Petsc::convergence_status;


                /*!
                 * \brief The purpose of this family of class is to provide two informations about Snes: whether
                 * it converged or not, and a small text that explain exactly the reason.
                 *
                 * This text might be very short: it is taken from Petsc documentation.
                 *
                 * Each specialization must provide the method:
                 *
                 * \code
                 * static constexpr convergence_status GetConvergenceStatus() noexcept;
                 * static const std::string& Explanation();
                 * \endcode
                 *
                 */
                template<SNESConvergedReason ReasonT>
                struct SnesConvergenceReason;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_FNORM_ABS>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_FNORM_RELATIVE>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_SNORM_RELATIVE>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_ITS>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


#if PETSC_VERSION_LT(3, 12, 0)
                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_TR_DELTA>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_FUNCTION_DOMAIN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_FUNCTION_COUNT>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_LINEAR_SOLVE>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_FNORM_NAN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


#if PETSC_VERSION_GE(3, 8, 0)
                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_DTOL>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif // PETSC_VERSION_GE(3, 8, 0)


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_MAX_IT>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_LINE_SEARCH>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_INNER>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_LOCAL_MIN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


                template<>
                struct SnesConvergenceReason<SNES_CONVERGED_ITERATING>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };


#if PETSC_VERSION_GE(3, 11, 0)
                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_JACOBIAN_DOMAIN>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif // PETSC_VERSION_GE(3, 11, 0)


#if PETSC_VERSION_GE(3, 12, 0)
                template<>
                struct SnesConvergenceReason<SNES_DIVERGED_TR_DELTA>
                {

                    static constexpr convergence_status GetConvergenceStatus() noexcept;


                    static const std::string& Explanation();
                };
#endif // PETSC_VERSION_GE(3, 11, 0)


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HPP_
