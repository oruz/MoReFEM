/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Sep 2016 12:01:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_FNORM_ABS>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_FNORM_RELATIVE>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_SNORM_RELATIVE>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_ITS>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::unspecified;
                }


#if PETSC_VERSION_LT(3, 12, 0)
                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_TR_DELTA>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }
#endif


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_FUNCTION_DOMAIN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_FUNCTION_COUNT>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_LINEAR_SOLVE>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_FNORM_NAN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_MAX_IT>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_LINE_SEARCH>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_INNER>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


#if PETSC_VERSION_GE(3, 8, 0)
                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_DTOL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }
#endif // PETSC_VERSION_GE(3, 8, 0)


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_LOCAL_MIN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                SnesConvergenceReason<SNES_CONVERGED_ITERATING>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::pending;
                }


#if PETSC_VERSION_GE(3, 11, 0)
                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_JACOBIAN_DOMAIN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }
#endif


#if PETSC_VERSION_GE(3, 12, 0)
                inline constexpr convergence_status
                SnesConvergenceReason<SNES_DIVERGED_TR_DELTA>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }
#endif


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_SNES_CONVERGENCE_REASON_HXX_
