/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 28 Sep 2016 00:10:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_RTOL_NORMAL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_ATOL_NORMAL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_RTOL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_ATOL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_ITS>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::unspecified;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_CG_NEG_CURVE>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_CG_CONSTRAINED>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_STEP_LENGTH>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_HAPPY_BREAKDOWN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::yes; // at least I suppose...
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_NULL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_ITS>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_DTOL>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_BREAKDOWN>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_BREAKDOWN_BICG>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_NONSYMMETRIC>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_INDEFINITE_PC>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_NANORINF>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_INDEFINITE_MAT>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }


#if PETSC_VERSION_LT(3, 11, 0)
                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_PCSETUP_FAILED>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }
#endif


#if PETSC_VERSION_GE(3, 11, 0)
                inline constexpr convergence_status
                KspConvergenceReason<KSP_DIVERGED_PC_FAILED>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::no;
                }
#endif


                inline constexpr convergence_status
                KspConvergenceReason<KSP_CONVERGED_ITERATING>::GetConvergenceStatus() noexcept
                {
                    return convergence_status::pending;
                }


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_CONVERGENCE_x_KSP_CONVERGENCE_REASON_HXX_
