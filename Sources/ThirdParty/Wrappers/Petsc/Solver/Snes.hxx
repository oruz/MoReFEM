/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include <cassert>
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::Wrappers::Petsc { struct BaseMatrix; }
namespace MoReFEM::Wrappers::Petsc { class Matrix; }
namespace MoReFEM::Wrappers::Petsc { class Vector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            inline Snes::SNESFunction Snes::GetSnesFunction() const
            {
                assert(!(!snes_function_));
                return snes_function_;
            }


            inline Snes::SNESJacobian Snes::GetSnesJacobian() const
            {
                assert(!(!snes_jacobian_));
                return snes_jacobian_;
            }


            inline Snes::SNESViewer Snes::GetSnesViewer() const
            {
                assert(!(!snes_viewer_));
                return snes_viewer_;
            }


            inline Snes::SNESConvergenceTestFunction Snes::GetSnesConvergenceTestFunction() const
            {
                // Might be nullptr.
                return snes_convergence_test_function_;
            }


            inline const Internal::Wrappers::Petsc::Solver& Snes::GetSolver() const noexcept
            {
                assert(!(!solver_));
                return *solver_;
            }


            inline Internal::Wrappers::Petsc::Solver& Snes::GetNonCstSolver() noexcept
            {
                return const_cast<Internal::Wrappers::Petsc::Solver&>(GetSolver());
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            Snes::SolveLinear(const MatrixT& matrix,
                              const MatrixT& preconditioner,
                              const Vector& rhs,
                              Vector& solution,
                              const char* invoking_file,
                              int invoking_line,
                              print_solver_infos do_print_solver_infos)
            {
                KSP ksp = GetKsp(invoking_file, invoking_line);

                int error_code =
                    KSPSetOperators(ksp, matrix.InternalForReadOnly(), preconditioner.InternalForReadOnly());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPSetOperators", invoking_file, invoking_line);

                // If there was a previous call to this very function, this argument is set to PETSC_TRUE.
                // That's not what we want: matrix and preconditioner may not be the same as in previous call.
                error_code = KSPSetReusePreconditioner(ksp, PETSC_FALSE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPSetReusePreconditioner", invoking_file, invoking_line);

                GetNonCstSolver().SetSolveLinearOptions(*this, invoking_file, invoking_line);

                // This optional line (would be called in KSPSolve) can lead to more explicit messages.
                // Petsc documentation quote: 'The explicit call of this routine enables the separate monitoring of any
                // computations performed during the set up phase, such as incomplete factorization for the ILU
                // preconditioner.' (this quote has since disappeared from Petsc doc...).
                // However, this triggers a Petsc error if the matrix is a shell one, hence the test below that is
                // false for such a matrix!
                // It seems numerical values change slightly if this method is called after the
                // KSPSetReusePreconditioner that sets PETSC_TRUE below.
                if (std::is_base_of<Wrappers::Petsc::Matrix, MatrixT>())
                {
                    error_code = ::KSPSetUp(ksp);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPSetUp", invoking_file, invoking_line);
                }

                // It seems I need this step to ensure Petsc doesn't do again the preconditioning; this is weird
                // because Petsc documentation doesn't mention it at all (this function is there in cases we want
                // to force to reuse the preconditioning even if the matrix or preconditioner has changed).
                error_code = KSPSetReusePreconditioner(ksp, PETSC_TRUE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPSetReusePreconditioner", invoking_file, invoking_line);


                // Now call the overload which works assuming matrices are well-defined - they should be by now!
                SolveLinear(rhs, solution, invoking_file, invoking_line, do_print_solver_infos);
            }


            template<SNESConvergedReason SNESConvergedReasonT>
            convergence_status Snes::GetNonLinearConvergenceReasonHelper() const noexcept
            {
                using type = Internal::Wrappers::Petsc::SnesConvergenceReason<SNESConvergedReasonT>;
                convergence_reason_ = &type::Explanation();
                return type::GetConvergenceStatus();
            }


            template<KSPConvergedReason KSPConvergedReasonT>
            convergence_status Snes::GetLinearConvergenceReasonHelper() const noexcept
            {
                using type = Internal::Wrappers::Petsc::KspConvergenceReason<KSPConvergedReasonT>;
                convergence_reason_ = &type::Explanation();
                return type::GetConvergenceStatus();
            }


            inline SNES Snes::Internal() noexcept
            {
                return snes_;
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HXX_
