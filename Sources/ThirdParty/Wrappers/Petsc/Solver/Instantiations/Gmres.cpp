/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 21:30:28 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

// IWYU pragma: no_include <iosfwd>
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Gmres.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace Instantiations
            {


                Gmres::Gmres(std::size_t restart)
                : parent(solver_type::iterative), restart_(static_cast<PetscInt>(restart))
                { }


                void Gmres::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                void Gmres::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    auto ksp = snes.GetKsp(invoking_file, invoking_line);

                    auto error_code = KSPGMRESSetRestart(ksp, GetRestart());

                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPGMRESSetRestart", invoking_file, invoking_line);
                }


                void Gmres::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                const std::string& Gmres::GetPetscName() const noexcept
                {
                    static std::string ret(KSPGMRES);
                    return ret;
                }


                PetscInt Gmres::GetRestart() const noexcept
                {
                    return restart_;
                }


            } // namespace Instantiations


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
