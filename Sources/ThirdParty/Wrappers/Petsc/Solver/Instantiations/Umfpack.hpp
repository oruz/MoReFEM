/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 17:41:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_UMFPACK_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_UMFPACK_HPP_

#include <iosfwd>
#include <memory>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace Instantiations
            {


                /*!
                 * \brief Wrappers over Umfpack solver within Petsc.
                 *
                 * \attention This solver might only be used sequentially.
                 */
                class Umfpack final : public Internal::Wrappers::Petsc::Solver
                {

                  public:
                    //! Alias to parent.
                    using parent = Internal::Wrappers::Petsc::Solver;

                    //! \copydoc doxygen_hide_alias_self
                    using self = Umfpack;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;


                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Umfpack();

                    //! Destructor.
                    ~Umfpack() override = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    Umfpack(const Umfpack& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Umfpack(Umfpack&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Umfpack& operator=(const Umfpack& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Umfpack& operator=(Umfpack&& rhs) = delete;

                    ///@}

                  private:
                    /*!
                     * \copydoc doxygen_hide_solver_set_solve_linear_option
                     *
                     * Currently nothing is done at this stage for Umfpack solver.
                     */
                    void SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

                    /*!
                     * \copydoc doxygen_hide_solver_suppl_init_option
                     *
                     * Currently nothing is done at this stage for Umfpack solver.
                     */
                    void SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

                    /*!
                     * \copydoc doxygen_hide_solver_print_infos
                     *
                     * Currently nothing is done at this stage for Umfpack solver.
                     */
                    void SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const override;

                    //! \copydoc doxygen_hide_petsc_solver_name
                    const std::string& GetPetscName() const noexcept override;
                };


            } // namespace Instantiations


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Umfpack.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_UMFPACK_HPP_
