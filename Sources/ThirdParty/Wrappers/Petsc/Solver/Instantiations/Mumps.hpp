/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 17:41:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_MUMPS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_MUMPS_HPP_

#include <iosfwd>
#include <memory>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace Instantiations
            {


                /*!
                 * \brief Wrappers over Mumps solver within Petsc.
                 */
                class Mumps final : public Internal::Wrappers::Petsc::Solver
                {

                  public:
                    //! Alias to parent.
                    using parent = Internal::Wrappers::Petsc::Solver;

                    //! \copydoc doxygen_hide_alias_self
                    using self = Mumps;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;


                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Mumps();

                    //! Destructor.
                    ~Mumps() override = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    Mumps(const Mumps& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Mumps(Mumps&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Mumps& operator=(const Mumps& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Mumps& operator=(Mumps&& rhs) = delete;

                    ///@}

                  private:
                    /*!
                     * \copydoc doxygen_hide_solver_set_solve_linear_option
                     *
                     * Here is the list of MUMPS parameters to customize (values here are the result of an example):
                     *
                     * - MUMPS run parameters:
                     * - SYM (matrix type):                   0
                     * - PAR (host participation):            1
                     * - ICNTL(1) (output for error):         6
                     * - ICNTL(2) (output of diagnostic msg): 0
                     * - ICNTL(3) (output for global info):   6
                     * - ICNTL(4) (level of printing):        4
                     * - ICNTL(5) (input mat struct):         0
                     * - ICNTL(6) (matrix prescaling):        7
                     * - ICNTL(7) (sequential matrix ordering):5
                     * - ICNTL(8) (scaling strategy):        77
                     * - ICNTL(10) (max num of refinements):  0
                     * - ICNTL(11) (error analysis):          0
                     * - ICNTL(12) (efficiency control):                         1
                     * - ICNTL(13) (efficiency control):                         0
                     * - ICNTL(14) (percentage of estimated workspace increase): 20
                     * - ICNTL(18) (input mat struct):                           0
                     * - ICNTL(19) (Schur complement info):                       0
                     * - ICNTL(20) (rhs sparse pattern):                         0
                     * - ICNTL(21) (solution struct):                            0
                     * - ICNTL(22) (in-core/out-of-core facility):               0
                     * - ICNTL(23) (max size of memory can be allocated locally):0
                     * - ICNTL(24) (detection of null pivot rows):               0
                     * - ICNTL(25) (computation of a null space basis):          0
                     * - ICNTL(26) (Schur options for rhs or solution):          0
                     * - ICNTL(27) (experimental parameter):                     -24
                     * - ICNTL(28) (use parallel or sequential ordering):        1
                     * - ICNTL(29) (parallel ordering):                          2
                     * - ICNTL(30) (user-specified set of entries in inv(A)):    0
                     * - ICNTL(31) (factors is discarded in the solve phase):    0
                     * - ICNTL(33) (compute determinant):                        1
                     * - CNTL(1) (relative pivoting threshold):      0.01
                     * - CNTL(2) (stopping criterion of refinement): 1.49012e-08
                     * - CNTL(3) (absolute pivoting threshold):      1e-06
                     * - CNTL(4) (value of static pivoting):         -1.
                     * - CNTL(5) (fixation for null pivots):         0.
                     * - RINFO(1) (local estimated flops for the elimination after analysis):     [0] 1.3472e+07
                     * - RINFO(2) (local estimated flops for the assembly after factorization):   [0]  193584.
                     * - RINFO(3) (local estimated flops for the elimination after factorization): [0]  1.3472e+07
                     * - INFO(15) (estimated size of (in MB) MUMPS internal data for running numerical factorization):
                     * [0] 3
                     * - INFO(16) (size of (in MB) MUMPS internal data used during numerical factorization): [0] 3
                     * - INFO(23) (num of pivots eliminated on this processor after factorization): [0] 1000
                     * - RINFOG(1) (global estimated flops for the elimination after analysis): 1.3472e+07
                     * - RINFOG(2) (global estimated flops for the assembly after factorization): 193584.
                     * - RINFOG(3) (global estimated flops for the elimination after factorization): 1.3472e+07
                     * - (RINFOG(12) RINFOG(13))*2^INFOG(34) (determinant): (-0.9371,0.)*(2^-2486)
                     * - INFOG(3) (estimated real workspace for factors on all processors after analysis): 149572
                     * - INFOG(4) (estimated integer workspace for factors on all processors after analysis): 10090
                     * - INFOG(5) (estimated maximum front size in the complete tree): 150
                     * - INFOG(6) (number of nodes in the complete tree): 90
                     * - INFOG(7) (ordering option effectively use after analysis): 5
                     * - INFOG(8) (structural symmetry in percent of the permuted matrix after analysis): 100
                     * - INFOG(9) (total real/complex workspace to store the matrix factors after factorization): 149572
                     * - INFOG(10) (total integer space store the matrix factors after factorization): 10090
                     * - INFOG(11) (order of largest frontal matrix after factorization): 150
                     * - INFOG(12) (number of off-diagonal pivots): 0
                     * - INFOG(13) (number of delayed pivots after factorization): 0
                     * - INFOG(14) (number of memory compress after factorization): 0
                     * - INFOG(15) (number of steps of iterative refinement after solution): 0
                     * - INFOG(16) (estimated size (in MB) of all MUMPS internal data for factorization after analysis:
                     * value on the most memory consuming processor): 3
                     * - INFOG(17) (estimated size of all MUMPS internal data for factorization after analysis: sum over
                     * all processors): 3
                     * - INFOG(18) (size of all MUMPS internal data allocated during factorization: value on the most
                     * memory consuming processor): 3
                     * - INFOG(19) (size of all MUMPS internal data allocated during factorization: sum over all
                     * processors): 3
                     * - INFOG(20) (estimated number of entries in the factors): 149572
                     * - INFOG(21) (size in MB of memory effectively used during factorization - value on the most
                     * memory consuming processor): 2
                     * - INFOG(22) (size in MB of memory effectively used during factorization - sum over all
                     * processors): 2
                     * - INFOG(23) (after analysis: value of ICNTL(6) effectively used): 0
                     * - INFOG(24) (after analysis: value of ICNTL(12) effectively used): 1
                     * - INFOG(25) (after factorization: number of pivots modified by static pivoting): 0
                     * - INFOG(28) (after factorization: number of null pivots encountered): 0
                     * - INFOG(29) (after factorization: effective number of entries in the factors (sum over all
                     * processors)): 149572
                     * - INFOG(30, 31) (after solution: size in Mbytes of memory used during solution phase): 0, 0
                     * - INFOG(32) (after analysis: type of analysis done): 1
                     * - INFOG(33) (value used for ICNTL(8)): 7
                     * - INFOG(34) (exponent of the determinant if determinant is requested): -2486
                     */
                    void SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

                    /*!
                     * \copydoc doxygen_hide_solver_suppl_init_option
                     *
                     * Currently nothing is done at this stage for Mumps solver.
                     */
                    void SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

                    /*!
                     * \copydoc doxygen_hide_solver_print_infos
                     *
                     */
                    void SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const override;

                    //! \copydoc doxygen_hide_petsc_solver_name
                    const std::string& GetPetscName() const noexcept override;
                };


            } // namespace Instantiations


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Mumps.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INSTANTIATIONS_x_MUMPS_HPP_
