/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 15:45:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <sstream>
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Mumps.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace Instantiations
            {


                Mumps::Mumps() : parent(solver_type::direct)
                { }


                void Mumps::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    auto pc = snes.GetPreconditioner(invoking_file, invoking_line);

#if PETSC_VERSION_GE(3, 9, 0)
                    auto error_code = PCFactorSetUpMatSolverType(pc); /* call MatGetFactor() to create F */
#else
                    auto error_code = PCFactorSetUpMatSolverPackage(pc); /* call MatGetFactor() to create F */
#endif

                    if (error_code)
                        throw ExceptionNS::Exception(
                            error_code, "PCFactorSetUpMatSolverType", invoking_file, invoking_line);

                    Mat F;
                    error_code = PCFactorGetMatrix(pc, &F);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "PCFactorGetMatrix", invoking_file, invoking_line);

                    // Here are few values that were recommanded; for others default valuea are used.
                    // I have not throughly investigated what is best here; it could be nice at some point to
                    // make publicly available in the input data file those that could gain to be customized for
                    // a model.
                    std::vector<std::pair<PetscInt, PetscInt>> customized_icntl{
                        //{ 4, 0 }, // level of printing in 0-4
                        { 7, 5 }, // computes a symmetric permutation (ordering) to determine the pivot order to be used
                                  // for the factorization in case of sequential analysis (ICNTL(28)=1).
                                  // 5 is for Metis \todo #1015
                        { 29, 2 },
                        { 24, 1 }, // detection of null pivot rows
                        { 33, 1 }, // compute determinant of A
                    };

                    for (const auto& pair : customized_icntl)
                    {
                        error_code = MatMumpsSetIcntl(F, pair.first, pair.second);
                        if (error_code)
                        {
                            std::ostringstream oconv;
                            oconv << "MatMumpsSetIcntl with pair ";
                            Utilities::PrintTuple(pair,
                                                  oconv,
                                                  PrintNS::Delimiter::separator(", "),
                                                  PrintNS::Delimiter::opener("("),
                                                  PrintNS::Delimiter::closer(")"));

                            throw ExceptionNS::Exception(error_code, oconv.str(), invoking_file, invoking_line);
                        }
                    }

                    // Same for real values.
                    std::vector<std::pair<PetscInt, PetscScalar>> customized_cntl{ { 3, 1.e-6 } };


                    for (const auto& pair : customized_cntl)
                    {
                        error_code = MatMumpsSetCntl(F, pair.first, pair.second);
                        if (error_code)
                        {
                            std::ostringstream oconv;
                            oconv << "MatMumpsSetCntl with pair ";
                            Utilities::PrintTuple(pair,
                                                  oconv,
                                                  PrintNS::Delimiter::separator(", "),
                                                  PrintNS::Delimiter::opener("("),
                                                  PrintNS::Delimiter::closer(")"));

                            throw ExceptionNS::Exception(error_code, oconv.str(), invoking_file, invoking_line);
                        }
                    }
                }


                void Mumps::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                void Mumps::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
                {
                    auto pc = snes.GetPreconditioner(invoking_file, invoking_line);

                    Mat F;
                    int error_code = PCFactorGetMatrix(pc, &F);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "PCFactorGetMatrix", invoking_file, invoking_line);

                    PetscInt ival;
                    error_code = MatMumpsGetInfog(F, 28, &ival); // 28 is the index of the wanted info
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "MatMumpsGetInfog", invoking_file, invoking_line);

                    PrintMessageOnFirstProcessor("After factorization: number of null pivots encountered: %d.\n",
                                                 snes.GetMpi(),
                                                 invoking_file,
                                                 invoking_line,
                                                 static_cast<int>(ival));
                }


                const std::string& Mumps::GetPetscName() const noexcept
                {
                    static std::string ret(MATSOLVERMUMPS);
                    return ret;
                }


            } // namespace Instantiations


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
