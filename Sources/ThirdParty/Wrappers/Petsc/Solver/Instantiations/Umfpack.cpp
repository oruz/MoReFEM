/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 21:30:28 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Umfpack.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace Instantiations
            {


                Umfpack::Umfpack() : parent(solver_type::direct)
                { }


                void Umfpack::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                void Umfpack::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                void Umfpack::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
                {
                    static_cast<void>(snes);
                    static_cast<void>(invoking_file);
                    static_cast<void>(invoking_line);
                }


                const std::string& Umfpack::GetPetscName() const noexcept
                {
                    static std::string ret(MATSOLVERUMFPACK);
                    return ret;
                }


            } // namespace Instantiations


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
