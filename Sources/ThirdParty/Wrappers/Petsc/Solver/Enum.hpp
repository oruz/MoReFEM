/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 15:45:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Enum that tags the available solvers.
             */
            enum class SolverId
            {
                none,
                Mumps,
                Umfpack,
                Gmres
            };


            //! Convenient enum class to tag the type of solver.
            enum class solver_type
            {
                direct,
                iterative
            };


            /*!
             * \brief Enum to say whether a Newton converged or not.
             *
             * 'pending' means the Newton is still in progress. I don't know how it may happen, as Petsc doc says
             *  SNESGetConvergedReason() can only be called once SNESSolve() is complete, but as it exists in Petsc
             * enum I retranscribe it here.
             * 'unspecified' is there for SNES_CONVERGED_ITS: this is obtained when the maximum number of iterations
             * is reached but no convergence test was performed. I put it if at some point we need to introduce
             * a wrapper over SNESConvergedSkip(); in current state it should never happen.
             */
            enum class convergence_status
            {
                yes,
                no,
                pending,
                unspecified
            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_
