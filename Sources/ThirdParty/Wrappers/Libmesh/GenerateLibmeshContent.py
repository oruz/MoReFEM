def splitIntoSubTuples(n, theTuple):
	"""Split a tuple into several sub tuples of maximum length n"""
   	return [theTuple[i:i+n] for i in range(0, len(theTuple), n)]


def setSpecializationHelper(StreamOut, index):
	"""Generates the specializations of Libmesh::MeditSetLin for input index and write them in the stream"""
	StreamOut.write("template<>\n")
	StreamOut.write("void Libmesh::MeditSetLin<{0}u>(auto mesh_index, GmfKwdCod geometric_elt_code, \n".format(str(index)))
	StreamOut.write("const std::vector<int>& coords, \n")
	StreamOut.write("int label_index)\n")
	StreamOut.write("{\n")

	functionArguments = ['mesh_index', 'geometric_elt_code']
	for j in range(0, index):
		functionArguments.append('coords[{0}]'.format(j))
	functionArguments.append('label_index')

	StreamOut.write("::GmfSetLin(")
	subtupleList = splitIntoSubTuples(4, functionArguments)

	firstDone = False

	for subtuple in subtupleList:
		if firstDone:
			StreamOut.write(", \n")
		else:
			firstDone = True
		StreamOut.write(", ".join(subtuple))

	StreamOut.write(");\n")
	StreamOut.write("}\n\n\n")


def getSpecializationHelper(StreamOut, index):
	"""Generates the specializations of Libmesh::MeditGetLin for input index and write them in the stream"""
	StreamOut.write("template<>\n")
	StreamOut.write("void Libmesh::MeditGetLin<{0}u>(auto mesh_index, GmfKwdCod geometric_elt_code, \n".format(str(index)))
	StreamOut.write("std::vector<unsigned int>& coords, \n")
	StreamOut.write("int& label_index)\n")
	StreamOut.write("{\n")
	StreamOut.write("std::vector<int> int_coords(coords.size());\n\n")


	functionArguments = ['mesh_index', 'geometric_elt_code']
	for j in range(0, index):
		functionArguments.append('&int_coords[{0}]'.format(j))
	functionArguments.append('&label_index')

	StreamOut.write("::GmfGetLin(")
	subtupleList = splitIntoSubTuples(4, functionArguments)

	firstDone = False

	for subtuple in subtupleList:
		if firstDone:
			StreamOut.write(", \n")
		else:
			firstDone = True
		StreamOut.write(", ".join(subtuple))

	StreamOut.write(");\n\n")

	StreamOut.write("coords = IntToUint::Perform(int_coords);\n")

	StreamOut.write("}\n\n\n")




def generateSpecializations(FILE_out, listNumberOfCoords):
	""""
	Generates all template specializations of Libmesh::MeditSetLin and Libmesh::MeditGetLin from the list given in input.

	Typically a new specialization for 10 if you get an "Undefined symbols" message.

	- param[in] FILE_out Name of the temporary file to which the generated code will be written. Copy/paste this content into proper cpp file.
	- param[in] listNumberOfCoords List for which new specializations must be generated.

	""""
	StreamOut = open(FILE_out, 'w')

	for i in listNumberOfCoords:
		setSpecializationHelper(StreamOut, i)
		getSpecializationHelper(StreamOut, i)

	StreamOut.close()

	print FILE_out + " has been generated"



if __name__ == '__main__':

    #    FILE_out_get = open("GmfGetLinSwitchContent.tmp", "w")
    # FILE_out_set = open("GmfSetLinSwitchContent.tmp", "w")
    listNumberOfCoords = (2, 3, 4, 6, 8, 9, 10, 27)

    generateSpecializations("libmesh_cpp.content.txt", listNumberOfCoords)
