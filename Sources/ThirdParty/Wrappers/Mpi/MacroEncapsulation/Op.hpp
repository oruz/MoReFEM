/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 13:53:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_OP_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_OP_HPP_


#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace MpiNS
        {


            /*!
             * \brief Enum that encapsulates MPI_Op.
             *
             * The reason for this is that MPI_Op are often macros that trigger the Wold-style-cast warning;
             * the level of indirection allows to neutralize it.
             *
             * \internal <b><tt>[internal]</tt></b> This enum is populated only for the operation I needed at some
             * point; some should be added as soon as they are required.
             * \endinternal
             */
            enum class Op
            {
                Sum,
                Max,
                Min,
                LogicalOr
            };


            /*!
             * \brief Function used to choose the Mpi operator (MPI_Op) from the MoReFEM defined enum.
             *
             * \param[in] operation Operation enum value defined in MoReFEM.
             */
            MPI_Op Operator(Op operation);


        } // namespace MpiNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_OP_HPP_
