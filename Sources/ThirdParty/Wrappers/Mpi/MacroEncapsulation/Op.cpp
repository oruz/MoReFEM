/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 13:53:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>
#include <cstdlib>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Op.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace MpiNS
        {


            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

            MPI_Op Operator(Op operation)
            {
                switch (operation)
                {
                case Op::Sum:
                    return MPI_SUM;
                case Op::Max:
                    return MPI_MAX;
                case Op::Min:
                    return MPI_MIN;
                case Op::LogicalOr:
                    return MPI_LOR;
                }

                assert(false && "Operation required wasn't correctly match with a native Mpi operation!");
                exit(EXIT_FAILURE);
            }


            PRAGMA_DIAGNOSTIC(pop)


        } // namespace MpiNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
