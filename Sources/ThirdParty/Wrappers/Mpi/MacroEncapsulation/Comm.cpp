/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 13:53:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>
#include <cstdlib>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Comm.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace MpiNS
        {


            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

            MPI_Comm Communicator(Comm communicator)
            {
                switch (communicator)
                {
                case Comm::World:
                    return MPI_COMM_WORLD;
                case Comm::Self:
                    return MPI_COMM_SELF;
                }

                assert(false && "Communicator required wasn't correctly match with a native Mpi communicator!");
                exit(EXIT_FAILURE);
            }


            PRAGMA_DIAGNOSTIC(pop)


        } // namespace MpiNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
