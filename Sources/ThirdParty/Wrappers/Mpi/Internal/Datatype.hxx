/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace MpiNS
            {


                PRAGMA_DIAGNOSTIC(push)
                PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")


                template<>
                struct Datatype<unsigned long>
                {
                    static auto Type()
                    {
                        return MPI_UNSIGNED_LONG;
                    }
                };


                template<>
                struct Datatype<char>
                {
                    static auto Type()
                    {
                        return MPI_CHAR;
                    }
                };


                template<>
                struct Datatype<short>
                {
                    static auto Type()
                    {
                        return MPI_SHORT;
                    }
                };


                template<>
                struct Datatype<int>
                {
                    static auto Type()
                    {
                        return MPI_INT;
                    }
                };


                template<>
                struct Datatype<long>
                {
                    static auto Type()
                    {
                        return MPI_LONG;
                    }
                };


                template<>
                struct Datatype<signed char>
                {
                    static auto Type()
                    {
                        return MPI_SIGNED_CHAR;
                    }
                };


                template<>
                struct Datatype<unsigned char>
                {
                    static auto Type()
                    {
                        return MPI_UNSIGNED_CHAR;
                    }
                };


                template<>
                struct Datatype<unsigned short>
                {
                    static auto Type()
                    {
                        return MPI_UNSIGNED_SHORT;
                    }
                };


                template<>
                struct Datatype<unsigned int>
                {
                    static auto Type()
                    {
                        return MPI_UNSIGNED;
                    }
                };


                template<>
                struct Datatype<float>
                {
                    static auto Type()
                    {
                        return MPI_FLOAT;
                    }
                };

                template<>
                struct Datatype<double>
                {
                    static auto Type()
                    {
                        return MPI_DOUBLE;
                    }
                };


                template<>
                struct Datatype<long double>
                {
                    static auto Type()
                    {
                        return MPI_LONG_DOUBLE;
                    }
                };


                template<>
                struct Datatype<long long>
                {
                    static auto Type()
                    {
                        return MPI_LONG_LONG;
                    }
                };


                template<>
                struct Datatype<unsigned long long>
                {
                    static auto Type()
                    {
                        return MPI_UNSIGNED_LONG_LONG;
                    }
                };


                template<class T, class ParameterT, template<class> class... Skills>
                struct Datatype<StrongType<T, ParameterT, Skills...>>
                {
                    using type = StrongType<T, ParameterT, Skills...>;

                    using check_is_hashable = typename std::enable_if<type::use_in_mpi_datatype, void>::type;

                    static auto Type()
                    {
                        return Datatype<T>::Type();
                    }
                };


                PRAGMA_DIAGNOSTIC(pop)


            } // namespace MpiNS


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HXX_
