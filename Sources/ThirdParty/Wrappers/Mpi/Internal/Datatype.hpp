/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HPP_


#include <cassert>
#include <vector>

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace Wrappers
        {


            namespace MpiNS
            {


                /*!
                 * \brief Associate the appropriate MPI datatype to a given C++ type.
                 *
                 * For instance:
                 *
                 * Datatype<unsigned long>::Type() should yield MPI_UNSIGNED_LONG.
                 *
                 * \tparam T C++ POD type considered.
                 *
                 * \internal <b><tt>[internal]</tt></b> There is no content on purpose: this class is intended to be
                 * specialized for each C++ type handled by MPI.
                 * \endinternal
                 */
                template<typename T>
                struct Datatype;


            } // namespace MpiNS


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_INTERNAL_x_DATATYPE_HPP_
