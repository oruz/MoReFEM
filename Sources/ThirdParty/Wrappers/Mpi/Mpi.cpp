/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Jul 2013 16:14:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>
#include <optional>
#include <sstream>
#include <type_traits>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        void Mpi::InitEnvironment(int argc, char** argv)
        {
            if (IsEnvironment())
                throw ExceptionNS::Mpi::Exception(
                    "Mpi::InitEnvironment() called when an MPI environment is still active!", __FILE__, __LINE__);

            assert(!Nalive() && "If not environment set Nalive() must be equal to 0!");

            IsEnvironment() = true;
            MPI_Init(&argc, &argv);
        }


        bool& Mpi::IsEnvironment()
        {
            static bool value = false;
            return value;
        }


        int& Mpi::Nalive()
        {
            static int counter = 0;
            return counter;
        }


        void Mpi::IncrementNalive()
        {
            ++Nalive();
        }


        void Mpi::DecrementNalive()
        {
            if (--Nalive() == 0)
            {
                IsEnvironment() = false;
                int error_code = MPI_Finalize();
                AbortIfErrorCode(std::numeric_limits<int>::lowest(), error_code, __FILE__, __LINE__);
            }
        }


        Mpi::Mpi(int root_processor, MpiNS::Comm comm)
        : root_processor_(root_processor), Nprocessor_(NumericNS::UninitializedIndex<int>()),
          comm_(MpiNS::Communicator(comm)), rank_(NumericNS::UninitializedIndex<int>())
        {
            if (!IsEnvironment())
                throw ExceptionNS::Mpi::Exception(
                    "Mpi::InitEnvironment() must be called before a Mpi object is created! "
                    "(such a call should typically occur in the beginning of your main).",
                    __FILE__,
                    __LINE__);

            const auto& communicator = GetCommunicator();

            int error_code = MPI_Comm_rank(communicator, &rank_);

            if (error_code != MPI_SUCCESS)
                throw ExceptionNS::Mpi::Exception(
                    "Didn't manage to get rank information through MPI_Comm_rank() call", __FILE__, __LINE__);

            error_code = MPI_Comm_size(communicator, &Nprocessor_);

            if (error_code != MPI_SUCCESS)
                throw ExceptionNS::Mpi::Exception(rank_, error_code, __FILE__, __LINE__);

            //            PRAGMA_DIAGNOSTIC(push)
            //            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
            error_code = MPI_Comm_set_errhandler(communicator, MPI_ERRORS_RETURN);
            //            PRAGMA_DIAGNOSTIC(pop)

            if (error_code != MPI_SUCCESS)
                throw ExceptionNS::Mpi::Exception(rank_, error_code, __FILE__, __LINE__);

            IncrementNalive();
        }


        Mpi::~Mpi()
        {
            DecrementNalive();
        }


        namespace // anonymous
        {


            std::vector<short int> BoolToShortInt(const std::vector<bool>& data)
            {
                std::vector<short int> ret(data.size());
                std::transform(data.cbegin(),
                               data.cend(),
                               ret.begin(),
                               [](bool value)
                               {
                                   return value ? 1 : 0;
                               });
                return ret;
            }


            template<class T>
            void ShortIntToBool(T&& data, std::vector<bool>& bool_data)
            {
                static_assert(std::is_same<std::remove_reference_t<T>, std::vector<short int>>(),
                              "Forwarding reference trick; T is expected to be std::vector<short int> "
                              "with possibly const or r/l value reference.");

                bool_data.resize(data.size());
                std::transform(data.cbegin(),
                               data.cend(),
                               bool_data.begin(),
                               [](bool value)
                               {
                                   return value == 1 ? true : false;
                               });
            }


        } // namespace


        void Mpi::Gather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);
            std::vector<short int> converted_gathered_data;
            Gather(converted_sent_data, converted_gathered_data);
            if (IsRootProcessor())
                ShortIntToBool(converted_gathered_data, gathered_data);
        }


        void Mpi::Gatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);
            std::vector<short int> converted_gathered_data;
            Gatherv(converted_sent_data, converted_gathered_data);
            if (IsRootProcessor())
                ShortIntToBool(converted_gathered_data, gathered_data);
        }


        void Mpi::AllGather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);
            std::vector<short int> converted_gathered_data;
            AllGather(converted_sent_data, converted_gathered_data);
            if (IsRootProcessor())
                ShortIntToBool(converted_gathered_data, gathered_data);
        }


        void Mpi::AllGatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);
            std::vector<short int> converted_gathered_data;
            AllGatherv(converted_sent_data, converted_gathered_data);
            if (IsRootProcessor())
                ShortIntToBool(converted_gathered_data, gathered_data);
        }


        void Mpi::Broadcast(std::vector<bool>& data, std::optional<int> send_processor) const
        {
            // assert(!data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto converted_data = BoolToShortInt(data);

            Broadcast(converted_data, send_processor);

            ShortIntToBool(converted_data, data);
        }


        std::vector<bool> Mpi::AllReduce(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);

            auto&& converted_gathered_data = AllReduce(converted_sent_data, mpi_operation);

            std::vector<bool> ret;
            ShortIntToBool(std::move(converted_gathered_data), ret);
            return ret;
        }


        std::vector<bool> Mpi::ReduceOnRootProcessor(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const
        {
            assert(!sent_data.empty());

            // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
            // back to bool.
            auto&& converted_sent_data = BoolToShortInt(sent_data);

            auto&& converted_gathered_data = ReduceOnRootProcessor(converted_sent_data, mpi_operation);

            std::vector<bool> ret;
            ShortIntToBool(std::move(converted_gathered_data), ret);
            return ret;
        }


        namespace // anonymous
        {


            std::string ComputeRankPreffix(const Mpi* const mpi)
            {
                std::ostringstream oconv;
                assert(!(!mpi));
                oconv << '[' << mpi->GetRank<int>() << ']';
                return oconv.str();
            }


        } // namespace


        const std::string& Mpi::GetRankPreffix() const
        {
            static std::string ret = ComputeRankPreffix(this);
            return ret;
        }


        void Mpi::AbortIfErrorCode(int rank, int error_code, const char* invoking_file, int invoking_line) const
        {
            if (error_code != MPI_SUCCESS)
            {
                // By implementing a failure on top of an exception, it's easy to change our mind if someday we rather
                // use exception instead of MPI abort. (However if you do so don't forget to handle the special case of
                // Mpi destructor: no exception should be called there).
                ::MoReFEM::Wrappers::ExceptionNS::Mpi::Exception exception(
                    rank, error_code, invoking_file, invoking_line);
                std::cerr << exception.what() << std::endl;
                MPI_Abort(GetCommunicator(), error_code);
            }
        }


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
