//! \file
//
//
//  IsSameForAllRanks.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 05/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hpp"


namespace MoReFEM
{


    template<class ContainerT>
    bool IsSameForAllRanks(const Wrappers::Mpi& mpi,
                           const ContainerT& container,
                           std::optional<typename ContainerT::value_type> epsilon)
    {
        using value_type = typename ContainerT::value_type;
        static_assert(std::is_arithmetic<value_type>());
        static_cast<void>(epsilon);

        assert(((std::is_integral<value_type>() && !epsilon.has_value())
                || (std::is_floating_point<value_type>() && epsilon.has_value()))
               && "Epsilon doesn't make sense for integers, and on the contrary comparing floating point without it "
                  "is slippery...");

        {
            // If container is not the same size on each rank, return false
            auto size_per_processor = mpi.CollectFromEachProcessor(container.size());

            assert(!container.empty());

            const auto last_value = size_per_processor.back();

            if (!std::all_of(size_per_processor.cbegin(),
                             size_per_processor.cend(),
                             [last_value](const auto value)
                             {
                                 return value == last_value;
                             }))
                return false;
        }

        // From here onward, the computation is done on root processor - so the root processor will need to
        // propagate the result of its computation to other ranks so tht all ranks yield the same result.

        bool ret{ true };

        auto min_container = mpi.ReduceOnRootProcessor(container, Wrappers::MpiNS::Op::Min);
        auto max_container = mpi.ReduceOnRootProcessor(container, Wrappers::MpiNS::Op::Max);

        if (mpi.IsRootProcessor())
        {
            if constexpr (std::is_integral<value_type>())
                ret = min_container == max_container ? true : false;
            else
            {
                assert(min_container.size() == max_container.size()
                       && "The case containers aren't the same size "
                          "should have been handled much earlier.");

                const auto size = min_container.size();

                for (auto i = 0ul; ret && i < size; ++i)
                {
                    if (!NumericNS::AreEqual(min_container[i], max_container[i], epsilon.value()))
                        ret = false;
                }
            }
        }

        std::vector<bool> ret_as_vector{ ret };

        mpi.Broadcast(ret_as_vector);
        assert(ret_as_vector.size() == 1ul);

        return ret_as_vector.back();
    }


} // namespace MoReFEM


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HXX_
