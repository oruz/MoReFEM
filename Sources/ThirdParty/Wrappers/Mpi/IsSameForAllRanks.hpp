//! \file
//
//
//  IsSameForAllRanks.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 05/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HPP_

#include <optional>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Check whether a container is the same on all ranks or not.
     *
     * \copydoc doxygen_hide_mpi_param
     * \param[in] container Container which is under scrutiny.
     * \param[in] epsilon Optional that must be activated ONLY IF Container::value_type is a floating point.
     *
     * \return True if the container is the same for all ranks.
     *
     * \internal Considering the use case I have, this method is implemented to be simple, not the fastest possible. I
     * have no doubt there are more clever and efficient way to implement such a functionality, but it would be overkill
     * in my case.
     *
     * \tparam ContainerT A container with either integer or floating point values.
     */
    template<class ContainerT>
    bool IsSameForAllRanks(const Wrappers::Mpi& mpi,
                           const ContainerT& container,
                           std::optional<typename ContainerT::value_type> epsilon = std::nullopt);


} // namespace MoReFEM


#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_IS_SAME_FOR_ALL_RANKS_HPP_
