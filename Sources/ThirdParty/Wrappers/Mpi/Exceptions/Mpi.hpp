/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HPP_

#include <sstream>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export // IWYU pragma: export


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace ExceptionNS
        {


            namespace Mpi
            {


                //! Generic class
                struct Exception : public MoReFEM::Exception
                {


                    /*!
                     * \brief Constructor with simple message.
                     *
                     * \param[in] msg Message.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                    /*!
                     * \brief Constructor with error code, to use when a mpi function call failed.
                     *
                     * \attention It should not be relevant at the moment due to the MPI_ERRORS_ARE_FATAL setting in
                     * Mpi class constructor. However it is functional should we reverse our position and choose instead
                     * MPI_ERRORS_RETURN; in this case return code checking should be added around each mpi function
                     * call.
                     *
                     * \param[in] rank Mpi rank on on which the exception was thrown.
                     * \param[in] error_code Error code returned by mpi API. MPI provides a function to make it readable
                     * directly.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit Exception(int rank, int error_code, const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~Exception() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    Exception(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    Exception(Exception&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    Exception& operator=(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    Exception& operator=(Exception&& rhs) = default;
                };


            } // namespace Mpi


        } // namespace ExceptionNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_EXCEPTIONS_x_MPI_HPP_
