/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <limits>
#include <sstream>
#include <vector>

#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hpp"


namespace // anonymous
{


    //! Prepare exception message from the error code provided by mpi API.
    std::string InterpretErrorCode(int rank, int error_code);


} // namespace


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace ExceptionNS
        {


            namespace Mpi
            {


                Exception::~Exception() = default;


                Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(msg, invoking_file, invoking_line)
                { }


                Exception::Exception(int rank, int error_code, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(InterpretErrorCode(rank, error_code), invoking_file, invoking_line)
                { }


            } // namespace Mpi


        } // namespace ExceptionNS


    } // namespace Wrappers


} // namespace MoReFEM


namespace // anonymous
{


    std::string InterpretErrorCode(int rank, int error_code)
    {
        std::vector<char> error_string(MPI_MAX_ERROR_STRING);
        int length_of_error_string;

        int bootstrap_error_code = MPI_Error_string(error_code, error_string.data(), &length_of_error_string);

        if (bootstrap_error_code != MPI_SUCCESS)
            throw MoReFEM::Exception("Interpretation of the mpi error code failed!", __FILE__, __LINE__);

        std::ostringstream oconv;
        if (rank != std::numeric_limits<int>::lowest())
            oconv << '[' << rank << "] ";

        oconv << MoReFEM::Utilities::String::ConvertCharArray(error_string);

        return oconv.str();
    }


} // namespace


/// @} // addtogroup ThirdPartyGroup
