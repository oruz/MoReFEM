/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <limits>
#include <numeric>
#include <optional>
#include <type_traits>
#include <vector>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        inline constexpr int Mpi::AnyTag()
        {
            return 0;
        }


        template<typename T>
        void Mpi::Gather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            GatherImpl(sent_data, gathered_data);
        }


        template<typename T>
        void Mpi::Gatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            GathervImpl(sent_data, gathered_data);
        }


        template<typename T>
        void Mpi::AllGather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            AllGatherImpl(sent_data, gathered_data);
        }


        template<typename T>
        void Mpi::AllGatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            AllGathervImpl(sent_data, gathered_data);
        }


        template<typename T>
        std::vector<T> Mpi::AllReduce(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            std::vector<T> ret(sent_data.size());
            AllReduce(sent_data, ret, mpi_operation);

            return ret;
        }


        template<typename T>
        T Mpi::AllReduce(T sent_data, MpiNS::Op mpi_operation) const
        {
            std::vector<T> buf{ sent_data };
            std::vector<T> gathered_data = AllReduce(buf, mpi_operation);

            assert(gathered_data.size() == 1);
            return gathered_data[0];
        }


        template<typename T>
        std::vector<T> Mpi::ReduceOnRootProcessor(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const
        {
            static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

            std::vector<T> ret;
            ReduceImpl(sent_data, ret, GetRootProcessor(), mpi_operation);

            return ret;
        }


        template<typename T>
        T Mpi::ReduceOnRootProcessor(T sent_data, MpiNS::Op mpi_operation) const
        {
            std::vector<T> buf{ sent_data };
            std::vector<T> gathered_data = ReduceOnRootProcessor(buf, mpi_operation);

            assert(gathered_data.size() == 1);
            return gathered_data[0];
        }


        template<typename IntT>
        inline IntT Mpi::GetRank() const
        {
            static_assert(std::is_integral<IntT>::value, "Return type should be integral!");

#ifndef NDEBUG
            int mpi_rank;
            int error_code = MPI_Comm_rank(GetCommunicator(), &mpi_rank);
            AbortIfErrorCode(std::numeric_limits<int>::lowest(), error_code, __FILE__, __LINE__);
            assert(mpi_rank == rank_);
#endif // NDEBUG

            return static_cast<IntT>(rank_);
        }


        inline MPI_Comm Mpi::GetCommunicator() const
        {
            return comm_;
        }


        inline bool Mpi::IsRootProcessor() const
        {
            return GetRank<int>() == GetRootProcessor();
        }


        template<typename IntT>
        inline IntT Mpi::Nprocessor() const
        {
#ifndef NDEBUG
            int mpi_Nprocessor;
            int error_code = MPI_Comm_size(comm_, &mpi_Nprocessor);
            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);
            assert(mpi_Nprocessor == Nprocessor_);
#endif // NDEBUG

            return static_cast<IntT>(Nprocessor_);
        }


        template<class ContainerT>
        void Mpi::GatherImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
        {
            int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

            if (IsRootProcessor())
                gathered_data.resize(Nprocessor<std::size_t>() * sent_data.size());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Gather(sent_data.data(),
                                        Ndata_sent_by_each_proc,
                                        mpi_datatype,
                                        gathered_data.data(),
                                        Ndata_sent_by_each_proc,
                                        mpi_datatype,
                                        GetRootProcessor(),
                                        GetCommunicator());

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<class ContainerT>
        void Mpi::GathervImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
        {
            int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            // When the suffix on_root_processor is present it means the quantity is only relevant on root processor.
            std::vector<int> gathered_sizes_on_root_processor;
            std::vector<int> disps_on_root_processor;
            std::vector<int> sent_size_on_each_proc = { static_cast<int>(sent_data.size()) };

            Gather(sent_size_on_each_proc, gathered_sizes_on_root_processor);

            if (IsRootProcessor())
            {
                const std::size_t total_size_on_root_processor = std::accumulate(
                    gathered_sizes_on_root_processor.cbegin(), gathered_sizes_on_root_processor.cend(), 0ul);

                gathered_data.resize(total_size_on_root_processor);

                const auto Nproc_on_root_processor = gathered_sizes_on_root_processor.size();
                disps_on_root_processor.resize(Nproc_on_root_processor, 0);

                for (auto i = 1ul; i < Nproc_on_root_processor; ++i)
                    disps_on_root_processor[i] =
                        disps_on_root_processor[i - 1ul] + gathered_sizes_on_root_processor[i - 1ul];
            }

            int error_code = MPI_Gatherv(sent_data.data(),
                                         Ndata_sent_by_each_proc,
                                         mpi_datatype,
                                         gathered_data.data(),
                                         gathered_sizes_on_root_processor.data(),
                                         disps_on_root_processor.data(),
                                         mpi_datatype,
                                         GetRootProcessor(),
                                         GetCommunicator());

            // gathered_data is relevant only on root processor.

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<class ContainerT>
        void Mpi::AllGatherImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
        {
            const int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

            gathered_data.resize(Nprocessor<std::size_t>() * sent_data.size());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Allgather(sent_data.data(),
                                           Ndata_sent_by_each_proc,
                                           mpi_datatype,
                                           gathered_data.data(),
                                           Ndata_sent_by_each_proc,
                                           mpi_datatype,
                                           GetCommunicator());

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<class ContainerT>
        void Mpi::AllGathervImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
        {
            const int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            std::vector<int> gathered_sizes_on_root_processor;
            std::vector<int> sent_size_on_each_proc = { static_cast<int>(sent_data.size()) };

            Gatherv(sent_size_on_each_proc, gathered_sizes_on_root_processor);

            const auto Nproc = Nprocessor<std::size_t>();
            std::vector<int> disps_on_each_processor(Nproc);
            std::vector<int> gathered_sizes_on_each_processor(Nproc);

            std::size_t total_size_on_root_processor = 0ul;

            if (IsRootProcessor())
            {
                total_size_on_root_processor = std::accumulate(
                    gathered_sizes_on_root_processor.cbegin(), gathered_sizes_on_root_processor.cend(), 0ul);

                const auto gathered_sizes_on_root_processor_size = gathered_sizes_on_root_processor.size();

                disps_on_each_processor[0ul] = 0;
                for (auto i = 1ul; i < gathered_sizes_on_root_processor_size; ++i)
                    disps_on_each_processor[i] =
                        disps_on_each_processor[i - 1ul] + gathered_sizes_on_root_processor[i - 1ul];

                gathered_sizes_on_each_processor = gathered_sizes_on_root_processor;
            }

            std::vector<std::size_t> total_size_on_each_processor = { total_size_on_root_processor };

            Barrier();

            Broadcast(total_size_on_each_processor);
            Broadcast(gathered_sizes_on_each_processor);
            Broadcast(disps_on_each_processor);

            Barrier();

            assert(total_size_on_each_processor.size() == 1ul);

            gathered_data.resize(total_size_on_each_processor[0ul]);

            int error_code = MPI_Allgatherv(sent_data.data(),
                                            Ndata_sent_by_each_proc,
                                            mpi_datatype,
                                            gathered_data.data(),
                                            gathered_sizes_on_each_processor.data(),
                                            disps_on_each_processor.data(),
                                            mpi_datatype,
                                            GetCommunicator());

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<class ContainerT>
        void Mpi::AllReduce(const ContainerT& sent_data, ContainerT& gathered_data, MpiNS::Op mpi_operation) const
        {
            const auto size = sent_data.size();
            assert(gathered_data.size() == size);

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();


#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Allreduce(sent_data.data(),
                                           gathered_data.data(),
                                           static_cast<int>(size),
                                           mpi_datatype,
                                           MpiNS::Operator(mpi_operation),
                                           GetCommunicator());
            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<class ContainerT>
        void Mpi::ReduceImpl(const ContainerT& sent_data,
                             ContainerT& gathered_data,
                             int target_processor,
                             MpiNS::Op mpi_operation) const
        {
            gathered_data.resize(sent_data.size());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();


#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Reduce(sent_data.data(),
                                        gathered_data.data(),
                                        static_cast<int>(gathered_data.size()),
                                        mpi_datatype,
                                        MpiNS::Operator(mpi_operation),
                                        target_processor,
                                        GetCommunicator());

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
        }


        template<std::size_t N>
        std::bitset<N> Mpi::ReduceOnRootProcessor(const std::bitset<N>& sent_data, MpiNS::Op mpi_operation) const
        {
            std::vector<short int> buf(N);

            for (std::size_t i = 0ul; i < N; ++i)
                buf[i] = (sent_data[i] ? 1 : 0);

            auto&& result = ReduceOnRootProcessor(buf, mpi_operation);

            std::bitset<N> ret;

            for (std::size_t i = 0ul; i < N; ++i)
                ret[i] = (result[i] > 0 ? 1 : 0);

            return ret;
        }


        template<typename T>
        std::vector<T> Mpi::CollectFromEachProcessor(T sent_data) const
        {
            std::vector<T> buf(Nprocessor<std::size_t>(), 0);

            buf[GetRank<std::size_t>()] = sent_data;

            return AllReduce(buf, Wrappers::MpiNS::Op::Sum);
        }


        inline void Mpi::Barrier() const
        {
            int error_code = MPI_Barrier(GetCommunicator());
            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);
        }


        inline int Mpi::GetRootProcessor() const
        {
            return root_processor_;
        }


        template<typename T>
        void Mpi::Broadcast(std::vector<T>& data, std::optional<int> send_processor) const
        {
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Bcast(data.data(),
                                       static_cast<int>(data.size()),
                                       mpi_datatype,
                                       send_processor.has_value() ? send_processor.value() : GetRootProcessor(),
                                       GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);
        }


        template<typename T>
        void Mpi::Broadcast(T& data, std::optional<int> send_processor) const
        {
            std::vector<T> data_as_vector{ data };
            Broadcast(data_as_vector, send_processor);
            data = data_as_vector.back();
        }


        template<typename T>
        void Mpi::Send(std::size_t destination, T data) const
        {
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code =
                MPI_Send(&data, 1, mpi_datatype, static_cast<int>(destination), AnyTag(), GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);
        }


        template<class ContainerT>
        void Mpi::SendContainer(std::size_t destination, const ContainerT& data) const
        {
            // assert(!data.empty());

            using value_type = typename ContainerT::value_type;
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Send(data.data(),
                                      static_cast<int>(data.size()),
                                      mpi_datatype,
                                      static_cast<int>(destination),
                                      AnyTag(),
                                      GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);
        }

        template<typename T>
        std::vector<T> Mpi::Receive(std::size_t source, std::size_t max_length) const
        {
            assert(max_length > 0ul);

            std::vector<T> ret(max_length);
            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

            MPI_Status status;

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_

            int error_code = MPI_Recv(ret.data(),
                                      static_cast<int>(max_length),
                                      mpi_datatype,
                                      static_cast<int>(source),
                                      AnyTag(),
                                      GetCommunicator(),
                                      &status);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
            PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

            int count;

            error_code = MPI_Get_count(&status, mpi_datatype, &count);

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

            ret.resize(static_cast<std::size_t>(count));

            return ret;
        }


        template<typename T>
        T Mpi::Receive(std::size_t source) const
        {
            T ret;

            const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

            PRAGMA_DIAGNOSTIC(push)
#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
#endif // __clang_
            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

            int error_code = MPI_Recv(&ret,
                                      1,
                                      mpi_datatype,
                                      static_cast<int>(source),
                                      AnyTag(),
                                      GetCommunicator(),
                                      nullptr); // MPI_STATUS_IGNORE is a 0 macro and triggers a warning!

            PRAGMA_DIAGNOSTIC(pop)

            AbortIfErrorCode(GetRank<int>(), error_code, __FILE__, __LINE__);

            return ret;
        }


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HXX_
