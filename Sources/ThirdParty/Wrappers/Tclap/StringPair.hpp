/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 13:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HPP_

#include <string> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Tclap
        {


            /*!
             * \brief This class aims to provide a new accepted type on command lines like: '-e FOO=BAR'.
             *
             * Internally, the string "FOO=BAR" is read and assigned in a std::pair<std::string, std::string>.
             * The class is minimalist on purpose: it is intended to be used only in Tclap context.
             */
            class StringPair
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = StringPair;

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit StringPair() = default;

                /*!
                 * \brief The only useful one: assignment from a std::string.
                 *
                 * \param[in] rhs String which is converted into a Pair object. Expected format is a string without
                 * spaces with a '=' somewhere. An empty string is accepted as well and leave the internal pair empty.
                 *
                 * \return Reference to the \a Pair object updated with the content from \a rhs.
                 */
                StringPair& operator=(std::string rhs);

                //! Destructor.
                ~StringPair() = default;

                //! \copydoc doxygen_hide_copy_constructor
                StringPair(const StringPair& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                StringPair(StringPair&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                StringPair& operator=(const StringPair& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                StringPair& operator=(StringPair&& rhs) = default;

                ///@}

                /*!
                 * \brief Accessor to the underlying pair.
                 *
                 * \return The pair key/value read from the command line.
                 */
                const std::pair<std::string, std::string>& GetValue() const noexcept;

              private:
                //! The underlying pair.
                std::pair<std::string, std::string> value_;
            };


        } // namespace Tclap


    } // namespace Wrappers


} // namespace MoReFEM


namespace TCLAP
{


    /*!
     * \brief Traits class to enable the use of StringPair in TClap.
     */
    template<>
    struct ArgTraits<MoReFEM::Wrappers::Tclap::StringPair>
    {

        //! The value to set for the traits class.
        using ValueCategory = StringLike;
    };


} // namespace TCLAP


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Tclap/StringPair.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HPP_
