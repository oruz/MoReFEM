/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 13:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <string> // IWYU pragma: keep
#include <utility>
// IWYU pragma:  no_include <iosfwd>

#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Tclap
        {


            StringPair& StringPair::operator=(std::string rhs)
            {
                if (rhs.empty())
                {
                    std::pair<std::string, std::string> empty;
                    value_.swap(empty);
                    return *this;
                }

                Utilities::String::Strip(rhs);

                const auto pos = rhs.find('=');

                const auto last_pos = rhs.rfind('=');

                if (pos == std::string::npos || pos != last_pos)
                    throw TCLAP::ArgParseException(rhs + " is not a StringPair, which expected format is KEY=VALUE.");

                value_.first.assign(rhs, 0, pos);
                value_.second.assign(rhs, pos + 1, rhs.size() - pos - 1);

                return *this;
            }


        } // namespace Tclap


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
