/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Oct 2013 17:33:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <sstream>

#include "ThirdParty/Wrappers/Parmetis/Exceptions/Parmetis.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string ErrorCodeMsg(int error_code, const std::string& parmetis_function);


} // namespace


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Parmetis
        {


            namespace ExceptionNS
            {


                Exception::~Exception() = default;


                Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(msg, invoking_file, invoking_line)
                { }


                Exception::Exception(int error_code,
                                     const std::string& parmetis_function,
                                     const char* invoking_file,
                                     int invoking_line)
                : MoReFEM::Exception(ErrorCodeMsg(error_code, parmetis_function), invoking_file, invoking_line)
                { }


            } // namespace ExceptionNS


        } // namespace Parmetis


    } // namespace Wrappers


} // namespace MoReFEM


namespace // anonymous
{


    // Definitions of functions declared at the beginning of the file.
    std::string ErrorCodeMsg(int error_code, const std::string& parmetis_function)
    {
        std::ostringstream oconv;
        oconv << "Parmetis function " << parmetis_function << " returned the error code " << error_code << '.';

        return oconv.str();
    }


} // namespace


/// @} // addtogroup ThirdPartyGroup
