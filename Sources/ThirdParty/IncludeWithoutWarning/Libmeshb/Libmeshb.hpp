/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LIBMESHB_x_LIBMESHB_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LIBMESHB_x_LIBMESHB_HPP_

#include "Utilities/Warnings/Pragma.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-result")

#include "Utilities/Warnings/Internal/IgnoreWarning/newline-eof.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/shorten-64-to-32.hpp"


extern "C"
{


#include "libmeshb7.h"
}

//! Alias to the integer type used in libmesh.
using libmeshb_int = int64_t;


PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LIBMESHB_x_LIBMESHB_HPP_
