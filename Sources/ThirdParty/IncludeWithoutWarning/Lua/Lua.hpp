/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Mar 2018 13:12:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

extern "C"
{
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
}


PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_LUA_x_LUA_HPP_
