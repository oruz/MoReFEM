//
//  xtensor.hpp
//  MoReFEM
//
//  Created by sebastien on 12/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_XTENSOR_x_XTENSOR_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_XTENSOR_x_XTENSOR_HPP_

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wshadow")
PRAGMA_DIAGNOSTIC(ignored "-Wundef")
PRAGMA_DIAGNOSTIC(ignored "-Wswitch-enum")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

#include "Utilities/Warnings/Internal/IgnoreWarning/comma.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi-stmt.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/newline-eof.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reorder.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/shadow-field-in-constructor.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-local-typedef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"

#include "xtensor-blas/xlinalg.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xnoalias.hpp"
#include "xtensor/xtensor.hpp"
#include "xtensor/xview.hpp"

PRAGMA_DIAGNOSTIC(pop)


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_XTENSOR_x_XTENSOR_HPP_
