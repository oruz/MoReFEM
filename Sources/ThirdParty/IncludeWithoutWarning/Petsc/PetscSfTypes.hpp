/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Jun 2021.
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_SF_TYPES_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_SF_TYPES_HPP_

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")

#include "Utilities/Warnings/Internal/IgnoreWarning/comma.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

// Space is important below to preserve ordering when clang-format is called.
#include "petscsystypes.h" // IWYU pragma: export

#include "petscsftypes.h" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_SF_TYPES_HPP_
