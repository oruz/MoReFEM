
include(${CMAKE_CURRENT_LIST_DIR}/Tools/CMakeLists.txt)

if (NOT MOREFEM_IGNORE_TESTS)
    include(${CMAKE_CURRENT_LIST_DIR}/Utilities/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/ThirdParty/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/Core/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/Geometry/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/FiniteElementSpace/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/Operators/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/Ondomatic/CMakeLists.txt)

    include(${CMAKE_CURRENT_LIST_DIR}/Parameter/CMakeLists.txt)
    include(${CMAKE_CURRENT_LIST_DIR}/ParameterInstances/CMakeLists.txt)

    include(${CMAKE_CURRENT_LIST_DIR}/PostProcessing/CMakeLists.txt)
endif()

