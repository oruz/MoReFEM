/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_matrix_io
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::PetscNS::MatrixNS::ToyModel,
                                                  TestNS::FixtureNS::call_run_method_at_first_call::no>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(creation, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);
}


BOOST_FIXTURE_TEST_CASE(equal_to_self, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, matrix, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(copy_is_equal, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    GlobalMatrix copy(matrix);

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, copy, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(not_equal, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    GlobalMatrix zero(matrix);
    zero.ZeroEntries(__FILE__, __LINE__);

    BOOST_CHECK(!Wrappers::Petsc::AreStrictlyEqual(matrix, zero, __FILE__, __LINE__));
}


PRAGMA_DIAGNOSTIC(pop)
