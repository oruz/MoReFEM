/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    //! Sole index used for this very simple model.
    constexpr auto sole = 1;

    //! Index for row index.
    constexpr auto row_index = 1;

    //! Index for column index.
    constexpr auto column_index = 2;

    //! Index for unknown 1
    constexpr auto unknown1_index = 1;

    //! Index for unknown 2
    constexpr auto unknown2_index = 2;


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using InputDataTuple = std::tuple
    <
        InputDataNS::Mesh<sole>,

        InputDataNS::Unknown<unknown1_index>,
        InputDataNS::Unknown<unknown2_index>,

        InputDataNS::Domain<sole>,

        InputDataNS::NumberingSubset<row_index>,
        InputDataNS::NumberingSubset<column_index>,

        InputDataNS::FEltSpace<sole>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using InputData = InputData<InputDataTuple>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<InputData, program_type::test>;


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_
