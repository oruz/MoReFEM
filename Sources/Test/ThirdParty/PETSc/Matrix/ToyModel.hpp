/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_TOY_MODEL_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_TOY_MODEL_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "Test/ThirdParty/PETSc/Matrix/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS::PetscNS::MatrixNS
    {


        /*!
         * \brief Toy model used to perform tests related to PETSc matrices
         *
         * Its role is mostly to fill a \a GlobalMatrix in a realistic case (mas operator over a mesh).
         */
        class ToyModel : public Crtp::CrtpMpi<ToyModel>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = ToyModel;

          public:
            //! Define as a trait - I need that to use it with \a TestNS::FixtureNS::Model .
            using morefem_data_type = ::MoReFEM::TestNS::PetscNS::MatrixNS::morefem_data_type;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            ToyModel(const morefem_data_type& morefem_data);

            //! Destructor.
            ~ToyModel() = default;

            //! \copydoc doxygen_hide_copy_constructor
            ToyModel(const ToyModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            ToyModel(ToyModel&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            ToyModel& operator=(const ToyModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            ToyModel& operator=(ToyModel&& rhs) = delete;

            ///@}

            //! Returns the path to result directory.
            const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

            //! Accessor to the matrix used for tests.
            const GlobalMatrix& GetMatrix() const noexcept;

          private:
            //! Global matrix which is used in the tests.
            GlobalMatrix::unique_ptr matrix_ = nullptr;

            //! Path to result directory.
            const FilesystemNS::Directory output_directory_;
        };


    } // namespace TestNS::PetscNS::MatrixNS


} // namespace MoReFEM


#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_TOY_MODEL_HPP_
