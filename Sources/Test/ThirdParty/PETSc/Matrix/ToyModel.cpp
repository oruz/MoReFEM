/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "Model/Internal/CreateMeshDataDirectory.hpp"
#include "Model/Internal/InitializeHelper.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    namespace // anonymous
    {


        FilesystemNS::Directory ExtractResultDirectory(const morefem_data_type& morefem_data)
        {
            const std::string path = Utilities::InputDataNS::Extract<InputDataNS::Result::OutputDirectory>::Path(
                morefem_data.GetInputData());

            return FilesystemNS::Directory(
                morefem_data.GetMpi(), path, FilesystemNS::behaviour::overwrite, __FILE__, __LINE__);
        }


    } // namespace


    ToyModel::ToyModel(const morefem_data_type& morefem_data)
    : Crtp::CrtpMpi<ToyModel>(morefem_data.GetMpi()), output_directory_(ExtractResultDirectory(morefem_data))
    {
        Internal::ModelNS::InitMostSingletonManager(morefem_data);

        Internal::ModelNS::InitAllGodOfDofs(morefem_data,
                                            DoConsiderProcessorWiseLocal2Global::no,
                                            Internal::ModelNS::CreateMeshDataDirectory(GetOutputDirectory()));

        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(sole);

        decltype(auto) row_numbering_subset = god_of_dof.GetNumberingSubset(row_index);
        decltype(auto) col_numbering_subset = god_of_dof.GetNumberingSubset(column_index);

        matrix_ = std::make_unique<GlobalMatrix>(row_numbering_subset, col_numbering_subset);
        auto& matrix = *matrix_;

        AllocateGlobalMatrix(god_of_dof, matrix);

        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) unknown1 = unknown_manager.GetUnknownPtr(unknown1_index);
        decltype(auto) unknown2 = unknown_manager.GetUnknownPtr(unknown2_index);

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(sole);

        GlobalVariationalOperatorNS::Mass mass_op(felt_space, unknown2, unknown1);

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS
