/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_matrix_io
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::PetscNS::MatrixNS::ToyModel,
                                                  TestNS::FixtureNS::call_run_method_at_first_call::no>;


    /*!
     * \brief Determines the path and name of the matrix on file and make sure it doesn't exist yet.
     */
    std::string PrepareFile(const TestNS::PetscNS::MatrixNS::ToyModel& model)
    {
        std::ostringstream oconv;
        oconv << model.GetOutputDirectory() << "/matrix_test_IO.bin";

        const auto filename = oconv.str();

        if (model.GetMpi().IsRootProcessor())
        {
            if (FilesystemNS::File::DoExist(filename))
                FilesystemNS::File::Remove(filename, __FILE__, __LINE__);
        }

        return filename;
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(creation, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);
}


BOOST_FIXTURE_TEST_CASE(load, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) matrix = model.GetMatrix();

    const auto matrix_file = PrepareFile(model);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(FilesystemNS::File::DoExist(matrix_file) == false);

    matrix.View(mpi, matrix_file, __FILE__, __LINE__, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(FilesystemNS::File::DoExist(matrix_file) == true);

    GlobalMatrix loaded_matrix(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

    loaded_matrix.DuplicateLayout(matrix, MAT_DO_NOT_COPY_VALUES, __FILE__, __LINE__);

    loaded_matrix.LoadBinary(mpi, matrix_file, __FILE__, __LINE__);

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, loaded_matrix, __FILE__, __LINE__));
}


PRAGMA_DIAGNOSTIC(pop)
