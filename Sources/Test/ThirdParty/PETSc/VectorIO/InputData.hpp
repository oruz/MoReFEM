/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    //! Sole index used for this very simple model.
    constexpr auto sole = 1;


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using InputDataTuple = std::tuple
    <
        InputDataNS::Mesh<sole>,

        InputDataNS::Unknown<sole>,
        InputDataNS::Domain<sole>,
        InputDataNS::NumberingSubset<sole>,
        InputDataNS::FEltSpace<sole>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using InputData = InputData<InputDataTuple>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<InputData, program_type::test>;


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_INPUT_DATA_HPP_
