/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Model/Internal/CreateMeshDataDirectory.hpp"
#include "Model/Internal/InitializeHelper.hpp"

#include "Test/ThirdParty/PETSc/VectorIO/ToyModel.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    namespace // anonymous
    {


        std::string ComputeProcessorWiseAsciiFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_" << model.GetMpi().GetRank<int>() << ".hhdata";
            return oconv.str();
        }


        std::string ComputeProcessorWiseBinaryFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_" << model.GetMpi().GetRank<int>() << ".bin";
            return oconv.str();
        }


        std::string ComputeProgramWiseAsciiFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.m";
            std::string ret = oconv.str();

            oconv.str("");
            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


        std::string ComputeProgramWiseBinaryFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.bin";
            std::string ret = oconv.str();

            oconv.str("");
            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


        FilesystemNS::Directory ExtractResultDirectory(const morefem_data_type& morefem_data)
        {
            const std::string path = Utilities::InputDataNS::Extract<InputDataNS::Result::OutputDirectory>::Path(
                morefem_data.GetInputData());

            return FilesystemNS::Directory(
                morefem_data.GetMpi(), path, FilesystemNS::behaviour::overwrite, __FILE__, __LINE__);
        }


    } // namespace


    ToyModel::ToyModel(const morefem_data_type& morefem_data)
    : Crtp::CrtpMpi<ToyModel>(morefem_data.GetMpi()), output_directory_(ExtractResultDirectory(morefem_data))
    {
        decltype(auto) mpi = GetMpi();

        Internal::ModelNS::InitMostSingletonManager(morefem_data);

        Internal::ModelNS::InitAllGodOfDofs(morefem_data,
                                            DoConsiderProcessorWiseLocal2Global::no,
                                            Internal::ModelNS::CreateMeshDataDirectory(GetOutputDirectory()));

        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(1);

        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(1);

        vector_ = std::make_unique<GlobalVector>(numbering_subset);
        auto& vector = *vector_;

        AllocateGlobalVector(god_of_dof, vector);

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector, __FILE__, __LINE__);


            const auto size = content.GetSize(__FILE__, __LINE__);
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            for (auto i = 0ul; i < size; ++i)
                content[i] = rank_plus_one * std::sqrt(i); // completely arbitrary value with no redundancy!
        }

        {
            const auto binary_file = GetProcessorWiseBinaryFile();
            if (FilesystemNS::File::DoExist(binary_file))
                FilesystemNS::File::Remove(binary_file, __FILE__, __LINE__);
        }

        {
            const auto ascii_file = GetProcessorWiseAsciiFile();
            if (FilesystemNS::File::DoExist(ascii_file))
                FilesystemNS::File::Remove(ascii_file, __FILE__, __LINE__);
        }

        if (mpi.IsRootProcessor())
        {
            {
                const auto binary_file = GetProgramWiseBinaryFile();
                if (FilesystemNS::File::DoExist(binary_file))
                    FilesystemNS::File::Remove(binary_file, __FILE__, __LINE__);
            }

            {
                const auto ascii_file = GetProgramWiseAsciiFile();
                if (FilesystemNS::File::DoExist(ascii_file))
                    FilesystemNS::File::Remove(ascii_file, __FILE__, __LINE__);
            }
        }
    }


    const std::string& ToyModel::GetProcessorWiseBinaryFile() const noexcept
    {
        static auto ret = ComputeProcessorWiseBinaryFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProcessorWiseAsciiFile() const noexcept
    {
        static auto ret = ComputeProcessorWiseAsciiFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProgramWiseBinaryFile() const noexcept
    {
        static auto ret = ComputeProgramWiseBinaryFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProgramWiseAsciiFile() const noexcept
    {
        static auto ret = ComputeProgramWiseAsciiFile(*this);
        return ret;
    }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS
