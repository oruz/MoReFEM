/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Test/ThirdParty/PETSc/VectorIO/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS::PetscNS::VectorIONS
    {


        /*!
         * \brief Toy model used to perform tests about I/O operations related to \a GlobalVector.
         *
         * This toy model role is to:
         * - Initialize the data (including partitioning among the different ranks in the case of parallel run)
         * - Create and store a \a GlobalVector onto which different I/O operations will be performed.
         * - Set filenames for output files and ensure these files don't exist at first - they will be created by the
         * tests themselves.
         */
        class ToyModel : public Crtp::CrtpMpi<ToyModel>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = ToyModel;

          public:
            //! Define as a trait - I need that to use it with \a TestNS::FixtureNS::Model .
            using morefem_data_type = ::MoReFEM::TestNS::PetscNS::VectorIONS::morefem_data_type;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            ToyModel(const morefem_data_type& morefem_data);

            //! Destructor.
            ~ToyModel() = default;

            //! \copydoc doxygen_hide_copy_constructor
            ToyModel(const ToyModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            ToyModel(ToyModel&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            ToyModel& operator=(const ToyModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            ToyModel& operator=(ToyModel&& rhs) = delete;

            ///@}

            //! Returns the path to result directory.
            const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

            //! Accessor to the vector used for tests.
            const GlobalVector& GetVector() const noexcept;

            //! Accessor to the path to the binary file for current rank.
            const std::string& GetProcessorWiseBinaryFile() const noexcept;

            //! Accessor to the path to the ascii file for current rank.
            const std::string& GetProcessorWiseAsciiFile() const noexcept;

            //! Accessor to the path to the program-wise binary file.
            const std::string& GetProgramWiseBinaryFile() const noexcept;

            //! Accessor to the path to the program-wise ascii file.
            const std::string& GetProgramWiseAsciiFile() const noexcept;

          private:
            //! Global vector which is used in the tests.
            GlobalVector::unique_ptr vector_ = nullptr;

            //! Path to result directory.
            const FilesystemNS::Directory output_directory_;
        };


    } // namespace TestNS::PetscNS::VectorIONS


} // namespace MoReFEM


#include "Test/ThirdParty/PETSc/VectorIO/ToyModel.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HPP_
