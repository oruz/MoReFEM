add_executable(MoReFEMTestPetscVectorIO
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
               ${CMAKE_CURRENT_LIST_DIR}/ToyModel.cpp
               ${CMAKE_CURRENT_LIST_DIR}/ToyModel.hpp
               ${CMAKE_CURRENT_LIST_DIR}/ToyModel.hxx
              )
          
target_link_libraries(MoReFEMTestPetscVectorIO
                      ${MOREFEM_TEST_TOOLS}
                      )

add_test(PetscVectorIO
         MoReFEMTestPetscVectorIO
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}/Seq
         ${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/VectorIO/demo.lua
         )

set_tests_properties(PetscVectorIO PROPERTIES TIMEOUT 20)

add_test(PetscVectorIO-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4
         MoReFEMTestPetscVectorIO
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4
         ${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/VectorIO/demo.lua
         )

set_tests_properties(PetscVectorIO-mpi PROPERTIES TIMEOUT 20)
