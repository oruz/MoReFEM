/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace TestNS::PetscNS::MatrixOperationsNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string ret("Test Petsc Matrix Operations");
            return ret;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


        template<std::size_t I>
        inline const GlobalMatrix& Model::GetInitializedMatrix() const noexcept
        {
            static_assert(I < Utilities::ArraySize<decltype(initialized_matrix_list_)>::GetValue());
            assert(!(!initialized_matrix_list_[I]));
            return *initialized_matrix_list_[I];
        }


        template<std::size_t I>
        inline GlobalMatrix& Model::GetNonCstInitializedMatrix() noexcept
        {
            return const_cast<GlobalMatrix&>(GetInitializedMatrix<I>());
        }


        inline const NumberingSubset& Model::GetNumberingSubset() const noexcept
        {
            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
            decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(sole);
            return god_of_dof.GetNumberingSubset(sole);
        }


        template<std::size_t I>
        inline const GlobalVector& Model::GetInitializedVector() const noexcept
        {
            static_assert(I < Utilities::ArraySize<decltype(initialized_vector_list_)>::GetValue());
            assert(!(!initialized_vector_list_[I]));
            return *initialized_vector_list_[I];
        }


        template<std::size_t I>
        inline GlobalVector& Model::GetNonCstInitializedVector() noexcept
        {
            return const_cast<GlobalVector&>(GetInitializedVector<I>());
        }


    } // namespace  TestNS::PetscNS::MatrixOperationsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HXX_
