/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(sole);
        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(sole);
        decltype(auto) mpi = god_of_dof.GetMpi();


        for (auto i = 0ul; i < Utilities::ArraySize<decltype(initialized_matrix_list_)>::GetValue(); ++i)
        {
            initialized_matrix_list_[i] = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
            AllocateGlobalMatrix(god_of_dof, *initialized_matrix_list_[i]);
        }


        for (auto i = 0ul; i < Utilities::ArraySize<decltype(initialized_vector_list_)>::GetValue(); ++i)
        {
            initialized_vector_list_[i] = std::make_unique<GlobalVector>(numbering_subset);
            AllocateGlobalVector(god_of_dof, *initialized_vector_list_[i]);
        }


        // Put values into the first vector.
        {
            auto& vector = GetNonCstInitializedVector<0>();

            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector, __FILE__, __LINE__);

            const auto size = content.GetSize(__FILE__, __LINE__);
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            for (auto i = 0ul; i < size; ++i)
                content[i] = rank_plus_one * std::sqrt(i); // completely arbitrary value with no redundancy!
        }
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
