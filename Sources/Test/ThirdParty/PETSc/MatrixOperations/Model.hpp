/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/Array.hpp"

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS::PetscNS::MatrixOperationsNS
    {


        /*!
         * \brief Toy model used to perform tests about reconstruction of a \a Mesh from pre-partitioned data.
         *
         * This test must be run in parallel; its principle is to:
         * - Initialize the model normally (including separating the mesh among all mpi processors and reducing it).
         * - Write the data related to the partition of said mesh.
         * - Load from the data written a new mesh.
         * - Check both mesh are indistinguishable.
         */
        //! \copydoc doxygen_hide_model_4_test
        class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = Model;

            //! Convenient alias.
            using parent = MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>;


          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize();

            /*!
             * \brief Constant access to a matrix that was fully initialized (i.e. with layout also defined, not just
             * the constructor called).
             *
             * \tparam I Index of the requested matrix (some tests require several so several are stored). There are
             * static check for the consistency of this index.
             *
             * \return Constant accessor to an initialized matrix.
             */
            template<std::size_t I>
            const GlobalMatrix& GetInitializedMatrix() const noexcept;

            /*!
             * \brief Non-constant access to a matrix that was fully initialized (i.e. with layout also defined, not
             * just the constructor called).
             *
             * \tparam I Index of the requested matrix (some tests require several so several are stored). There are
             * static check for the consistency of this index.
             *
             * \return Non-constant accessor to an initialized matrix.
             */
            template<std::size_t I>
            GlobalMatrix& GetNonCstInitializedMatrix() noexcept;


            /*!
             * \brief Constant access to a vector that was fully initialized (i.e. with layout also defined, not just
             * the constructor called).
             *
             * \tparam I Index of the requested vector (some tests require several so several are stored). There are
             * static check for the consistency of this index.
             *
             * \return Constant accessor to an initialized vector.
             */
            template<std::size_t I>
            const GlobalVector& GetInitializedVector() const noexcept;

            /*!
             * \brief Non-constant access to a vector that was fully initialized (i.e. with layout also defined, not
             * just the constructor called).
             *
             * \tparam I Index of the requested vector (some tests require several so several are stored). There are
             * static check for the consistency of this index.
             *
             * \return Non-constant accessor to an initialized vector.
             */
            template<std::size_t I>
            GlobalVector& GetNonCstInitializedVector() noexcept;


            //! Get the \a NumberingSubset used in the tests.
            const NumberingSubset& GetNumberingSubset() const noexcept;


          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}

          private:
            //! Matrices used in the test that have been fully initialized (mpi layout is assigned).
            GlobalMatrix::array_unique_ptr<3> initialized_matrix_list_ =
                Utilities::NullptrArray<GlobalMatrix::unique_ptr, 3>();

            //! Vectors used in the test that have been fully initialized (mpi layout is assigned).
            GlobalVector::array_unique_ptr<3> initialized_vector_list_ =
                Utilities::NullptrArray<GlobalVector::unique_ptr, 3>();
        };

    } // namespace TestNS::PetscNS::MatrixOperationsNS


} // namespace MoReFEM


#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_OPERATIONS_x_MODEL_HPP_
