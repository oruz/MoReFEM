/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_matrix_operations
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::PetscNS::MatrixOperationsNS::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(model_initialization, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);
}


BOOST_FIXTURE_TEST_CASE(create_transpose, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) numbering_subset = model.GetNumberingSubset();

    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::MatCreateTranspose(model.GetInitializedMatrix<0>(), non_allocated_matrix, __FILE__, __LINE__);
}

BOOST_FIXTURE_TEST_CASE(axpy, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::AXPY<NonZeroPattern::same>(
        1., model.GetInitializedMatrix<0>(), model.GetNonCstInitializedMatrix<1>(), __FILE__, __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_shift, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatShift(1., model.GetNonCstInitializedMatrix<0>(), __FILE__, __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatMult(model.GetInitializedMatrix<0>(),
                             model.GetInitializedVector<0>(),
                             model.GetNonCstInitializedVector<1>(),
                             __FILE__,
                             __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_mult_add, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatMultAdd(model.GetInitializedMatrix<0>(),
                                model.GetInitializedVector<0>(),
                                model.GetInitializedVector<1>(),
                                model.GetNonCstInitializedVector<2>(),
                                __FILE__,
                                __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_mult_transpose, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatMultTranspose(model.GetInitializedMatrix<0>(),
                                      model.GetInitializedVector<0>(),
                                      model.GetNonCstInitializedVector<1>(),
                                      __FILE__,
                                      __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_mult_transpose_add, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatMultTransposeAdd(model.GetInitializedMatrix<0>(),
                                         model.GetInitializedVector<0>(),
                                         model.GetInitializedVector<1>(),
                                         model.GetNonCstInitializedVector<2>(),
                                         __FILE__,
                                         __LINE__);
}


BOOST_FIXTURE_TEST_CASE(mat_transpose, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::MatTranspose(
        model.GetInitializedMatrix<0>(), non_allocated_matrix, __FILE__, __LINE__, Wrappers::Petsc::DoReuseMatrix::no);

    Wrappers::Petsc::MatTranspose(
        model.GetInitializedMatrix<0>(), non_allocated_matrix, __FILE__, __LINE__, Wrappers::Petsc::DoReuseMatrix::yes);
}


BOOST_FIXTURE_TEST_CASE(mat_transpose_inplace, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    Wrappers::Petsc::MatTranspose(model.GetNonCstInitializedMatrix<0>(),
                                  model.GetNonCstInitializedMatrix<0>(),
                                  __FILE__,
                                  __LINE__,
                                  Wrappers::Petsc::DoReuseMatrix::in_place);
}


BOOST_FIXTURE_TEST_CASE(mat_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::MatMatMult(model.GetInitializedMatrix<0>(),
                                model.GetInitializedMatrix<1>(),
                                non_allocated_matrix,
                                __FILE__,
                                __LINE__,
                                Wrappers::Petsc::DoReuseMatrix::no);

    Wrappers::Petsc::MatMatMult(model.GetInitializedMatrix<0>(),
                                model.GetInitializedMatrix<1>(),
                                non_allocated_matrix,
                                __FILE__,
                                __LINE__,
                                Wrappers::Petsc::DoReuseMatrix::yes);
}


BOOST_FIXTURE_TEST_CASE(mat_transpose_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::MatTransposeMatMult(model.GetInitializedMatrix<0>(),
                                         model.GetInitializedMatrix<1>(),
                                         non_allocated_matrix,
                                         __FILE__,
                                         __LINE__,
                                         Wrappers::Petsc::DoReuseMatrix::no);

    Wrappers::Petsc::MatTransposeMatMult(model.GetInitializedMatrix<0>(),
                                         model.GetInitializedMatrix<1>(),
                                         non_allocated_matrix,
                                         __FILE__,
                                         __LINE__,
                                         Wrappers::Petsc::DoReuseMatrix::yes);
}


BOOST_FIXTURE_TEST_CASE(mat_mat_transpose_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    if (mpi.Nprocessor<int>() == 1)
    {
        Wrappers::Petsc::MatMatTransposeMult(model.GetInitializedMatrix<0>(),
                                             model.GetInitializedMatrix<1>(),
                                             non_allocated_matrix,
                                             __FILE__,
                                             __LINE__,
                                             Wrappers::Petsc::DoReuseMatrix::no);

        Wrappers::Petsc::MatMatTransposeMult(model.GetInitializedMatrix<0>(),
                                             model.GetInitializedMatrix<1>(),
                                             non_allocated_matrix,
                                             __FILE__,
                                             __LINE__,
                                             Wrappers::Petsc::DoReuseMatrix::yes);
    }
}


BOOST_FIXTURE_TEST_CASE(pt_a_p, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::PtAP(model.GetInitializedMatrix<0>(),
                          model.GetInitializedMatrix<1>(),
                          non_allocated_matrix,
                          __FILE__,
                          __LINE__,
                          Wrappers::Petsc::DoReuseMatrix::no);

    Wrappers::Petsc::PtAP(model.GetInitializedMatrix<0>(),
                          model.GetInitializedMatrix<1>(),
                          non_allocated_matrix,
                          __FILE__,
                          __LINE__,
                          Wrappers::Petsc::DoReuseMatrix::yes);
}


BOOST_FIXTURE_TEST_CASE(mat_mat_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) numbering_subset = model.GetNumberingSubset();
    GlobalMatrix non_allocated_matrix(numbering_subset, numbering_subset);

    Wrappers::Petsc::MatMatMatMult(model.GetInitializedMatrix<0>(),
                                   model.GetInitializedMatrix<1>(),
                                   model.GetInitializedMatrix<2>(),
                                   non_allocated_matrix,
                                   __FILE__,
                                   __LINE__,
                                   Wrappers::Petsc::DoReuseMatrix::no);

    Wrappers::Petsc::MatMatMatMult(model.GetInitializedMatrix<0>(),
                                   model.GetInitializedMatrix<1>(),
                                   model.GetInitializedMatrix<2>(),
                                   non_allocated_matrix,
                                   __FILE__,
                                   __LINE__,
                                   Wrappers::Petsc::DoReuseMatrix::yes);
}

PRAGMA_DIAGNOSTIC(pop)
