/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr>
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cassert>
#include <cstdlib>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#define BOOST_TEST_MODULE matrix_pattern
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    std::vector<std::vector<PetscInt>> MatrixContent()
    {
        static std::vector<std::vector<PetscInt>> ret{
            { 10, 20, 0, 0, 0, 0 }, { 0, 30, 0, 40, 0, 0 }, { 0, 0, 50, 60, 70, 0 }, { 0, 0, 0, 0, 0, 80 }
        };

        return ret;
    };


    std::vector<PetscInt> iCSR()
    {
        static std::vector<PetscInt> ret{ 0, 2, 4, 7, 8 };

        return ret;
    }


    std::vector<PetscInt> jCSR()
    {
        static std::vector<PetscInt> ret{ 0, 1, 1, 3, 2, 3, 4, 5 };

        return ret;
    }


    template<class T>
    std::vector<std::vector<PetscInt>> NnonZeroPerRow(const std::vector<std::vector<T>>& matrix);


} // namespace


BOOST_AUTO_TEST_CASE(check_csr)
{
    Wrappers::Petsc::MatrixPattern pattern(NnonZeroPerRow(MatrixContent()));

    BOOST_CHECK_EQUAL(pattern.GetICsr().size(), iCSR().size());
    BOOST_CHECK(pattern.GetICsr() == iCSR());

    BOOST_CHECK_EQUAL(pattern.GetJCsr().size(), jCSR().size());
    BOOST_CHECK(pattern.GetJCsr() == jCSR());
}


BOOST_AUTO_TEST_CASE(non_zero_position_per_row)
{
    Wrappers::Petsc::MatrixPattern pattern(NnonZeroPerRow(MatrixContent()));

    const auto non_zero_position_per_row = ExtractNonZeroPositionsPerRow(pattern);

    BOOST_CHECK_EQUAL(non_zero_position_per_row.size(), pattern.Nrow());
    BOOST_CHECK_EQUAL(non_zero_position_per_row.size(), 4ul);

    BOOST_CHECK(non_zero_position_per_row[0] == std::vector<PetscInt>({ 0, 1 }));
    BOOST_CHECK(non_zero_position_per_row[1] == std::vector<PetscInt>({ 1, 3 }));
    BOOST_CHECK(non_zero_position_per_row[2] == std::vector<PetscInt>({ 2, 3, 4 }));
    BOOST_CHECK(non_zero_position_per_row[3] == std::vector<PetscInt>({ 5 }));
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    template<class T>
    std::vector<std::vector<PetscInt>> NnonZeroPerRow(const std::vector<std::vector<T>>& matrix)
    {
        std::vector<std::vector<PetscInt>> ret;
        ret.reserve(matrix.size());
        assert(!matrix.empty());
        const auto Ncol = matrix.back().size();

        for (const auto& row : matrix)
        {
            assert(row.size() == Ncol);

            std::vector<PetscInt> non_zero_position;

            for (auto col_index = 0ul; col_index < Ncol; ++col_index)
            {
                if (!NumericNS::IsZero(row[col_index]))
                    non_zero_position.push_back(static_cast<PetscInt>(col_index));
            }

            ret.emplace_back(std::move(non_zero_position));
        }

        return ret;
    }


} // namespace
