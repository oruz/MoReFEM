/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE mpi_broadcast
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_broadcast, MoReFEM::TestNS::FixtureNS::Mpi)


BOOST_AUTO_TEST_CASE(broadcast_vector_root_sender)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<std::size_t> expected_result{ 2, 5, 7, 11 };
    std::vector<std::size_t> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_vector_no_root_sender)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    constexpr auto sender_rank = 2;

    std::vector<std::size_t> expected_result{ 2, 5, 7, 11 };
    std::vector<std::size_t> broadcasted_data;

    if (mpi.GetRank<int>() == sender_rank)
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data, sender_rank);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_single_value)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    long expected_result{ 15457215l };
    long broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}

BOOST_AUTO_TEST_CASE(broadcast_bool_true)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    bool expected_result{ true };
    bool broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}

BOOST_AUTO_TEST_CASE(broadcast_bool_false)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    bool expected_result{ false };
    bool broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_vector_bool)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<bool> expected_result{ false, true, true, false, true };
    std::vector<bool> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_empty_vector)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<bool> expected_result;
    std::vector<bool> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
