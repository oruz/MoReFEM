/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE mpi_gather
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    const std::vector<int>& GetImbalancedSentData(const Wrappers::Mpi& mpi) noexcept;

    const std::vector<int>& GetBalancedSentData(const Wrappers::Mpi& mpi) noexcept;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_gather, MoReFEM::TestNS::FixtureNS::Mpi)


BOOST_AUTO_TEST_CASE(gather)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) balanced_sent_data = GetBalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.Gather(balanced_sent_data, gathered_data));

    if (mpi.GetRank<int>() == 0)
    {
        std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23 };
        BOOST_CHECK(gathered_data == expected);
    } else
        BOOST_CHECK(gathered_data.empty());
}


BOOST_AUTO_TEST_CASE(all_gather)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) balanced_sent_data = GetBalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.AllGather(balanced_sent_data, gathered_data));

    std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23 };
    BOOST_CHECK(gathered_data == expected);
}


//// \todo See if Boost may handle a check abort in some way
////    BOOST_AUTO_TEST_CASE(gather_invalid_size)
////    {
////        decltype(auto) imbalanced_sent_data = GetImbalancedSentData();
////        decltype(auto) mpi = GetMpi();
////
////        std::vector<int> gathered_data;
////        BOOST_CHECK_ABORT(mpi.Gather(imbalanced_sent_data, gathered_data));
////    }
//
//
BOOST_AUTO_TEST_CASE(gatherv)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) imbalanced_sent_data = GetImbalancedSentData(mpi);


    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.Gatherv(imbalanced_sent_data, gathered_data));

    if (mpi.GetRank<int>() == 0)
    {
        std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
        BOOST_CHECK(gathered_data == expected);
    } else
        BOOST_CHECK(gathered_data.empty());
}


BOOST_AUTO_TEST_CASE(allgatherv)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) imbalanced_sent_data = GetImbalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.AllGatherv(imbalanced_sent_data, gathered_data));

    std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
    BOOST_CHECK(gathered_data == expected);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    template<bool IsBalancedT>
    std::vector<int> GenerateSentData(int rank)
    {
        std::vector<int> ret;

        switch (rank)
        {
        case 0:
            ret = { 1, 2, 3 };
            break;
        case 1:
            ret = { 11, 12, 13 };
            break;
        case 2:
            if (IsBalancedT)
                ret = { 21, 22, 23 };
            else
                ret = { 21, 22, 23, 24 };
            break;
        }

        return ret;
    }


    const std::vector<int>& GetImbalancedSentData(const Wrappers::Mpi& mpi) noexcept
    {
        static std::vector<int> ret = GenerateSentData<false>(mpi.GetRank<int>());
        return ret;
    }


    const std::vector<int>& GetBalancedSentData(const Wrappers::Mpi& mpi) noexcept
    {
        static std::vector<int> ret = GenerateSentData<true>(mpi.GetRank<int>());
        return ret;
    }


} // namespace
