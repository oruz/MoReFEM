/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE mpi_is_same_for_all_ranks
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_is_same_for_all_ranks, MoReFEM::TestNS::FixtureNS::Mpi)


BOOST_AUTO_TEST_CASE(integer_success)
{
    decltype(auto) mpi = GetMpi();

    std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == true);
}


BOOST_AUTO_TEST_CASE(integer_failure_size)
{
    decltype(auto) mpi = GetMpi();

    std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    if (mpi.GetRank<int>() == 1)
        test.push_back(11);

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == false);
}


BOOST_AUTO_TEST_CASE(integer_failure_content)
{
    decltype(auto) mpi = GetMpi();

    std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    if (mpi.GetRank<int>() == 2)
        test[0] = 1;

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == false);
}


BOOST_AUTO_TEST_CASE(float_epsilon)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<float> test{ 2.34f, 3.84f, 0.25f, 1.47f, 9.f };

    if (mpi.GetRank<int>() == 2)
        test[1] += 0.004f;

    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-2f) == true);
    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-5f) == false);
}


BOOST_AUTO_TEST_CASE(double_epsilon)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<double> test{ 2.34, 3.84, 0.25, 1.47, 9. };

    if (mpi.GetRank<int>() == 2)
        test[1] += 0.004;

    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-2) == true);
    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-5) == false);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
