/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE mpi_send_receive
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_send_receive, MoReFEM::TestNS::FixtureNS::Mpi)

BOOST_AUTO_TEST_CASE(single_value)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 4);

    const auto rank = mpi.GetRank<int>();
    const auto Nproc = mpi.Nprocessor<int>();

    double token = std::numeric_limits<double>::lowest();

    switch (rank)
    {
    case 1:
        token = mpi.Receive<double>(static_cast<std::size_t>(rank - 1));
        BOOST_CHECK_CLOSE(token, 1.245, 1.e-6);
        break;
    case 2:
        token = mpi.Receive<double>(static_cast<std::size_t>(rank - 1));
        BOOST_CHECK_CLOSE(token, 11.245, 1.e-6);
        break;
    case 3:
        token = mpi.Receive<double>(static_cast<std::size_t>(rank - 1));
        BOOST_CHECK_CLOSE(token, 31.245, 1.e-6);
        break;
    case 0:
        // Set the token's value if you are process 0
        token = 1.245;
        break;
    }

    token += static_cast<double>(10 * rank);

    mpi.Send(static_cast<std::size_t>((rank + 1) % Nproc), token);

    // Now process 0 can receive from the last process.
    if (rank == 0)
    {
        token = mpi.Receive<double>(static_cast<std::size_t>(Nproc - 1));

        BOOST_CHECK_CLOSE(token, 61.245, 1.e-6);
    }
}

BOOST_AUTO_TEST_CASE(array)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() == 4);

    const auto rank = mpi.GetRank<int>();
    const auto Nproc = mpi.Nprocessor<int>();

    namespace ipl = Utilities::InputDataNS;

    std::vector<int> token{ NumericNS::UninitializedIndex<int>(), NumericNS::UninitializedIndex<int>() };

    constexpr auto MAX_LENGTH = 6u;

    switch (rank)
    {
    case 1:
        token = mpi.Receive<int>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0 }));
        break;
    case 2:
        token = mpi.Receive<int>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10 }));
        break;
    case 3:
        token = mpi.Receive<int>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10, 20 }));
        break;
    case 0:
        // Set the token's value if you are process 0
        token = { -1, -2 };
        break;
    }

    token.push_back(rank * 10);

    mpi.SendContainer(static_cast<std::size_t>((rank + 1) % Nproc), token);

    // Now process 0 can receive from the last process.
    if (rank == 0)
    {
        token = mpi.Receive<int>(static_cast<std::size_t>(Nproc - 1), MAX_LENGTH);

        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10, 20, 30 }));
    }
}


BOOST_AUTO_TEST_CASE(array_strong_type)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() == 4);

    const auto rank = mpi.GetRank<int>();
    const auto Nproc = mpi.Nprocessor<int>();

    using IntType = StrongType<int,
                               struct IntTypeTag,
                               StrongTypeNS::DefaultConstructible,
                               StrongTypeNS::Comparable,
                               StrongTypeNS::AsMpiDatatype>;

    namespace ipl = Utilities::InputDataNS;

    std::vector<IntType> token(2ul);

    constexpr auto MAX_LENGTH = 6u;

    switch (rank)
    {
    case 1:
        token = mpi.Receive<IntType>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0) }));
        break;
    case 2:
        token = mpi.Receive<IntType>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10) }));
        break;
    case 3:
        token = mpi.Receive<IntType>(static_cast<std::size_t>(rank - 1), MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10), IntType(20) }));
        break;
    case 0:
        // Set the token's value if you are process 0
        token = { IntType(-1), IntType(-2) };
        break;
    }

    token.push_back(IntType(rank * 10));

    mpi.SendContainer(static_cast<std::size_t>((rank + 1) % Nproc), token);

    // Now process 0 can receive from the last process.
    if (rank == 0)
    {
        token = mpi.Receive<IntType>(static_cast<std::size_t>(Nproc - 1), MAX_LENGTH);

        BOOST_CHECK(
            (token
             == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10), IntType(20), IntType(30) }));
    }
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
