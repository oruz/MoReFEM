/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 16 Apr 2018 17:32:47 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#define BOOST_TEST_MODULE variable_time_step
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/VariableTimeStep.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "Test/Core/VariableTimeStep/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestVariableTimeStepNS::InputData, program_type::test>;
    };


    using fixture_type = TestNS::FixtureNS::Model<MockModel>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(variable_time_step, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.057, 1.e-6);
    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.05, 1.e-6);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.057, 1.e-6);

    time_manager.IncrementTime();
    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.107, 1.e-6);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    time_manager.IncrementTime();
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.025, 1.e-6);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.132, 1.e-6);

    // \todo #1274 Clarify expected behaviour and if correct adapt the time steps.
    // \todo #1274 Also add tests for others variable step method.
    //    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    //    BOOST_CHECK(time_manager.GetTimeStep(),0.05));
    //    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    //    BOOST_CHECK(time_manager.GetTimeStep(),0.1));
    //    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    //    BOOST_CHECK(time_manager.GetTimeStep(),0.1));
    //
    //    time_manager.IncrementTime();
    //    BOOST_CHECK(time_manager.GetTime(),0.232));
}

PRAGMA_DIAGNOSTIC(pop)
