/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestVariableTimeStepNS
    {


        // clang-format off
        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,
            InputDataNS::TimeManager::MinimumTimeStep,
            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestVariableTimeStepNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_INPUT_DATA_HPP_
