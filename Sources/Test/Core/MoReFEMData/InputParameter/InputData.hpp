/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_PARAMETER_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_PARAMETER_x_INPUT_DATA_HPP_

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestInputDataSolidNS
    {


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::VectorialTransientSource<1>
        >;
        // clang-format on

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestInputDataSolidNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_PARAMETER_x_INPUT_DATA_HPP_
