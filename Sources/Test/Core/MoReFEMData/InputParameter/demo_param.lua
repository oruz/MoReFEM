-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


Solid = {

	Kappa1 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
		value = 4.

	}, -- Kappa1

	Kappa2 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "lua_function",


        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = [[
        function (x, y, z)
            return 3. * x + 0.5 * y - 4.7 * z;
        end]]
        ,

	}, -- Kappa2

	HyperelasticBulk = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "piecewise_constant_by_domain",

		-- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = { [1] = 10. , [2] = -15. }

	}, -- HyperelasticBulk

} -- Solid


TransientSource1 = {

    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = { 'constant', 'lua_function', 'lua_function'},

    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    value = { 23.,
            [[
                function(x, y, z)
                    return 2. * x + y - z
                end
            ]]
            ,
            [[
                function(x, y, z)
                    return -12. * x + 3. * y -2.32 * z - 4.452
                end
            ]]
    }

} -- TransientSource1

