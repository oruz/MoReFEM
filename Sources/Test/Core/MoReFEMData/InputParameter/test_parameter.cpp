/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE input_data_parameter
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


#include <cstdlib>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/InputData.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/SpatialLuaFunction.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Core/MoReFEMData/InputParameter/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestInputDataSolidNS::InputData, program_type::test>;
    };


    using fixture_type = TestNS::FixtureNS::Model<MockModel>;


} // namespace


BOOST_FIXTURE_TEST_CASE(param_as_constant, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    const auto kappa1_constant_value = IPL::Extract<InputDataNS::Solid::Kappa1::Value>::Value(input_data);

    BOOST_REQUIRE(std::get_if<0>(&kappa1_constant_value) != nullptr);
    BOOST_CHECK(NumericNS::AreEqual(std::get<0>(kappa1_constant_value), 4.));
}


BOOST_FIXTURE_TEST_CASE(param_as_lua_function, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    const auto kappa2_lua_value = IPL::Extract<InputDataNS::Solid::Kappa2::Value>::Value(input_data);

    BOOST_REQUIRE(std::get_if<1>(&kappa2_lua_value) != nullptr);
    BOOST_CHECK(NumericNS::AreEqual(std::get<1>(kappa2_lua_value)(0., 0., 0.), 0.));
    BOOST_CHECK(NumericNS::AreEqual(std::get<1>(kappa2_lua_value)(1., -1.5, 10.), -44.75));
}


BOOST_FIXTURE_TEST_CASE(param_as_piecewise_constant, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    const auto bulk_piecewise_value = IPL::Extract<InputDataNS::Solid::HyperelasticBulk::Value>::Value(input_data);

    BOOST_REQUIRE(std::get_if<2>(&bulk_piecewise_value) != nullptr);
    BOOST_REQUIRE(std::get<2>(bulk_piecewise_value).size() == 2ul);

    const auto it1 = std::get<2>(bulk_piecewise_value).find(1);

    BOOST_CHECK(it1 != std::get<2>(bulk_piecewise_value).cend());
    BOOST_CHECK(NumericNS::AreEqual(it1->second, 10.));

    const auto it2 = std::get<2>(bulk_piecewise_value).find(2);

    BOOST_CHECK(it2 != std::get<2>(bulk_piecewise_value).cend());
    BOOST_CHECK(NumericNS::AreEqual(it2->second, -15.));

    const auto it3 = std::get<2>(bulk_piecewise_value).find(0);

    BOOST_CHECK(it3 == std::get<2>(bulk_piecewise_value).cend());
}


BOOST_FIXTURE_TEST_CASE(param_as_variant, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    decltype(auto) value_list = IPL::Extract<InputDataNS::VectorialTransientSource<1>::Value>::Value(input_data);

    BOOST_CHECK(NumericNS::AreEqual(std::get<double>(value_list[0]), 23.));

    decltype(auto) y_function = std::get<spatial_lua_function>(value_list[1]);
    decltype(auto) z_function = std::get<spatial_lua_function>(value_list[2]);

    BOOST_CHECK(NumericNS::AreEqual(y_function(0., 0., 0.), 0.));
    BOOST_CHECK(NumericNS::AreEqual(y_function(1., 2., 3.), 1.));

    BOOST_CHECK(NumericNS::AreEqual(z_function(0., 0., 0.), -4.452));
    BOOST_CHECK(NumericNS::AreEqual(z_function(1., 2., 3.), -17.412));
}


PRAGMA_DIAGNOSTIC(pop)
