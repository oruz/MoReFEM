/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_WRITE_LUA_FILE_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_WRITE_LUA_FILE_x_INPUT_DATA_HPP_

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS::WriteLuaFileNS
    {


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,
            InputDataNS::Result,
            InputDataNS::ReactionNS::ReactionCoefficient,

            InputDataNS::Mesh<5>,

            InputDataNS::ScalarTransientSource<1>,
            InputDataNS::ScalarTransientSource<72>,
            InputDataNS::VectorialTransientSource<4>
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNS::WriteLuaFileNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_WRITE_LUA_FILE_x_INPUT_DATA_HPP_
