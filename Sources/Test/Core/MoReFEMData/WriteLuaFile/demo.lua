-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {


	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,


	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,


	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 10.

} -- transient

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
    output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/TestWriteLuaFile",


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,


	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

Mesh5 = {


	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = "${MOREFEM_ROOT}/Data/Mesh/Bar.mesh",


	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = "Medit",


	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,


	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh5


-- Value of R.
-- Expected format: VALUE
ReactionCoefficient = 1.


TransientSource4 = {

    nature = { "lua_function", "constant", "piecewise_constant_by_domain"},

    value = {
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],
        0.,
        { [1] = 0., [3] = 14.5, [4] = -31.231 } }

} -- TransientSource4



TransientSource1 = {

    nature = "piecewise_constant_by_domain",

    value = { [1] = 0., [3] = 14.5, [4] = -31.231 }

} -- TransientSource1


TransientSource72 = {

    nature = "lua_function",

    value =
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],

} -- TransientSource72
