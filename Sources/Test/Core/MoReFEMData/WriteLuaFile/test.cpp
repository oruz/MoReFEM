/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 25 Jul 2018 18:29:56 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>
#include <tuple>

#include "Utilities/Containers/Print.hpp"

#define BOOST_TEST_MODULE core_write_lua_file
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Core/MoReFEMData/WriteLuaFile/InputData.hpp"
#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/InitializeTestMoReFEMData.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    const std::string& GetOutputDirectory();


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(create_default_lua_when_unexisting, TestNS::FixtureNS::Environment)
{
    decltype(auto) output_directory = GetOutputDirectory();

    auto generated_lua = output_directory + "implicitly_default_generated.lua";

    BOOST_REQUIRE(
        !FilesystemNS::File::DoExist(generated_lua)); // The directory in which it stands has just been recreated!

    // When the Lua file doesn't exist, it is created and an exception is thrown to stop the program.
    using input_data_type = TestNS::WriteLuaFileNS::InputData;
    BOOST_CHECK_THROW(TestNS::InitializeTestMoReFEMData<input_data_type> init(std::move(generated_lua)),
                      ExceptionNS::GracefulExit);
}


BOOST_FIXTURE_TEST_CASE(create_explicitly_default_lua_file, TestNS::FixtureNS::Environment)
{
    decltype(auto) output_directory = GetOutputDirectory();

    auto implicitly_generated_lua = output_directory + "implicitly_default_generated.lua";
    auto explicitly_generated_lua = output_directory + "explicitly_default_generated.lua";

    using input_data_type = TestNS::WriteLuaFileNS::InputData;
    Utilities::InputDataNS::CreateDefaultInputFile<input_data_type::Tuple>(explicitly_generated_lua);

    BOOST_CHECK(FilesystemNS::File::AreEquals(implicitly_generated_lua, explicitly_generated_lua, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(write_full_fledged_lua_file, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    auto full_fledged_lua_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/WriteLuaFile/demo.lua");

    using input_data_type = TestNS::WriteLuaFileNS::InputData;

    TestNS::InitializeTestMoReFEMData<input_data_type> init(std::move(full_fledged_lua_file));
    decltype(auto) morefem_data = init.GetMoReFEMData();

    // Write in output directory the content of the Lua file. We can't however do a direct comparison: Lua file may
    // not be paginated (indentation, spaces, new lines, ...) and so forth the same way.
    decltype(auto) output_directory = GetOutputDirectory();

    auto lua_file_with_content = output_directory + "explicitly_with_content.lua";
    Utilities::InputDataNS::Write(morefem_data.GetInputData(), lua_file_with_content);

    // However if now we load the newly generated file and rewrite it, we should obtain the exact same file.
    TestNS::InitializeTestMoReFEMData<input_data_type> init_from_generated(lua_file_with_content);

    auto regenerated_lua_file_with_content = output_directory + "regenerated_from_explicitly_with_content.lua";
    Utilities::InputDataNS::Write(init_from_generated.GetMoReFEMData().GetInputData(),
                                  regenerated_lua_file_with_content);

    BOOST_CHECK(
        FilesystemNS::File::AreEquals(lua_file_with_content, regenerated_lua_file_with_content, __FILE__, __LINE__));
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    const std::string& GetOutputDirectory()
    {
        static bool first_call = true;
        static std::string ret;

        if (first_call)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

            // Deliberately not the same as in the Lua file (to avoid automatic deletion of the directory during
            // MoReFEMData initialization).
            ret = environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/TestWriteLuaFile_not_from_lua_file/");

            if (Advanced::FilesystemNS::DirectoryNS::DoExist(ret))
                Advanced::FilesystemNS::DirectoryNS::Remove(ret, __FILE__, __LINE__);

            Advanced::FilesystemNS::DirectoryNS::Create(ret, __FILE__, __LINE__);

            first_call = false;
        }

        return ret;
    }


} // namespace
