/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Jul 2018 11:13:16 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE core_initial_condition
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


#include <cstdlib>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Core/MoReFEMData/InitialCondition/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestInputDataSolidNS::InputData, program_type::test>;
    };


    using fixture_type = TestNS::FixtureNS::Model<MockModel>;


} // namespace


BOOST_FIXTURE_TEST_CASE(initial_condition_properly_read, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    decltype(auto) initial_condition1_value_list =
        IPL::Extract<InputDataNS::InitialCondition<1>::Value>::Value(input_data);

    BOOST_REQUIRE(initial_condition1_value_list.size() == 3ul);


    const auto constant_value = initial_condition1_value_list[0];

    BOOST_REQUIRE(std::get_if<0>(&constant_value) != nullptr);
    BOOST_CHECK(NumericNS::AreEqual(std::get<0>(constant_value), 4.));

    const auto lua_value = initial_condition1_value_list[1];

    BOOST_REQUIRE(std::get_if<1>(&lua_value) != nullptr);
    BOOST_CHECK(NumericNS::AreEqual(std::get<1>(lua_value)(0., 0., 0.), 0.));
    BOOST_CHECK(NumericNS::AreEqual(std::get<1>(lua_value)(1., -1.5, 10.), -44.75));

    const auto piecewise_value = initial_condition1_value_list[2];

    BOOST_REQUIRE(std::get_if<2>(&piecewise_value) != nullptr);
    BOOST_REQUIRE(std::get<2>(piecewise_value).size() == 2ul);

    const auto it1 = std::get<2>(piecewise_value).find(1);

    BOOST_CHECK(it1 != std::get<2>(piecewise_value).cend());
    BOOST_CHECK(NumericNS::AreEqual(it1->second, 10.));

    const auto it2 = std::get<2>(piecewise_value).find(2);

    BOOST_CHECK(it2 != std::get<2>(piecewise_value).cend());
    BOOST_CHECK(NumericNS::AreEqual(it2->second, -15.));

    const auto it3 = std::get<2>(piecewise_value).find(0);

    BOOST_CHECK(it3 == std::get<2>(piecewise_value).cend());
}


BOOST_FIXTURE_TEST_CASE(initial_condition_ignored, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    decltype(auto) initial_condition4_value_list =
        IPL::Extract<InputDataNS::InitialCondition<4>::Value>::Value(input_data);

    BOOST_REQUIRE(initial_condition4_value_list.size() == 3ul);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[0]) == nullptr);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[1]) == nullptr);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[2]) == nullptr);
}


PRAGMA_DIAGNOSTIC(pop)
