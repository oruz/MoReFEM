add_executable(MoReFEMTestMoReFEMDataInputData
                ${CMAKE_CURRENT_LIST_DIR}/test_input_data.cpp
               )

target_link_libraries(MoReFEMTestMoReFEMDataInputData
                      ${MOREFEM_TEST_TOOLS})

add_test(MoReFEMDataInputData
         MoReFEMTestMoReFEMDataInputData
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}
         ${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/InputData/demo_input_data.lua
         )

set_tests_properties(MoReFEMDataInputData PROPERTIES TIMEOUT 20)
