/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 25 Jul 2018 18:29:56 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#define BOOST_TEST_MODULE core_input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/InputData/InputData.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Core/MoReFEMData/InputData/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestInputDataSolidNS::InputData, program_type::test>;
    };


    using fixture_type = TestNS::FixtureNS::Model<MockModel>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(check_value, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    namespace IPL = Utilities::InputDataNS;

    const auto coeff = IPL::Extract<InputDataNS::ReactionNS::ReactionCoefficient>::Value(input_data);

    BOOST_CHECK(NumericNS::AreEqual(coeff, 4.));
}

PRAGMA_DIAGNOSTIC(pop)
