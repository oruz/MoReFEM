/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Mar 2018 17:18:25 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <sstream>

#include "Utilities/InputData/LuaFunction.hpp"

#define BOOST_TEST_MODULE lua_function
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    std::string Fct1()
    {
        static std::string ret("function (x, y, z) "
                               "return 3 * x + y - z "
                               "end");

        return ret;
    }


    std::string Fct2()
    {
        static std::string ret("function (x, y, z) "
                               "return -10 * x + 5 * y - z "
                               "end");

        return ret;
    }


    using lua_function_type = Utilities::InputDataNS::LuaFunction<int(int, int, int)>;


} // namespace


BOOST_AUTO_TEST_CASE(simple_construction)
{
    lua_function_type function(Fct1());

    BOOST_CHECK_EQUAL(function(1, 2, 3), 2);
}


BOOST_AUTO_TEST_CASE(default_constructed_cant_be_used)
{
    lua_function_type function;

    BOOST_CHECK_THROW(function(1, 2, 3), std::exception);
}


BOOST_AUTO_TEST_CASE(copy_constructor)
{
    lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

    lua_function_type function2(function1);

    BOOST_CHECK_EQUAL(function2(-2, 4, -7), 5);
    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);
}


BOOST_AUTO_TEST_CASE(move_constructor)
{
    lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

    lua_function_type function2(std::move(function1));

    BOOST_CHECK_EQUAL(function2(-2, 4, -7), 5);
    BOOST_CHECK_THROW(function1(4, -11, 5), std::exception);
}


BOOST_AUTO_TEST_CASE(copy_assignment)
{
    lua_function_type function1(Fct1());

    lua_function_type function3;
    function3 = function1;
    BOOST_CHECK_EQUAL(function3(10, -2, 6), 22);

    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);

    lua_function_type function2(Fct2());

    function3 = function2;

    BOOST_CHECK_EQUAL(function3(4, -11, 5), -100);
}


BOOST_AUTO_TEST_CASE(move_assignment)
{
    lua_function_type function1(Fct1());

    lua_function_type function3;
    function3 = std::move(function1);
    BOOST_CHECK_EQUAL(function3(10, -2, 6), 22);

    BOOST_CHECK_THROW(function1(4, -11, 5), std::exception);

    lua_function_type function2(Fct2());

    function3 = std::move(function2);

    BOOST_CHECK_EQUAL(function3(4, -11, 5), -100);
}


BOOST_AUTO_TEST_CASE(self_copy)
{
    lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

#ifdef __clang__
    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wself-assign-overloaded") // that's on purpose here!
#endif                                                    // __clang__

    function1 = function1;

#ifdef __clang__
    PRAGMA_DIAGNOSTIC(pop)
#endif // __clang__

    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);
}


PRAGMA_DIAGNOSTIC(pop)
