/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 27 Feb 2020 18:22:06 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>

#define BOOST_TEST_MODULE strongtype

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strong_type_test)
{
    using StrongIntType = StrongType<int, struct IntTag>;
    using StrongIntRefType = StrongType<int&, struct IntRefTag>;
    using OtherStrongIntType = StrongType<int, struct OtherIntTag>;

    int raw_int = 42;
    auto raw_int_ref = &raw_int;

    StrongIntType strong_int = StrongIntType(raw_int);
    StrongIntRefType strong_int_ref = StrongIntRefType(raw_int);

    BOOST_REQUIRE(strong_int.Get() == raw_int);
    BOOST_REQUIRE(strong_int_ref.Get() == raw_int);
    BOOST_REQUIRE(&strong_int_ref.Get() == raw_int_ref);
    BOOST_REQUIRE(sizeof(StrongIntType) == sizeof(int));

    OtherStrongIntType other_strong_int = OtherStrongIntType(raw_int);
    StrongIntType casted_strong_int = StrongIntType(other_strong_int.Get());

    BOOST_REQUIRE(casted_strong_int.Get() == strong_int.Get());

    strong_int_ref.Get() = 3;
    BOOST_REQUIRE(raw_int == 3);

    using StrongStringType = StrongType<std::string, struct StringTag>;
    using StrongStringRefType = StrongType<std::string&, struct StringRefTag>;

    std::string raw_string = "foo";
    auto raw_string_ref = &raw_string;

    StrongStringType strong_string = StrongStringType(raw_string);
    StrongStringRefType strong_string_ref = StrongStringRefType(raw_string);

    BOOST_REQUIRE(strong_string.Get() == raw_string);
    BOOST_REQUIRE(strong_string_ref.Get() == raw_string);
    BOOST_REQUIRE(&strong_string_ref.Get() == raw_string_ref);
    BOOST_REQUIRE(sizeof(StrongStringType) == sizeof(std::string));

    strong_string_ref.Get() = "bar";
    BOOST_REQUIRE(raw_string == "bar");
}


BOOST_AUTO_TEST_CASE(addable)
{
    using Width = StrongType<double, struct WidthTag, StrongTypeNS::Addable>;
    Width w1(5.);
    Width w2(10.);
    Width sum = w1 + w2;

    BOOST_CHECK_EQUAL(sum.Get(), w1.Get() + w2.Get());

    Width diff = sum - w2;
    BOOST_CHECK_EQUAL(diff.Get(), w1.Get());
}


BOOST_AUTO_TEST_CASE(addable_const)
{
    // Note: this case was failing compilation prior to #1620; a failure here would probably be at compile time rather
    // than runtime.
    using Width = StrongType<double, struct WidthTag, StrongTypeNS::Addable>;
    const Width w1(5.);
    Width w2(10.);
    Width sum = w1 + w2;
    Width sum2 = w2 + w1;
    BOOST_CHECK_EQUAL(sum.Get(), w1.Get() + w2.Get());
    BOOST_CHECK_EQUAL(sum2.Get(), w1.Get() + w2.Get());

    Width diff = sum - w1;
    Width diff2 = w1 - sum;
    BOOST_CHECK_EQUAL(diff.Get(), w2.Get());
    BOOST_CHECK_EQUAL(diff2.Get(), -w2.Get());
}


BOOST_AUTO_TEST_CASE(addable_and_incrementable)
{
    {
        // First block for sanity check: ensure the tests are valid!
        // This is not DRY, but that's not a crime in a test...
        using MyInt = std::size_t;
        MyInt w1(10u);
        MyInt w2(5u);

        MyInt sum = w1++ + w2;
        BOOST_CHECK_EQUAL(w1, 11u);
        BOOST_CHECK_EQUAL(w2, 5u);
        BOOST_CHECK_EQUAL(sum, 15u);

        MyInt sum2 = ++w1 + w2;
        BOOST_CHECK_EQUAL(w1, 12u);
        BOOST_CHECK_EQUAL(w2, 5u);
        BOOST_CHECK_EQUAL(sum2, 17u);

        MyInt sum3 = w1 + w2--;
        BOOST_CHECK_EQUAL(w1, 12u);
        BOOST_CHECK_EQUAL(w2, 4u);
        BOOST_CHECK_EQUAL(sum3, 17u);

        MyInt diff = --w1 - w2;
        BOOST_CHECK_EQUAL(w1, 11u);
        BOOST_CHECK_EQUAL(w2, 4u);
        BOOST_CHECK_EQUAL(diff, 7u);

        w2 += w1;
        BOOST_CHECK_EQUAL(w2, 15u);

        sum2 -= w2;
        BOOST_CHECK_EQUAL(sum2, 2u);
    }

    {
        using MyInt = StrongType<std::size_t, struct WidthTag, StrongTypeNS::Addable, StrongTypeNS::Incrementable>;
        MyInt w1(10u);
        MyInt w2(5u);

        MyInt sum = w1++ + w2;
        BOOST_CHECK_EQUAL(w1.Get(), 11u);
        BOOST_CHECK_EQUAL(w2.Get(), 5u);
        BOOST_CHECK_EQUAL(sum.Get(), 15u);

        MyInt sum2 = ++w1 + w2;
        BOOST_CHECK_EQUAL(w1.Get(), 12u);
        BOOST_CHECK_EQUAL(w2.Get(), 5u);
        BOOST_CHECK_EQUAL(sum2.Get(), 17u);

        MyInt sum3 = w1 + w2--;
        BOOST_CHECK_EQUAL(w1.Get(), 12u);
        BOOST_CHECK_EQUAL(w2.Get(), 4u);
        BOOST_CHECK_EQUAL(sum3.Get(), 17u);

        MyInt diff = --w1 - w2;
        BOOST_CHECK_EQUAL(w1.Get(), 11u);
        BOOST_CHECK_EQUAL(w2.Get(), 4u);
        BOOST_CHECK_EQUAL(diff.Get(), 7u);

        w2 += w1;
        BOOST_CHECK_EQUAL(w2.Get(), 15u);

        sum2 -= w2;
        BOOST_CHECK_EQUAL(sum2.Get(), 2u);
    }
}


BOOST_AUTO_TEST_CASE(comparable)
{
    using Width = StrongType<int, struct WidthTag, StrongTypeNS::Comparable>;
    Width w1(5);
    Width w2(5);
    Width w3(10);

    BOOST_CHECK(w1 == w2);
    BOOST_CHECK(w1 < w3);
    BOOST_CHECK(w1 <= w2);
    BOOST_CHECK(w1 >= w2);
    BOOST_CHECK(w3 > w2);
    BOOST_CHECK(w2 != w3);
}


BOOST_AUTO_TEST_CASE(unordered_map)
{
    using SerialNumber =
        StrongType<std::string, struct SerialNumberTag, StrongTypeNS::Comparable, StrongTypeNS::Hashable>;

    std::string serial_string("ABC");
    SerialNumber serial_number(serial_string);

    std::unordered_map<SerialNumber, int> hash_map{ { SerialNumber{ "AA11" }, 10 }, { SerialNumber{ "BB22" }, 20 } };

    std::map<SerialNumber, int> map{ { SerialNumber{ "AA11" }, 10 }, { SerialNumber{ "BB22" }, 20 } };

    BOOST_CHECK_EQUAL(map[SerialNumber{ "AA11" }], 10);
    BOOST_CHECK_EQUAL(hash_map[SerialNumber{ "BB22" }], 20);
}


BOOST_AUTO_TEST_CASE(printable)
{
    using SerialNumber = StrongType<std::string, struct SerialNumberTag, StrongTypeNS::Printable>;

    SerialNumber serial_number("ABC");

    std::ostringstream oconv;
    oconv << serial_number;

    BOOST_CHECK_EQUAL(oconv.str(), "ABC");
}


BOOST_AUTO_TEST_CASE(default_constructible)
{
    using IntType = StrongType<double, struct IntTypeTag, StrongTypeNS::DefaultConstructible>;

    // This doesn't seem much, but the actual test failure is if the following line doesn't compile
    // (and it won't if the DefaultConstructible is omitted).
    IntType a;
    static_cast<void>(a);

    // One of the thing we want to be able to do with some StrongTypes...
    // Without DefaultConstructible resize() wouldn't work (you could work around with reserve / back_inserter in most
    // cases but still...)
    std::vector<IntType> v;
    v.resize(5ul);
}


namespace TestNS::StrongTypeNS
{

    using Width = StrongType<int, struct WidthTag, ::MoReFEM::StrongTypeNS::Comparable>;

} // namespace TestNS::StrongTypeNS


BOOST_AUTO_TEST_CASE(comparable_in_namespace)
{
    TestNS::StrongTypeNS::Width w1(5);
    TestNS::StrongTypeNS::Width w2(5);
    TestNS::StrongTypeNS::Width w3(10);

    BOOST_CHECK(w1 == w2);
    BOOST_CHECK(w1 < w3);
}


BOOST_AUTO_TEST_CASE(divisible_int)
{
    using UnsignedInt = StrongType<std::size_t, struct UnsignedIntTag, StrongTypeNS::Divisible>;
    UnsignedInt w1(23u);
    UnsignedInt w2(10u);

    UnsignedInt quotient = w1 / w2;
    BOOST_CHECK_EQUAL(quotient.Get(), 2u);

    UnsignedInt remainder = w1 % w2;
    BOOST_CHECK_EQUAL(remainder.Get(), 3u);
}


BOOST_AUTO_TEST_CASE(divisible_double)
{
    using Double = StrongType<double, struct DoubleTag, StrongTypeNS::Divisible>;
    Double w1(23.5);
    Double w2(10.);

    Double result = w1 / w2;
    BOOST_CHECK_EQUAL(result.Get(), 2.35);
}

PRAGMA_DIAGNOSTIC(pop)
