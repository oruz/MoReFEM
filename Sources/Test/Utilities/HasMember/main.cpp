//! \file
//
//
//  main.cpp
//  MoReFEM
//
//  Created by sebastien on 19/06/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <experimental/type_traits>
#include <vector>

#define BOOST_TEST_MODULE HAS_MEMBER
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/HasMember/HasMember.hpp"
#include "Utilities/Warnings/Pragma.hpp"


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-member-function")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-private-field")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-template")
#endif // __clang__


namespace
{


    struct A
    {

        void Foo();
    };


    class B
    {
      private:
        double Foo() const noexcept;
    };


    class ChildA : public A
    { };


    class C
    {

        bool Foo = false;
    };


    class D
    {
        int Foo();

        void Foo(double);
    };


    class WithTemplateMethod
    {

        template<class T>
        void Foo(T value);
    };


} // namespace

CREATE_MEMBER_CHECK(size);

template<typename T>
using size_helper = decltype(std::declval<T>().size());

template<typename T>
using supports_size = std::experimental::is_detected<size_helper, T>;


BOOST_AUTO_TEST_CASE(basic_case)
{
    BOOST_CHECK(HAS_MEMBER_size<std::vector<int>>::value == true);
    BOOST_CHECK(HAS_MEMBER_size<std::true_type>::value == false);

    BOOST_CHECK(supports_size<std::vector<int>>::value == true);
    BOOST_CHECK(supports_size<std::true_type>::value == false);
}

CREATE_MEMBER_CHECK(Foo);

BOOST_AUTO_TEST_CASE(inheritance)
{
    BOOST_CHECK(HAS_MEMBER_Foo<A>::value == true);
    BOOST_CHECK(HAS_MEMBER_Foo<ChildA>::value == true);
}


BOOST_AUTO_TEST_CASE(data_attribute)
{
    BOOST_CHECK(HAS_MEMBER_Foo<C>::value == true);
}


BOOST_AUTO_TEST_CASE(overload)
{
    BOOST_CHECK(HAS_MEMBER_Foo<D>::value == true);
}


BOOST_AUTO_TEST_CASE(template_method)
{
    BOOST_CHECK(HAS_MEMBER_Foo<WithTemplateMethod>::value == true);
}


PRAGMA_DIAGNOSTIC(pop)
