/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 27 Feb 2020 18:22:06 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <algorithm>
#include <memory>

#define BOOST_TEST_MODULE is_smart_ptr
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Miscellaneous.hpp"

using namespace MoReFEM::Utilities;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// Note: I know these tests could be handled at compilation time, but it would be rather wordy and I have deemed a
// runtime check good enough due to the (lack of) criticity of the feature tested here.


BOOST_AUTO_TEST_CASE(non_pointer_type)
{
    using type = int;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(raw_pointer)
{
    using type = int*;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(unique_ptr)
{
    using type = std::unique_ptr<float>;

    BOOST_CHECK(IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(shared_ptr)
{
    using type = std::shared_ptr<float>;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(unique_ptr_const_inside)
{
    using type = std::unique_ptr<const float>;

    BOOST_CHECK(IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(shared_ptr_const_inside)
{
    using type = std::shared_ptr<const float>;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(unique_ptr_const_outside)
{
    using type = const std::unique_ptr<float>;

    BOOST_CHECK(IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(shared_ptr_const_outside)
{
    using type = const std::shared_ptr<float>;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(unique_ptr_const_inside_and_outside)
{
    using type = const std::unique_ptr<const float>;

    BOOST_CHECK(IsUniquePtr<type>());
    BOOST_CHECK(!IsSharedPtr<type>());
}

BOOST_AUTO_TEST_CASE(shared_ptr_const_inside_and_outside)
{
    using type = const std::shared_ptr<const float>;

    BOOST_CHECK(!IsUniquePtr<type>());
    BOOST_CHECK(IsSharedPtr<type>());
}


PRAGMA_DIAGNOSTIC(pop)
