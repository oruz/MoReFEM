/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <chrono>
#include <iostream>
#include <thread>

#define BOOST_TEST_MODULE now_as_string
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct Fixture : public TestNS::FixtureNS::Environment, public TestNS::FixtureNS::Mpi
    { };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(same_on_all_processors, Fixture)
{
    decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    decltype(auto) output_directory =
        environment.GetEnvironmentVariable(std::string("MOREFEM_TEST_OUTPUT_DIR"), __FILE__, __LINE__);

    output_directory += "/Test/Utilities/Now";

    if (mpi.IsRootProcessor())
    {
        if (!Advanced::FilesystemNS::DirectoryNS::DoExist(output_directory))
            Advanced::FilesystemNS::DirectoryNS::Create(output_directory, __FILE__, __LINE__);
    }

    mpi.Barrier();

    std::ofstream out;
    std::ostringstream oconv;
    oconv << output_directory << "/now_" << mpi.GetRank<int>() << ".txt";

    std::string output_file = oconv.str();
    FilesystemNS::File::Create(out, output_file, __FILE__, __LINE__);

    for (auto i = 0ul; i < 10; ++i)
    {
        out << Utilities::Now(mpi) << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }

    mpi.Barrier();

    if (mpi.IsRootProcessor())
    {
        const auto Nprocessor = mpi.Nprocessor<int>();

        for (int i = 0; i < Nprocessor; ++i)
        {
            oconv.str("");
            oconv << output_directory << "/now_" << i << ".txt";

            FilesystemNS::File::AreEquals(output_file, oconv.str(), __FILE__, __LINE__);
        }
    }
}


PRAGMA_DIAGNOSTIC(pop)
