/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <deque>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE print_containers
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"
#include "Utilities/Containers/PrintPolicy/Pointer.hpp"
#include "Utilities/Containers/PrintPolicy/Variant.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(vector_simple)
{
    std::vector<int> a{ 5, 7, -4, 3 };

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(a, oconv);
    BOOST_CHECK_EQUAL(oconv.str(), "[5, 7, -4, 3]\n");
}


BOOST_AUTO_TEST_CASE(deque_custom)
{
    std::deque<std::string> a{ "Not", "a", "hello", "world" };

    std::ostringstream oconv;

    // clang-format off
    Utilities::PrintContainer<>::Do(a,
                                    oconv,
                                    PrintNS::Delimiter::separator(" "),
                                    PrintNS::Delimiter::opener(""),
                                    PrintNS::Delimiter::closer("!"));
    // clang-format on

    BOOST_CHECK_EQUAL(oconv.str(), "Not a hello world!");
}


BOOST_AUTO_TEST_CASE(vector_of_simple_variant)
{
    using variant = std::variant<int, double, std::string>;

    std::vector<variant> a{ 4.3, 3, "Hello", -6, "Bye!" };
    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Variant>::Do(a,
                                                                     oconv,
                                                                     PrintNS::Delimiter::separator(", "),
                                                                     PrintNS::Delimiter::opener("{"),
                                                                     PrintNS::Delimiter::closer("}"));

    BOOST_CHECK_EQUAL(oconv.str(), "{4.3, 3, Hello, -6, Bye!}");
}


BOOST_AUTO_TEST_CASE(list_print_n_elt)
{
    std::list<std::size_t> primes{ 2, 3, 5, 7, 11, 13 };
    std::ostringstream oconv;
    Utilities::PrintContainer<>::Nelt<3>(primes,
                                         oconv,
                                         PrintNS::Delimiter::separator(", "),
                                         PrintNS::Delimiter::opener("{"),
                                         PrintNS::Delimiter::closer(", ... }"));


    BOOST_CHECK_EQUAL(oconv.str(), "{2, 3, 5, ... }");
}


BOOST_AUTO_TEST_CASE(print_tuple)
{
    std::tuple<int, std::string, double, char> tuple{ 5, "Hello", 3.14, 'a' };

    std::ostringstream oconv;
    Utilities::PrintTuple(tuple,
                          oconv,
                          PrintNS::Delimiter::separator(", "),
                          PrintNS::Delimiter::opener("{ "),
                          PrintNS::Delimiter::closer("}"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ 5, \"Hello\", 3.14, \"a\"}");
}


BOOST_AUTO_TEST_CASE(print_vector_of_pointer)
{
    std::vector<std::unique_ptr<std::size_t>> vector_of_ptr;

    for (auto i = 0ul; i < 10; ++i)
        vector_of_ptr.emplace_back(std::make_unique<std::size_t>(i));

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Pointer>::Do(vector_of_ptr,
                                                                     oconv,
                                                                     PrintNS::Delimiter::separator(", "),
                                                                     PrintNS::Delimiter::opener("{ "),
                                                                     PrintNS::Delimiter::closer(" }"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }");
}


BOOST_AUTO_TEST_CASE(print_associative_containers)
{
    std::map<std::string, std::size_t> map{ { "triangle", 3 }, { "segment", 2 }, { "quadrangle", 4 } };

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Associative<>>::Do(map,
                                                                           oconv,
                                                                           PrintNS::Delimiter::separator(", "),
                                                                           PrintNS::Delimiter::opener("{ "),
                                                                           PrintNS::Delimiter::closer(" }"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ (quadrangle, 4), (segment, 2), (triangle, 3) }");
}


BOOST_AUTO_TEST_CASE(print_associative_keys)
{
    std::map<std::string, std::size_t> map{ { "triangle", 3 }, { "segment", 2 }, { "quadrangle", 4 } };

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(map,
                                                                 oconv,
                                                                 PrintNS::Delimiter::separator(", "),
                                                                 PrintNS::Delimiter::opener("{ "),
                                                                 PrintNS::Delimiter::closer(" }"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ quadrangle, segment, triangle }");
}

BOOST_AUTO_TEST_CASE(print_lua_map)
{
    std::map<int, std::size_t> map{ { 4, 3 }, { -2, 2 }, { 0, 4 } };

    std::ostringstream oconv;

    using policy = Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

    Utilities::PrintContainer<policy>::Do(map,
                                          oconv,
                                          PrintNS::Delimiter::separator(", "),
                                          PrintNS::Delimiter::opener("{ "),
                                          PrintNS::Delimiter::closer(" }"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ [-2] = 2, [0] = 4, [4] = 3 }");
}


BOOST_AUTO_TEST_CASE(print_lua_map_with_string)
{
    std::map<std::string, std::size_t> map{ { "triangle", 3 }, { "segment", 2 }, { "quadrangle", 4 } };

    std::ostringstream oconv;

    using policy = Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

    Utilities::PrintContainer<policy>::Do(map,
                                          oconv,
                                          PrintNS::Delimiter::separator(", "),
                                          PrintNS::Delimiter::opener("{ "),
                                          PrintNS::Delimiter::closer(" }"));

    BOOST_CHECK_EQUAL(oconv.str(), "{ ['quadrangle'] = 4, ['segment'] = 2, ['triangle'] = 3 }");
}


PRAGMA_DIAGNOSTIC(pop)
