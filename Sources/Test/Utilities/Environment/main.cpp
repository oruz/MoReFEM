/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE environment
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Environment/Environment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct Fixture
    {
        Fixture();

        Utilities::Environment& environment;

        static std::string unix_user;
    };


    Fixture::Fixture() : environment(Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__))
    { }


    std::string Fixture::unix_user = std::getenv("USER");
    // < I assume here USER is rather universal and should be defined in
    // all Unix environments.


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(get_environment_variable, Fixture)

BOOST_AUTO_TEST_CASE(unix_user_as_string)
{
    BOOST_CHECK(environment.DoExist(std::string("USER")));
    BOOST_CHECK(environment.GetEnvironmentVariable(std::string("USER"), __FILE__, __LINE__) == unix_user);
}

BOOST_AUTO_TEST_CASE(unix_user_as_char_array)
{
    BOOST_CHECK(environment.DoExist("USER"));
    BOOST_CHECK(environment.GetEnvironmentVariable("USER", __FILE__, __LINE__) == unix_user);
}

BOOST_AUTO_TEST_CASE(unexisting_variable)
{
    BOOST_CHECK(environment.DoExist("FHLKJHFFLKJ") == false);
    BOOST_CHECK_THROW(environment.GetEnvironmentVariable("FHLKJHFFLKJ", __FILE__, __LINE__), std::exception);
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_CASE(substitute_values, Fixture)
{
    std::string line = "/Volumes/Blah/${USER}";
    BOOST_CHECK(environment.SubstituteValues(line) == "/Volumes/Blah/" + unix_user);
}


BOOST_FIXTURE_TEST_CASE(internal_storage, Fixture)
{
    BOOST_CHECK(environment.DoExist("ASDFGHJK") == false);

    /* BOOST_CHECK_NO_THROW */ (
        environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "42 "), __FILE__, __LINE__));

    BOOST_CHECK(environment.DoExist("ASDFGHJK") == true);

    BOOST_CHECK(environment.GetEnvironmentVariable("ASDFGHJK", __FILE__, __LINE__) == "42 ");

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "47 "), __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("USER", "42"), __FILE__, __LINE__),
                      std::exception);
}


PRAGMA_DIAGNOSTIC(pop)
