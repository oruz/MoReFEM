-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


NumberingSubset1 = {

	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
	name = "Numbering subset",

	-- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a 
	-- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation. 
	-- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a 
	-- displacement. 
	-- Expected format: 'true' or 'false' (without the quote)
	do_move_mesh = false

} -- NumberingSubset1


-- File that gives for each vertex on the first mesh on the interface the index of the equivalent vertex in
-- the second mesh.
-- Expected format: "VALUE"
interpolation_file = "${MOREFEM_ROOT}/Data/Interpolation/Poromechanics/Identity_6x6.hhdata"


