/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/InterpolationFile.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace ReadInputDataNS
        {


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex : std::size_t
            {
                numbering_subset = 1
            };


            //! \copydoc doxygen_hide_input_data_tuple
            // clang-format off
            using InputDataTuple = std::tuple
            <
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::numbering_subset)>,

                InputDataNS::InterpolationFile
            >;
            // clang-format on


            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace ReadInputDataNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HPP_
