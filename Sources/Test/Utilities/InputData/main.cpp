/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/String/String.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/InitializeTestMoReFEMData.hpp"
#include "Test/Utilities/InputData/InputData.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(properly_read, ::MoReFEM::TestNS::FixtureNS::Environment)
{
    using namespace MoReFEM;

    using InputData = TestNS::ReadInputDataNS::InputData;

    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    TestNS::InitializeTestMoReFEMData<InputData> init(
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/InputData/"
                                     "demo.lua"));

    decltype(auto) morefem_data = init.GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();
    namespace ipl = Utilities::InputDataNS;

    {
        using NS = InputDataNS::NumberingSubset<1>;
        decltype(auto) value = ipl::Extract<NS::Name>::Value(input_data);

        BOOST_CHECK_EQUAL(value, "Numbering subset");
    } // namespace Utilities::InputDataNS;

    using interp_type = InputDataNS::InterpolationFile;

    {
        decltype(auto) value = ipl::Extract<interp_type>::Value(input_data);

        BOOST_CHECK_EQUAL(value, "${MOREFEM_ROOT}/Data/Interpolation/Poromechanics/Identity_6x6.hhdata");
    }

    {

        decltype(auto) path = ipl::Extract<interp_type>::Path(input_data);

        BOOST_CHECK(Utilities::String::EndsWith(path, "Data/Interpolation/Poromechanics/Identity_6x6.hhdata")
                    || path.find("${MOREFEM_ROOT}") == std::string::npos);
    }
}


PRAGMA_DIAGNOSTIC(pop)
