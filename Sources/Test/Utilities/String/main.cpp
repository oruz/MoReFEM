/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 27 Feb 2020 18:22:06 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <algorithm>

#define BOOST_TEST_MODULE string
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/String/String.hpp"

using namespace MoReFEM::Utilities::String;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strip)
{
    const std::string string = "  \t   ABCDEF \n\n  ";

    {
        auto copy = string;
        StripLeft(copy);
        BOOST_CHECK_EQUAL(copy, "ABCDEF \n\n  ");
    }

    {
        auto copy = string;
        StripRight(copy);
        BOOST_CHECK_EQUAL(copy, "  \t   ABCDEF");
    }

    {
        auto copy = string;
        Strip(copy);
        BOOST_CHECK_EQUAL(copy, "ABCDEF");
    }

    {
        auto copy = string;
        Strip(copy, " ");
        BOOST_CHECK_EQUAL(copy, "\t   ABCDEF \n\n");
    }
}


BOOST_AUTO_TEST_CASE(start_end)
{
    const std::string string = "Hello world!";

    BOOST_CHECK(StartsWith(string, "Hello") == true);
    BOOST_CHECK(StartsWith(string, "Bye") == false);

    BOOST_CHECK(EndsWith(string, "!") == true);
    BOOST_CHECK(EndsWith(string, "Bye") == false);
}


BOOST_AUTO_TEST_CASE(replace)
{
    std::string string = "Hello world!";


    BOOST_CHECK_EQUAL(Replace("Bonjour", "Hello", string), 0ul);

    BOOST_CHECK_EQUAL(Replace("Hello", "Bonjour", string), 1u);
    BOOST_CHECK_EQUAL(Replace("world", "le monde", string), 1u);

    BOOST_CHECK_EQUAL(string, "Bonjour le monde!");
}


BOOST_AUTO_TEST_CASE(repeat)
{
    const std::string string = "parrot";

    BOOST_CHECK_EQUAL(Repeat(string, 5u), "parrotparrotparrotparrotparrot");
    BOOST_CHECK_EQUAL(Repeat("parrot", 5u), "parrotparrotparrotparrotparrot");
}


BOOST_AUTO_TEST_CASE(convert_char_array)
{
    std::vector<char> char_hello{ 'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!' };

    const std::string hello = "Hello world!";

    BOOST_CHECK(ConvertCharArray(char_hello) == hello);

    char_hello.push_back('\0');

    BOOST_CHECK(ConvertCharArray(char_hello) == hello);
}


BOOST_AUTO_TEST_CASE(random_string)
{
    const std::string charset = "+-0123456789";


    const auto string = GenerateRandomString(10, charset);

    BOOST_CHECK_EQUAL(string.size(), 10ul);

    BOOST_CHECK(std::all_of(string.cbegin(),
                            string.cend(),
                            [&charset](char character)
                            {
                                return std::find(charset.cbegin(), charset.cend(), character) != charset.cend();
                            }));
}


BOOST_AUTO_TEST_CASE(split)
{
    const std::string string = "Triangle 3; 0; 5; P1; 0.16 5.14 3.2";

    BOOST_CHECK(Split(string, ";")
                == (std::vector<std::string_view>{ "Triangle 3", " 0", " 5", " P1", " 0.16 5.14 3.2" }));

    BOOST_CHECK(Split(string, "!") == (std::vector<std::string_view>{ string }));
}


PRAGMA_DIAGNOSTIC(pop)
