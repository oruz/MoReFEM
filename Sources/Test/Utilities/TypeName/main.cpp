/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Mar 2018 09:12:06 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <iostream>

#define BOOST_TEST_MODULE typename
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Type/PrintTypeName.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(typename_test)
{
    BOOST_REQUIRE(GetTypeName<int>() == "int");
    BOOST_REQUIRE(GetTypeName<double>() == "double");

#ifdef __clang__
    constexpr auto expected = "const int &";
#elif defined(__GNUC__)
    constexpr auto expected = "const int&";
#endif

    BOOST_REQUIRE(GetTypeName<const int&>() == expected);

    // The tests below are not complete: it's highly possible a compiler will yield a correct result not present here.
    // Feel free to extend the test; currently it covers output by AppleClang, Clang and gcc.
    std::cout << "The result for std::vector<double> is |" << GetTypeName<std::vector<double>>() << '|' << std::endl;

    BOOST_REQUIRE(( // double parenthesis on purpose!
        GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double> >"
        || GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double>>"
        || GetTypeName<std::vector<double>>() == "std::vector<double, std::allocator<double> >"
        || GetTypeName<std::vector<double>>() == "std::vector<double>"
        || GetTypeName<std::vector<double>>() == "std::__1::vector<double>"));

    std::cout << "The result for std::string is |" << GetTypeName<std::string>() << '|' << std::endl;

    BOOST_REQUIRE(( // double parenthesis on purpose!
        GetTypeName<std::string>() == "std::__1::basic_string<char>"
        || GetTypeName<std::string>()
               == "std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >"
        || GetTypeName<std::string>()
               == "std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>>"
        || GetTypeName<std::string>() == "std::__cxx11::basic_string<char>"
        || GetTypeName<std::string>() == "std::string"));
}

PRAGMA_DIAGNOSTIC(pop)
