/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Mar 2018 18:46:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE tuple_has_type
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Tuple.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(simple_is_same)
{
    using tuple = std::tuple<int, float, double, float>;
    BOOST_REQUIRE((Utilities::Tuple::HasType<int, std::is_same, tuple>()));
    BOOST_REQUIRE((!Utilities::Tuple::HasType<char, std::is_same, tuple>()));
}


BOOST_AUTO_TEST_CASE(is_same__with_decay)
{
    using tuple = std::tuple<int&&, float[3], double&>;
    using decayed_tuple = Utilities::Tuple::Decay<tuple>::type;

    BOOST_REQUIRE((Utilities::Tuple::HasType<int, std::is_same, decayed_tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<float*, std::is_same, decayed_tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<double, std::is_same, decayed_tuple>()));
}


BOOST_AUTO_TEST_CASE(is_base_of)
{
    struct Base
    { };

    struct Derived : public Base
    { };

    using tuple = std::tuple<int, Derived, double>;

    BOOST_REQUIRE((!Utilities::Tuple::HasType<Base, std::is_same, tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<Base, std::is_base_of, tuple>()));
}


PRAGMA_DIAGNOSTIC(pop)
