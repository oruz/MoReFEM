/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Mar 2018 18:46:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>

#define BOOST_TEST_MODULE utilities_directory

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct fixture : public TestNS::FixtureNS::Mpi, public TestNS::FixtureNS::Environment
    { };


    enum class stdin_case
    {
        yes,
        no
    };

    template<stdin_case CaseT>
    void SetStdin(std::ifstream& in, const std::string& directory_test);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(create, fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    std::string path = test_dir + "/directory_test/" + Utilities::String::GenerateRandomString(10);

    std::unique_ptr<FilesystemNS::Directory> create_ptr;

    BOOST_CHECK_NO_THROW(create_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, path, FilesystemNS::behaviour::create, __FILE__, __LINE__));

    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(*create_ptr));

    BOOST_CHECK_THROW(
        FilesystemNS::Directory create_once_again(mpi, path, FilesystemNS::behaviour::create, __FILE__, __LINE__),
        Exception);
}


BOOST_FIXTURE_TEST_CASE(overwrite, fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    std::unique_ptr<FilesystemNS::Directory> overwrite_ptr;

    BOOST_CHECK_NO_THROW(
        overwrite_ptr = std::make_unique<FilesystemNS::Directory>(
            mpi, test_dir + "/directory_test", FilesystemNS::behaviour::overwrite, __FILE__, __LINE__));

    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(*overwrite_ptr));

    BOOST_CHECK_NO_THROW(FilesystemNS::Directory overwrite_once_again(
        mpi, test_dir + "/directory_test", FilesystemNS::behaviour::overwrite, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(read_case, fixture) // 'read' can't be used here hence the _case.
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    const std::string directory_test = test_dir + "/directory_test";

    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(directory_test));

    std::unique_ptr<FilesystemNS::Directory> read_ptr{ nullptr };
    BOOST_CHECK_NO_THROW(read_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, directory_test, FilesystemNS::behaviour::read, __FILE__, __LINE__));

    BOOST_CHECK(read_ptr != nullptr);
    BOOST_CHECK_EQUAL(read_ptr->GetPath(), directory_test + "/Rank_" + std::to_string(mpi.GetRank<int>()) + "/");


    BOOST_CHECK_THROW(FilesystemNS::Directory read_inexistant(
                          mpi, directory_test + "/qwert", FilesystemNS::behaviour::read, __FILE__, __LINE__),
                      Exception);
}


BOOST_FIXTURE_TEST_CASE(quit, fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    const std::string directory_test = test_dir + "/directory_test";

    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(directory_test));

    BOOST_CHECK_THROW(
        FilesystemNS::Directory read(mpi, directory_test, FilesystemNS::behaviour::quit, __FILE__, __LINE__),
        ExceptionNS::GracefulExit);
}


BOOST_FIXTURE_TEST_CASE(ask_yes, fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    const std::string directory_test = test_dir + "/directory_test";

    std::ifstream in;
    SetStdin<stdin_case::yes>(in, directory_test);

    BOOST_CHECK_NO_THROW(
        FilesystemNS::Directory ask_yes(mpi, directory_test, FilesystemNS::behaviour::ask, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(ask_no, fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);
    decltype(auto) mpi = GetMpi();

    const std::string directory_test = test_dir + "/directory_test";

    std::ifstream in;

    SetStdin<stdin_case::no>(in, directory_test);

    BOOST_CHECK_THROW(
        FilesystemNS::Directory ask_no(mpi, directory_test, FilesystemNS::behaviour::ask, __FILE__, __LINE__),
        ExceptionNS::GracefulExit);
}


// When the directory already exists ONLY for one of the non root rank...
BOOST_FIXTURE_TEST_CASE(ask_on_rank, fixture)
{
    decltype(auto) mpi = GetMpi();

    if (mpi.Nprocessor<int>() > 1)
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

        const std::string directory_test = test_dir + "/directory_test/asymmetric/";

        if (mpi.IsRootProcessor())
        {
            std::string rank_2_dir = directory_test + "Rank_2";

            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(rank_2_dir))
                Advanced::FilesystemNS::DirectoryNS::Create(rank_2_dir, __FILE__, __LINE__);
        }

        mpi.Barrier();

        std::ifstream in;
        SetStdin<stdin_case::yes>(in, directory_test);

        BOOST_CHECK_NO_THROW(
            FilesystemNS::Directory ask_yes(mpi, directory_test, FilesystemNS::behaviour::ask, __FILE__, __LINE__));
    }
}


BOOST_FIXTURE_TEST_CASE(subdirectory, fixture)
{
    decltype(auto) mpi = GetMpi();


    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

    const std::string directory_test = test_dir + "/directory_test/";

    std::unique_ptr<FilesystemNS::Directory> directory_ptr{ nullptr };
    std::unique_ptr<FilesystemNS::Directory> subdirectory_ptr{ nullptr };

    BOOST_CHECK_NO_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, directory_test, FilesystemNS::behaviour::overwrite, __FILE__, __LINE__));
    BOOST_CHECK(directory_ptr != nullptr);
    BOOST_CHECK_NO_THROW(subdirectory_ptr = std::make_unique<FilesystemNS::Directory>(
                             FilesystemNS::Directory(*directory_ptr, "Subdirectory", __FILE__, __LINE__)));

    std::ostringstream oconv;
    oconv << directory_test << "Rank_" << mpi.GetRank<int>() << "/Subdirectory";

    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(oconv.str()));
}


BOOST_FIXTURE_TEST_CASE(subdirectory_not_existing, fixture)
{
    decltype(auto) mpi = GetMpi();


    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

    const std::string directory_test = test_dir + "/directory_test/";
    std::unique_ptr<FilesystemNS::Directory> directory_ptr{ nullptr };

    BOOST_CHECK_NO_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, directory_test, FilesystemNS::behaviour::read, __FILE__, __LINE__));

    // Throw because it doesn't exist and behaviour is read.
    BOOST_CHECK_THROW(FilesystemNS::Directory subdirectory(*directory_ptr, "NotExisting", __FILE__, __LINE__),
                      Exception);
}


BOOST_FIXTURE_TEST_CASE(many_subdirectories, fixture)
{
    decltype(auto) mpi = GetMpi();

    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
    decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

    const std::string directory_test = test_dir + "/directory_test/";

    std::unique_ptr<FilesystemNS::Directory> directory_ptr{ nullptr };
    std::unique_ptr<FilesystemNS::Directory> subdirectory_ptr{ nullptr };

    BOOST_CHECK_NO_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, directory_test, FilesystemNS::behaviour::overwrite, __FILE__, __LINE__));

    BOOST_CHECK_NO_THROW(subdirectory_ptr = std::make_unique<FilesystemNS::Directory>(
                             *directory_ptr,
                             std::vector<std::string>{ "Subfolder", "Subsubfolder", "Subsubsubfolder" },
                             __FILE__,
                             __LINE__));

    BOOST_CHECK(subdirectory_ptr != nullptr);
    BOOST_CHECK_EQUAL(subdirectory_ptr->GetPath(),
                      directory_test + "/Rank_" + std::to_string(mpi.GetRank<int>())
                          + "/Subfolder/Subsubfolder/Subsubsubfolder/");
    BOOST_CHECK(Advanced::FilesystemNS::DirectoryNS::DoExist(*subdirectory_ptr));
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    template<stdin_case CaseT>
    void SetStdin(std::ifstream& in, const std::string& directory_test)

    {
        std::string stdin_file = directory_test + "/input_" + (CaseT == stdin_case::yes ? "yes" : "no") + ".txt";

        std::ofstream out;
        FilesystemNS::File::Create(out, stdin_file, __FILE__, __LINE__);
        out << "w w e s f 4 515 s j" << std::endl;

        if (CaseT == stdin_case::yes)
            out << 'y';
        else
            out << 'n';

        out.close();

        in.close();
        FilesystemNS::File::Read(in, stdin_file, __FILE__, __LINE__);

        // See //https://stackoverflow.com/questions/10150468/how-to-redirect-cin-and-cout-to-files.
        std::cin.rdbuf(in.rdbuf()); // redirect std::cin to in.txt!
    }


} // namespace
