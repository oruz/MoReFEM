/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Mar 2018 17:18:25 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <sstream>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/InputData/LuaFunction.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/SpatialLuaFunction.hpp"

#define BOOST_TEST_MODULE lua_option_file
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Environment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    /*!
     * \brief Just an helper function to make the tests more concise.
     *
     * The function returns by value what was read, and constraint is bypassed in the call by default.
     */
    template<class T>
    T ReadLuaOptionFile(LuaOptionFile& lua_option_file,
                        const std::string& key,
                        const char* invoking_file,
                        int invoking_line,
                        const std::string& constraint = "")
    {
        T ret;
        lua_option_file.Read(key, constraint, ret, invoking_file, invoking_line);
        return ret;
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(parameters_properly_read, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));

    auto& lua_option_file = *(ptr.get());

    BOOST_CHECK(
        NumericNS::AreEqual(ReadLuaOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__), 2.44));

    BOOST_CHECK(ReadLuaOptionFile<std::string>(lua_option_file, "section1.string_value", __FILE__, __LINE__)
                == "string");
    BOOST_CHECK(NumericNS::AreEqual(
        ReadLuaOptionFile<double>(lua_option_file, "section1.double_value", __FILE__, __LINE__), 5.215));

    BOOST_CHECK(ReadLuaOptionFile<std::size_t>(lua_option_file, "section1.int_value", __FILE__, __LINE__) == 10u);
    BOOST_CHECK_THROW(ReadLuaOptionFile<std::size_t>(lua_option_file, "section1.double_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK(
        ReadLuaOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__).size()
        == 3ul);
    BOOST_CHECK(
        ReadLuaOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[0]
        == "foo");
    BOOST_CHECK(
        ReadLuaOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[1]
        == "bar");
    BOOST_CHECK(
        ReadLuaOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[2]
        == "baz");
    BOOST_CHECK_THROW(ReadLuaOptionFile<int>(lua_option_file, "section1.vector_value", __FILE__, __LINE__),
                      std::exception);
    BOOST_CHECK_THROW(ReadLuaOptionFile<std::vector<int>>(lua_option_file, "section1.int_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK(ReadLuaOptionFile<std::string>(lua_option_file, "section1.string_value", __FILE__, __LINE__)
                == "string");

    BOOST_CHECK((ReadLuaOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__).size()
                 == 3ul));
    BOOST_CHECK(
        (ReadLuaOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[3] == 5));
    BOOST_CHECK(
        (ReadLuaOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[4] == 7));
    BOOST_CHECK(
        (ReadLuaOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[5] == 8));

    BOOST_CHECK_THROW(ReadLuaOptionFile<std::string>(lua_option_file, "unknown_key", __FILE__, __LINE__),
                      std::exception);
    BOOST_CHECK_THROW(ReadLuaOptionFile<double>(lua_option_file, "section1.string_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK_THROW(ReadLuaOptionFile<double>(lua_option_file, "section1.invalid_value", __FILE__, __LINE__),
                      std::exception);

    // When I have time to investigate how to do so in Boost
    // CHECK_ABORT(ReadLuaOptionFile<double>(lua_option_file, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(constraints, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)));
    auto& lua_option_file = *(ptr.get());

    BOOST_CHECK_THROW(NumericNS::AreEqual(
                          ReadLuaOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__, "v > 5."), 2.44),
                      std::exception);
    /* BOOST_CHECK_NO_THROW */ (NumericNS::AreEqual(
        ReadLuaOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__, "v < 3."), 2.44));

    /* BOOST_CHECK_NO_THROW */ (ReadLuaOptionFile<std::vector<std::string>>(
        lua_option_file, "section1.vector_value", __FILE__, __LINE__, "value_in(v, { 'foo', 'bar', 'baz' })"));

    BOOST_CHECK_THROW(
        ReadLuaOptionFile<std::vector<std::string>>(
            lua_option_file, "section1.vector_value", __FILE__, __LINE__, "value_in(v, { 'bar', 'baz' })"),
        std::exception);
}


BOOST_FIXTURE_TEST_CASE(lua_functions, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__));
    auto& lua_option_file = *(ptr.get());

    decltype(auto) one_arg_fct = ReadLuaOptionFile<Utilities::InputDataNS::LuaFunction<double(double)>>(
        lua_option_file, "section2.one_arg_function", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(one_arg_fct(3.), -3.));

    decltype(auto) several_arg_function =
        ReadLuaOptionFile<Utilities::InputDataNS::LuaFunction<double(double, double, double)>>(
            lua_option_file, "section2.several_arg_function", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(several_arg_function(3., 4., 5.), 2.));


    decltype(auto) function_with_if =
        ReadLuaOptionFile<Utilities::InputDataNS::LuaFunction<double(double, double, double)>>(
            lua_option_file, "section2.function_with_if", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(function_with_if(5., 0., 0.), 1.));
    BOOST_CHECK(NumericNS::AreEqual(function_with_if(15., 0., 0.), 0.));
}


BOOST_FIXTURE_TEST_CASE(redundant, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/redundancy.lua");

    std::unique_ptr<LuaOptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)), std::exception);
}


BOOST_FIXTURE_TEST_CASE(redundant_in_section, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/redundancy_in_section.lua");

    std::unique_ptr<LuaOptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__)), std::exception);
}


BOOST_FIXTURE_TEST_CASE(invalid_lua, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/demo_lua_option_file.lua");

    std::unique_ptr<LuaOptionFile> ptr;
    ptr.reset(new LuaOptionFile(input_file, __FILE__, __LINE__));
    auto& lua_option_file = *(ptr.get());

    BOOST_REQUIRE_THROW(ReadLuaOptionFile<Utilities::InputDataNS::LuaFunction<double(double)>>(
                            lua_option_file, "section3.invalid_function", __FILE__, __LINE__),
                        std::exception);
}


BOOST_FIXTURE_TEST_CASE(map_in_vector, TestNS::FixtureNS::Environment)
{
    // Introduced after #1468, in which two braces on the same line wreak havoc...
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/map_in_vector.lua");

    LuaOptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(),
                      "[ValueOutsideBrace, "
                      "TransientSource4.nature, TransientSource4.value, "
                      "TransientSource5.nature, TransientSource5.value, "
                      "TransientSource5.whatever.sublevel, "
                      "TransientSource5.whatever.sublevel2, "
                      "InitialCondition1.nature, InitialCondition1.value]");
}


BOOST_FIXTURE_TEST_CASE(no_section, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/no_section.lua");

    LuaOptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[a, b, c, d, e, f]");
}


BOOST_FIXTURE_TEST_CASE(entry_with_underscore, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Utilities/LuaOptionFile/entry_with_underscore.lua");

    LuaOptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[transient.init_time, transient.timeStep, transient.timeMax]");
}

PRAGMA_DIAGNOSTIC(pop)
