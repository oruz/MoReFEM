/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Geometry/NcoordInDomain/Model.hpp"


namespace MoReFEM
{


    namespace TestNcoordInDomainNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("TestNcoordInDomain");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


    } // namespace TestNcoordInDomainNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_MODEL_HXX_
