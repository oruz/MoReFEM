/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Test/Geometry/NcoordInDomain/Model.hpp"


namespace MoReFEM
{


    namespace TestNcoordInDomainNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            decltype(auto) god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) mesh = god_of_dof.GetMesh();

            decltype(auto) mpi = parent::GetMpi();

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            const auto Ncoord_in_domain =
                NcoordsInDomain<MpiScale::program_wise>(mpi, domain_manager.GetDomain(1, __FILE__, __LINE__), mesh);
            if (Ncoord_in_domain != 4u)
            {
                std::ostringstream oconv;
                oconv << "4 coords were expected in domain 3 but " << Ncoord_in_domain << " were found.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestNcoordInDomainNS


} // namespace MoReFEM
