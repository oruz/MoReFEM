/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNcoordInDomainNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            full_mesh = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            volume_potential_1_potential_2 = 1,
            volume_displacement_potential_1 = 5,
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            potential_1 = 1,
            displacement = 5
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            potential_1 = 1,
            displacement_potential_1 = 5,
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : std::size_t
        {
            source = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNcoordInDomainNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_NCOORD_IN_DOMAIN_x_INPUT_DATA_HPP_
