/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/

#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <sstream>

#define BOOST_TEST_MODULE coords_matching
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/String/String.hpp"

#include "Core/InputData/InputData.hpp"

#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "Test/Geometry/CoordsMatching/InputData.hpp"
#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/Fixture/Mpi.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    enum class print_meshes_line
    {
        no,
        yes,
        botched
    };

    /*!
     * \brief Generate on the fly in the test directory a Lua file and a simple interpolation file from given vectors.
     */
    class TestData
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] moniker A moniker for the test which will be used in creating a dedicated subdirectory (so that a test doesn't overwrite the directory
         * of the prevous one).
         */
        TestData(std::string_view moniker,
                 std::vector<std::size_t>&& source_values,
                 std::vector<std::size_t>&& target_values,
                 print_meshes_line do_print_meshes_line = print_meshes_line::yes);

        const std::string& GetLuaFile() const noexcept;

      private:
        std::string lua_file_;

        std::vector<CoordsNS::index_from_mesh_file> source_values_;

        std::vector<CoordsNS::index_from_mesh_file> target_values_;
    };


    struct Fixture : public TestNS::FixtureNS::Mpi, public TestNS::FixtureNS::Environment
    { };


    using IntToStrongType =
        Utilities::StaticCast<std::vector<std::size_t>, std::vector<CoordsNS::index_from_mesh_file>>;

    constexpr auto source_mesh_id = 1u;
    constexpr auto target_mesh_id = 42u;

} // namespace


BOOST_FIXTURE_TEST_SUITE(coords_matching, Fixture)


BOOST_AUTO_TEST_CASE(ok_case)
{
    TestData generated_data("ok_case", { 1, 2, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    MeshNS::InterpolationNS::CoordsMatching coords_matching(input_data);

    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 11 }),
                      CoordsNS::index_from_mesh_file{ 1 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 12 }),
                      CoordsNS::index_from_mesh_file{ 2 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 13 }),
                      CoordsNS::index_from_mesh_file{ 3 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 14 }),
                      CoordsNS::index_from_mesh_file{ 4 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 15 }),
                      CoordsNS::index_from_mesh_file{ 5 });

    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 1 }),
                      CoordsNS::index_from_mesh_file{ 11 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 2 }),
                      CoordsNS::index_from_mesh_file{ 12 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 3 }),
                      CoordsNS::index_from_mesh_file{ 13 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 4 }),
                      CoordsNS::index_from_mesh_file{ 14 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 5 }),
                      CoordsNS::index_from_mesh_file{ 15 });

    BOOST_CHECK_EQUAL(coords_matching.GetSourceMeshId(), source_mesh_id);
    BOOST_CHECK_EQUAL(coords_matching.GetTargetMeshId(), target_mesh_id);
}

BOOST_AUTO_TEST_CASE(repeated_target_value)
{
    TestData generated_data("repeated_target_value", { 1, 2, 3, 4, 5 }, { 11, 11, 13, 14, 15 });

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    std::unique_ptr<MeshNS::InterpolationNS::CoordsMatching> coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = std::make_unique<MeshNS::InterpolationNS::CoordsMatching>(input_data),
                      std::exception);
}

BOOST_AUTO_TEST_CASE(repeated_source_value)
{
    TestData generated_data("repeated_source_value", { 1, 1, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    std::unique_ptr<MeshNS::InterpolationNS::CoordsMatching> coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = std::make_unique<MeshNS::InterpolationNS::CoordsMatching>(input_data),
                      std::exception);
}


BOOST_AUTO_TEST_CASE(imbalanced_data)
{
    TestData generated_data("imbalanced_data", { 1, 2, 3 }, { 11, 12 });

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    std::unique_ptr<MeshNS::InterpolationNS::CoordsMatching> coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = std::make_unique<MeshNS::InterpolationNS::CoordsMatching>(input_data),
                      std::exception);
}


BOOST_AUTO_TEST_CASE(missing_meshes_line)
{
    TestData generated_data("missing_meshes_line", { 1, 2, 3 }, { 11, 12, 13 }, print_meshes_line::no);

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    std::unique_ptr<MeshNS::InterpolationNS::CoordsMatching> coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = std::make_unique<MeshNS::InterpolationNS::CoordsMatching>(input_data),
                      std::exception);
}


BOOST_AUTO_TEST_CASE(botched_meshes_line)
{
    TestData generated_data("botched_meshes_line", { 1, 2, 3 }, { 11, 12, 13 }, print_meshes_line::botched);

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    std::unique_ptr<MeshNS::InterpolationNS::CoordsMatching> coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = std::make_unique<MeshNS::InterpolationNS::CoordsMatching>(input_data),
                      std::exception);
}


BOOST_AUTO_TEST_CASE(array)
{
    TestData generated_data("array", { 1, 2, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    TestNS::CoordsMatchingNS::InputData input_data(generated_data.GetLuaFile(), GetMpi());

    MeshNS::InterpolationNS::CoordsMatching coords_matching(input_data);

    {
        auto array = IntToStrongType::Perform({ 1, 3 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 11, 13 }));
    }

    {
        auto array = IntToStrongType::Perform({ 3, 1 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 11, 13 }));
    }

    {
        auto array = IntToStrongType::Perform({ 2, 3, 5 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 12, 13, 15 }));
    }

    {
        auto array = IntToStrongType::Perform({ 11, 13 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 1, 3 }));
    }

    {
        auto array = IntToStrongType::Perform({ 13, 11 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 1, 3 }));
    }

    {
        auto array = IntToStrongType::Perform({ 12, 13, 15 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 2, 3, 5 }));
    }

    BOOST_CHECK_EQUAL(coords_matching.GetSourceMeshId(), source_mesh_id);
    BOOST_CHECK_EQUAL(coords_matching.GetTargetMeshId(), target_mesh_id);
}


BOOST_AUTO_TEST_SUITE_END()


namespace // anonymous
{


    TestData::TestData(std::string_view moniker,
                       std::vector<std::size_t>&& source_values,
                       std::vector<std::size_t>&& target_values,
                       print_meshes_line do_print_meshes_line)
    : source_values_(IntToStrongType::Perform(source_values)), target_values_(IntToStrongType::Perform(target_values))
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);

        FilesystemNS::Directory test_directory(
            environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__),
            FilesystemNS::behaviour::ignore,
            __FILE__,
            __LINE__);

        test_directory.SetBehaviour(FilesystemNS::behaviour::overwrite);
        test_directory.AddSubdirectory(moniker, __FILE__, __LINE__);

        std::ostringstream oconv;
        lua_file_ = test_directory.AddFile("demo_.lua");

        std::ofstream out;

        {
            FilesystemNS::File::Create(out, lua_file_, __FILE__, __LINE__);

            out << "interpolation_file = '" << test_directory << "/test.hhdata'";
            out.close();
        }

        {
            FilesystemNS::File::Create(out, test_directory.AddFile("test.hhdata"), __FILE__, __LINE__);

            switch (do_print_meshes_line)
            {
            case print_meshes_line::yes:
                out << "Meshes: " << source_mesh_id << ' ' << target_mesh_id << std::endl;
                break;
            case print_meshes_line::no:
                break;
            case print_meshes_line::botched:
                out << "Meshes: " << source_mesh_id << ' ' << target_mesh_id << " 5" << std::endl;
                break;
            }

            const auto source_size = source_values.size();
            const auto target_size = target_values.size();
            const auto min_size = std::min(source_size, target_size);

            for (auto i = 0ul; i < min_size; ++i)
            {
                out << source_values[i] << " " << target_values[i] << std::endl;
            }

            if (source_size > min_size)
            {
                for (auto i = min_size; i < source_size; ++i)
                    out << source_values[i] << "  " << std::endl;
            } else if (target_size > min_size)
            {
                for (auto i = min_size; i < target_size; ++i)
                    out << "  " << target_values[i] << std::endl;
            }

            out.close();
        }
    }


    const std::string& TestData::GetLuaFile() const noexcept
    {
        return lua_file_;
    }

} // namespace

PRAGMA_DIAGNOSTIC(pop)
