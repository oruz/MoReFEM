/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/Geometry/InterpolationFile.hpp"
#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::TestNS::CoordsMatchingNS
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using InputDataTuple = std::tuple
    <
        InputDataNS::InterpolationFile
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using InputData = InputData<InputDataTuple>;


} // namespace MoReFEM::TestNS::CoordsMatchingNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_
