/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "Model/Model.hpp"

#include "Test/Geometry/Coloring/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace ColoringNS
        {


            //! \copydoc doxygen_hide_model_4_test
            class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>
            {

              private:
                //! \copydoc doxygen_hide_alias_self
                using self = Model;

                //! Convenient alias.
                using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>;

              public:
                //! Return the name of the model.
                static const std::string& ClassName();

                //! Friendship granted to the base class so this one can manipulates private methods.
                friend parent;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_morefem_data_param
                 */
                Model(const morefem_data_type& morefem_data);

                //! Destructor.
                ~Model() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Model(const Model& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Model(Model&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Model& operator=(const Model& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Model& operator=(Model&& rhs) = delete;

                ///@}


                /// \name Crtp-required methods.
                ///@{


                /*!
                 * \brief Initialise the problem.
                 *
                 * This initialisation includes the resolution of the static problem.
                 */
                void SupplInitialize();


                //! Manage time iteration.
                void Forward();

                /*!
                 * \brief Additional operations to finalize a dynamic step.
                 *
                 * Base class already update the time for next time iterations.
                 */
                void SupplFinalizeStep();


                /*!
                 * \brief Initialise a dynamic step.
                 *
                 */
                void SupplFinalize();


              private:
                //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
                bool SupplHasFinishedConditions() const;


                /*!
                 * \brief Part of InitializedStep() specific to Elastic model.
                 *
                 * As there are none, the body of this method is empty.
                 */
                void SupplInitializeStep();


                ///@}
            };


        } // namespace ColoringNS


    } // namespace TestNS


} // namespace MoReFEM


#include "Test/Geometry/Coloring/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_MODEL_HPP_
