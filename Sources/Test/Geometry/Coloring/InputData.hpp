/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace ColoringNS
        {


            //! Default value for some input parameter that are required by a MoReFEM model but are actually unused for
            //! current test.
            enum class Unused
            {
                value = 1
            };


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! Enum used to index numbering subsets, domains and finite element spaces.
            enum class DimensionIndex
            {
                Dim1 = 1,
                Dim2 = 2
            };


            //! \copydoc doxygen_hide_input_data_tuple
            // clang-format off
            using InputDataTuple = std::tuple
            <
                InputDataNS::TimeManager,

                InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim2)>,

                InputDataNS::Unknown<EnumUnderlyingType(Unused::value)>,

                InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>,

                InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>,

                InputDataNS::Petsc<EnumUnderlyingType(Unused::value)>,

                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputDataNS::Result
            >;
            // clang-format on


            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace ColoringNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_DATA_HPP_
