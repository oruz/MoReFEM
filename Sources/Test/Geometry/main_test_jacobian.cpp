/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Utilities/TimeKeep/TimeKeep.hpp"


#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/Mesh/ComputeColoring.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"


int main(int argc, char** argv)
{
    using namespace MoReFEM;

    Wrappers::Mpi::InitEnvironment(argc, argv);

    Wrappers::Mpi mpi_world(0, Wrappers::MpiNS::Comm::World); // must be called before Petsc RAII

    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(invoking_file, invoking_line);

    manager.Create(1,
                   Utilities::EnvironmentNS ::SubstituteValues(
                       "${HOME}/Codes/MoReFEM/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh"),
                   2,
                   ::MoReFEM::MeshNS::Format::Medit,
                   1.,
                   Mesh::BuildEdge::no,
                   Mesh::BuildFace::no,
                   Mesh::BuildVolume::no);

    const auto& mesh = manager.GetMesh(1);


    decltype(auto) geom_elt = mesh.GetGeometricEltFromIndex(1u);

    std::cout << geom_elt.GetName() << std::endl;

    LocalCoords local_coords({ 0.15 });

    Advanced::GeomEltNS::ComputeJacobian compute_jacobian_helper(mesh.GetDimension());

    std::cout << compute_jacobian_helper.Compute(geom_elt, local_coords) << std::endl;


    for (const auto& coords_ptr : geom_elt.GetCoordsList())
    {
        std::cout << '\t' << *coords_ptr << std::endl;
    }

    SpatialPoint barycenter;
    Advanced::GeomEltNS::ComputeBarycenter(geom_elt, barycenter);

    std::cout << "=> Barycenter = " << barycenter << std::endl;

    SpatialPoint global;
    Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, global);

    std::cout << "Coords in mesh = " << global << std::endl;

    //    std::cout << "Reverse " << Global2local(geom_elt, global) << std::endl;

    return EXIT_SUCCESS;
}
