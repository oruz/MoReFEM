/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstdlib>

#define BOOST_TEST_MODULE load_mesh_prepartitioned_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/LoadPrepartitionedMesh/InputData.hpp"
#include "Test/Geometry/LoadPrepartitionedMesh/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::LoadPrepartitionedMeshNS::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__


BOOST_FIXTURE_TEST_CASE(geometric_elt_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckGeometricEltList();
}


BOOST_FIXTURE_TEST_CASE(mesh_label_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckLabelList();
}


BOOST_FIXTURE_TEST_CASE(coords_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckCoordsList();
}


BOOST_FIXTURE_TEST_CASE(interface_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckInterfaceList();
}


PRAGMA_DIAGNOSTIC(pop)
