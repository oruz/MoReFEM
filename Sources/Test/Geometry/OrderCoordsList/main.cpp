/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/

#include <vector>

#define BOOST_TEST_MODULE order_coords_list
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;


namespace MoReFEM::TestNS
{


    struct GenerateCoord
    {

        static Coords::shared_ptr NewCoords(CoordsNS::processor_wise_position index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetProcessorWisePosition(index);
            return ret;
        }

        static Coords::shared_ptr NewCoords(CoordsNS::program_wise_position index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetProgramWisePosition(index);
            return ret;
        }

        static Coords::shared_ptr NewCoords(CoordsNS::index_from_mesh_file index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetIndexFromMeshFile(index);
            return ret;
        }
    };

} // namespace MoReFEM::TestNS


BOOST_AUTO_TEST_CASE(two_coords)
{
    auto a = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 3ul });
    auto b = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 5ul });
    auto c = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 7ul });

    {
        Coords::vector_shared_ptr list{ a, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b }));
    }

    {
        Coords::vector_shared_ptr list{ b, a };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b }));
    }

    {
        Coords::vector_shared_ptr list{ a, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c }));
    }

    {
        Coords::vector_shared_ptr list{ c, a };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c }));
    }

    {
        Coords::vector_shared_ptr list{ b, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ b, c }));
    }

    {
        Coords::vector_shared_ptr list{ c, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ b, c }));
    }
}


BOOST_AUTO_TEST_CASE(more_coords)
{
    auto a = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 3ul });
    auto b = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 5ul });
    auto c = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 7ul });
    auto d = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 9ul });

    {
        Coords::vector_shared_ptr list{ a, b, c, d };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b, c, d }));
    }


    {
        Coords::vector_shared_ptr list{ a, d, c, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b, c, d }));
    }

    {
        Coords::vector_shared_ptr list{ a, d, b, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c, b, d }));
    }
}


BOOST_AUTO_TEST_CASE(two_coords_index_from_mesh_file)
{
    // Same test as two_coords but upon a vector of Coords indexes.
    auto a = CoordsNS::index_from_mesh_file{ 3ul };
    auto b = CoordsNS::index_from_mesh_file{ 5ul };
    auto c = CoordsNS::index_from_mesh_file{ 7ul };

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ a, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, b }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ b, a };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, b }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ a, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ c, a };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ b, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ b, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ c, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::index_from_mesh_file>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ b, c }));
    }
}

BOOST_AUTO_TEST_CASE(more_coords_program_wise_index)
{
    auto a = CoordsNS::program_wise_position{ 3ul };
    auto b = CoordsNS::program_wise_position{ 5ul };
    auto c = CoordsNS::program_wise_position{ 7ul };
    auto d = CoordsNS::program_wise_position{ 9ul };

    {
        std::vector<CoordsNS::program_wise_position> list{ a, b, c, d };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::program_wise_position>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, b, c, d }));
    }

    {
        std::vector<CoordsNS::program_wise_position> list{ a, d, c, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::program_wise_position>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, b, c, d }));
    }

    {
        std::vector<CoordsNS::program_wise_position> list{ a, d, b, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<CoordsNS::program_wise_position>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, c, b, d }));
    }
}

PRAGMA_DIAGNOSTIC(pop)
