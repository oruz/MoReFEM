/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstdlib>

#define BOOST_TEST_MODULE domain_list_in_coords
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/DomainListInCoords/InputData.hpp"
#include "Test/Geometry/DomainListInCoords/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::DomainListInCoordsNS::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(normal_domains, fixture_type)
{
    GetModel().CheckNormalDomain();
}


BOOST_FIXTURE_TEST_CASE(lightweight_domains, fixture_type)
{
    GetModel().CheckLightweightDomain();
}


PRAGMA_DIAGNOSTIC(pop)
