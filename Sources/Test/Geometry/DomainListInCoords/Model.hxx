/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Geometry/DomainListInCoords/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {


            inline const std::string& Model::ClassName()
            {
                static std::string ret("Test DomainListInCoords");
                return ret;
            }


            inline bool Model::SupplHasFinishedConditions() const
            {
                return false; // ie no additional condition
            }


            inline void Model::SupplInitializeStep()
            { }


        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_
