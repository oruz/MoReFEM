/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                volume = 1,
                exterior_surface = 2,
                interior_surface = 3,
                ring = 4,
                exterior_and_ring = 5, // through lightweight domain list
                interior_and_ring = 6  // through lightweight domain list
            };


            //! Default value for some input parameter that are required by a MoReFEM model but are actually unused for
            //! current test.
            constexpr auto whatever = 0;


            //! \copydoc doxygen_hide_input_data_tuple
            // clang-format off
            using InputDataTuple = std::tuple
            <
                InputDataNS::TimeManager,

                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>,
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>,
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::ring)>,

                InputDataNS::LightweightDomainList<whatever>,

                InputDataNS::Unknown<whatever>,
                InputDataNS::NumberingSubset<whatever>,
                InputDataNS::FEltSpace<whatever>,
                InputDataNS::Petsc<whatever>,

                InputDataNS::Result
            >;
            // clang-format on

            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_
