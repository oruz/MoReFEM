/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Aug 2017 16:35:55 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Geometry/CoordsInParallel/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace CoordsInParallelNS
        {


            inline const std::string& Model::ClassName()
            {
                static std::string name("Model");
                return name;
            }


            inline bool Model::SupplHasFinishedConditions() const
            {
                return true;
            }


            inline void Model::SupplInitializeStep()
            { }


        } // namespace CoordsInParallelNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_MODEL_HXX_
