/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace CoordsInParallelNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
            {
                unknown = 1
            };


            //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

            {
                solver = 1
            };


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_input_data_tuple
            // clang-format off
            using InputDataTuple = std::tuple
            <
                InputDataNS::TimeManager,

                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>,
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>,

                InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,

                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>,
                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>,

                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh1)>,
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh2)>,

                InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>,
                InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>,

                InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

                InputDataNS::Result

            >;
            // clang-format on


            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace CoordsInParallelNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_
