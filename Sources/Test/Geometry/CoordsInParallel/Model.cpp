/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Aug 2017 16:35:55 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "Test/Geometry/CoordsInParallel/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace CoordsInParallelNS
        {


            Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
            { }


            void Model::SupplInitialize()
            { }


            void Model::Forward()
            { }


            void Model::SupplFinalizeStep()
            { }


            void Model::SupplFinalize() const
            {
#ifndef NDEBUG
                {
                    decltype(auto) mesh1 = this->GetMesh(EnumUnderlyingType(MeshIndex::mesh1));
                    decltype(auto) mesh2 = this->GetMesh(EnumUnderlyingType(MeshIndex::mesh2));
                    decltype(auto) mpi = parent::GetMpi();

                    const auto Nmesh1_coords = mesh1.NprocessorWiseCoord() + mesh1.NghostCoord();
                    const auto Nmesh2_coords = mesh2.NprocessorWiseCoord() + mesh2.NghostCoord();

                    const auto Ncoords_in_meshes = Nmesh1_coords + Nmesh2_coords;

                    if (Ncoords_in_meshes != Coords::Nobjects())
                    {
                        std::ostringstream oconv;
                        oconv << "Inconsistent number of Coords on processor " << mpi.GetRank<int>()
                              << ": both meshes "
                                 "encompasses "
                              << Ncoords_in_meshes << " whereas there are " << Coords::Nobjects()
                              << " Coords "
                                 "object currently alive on the processor.";

                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                }
#endif // NDEBUG
            }


        } // namespace CoordsInParallelNS


    } // namespace TestNS

} // namespace MoReFEM
