/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HXX_

// IWYU pragma: private, include "Test/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace LoadPrepartitionedGodOfDofNS
        {


            inline const std::string& Model::ClassName()
            {
                static std::string ret("Test LoadPrepartitionedGodOfDof");
                return ret;
            }


            inline bool Model::SupplHasFinishedConditions() const
            {
                return false; // ie no additional condition
            }


            inline void Model::SupplInitializeStep()
            { }


        } // namespace LoadPrepartitionedGodOfDofNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HXX_
