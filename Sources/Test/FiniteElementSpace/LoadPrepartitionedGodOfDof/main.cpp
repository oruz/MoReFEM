/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstdlib>

#define BOOST_TEST_MODULE load_god_of_dof_prepartitioned_data_tri
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"
#include "Test/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::LoadPrepartitionedGodOfDofNS::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__


BOOST_FIXTURE_TEST_CASE(load_god_of_dof_from_prepartitioned, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.LoadGodOfDofFromPrepartionedData();
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_processor_wise_node_bearer_list, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.CheckProcessorWiseNodeBearerList();
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_ghost_node_bearer_list, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.CheckGhostNodeBearerList();
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.CheckDofListInGodOfDof();
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_scalar_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        decltype(auto) model = GetModel();
        model.CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex::scalar);
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_vectorial_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        decltype(auto) model = GetModel();
        model.CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex::vectorial);
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_mixed_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        decltype(auto) model = GetModel();
        model.CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex::mixed);
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_separated_felt_space, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        decltype(auto) model = GetModel();
        model.CheckDofListInFEltSpace(FEltSpaceIndex::separated);
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_mixed_felt_space, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        decltype(auto) model = GetModel();
        model.CheckDofListInFEltSpace(FEltSpaceIndex::mixed);
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


BOOST_FIXTURE_TEST_CASE(check_matrix_pattern, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.CheckMatrixPattern();
    }
    catch (const ExceptionNS::GracefulExit&) // TMP #1443
    { }
}


PRAGMA_DIAGNOSTIC(pop)
