/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace LoadPrepartitionedGodOfDofNS
        {


            /*!
             * \brief Toy model used to perform tests about reconstruction of a \a Mesh from pre-partitioned data.
             *
             * This test must be run in parallel; its principle is to:
             * - Initialize the model normally (including separating the mesh among all mpi processors and reducing it).
             * - Write the data related to the partition of said mesh.
             * - Load from the data written a new mesh.
             * - Check both mesh are indistinguishable.
             */
            //! \copydoc doxygen_hide_model_4_test
            class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
            {

              private:
                //! \copydoc doxygen_hide_alias_self
                using self = Model;

                //! Convenient alias.
                using parent = MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;


              public:
                //! Return the name of the model.
                static const std::string& ClassName();

                //! Friendship granted to the base class so this one can manipulates private methods.
                friend parent;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_morefem_data_param
                 */
                Model(const morefem_data_type& morefem_data);

                //! Destructor.
                ~Model() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Model(const Model& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Model(Model&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Model& operator=(const Model& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Model& operator=(Model&& rhs) = delete;

                ///@}


                /// \name Crtp-required methods.
                ///@{


                /*!
                 * \brief Initialise the problem.
                 *
                 * This initialisation includes the resolution of the static problem.
                 */
                void SupplInitialize();


                //! Manage time iteration.
                void Forward();

                /*!
                 * \brief Additional operations to finalize a dynamic step.
                 *
                 * Base class already update the time for next time iterations.
                 */
                void SupplFinalizeStep();


                /*!
                 * \brief Initialise a dynamic step.
                 *
                 */
                void SupplFinalize();

                /*!
                 * \brief Load the \a GodOfDof from prepartitioned data.
                 */
                void LoadGodOfDofFromPrepartionedData() const;

                //! Check the \a NodeBearer list for processor-wise data is the same as in the original run.
                void CheckProcessorWiseNodeBearerList() const;

                //! Check the \a NodeBearer list for ghost data is the same as in the original run.
                void CheckGhostNodeBearerList() const;

                //! Check the \a Dof list is the same as in the original run in \a GodOfDof.
                void CheckDofListInGodOfDof() const;

                //! Check the indexes of \a Dof related to the \a NumberingSubset indexed by \a numbering_subset_index
                //! are on par with original run. \param[in] numbering_subset_index Index of the \a NumberingSubset
                //! considered.
                void CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex numbering_subset_index) const;

                //! Check the \a Dof list is the same as in the original run in \a FEltSpace.
                //! \param[in] felt_space_index Index of the \a FEltSpace considered.
                void CheckDofListInFEltSpace(FEltSpaceIndex felt_space_index) const;

                //! Check the matrix pattern match.
                void CheckMatrixPattern() const;


              private:
                //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
                bool SupplHasFinishedConditions() const;


                /*!
                 * \brief Part of InitializedStep() specific to Elastic model.
                 *
                 * As there are none, the body of this method is empty.
                 */
                void SupplInitializeStep();


                ///@}
            };


        } // namespace LoadPrepartitionedGodOfDofNS


    } // namespace TestNS


} // namespace MoReFEM


#include "Test/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_MODEL_HPP_
