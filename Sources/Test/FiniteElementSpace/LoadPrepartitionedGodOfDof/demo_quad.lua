-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {


	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,


	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,


	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = .1

} -- transient

Mesh1 = {


	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = "${MOREFEM_ROOT}/Data/Mesh/test_cube_2.mesh",


	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = "Medit",


	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an
	-- exception is thrown.
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,


	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh1


Domain1 = {

    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },

    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = {},

    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = {}

} -- Domain1


Domain11 = {

    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 11 },

    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },

    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = {},

    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = {}

} -- Domain11


Unknown1 = {


    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "scalar",


    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar"

} -- Unknown1


Unknown2 = {


	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = "vectorial",


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'scalar', 'vectorial'})
	nature = "vectorial"

} -- Unknown2



NumberingSubset1 = {


	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
	-- the possible values to choose elsewhere).
	-- Expected format: "VALUE"
	name = "scalar",


	-- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
	-- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
	-- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
	-- displacement.
	-- Expected format: 'true' or 'false' (without the quote)
	do_move_mesh = false

} -- NumberingSubset1


NumberingSubset2 = {


    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "vectorial",


    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

} -- NumberingSubset2


NumberingSubset3 = {


    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "mixed",


    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

} -- NumberingSubset3


FiniteElementSpace1 = {


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,


	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 1,


	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "scalar", "vectorial" },


    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "Q3", "Q1" },


    -- List of the numbering subset to use for each unknown;
    -- Expected format: { VALUE1, VALUE2, ...}
    numbering_subset_list = { 1, 2 }

} -- FiniteElementSpace1


FiniteElementSpace2 = {


    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,


    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,


    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "scalar", "vectorial" },


    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "Q3", "Q1" },


    -- List of the numbering subset to use for each unknown;
    -- Expected format: { VALUE1, VALUE2, ...}
    numbering_subset_list = { 3, 3 }

} -- FiniteElementSpace2


Petsc1 = {


	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,


	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,


	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,


	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',


	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-9,


	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-8,


	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'Mumps'

} -- Petsc1


EssentialBoundaryCondition1 = {


    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = 'scalar',


    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp1',


    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'scalar',


    -- Values at each of the relevant component.
    -- Expected format: { VALUE1, VALUE2, ...}
    value = { 5 },


    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 1,


    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false

} -- EssentialBoundaryCondition1


EssentialBoundaryCondition2 = {


    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = 'vectorial',


    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp123',


    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'scalar',


    -- Values at each of the relevant component.
    -- Expected format: { VALUE1, VALUE2, ...}
    value = { 10, 20, 30 },


    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 1,


    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false

} -- EssentialBoundaryCondition2

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. Please do not read this value
	-- directly: it might have been extended in MoReFEMData class! Rather call the GetResultDirectory() from
	-- this class.
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_TEST_OUTPUT_DIR}/Test/LoadPrepartitionedGodOfDof/Quad/Results',


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,

    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false

} -- Result


Parallelism = {


    -- What should be done for a parallel run. There are 4 possibilities:
    --  'Precompute': Precompute the data for a later parallel run and stop once it's done.
    --  'ParallelNoWrite': Run the code in parallel without using any pre-processed data and do not write down
    -- the processed data.
    --  'Parallel': Run the code in parallel without using any pre-processed data and write down the processed
    -- data.
    --  'RunFromPreprocessed': Run the code in parallel using pre-processed data.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})
    policy = 'Parallel',


    -- Directory in which parallelism data will be written or read (depending on the policy).
    -- Expected format: "VALUE"
    directory = '${MOREFEM_TEST_OUTPUT_DIR}/Test/LoadPrepartitionedGodOfDof/Quad/PrepartitionedData'

} -- Parallelism
