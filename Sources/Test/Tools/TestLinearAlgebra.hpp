/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 13 Sep 2017 17:32:09 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HPP_

#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \class doxygen_hide_test_linear_alg_common_arg
         *
         * This functionality, as pointed out by its namespace, is dedicated to tests!
         *
         * It is written to work both in sequential and in parallel; in parallel ghost values aren't checked.
         *
         * \param[in] god_of_dof \a GodOfDof onto which the dofs present in the matrix or vector checked is defined.
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         */


        /*!
         * \brief Check whether a Petsc matrix includes the expected content.
         *
         * \copydoc doxygen_hide_test_linear_alg_common_arg
         * \param[in] obtained The matrix obtained by the computation. As it is for a test, it is expected to be a
         * fairly small matrix.
         * \param[in] expected The expected (program-wise) matrix, given in dense format. Interior vector features all
         * the values on a given row.
         */
        void CheckMatrix(const GodOfDof& god_of_dof,
                         const GlobalMatrix& obtained,
                         const std::vector<std::vector<PetscScalar>>& expected,
                         const char* invoking_file,
                         int invoking_line,
                         double epsilon = 1.e-12);


        /*!
         * \brief Check whether a Petsc vector includes the expected content.
         *
         * \copydoc doxygen_hide_test_linear_alg_common_arg
         * \param[in] obtained The vector obtained by the computation.
         * \param[in] expected The expected (program-wise) values to be found in the vector.
         */
        void CheckVector(const GodOfDof& god_of_dof,
                         const GlobalVector& obtained,
                         const std::vector<PetscScalar>& expected,
                         const char* invoking_file,
                         int invoking_line,
                         double epsilon = 1.e-12);


    } // namespace TestNS


} // namespace MoReFEM


#include "Test/Tools/TestLinearAlgebra.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HPP_
