/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 13 Sep 2017 17:32:09 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HXX_

// IWYU pragma: private, include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_TEST_LINEAR_ALGEBRA_HXX_
