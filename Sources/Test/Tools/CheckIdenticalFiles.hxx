/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HXX_

// IWYU pragma: private, include "Test/Tools/CheckIdenticalFiles.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HXX_
