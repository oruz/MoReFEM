/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENVIRONMENT_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENVIRONMENT_HPP_

#include "Utilities/Environment/Environment.hpp" // IWYU pragma: export

namespace MoReFEM
{


    namespace TestNS::FixtureNS
    {


        /*!
         * \brief An helper class to build tests with with Boost test.
         *
         * This class just defines environment variables MOREFEM_ROOT and MOREFEM_TEST_OUTPUT_DIR which are expected
         * to be given on the command line in second and third position.
         */
        struct Environment
        {
          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Environment();

            //! Destructor.
            ~Environment() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Environment(const Environment& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Environment(Environment&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Environment& operator=(const Environment& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Environment& operator=(Environment&& rhs) = delete;

            ///@}
        };


    } // namespace TestNS::FixtureNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENVIRONMENT_HPP_
