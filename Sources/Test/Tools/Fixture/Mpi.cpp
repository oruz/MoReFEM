/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cassert>

#include "Test/Tools/Fixture/Mpi.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    namespace Impl
    {


        MpiHolder::MpiHolder()
        {
            char program_name[] = "ProgramName";
            char* argv[] = { program_name, nullptr };

            Wrappers::Mpi::InitEnvironment(1, argv);
            mpi_ = std::make_unique<Wrappers::Mpi>(0, Wrappers::MpiNS::Comm::World);
        }


        const Wrappers::Mpi& MpiHolder::GetMpi() const noexcept
        {
            assert(!(!mpi_));
            return *mpi_;
        }


    } // namespace Impl


    const Impl::MpiHolder& Mpi::GetMpiHolder() noexcept
    {
        static Impl::MpiHolder mpi_holder;
        return mpi_holder;
    }


    const Wrappers::Mpi& Mpi::GetMpi() noexcept
    {
        return GetMpiHolder().GetMpi();
    }


} // namespace MoReFEM::TestNS::FixtureNS
