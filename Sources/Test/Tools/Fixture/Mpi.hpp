/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MPI_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MPI_HPP_

#include <memory>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace TestNS::FixtureNS
    {


        namespace Impl
        {


            /*!
             * \brief An helper object to TestNS::FixtureNS::Mpi.
             *
             * Its purpose is solely to enable proper setup / tear down of Mpi environment; it is supposed to be called
             * once to create the environment (in TestNS::FixtureNS::Mpi constructor). Destruction should occur when
             * static local variables are freed at the end of the program.
             */
            class MpiHolder
            {
              public:
                //! Constructor which
                MpiHolder();

                //! Accessor to the \a ::MoReFEM::Wrappers::Mpi object.
                const Wrappers::Mpi& GetMpi() const noexcept;

              private:
                //! The \a ::MoReFEM::Wrappers::Mpi object.
                std::unique_ptr<Wrappers::Mpi> mpi_ = nullptr;
            };


        } // namespace Impl


        /*!
         * \brief An helper class to build tests with with Boost test.
         *
         * The idea is that mpi is *extremely* touchy when it comes to MPI_Init() and MPI_Finalize() calls;
         * the fixture is there only to ensure these are called only once.
         *
         */
        struct Mpi
        {
          public:
            //! Accessor to the Mpi object (which was the goal of all this hassle!)
            static const Wrappers::Mpi& GetMpi() noexcept;

          private:
            //! Accessor to an internal object which is used solely to enable proper setup / tear down of Mpi.
            static const Impl::MpiHolder& GetMpiHolder() noexcept;


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Mpi() = default;

            //! Destructor.
            ~Mpi() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Mpi(const Mpi& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Mpi(Mpi&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Mpi& operator=(const Mpi& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Mpi& operator=(Mpi&& rhs) = delete;

            ///@}
        };


    } // namespace TestNS::FixtureNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MPI_HPP_
