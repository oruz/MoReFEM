/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <iosfwd>
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Environment.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    Environment::Environment()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
            decltype(auto) cli_args = boost::unit_test::framework::master_test_suite().argv;

            environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", cli_args[1]), __FILE__, __LINE__);
            environment.SetEnvironmentVariable(
                std::make_pair("MOREFEM_TEST_OUTPUT_DIR", cli_args[2]), __FILE__, __LINE__);

            first_call = false;
        }
    }


} // namespace MoReFEM::TestNS::FixtureNS
