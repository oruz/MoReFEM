/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HPP_

#include <iosfwd>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \brief Check a file obtained in a test about a model is exactly the same as a reference one.
         *
         * This function is assumed to be called within a Catch TEST_CASE; in case there is an issue (non existent
         * file or non consistant content) the Catch test fails.
         *
         * \param[in] ref_dir The reference directory. It is a subdirectory of ModelInstances/ *YourModel*
         * /ExpectedResults, e.g. ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/ExpectedResults/1D/Mesh_1/Ensight6.
         * \param[in] obtained_dir The pendant of \a ref_dir for the outputs given by the model that is expected to have
         * run juste before the call to the result check. It is expected to be a subdirectory somwehere in
         * ${MOREFEM_TEST_OUTPUT_DIR}.
         * \param[in] filename Name of the file being compared. It is expected to be the same in \a ref_dir and in
         * \a obtained_dir and to be located directly there (not in a subdirectory).
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        void CheckIdenticalFiles(const FilesystemNS::Directory& ref_dir,
                                 const FilesystemNS::Directory& obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file,
                                 int invoking_line);


    } // namespace TestNS


} // namespace MoReFEM


#include "Test/Tools/CheckIdenticalFiles.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_TOOLS_x_CHECK_IDENTICAL_FILES_HPP_
