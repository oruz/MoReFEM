/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Mar 2018 16:10:26 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HPP_

#include <memory>

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        /*!
         * \brief Class which point is to initialize properly \a MoReFEMData object.
         *
         * In a \a Model this is done by parsing the command line arguments, but in Catch2 tests we can't do that,
         * so the Lua file is given as is in the main program. Under the hood we have to mimic the argument argv
         * we would have obtained from the command line; it is the point of current class.
         */
        template<class InputDataT>
        class InitializeTestMoReFEMData
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = InitializeTestMoReFEMData<InputDataT>;

            //! Alias to the type of \a MoReFEMData.
            using morefem_data_type = MoReFEMData<InputDataT, program_type::test>;

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const self>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] lua_file Path to the Lua input data file.
             *
             * \tparam T A type convertible to std::string; the point here is to use an universal reference.
             */
            template<class T>
            explicit InitializeTestMoReFEMData(T&& lua_file);

            //! Destructor.
            ~InitializeTestMoReFEMData() = default;

            //! \copydoc doxygen_hide_copy_constructor
            InitializeTestMoReFEMData(const InitializeTestMoReFEMData& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            InitializeTestMoReFEMData(InitializeTestMoReFEMData&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            InitializeTestMoReFEMData& operator=(const InitializeTestMoReFEMData& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            InitializeTestMoReFEMData& operator=(InitializeTestMoReFEMData&& rhs) = delete;

            ///@}

            //! Accessor to the \a MoReFEMData object.
            const morefem_data_type& GetMoReFEMData() const noexcept;

          private:
            //! Storage of the \a MoReFEMData object.
            typename morefem_data_type::const_unique_ptr morefem_data_ = nullptr;

            //! Generated argv.
            char** argv_;
        };


    } // namespace TestNS


} // namespace MoReFEM


#include "Test/Tools/InitializeTestMoReFEMData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HPP_
