/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <sstream>
#include <string>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        void CheckIdenticalFiles(const FilesystemNS::Directory& ref_dir,
                                 const FilesystemNS::Directory& obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file,
                                 int invoking_line)
        {
            std::ostringstream oconv;

            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(ref_dir.GetPath()))
            {
                oconv << "Reference folder " << ref_dir << " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(obtained_dir.GetPath()))
            {
                oconv << "Result folder " << obtained_dir << " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            std::string ref_input_data = ref_dir.AddFile(filename);

            if (!FilesystemNS::File::DoExist(ref_dir))
            {
                oconv << "Reference file " + ref_input_data + " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            std::string obtained_input_data = obtained_dir.AddFile(filename);

            if (!FilesystemNS::File::DoExist(obtained_input_data))
            {
                oconv << "Result file " << obtained_input_data << " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            if (!FilesystemNS::File::AreEquals(ref_input_data, obtained_input_data, invoking_file, invoking_line))
            {
                oconv << "Reference file " << ref_input_data << " and result file " << obtained_input_data
                      << " are not identical.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }
        }


    } // namespace TestNS


} // namespace MoReFEM
