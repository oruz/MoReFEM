/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 13 Apr 2018 13:08:50 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <algorithm>
#include <cassert>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Test/Tools/CompareEnsightFiles.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace // anonymous
        {


            std::vector<double> ReadEnsightFile(const std::string& file);

            void CheckAreEquals(const std::vector<double>& ref,
                                const std::vector<double>& obtained,
                                const std::string& ref_file,
                                const std::string& obtained_file,
                                const char* invoking_file,
                                int invoking_line,
                                double epsilon);


        } // namespace


        void CompareEnsightFiles(const FilesystemNS::Directory& a_ref_dir,
                                 const FilesystemNS::Directory& a_obtained_dir,
                                 std::string&& filename,
                                 const char* invoking_file,
                                 int invoking_line,
                                 double epsilon)
        {
            std::ostringstream oconv;

            decltype(auto) ref_dir = a_ref_dir.GetPath();
            decltype(auto) obtained_dir = a_obtained_dir.GetPath();

            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(ref_dir))
            {
                oconv << "Reference folder " + ref_dir + " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(obtained_dir))
            {
                oconv << "Result folder " + obtained_dir + " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            std::string ref_input_data = ref_dir + "/" + filename;

            if (!FilesystemNS::File::DoExist(ref_dir))
            {
                oconv << "Reference file " + ref_input_data + " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            std::string obtained_input_data = obtained_dir + "/" + filename;

            if (!FilesystemNS::File::DoExist(obtained_input_data))
            {
                oconv << "Result file " + obtained_input_data + " does not exist.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            const auto ref_values = ReadEnsightFile(ref_input_data);
            const auto obtained_values = ReadEnsightFile(obtained_input_data);

            if (ref_values.size() != obtained_values.size())
            {
                oconv << "Number of values read in reference (" << ref_values.size() << " for file " << ref_input_data
                      << ") and output (" << obtained_values.size() << " for file " << obtained_input_data
                      << ") is not the same!";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            // An exception is thrown in this function if they aren't very similar.
            CheckAreEquals(ref_values,
                           obtained_values,
                           ref_input_data,
                           obtained_input_data,
                           invoking_file,
                           invoking_line,
                           epsilon);
        }


        namespace // anonymous
        {


            std::vector<double> ReadEnsightFile(const std::string& file)
            {
                std::ifstream in;
                FilesystemNS::File::Read(in, file, __FILE__, __LINE__);

                std::string line;
                getline(in, line); // skip first line

                std::string str_value;

                std::vector<double> ret;

                while (getline(in, line))
                {
                    const auto max = std::min(line.size(), 72ul);

                    for (auto index = 0ul; index < max; index += 12ul)
                    {
                        str_value.assign(line, index, 12ul);
                        ret.push_back(std::stod(str_value));
                    }
                }

                return ret;
            }


            void CheckAreEquals(const std::vector<double>& ref,
                                const std::vector<double>& obtained,
                                const std::string& ref_file,
                                const std::string& obtained_file,
                                const char* invoking_file,
                                int invoking_line,
                                double epsilon)
            {
                assert(ref.size() == obtained.size() && "REQUIRE conditions should make sure it's the case.");

                const auto end_obtained = obtained.cend();

                for (auto it_obtained = obtained.cbegin(), it_ref = ref.cbegin(); it_obtained != end_obtained;
                     ++it_obtained, ++it_ref)
                {
                    const auto obtained_value = *it_obtained;
                    const auto ref_value = *it_ref;

                    // Crude condition, that is ok for most of the cases!
                    if (!NumericNS::AreEqual(ref_value, obtained_value, epsilon))
                    {
                        std::ostringstream oconv;
                        oconv << it_ref - ref.cbegin() << "-th value in " << ref_file
                              << " is not identical to what "
                                 "was stored as a reference in file "
                              << obtained_file << "(values are respectively " << ref_value << " and " << obtained_value
                              << ").";
                        throw Exception(oconv.str(), invoking_file, invoking_line);
                    }
                }
            }


        } // namespace


    } // namespace TestNS


} // namespace MoReFEM
