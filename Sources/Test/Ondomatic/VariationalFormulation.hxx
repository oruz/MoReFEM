/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Apr 2016 11:29:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "Test/Ondomatic/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace OndomaticNS
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
        VariationalFormulation::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const VariationalFormulation::source_operator_type&
        VariationalFormulation::GetSurfacePressure4Operator() const noexcept
        {
            assert(!(!surface_pressure_4_operator_));
            return *surface_pressure_4_operator_;
        }

        inline const VariationalFormulation::source_operator_type&
        VariationalFormulation::GetSurfacePressure5Operator() const noexcept
        {
            assert(!(!surface_pressure_5_operator_));
            return *surface_pressure_5_operator_;
        }


        inline const VariationalFormulation::source_operator_type&
        VariationalFormulation::GetSurfacePressure6Operator() const noexcept
        {
            assert(!(!surface_pressure_6_operator_));
            return *surface_pressure_6_operator_;
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixStiffness() const noexcept
        {
            assert(!(!matrix_stiffness_));
            return *matrix_stiffness_;
        }

        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixStiffness()
        {
            return const_cast<GlobalMatrix&>(GetMatrixStiffness());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacePressure4() const noexcept
        {
            assert(!(!vector_surface_pressure_4_));
            return *vector_surface_pressure_4_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacePressure4()
        {
            return const_cast<GlobalVector&>(GetVectorSurfacePressure4());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacePressure5() const noexcept
        {
            assert(!(!vector_surface_pressure_5_));
            return *vector_surface_pressure_5_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacePressure5()
        {
            return const_cast<GlobalVector&>(GetVectorSurfacePressure5());
        }

        inline const GlobalVector& VariationalFormulation::GetVectorSurfacePressure6() const noexcept
        {
            assert(!(!vector_surface_pressure_6_));
            return *vector_surface_pressure_6_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacePressure6()
        {
            return const_cast<GlobalVector&>(GetVectorSurfacePressure6());
        }


        inline const NumberingSubset& VariationalFormulation::GetNumberingSubset() const
        {
            return numbering_subset_;
        }


    } // namespace OndomaticNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HXX_
