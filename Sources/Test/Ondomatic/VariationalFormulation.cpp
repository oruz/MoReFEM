/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Apr 2016 11:29:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"

#include "Test/Ondomatic/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace OndomaticNS
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation::VariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          numbering_subset_(numbering_subset)
        { }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof();

            decltype(auto) input_data = morefem_data.GetInputData();

            const FEltSpace& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const FEltSpace& felt_space_surface_pressure_4 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_pressure_4));
            const FEltSpace& felt_space_surface_pressure_5 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_pressure_5));
            const FEltSpace& felt_space_surface_pressure_6 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_pressure_6));

            const auto& pressure_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                           .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));


            {
                const auto& numbering_subset = GetNumberingSubset();

                constexpr auto index = EnumUnderlyingType(InitialConditionIndex::pressure_initial_condition);

                InitializeVectorSystemSolution<index>(
                    input_data, numbering_subset, *pressure_ptr, felt_space_highest_dimension);
            }

            namespace GVO = GlobalVariationalOperatorNS;
            decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                        .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_4)>;

                surface_pressure_4_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("surface_pressure_4", domain, input_data);

                if (surface_pressure_4_parameter_ != nullptr)
                {
                    surface_pressure_4_operator_ = std::make_unique<source_operator_type>(
                        felt_space_surface_pressure_4, pressure_ptr, *surface_pressure_4_parameter_);
                }
            }

            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_5)>;

                surface_pressure_5_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("surface_pressure_5", domain, input_data);

                if (surface_pressure_5_parameter_ != nullptr)
                {
                    surface_pressure_5_operator_ = std::make_unique<source_operator_type>(
                        felt_space_surface_pressure_5, pressure_ptr, *surface_pressure_5_parameter_);
                }
            }

            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_6)>;

                surface_pressure_6_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("surface_pressure_6", domain, input_data);

                if (surface_pressure_6_parameter_ != nullptr)
                {
                    surface_pressure_6_operator_ = std::make_unique<source_operator_type>(
                        felt_space_surface_pressure_6, pressure_ptr, *surface_pressure_6_parameter_);
                }
            }

            const auto& numbering_subset = GetNumberingSubset();
            const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();

            std::vector<std::size_t> dofs_bc;

            for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
            {
                assert(!(!dirichlet_bc_ptr));

                const auto& dirichlet_bc = *dirichlet_bc_ptr;

                const auto& dof_list = dirichlet_bc.GetDofList();

                std::size_t size = dof_list.size();

                for (std::size_t i = 0; i < size; ++i)
                    dofs_bc.push_back(dof_list[i]->GetProcessorWiseOrGhostIndex(numbering_subset));
            }

            DefineOperators();
            AssembleStaticOperators();
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& numbering_subset = GetNumberingSubset();

            parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
            parent::AllocateSystemVector(numbering_subset);

            const auto& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
            const auto& system_rhs = GetSystemRhs(numbering_subset);

            vector_surface_pressure_4_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surface_pressure_5_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surface_pressure_6_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
        }


        void VariationalFormulation::DefineOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));

            const auto& pressure_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                           .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));

            namespace GVO = GlobalVariationalOperatorNS;

            stiffness_operator_ =
                std::make_unique<GVO::GradPhiGradPhi>(felt_space_highest_dimension, pressure_ptr, pressure_ptr);
        }


        void VariationalFormulation::AssembleStaticOperators()
        {
            const auto& numbering_subset = GetNumberingSubset();

            auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);

            {
                GlobalMatrixWithCoefficient matrix(system_matrix, 1.);
                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(matrix)));
            }

            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(numbering_subset,
                                                                                         numbering_subset);

            const double time = GetTimeManager().GetTime();

            GetNonCstVectorSurfacePressure4().ZeroEntries(__FILE__, __LINE__);
            GetNonCstVectorSurfacePressure5().ZeroEntries(__FILE__, __LINE__);
            GetNonCstVectorSurfacePressure6().ZeroEntries(__FILE__, __LINE__);

            if (surface_pressure_4_parameter_ != nullptr)
            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorSurfacePressure4(), 1.);
                GetSurfacePressure4Operator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            if (surface_pressure_5_parameter_ != nullptr)
            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorSurfacePressure5(), 1.);
                GetSurfacePressure5Operator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            if (surface_pressure_6_parameter_ != nullptr)
            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorSurfacePressure6(), 1.);
                GetSurfacePressure6Operator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            GlobalVector& rhs = GetNonCstSystemRhs(GetNumberingSubset());

            rhs.Copy(GetVectorSurfacePressure4(), __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(1., GetVectorSurfacePressure5(), rhs, __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., GetVectorSurfacePressure6(), rhs, __FILE__, __LINE__);

            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                                      numbering_subset);
        }


    } // namespace OndomaticNS


} // namespace MoReFEM
