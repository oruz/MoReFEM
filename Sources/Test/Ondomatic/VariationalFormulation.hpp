/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HPP_
#define MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HPP_

#include <memory>
#include <vector>

#include "Utilities/InputData/LuaFunction.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/VariationalFormulation.hpp"

#include "Test/Ondomatic/InputData.hpp"


namespace MoReFEM
{


    namespace OndomaticNS
    {


        //! \copydoc doxygen_hide_varf_4_test
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>
        {
          private:
            //! \copydoc doxygen_hide_alias_self
            using self = VariationalFormulation;

            //! Alias to the parent class.
            using parent =
                MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

          public:
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to source operator.
            using source_operator_type = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \copydoc doxygen_hide_varf_constructor
             * \param[in] numbering_subset Only \a NumberingSubset relevant for this formulation. It is a remnant of
             * a former interface of \a MoReFEM::VariationalFormulation; now in most cases (but obviously not in
             * test...) \a NumberingSubset is reached through the \a GodOfDof dedicated method.
             */
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& numbering_subset,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() override;

            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulation(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulation(VariationalFormulation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

            ///@}

            //! Get the only numbering subset relevant for this VariationalFormulation.
            const NumberingSubset& GetNumberingSubset() const;

          private:
            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const morefem_data_type& morefem_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}

          private:
            /*!
             * \brief Assemble method for all the static operators.
             */
            void AssembleStaticOperators();

          private:
            /*!
             * \brief Define the properties of all the global variational operators involved.
             */
            void DefineOperators();

            //! Get the hyperelastic stiffness operator.
            const GlobalVariationalOperatorNS::GradPhiGradPhi& GetStiffnessOperator() const noexcept;

            //! Get the  Operator
            const source_operator_type& GetSurfacePressure4Operator() const noexcept;

            //! Get the  Operator
            const source_operator_type& GetSurfacePressure5Operator() const noexcept;

            //! Get the  Operator
            const source_operator_type& GetSurfacePressure6Operator() const noexcept;

          private:
            /// \name Global variational operators.
            ///@{

            //! Stiffness operator.
            GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr stiffness_operator_ = nullptr;

            //! Volumic source operator.
            source_operator_type::const_unique_ptr surface_pressure_4_operator_ = nullptr;

            //! Volumic source operator.
            source_operator_type::const_unique_ptr surface_pressure_5_operator_ = nullptr;

            //! Volumic source operator.
            source_operator_type::const_unique_ptr surface_pressure_6_operator_ = nullptr;

            ///@}

            /// \name Parameters used to define TransientSource operators.
            ///@{

            //! Volumic source parameter.
            ScalarParameter<>::unique_ptr surface_pressure_4_parameter_ = nullptr;

            //! Neumann parameter.
            ScalarParameter<>::unique_ptr surface_pressure_5_parameter_ = nullptr;

            //! Robin parameter.
            ScalarParameter<>::unique_ptr surface_pressure_6_parameter_ = nullptr;

            ///@}

          private:
            /// \name Accessors to the global vectors and matrices managed by the class.
            ///@{

            //! \accessor{SurfacePressure4 vector}
            const GlobalVector& GetVectorSurfacePressure4() const noexcept;

            //! \non_cst_accessor{SurfacePressure4 vector}
            GlobalVector& GetNonCstVectorSurfacePressure4();

            //! \accessor{SurfacePressure5 vector}
            const GlobalVector& GetVectorSurfacePressure5() const noexcept;

            //! \non_cst_accessor{SurfacePressure5 vector}
            GlobalVector& GetNonCstVectorSurfacePressure5();

            //! \accessor{SurfacePressure6 vector}
            const GlobalVector& GetVectorSurfacePressure6() const noexcept;

            //! \non_cst_accessor{SurfacePressure6 vector}
            GlobalVector& GetNonCstVectorSurfacePressure6();

            //! \accessor{Stiffness matrix}
            const GlobalMatrix& GetMatrixStiffness() const noexcept;

            //! \non_cst_accessor{Stiffness matrix}
            GlobalMatrix& GetNonCstMatrixStiffness();

            ///@}

          private:
            /// \name Global vectors and matrices specific to the problem.
            ///@{

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_surface_pressure_4_ = nullptr;

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_surface_pressure_5_ = nullptr;

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_surface_pressure_6_ = nullptr;

            //! Matrix tangent following pressure.
            GlobalMatrix::unique_ptr matrix_stiffness_ = nullptr;

            ///@}

          private:
            /// \name Numbering subsets used in the formulation.
            ///@{

            //! \a NumberingSubset.
            const NumberingSubset& numbering_subset_;

            ///@}
        };


    } // namespace OndomaticNS


} // namespace MoReFEM


#include "Test/Ondomatic/VariationalFormulation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_ONDOMATIC_x_VARIATIONAL_FORMULATION_HPP_
