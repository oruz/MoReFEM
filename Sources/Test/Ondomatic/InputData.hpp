/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace OndomaticNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            highest_dimension = 1,
            face1 = 2,
            face2 = 3,
            face3 = 4,
            face4 = 5,
            face5 = 6,
            face6 = 7,
            face123 = 8,
            full_mesh = 9
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            face123 = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            surface_pressure_4 = 2,
            surface_pressure_5 = 3,
            surface_pressure_6 = 4
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            pressure = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            monolithic = 1
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            pressure_initial_condition = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : std::size_t
        {
            surface_pressure_4 = 1,
            surface_pressure_5 = 2,
            surface_pressure_6 = 3
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face2)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face3)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face4)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face5)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face6)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face123)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face123)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_4)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_5)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_6)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::pressure_initial_condition)>,

            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_4)>,
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_5)>,
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_6)>,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace OndomaticNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_DATA_HPP_
