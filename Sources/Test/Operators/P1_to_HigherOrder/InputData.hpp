/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace P1_to_P_HigherOrder_NS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                domain = 1
            };


            //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
            {
                fluid_velocity = 1,
                solid_velocity = 2
            };


            //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
            {
                velocity = 1,
                higher_order_velocity = 2,
                pressure = 3,
            };


            //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

            {
                solver = 1
            };


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
            {
                fluid_velocity = 1,
                solid_velocity = 2
            };


            //! \copydoc doxygen_hide_input_data_tuple
            // clang-format off
            using InputDataTuple = std::tuple
            <
                InputDataNS::TimeManager,

                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fluid_velocity)>,
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::solid_velocity)>,

                InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
                InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,
                InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::higher_order_velocity)>,

                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

                InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid_velocity)>,
                InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid_velocity)>,

                InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

                InputDataNS::Result
            >;
            // clang-format on


            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace P1_to_P_HigherOrder_NS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_DATA_HPP_
