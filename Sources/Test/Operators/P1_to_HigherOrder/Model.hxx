/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/P1_to_HigherOrder/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace P1_to_P_HigherOrder_NS
        {


            template<HigherOrder HigherOrderT>
            Model<HigherOrderT>::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
            { }


            namespace // anonymous
            {


                template<class InterpolatorT>
                void WriteInterpolatorMatrix(const InterpolatorT& interpolator,
                                             const std::string& output_directory,
                                             const Wrappers::Mpi& mpi)
                {

                    const auto& matrix = interpolator.GetInterpolationMatrix();

                    std::ostringstream oconv;
                    oconv << output_directory << "/interpolation" << matrix.GetColNumberingSubset().GetUniqueId() << '_'
                          << matrix.GetRowNumberingSubset().GetUniqueId() << ".hhdata";

                    std::string output_file(oconv.str());

                    if (mpi.IsRootProcessor())
                    {
                        if (FilesystemNS::File::DoExist(output_file))
                            FilesystemNS::File::Remove(output_file, __FILE__, __LINE__);
                    }

                    mpi.Barrier();

                    matrix.View(mpi, output_file, __FILE__, __LINE__);
                }


            } // namespace


            template<HigherOrder HigherOrderT>
            void Model<HigherOrderT>::SupplInitialize()
            {
                const auto& god_of_dof = parent::GetGodOfDof(1);

                const auto& source_felt_space = god_of_dof.GetFEltSpace(1);
                const auto& target_felt_space = god_of_dof.GetFEltSpace(2);


                // \todo #67
                const auto& source_numbering_subset = *source_felt_space.GetNumberingSubsetList()[0];
                const auto& target_numbering_subset = *target_felt_space.GetNumberingSubsetList()[0];

                const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

                const auto& velocity_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::velocity));
                const auto& p_higher_order_velocity_ptr =
                    unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::higher_order_velocity));
                const auto& output_directory = parent::GetOutputDirectory();
                const auto& mpi = parent::GetMpi();


                {
                    ConformInterpolatorNS::pairing_type&& pairing{ { velocity_ptr, p_higher_order_velocity_ptr } };


                    interpolator_p1_p_higher_order_ =
                        std::make_unique<typename traits::to_higher_order_type>(source_felt_space,
                                                                                source_numbering_subset,
                                                                                target_felt_space,
                                                                                target_numbering_subset,
                                                                                std::move(pairing));


                    interpolator_p1_p_higher_order_->Init();

                    WriteInterpolatorMatrix(*interpolator_p1_p_higher_order_, output_directory, mpi);
                }

                {

                    ConformInterpolatorNS::pairing_type&& pairing{ { p_higher_order_velocity_ptr, velocity_ptr } };

                    interpolator_p_higher_order_p1_ =
                        std::make_unique<typename traits::from_higher_order_type>(target_felt_space,
                                                                                  target_numbering_subset,
                                                                                  source_felt_space,
                                                                                  source_numbering_subset,
                                                                                  std::move(pairing));

                    interpolator_p_higher_order_p1_->Init();


                    WriteInterpolatorMatrix(*interpolator_p_higher_order_p1_, output_directory, mpi);
                }


                {
                    Wrappers::Petsc::Matrix matrix;

                    Wrappers::Petsc::MatMatMult(interpolator_p_higher_order_p1_->GetInterpolationMatrix(),
                                                interpolator_p1_p_higher_order_->GetInterpolationMatrix(),
                                                matrix,
                                                __FILE__,
                                                __LINE__);

                    std::ostringstream oconv;
                    oconv << output_directory << "/product.hhdata";
                    matrix.View(mpi, oconv.str(), __FILE__, __LINE__);

                    // Check it is the identity matrix, by using a test vector...
                    GlobalVector test(source_numbering_subset);
                    AllocateGlobalVector(god_of_dof, test);
                    GlobalVector result(test);

                    test.SetUniformValue(1., __FILE__, __LINE__);

                    Wrappers::Petsc::MatMult(matrix, test, result, __FILE__, __LINE__);

                    std::string desc;
                    if (!Wrappers::Petsc::AreEqual(
                            test, result, NumericNS::DefaultEpsilon<double>(), desc, __FILE__, __LINE__))
                        throw Exception(
                            "Transformation (Phigher -> P1) o (P1 -> Phigher) is not identity!", __FILE__, __LINE__);
                }
            }


            template<HigherOrder HigherOrderT>
            void Model<HigherOrderT>::Forward()
            { }


            template<HigherOrder HigherOrderT>
            void Model<HigherOrderT>::SupplFinalizeStep()
            { }


            template<HigherOrder HigherOrderT>
            void Model<HigherOrderT>::SupplFinalize()
            { }


            template<HigherOrder HigherOrderT>
            inline const std::string& Model<HigherOrderT>::ClassName()
            {
                return traits::to_higher_order_type::ClassName();
            }


            template<HigherOrder HigherOrderT>
            inline bool Model<HigherOrderT>::SupplHasFinishedConditions() const
            {
                return false; // ie no additional condition
            }


            template<HigherOrder HigherOrderT>
            inline void Model<HigherOrderT>::SupplInitializeStep()
            { }


        } // namespace P1_to_P_HigherOrder_NS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HXX_
