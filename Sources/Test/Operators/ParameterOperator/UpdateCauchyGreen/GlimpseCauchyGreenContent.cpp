/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/GlimpseCauchyGreenContent.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    auto GlimpseCauchyGreenContent(const Model& model) -> const Model::glimpse_cauchy_green_type&
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) mesh = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh)).GetMesh();

        const auto& [it_begin, it_end] =
            mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(mesh.GetDimension());

        // The tests are run only on specific meshes with only one \a GeometricElt.
        // There are no strong content for that, but no need either to check further.
        BOOST_CHECK_EQUAL(it_end - it_begin, 1l);

        BOOST_CHECK(!(!*it_begin));

        const auto geom_elt_id = (*it_begin)->GetIndex();

        decltype(auto) content = model.GetCauchyGreenTensorStorage();

        auto it = content.find(geom_elt_id);

        BOOST_CHECK(it != content.cend());

        constexpr auto arbitrary_quad_pt_chosen = 2ul;

        BOOST_CHECK(arbitrary_quad_pt_chosen < content.size());

        return it->second[arbitrary_quad_pt_chosen].value;
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
