/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

// IWYU pragma: private, include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/String/EmptyString.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/CauchyGreenValue.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS
{


    const std::string& Initial::NameInFile()
    {
        static std::string ret("cauchy_green_initial_value");
        return ret;
    }


    const std::string& Initial::Description()
    {
        static std::string ret("Arbitrary initial Cauchy-Green value put in the tensor");
        return ret;
    }


    const std::string& Initial::Constraint()
    {
        return Utilities::EmptyString();
    }


    const std::string& Initial::DefaultValue()
    {
        return Utilities::EmptyString();
    }


    const std::string& AfterOperator::NameInFile()
    {
        static std::string ret("cauchy_green_value_after_operator");
        return ret;
    }


    const std::string& AfterOperator::Description()
    {
        static std::string ret("Expected Cauchy-Green value after the operator was applied.");
        return ret;
    }


    const std::string& AfterOperator::Constraint()
    {
        return Utilities::EmptyString();
    }


    const std::string& AfterOperator::DefaultValue()
    {
        return Utilities::EmptyString();
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS


/// @} // addtogroup CoreGroup
