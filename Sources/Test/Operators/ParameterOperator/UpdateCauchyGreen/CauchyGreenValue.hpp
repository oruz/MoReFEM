#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS
{


    // clang-format off

    /*!
     * \brief Input data which gives the initial content at a \a QuadraturePoint of the Cauchy-Green tensor.
     */
    struct Initial
    : public ::MoReFEM::InputDataNS::Crtp::InputData
            <
                Initial,
                ::MoReFEM::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input parameter in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input parameter.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
         * text from \a LuaOptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();

        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * LuaOptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    // clang-format off


    /*!
     * \brief Input data which gives the expected result at a \a QuadraturePoint of the Cauchy-Green tensor after the arbitrary upgrade operator implemented
     * in the test is applied.
     */

    struct AfterOperator
    : public ::MoReFEM::InputDataNS::Crtp::InputData
            <
                AfterOperator,
                ::MoReFEM::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input parameter in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input parameter.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
         * text from \a LuaOptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();

        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * LuaOptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_
