/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE update_cauchy_green
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/MacroVariationalOperator.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/GlimpseCauchyGreenContent.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::UpdateCauchyGreenTensorNS::Model>;

    auto ComputeArbitraryDisplacement(TestNS::UpdateCauchyGreenTensorNS::Model& model) -> GlobalVector&;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(CheckUpdateCauchyGreen, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) content = GlimpseCauchyGreenContent(model);
    BOOST_CHECK_EQUAL(model.GetInitialValue(), content);

    auto& displacement = ComputeArbitraryDisplacement(model);

    decltype(auto) cauchy_green_tensor_operator = model.GetUpgradeOperator();

    cauchy_green_tensor_operator.Update(displacement);

    BOOST_CHECK(model.GetInitialValue() != content); // sanity check...

    decltype(auto) expected_result = model.GetExpectedValueAfterOperator();

    BOOST_CHECK_EQUAL(expected_result.size(), content.size());

    const auto size = expected_result.size();

    for (auto i = 0ul; i < size; ++i)
        BOOST_CHECK_CLOSE(content[i], expected_result[i], 1.e-13);

    // Doesn't change if reapplied:
    cauchy_green_tensor_operator.Update(displacement);

    for (auto i = 0ul; i < size; ++i)
        BOOST_CHECK_CLOSE(content[i], expected_result[i], 1.e-13);
}


namespace // anonymous
{

    auto ComputeArbitraryDisplacement(TestNS::UpdateCauchyGreenTensorNS::Model& model) -> GlobalVector&
    {
        decltype(auto) displacement = model.GetNonCstDisplacement();

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(
                displacement, __FILE__, __LINE__);

            for (auto i = 0ul; i < content.GetSize(__FILE__, __LINE__); ++i)
            {
                // Arbitrary displacement!
                content[i] = 0.12 * std::cos(i) - 0.78 * std::sin(i);
            }
        }

        return displacement;
    }


} // namespace
