add_library(MoReFEMTestUpdateCauchyGreen_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEMTestUpdateCauchyGreen_lib
    PRIVATE
                ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
                ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
                ${CMAKE_CURRENT_LIST_DIR}/CauchyGreenValue.cpp
                ${CMAKE_CURRENT_LIST_DIR}/CauchyGreenValue.hpp
)

add_executable(MoReFEMTestUpdateCauchyGreen
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               ${CMAKE_CURRENT_LIST_DIR}/GlimpseCauchyGreenContent.cpp
               ${CMAKE_CURRENT_LIST_DIR}/GlimpseCauchyGreenContent.hpp)
               
target_link_libraries(MoReFEMTestUpdateCauchyGreen
                      MoReFEMTestUpdateCauchyGreen_lib
                      ${MOREFEM_TEST_TOOLS})

add_test(UpdateCauchyGreen3D
         MoReFEMTestUpdateCauchyGreen
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}
         ${MOREFEM_ROOT}/Sources/Test/Operators/ParameterOperator/UpdateCauchyGreen/demo_3D.lua)

set_tests_properties(UpdateCauchyGreen3D PROPERTIES TIMEOUT 20)

add_test(UpdateCauchyGreen2D
         MoReFEMTestUpdateCauchyGreen
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}
         ${MOREFEM_ROOT}/Sources/Test/Operators/ParameterOperator/UpdateCauchyGreen/demo_2D.lua)

set_tests_properties(UpdateCauchyGreen2D PROPERTIES TIMEOUT 20)


add_executable(MoReFEMTestUpdateCauchyGreenTensorUpdateLuaFile ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
target_link_libraries(MoReFEMTestUpdateCauchyGreenTensorUpdateLuaFile
                      MoReFEMTestUpdateCauchyGreen_lib
                      ${MOREFEM_MODEL})
                      
