/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    namespace // anonymous
    {

        //! \tparam WhichT Either InputDataNS::CauchyGreenValueNS::Initial or InputDataNS::CauchyGreenValueNS::AfterOperator.
        template<class WhichT, class InputDataT>
        LocalVector ReadCauchyGreenValue(const InputDataT& input_data);


    } // namespace


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

        decltype(auto) input_data = GetMoReFEMData().GetInputData();

        initial_value_ = ReadCauchyGreenValue<InputDataNS::CauchyGreenValueNS::Initial>(input_data);

        expected_value_ = ReadCauchyGreenValue<InputDataNS::CauchyGreenValueNS::AfterOperator>(input_data);

        decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

        decltype(auto) time_manager = parent::GetTimeManager();

        cauchy_green_tensor_ = std::make_unique<cauchy_green_tensor_type>(
            "Cauchy-Green tensor", felt_space.GetDomain(), quadrature_rule_per_topology, initial_value_, time_manager);
        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) unknown = unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::generic_unknown));

        upgrade_operator_ = std::make_unique<GlobalParameterOperatorNS::UpdateCauchyGreenTensor>(
            felt_space, unknown, *cauchy_green_tensor_, &quadrature_rule_per_topology);

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::generic_numbering_subset));

        displacement_ = std::make_unique<GlobalVector>(numbering_subset);
        AllocateGlobalVector(god_of_dof, *displacement_);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


    namespace // anonymous
    {

        template<class WhichT, class InputDataT>
        LocalVector ReadCauchyGreenValue(const InputDataT& input_data)
        {
            const auto value = Utilities::InputDataNS::Extract<WhichT>::Value(input_data);

            LocalVector ret;
            ret.resize({ value.size() });

            std::copy(value.cbegin(), value.cend(), ret.begin());

            return ret;
        }


    } // namespace


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
