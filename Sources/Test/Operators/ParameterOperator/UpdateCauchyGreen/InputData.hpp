/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/CauchyGreenValue.hpp"


namespace MoReFEM
{


    namespace TestNS::UpdateCauchyGreenTensorNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            domain = 1u
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            felt_space = 1u
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            generic_unknown = 1u
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            generic_numbering_subset = 1u
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1u
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            ::MoReFEM::InputDataNS::TimeManager,

            ::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_numbering_subset)>,

            ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_unknown)>,

            ::MoReFEM::InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

            ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,
        
            InputDataNS::CauchyGreenValueNS::Initial,
            InputDataNS::CauchyGreenValueNS::AfterOperator,

            ::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            ::MoReFEM::InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNS::UpdateCauchyGreenTensorNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_INPUT_DATA_HPP_
