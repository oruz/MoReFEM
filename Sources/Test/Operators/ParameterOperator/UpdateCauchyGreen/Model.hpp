/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

        //! Alias.
        using cauchy_green_tensor_type =
            ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>;

        //! Alias
        using glimpse_cauchy_green_type = cauchy_green_tensor_type::value_holder_type::type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_param
         */
        Model(const morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}


        /*!
         * \brief Non constant accessor tio displacement.
         */
        GlobalVector& GetNonCstDisplacement() noexcept;

        //! Accessor to upgrade operator.
        const GlobalParameterOperatorNS::UpdateCauchyGreenTensor& GetUpgradeOperator() const noexcept;

        //! Accessor to Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensor() const noexcept;

        //! Accessor to Cauchy-Green tensor underlying storage.
        auto GetCauchyGreenTensorStorage() const noexcept -> const cauchy_green_tensor_type::storage_type&;


        /*!
         * \brief Access Cauchy-Green initial value.
         *
         * \return Initial value.
         */
        const LocalVector& GetInitialValue() const noexcept;

        /*!
         * \brief Access Cauchy-Green expected value after operator is applied (assuming the current arbitrary displacement vector computed in main file).
         *
         * \return Expected value.
         */
        const LocalVector& GetExpectedValueAfterOperator() const noexcept;


      private:
        //! Accessor to the directory object which holds the main output directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

      private:
        //! Cauchy-Green tensor.
        typename cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_{ nullptr };

        //! Cauchy-green initial value.
        LocalVector initial_value_;

        //! Cauchy-green expected value after operator is applied.
        LocalVector expected_value_;

        //! Update operator
        GlobalParameterOperatorNS::UpdateCauchyGreenTensor::const_unique_ptr upgrade_operator_{ nullptr };

        //! Displacement vector used to upgrade the Cauchy-Green tensor.
        GlobalVector::unique_ptr displacement_{ nullptr };
    };


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HPP_
