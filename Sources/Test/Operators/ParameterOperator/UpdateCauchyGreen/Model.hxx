/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test UpdateCauchyGreen operator");
        return name;
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    inline void Model::SupplInitializeStep()
    { }


    inline const FilesystemNS::Directory& Model::GetOutputDirectory() const noexcept
    {
        return GetMoReFEMData().GetResultDirectory();
    }


    inline GlobalVector& Model::GetNonCstDisplacement() noexcept
    {
        assert(!(!displacement_));
        return *displacement_;
    }


    inline auto Model::GetCauchyGreenTensor() const noexcept -> const cauchy_green_tensor_type&
    {
        assert(!(!cauchy_green_tensor_));
        return *cauchy_green_tensor_;
    }


    inline auto Model::GetCauchyGreenTensorStorage() const noexcept -> const cauchy_green_tensor_type::storage_type&
    {
        return GetCauchyGreenTensor().GetStorage();
    }


    inline const GlobalParameterOperatorNS::UpdateCauchyGreenTensor& Model::GetUpgradeOperator() const noexcept
    {
        assert(!(!upgrade_operator_));
        return *upgrade_operator_;
    }


    inline const LocalVector& Model::GetInitialValue() const noexcept
    {
        return initial_value_;
    }


    inline const LocalVector& Model::GetExpectedValueAfterOperator() const noexcept
    {
        return expected_value_;
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_MODEL_HXX_
