/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_PTS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_PTS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/Model.hpp"


namespace MoReFEM::TestNS::GlobalCoordsQuadPt
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test GlobalCoordsQuadPts operator");
        return name;
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    inline void Model::SupplInitializeStep()
    { }


    inline const FilesystemNS::Directory& Model::GetOutputDirectory() const noexcept
    {
        return GetMoReFEMData().GetResultDirectory();
    }


} // namespace MoReFEM::TestNS::GlobalCoordsQuadPt


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_PTS_x_MODEL_HXX_
