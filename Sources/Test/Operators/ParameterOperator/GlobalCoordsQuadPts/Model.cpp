/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/Model.hpp"


namespace MoReFEM::TestNS::GlobalCoordsQuadPt
{


    namespace // anonymous
    {


        void CheckQuadratureRule(const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                 const ParameterAtQuadraturePoint<ParameterNS::Type::vector>& global_coords,
                                 const FilesystemNS::Directory& result_directory_path,
                                 std::string_view quadrature_order);


    } // namespace


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.Nprocessor<int>() > 1)
        {
            throw Exception("As there is only one finite element in each of these tests, "
                            "running in parallel is pointless.",
                            __FILE__,
                            __LINE__);
        }
    }


    void Model::SupplInitialize()
    {
        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    void Model::CheckQuadrature(const QuadratureRulePerTopology* const quadrature_rule,
                                std::string_view quadrature_order) const
    {
        decltype(auto) god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

        decltype(auto) time_manager = GetTimeManager();

        namespace GPO = GlobalParameterOperatorNS;
        decltype(auto) generic_unknown = UnknownManager::GetInstance(__FILE__, __LINE__)
                                             .GetUnknown(EnumUnderlyingType(UnknownIndex::generic_unknown));


        LocalVector init_vector;
        constexpr const std::size_t spatial_point_size = 3;
        init_vector.resize({ spatial_point_size });
        init_vector.fill(0.);

        decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                    .GetDomain(EnumUnderlyingType(DomainIndex::domain), __FILE__, __LINE__);

        ParameterAtQuadraturePoint<ParameterNS::Type::vector> global_coords_quad_pt(
            "Global coords of quad points - Low degree", domain, *quadrature_rule, init_vector, time_manager);

        namespace GPO = GlobalParameterOperatorNS;

        GPO::GlobalCoordsQuadPoints write_at_quad_pt_operator(
            felt_space, generic_unknown, quadrature_rule, global_coords_quad_pt);

        write_at_quad_pt_operator.Update();


        CheckQuadratureRule(*quadrature_rule, global_coords_quad_pt, GetOutputDirectory(), quadrature_order);
    }


    void Model::CheckLowOrderQuadrature() const
    {
        auto rule = std::make_unique<const QuadratureRulePerTopology>(1, 1);
        CheckQuadrature(rule.get(), "low_order_global_coords.txt");
    }


    void Model::CheckMediumOrderQuadrature() const
    {
        auto rule = std::make_unique<const QuadratureRulePerTopology>(3, 3);
        CheckQuadrature(rule.get(), "medium_order_global_coords.txt");
    }


    void Model::CheckHighOrderQuadrature() const
    {
        auto rule = std::make_unique<const QuadratureRulePerTopology>(5, 5);
        CheckQuadrature(rule.get(), "high_order_global_coords.txt");
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


    namespace // anonymous
    {


        /*!
         * \brief Compute analytically  the \a GlobalCoords .
         */
        std::vector<double> FillExpectedGlobalCoords(const QuadratureRule& quadrature_rule)
        {
            decltype(auto) quadrature_point_list = quadrature_rule.GetQuadraturePointList();
            const auto topology_identifier = quadrature_rule.GetTopologyIdentifier();
            std::vector<double> ret;
            for (const auto& it_quad_pt : quadrature_point_list)
            {
                assert(!(!it_quad_pt));
                const auto& vec_values = it_quad_pt->GetCoordinates();
                for (const auto local_coords : vec_values)
                {
                    double expected_value;

                    switch (topology_identifier)
                    {
                    case RefGeomEltNS::TopologyNS::Type::triangle:
                    case RefGeomEltNS::TopologyNS::Type::tetrahedron:
                    {
                        // Integration interval is already defined on [0., 1.]^n for these topologies.
                        expected_value = local_coords;
                        break;
                    }
                    case RefGeomEltNS::TopologyNS::Type::point:
                    case RefGeomEltNS::TopologyNS::Type::segment:
                    case RefGeomEltNS::TopologyNS::Type::quadrangle:
                    case RefGeomEltNS::TopologyNS::Type::hexahedron:
                    {
                        // Integration interval is on [-1., 1.], this maps the values to the [0., 1.] interval.
                        expected_value = (local_coords + 1.) * .5;
                        break;
                    }
                    }

                    ret.push_back(expected_value);
                }

                // Set to 0 global coords of the dimensions that are not relevant as local coords of the quadrature
                // have the same number of components as the dimension of the finite element considered.
                const auto Ndimension = it_quad_pt->GetDimension();
                for (auto i = Ndimension; i < 3; ++i)
                    ret.push_back(0.);
            }

            return ret;
        }


        /*!
         * \brief Convert the obtained global coords into a vector of double which is filled with same ordering as the
         * expected ones.
         *
         */
        std::vector<double> ConvertObtainedGloballCoords(
            const FilesystemNS::Directory& result_directory_path,
            std::string_view quadrature_order,
            const ParameterAtQuadraturePoint<ParameterNS::Type::vector>& obtained_global_coords)
        {
            const std::string output_file = result_directory_path.AddFile(quadrature_order);
            obtained_global_coords.Write(output_file);

            std::ifstream file_stream;
            FilesystemNS::File::Read(file_stream, output_file, __FILE__, __LINE__);
            std::string line;

            // skip first two lines
            for (int i = 0; i < 2; ++i)
                getline(file_stream, line);

            std::vector<double> ret;

            while (getline(file_stream, line))
            {
                const auto item_list = Utilities::String::Split(line, ";");

                BOOST_CHECK_EQUAL(item_list.size(), 4ul);

                const auto coords_list = std::string(item_list.back()); // istringstream can't act upon a
                                                                        // std::string_view hence this copy.
                std::istringstream iconv(coords_list);
                double value;
                while (iconv >> value)
                    ret.push_back(value);
            }

            return ret;
        }


        void CheckQuadratureRule(const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                 const ParameterAtQuadraturePoint<ParameterNS::Type::vector>& obtained_global_coords,
                                 const FilesystemNS::Directory& result_directory_path,
                                 std::string_view filename)
        {
            decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                        .GetDomain(EnumUnderlyingType(DomainIndex::domain), __FILE__, __LINE__);

            decltype(auto) mesh = domain.GetMesh();

            BOOST_CHECK_EQUAL(domain.GetDimensionList().size(), 1ul);

            // For this test, we intend to work on a simple mesh with a single element and we are doing the
            // quadrature points computation only on the highest dimension.
            decltype(auto) ref_geom_elt_list =
                mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>(domain.GetDimensionList().back());
            BOOST_CHECK_EQUAL(ref_geom_elt_list.size(), 1ul);

            BOOST_CHECK(ref_geom_elt_list.back() != nullptr);

            const auto& ref_geom_elt = *(ref_geom_elt_list.back());
            BOOST_CHECK(domain.DoRefGeomEltMatchCriteria(ref_geom_elt));

            const auto topology_identifier = ref_geom_elt.GetTopologyIdentifier();
            const auto& quadrature_rule = quadrature_rule_per_topology.GetRule(topology_identifier);

            const auto expected_global_coords = FillExpectedGlobalCoords(quadrature_rule);

            const auto obtained_global_coords_as_vector =
                ConvertObtainedGloballCoords(result_directory_path, filename, obtained_global_coords);

            const auto end_obtained = obtained_global_coords_as_vector.cend();

            BOOST_CHECK_EQUAL(obtained_global_coords_as_vector.size(), expected_global_coords.size());

            constexpr const auto precision = 1.e-12;

            for (auto it_obtained = obtained_global_coords_as_vector.cbegin(),
                      it_expected = expected_global_coords.cbegin();
                 it_obtained != end_obtained;
                 ++it_obtained, ++it_expected)
            {
                const auto obtained_value = *it_obtained;
                const auto expected_value = *it_expected;

                // Takes into account the precision at which we wrote the quadrature point coordinates.
                BOOST_CHECK_CLOSE(expected_value, obtained_value, precision);
            }
        }
    } // namespace


} // namespace MoReFEM::TestNS::GlobalCoordsQuadPt
