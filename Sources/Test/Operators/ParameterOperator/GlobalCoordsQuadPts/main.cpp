/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE global_coords_quad_pt_1d_edge
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/MacroVariationalOperator.hpp"

#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/InputData.hpp"
#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::GlobalCoordsQuadPt::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_SUITE(CheckQuadPtCoords, fixture_type)

BOOST_AUTO_TEST_CASE(lower_order)
{
    GetModel().CheckLowOrderQuadrature();
}

BOOST_AUTO_TEST_CASE(medium_order)
{
    GetModel().CheckMediumOrderQuadrature();
}

BOOST_AUTO_TEST_CASE(high_order)
{
    GetModel().CheckHighOrderQuadrature();
}

BOOST_AUTO_TEST_SUITE_END()
