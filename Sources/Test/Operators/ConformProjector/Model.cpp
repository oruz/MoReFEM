/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Core/InputData/Instances/Result.hpp"

#include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hpp"

#include "Test/Operators/ConformProjector/Model.hpp"


namespace MoReFEM
{


    namespace ConformProjectorNS
    {


        namespace // anonymous
        {


            void WriteInterpolatorMatrix(const ConformInterpolatorNS::SubsetOrSuperset& interpolator,
                                         const std::string& output_directory,
                                         const Wrappers::Mpi& mpi)
            {

                const auto& matrix = interpolator.GetInterpolationMatrix();

                std::ostringstream oconv;
                oconv << output_directory << "/projection_"
                      << interpolator.GetInterpolationMatrix().GetColNumberingSubset().GetUniqueId() << '_'
                      << interpolator.GetInterpolationMatrix().GetRowNumberingSubset().GetUniqueId() << ".hhdata";

                std::string output_file(oconv.str());

                if (mpi.IsRootProcessor())
                {
                    if (FilesystemNS::File::DoExist(output_file))
                        FilesystemNS::File::Remove(output_file, __FILE__, __LINE__);
                }

                mpi.Barrier();

                matrix.View(mpi, output_file, __FILE__, __LINE__);
            }


            void WriteConformInterpolatorMatrix(const FEltSpace& orig_felt_space,
                                                const FEltSpace& target_felt_space,
                                                const std::string& output_directory,
                                                const Wrappers::Mpi& mpi)
            {
                const auto& orig_numbering_subset_list = orig_felt_space.GetNumberingSubsetList();
                assert(orig_numbering_subset_list.size() == 1ul);

                const auto& target_numbering_subset_list = target_felt_space.GetNumberingSubsetList();
                assert(target_numbering_subset_list.size() == 1ul);

                const auto& orig_numbering_subset = *orig_numbering_subset_list.back();
                const auto& target_numbering_subset = *target_numbering_subset_list.back();

                ConformInterpolatorNS::SubsetOrSuperset interpolator12(
                    orig_felt_space, orig_numbering_subset, target_felt_space, target_numbering_subset);

                interpolator12.Init();

                const auto& matrix = interpolator12.GetInterpolationMatrix();

                std::ostringstream oconv;
                oconv << output_directory << "/conform_projection_" << orig_numbering_subset.GetUniqueId() << '_'
                      << target_numbering_subset.GetUniqueId() << ".hhdata";

                std::string output_file(oconv.str());

                if (mpi.IsRootProcessor())
                {
                    if (FilesystemNS::File::DoExist(output_file))
                        FilesystemNS::File::Remove(output_file, __FILE__, __LINE__);
                }

                mpi.Barrier();

                matrix.View(mpi, output_file, __FILE__, __LINE__);
            }


        } // namespace


        Model::Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {

            void SupplInitialize();

            using interpolator = ConformInterpolatorNS::SubsetOrSuperset;

            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            const auto& felt_space1 = god_of_dof.GetFEltSpace(1);
            const auto& felt_space2 = god_of_dof.GetFEltSpace(2);
            const auto& felt_space3 = god_of_dof.GetFEltSpace(3);

            decltype(auto) numbering_subset_manager =
                Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance(__FILE__, __LINE__);

            const auto& numbering_subset1 = numbering_subset_manager.GetNumberingSubset(1);
            const auto& numbering_subset2 = numbering_subset_manager.GetNumberingSubset(2);
            const auto& numbering_subset3 = numbering_subset_manager.GetNumberingSubset(3);

            const auto& output_directory = GetOutputDirectory();
            const auto& mpi = GetMpi();

            WriteConformInterpolatorMatrix(felt_space1, felt_space2, output_directory, mpi);
            // ------------
            // 1 -> 2
            // ------------
            interpolator_1_2_ =
                std::make_unique<interpolator>(felt_space1, numbering_subset1, felt_space2, numbering_subset2);

            interpolator_1_2_->Init();


            WriteInterpolatorMatrix(*interpolator_1_2_, output_directory, mpi);

            WriteConformInterpolatorMatrix(felt_space1, felt_space2, output_directory, mpi);


            // ------------
            // 2 -> 1
            // ------------
            interpolator_2_1_ =
                std::make_unique<interpolator>(felt_space2, numbering_subset2, felt_space1, numbering_subset1);

            interpolator_2_1_->Init();


            WriteInterpolatorMatrix(*interpolator_2_1_, output_directory, mpi);

            WriteConformInterpolatorMatrix(felt_space2, felt_space1, output_directory, mpi);

            // ------------
            // 1 -> 3
            // ------------
            interpolator_1_3_ =
                std::make_unique<interpolator>(felt_space1, numbering_subset1, felt_space3, numbering_subset3);

            interpolator_1_3_->Init();


            WriteInterpolatorMatrix(*interpolator_1_3_, output_directory, mpi);

            WriteConformInterpolatorMatrix(felt_space1, felt_space3, output_directory, mpi);

            // ------------
            // 3 -> 1
            // ------------
            interpolator_3_1_ =
                std::make_unique<interpolator>(felt_space3, numbering_subset3, felt_space1, numbering_subset1);

            interpolator_3_1_->Init();


            WriteInterpolatorMatrix(*interpolator_3_1_, output_directory, mpi);

            WriteConformInterpolatorMatrix(felt_space3, felt_space1, output_directory, mpi);


            // This one should fail!

            //            WriteConformInterpolatorMatrix(felt_space2,
            //                                           felt_space3,
            //                                           output_directory,
            //                                           mpi);
            //
            //
            //            interpolator_2_3_ = std::make_unique<interpolator>(felt_space2,
            //                                                               numbering_subset2,
            //                                                               felt_space3,
            //                                                               numbering_subset3);
            //
            //            interpolator_2_3_->Init();
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace ConformProjectorNS


} // namespace MoReFEM
