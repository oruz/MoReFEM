/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 21:38:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================0


    namespace TestFunctionsNS
    {


        /*!
         * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
         * \a IsMatrixOrVectorT template parameter.
         *
         * \tparam IsMatrixOrVectorT Whether matrix or vector is considered.
         */
        // clang-format off
        template<IsMatrixOrVector IsMatrixOrVectorT>
        using content_type =
            std::conditional_t
            <
                IsMatrixOrVectorT == IsMatrixOrVector::matrix,
                std::vector<std::vector<PetscScalar>>,
                std::vector<PetscScalar>
            >;
        // clang-format on


        /*!
         * \brief Convenient alias to define the associative container which stores all expected results (key is a
         * unique keyword to identify which is which).
         *
         * \tparam LinAlgT Whether matrix or vector is considered.
         */
        template<IsMatrixOrVector LinAlgT>
        using expected_results_type = std::unordered_map<std::string, content_type<LinAlgT>>;

        /*!
         * \class doxygen_hide_GetExpectedVectorialResults_common
         *
         * \param[in] is_parallel Whether the vectorial expected results were obtained in sequential or in
         * parallel. Due to parallel renumbering, positioning of values might not be the same.
         *
         * \return The expected results for all vectorial quantities to check (key is a unique string identifier,
         * value the expected result stored in a std::vector).
         */


        /*!
         * \class doxygen_hide_GetExpectedMatricialResults_common
         *
         * \param[in] is_parallel Whether the matricial expected results were obtained in sequential or in
         * parallel. Due to parallel renumbering, positioning of values might not be the same.
         *
         * \return The expected results for all matricial quantities to check (key is a unique string identifier,
         * value the expected result stored in a std::vector of std::vector: first for indexing the row, and inner
         * one to store the actual values to check).
         */

        /*!
         * \brief Get the expected results for the vectorial quantities to check.
         *
         * \param[in] dimension Whether we're considering dimension 1, 2 or 3.
         * \copydoc doxygen_hide_GetExpectedVectorialResults_common
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults(std::size_t dimension,
                                                                                    bool is_parallel);

        /*!
         * \brief Get the expected results for the matricial quantities to check.
         *
         * \param[in] dimension Whether we're considering dimension 1, 2 or 3.
         * \copydoc doxygen_hide_GetExpectedMatricialResults_common
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults(std::size_t dimension,
                                                                                    bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedVectorialResults() for 1D case.
         *
         * \copydoc doxygen_hide_GetExpectedVectorialResults_common
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults1D(bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedMatricialResults() for 1D case.
         *
         * \copydoc doxygen_hide_GetExpectedMatricialResults_common
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults1D(bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedVectorialResults() for 2D case.
         *
         * \copydoc doxygen_hide_GetExpectedVectorialResults_common
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults2D(bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedMatricialResults() for 2D case.
         *
         * \copydoc doxygen_hide_GetExpectedMatricialResults_common
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults2D(bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedVectorialResults() for 3D case.
         *
         * \copydoc doxygen_hide_GetExpectedVectorialResults_common
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults3D(bool is_parallel);

        /*!
         * \brief The underlying function called by \a GetExpectedMatricialResults() for 3D case.
         *
         * \copydoc doxygen_hide_GetExpectedMatricialResults_common
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults3D(bool is_parallel);


        /*!
         * \brief Insert a new entry in the expected results storage.
         *
         * \tparam IsMatrixOrVectorT Whether the new entry is matricial or vectorial.
         *
         * \param[in] name Identifier of the new entry; must be unique in the storage.
         * \param[in] content The associated numerical values of the new entry.
         * \param[in,out] expected_results The storage in which all entries are stored. New one is added in output
         * of this function.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        template<IsMatrixOrVector IsMatrixOrVectorT>
        void InsertNewEntry(const std::string& name,
                            content_type<IsMatrixOrVectorT>&& content,
                            expected_results_type<IsMatrixOrVectorT>& expected_results,
                            const char* invoking_file,
                            int invoking_line);


        /*!
         * \brief Check whether the matrix obtained is the same as the one stored in expected result storage.
         *
         * \param[in] god_of_dof \a GodOfDof upon which the matrix is defined. It is requested to retrieve
         * informations related to numbering.
         * \param[in] name Identifier of the entry to check.
         * \param[in] obtained Value obtained by the computation; the content must match the expected one.
         * \param[in] expected_results The storage in which all matricial entries are stored.
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] epsilon Value under which two floating point quantities are consider equal.
         */
        void CheckMatrix(const GodOfDof& god_of_dof,
                         const GlobalMatrix& obtained,
                         const expected_results_type<IsMatrixOrVector::matrix>& expected_results,
                         std::string&& name,
                         const char* invoking_file,
                         int invoking_line,
                         const double epsilon = 1.e-12);


        /*!
         * \brief Check whether the vector obtained is the same as the one stored in expected result storage.
         *
         * \param[in] god_of_dof \a GodOfDof upon which the vector is defined. It is requested to retrieve
         * informations related to numbering.
         * \param[in] name Identifier of the entry to check.
         * \param[in] obtained Value obtained by the computation; the content must match the expected one.
         * \param[in] expected_results The storage in which all vectorial entries are stored.
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] epsilon Value under which two floating point quantities are consider equal.
         */
        void CheckVector(const GodOfDof& god_of_dof,
                         const GlobalVector& obtained,
                         const expected_results_type<IsMatrixOrVector::vector>& expected_results,
                         std::string&& name,
                         const char* invoking_file,
                         int invoking_line,
                         const double epsilon = 1.e-12);


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#include "Test/Operators/TestFunctions/ExpectedResults.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HPP_
