/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/TestFunctions/Model.hpp"


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("TestFunctions");
            return name;
        }

        inline const VariationalFormulation& Model::GetVariationalFormulation() const
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
        {
            return const_cast<VariationalFormulation&>(GetVariationalFormulation());
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_MODEL_HXX_
