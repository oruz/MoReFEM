/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE test_function
#include "Test/Tools/InitializeTestMoReFEMData.hpp"
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/TestFunctions/InputData.hpp"
#include "Test/Operators/TestFunctions/Model.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(test_function)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    decltype(auto) input_file = environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/TestFunctions/"
                                                             "demo_3D.lua");

    using InputData = TestFunctionsNS::InputData;

    TestNS::InitializeTestMoReFEMData<InputData> helper(std::move(input_file));

    decltype(auto) morefem_data = helper.GetMoReFEMData();

    TestFunctionsNS::Model model(morefem_data);

    model.Run();
}

PRAGMA_DIAGNOSTIC(pop)
