/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 16 Sep 2017 22:22:57 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Test/Operators/TestFunctions/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults1D(bool is_parallel)
        {
            static_cast<void>(is_parallel); // For 1D case matrices are identical in both sequential and
                                            // parallel case.

            expected_results_type<IsMatrixOrVector::matrix> expected_results;

            constexpr double one_3rd = 1. / 3.;
            constexpr double one_6th = 1. / 6.;

            using content_type = content_type<IsMatrixOrVector::matrix>;


            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1",
                                                     content_type{
                                                         { 1., -1. },
                                                         { -1., 1. },
                                                     },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "stiffness_operator_potential_1_potential_1",
                content_type{ { 1., 0., -1., 0. }, { 0., 0., 0., 0. }, { -1., 0., 1., 0. }, { 0., 0., 0., 0. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "stiffness_operator_potential_1_potential_2",
                content_type{ { 1., 0., -1., 0. }, { 1., 0., -1., 0. }, { -1., 0., 1., 0. }, { -1., 0., 1., 0. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "stiffness_operator_potential_2_potential_1",
                content_type{ { 1., 1., -1., -1. }, { 1., 0., -1., 0. }, { -1., -1., 1., 1. }, { -1., 0., 1., 0. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "stiffness_operator_potential_2_potential_2",
                content_type{ { 1., 1., -1., -1. }, { 1., 1., -1., -1. }, { -1., -1., 1., 1. }, { -1., -1., 1., 1. } },
                expected_results,
                __FILE__,
                __LINE__);

            constexpr double eight_3rd = 8. * one_3rd;

            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_3",
                                                     content_type{
                                                         { 7. * one_3rd, one_3rd, -eight_3rd },
                                                         { one_3rd, 7. * one_3rd, -eight_3rd },
                                                         { -eight_3rd, -eight_3rd, 16. * one_3rd },
                                                     },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_3",
                                                     content_type{ { 1., -1., 0. }, { -1., 1., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("variable_mass_operator_potential_1",
                                                     content_type{ { one_3rd, one_6th }, { one_6th, one_3rd } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "bidomain_operator",
                content_type{ { 2., 2., -2., -2. }, { 2., 4., -2., -4. }, { -2., -2., 2., 2. }, { -2., -4., 2., 4. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_potential_124_operator",
                                                     content_type{ { 2., 2., 0., -2., -2., 0. },
                                                                   { 2., 4., 0., -2., -4., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. },
                                                                   { -2., -2., 0., 2., 2., 0. },
                                                                   { -2., -4., 0., 2., 4., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("grad_phi_tau_tau_grad_phi_operator",
                                                     content_type{ { 2., -2. }, { -2., 2. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            constexpr double five_sixth = 5. * one_6th;

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "ale_operator_2",
                content_type{ { -one_3rd, -2. * one_3rd }, { five_sixth, 7 * one_6th } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("pk2_operator",
                                                     content_type{ { 1.01224, -1.01224 }, { -1.01224, 1.01224 } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("scalar_div_vectorial_operator",
                                                     content_type{ { 0., 0., -3. }, { 0., 0., 3. }, { -2., 2., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("stokes_operator",
                                                     content_type{ { 1, -1., 1. }, { -1., 1., -1. }, { 1., -1., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "elasticity_operator", content_type{ { 1., -1. }, { -1., 1. } }, expected_results, __FILE__, __LINE__);


            InsertNewEntry<IsMatrixOrVector::matrix>(
                "ale_operator_1", content_type{ { -0.5, -0.5 }, { 0.5, 0.5 } }, expected_results, __FILE__, __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "variable_mass_operator_potential_1_non_symmetric",
                content_type{ { one_6th, 0. }, { 0., one_6th }, { one_3rd, one_3rd } },
                expected_results,
                __FILE__,
                __LINE__);


            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_operator_non_symmetric",
                                                     content_type{ { 2., 2., -2., -2. },
                                                                   { 2., 4., -2., -4. },
                                                                   { -2., -2., 2., 2. },
                                                                   { -2., -4., 2., 4. },
                                                                   { 0., 0., 0., 0. },
                                                                   { 0., 0., 0., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_potential_124_operator_non_symmetric",
                                                     content_type{ { 2., 2., 0., -2., -2., 0. },
                                                                   { 2., 4., 0., -2., -4., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. },
                                                                   { -2., -2., 0., 2., 2., 0. },
                                                                   { -2., -4., 0., 2., 4., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. },
                                                                   { 0., 0., 0., 0., 0., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("grad_phi_tau_tau_grad_phi_operator_non_symmetric",
                                                     content_type{ { 2., -2. }, { -2., 2. }, { 0., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "scalar_div_vectorial_operator_non_symmetric",
                content_type{ { -1., 1., 0. }, { 0., 0., -3. }, { -1., 1., 0. }, { 0., 0., 3. }, { 0., 0., 0. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "stokes_operator_non_symmetric",
                content_type{ { 0.5, -0.5, 0. }, { 1., -1., 1. }, { 0.5, -0.5, 0. }, { -1., 1., -1. }, { 0., 0., 0. } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>("elasticity_operator_non_symmetric",
                                                     content_type{ { 1., -1. }, { -1., 1. }, { 0., 0. } },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "ale_operator_1_non_symmetric",
                content_type{ { -0.833333, -0.166667 }, { 0.166667, 0.833333 }, { 0.666667, -0.666667 } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "ale_operator_2_non_symmetric",
                content_type{ { -0.833333, -0.166667 }, { 0.166667, 0.833333 }, { 0.666667, -0.666667 } },
                expected_results,
                __FILE__,
                __LINE__);

            InsertNewEntry<IsMatrixOrVector::matrix>(
                "pk2_operator_non_symmetric",
                content_type{ { 0.931017, -0.931017 }, { -0.723218, 0.723218 }, { -0.207798, 0.207798 } },
                expected_results,
                __FILE__,
                __LINE__);


            return expected_results;
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults1D(bool is_parallel)
        {
            static_cast<void>(is_parallel); // For 1D case vectors are identical in both sequential and
                                            // parallel case.

            expected_results_type<IsMatrixOrVector::vector> expected_results;

            InsertNewEntry<IsMatrixOrVector::vector>(
                "source_operator_potential_1", { .5, .5 }, expected_results, __FILE__, __LINE__);

            InsertNewEntry<IsMatrixOrVector::vector>(
                "source_operator_potential_1_potential_1", { .5, 0., .5, 0. }, expected_results, __FILE__, __LINE__);

            InsertNewEntry<IsMatrixOrVector::vector>(
                "source_operator_potential_1_potential_2", { .5, .5, .5, .5 }, expected_results, __FILE__, __LINE__);

            InsertNewEntry<IsMatrixOrVector::vector>(
                "pk2_operator", { -2.55362, 2.55362 }, expected_results, __FILE__, __LINE__);

            constexpr double one_6th = 1. / 6.;
            constexpr double two_3rd = 2. / 3.;

            InsertNewEntry<IsMatrixOrVector::vector>(
                "source_operator_potential_3", { one_6th, one_6th, two_3rd }, expected_results, __FILE__, __LINE__);

            constexpr double one_twelfth = 1. / 12.;

            InsertNewEntry<IsMatrixOrVector::vector>(
                "non_linear_source_operator", { -one_twelfth, -one_twelfth }, expected_results, __FILE__, __LINE__);

            InsertNewEntry<IsMatrixOrVector::vector>("non_linear_source_operator_non_symmetric",
                                                     { -0.0277778, -0.0277778, -0.111111 },
                                                     expected_results,
                                                     __FILE__,
                                                     __LINE__);

            InsertNewEntry<IsMatrixOrVector::vector>(
                "pk2_operator_non_symmetric", { -2.57246, 1.63643, 0.936027 }, expected_results, __FILE__, __LINE__);

            return expected_results;
        }


    } // namespace TestFunctionsNS


} // namespace MoReFEM
