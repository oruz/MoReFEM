/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            volume = 1,
            surface = 2,
            full_mesh = 3
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            volume_potential_1_potential_2 = 1,
            volume_potential_1 = 2,
            volume_potential_2 = 3,
            volume_potential_3 = 4,
            volume_displacement_potential_1 = 5,
            volume_displacement = 6,
            volume_potential_1_potential_2_potential_4 = 7,
            surface_potential_1 = 8,
            surface_displacement = 9,
            surface_potential_1_potential_2 = 10
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            potential_1 = 1,
            potential_2 = 2,
            potential_3 = 3,
            potential_4 = 4,
            displacement = 5,

            potential_test_1 = 11,
            potential_test_2 = 12,
            potential_test_3 = 13,
            potential_test_4 = 14,
            displacement_test = 15,
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            potential_1_potential_2 = 1,
            potential_1 = 2,
            potential_2 = 3,
            potential_3 = 4,
            displacement_potential_1 = 5,
            displacement = 6,
            potential_1_potential_2_potential_4 = 7,
            P1_potential_1_P2_potential_2 = 8,


            potential_1_potential_2_test = 11,
            potential_1_test = 12,
            potential_2_test = 13,
            potential_3_test = 14,
            displacement_potential_1_test = 15,
            displacement_test = 16,
            potential_1_potential_2_potential_4_test = 17,
            P1_potential_1_P2_potential_2_test = 18
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fibers_in_volume = 1,
            fibers_on_surface = 2,
            angles = 3
        };


        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : std::size_t
        {
            source = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_2)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_3)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_2_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_3_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4_test)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2_test)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_2)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_3)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_4)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_test_1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_test_2)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_test_3)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_test_4)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_test)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::surface)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_2)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_3)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2_potential_4)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_potential_1)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_displacement)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_potential_1_potential_2)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fibers_in_volume),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >,
            InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fibers_on_surface),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >,
            InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::angles),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::scalar
            >,

            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::source)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::LameLambda,
            InputDataNS::Solid::LameMu,
            InputDataNS::Solid::Viscosity,
            InputDataNS::Solid::PlaneStressStrain,

            InputDataNS::Fluid::Viscosity,

            InputDataNS::ReactionNS::MitchellSchaeffer,
            InputDataNS::InitialConditionGate,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_DATA_HPP_
