/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HPP_

#include <memory>
#include <vector>

#include "Utilities/InputData/LuaFunction.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Ale.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauOrthoTauGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauTauGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/SurfacicBidomain.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/VariableMass.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/NonLinearSource.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/FollowingPressure.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "Test/Operators/TestFunctions/InputData.hpp"


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        //! \copydoc doxygen_hide_varf_4_test
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>,
          public FormulationSolverNS::HyperelasticLaw<VariationalFormulation, HyperelasticLawNS::CiarletGeymonat>
        {
          private:
            //! \copydoc doxygen_hide_alias_self
            using self = VariationalFormulation;

            //! Alias to the parent class.
            using parent =
                MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Alias to the non linear source type.
            using non_linear_source_operator_type = GlobalVariationalOperatorNS::NonLinearSource<
                Advanced::ReactionLawNS ::ReactionLawName::MitchellSchaeffer>;

            //! Alias to the reaction law type.
            using reaction_law_type = typename non_linear_source_operator_type::reaction_law_type;

            //! Alias to hyperlastic law parent,
            using hyperelastic_law_parent =
                FormulationSolverNS::HyperelasticLaw<VariationalFormulation, HyperelasticLawNS::CiarletGeymonat>;

            //! Alias to the viscoelasticity policy used.
            using ViscoelasticityPolicyNone =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None;

            //! Alias
            using visco_derivate = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
                ViscoelasticityPolicyNS::DerivatesWithRespectTo;

            //! Alias to the viscoelasticity policy used.
            using ViscoelasticityPolicy = GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                ViscoelasticityPolicyNS ::Viscoelasticity<visco_derivate::displacement_and_velocity>;

            //! Alias to the active stress policy used.
            using InternalVariablePolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None;

            //! Alias to the hyperelasticity policy used.
            using hyperelasticity_policy = GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                HyperelasticityPolicyNS ::Hyperelasticity<typename hyperelastic_law_parent::hyperelastic_law_type>;

            //! Alias to the stiffness operator type used.
            using StiffnessOperatorType =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor<hyperelasticity_policy,
                                                                              ViscoelasticityPolicyNone,
                                                                              InternalVariablePolicy>;

            //!  Alias to the stiffness operator type used with viscoelasticity.
            using ViscoOperatorType = GlobalVariationalOperatorNS::
                SecondPiolaKirchhoffStressTensor<hyperelasticity_policy, ViscoelasticityPolicy, InternalVariablePolicy>;

          public:
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to source operator type.
            using source_operator_type = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar>;

            //! Alias to source parameter type.
            using source_parameter_type = ScalarParameter<>;

            //! Alias to variable mass type.
            using variable_mass_type = ::MoReFEM::GlobalVariationalOperatorNS::VariableMass<ScalarParameter<>>;

            //! Strong type for displacement global vectors.
            using ConstRefDisplacementGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

            //! Strong type for velocity global vectors.
            using ConstRefVelocityGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::VelocityTag>;

          public:
            /// \name Special members.
            ///@{

            //! \copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() override;

            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulation(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulation(VariationalFormulation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

            ///@}

          private:
            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const morefem_data_type& morefem_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}

          private:
            /*!
             * \brief Assemble method for all the static operators.
             */
            void AssembleStaticOperators();

          private:
            /*!
             * \brief Define the properties of all the global variational operators involved.
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            void DefineOperators(const InputData& input_data);

          private:
            /// \name Global variational operators.
            ///@{


            //! Source operator on potential 1.
            source_operator_type::const_unique_ptr source_operator_potential_1_ = nullptr;

            //! Source operator on potential 1.
            source_operator_type::const_unique_ptr source_operator_potential_1_potential_1_ = nullptr;

            //! Source operator on potential 1.
            source_operator_type::const_unique_ptr source_operator_potential_1_potential_2_ = nullptr;

            //! Source operator on potential 3.
            source_operator_type::const_unique_ptr source_operator_potential_3_ = nullptr;

            //! Mass operator on potential 1.
            variable_mass_type::const_unique_ptr variable_mass_operator_potential_1_ = nullptr;

            //! Mass operator on potential 1 with test function different from unknown.
            variable_mass_type::const_unique_ptr variable_mass_operator_potential_1_non_symm_ = nullptr;

            //! Bidomain operator.
            GlobalVariationalOperatorNS::Bidomain::const_unique_ptr bidomain_operator_ = nullptr;

            //! Bidomain operator with test function different from unknown.
            GlobalVariationalOperatorNS::Bidomain::const_unique_ptr bidomain_operator_non_symm_ = nullptr;

            //! Bidomain operator.
            GlobalVariationalOperatorNS::Bidomain::const_unique_ptr bidomain_potential_124_operator_ = nullptr;

            //! Bidomain operator with test function different from unknown.
            GlobalVariationalOperatorNS::Bidomain::const_unique_ptr bidomain_potential_124_operator_non_symm_ = nullptr;

            //! SurfacicBidomain operator.
            GlobalVariationalOperatorNS::SurfacicBidomain::const_unique_ptr surfacic_bidomain_operator_ = nullptr;


            //! SurfacicBidomain operator with test function different from unknown.
            GlobalVariationalOperatorNS::SurfacicBidomain::const_unique_ptr surfacic_bidomain_operator_non_symm_ =
                nullptr;

            //! GradPhiTauTauGradPhi operator.
            GlobalVariationalOperatorNS::GradPhiTauTauGradPhi::const_unique_ptr grad_phi_tau_tau_grad_phi_operator_ =
                nullptr;

            //! GradPhiTauTauGradPhi operator with test function different from unknown.
            GlobalVariationalOperatorNS::GradPhiTauTauGradPhi::const_unique_ptr
                grad_phi_tau_tau_grad_phi_operator_non_symm_ = nullptr;

            //! GradPhiTauOrthoTauGradPhi operator.
            GlobalVariationalOperatorNS::GradPhiTauOrthoTauGradPhi::const_unique_ptr
                grad_phi_tau_ortho_tau_grad_phi_operator_ = nullptr;

            //! GradPhiTauOrthoTauGradPhi operator with test function different from unknown.
            GlobalVariationalOperatorNS::GradPhiTauOrthoTauGradPhi::const_unique_ptr
                grad_phi_tau_ortho_tau_grad_phi_operator_non_symm_ = nullptr;

            //! ScalarDivVectorial operator.
            GlobalVariationalOperatorNS::ScalarDivVectorial::const_unique_ptr scalar_div_vectorial_operator_ = nullptr;

            //! ScalarDivVectorial operator with test function different from unknown.
            GlobalVariationalOperatorNS::ScalarDivVectorial::const_unique_ptr scalar_div_vectorial_operator_non_symm_ =
                nullptr;

            //! GradOnGradientBasedElasticityTensor operator.
            GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor::const_unique_ptr elasticity_operator_ =
                nullptr;

            //! GradOnGradientBasedElasticityTensor operator with test function different from unknown.
            GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor::const_unique_ptr
                elasticity_operator_non_symm_ = nullptr;

            //! Stokes operator.
            GlobalVariationalOperatorNS::Stokes::const_unique_ptr stokes_operator_ = nullptr;

            //! Stokes operator with test function different from unknown.
            GlobalVariationalOperatorNS::Stokes::const_unique_ptr stokes_operator_non_symm_ = nullptr;

            //! Ale operator.
            GlobalVariationalOperatorNS::Ale::const_unique_ptr ale_operator_ = nullptr;

            //! Ale operator with test function different from unknown.
            GlobalVariationalOperatorNS::Ale::const_unique_ptr ale_operator_non_symm_ = nullptr;

            //! Following pressure operator.
            GlobalVariationalOperatorNS::FollowingPressure<>::const_unique_ptr following_pressure_operator_ = nullptr;

            //! Following pressure operator with test function different from unknown.
            GlobalVariationalOperatorNS::FollowingPressure<>::const_unique_ptr following_pressure_operator_non_symm_ =
                nullptr;

            //! NonLinear source operator.
            non_linear_source_operator_type::const_unique_ptr non_linear_source_operator_ = nullptr;

            //! NonLinear source operator with test function different from unknown.
            non_linear_source_operator_type::const_unique_ptr non_linear_source_operator_non_symm_ = nullptr;

            //! Reaction law.
            reaction_law_type::unique_ptr reaction_law_ = nullptr;

            //! Reaction law with test function different from unknown.
            reaction_law_type::unique_ptr reaction_law_non_symm_ = nullptr;

            //! Visco operator.
            ViscoOperatorType::const_unique_ptr visco_operator_ = nullptr;

            //! Visco operator with test function different from unknown.
            ViscoOperatorType::const_unique_ptr visco_operator_non_symm_ = nullptr;

            ///@}

          private:
            /// \name Accessors to the global vectors and matrices managed by the class.
            ///@{

            //! \accessor{bidomain matrix}
            const GlobalMatrix& GetMatrixBidomain() const noexcept;

            //! \non_cst_accessor{bidomain matrix}
            GlobalMatrix& GetNonCstMatrixBidomain();

            //! \accessor{GradPhiTauTauGradPhi matrix}
            const GlobalMatrix& GetMatrixGradPhiTauTauGradPhi() const noexcept;

            //! \non_cst_accessor{GradPhiTauTauGradPhi matrix}
            GlobalMatrix& GetNonCstMatrixGradPhiTauTauGradPhi();

            //! \accessor{bidomain non symmetric matrix}
            const GlobalMatrix& GetMatrixBidomainNonSymmetric() const noexcept;

            //! \non_cst_accessor{bidomain non symmetric matrix}
            GlobalMatrix& GetNonCstMatrixBidomainNonSymmetric();

            //! \accessor{GradPhiTauTauGradPhiNonSymmetric matrix}
            const GlobalMatrix& GetMatrixGradPhiTauTauGradPhiNonSymmetric() const noexcept;

            //! \non_cst_accessor{GradPhiTauTauGradPhiNonSymmetric matrix}
            GlobalMatrix& GetNonCstMatrixGradPhiTauTauGradPhiNonSymmetric();

            ///@}

            /// \name Accessors to the material parameters.
            ///@{

            //! Intracellular Diffusion tensor.
            const ScalarParameter<>& GetIntracelluarTransDiffusionTensor() const noexcept;

            //! Extracellular Diffusion tensor.
            const ScalarParameter<>& GetExtracelluarTransDiffusionTensor() const noexcept;

            //! Intracellular Diffusion tensor.
            const ScalarParameter<>& GetIntracelluarFiberDiffusionTensor() const noexcept;

            //! Extracellular Diffusion tensor.
            const ScalarParameter<>& GetExtracelluarFiberDiffusionTensor() const noexcept;

            ///@}

          private:
            //! \name Material parameters.
            ///@{

            //! Intracellular Trans Diffusion tensor.
            ScalarParameter<>::unique_ptr intracellular_trans_diffusion_tensor_ = nullptr;

            //! Extracellular Trans Diffusion tensor.
            ScalarParameter<>::unique_ptr extracellular_trans_diffusion_tensor_ = nullptr;

            //! Intracellular Fiber Diffusion tensor.
            ScalarParameter<>::unique_ptr intracellular_fiber_diffusion_tensor_ = nullptr;

            //! Extracellular Fiber Diffusion tensor.
            ScalarParameter<>::unique_ptr extracellular_fiber_diffusion_tensor_ = nullptr;

            //! Heterogeneous conductivity coefficient.
            ScalarParameter<>::unique_ptr heterogeneous_conductivity_coefficient_ = nullptr;

            //! Extracellular Fiber Diffusion tensor.
            ScalarParameter<>::unique_ptr density_ = nullptr;

            //! Young modulus.
            ScalarParameter<>::unique_ptr young_modulus_ = nullptr;

            //! Poisson ratio.
            ScalarParameter<>::unique_ptr poisson_ratio_ = nullptr;

            //! Viscosity of the fluid.
            ScalarParameter<>::unique_ptr fluid_viscosity_ = nullptr;

            //! Material parameters of the solid.
            Solid::const_unique_ptr solid_ = nullptr;

            //! Applied pressure.
            ScalarParameter<>::unique_ptr static_pressure_ = nullptr;

            ///@}

            /// \name Parameters used to define TransientSource operators.
            ///@{

            //! Volumic source parameter.
            source_parameter_type::unique_ptr source_parameter_ = nullptr;

            ///@}

          private:
            /// \name Global vectors and matrices specific to the problem.
            ///@{

            //! Matrix current bidomain operator.
            GlobalMatrix::unique_ptr matrix_bidomain_ = nullptr;

            //! Matrix current grad_phi_tau_tau_grad_phi operator.
            GlobalMatrix::unique_ptr matrix_grad_phi_tau_tau_grad_phi_ = nullptr;

            //! Matrix current bidomain operator.
            GlobalMatrix::unique_ptr matrix_bidomain_non_symmetric_ = nullptr;

            //! Matrix current grad_phi_tau_tau_grad_phi operator.
            GlobalMatrix::unique_ptr matrix_grad_phi_tau_tau_grad_phi_non_symmetric_ = nullptr;
            ///@}

          private:
            //! Quadrature rule topology used for the operators.
            QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;
        };


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#include "Test/Operators/TestFunctions/VariationalFormulation.hxx" // IWYU pragma: export

#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_VARIATIONAL_FORMULATION_HPP_
