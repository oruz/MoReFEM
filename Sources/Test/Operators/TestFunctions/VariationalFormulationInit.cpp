/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 13 Sep 2017 22:46:51 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Operators/TestFunctions/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation ::VariationalFormulation(
            const morefem_data_type& morefem_data,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list))
        { }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            decltype(auto) scalar_fiber_manager =
                FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::scalar>::GetInstance(__FILE__, __LINE__);

            decltype(auto) vectorial_fiber_manager =
                FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__);

            auto& fibers_in_volume =
                vectorial_fiber_manager.GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fibers_in_volume));

            fibers_in_volume.Initialize();

            auto& fibers_on_surface =
                vectorial_fiber_manager.GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fibers_on_surface));

            fibers_on_surface.Initialize();

            auto& angles = scalar_fiber_manager.GetNonCstFiberList(EnumUnderlyingType(FiberIndex::angles));

            angles.Initialize();

            using parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::scalar,
                                                                            ::MoReFEM::ParameterNS::Policy::Constant,
                                                                            ParameterNS::TimeDependencyNS::None>;

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) domain_volume =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);
            decltype(auto) domain_full_mesh =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            decltype(auto) domain_surface =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::surface), __FILE__, __LINE__);

            intracellular_trans_diffusion_tensor_ =
                std::make_unique<parameter_type>("Intracellular Trans Diffusion tensor", domain_full_mesh, 1.);

            extracellular_trans_diffusion_tensor_ =
                std::make_unique<parameter_type>("Extracellular Trans Diffusion tensor", domain_full_mesh, 1.);

            intracellular_fiber_diffusion_tensor_ =
                std::make_unique<parameter_type>("Intracellular Fiber Diffusion tensor", domain_full_mesh, 2.);

            extracellular_fiber_diffusion_tensor_ =
                std::make_unique<parameter_type>("Extracellular Fiber Diffusion tensor", domain_full_mesh, 2.);

            heterogeneous_conductivity_coefficient_ =
                std::make_unique<parameter_type>("Heterogeneous conductivity coefficient", domain_surface, 1.);

            density_ = std::make_unique<parameter_type>("Density", domain_volume, 1.);

            decltype(auto) input_data = morefem_data.GetInputData();

            young_modulus_ = InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>(
                "Young modulus", domain_volume, input_data);

            poisson_ratio_ = InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>(
                "Poisson ratio", domain_volume, input_data);

            fluid_viscosity_ =
                InitScalarParameterFromInputData<InputDataNS::Fluid::Viscosity>("Viscosity", domain_volume, input_data);

            static_pressure_ = std::make_unique<parameter_type>("StaticPressure", domain_surface, 1.);

            using input_file_source_parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::source)>;

            source_parameter_ = InitScalarParameterFromInputData<input_file_source_parameter_type>(
                "Volumic source", domain_volume, input_data);

            DefineOperators(morefem_data.GetInputData());
            AssembleStaticOperators();
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& god_of_dof = GetGodOfDof();

            const auto& potential_1_potential_2_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2));
            const auto& potential_1_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1));
            const auto& potential_2_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_2));
            const auto& potential_3_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_3));
            const auto& displacement_potential_1_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1));
            const auto& potential_1_potential_2_potential_4_numbering_subset = god_of_dof.GetNumberingSubset(
                EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4));
            const auto& displacement_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement));
            const auto& P1_potential_1_P2_potential_2_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2));

            const auto& potential_1_potential_2_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_test));
            const auto& potential_1_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_test));
            const auto& potential_2_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_2_test));
            const auto& potential_3_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_3_test));
            const auto& displacement_potential_1_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1_test));
            const auto& potential_1_potential_2_potential_4_test_numbering_subset = god_of_dof.GetNumberingSubset(
                EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4_test));
            const auto& displacement_test_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_test));
            const auto& P1_potential_1_P2_potential_2_test_numbering_subset = god_of_dof.GetNumberingSubset(
                EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2_test));


            parent::AllocateSystemMatrix(potential_1_potential_2_numbering_subset,
                                         potential_1_potential_2_numbering_subset);
            parent::AllocateSystemVector(potential_1_potential_2_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_numbering_subset, potential_1_numbering_subset);
            parent::AllocateSystemVector(potential_1_numbering_subset);

            parent::AllocateSystemMatrix(potential_2_numbering_subset, potential_2_numbering_subset);

            parent::AllocateSystemMatrix(potential_3_numbering_subset, potential_3_numbering_subset);
            parent::AllocateSystemVector(potential_3_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_numbering_subset, potential_3_numbering_subset);

            parent::AllocateSystemMatrix(displacement_potential_1_numbering_subset,
                                         displacement_potential_1_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_potential_2_potential_4_numbering_subset,
                                         potential_1_potential_2_potential_4_numbering_subset);

            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);

            parent::AllocateSystemMatrix(P1_potential_1_P2_potential_2_numbering_subset,
                                         P1_potential_1_P2_potential_2_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_test_numbering_subset, potential_1_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_potential_2_test_numbering_subset,
                                         potential_1_potential_2_numbering_subset);
            parent::AllocateSystemVector(potential_1_potential_2_test_numbering_subset);

            parent::AllocateSystemVector(potential_1_test_numbering_subset);

            parent::AllocateSystemMatrix(potential_2_test_numbering_subset, potential_2_numbering_subset);

            parent::AllocateSystemMatrix(potential_3_test_numbering_subset, potential_3_numbering_subset);
            parent::AllocateSystemVector(potential_3_test_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_test_numbering_subset, potential_3_numbering_subset);

            parent::AllocateSystemMatrix(displacement_potential_1_test_numbering_subset,
                                         displacement_potential_1_numbering_subset);

            parent::AllocateSystemMatrix(potential_1_potential_2_potential_4_test_numbering_subset,
                                         potential_1_potential_2_potential_4_numbering_subset);

            parent::AllocateSystemMatrix(displacement_test_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_test_numbering_subset);

            parent::AllocateSystemMatrix(P1_potential_1_P2_potential_2_test_numbering_subset,
                                         P1_potential_1_P2_potential_2_numbering_subset);

            matrix_bidomain_ = std::make_unique<GlobalMatrix>(
                GetSystemMatrix(potential_1_potential_2_numbering_subset, potential_1_potential_2_numbering_subset));

            matrix_grad_phi_tau_tau_grad_phi_ = std::make_unique<GlobalMatrix>(
                GetSystemMatrix(potential_1_numbering_subset, potential_1_numbering_subset));

            matrix_bidomain_non_symmetric_ = std::make_unique<GlobalMatrix>(GetSystemMatrix(
                potential_1_potential_2_test_numbering_subset, potential_1_potential_2_numbering_subset));

            matrix_grad_phi_tau_tau_grad_phi_non_symmetric_ = std::make_unique<GlobalMatrix>(
                GetSystemMatrix(potential_1_test_numbering_subset, potential_1_numbering_subset));
        }


        void VariationalFormulation::DefineOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();

            const auto& mesh = god_of_dof.GetMesh();

            const auto dimension = mesh.GetDimension();
            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space_volume_potential_1_potential_2 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2));
            const auto& felt_space_volume_potential_1 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1));
            //            const auto& felt_space_volume_potential_2 =
            //                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_2));
            const auto& felt_space_volume_potential_3 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_3));
            const auto& felt_space_volume_displacement_potential_1 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1));
            const auto& felt_space_volume_potential_1_potential_2_potential_4 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2_potential_4));
            const auto& felt_space_volume_displacement =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume_displacement));
            const auto& felt_space_surface_displacement =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_displacement));
            const auto& felt_space_surface_potential_1 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_potential_1));
            const auto& felt_space_surface_potential_1_potential_2 =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_potential_1_potential_2));

            const auto& potential_1_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_1));
            const auto& potential_2_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_2));
            const auto& potential_3_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_3));
            // const auto& potential_4_ptr =
            // unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_4));
            const auto& displacement_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            const auto& potential_1_test_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_test_1));
            const auto& potential_2_test_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_test_2));
            // const auto& potential_3_test_ptr =
            // unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_test_3));
            const auto& displacement_test_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement_test));

            namespace GVO = GlobalVariationalOperatorNS;


            if (source_parameter_ != nullptr)
            {
                source_operator_potential_1_ = std::make_unique<source_operator_type>(
                    felt_space_volume_potential_1, potential_1_ptr, *source_parameter_);

                source_operator_potential_1_potential_1_ = std::make_unique<source_operator_type>(
                    felt_space_volume_potential_1_potential_2, potential_1_ptr, *source_parameter_);

                source_operator_potential_1_potential_2_ = std::make_unique<source_operator_type>(
                    felt_space_volume_potential_1_potential_2, potential_2_ptr, *source_parameter_);

                source_operator_potential_3_ = std::make_unique<source_operator_type>(
                    felt_space_volume_potential_3, potential_3_ptr, *source_parameter_);
            }

            variable_mass_operator_potential_1_ = std::make_unique<variable_mass_type>(
                felt_space_volume_potential_1, potential_1_ptr, potential_1_ptr, *density_);

            variable_mass_operator_potential_1_non_symm_ = std::make_unique<variable_mass_type>(
                felt_space_volume_potential_1, potential_1_ptr, potential_1_test_ptr, *density_);

            decltype(auto) scalar_fiber_manager =
                FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::scalar>::GetInstance(__FILE__, __LINE__);

            decltype(auto) vectorial_fiber_manager =
                FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__);

            const auto& fibers_in_volume =
                vectorial_fiber_manager.GetFiberList(EnumUnderlyingType(FiberIndex::fibers_in_volume));
            const auto& fibers_on_surface =
                vectorial_fiber_manager.GetFiberList(EnumUnderlyingType(FiberIndex::fibers_on_surface));
            const auto& angles = scalar_fiber_manager.GetFiberList(EnumUnderlyingType(FiberIndex::angles));

            const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
            const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
            const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
            const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();

            const std::array<Unknown::const_shared_ptr, 2> potential_1_potential_2{ { potential_1_ptr,
                                                                                      potential_2_ptr } };

            const std::array<Unknown::const_shared_ptr, 2> potential_1_potential_2_test{ { potential_1_test_ptr,
                                                                                           potential_2_test_ptr } };

            bidomain_operator_ = std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2,
                                                                 potential_1_potential_2,
                                                                 potential_1_potential_2,
                                                                 intracellular_trans_diffusion_tensor,
                                                                 extracellular_trans_diffusion_tensor,
                                                                 intracellular_fiber_diffusion_tensor,
                                                                 extracellular_fiber_diffusion_tensor,
                                                                 fibers_in_volume);

            bidomain_operator_non_symm_ = std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2,
                                                                          potential_1_potential_2,
                                                                          potential_1_potential_2_test,
                                                                          intracellular_trans_diffusion_tensor,
                                                                          extracellular_trans_diffusion_tensor,
                                                                          intracellular_fiber_diffusion_tensor,
                                                                          extracellular_fiber_diffusion_tensor,
                                                                          fibers_in_volume);

            bidomain_potential_124_operator_ =
                std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2_potential_4,
                                                potential_1_potential_2,
                                                potential_1_potential_2,
                                                intracellular_trans_diffusion_tensor,
                                                extracellular_trans_diffusion_tensor,
                                                intracellular_fiber_diffusion_tensor,
                                                extracellular_fiber_diffusion_tensor,
                                                fibers_in_volume);

            bidomain_potential_124_operator_non_symm_ =
                std::make_unique<GVO::Bidomain>(felt_space_volume_potential_1_potential_2_potential_4,
                                                potential_1_potential_2,
                                                potential_1_potential_2_test,
                                                intracellular_trans_diffusion_tensor,
                                                extracellular_trans_diffusion_tensor,
                                                intracellular_fiber_diffusion_tensor,
                                                extracellular_fiber_diffusion_tensor,
                                                fibers_in_volume);


            if (dimension == 3)
            {
                surfacic_bidomain_operator_ =
                    std::make_unique<GVO::SurfacicBidomain>(felt_space_surface_potential_1_potential_2,
                                                            potential_1_potential_2,
                                                            potential_1_potential_2,
                                                            intracellular_trans_diffusion_tensor,
                                                            extracellular_trans_diffusion_tensor,
                                                            intracellular_fiber_diffusion_tensor,
                                                            extracellular_fiber_diffusion_tensor,
                                                            *heterogeneous_conductivity_coefficient_,
                                                            fibers_on_surface,
                                                            angles);

                surfacic_bidomain_operator_non_symm_ =
                    std::make_unique<GVO::SurfacicBidomain>(felt_space_surface_potential_1_potential_2,
                                                            potential_1_potential_2,
                                                            potential_1_potential_2_test,
                                                            intracellular_trans_diffusion_tensor,
                                                            extracellular_trans_diffusion_tensor,
                                                            intracellular_fiber_diffusion_tensor,
                                                            extracellular_fiber_diffusion_tensor,
                                                            *heterogeneous_conductivity_coefficient_,
                                                            fibers_on_surface,
                                                            angles);
            }

            grad_phi_tau_tau_grad_phi_operator_ =
                std::make_unique<GVO::GradPhiTauTauGradPhi>(felt_space_volume_potential_1,
                                                            potential_1_ptr,
                                                            potential_1_ptr,
                                                            extracellular_trans_diffusion_tensor,
                                                            extracellular_fiber_diffusion_tensor,
                                                            fibers_in_volume);

            grad_phi_tau_tau_grad_phi_operator_non_symm_ =
                std::make_unique<GVO::GradPhiTauTauGradPhi>(felt_space_volume_potential_1,
                                                            potential_1_ptr,
                                                            potential_1_test_ptr,
                                                            extracellular_trans_diffusion_tensor,
                                                            extracellular_fiber_diffusion_tensor,
                                                            fibers_in_volume);

            grad_phi_tau_ortho_tau_grad_phi_operator_ =
                std::make_unique<GVO::GradPhiTauOrthoTauGradPhi>(felt_space_surface_potential_1,
                                                                 potential_1_ptr,
                                                                 potential_1_ptr,
                                                                 extracellular_trans_diffusion_tensor,
                                                                 extracellular_fiber_diffusion_tensor,
                                                                 fibers_on_surface,
                                                                 angles);

            grad_phi_tau_ortho_tau_grad_phi_operator_non_symm_ =
                std::make_unique<GVO::GradPhiTauOrthoTauGradPhi>(felt_space_surface_potential_1,
                                                                 potential_1_ptr,
                                                                 potential_1_test_ptr,
                                                                 extracellular_trans_diffusion_tensor,
                                                                 extracellular_fiber_diffusion_tensor,
                                                                 fibers_on_surface,
                                                                 angles);

            const std::array<Unknown::const_shared_ptr, 2> displacement_potential_1{ { displacement_ptr,
                                                                                       potential_1_ptr } };

            const std::array<Unknown::const_shared_ptr, 2> displacement_potential_test_1{ { displacement_test_ptr,
                                                                                            potential_1_test_ptr } };

            scalar_div_vectorial_operator_ = std::make_unique<GVO::ScalarDivVectorial>(
                felt_space_volume_displacement_potential_1, displacement_potential_1, displacement_potential_1, 2., 3.);

            scalar_div_vectorial_operator_non_symm_ =
                std::make_unique<GVO::ScalarDivVectorial>(felt_space_volume_displacement_potential_1,
                                                          displacement_potential_1,
                                                          displacement_potential_test_1,
                                                          2.,
                                                          3.);

            const auto mesh_dimension = god_of_dof.GetMesh().GetDimension();

            const auto elast_config =
                ParameterNS::ReadGradientBasedElasticityTensorConfigurationFromFile(mesh_dimension, input_data);

            elasticity_operator_ =
                std::make_unique<GVO::GradOnGradientBasedElasticityTensor>(felt_space_volume_displacement,
                                                                           displacement_ptr,
                                                                           displacement_ptr,
                                                                           *young_modulus_,
                                                                           *poisson_ratio_,
                                                                           elast_config);

            elasticity_operator_non_symm_ =
                std::make_unique<GVO::GradOnGradientBasedElasticityTensor>(felt_space_volume_displacement,
                                                                           displacement_ptr,
                                                                           displacement_test_ptr,
                                                                           *young_modulus_,
                                                                           *poisson_ratio_,
                                                                           elast_config);


            stokes_operator_ = std::make_unique<GVO::Stokes>(felt_space_volume_displacement_potential_1,
                                                             displacement_potential_1,
                                                             displacement_potential_1,
                                                             *fluid_viscosity_);

            stokes_operator_non_symm_ = std::make_unique<GVO::Stokes>(felt_space_volume_displacement_potential_1,
                                                                      displacement_potential_1,
                                                                      displacement_potential_test_1,
                                                                      *fluid_viscosity_);

            ale_operator_ = std::make_unique<GVO::Ale>(
                felt_space_volume_displacement, displacement_ptr, displacement_ptr, *density_);

            ale_operator_non_symm_ = std::make_unique<GVO::Ale>(
                felt_space_volume_displacement, displacement_ptr, displacement_test_ptr, *density_);

            if (dimension == 3)
            {
                following_pressure_operator_ = std::make_unique<GVO::FollowingPressure<>>(
                    felt_space_surface_displacement, displacement_ptr, displacement_ptr, *static_pressure_);

                following_pressure_operator_non_symm_ = std::make_unique<GVO::FollowingPressure<>>(
                    felt_space_surface_displacement, displacement_ptr, displacement_test_ptr, *static_pressure_);
            }

            decltype(auto) domain_volume = DomainManager::GetInstance(__FILE__, __LINE__)
                                               .GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);

            reaction_law_ =
                std::make_unique<reaction_law_type>(input_data,
                                                    domain_volume,
                                                    GetTimeManager(),
                                                    felt_space_volume_potential_1.GetQuadratureRulePerTopology());

            non_linear_source_operator_ = std::make_unique<non_linear_source_operator_type>(
                felt_space_volume_potential_1, potential_1_ptr, *reaction_law_);

            non_linear_source_operator_non_symm_ = std::make_unique<non_linear_source_operator_type>(
                felt_space_volume_potential_1, potential_1_test_ptr, *reaction_law_);


            solid_ = std::make_unique<Solid>(input_data,
                                             domain_volume,
                                             felt_space_volume_displacement.GetQuadratureRulePerTopology(),
                                             -1.); // Negative tolerance to cancel CheckConsistancy issues.

            hyperelastic_law_parent::Create(*solid_);


            if (dimension > 1)
            {
                visco_operator_ = std::make_unique<ViscoOperatorType>(felt_space_volume_displacement,
                                                                      displacement_ptr,
                                                                      displacement_ptr,
                                                                      *solid_,
                                                                      GetTimeManager(),
                                                                      hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                                      nullptr,
                                                                      nullptr);

                visco_operator_non_symm_ =
                    std::make_unique<ViscoOperatorType>(felt_space_volume_displacement,
                                                        displacement_ptr,
                                                        displacement_test_ptr,
                                                        *solid_,
                                                        GetTimeManager(),
                                                        hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                        nullptr,
                                                        nullptr);
            }
        }


    } // namespace TestFunctionsNS


} // namespace MoReFEM
