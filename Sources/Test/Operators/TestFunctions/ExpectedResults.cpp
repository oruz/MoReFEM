/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 21:38:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/TestFunctions/ExpectedResults.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"

namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        void CheckMatrix(const GodOfDof& god_of_dof,
                         const GlobalMatrix& obtained,
                         const expected_results_type<IsMatrixOrVector::matrix>& expected_results,
                         std::string&& name,
                         const char* invoking_file,
                         int invoking_line,
                         const double epsilon)
        {
            const auto it = expected_results.find(name);

            //            std::cout << "=== Matrix " << name << std::endl;
            //            obtained.View(god_of_dof.GetMpi(), __FILE__, __LINE__);

            if (it == expected_results.cend())
                throw Exception(
                    "Requested expected result for matrix (" + name + ") is unknown", invoking_file, invoking_line);

            TestNS::CheckMatrix(god_of_dof, obtained, it->second, invoking_file, invoking_line, epsilon);
        }


        void CheckVector(const GodOfDof& god_of_dof,
                         const GlobalVector& obtained,
                         const expected_results_type<IsMatrixOrVector::vector>& expected_results,
                         std::string&& name,
                         const char* invoking_file,
                         int invoking_line,
                         const double epsilon)
        {
            const auto it = expected_results.find(name);

            //            std::cout << "=== vector " << name << std::endl;
            //            obtained.View(god_of_dof.GetMpi(), __FILE__, __LINE__);

            if (it == expected_results.cend())
                throw Exception(
                    "Requested expected result for vector (" + name + ") is unknown", invoking_file, invoking_line);

            TestNS::CheckVector(god_of_dof, obtained, it->second, invoking_file, invoking_line, epsilon);
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults(std::size_t dimension,
                                                                                    bool is_parallel)
        {
            switch (dimension)
            {
            case 1u:
                return GetExpectedVectorialResults1D(is_parallel);
            case 2u:
                return GetExpectedVectorialResults2D(is_parallel);
            case 3u:
                return GetExpectedVectorialResults3D(is_parallel);
            default:
                throw Exception("Invalid dimension: 1, 2 or 3 were expected and " + std::to_string(dimension)
                                    + " was  provided.",
                                __FILE__,
                                __LINE__);
            }
        }


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults(std::size_t dimension,
                                                                                    bool is_parallel)
        {
            switch (dimension)
            {
            case 1u:
                return GetExpectedMatricialResults1D(is_parallel);
            case 2u:
                return GetExpectedMatricialResults2D(is_parallel);
            case 3u:
                return GetExpectedMatricialResults3D(is_parallel);
            default:
                throw Exception("Invalid dimension: 1, 2 or 3 were expected and " + std::to_string(dimension)
                                    + " was  provided.",
                                __FILE__,
                                __LINE__);
            }
        }


    } // namespace TestFunctionsNS


} // namespace MoReFEM
