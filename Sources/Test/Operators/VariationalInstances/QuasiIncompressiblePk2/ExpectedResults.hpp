/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_EXPECTED_RESULTS_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_EXPECTED_RESULTS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================0


    namespace TestNS::QuasiIncompressiblePk2
    {


        /*!
         * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
         * \a IsMatrixOrVectorT template parameter.
         *
         * \tparam IsMatrixOrVectorT Whether matrix or vector is considered.
         */
        template<IsMatrixOrVector IsMatrixOrVectorT>
        // clang-format off
        using expected_results_type =
            std::conditional_t
            <
                IsMatrixOrVectorT == IsMatrixOrVector::matrix,
                std::vector<std::vector<PetscScalar>>,
                std::vector<PetscScalar>
            >;
        // clang-format on


        /*!
         * \brief Returns the expected matrix when unknown and test unknowns are both P1b/P1.
         *
         * \param[in] dimension Dimension of the mesh considered.
         *
         * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1bP1(std::size_t dimension);


        /*!
         * \brief Returns the expected vector when unknown and test unknowns are both P1b/P1.
         *
         * \param[in] dimension Dimension of the mesh considered.
         *
         * \return The expected content of the vector, in \a content_type format.
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1bP1(std::size_t dimension);


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_EXPECTED_RESULTS_HPP_
