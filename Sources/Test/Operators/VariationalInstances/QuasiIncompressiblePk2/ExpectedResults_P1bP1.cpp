/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::QuasiIncompressiblePk2
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();
            expected_results_type<IsMatrixOrVector::vector> Vector3D();
            expected_results_type<IsMatrixOrVector::vector> Vector2D();
            expected_results_type<IsMatrixOrVector::vector> Vector1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1bP1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1bP1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Vector3D();
            case 2u:
                return Vector2D();
            case 1u:
                return Vector1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>

                    { { 309310,
                        308417,
                        377900,
                        0.0470026,
                        983691,
                        999469,
                        1.15034e+06,
                        0.0216057,
                        1.28774e+06,
                        1.28312e+06,
                        1.53225e+06,
                        0.0343041,
                        49167,
                        55139,
                        3558.53,
                        0.0407315,
                        -2.62991e+06,
                        -2.64615e+06,
                        -3.06405e+06 },
                      { 308417,
                        313565,
                        377130,
                        0.0470026,
                        972998,
                        999099,
                        1.15659e+06,
                        0.0343041,
                        1.30549e+06,
                        1.3189e+06,
                        1.53111e+06,
                        0.0216057,
                        54779.7,
                        57619.8,
                        8255.26,
                        0.0407315,
                        -2.64169e+06,
                        -2.68918e+06,
                        -3.07309e+06 },
                      { 377900,
                        377130,
                        600991,
                        0.0335495,
                        1.17229e+06,
                        1.17376e+06,
                        1.77629e+06,
                        0.0208511,
                        1.54955e+06,
                        1.55271e+06,
                        2.37763e+06,
                        0.0208511,
                        43396.1,
                        51033.4,
                        -47367.6,
                        0.0263243,
                        -3.14314e+06,
                        -3.15463e+06,
                        -4.70755e+06 },
                      {
                          551.41,      580.593,     -9657.48, 1.66667e-07, 9190.64,     5509.99, -25204.2,
                          8.33333e-08, 5973.85,     9858.78,  -34861.7,    8.33333e-08, 3768.21, 3768.21,
                          7536.41,     8.33333e-08, -19484.1, -19717.6,    62187,
                      },
                      { 983691,
                        972998,
                        1.17229e+06,
                        -0.0754181,
                        3.17684e+06,
                        3.1764e+06,
                        3.63054e+06,
                        -0.100815,
                        4.13257e+06,
                        4.05223e+06,
                        4.78645e+06,
                        -0.0373229,
                        193060,
                        187347,
                        33822.7,
                        -0.0983602,
                        -8.48617e+06,
                        -8.38898e+06,
                        -9.6231e+06 },
                      { 999469,
                        999099,
                        1.17376e+06,
                        0.0327948,
                        3.1764e+06,
                        3.20948e+06,
                        3.6451e+06,
                        0.0200964,
                        4.26443e+06,
                        4.22802e+06,
                        4.82975e+06,
                        -0.00530045,
                        208112,
                        204292,
                        56963.4,
                        0.0119171,
                        -8.64841e+06,
                        -8.64089e+06,
                        -9.70557e+06 },
                      { 1.15034e+06,
                        1.15659e+06,
                        1.77629e+06,
                        -0.00756449,
                        3.63054e+06,
                        3.6451e+06,
                        5.33608e+06,
                        -0.0202629,
                        4.78422e+06,
                        4.78879e+06,
                        7.1051e+06,
                        -0.00756449,
                        218421,
                        212882,
                        -58301.7,
                        -0.0313044,
                        -9.78352e+06,
                        -9.80336e+06,
                        -1.41592e+07 },
                      { 4566.95,
                        4574.67,
                        -75864,
                        8.33333e-08,
                        72944.9,
                        43346,
                        -197970,
                        1.66667e-07,
                        47889.8,
                        77542.7,
                        -273834,
                        8.33333e-08,
                        29622,
                        29622,
                        59244,
                        8.33333e-08,
                        -155024,
                        -155085,
                        488424 },
                      { 1.28774e+06,
                        1.30549e+06,
                        1.54955e+06,
                        0.0797973,
                        4.13257e+06,
                        4.26443e+06,
                        4.78422e+06,
                        0.00360686,
                        5.40034e+06,
                        5.45401e+06,
                        6.33269e+06,
                        0.0670989,
                        233632,
                        266321,
                        48925.6,
                        0.0526486,
                        -1.10543e+07,
                        -1.12903e+07,
                        -1.27154e+07 },
                      { 1.28312e+06,
                        1.3189e+06,
                        1.55271e+06,
                        -0.0284155,
                        4.05223e+06,
                        4.22802e+06,
                        4.78879e+06,
                        -0.0284155,
                        5.45401e+06,
                        5.58119e+06,
                        6.36135e+06,
                        -0.0538124,
                        238035,
                        268307,
                        51763.6,
                        -0.0576287,
                        -1.10274e+07,
                        -1.13964e+07,
                        -1.27546e+07 },
                      { 1.53225e+06,
                        1.53111e+06,
                        2.37763e+06,
                        0.025985,
                        4.78645e+06,
                        4.82975e+06,
                        7.1051e+06,
                        0.000588151,
                        6.33269e+06,
                        6.36135e+06,
                        9.4968e+06,
                        0.0132866,
                        237391,
                        288373,
                        -104808,
                        -0.00498007,
                        -1.28888e+07,
                        -1.30106e+07,
                        -1.88747e+07 },
                      { 4415.92,
                        4478.14,
                        -76014.8,
                        8.33333e-08,
                        72835.7,
                        43228.4,
                        -198251,
                        8.33333e-08,
                        47457.6,
                        77500.5,
                        -274265,
                        1.66667e-07,
                        29794,
                        29794,
                        59587.9,
                        8.33333e-08,
                        -154503,
                        -155001,
                        488943 },
                      { 49167,
                        54779.7,
                        43396.1,
                        -0.108213,
                        193060,
                        208112,
                        218421,
                        -0.0828161,
                        233632,
                        238035,
                        237391,
                        -0.0701176,
                        45158.9,
                        35017.3,
                        60825.3,
                        -0.110277,
                        -521017,
                        -535944,
                        -560033 },
                      { 55139,
                        57619.8,
                        51033.4,
                        -0.108213,
                        187347,
                        204292,
                        212882,
                        -0.0828161,
                        266321,
                        268307,
                        288373,
                        -0.0701176,
                        35017.3,
                        39113.1,
                        57748.3,
                        -0.110277,
                        -543824,
                        -569332,
                        -610037 },
                      { 3558.53,
                        8255.26,
                        -47367.6,
                        -0.216426,
                        33822.7,
                        56963.4,
                        -58301.7,
                        -0.165632,
                        48925.6,
                        51763.6,
                        -104808,
                        -0.140235,
                        60825.3,
                        57748.3,
                        105095,
                        -0.220555,
                        -147132,
                        -174730,
                        105383 },
                      { 47710.8,
                        47718.7,
                        37475,
                        8.33333e-08,
                        170136,
                        156658,
                        125927,
                        8.33333e-08,
                        204345,
                        217879,
                        163402,
                        8.33333e-08,
                        13501.9,
                        13501.9,
                        27003.9,
                        1.66667e-07,
                        -435694,
                        -435758,
                        -353807 },
                      { -2.62991e+06,
                        -2.64169e+06,
                        -3.14314e+06,
                        0.0568311,
                        -8.48617e+06,
                        -8.64841e+06,
                        -9.78352e+06,
                        0.158418,
                        -1.10543e+07,
                        -1.10274e+07,
                        -1.28888e+07,
                        0.00603743,
                        -521017,
                        -543824,
                        -147132,
                        0.115257,
                        2.26914e+07,
                        2.28613e+07,
                        2.59626e+07 },
                      { -2.64615e+06,
                        -2.68918e+06,
                        -3.15463e+06,
                        0.0568311,
                        -8.38898e+06,
                        -8.64089e+06,
                        -9.80336e+06,
                        0.0568311,
                        -1.12903e+07,
                        -1.13964e+07,
                        -1.30106e+07,
                        0.107625,
                        -535944,
                        -569332,
                        -174730,
                        0.115257,
                        2.28613e+07,
                        2.32958e+07,
                        2.61433e+07 },
                      { -3.06405e+06,
                        -3.07309e+06,
                        -4.70755e+06,
                        0.164456,
                        -9.6231e+06,
                        -9.70557e+06,
                        -1.41592e+07,
                        0.164456,
                        -1.27154e+07,
                        -1.27546e+07,
                        -1.88747e+07,
                        0.113662,
                        -560033,
                        -610037,
                        105383,
                        0.230515,
                        2.59626e+07,
                        2.61433e+07,
                        3.76361e+07 } };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector3D()
            {
                return expected_results_type<IsMatrixOrVector::vector>{
                    -14995.7, -17870.8, 3880.51,  -1274.06, -53230.6, -62510.9, -13575.6, -13169.2, -70907.4, -75092.8,
                    -10276.6, -13189.4, -15747.8, -14303.1, -21777.6, -5764.01, 154881,   169778,   41749.3
                };
            }


            expected_results_type<IsMatrixOrVector::vector> Vector2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


        } // namespace


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM
