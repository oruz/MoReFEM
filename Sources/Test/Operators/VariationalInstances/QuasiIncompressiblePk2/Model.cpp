/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::QuasiIncompressiblePk2
    {


        Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
        {
            decltype(auto) mpi = parent::GetMpi();

            if (mpi.Nprocessor<int>() > 1)
            {
                throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                                "a vector; the expected values assume the dof numbering of the sequential case. Please "
                                "run it sequentially.",
                                __FILE__,
                                __LINE__);
            }
        }


        void Model::SupplInitialize()
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            decltype(auto) input_data = morefem_data.GetInputData();

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) domain_volume =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);

            quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(10, 3);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space_monolithic));


            solid_ = std::make_unique<Solid>(input_data,
                                             domain_volume,
                                             felt_space.GetQuadratureRulePerTopology(),
                                             -1.); // Negative to cancel CheckConsistancy issues.

            hyperelastic_deviatoric_law_parent::Create(*solid_);
            penalization_law_parent::Create(*solid_);

            // Required to enable construction of an operator after initialization step.
            parent::SetClearGodOfDofTemporaryDataToFalse();
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM
