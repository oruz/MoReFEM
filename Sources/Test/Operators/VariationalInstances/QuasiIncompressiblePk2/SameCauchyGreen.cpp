/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS::QuasiIncompressiblePk2
    {


        namespace // anonymous
        {


            using ::MoReFEM::Internal::assemble_into_matrix;

            using ::MoReFEM::Internal::assemble_into_vector;


        } // namespace


        void Model::SameCauchyGreen(assemble_into_matrix do_assemble_into_matrix,
                                    assemble_into_vector do_assemble_into_vector) const
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space_monolithic));

            const auto& displacement_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            const auto& pressure_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));

            const std::array<Unknown::const_shared_ptr, 2> displacement_pressure{ { displacement_ptr, pressure_ptr } };

            SameCauchyGreenIncompressibleStiffnessOperatorType incompressible_stiffness_operator(
                felt_space,
                displacement_pressure,
                displacement_pressure,
                *solid_,
                GetTimeManager(),
                hyperelastic_deviatoric_law_parent::GetHyperelasticLawPtr(),
                penalization_law_parent::GetPenalizationLawPtr(),
                nullptr,
                nullptr);


            decltype(auto) monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            GlobalMatrix matrix_p1b_p1(monolithic_numbering_subset, monolithic_numbering_subset);
            AllocateGlobalMatrix(god_of_dof, matrix_p1b_p1);

            GlobalVector vector_p1b_p1(monolithic_numbering_subset);
            AllocateGlobalVector(god_of_dof, vector_p1b_p1);

            GlobalVector previous_iteration_p1b_p1(vector_p1b_p1);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            {
                matrix_p1b_p1.ZeroEntries(__FILE__, __LINE__);
                vector_p1b_p1.ZeroEntries(__FILE__, __LINE__);
                previous_iteration_p1b_p1.ZeroEntries(__FILE__, __LINE__);

                switch (dimension)
                {
                case 3:
                    previous_iteration_p1b_p1.SetValue(3, 2., INSERT_VALUES, __FILE__, __LINE__);
                    [[fallthrough]];
                case 2:
                    previous_iteration_p1b_p1.SetValue(2, 2., INSERT_VALUES, __FILE__, __LINE__);
                    [[fallthrough]];
                case 1:
                    previous_iteration_p1b_p1.SetValue(0, -1., INSERT_VALUES, __FILE__, __LINE__);
                    previous_iteration_p1b_p1.SetValue(1, -2., INSERT_VALUES, __FILE__, __LINE__);
                    break;
                }

                previous_iteration_p1b_p1.Assembly(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient matrix(matrix_p1b_p1, 1.);
                GlobalVectorWithCoefficient vec(vector_p1b_p1, 1.);

                if (do_assemble_into_matrix == assemble_into_matrix::yes
                    && do_assemble_into_vector == assemble_into_vector::yes)
                {
                    incompressible_stiffness_operator.Assemble(
                        std::make_tuple(std::ref(matrix), std::ref(vec)),
                        MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                    /* BOOST_CHECK_NO_THROW */ (CheckMatrix(
                        god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), __FILE__, __LINE__, 1.e2));

                    /* BOOST_CHECK_NO_THROW */ (CheckVector(
                        god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), __FILE__, __LINE__, 1.e2));
                } else if (do_assemble_into_matrix == assemble_into_matrix::yes
                           && do_assemble_into_vector == assemble_into_vector::no)
                {
                    incompressible_stiffness_operator.Assemble(
                        std::make_tuple(std::ref(matrix)), MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                    /* BOOST_CHECK_NO_THROW */ (CheckMatrix(
                        god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), __FILE__, __LINE__, 1.e2));
                } else if (do_assemble_into_matrix == assemble_into_matrix::no
                           && do_assemble_into_vector == assemble_into_vector::yes)
                {
                    incompressible_stiffness_operator.Assemble(
                        std::make_tuple(std::ref(vec)), MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                    /* BOOST_CHECK_NO_THROW */ (CheckVector(
                        god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), __FILE__, __LINE__, 1.e2));
                }
            }
        }


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM
