/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/Crtp/Penalization.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS::QuasiIncompressiblePk2
    {


        //! \copydoc doxygen_hide_model_4_test
        class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>,
                      public FormulationSolverNS::HyperelasticLaw<Model, HyperelasticLawNS::CiarletGeymonatDeviatoric>,
                      public FormulationSolverNS::Penalization<Model, HyperelasticLawNS::LogI3Penalization>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = Model;

            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

            //! Alias to hyperlastic law parent,
            using hyperelastic_deviatoric_law_parent =
                FormulationSolverNS::HyperelasticLaw<self, HyperelasticLawNS::CiarletGeymonatDeviatoric>;

            //! Alias to hyperlastic law parent,
            using penalization_law_parent =
                FormulationSolverNS::Penalization<self, HyperelasticLawNS::LogI3Penalization>;

            //! Alias to the viscoelasticity policy used.
            using ViscoelasticityPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None;

            //! Alias to the active stress policy used.
            using InternalVariablePolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None;

            //! Alias to the hyperelasticity policy used for the deviatoric part.
            using hyperelasticity_deviatoric_policy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::
                    Hyperelasticity<typename hyperelastic_deviatoric_law_parent::hyperelastic_law_type>;

            //! Alias to the penalization law used for the volumic part.
            using penalization_law = typename penalization_law_parent::penalization_law_type;

            //! Policy used to have a different cauchy green on the volumetric and deviatoric parts.
            template<class U>
            using DifferentCauchyGreenMixedSolidIncompressibilityPolicy =
                GlobalVariationalOperatorNS::DifferentCauchyGreenMixedSolidIncompressibility<U>;

            //! Policy used to have the same cauchy green on the volumetric and deviatoric parts.
            template<class U>
            using SameCauchyGreenMixedSolidIncompressibilityPolicy =
                GlobalVariationalOperatorNS::SameCauchyGreenMixedSolidIncompressibility<U>;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

            //! Strong type for displacement global vectors.
            using DisplacementGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

            //! Strong type for deviatoric part of monolithic global vectors.
            using MonolithicDeviatoricGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::MonolithicDeviatoricTag>;

            //! Strong type for volumetric part of monolithic global vectors.
            using MonolithicVolumetricGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::MonolithicVolumetricTag>;


          public:
            //! Alias to the QuasiIncompressibleSecondPiolaKirchhoff type.
            using DifferentCauchyGreenIncompressibleStiffnessOperatorType =
                GlobalVariationalOperatorNS::QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
                    hyperelasticity_deviatoric_policy,
                    ViscoelasticityPolicy,
                    InternalVariablePolicy,
                    penalization_law,
                    DifferentCauchyGreenMixedSolidIncompressibilityPolicy>;

            //! Alias to the QuasiIncompressibleSecondPiolaKirchhoff type.
            using SameCauchyGreenIncompressibleStiffnessOperatorType =
                GlobalVariationalOperatorNS::QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
                    hyperelasticity_deviatoric_policy,
                    ViscoelasticityPolicy,
                    InternalVariablePolicy,
                    penalization_law,
                    SameCauchyGreenMixedSolidIncompressibilityPolicy>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize();


          public:
            /*!
             * \brief Case when the test function uses up the same \a Unknown.
             *
             * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
             * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
             *
             * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
             */
            void DifferentCauchyGreen(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                                      ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;

            /*!
             * \brief Case when the test function uses up the same \a Unknown.
             *
             * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
             * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
             *
             * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
             */
            void SameCauchyGreen(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                                 ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;

          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();

            ///@}

          private:
            //! Young modulus.
            ScalarParameter<>::unique_ptr young_modulus_ = nullptr;

            //! Poisson ratio.
            ScalarParameter<>::unique_ptr poisson_ratio_ = nullptr;

            //! Material parameters of the solid.
            Solid::const_unique_ptr solid_ = nullptr;

          private:
            //! Quadrature rule topology used for the operators.
            QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;
        };


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM


#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HPP_
