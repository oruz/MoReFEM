/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::QuasiIncompressiblePk2
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("Test QuasiIncompressiblePk2 operator");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


    } // namespace TestNS::QuasiIncompressiblePk2


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_MODEL_HXX_
