/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE quasi_incompressible_second_piola_kirchhoff_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/InputData.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::QuasiIncompressiblePk2::Model>;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().DifferentCauchyGreen(assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().DifferentCauchyGreen(assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().DifferentCauchyGreen(assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(same_cauchy_green, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().SameCauchyGreen(assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().SameCauchyGreen(assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().SameCauchyGreen(assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()
