//! \file
//
//
//  VariationalFormulation.hxx
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/Microsphere/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace TestNS::Microsphere
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation::GetMassOperator() const noexcept
        {
            assert(!(!mass_operator_));
            return *mass_operator_;
        }


        inline const VariationalFormulation::StiffnessOperatorType&
        VariationalFormulation::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace1() const noexcept
        {
            assert(!(!surfacic_force_operator_face_1_));
            return *surfacic_force_operator_face_1_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace2() const noexcept
        {
            assert(!(!surfacic_force_operator_face_2_));
            return *surfacic_force_operator_face_2_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace3() const noexcept
        {
            assert(!(!surfacic_force_operator_face_3_));
            return *surfacic_force_operator_face_3_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace4() const noexcept
        {
            assert(!(!surfacic_force_operator_face_4_));
            return *surfacic_force_operator_face_4_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace5() const noexcept
        {
            assert(!(!surfacic_force_operator_face_5_));
            return *surfacic_force_operator_face_5_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace6() const noexcept
        {
            assert(!(!surfacic_force_operator_face_6_));
            return *surfacic_force_operator_face_6_;
        }


        inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
        {
            assert(!(!vector_stiffness_residual_));
            return *vector_stiffness_residual_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorVelocityAtNewtonIteration() const noexcept
        {
            assert(!(!vector_velocity_at_newton_iteration_));
            return *vector_velocity_at_newton_iteration_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorVelocityAtNewtonIteration() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorVelocityAtNewtonIteration());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixMassPerSquareTimeStep() const noexcept
        {
            assert(!(!matrix_mass_per_square_time_step_));
            return *matrix_mass_per_square_time_step_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixMassPerSquareTimeStep() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
        {
            assert(!(!matrix_tangent_stiffness_));
            return *matrix_tangent_stiffness_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace1() const noexcept
        {
            assert(!(!vector_surfacic_force_face_1_));
            return *vector_surfacic_force_face_1_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace1() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace1());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace2() const noexcept
        {
            assert(!(!vector_surfacic_force_face_2_));
            return *vector_surfacic_force_face_2_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace2() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace2());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace3() const noexcept
        {
            assert(!(!vector_surfacic_force_face_3_));
            return *vector_surfacic_force_face_3_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace3() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace3());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace4() const noexcept
        {
            assert(!(!vector_surfacic_force_face_4_));
            return *vector_surfacic_force_face_4_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace4() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace4());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace5() const noexcept
        {
            assert(!(!vector_surfacic_force_face_5_));
            return *vector_surfacic_force_face_5_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace5() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace5());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace6() const noexcept
        {
            assert(!(!vector_surfacic_force_face_6_));
            return *vector_surfacic_force_face_6_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace6() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace6());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
        {
            assert(!(!vector_current_displacement_));
            return *vector_current_displacement_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentVelocity() const noexcept
        {
            assert(!(!vector_current_velocity_));
            return *vector_current_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointPosition() const noexcept
        {
            assert(!(!vector_midpoint_position_));
            return *vector_midpoint_position_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointPosition() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointPosition());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointVelocity() const noexcept
        {
            assert(!(!vector_midpoint_velocity_));
            return *vector_midpoint_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorDiffDisplacement() const noexcept
        {
            assert(!(!vector_diff_displacement_));
            return *vector_diff_displacement_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorDiffDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorDiffDisplacement());
        }


        inline const Solid& VariationalFormulation::GetSolid() const noexcept
        {
            assert(!(!solid_));
            return *solid_;
        }

        inline const InputMicrosphere& VariationalFormulation::GetInputMicrosphere() const noexcept
        {
            assert(!(!input_microsphere_));
            return *input_microsphere_;
        }

        inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const
        {
            return displacement_numbering_subset_;
        }


    } // namespace TestNS::Microsphere


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_VARIATIONAL_FORMULATION_HXX_
