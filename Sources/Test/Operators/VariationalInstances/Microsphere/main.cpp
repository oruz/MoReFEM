//! \file
//
//
//  main.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "InputData.hpp"
#include "Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    using InputData = TestNS::Microsphere::InputData;

    try
    {
        MoReFEMData<InputData, program_type::model> morefem_data(argc, argv);

        decltype(auto) input_data = morefem_data.GetInputData();
        decltype(auto) mpi = morefem_data.GetMpi();

        try
        {
            TestNS::Microsphere::Model model(morefem_data);
            model.Run();

            input_data.PrintUnused(std::cout);
        }
        catch (const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch (const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;

        std::cout << oconv.str();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
