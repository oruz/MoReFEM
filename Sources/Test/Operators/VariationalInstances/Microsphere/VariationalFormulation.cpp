//! \file
//
//
//  VariationalFormulation.cpp
//  MoReFEM
//
//  Created by Jérôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"
#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"

#include "VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace TestNS::Microsphere
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation::VariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& displacement_numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          displacement_numbering_subset_(displacement_numbering_subset)
        {
            assert(time_manager.IsTimeStepConstant() && "Current instantiation relies on this assumption!");
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();

            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);

            const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);

            vector_surfacic_force_face_1_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_2_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_3_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_4_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_5_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_6_ = std::make_unique<GlobalVector>(system_rhs);

            vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

            vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
        }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            namespace IPL = Utilities::InputDataNS;

            constexpr std::size_t degree_of_exactness = 2;
            constexpr std::size_t shape_function_order = 1;

            quadrature_rule_per_topology_parameter_ =
                std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

            quadrature_rule_per_topology_for_operators_ =
                std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

            auto& fiberI4 = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__)
                                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiberI4));

            fiberI4.Initialize(quadrature_rule_per_topology_parameter_.get());

            auto& fiberI6 = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__)
                                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiberI6));

            fiberI6.Initialize(quadrature_rule_per_topology_parameter_.get());

            decltype(auto) domain_full_mesh =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            decltype(auto) input_data = morefem_data.GetInputData();

            solid_ =
                std::make_unique<Solid>(input_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

            input_microsphere_ = std::make_unique<InputMicrosphere>(
                input_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

            DefineStaticOperators(input_data);
        }


        void VariationalFormulation::DefineStaticOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const auto& felt_space_face1 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face1));
            const auto& felt_space_face2 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face2));
            const auto& felt_space_face3 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face3));
            const auto& felt_space_face4 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face4));
            const auto& felt_space_face5 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face5));
            const auto& felt_space_face6 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face6));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;

            hyperelastic_law_parent::Create(GetSolid());

            InputMicrosphere* raw_input_microsphere = input_microsphere_.get();

            stiffness_operator_ =
                std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                        displacement_ptr,
                                                        displacement_ptr,
                                                        GetSolid(),
                                                        GetTimeManager(),
                                                        hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                        quadrature_rule_per_topology_for_operators_.get(),
                                                        raw_input_microsphere);

            decltype(auto) domain_force_face_1 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face1), __FILE__, __LINE__);

            decltype(auto) domain_force_face_2 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face2), __FILE__, __LINE__);

            decltype(auto) domain_force_face_3 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face3), __FILE__, __LINE__);

            decltype(auto) domain_force_face_4 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face4), __FILE__, __LINE__);

            decltype(auto) domain_force_face_5 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face5), __FILE__, __LINE__);

            decltype(auto) domain_force_face_6 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face6), __FILE__, __LINE__);

            using parameter_type_face1 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>;

            force_parameter_face_1_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face1>(
                    "Surfacic force", domain_force_face_1, input_data);

            using parameter_type_face2 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face2)>;

            force_parameter_face_2_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face2>(
                    "Surfacic force", domain_force_face_2, input_data);

            using parameter_type_face3 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face3)>;

            force_parameter_face_3_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face3>(
                    "Surfacic force", domain_force_face_3, input_data);

            using parameter_type_face4 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face4)>;

            force_parameter_face_4_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face4>(
                    "Surfacic force", domain_force_face_4, input_data);

            using parameter_type_face5 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face5)>;

            force_parameter_face_5_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face5>(
                    "Surfacic force", domain_force_face_5, input_data);

            using parameter_type_face6 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face6)>;

            force_parameter_face_6_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face6>(
                    "Surfacic force", domain_force_face_6, input_data);

            if (force_parameter_face_1_ != nullptr && force_parameter_face_2_ != nullptr
                && force_parameter_face_3_ != nullptr && force_parameter_face_4_ != nullptr
                && force_parameter_face_5_ != nullptr && force_parameter_face_6_ != nullptr)
            {
                surfacic_force_operator_face_1_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face1, displacement_ptr, *force_parameter_face_1_);

                surfacic_force_operator_face_2_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face2, displacement_ptr, *force_parameter_face_2_);

                surfacic_force_operator_face_3_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face3, displacement_ptr, *force_parameter_face_3_);

                surfacic_force_operator_face_4_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face4, displacement_ptr, *force_parameter_face_4_);

                surfacic_force_operator_face_5_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face5, displacement_ptr, *force_parameter_face_5_);

                surfacic_force_operator_face_6_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face6, displacement_ptr, *force_parameter_face_6_);
            }
        }


        void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
        {
            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                break;
            case StaticOrDynamic::dynamic_:
                UpdateDynamicVectorsAndMatrices(evaluation_state);
                break;
            }

            AssembleOperators(evaluation_state);
        }


        void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state)
        {
            UpdateVelocityAtNewtonIteration(evaluation_state);

            auto& midpoint_position = GetNonCstVectorMidpointPosition();

            midpoint_position.Copy(GetVectorCurrentDisplacement(), __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., evaluation_state, midpoint_position, __FILE__, __LINE__);

            midpoint_position.Scale(0.5, __FILE__, __LINE__);

            midpoint_position.UpdateGhosts(__FILE__, __LINE__);

            auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();

            midpoint_velocity.Copy(evaluation_state, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity, __FILE__, __LINE__);

            midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);

            midpoint_velocity.UpdateGhosts(__FILE__, __LINE__);

            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state)
        {
            auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

            if (GetSnes().GetSnesIteration(__FILE__, __LINE__) == 0)
            {
                velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity(), __FILE__, __LINE__);
            } else
            {
                const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();

                auto& diff_displacement = GetNonCstVectorDiffDisplacement();

                diff_displacement.Copy(evaluation_state, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement, __FILE__, __LINE__);
                diff_displacement.Scale(2. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
                velocity_at_newton_iteration.Copy(diff_displacement, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(
                    -1., velocity_previous_time_iteration, velocity_at_newton_iteration, __FILE__, __LINE__);
            }

            velocity_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
        {
            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                AssembleNewtonStaticOperators(evaluation_state);
                break;
            case StaticOrDynamic::dynamic_:
                AssembleNewtonDynamicOperators();
                break;
            }
        }


        void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(evaluation_state));
            }

            const std::size_t newton_iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);

            if (newton_iteration == 0)
            {
                const double time = parent::GetTimeManager().GetTime();

                auto& vector_surfacic_force_face1 = GetNonCstVectorSurfacicForceFace1();
                vector_surfacic_force_face1.ZeroEntries(__FILE__, __LINE__);

                auto& vector_surfacic_force_face2 = GetNonCstVectorSurfacicForceFace2();
                vector_surfacic_force_face2.ZeroEntries(__FILE__, __LINE__);

                auto& vector_surfacic_force_face3 = GetNonCstVectorSurfacicForceFace3();
                vector_surfacic_force_face3.ZeroEntries(__FILE__, __LINE__);

                auto& vector_surfacic_force_face4 = GetNonCstVectorSurfacicForceFace4();
                vector_surfacic_force_face4.ZeroEntries(__FILE__, __LINE__);

                auto& vector_surfacic_force_face5 = GetNonCstVectorSurfacicForceFace5();
                vector_surfacic_force_face5.ZeroEntries(__FILE__, __LINE__);

                auto& vector_surfacic_force_face6 = GetNonCstVectorSurfacicForceFace6();
                vector_surfacic_force_face6.ZeroEntries(__FILE__, __LINE__);

                GlobalVectorWithCoefficient vec_face1(vector_surfacic_force_face1, 1.);
                GlobalVectorWithCoefficient vec_face2(vector_surfacic_force_face2, 1.);
                GlobalVectorWithCoefficient vec_face3(vector_surfacic_force_face3, 1.);
                GlobalVectorWithCoefficient vec_face4(vector_surfacic_force_face4, 1.);
                GlobalVectorWithCoefficient vec_face5(vector_surfacic_force_face5, 1.);
                GlobalVectorWithCoefficient vec_face6(vector_surfacic_force_face6, 1.);

                GetSurfacicForceOperatorFace1().Assemble(std::make_tuple(std::ref(vec_face1)), time);
                GetSurfacicForceOperatorFace2().Assemble(std::make_tuple(std::ref(vec_face2)), time);
                GetSurfacicForceOperatorFace3().Assemble(std::make_tuple(std::ref(vec_face3)), time);
                GetSurfacicForceOperatorFace4().Assemble(std::make_tuple(std::ref(vec_face4)), time);
                GetSurfacicForceOperatorFace5().Assemble(std::make_tuple(std::ref(vec_face5)), time);
                GetSurfacicForceOperatorFace6().Assemble(std::make_tuple(std::ref(vec_face6)), time);
            }
        }

        void VariationalFormulation::AssembleNewtonDynamicOperators()
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                const GlobalVector& displacement_vector = GetVectorMidpointPosition();

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(displacement_vector));
            }
        }


        void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
        {
            UpdateVectorsAndMatrices(evaluation_state);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticResidual(residual);
                break;
            case StaticOrDynamic::dynamic_:
                ComputeDynamicResidual(evaluation_state, residual);
                break;
            }

            ApplyEssentialBoundaryCondition(residual);
        }


        void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
        {
            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace1(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace2(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace3(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace4(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace5(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace6(), residual, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeDynamicResidual(const GlobalVector& evaluation_state,
                                                            GlobalVector& residual)
        {
            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            const auto& time_manager = GetTimeManager();
            const double time_step = time_manager.GetTimeStep();
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            auto& diff_displacement = GetNonCstVectorDiffDisplacement();
            diff_displacement.Copy(evaluation_state, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-time_step, velocity_previous_time_iteration, diff_displacement, __FILE__, __LINE__);

            Wrappers::Petsc::MatMultAdd(
                GetMatrixMassPerSquareTimeStep(), diff_displacement, residual, residual, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeTangent(const GlobalVector& evaluation_state,
                                                    GlobalMatrix& tangent,
                                                    GlobalMatrix& preconditioner)
        {
            assert(tangent.Internal() == preconditioner.Internal());
            static_cast<void>(evaluation_state);
            static_cast<void>(preconditioner);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticTangent(tangent);
                break;
            case StaticOrDynamic::dynamic_:
                ComputeDynamicTangent(tangent);
                break;
            }

            ApplyEssentialBoundaryCondition(tangent);
        }


        void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
        {
            tangent.ZeroEntries(__FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeDynamicTangent(GlobalMatrix& tangent)
        {
            tangent.ZeroEntries(__FILE__, __LINE__);

#ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
            AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), tangent);
#endif // NDEBUG

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(
                1., GetMatrixMassPerSquareTimeStep(), tangent, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(0.5, GetMatrixTangentStiffness(), tangent, __FILE__, __LINE__);
        }


        void VariationalFormulation::PrepareDynamicRuns()
        {
            DefineDynamicOperators();

            AssembleDynamicOperators();

            UpdateForNextTimeStep();
        }


        void VariationalFormulation::DefineDynamicOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume, displacement_ptr, displacement_ptr);
        }


        void VariationalFormulation::AssembleDynamicOperators()
        {
            const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();

            const double time_step = GetTimeManager().GetTimeStep();

            const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);

            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(), mass_coefficient);

            GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }


        void VariationalFormulation::UpdateForNextTimeStep()
        {
            UpdateDisplacementBetweenTimeStep();

            UpdateVelocityBetweenTimeStep();

            // ComputeGuessForNextTimeStep();
        }


        void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
        {
            GetNonCstVectorCurrentDisplacement().Copy(
                GetSystemSolution(GetDisplacementNumberingSubset()), __FILE__, __LINE__);

            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::UpdateVelocityBetweenTimeStep()
        {
            GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration(), __FILE__, __LINE__);

            GetNonCstVectorCurrentVelocity().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeGuessForNextTimeStep()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

            Wrappers::Petsc::AXPY(
                GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution, __FILE__, __LINE__);

            system_solution.UpdateGhosts(__FILE__, __LINE__);
        }


    } // namespace TestNS::Microsphere


} // namespace MoReFEM
