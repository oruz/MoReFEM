/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE non_linear_membrane_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Enum.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/InputData.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::NonLinearMembraneOperatorNS::Model>;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(no_pretension, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(pretension, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()
