/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 21:38:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_EXPECTED_RESULTS_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_EXPECTED_RESULTS_HPP_

#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Enum.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================0


    namespace TestNS::NonLinearMembraneOperatorNS
    {


        /*!
         * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
         * \a IsMatrixOrVectorT template parameter.
         *
         * \tparam IsMatrixOrVectorT Whether matrix or vector is considered.
         */
        template<IsMatrixOrVector IsMatrixOrVectorT>
        // clang-format off
        using expected_results_type =
            std::conditional_t
            <
                IsMatrixOrVectorT == IsMatrixOrVector::matrix,
                std::vector<std::vector<PetscScalar>>,
                std::vector<PetscScalar>
            >;
        // clang-format on


        /*!
         * \brief Returns the expected matrix when unknown and test unknowns are both P1.
         *
         * \param[in] is_pretension Whether a pretension is used or not.
         *
         * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1(pretension is_pretension);


        /*!
         * \brief Returns the expected vector when test unknown is P1.
         *
         * \param[in] is_pretension Whether a pretension is used or not.
         *
         * \return The expected content of the vector, in \a content_type format.
         */
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1P1(pretension is_pretension);


    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_EXPECTED_RESULTS_HPP_
