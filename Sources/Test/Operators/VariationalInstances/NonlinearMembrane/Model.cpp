/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"
#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS::NonLinearMembraneOperatorNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
        { }


        void Model::SupplInitialize()
        {
            // Required to enable construction of an operator after initialization step.
            parent::SetClearGodOfDofTemporaryDataToFalse();
        }


        void Model::SupplInitializeStep()
        { }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM
