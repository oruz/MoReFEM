/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_ENUM_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_ENUM_HPP_


namespace MoReFEM
{


    namespace TestNS::NonLinearMembraneOperatorNS
    {


        //! Whether pretension is considered or not.
        enum class pretension
        {
            no,
            yes
        };


    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_ENUM_HPP_
