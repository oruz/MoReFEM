/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                constexpr double one_3rd = 1. / 3.;
                constexpr double one_6th = 1. / 6.;

                return expected_results_type<IsMatrixOrVector::matrix>{ { 0., 0., 0., 0. },
                                                                        { 0., 0., 0., 0. },
                                                                        { 0., 0., 0., 0. },
                                                                        { 0., 0., 0., 0. },
                                                                        { one_3rd, 0., -one_6th, -one_6th },
                                                                        { -one_3rd, one_6th, one_6th, 0. },
                                                                        { one_3rd, -one_6th, 0., -one_6th },
                                                                        { one_3rd, -one_6th, -one_6th, 0. },
                                                                        { -one_3rd, one_6th, 0., one_6th },
                                                                        { -one_3rd, 0., one_6th, one_6th } };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                constexpr double one_3rd = 1. / 3.;
                constexpr double one_6th = 1. / 6.;
                constexpr double two_3rd = 2. * one_3rd;
                constexpr double four_3rd = 4. * one_3rd;

                return expected_results_type<IsMatrixOrVector::matrix>{
                    { one_3rd, -one_6th, -one_6th }, { -one_6th, one_6th, 0. },       { -one_6th, 0., one_6th },
                    { two_3rd, 0., -two_3rd },       { -four_3rd, two_3rd, two_3rd }, { two_3rd, -two_3rd, 0. }
                };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>{ { 1., -1. }, { -1., 1. }, { 0., 0. } };
            }


        } // namespace


    } // namespace TestNS::GradGrad


} // namespace MoReFEM
