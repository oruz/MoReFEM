/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            domain = 1u
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            felt_space = 1u
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            scalar_unknown_P1 = 1u,
            other_scalar_unknown_P1 = 2u,
            scalar_unknown_P2 = 3u,
            vectorial_unknown_P1 = 4u
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            scalar_unknown_P1 = 1u,
            other_scalar_unknown_P1 = 2u,
            scalar_unknown_P2 = 3u,
            vectorial_unknown_P1 = 4u
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1u
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_scalar_unknown_P1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial_unknown_P1)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_scalar_unknown_P1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P2)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_P1)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNS::GradGrad


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_INPUT_DATA_HPP_
