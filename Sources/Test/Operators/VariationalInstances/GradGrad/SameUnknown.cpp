/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        void Model::SameUnknownP1(UnknownNS::Nature scalar_or_vectorial) const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto unknown_id = static_cast<std::size_t>(EnumUnderlyingType(
                scalar_or_vectorial == UnknownNS::Nature::scalar ? UnknownIndex::scalar_unknown_P1
                                                                 : UnknownIndex::vectorial_unknown_P1));

            const auto numbering_subset_id = static_cast<std::size_t>(EnumUnderlyingType(
                scalar_or_vectorial == UnknownNS::Nature::scalar ? NumberingSubsetIndex::scalar_unknown_P1
                                                                 : NumberingSubsetIndex::vectorial_unknown_P1));


            const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

            GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(felt_space, unknown_ptr, unknown_ptr);

            decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(numbering_subset_id);

            GlobalMatrix matrix(numbering_subset, numbering_subset);
            AllocateGlobalMatrix(god_of_dof, matrix);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

            variational_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

            /* BOOST_CHECK_NO_THROW */ (CheckMatrix(
                god_of_dof, matrix, GetExpectedMatrixP1P1(dimension, scalar_or_vectorial), __FILE__, __LINE__, 1.e-5));
        }


        void Model::SameUnknownP2() const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& unknown_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::scalar_unknown_P2));

            GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(felt_space, unknown_ptr, unknown_ptr);

            decltype(auto) numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2));

            GlobalMatrix matrix(numbering_subset, numbering_subset);
            AllocateGlobalMatrix(god_of_dof, matrix);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

            variational_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

            /* BOOST_CHECK_NO_THROW */ (
                CheckMatrix(god_of_dof, matrix, GetExpectedMatrixP2P2(dimension), __FILE__, __LINE__, 1.e-5));
        }


    } // namespace TestNS::GradGrad


} // namespace MoReFEM
