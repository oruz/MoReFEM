/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        void Model::UnknownP1TestP1() const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& scalar_unknown_P1_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::scalar_unknown_P1));

            const auto& other_scalar_unknown_P1_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::other_scalar_unknown_P1));


            GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(
                felt_space, scalar_unknown_P1_ptr, other_scalar_unknown_P1_ptr);

            decltype(auto) numbering_subset_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1));
            decltype(auto) numbering_subset_other_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::other_scalar_unknown_P1));

            GlobalMatrix matrix_p1_p1(numbering_subset_other_p1, numbering_subset_p1);
            AllocateGlobalMatrix(god_of_dof, matrix_p1_p1);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix_p1_p1.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix(matrix_p1_p1, 1.);

            variational_operator.Assemble(std::make_tuple(std::ref(matrix)));

            /* BOOST_CHECK_NO_THROW */ (CheckMatrix(god_of_dof,
                                                    matrix_p1_p1,
                                                    GetExpectedMatrixP1P1(dimension, UnknownNS::Nature::scalar),
                                                    __FILE__,
                                                    __LINE__,
                                                    1.e-5));
        }


    } // namespace TestNS::GradGrad


} // namespace MoReFEM
