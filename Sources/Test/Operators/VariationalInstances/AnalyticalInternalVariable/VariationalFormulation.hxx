//! \file
//
//
//  VariationalFormulation.hxx
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace TestNS::AnalyticalInternalVariable
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const VariationalFormulation::StiffnessOperatorType&
        VariationalFormulation::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperatorFace1() const noexcept
        {
            assert(!(!surfacic_force_operator_face_1_));
            return *surfacic_force_operator_face_1_;
        }


        inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
        {
            assert(!(!vector_stiffness_residual_));
            return *vector_stiffness_residual_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
        {
            assert(!(!matrix_tangent_stiffness_));
            return *matrix_tangent_stiffness_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace1() const noexcept
        {
            assert(!(!vector_surfacic_force_face_1_));
            return *vector_surfacic_force_face_1_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace1() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForceFace1());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
        {
            assert(!(!vector_current_displacement_));
            return *vector_current_displacement_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
        }


        inline const Solid& VariationalFormulation::GetSolid() const noexcept
        {
            assert(!(!solid_));
            return *solid_;
        }


        inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const
        {
            return displacement_numbering_subset_;
        }


        inline const NumberingSubset& VariationalFormulation::GetElectricalActivationNumberingSubset() const
        {
            return electrical_activation_numbering_subset_;
        }


    } // namespace TestNS::AnalyticalInternalVariable


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_VARIATIONAL_FORMULATION_HXX_
