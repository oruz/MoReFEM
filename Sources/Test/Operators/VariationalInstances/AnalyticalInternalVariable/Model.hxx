//! \file
//
//
//  Model.hxx
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::AnalyticalInternalVariable
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("Test AnalyticalInternalVariable");
            return name;
        }


        inline const VariationalFormulation& Model::GetVariationalFormulation() const
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
        {
            return const_cast<VariationalFormulation&>(GetVariationalFormulation());
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


    } // namespace TestNS::AnalyticalInternalVariable


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_MODEL_HXX_
