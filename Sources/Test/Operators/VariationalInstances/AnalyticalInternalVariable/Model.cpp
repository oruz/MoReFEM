//! \file
//
//
//  Model.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include "Model.hpp"


namespace MoReFEM
{


    namespace TestNS::AnalyticalInternalVariable
    {

        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes)
        { }


        void Model::SupplInitialize()
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();

            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));

            {
                const DirichletBoundaryConditionManager& bc_manager =
                    DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

                auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr("edge1"),
                                   bc_manager.GetDirichletBoundaryConditionPtr("edge2"),
                                   bc_manager.GetDirichletBoundaryConditionPtr("edge3") };

                const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

                const auto& displacement = unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::displacement));
                const auto& electrical_activation =
                    unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::electrical_activation));

                variational_formulation_ = std::make_unique<VariationalFormulation>(
                    morefem_data,
                    felt_space_volume.GetNumberingSubset(displacement),
                    felt_space_volume.GetNumberingSubset(electrical_activation),
                    GetNonCstTimeManager(),
                    god_of_dof,
                    std::move(bc_list));
            }

            auto& variational_formulation = GetNonCstVariationalFormulation();

            variational_formulation.Init(morefem_data);

            const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();
            const auto& electrical_activation_numbering_subset =
                variational_formulation.GetElectricalActivationNumberingSubset();

            const auto& mpi = GetMpi();

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "----------------------------------------------\n", mpi, __FILE__, __LINE__);


            variational_formulation.SolveNonLinear(
                displacement_numbering_subset, displacement_numbering_subset, __FILE__, __LINE__);

            variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
            variational_formulation.WriteSolution(GetTimeManager(), electrical_activation_numbering_subset);
        }


        void Model::SupplInitializeStep()
        { }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }

    } // namespace TestNS::AnalyticalInternalVariable


} // namespace MoReFEM
