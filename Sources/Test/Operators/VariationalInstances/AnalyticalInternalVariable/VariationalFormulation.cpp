//! \file
//
//
//  VariationalFormulation.cpp
//  MoReFEM
//
//  Created by Jérôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"

#include "VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace TestNS::AnalyticalInternalVariable
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation::VariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& displacement_numbering_subset,
            const NumberingSubset& electrical_activation_numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          displacement_numbering_subset_(displacement_numbering_subset),
          electrical_activation_numbering_subset_(electrical_activation_numbering_subset)
        {
            assert(time_manager.IsTimeStepConstant() && "Current instantiation relies on this assumption!");
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            const auto& electrical_activation_numbering_subset = GetElectricalActivationNumberingSubset();

            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);

            parent::AllocateSystemVector(electrical_activation_numbering_subset);

            const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_face_1_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

            vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            namespace IPL = Utilities::InputDataNS;

            std::size_t degree_of_exactness = 2;
            std::size_t shape_function_order = 1;

            quadrature_rule_per_topology_parameter_ =
                std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

            quadrature_rule_per_topology_for_operators_ =
                std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);
            auto& fiber = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                    ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__)
                              .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber));

            fiber.Initialize(quadrature_rule_per_topology_parameter_.get());

            decltype(auto) domain_full_mesh =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            decltype(auto) input_data = morefem_data.GetInputData();

            solid_ =
                std::make_unique<Solid>(input_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

            input_analytical_prestress_ = std::make_unique<InputAnalyticalPrestress>(input_data, domain_full_mesh);

            DefineStaticOperators(input_data);
        }


        void VariationalFormulation::DefineStaticOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const auto& felt_space_face1 = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::face1));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;

            hyperelastic_law_parent::Create(GetSolid());

            InputAnalyticalPrestress* raw_input_analytical_prestress = input_analytical_prestress_.get();

            stiffness_operator_ =
                std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                        displacement_ptr,
                                                        displacement_ptr,
                                                        GetSolid(),
                                                        GetTimeManager(),
                                                        hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                        quadrature_rule_per_topology_for_operators_.get(),
                                                        raw_input_analytical_prestress);

            decltype(auto) domain_force_face_1 =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::face1), __FILE__, __LINE__);

            using parameter_type_face1 =
                InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>;

            force_parameter_face_1_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type_face1>(
                    "Surfacic force", domain_force_face_1, input_data);

            if (force_parameter_face_1_ != nullptr)
            {
                surfacic_force_operator_face_1_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_face1, displacement_ptr, *force_parameter_face_1_);
            }
        }


        void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
        {
            AssembleOperators(evaluation_state);
        }

        void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
        {
            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                AssembleNewtonStaticOperators(evaluation_state);
                break;
            case StaticOrDynamic::dynamic_:
                break;
            }
        }


        void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                const auto& electrical_activation_numbering_subset = GetElectricalActivationNumberingSubset();
                const GlobalVector& u0 = GetSystemRhs(electrical_activation_numbering_subset);
                const GlobalVector& u1 = GetSystemSolution(electrical_activation_numbering_subset);

                const bool do_update_sigma_c = GetSnes().GetSnesIteration(__FILE__, __LINE__) == 0 ? true : false;

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(evaluation_state),
                                                PreviousElectricalActivationGlobalVector(u0),
                                                CurrentElectricalActivationGlobalVector(u1),
                                                do_update_sigma_c);
            }

            const std::size_t newton_iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);

            if (newton_iteration == 0)
            {
                const double time = parent::GetTimeManager().GetTime();

                auto& vector_surfacic_force_face1 = GetNonCstVectorSurfacicForceFace1();
                vector_surfacic_force_face1.ZeroEntries(__FILE__, __LINE__);

                GlobalVectorWithCoefficient vec_face1(vector_surfacic_force_face1, 1.);

                GetSurfacicForceOperatorFace1().Assemble(std::make_tuple(std::ref(vec_face1)), time);
            }
        }


        void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
        {
            UpdateVectorsAndMatrices(evaluation_state);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticResidual(residual);
                break;
            case StaticOrDynamic::dynamic_:
                break;
            }

            ApplyEssentialBoundaryCondition(residual);
        }


        void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
        {
            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace1(), residual, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeTangent(const GlobalVector& evaluation_state,
                                                    GlobalMatrix& tangent,
                                                    GlobalMatrix& preconditioner)
        {
            assert(tangent.Internal() == preconditioner.Internal());
            static_cast<void>(evaluation_state);
            static_cast<void>(preconditioner);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticTangent(tangent);
                break;
            case StaticOrDynamic::dynamic_:
                break;
            }

            ApplyEssentialBoundaryCondition(tangent);
        }


        void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
        {
            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent, __FILE__, __LINE__);
        }


    } // namespace TestNS::AnalyticalInternalVariable


} // namespace MoReFEM
