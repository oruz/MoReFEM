//! \file
//
//
//  main_ensight_output.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 23/01/2019.
// Copyright © 2019 Jerôme Diaz. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"
#include "PostProcessing/PostProcessing.hpp"

#include "InputData.hpp"
#include "Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::AnalyticalInternalVariable;


int main(int argc, char** argv)
{

    try
    {
        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = TestNS::AnalyticalInternalVariable::InputData;

        MoReFEMData<InputData, program_type::post_processing, Utilities::InputDataNS::DoTrackUnusedFields::no>
            morefem_data(argc, argv);

        const auto& input_data = morefem_data.GetInputData();
        const auto& mpi = morefem_data.GetMpi();

        try
        {
            namespace ipl = Utilities::InputDataNS;

            using Result = InputDataNS::Result;
            decltype(auto) result_directory_path = ipl::Extract<Result::OutputDirectory>::Path(input_data);

            FilesystemNS::Directory result_directory(
                mpi, result_directory_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

            decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
            MoReFEM::Advanced::SetFromInputData<>(input_data, mesh_manager);

            const Mesh& mesh = mesh_manager.GetMesh(EnumUnderlyingType(MeshIndex::mesh));

            {
                decltype(auto) manager =
                    Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
                MoReFEM::Advanced::SetFromInputData<>(input_data, manager);
            }

            {
                std::vector<std::size_t> numbering_subset_id_list{
                    EnumUnderlyingType(NumberingSubsetIndex::displacement),
                    EnumUnderlyingType(NumberingSubsetIndex::electrical_activation)

                };

                std::vector<std::string> unknown_list{ "displacement", "electrical_activation" };

                PostProcessingNS::OutputFormat::Ensight6 ensight_output(
                    result_directory, unknown_list, numbering_subset_id_list, mesh);
            }

            std::cout << "End of Post-Processing." << std::endl;
            std::cout << TimeKeep::GetInstance(__FILE__, __LINE__).TimeElapsedSinceBeginning() << std::endl;
        }
        catch (const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch (const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }


    return EXIT_SUCCESS;
}
