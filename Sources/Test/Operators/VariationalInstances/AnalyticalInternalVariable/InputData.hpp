//! \file
//
//
//  InputData.hpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS::AnalyticalInternalVariable
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement = 1,
            electrical_activation = 2
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement = 1,
            electrical_activation = 2
        };

        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fiber = 1
        };

        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            highest_dimension = 1,
            face1 = 2,
            full_mesh = 3,
            all_faces = 4,
            edge1 = 5,
            edge2 = 6,
            edge3 = 7
        };

        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            edge1 = 1,
            edge2 = 2,
            edge3 = 3
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            face1 = 2
        };

        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic_force_face1 = 1,
        };

        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::electrical_activation)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::electrical_activation)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>,
            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>,
            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::CheckInvertedElements,

            InputDataNS::AnalyticalPrestress,

            InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiber),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace TestNS::AnalyticalInternalVariable


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_ANALYTICAL_INTERNAL_VARIABLE_x_INPUT_DATA_HPP_
