/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/Mass/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{


    namespace TestNS::Mass
    {


        void Model::UnknownP2TestP1() const
        {
            const auto& god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));

            const auto& potential_1_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_P1));

            const auto& potential_2_ptr = unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::potential_P2));


            GlobalVariationalOperatorNS::Mass mass_op(felt_space, potential_1_ptr, potential_2_ptr);

            decltype(auto) numbering_subset_p1 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_P1));
            decltype(auto) numbering_subset_p2 =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_P2));

            GlobalMatrix matrix(numbering_subset_p2, numbering_subset_p1);
            AllocateGlobalMatrix(god_of_dof, matrix);

            const auto& mesh = god_of_dof.GetMesh();
            const auto dimension = mesh.GetDimension();

            matrix.ZeroEntries(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

            mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

            /* BOOST_CHECK_NO_THROW */ (
                CheckMatrix(god_of_dof, matrix, GetExpectedMatrixP2P1(dimension), __FILE__, __LINE__, 1.e-5));
        }


    } // namespace TestNS::Mass


} // namespace MoReFEM
