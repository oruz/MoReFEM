/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 21:38:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_EXPECTED_RESULTS_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_EXPECTED_RESULTS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/Unknown/EnumUnknown.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================0


    namespace TestNS::Mass
    {


        /*!
         * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
         * \a IsMatrixOrVectorT template parameter.
         *
         * \tparam IsMatrixOrVectorT Whether matrix or vector is considered.
         */
        // clang-format off
        template<IsMatrixOrVector IsMatrixOrVectorT>
        using expected_results_type =
            std::conditional_t
            <
                IsMatrixOrVectorT == IsMatrixOrVector::matrix,
                std::vector<std::vector<PetscScalar>>,
                std::vector<PetscScalar>
            >;
        // clang-format on


        /*!
         * \brief Returns the expected matrix when unknown and test unknowns are both P1.
         *
         * \param[in] dimension Dimension of the mesh considered.
         * \param[in] scalar_or_vectorial Whether we're considering a scalar or a vectorial unknown.
         *
         * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1(std::size_t dimension,
                                                                              UnknownNS::Nature scalar_or_vectorial);


        /*!
         * \brief Returns the expected matrix when unknown is P2 and test unknowns is P1.
         *
         * \param[in] dimension Dimension of the mesh considered.
         *
         * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P1(std::size_t dimension);

        /*!
         * \brief Returns the expected matrix when unknown is P2 and test unknowns is P2.
         *
         * \param[in] dimension Dimension of the mesh considered.
         *
         * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
         */
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P2(std::size_t dimension);


    } // namespace TestNS::Mass


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_EXPECTED_RESULTS_HPP_
