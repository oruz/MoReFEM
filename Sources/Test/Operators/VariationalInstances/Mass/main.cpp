/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE mass_operator_3d
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/MacroVariationalOperator.hpp"

#include "Test/Operators/VariationalInstances/Mass/InputData.hpp"
#include "Test/Operators/VariationalInstances/Mass/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::Mass::Model>;


} // namespace


TEST_VARIATIONAL_OPERATOR
