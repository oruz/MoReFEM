/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::Mass
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P2(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                constexpr double one_420th = 1. / 420.;
                constexpr double one_2520th = 1. / 2520.;
                constexpr double one_630th = 1. / 630.;
                constexpr double one_315th = 1. / 315.;

                return expected_results_type<IsMatrixOrVector::matrix>{ { one_420th,
                                                                          one_2520th,
                                                                          one_2520th,
                                                                          one_2520th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_420th },
                                                                        { one_2520th,
                                                                          one_420th,
                                                                          one_2520th,
                                                                          one_2520th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_420th },
                                                                        { one_2520th,
                                                                          one_2520th,
                                                                          one_420th,
                                                                          one_2520th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          -one_630th },
                                                                        { one_2520th,
                                                                          one_2520th,
                                                                          one_2520th,
                                                                          one_420th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          -one_630th },
                                                                        { -one_630th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          4. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          one_315th },
                                                                        { -one_420th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          2. * one_315th,
                                                                          4. * one_315th,
                                                                          2. * one_315th,
                                                                          one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th },
                                                                        { -one_630th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          4. * one_315th,
                                                                          2. * one_315th,
                                                                          one_315th,
                                                                          2. * one_315th },
                                                                        { -one_630th,
                                                                          -one_420th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          2. * one_315th,
                                                                          one_315th,
                                                                          2. * one_315th,
                                                                          4. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th },
                                                                        { -one_420th,
                                                                          -one_630th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          one_315th,
                                                                          2. * one_315th,
                                                                          4. * one_315th,
                                                                          2. * one_315th },
                                                                        { -one_420th,
                                                                          -one_420th,
                                                                          -one_630th,
                                                                          -one_630th,
                                                                          one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          2. * one_315th,
                                                                          4. * one_315th } };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                constexpr double one_60th = 1. / 60.;
                constexpr double minus_one_90th = -1. / 90.;
                constexpr double two_45th = 2. / 45.;
                constexpr double four_45th = 4. / 45.;
                constexpr double minus_one_360th = -1. / 360.;

                return expected_results_type<IsMatrixOrVector::matrix>{
                    { one_60th, minus_one_360th, minus_one_360th, 0., minus_one_90th, 0. },
                    { minus_one_360th, one_60th, minus_one_360th, 0., 0., minus_one_90th },
                    { minus_one_360th, minus_one_360th, one_60th, minus_one_90th, 0., 0. },
                    { 0., 0., minus_one_90th, four_45th, two_45th, two_45th },
                    { minus_one_90th, 0., 0., two_45th, four_45th, two_45th },
                    { 0., minus_one_90th, 0., two_45th, two_45th, four_45th }
                };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                constexpr auto one_15th = 1. / 15.;
                constexpr auto two_15th = 2. / 15.;
                constexpr auto minus_one_30th = -1. / 30.;

                return expected_results_type<IsMatrixOrVector::matrix>{ { two_15th, minus_one_30th, one_15th },
                                                                        { minus_one_30th, two_15th, one_15th },
                                                                        { one_15th, one_15th, 8. * one_15th } };
            }


        } // namespace


    } // namespace TestNS::Mass


} // namespace MoReFEM
