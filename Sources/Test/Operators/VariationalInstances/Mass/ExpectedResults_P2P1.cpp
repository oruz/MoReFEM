/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Mar 2018 15:08:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::Mass
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                constexpr double one_90th = 1. / 90.;
                constexpr double one_180th = 1. / 180.;
                constexpr double one_360th = 1. / 360.;

                return expected_results_type<IsMatrixOrVector::matrix>{
                    { 0., -one_360th, -one_360th, -one_360th },   { -one_360th, 0., -one_360th, -one_360th },
                    { -one_360th, -one_360th, 0., -one_360th },   { -one_360th, -one_360th, -one_360th, 0. },
                    { one_90th, one_90th, one_180th, one_180th }, { one_180th, one_90th, one_90th, one_180th },
                    { one_90th, one_180th, one_90th, one_180th }, { one_90th, one_180th, one_180th, one_90th },
                    { one_180th, one_90th, one_180th, one_90th }, { one_180th, one_180th, one_90th, one_90th }
                };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                constexpr double one_60th = 1. / 60.;
                constexpr double minus_one_120th = -1. / 120.;
                constexpr double one_15th = 1. / 15.;
                constexpr double one_30th = 1. / 30.;

                return expected_results_type<IsMatrixOrVector::matrix>{ { one_60th, minus_one_120th, minus_one_120th },
                                                                        { minus_one_120th, one_60th, minus_one_120th },
                                                                        { minus_one_120th, minus_one_120th, one_60th },
                                                                        { one_15th, one_15th, one_30th },
                                                                        { one_30th, one_15th, one_15th },
                                                                        { one_15th, one_30th, one_15th } };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                constexpr double one_sixth = 1. / 6.;
                constexpr double one_third = 1. / 3.;

                return expected_results_type<IsMatrixOrVector::matrix>{ { one_sixth, 0. },
                                                                        { 0., one_sixth },
                                                                        { one_third, one_third } };
            }


        } // namespace


    } // namespace TestNS::Mass


} // namespace MoReFEM
