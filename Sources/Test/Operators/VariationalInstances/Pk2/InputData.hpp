/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace TestNS::Pk2
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            volume = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            felt_space = 1,
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement_p1 = 1,
            displacement_p2 = 2,
            other_displacement_p1 = 3
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement_p1 = 1,
            displacement_p2 = 2,
            other_displacement_p1 = 3
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1 )>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::felt_space)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::LameLambda,
            InputDataNS::Solid::LameMu,
            InputDataNS::Solid::Viscosity,
            InputDataNS::Solid::CheckInvertedElements,

            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNS::Pk2


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_PK2_x_INPUT_DATA_HPP_
