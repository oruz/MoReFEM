/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/Pk2/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/Pk2/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::Pk2
    {


        Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
        {
            decltype(auto) mpi = parent::GetMpi();

            if (mpi.Nprocessor<int>() > 1)
            {
                throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                                "a vector; the expected values assume the dof numbering of the sequential case. Please "
                                "run it sequentially.",
                                __FILE__,
                                __LINE__);
            }
        }


        void Model::SupplInitialize()
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            decltype(auto) input_data = morefem_data.GetInputData();

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) domain_volume =
                domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);

            young_modulus_ = InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>(
                "Young modulus", domain_volume, input_data);

            poisson_ratio_ = InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>(
                "Poisson ratio", domain_volume, input_data);

            quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(10, 3);

            const auto& felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));


            solid_ = std::make_unique<Solid>(input_data,
                                             domain_volume,
                                             felt_space.GetQuadratureRulePerTopology(),
                                             -1.); // Negative to cancel CheckConsistancy issues.

            hyperelastic_law_parent::Create(*solid_);

            // Required to enable construction of an operator after initialization step.
            parent::SetClearGodOfDofTemporaryDataToFalse();
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestNS::Pk2


} // namespace MoReFEM
