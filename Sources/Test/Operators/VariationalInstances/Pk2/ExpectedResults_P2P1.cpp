/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Mar 2018 16:50:27 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Pk2/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::Pk2
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();
            expected_results_type<IsMatrixOrVector::vector> Vector3D();
            expected_results_type<IsMatrixOrVector::vector> Vector2D();
            expected_results_type<IsMatrixOrVector::vector> Vector1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP2P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP2P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Vector3D();
            case 2u:
                return Vector2D();
            case 1u:
                return Vector1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>

                    //                {
                    //                    {
                    //                        1.42557, -0.369358, -0.369358, 1.94944, -0.518319, -0.518319,
                    //                        -0.289351, -0.235915, 0.00200913, -0.289351, 0.00200913, -0.235915,
                    //                        6.55771, -3.81767, -3.81767, -9.5386, 5.12198, 4.41767, 11.5203,
                    //                        -4.92289, -3.55621, 11.5203, -3.55621, -4.92289, -9.5386, 4.41767,
                    //                        5.12198, -13.3173, 3.87871, 3.87871
                    //                    },

                    //                    {
                    //                        -0.369358, 1.42557, -0.369358, -0.235915, -0.289351, 0.00200913,
                    //                        -0.518319, 1.94944, -0.518319, 0.00200913, -0.289351, -0.235915,
                    //                        -4.92289, 11.5203, -3.55621, 5.12198, -9.5386, 4.41767, -3.81767, 6.55771,
                    //                        -3.81767, -3.55621, 11.5203, -4.92289, 3.87871,
                    //                        -13.3173, 3.87871, 4.41767, -9.5386, 5.12198
                    //                    },
                    //                    {
                    //                        -0.369358, -0.369358, 1.42557, -0.235915, 0.00200913, -0.289351,
                    //                        0.00200913, -0.235915, -0.289351, -0.518319, -0.518319, 1.94944, -4.92289,
                    //                        -3.55621, 11.5203, 3.87871, 3.87871, -13.3173, -3.55621,
                    //                        -4.92289, 11.5203, -3.81767, -3.81767, 6.55771, 5.12198, 4.41767,
                    //                        -9.5386, 4.41767, 5.12198, -9.5386
                    //
                    //                    },
                    //                    {
                    //                        -0.0672069, -0.0775591, -0.0775591, -0.90369, 0.196631, 0.196631,
                    //                        0.0998427, -0.119881, -0.0166438, 0.0998427, -0.0166438,
                    //                        -0.119881, 1.1545, -0.367515, -0.367515, 3.91215, -1.92861, -1.74532,
                    //                        -4.82034, 2.9122, 1.21639, -4.82034, 1.21639, 2.9122, 3.91215, -1.74532,
                    //                        -1.92861, 1.4331, -0.0696992, -0.0696992
                    //                    },
                    //                    {
                    //                        0.48797, -0.679182, -0.0410524, 0.196631, 0.0714767, 0.0629398, 0.0845791,
                    //                        -0.522877, 0.237109, -0.0483051, 0.118032, 0.159165, -0.110229,
                    //                        -2.5446, 1.72351, -1.24901, 3.23031, -2.1733, 3.20332, -3.85611, 0.981868,
                    //                        0.616306, -4.15532, 2.12092, -2.6822, 5.94212, -1.12681,
                    //                        -0.499063, 2.39615, -1.94436
                    //                    },
                    //                    {
                    //                        0.48797, -0.0410524, -0.679182, 0.196631, 0.0629398, 0.0714767,
                    //                        -0.0483051, 0.159165, 0.118032, 0.0845791, 0.237109, -0.522877,
                    //                        -0.110229, 1.72351, -2.5446, -2.6822, -1.12681, 5.94212,
                    //                        0.616306, 2.12092, -4.15532, 3.20332, 0.981868, -3.85611, -1.24901,
                    //                        -2.1733, 3.23031, -0.499063, -1.94436, 2.39615
                    //                    },
                    //                    {
                    //                        -0.679182, 0.48797, -0.0410524, -0.522877, 0.0845791, 0.237109, 0.0714767,
                    //                        0.196631, 0.0629398, 0.118032, -0.0483051, 0.159165, -3.85611, 3.20332,
                    //                        0.981868, 3.23031, -1.24901, -2.1733, -2.5446, -0.110229, 1.72351,
                    //                        -4.15532, 0.616306, 2.12092, 2.39615, -0.499063, -1.94436, 5.94212,
                    //                        -2.6822, -1.12681
                    //                    },
                    //                    {
                    //                        -0.0775591, -0.0672069, -0.0775591, -0.119881, 0.0998427, -0.0166438,
                    //                        0.196631, -0.90369, 0.196631, -0.0166438, 0.0998427, -0.119881, 2.9122,
                    //                        -4.82034, 1.21639, -1.92861, 3.91215, -1.74532, -0.367515, 1.1545,
                    //                        -0.367515, 1.21639, -4.82034, 2.9122, -0.0696992, 1.4331, -0.0696992,
                    //                        -1.74532, 3.91215, -1.92861
                    //                    },
                    //                    {
                    //                        -0.0410524, 0.48797, -0.679182, 0.159165, -0.0483051, 0.118032, 0.0629398,
                    //                        0.196631, 0.0714767, 0.237109, 0.0845791, -0.522877, 2.12092, 0.616306,
                    //                        -4.15532, -1.12681, -2.6822, 5.94212, 1.72351, -0.110229, -2.5446,
                    //                        0.981868, 3.20332, -3.85611, -1.94436, -0.499063, 2.39615, -2.1733,
                    //                        -1.24901, 3.23031
                    //                    },
                    //                    {
                    //                        -0.679182, -0.0410524, 0.48797, -0.522877, 0.237109, 0.0845791, 0.118032,
                    //                        0.159165, -0.0483051, 0.0714767, 0.0629398, 0.196631, -3.85611, 0.981868,
                    //                        3.20332, 2.39615, -1.94436, -0.499063, -4.15532, 2.12092, 0.616306,
                    //                        -2.5446, 1.72351, -0.110229, 3.23031, -2.1733, -1.24901, 5.94212,
                    //                        -1.12681, -2.6822
                    //                    },
                    //                    {
                    //                        -0.0410524, -0.679182, 0.48797, 0.159165, 0.118032, -0.0483051, 0.237109,
                    //                        -0.522877, 0.0845791, 0.0629398, 0.0714767, 0.196631, 2.12092, -4.15532,
                    //                        0.616306, -1.94436, 2.39615, -0.499063, 0.981868, -3.85611, 3.20332,
                    //                        1.72351, -2.5446, -0.110229, -1.12681, 5.94212, -2.6822, -2.1733,
                    //                        3.23031, -1.24901
                    //                    },
                    //                    {
                    //                        -0.0775591, -0.0775591, -0.0672069, -0.119881, -0.0166438, 0.0998427,
                    //                        -0.0166438, -0.119881, 0.0998427, 0.196631, 0.196631, -0.90369, 2.9122,
                    //                        1.21639, -4.82034, -0.0696992, -0.0696992, 1.4331, 1.21639, 2.9122,
                    //                        -4.82034, -0.367515, -0.367515, 1.1545, -1.92861, -1.74532, 3.91215,
                    //                        -1.74532, -1.92861, 3.91215
                    //                    },
                    //                };

                    {

                        { 1.263527e-01,  7.676006e-02,  2.799028e-02,  1.635337e-01,  1.732473e-02,  4.281041e-02,
                          5.982329e-02,  2.005053e-02,  2.502572e-02,  9.341812e-03,  -5.465460e-03, 1.237126e-02,
                          1.083678e+00,  -3.355434e-01, -3.401001e-01, -1.812006e+00, -3.370498e-01, -7.324716e-01,
                          1.727378e+00,  5.493984e-01,  4.205642e-01,  1.824948e+00,  4.486969e-01,  6.445379e-01,
                          -1.795269e+00, -7.380983e-01, -3.586986e-01, -1.387781e+00, 3.039263e-01,  2.579706e-01 },

                        { 7.676006e-02,  1.517818e-01,  6.513230e-02,  -3.317239e-02, 1.004483e-02,  -2.624805e-02,
                          7.628164e-02,  2.197505e-01,  4.768459e-02,  1.418015e-02,  3.202732e-02,  5.672448e-02,
                          7.018934e-01,  1.874511e+00,  4.655193e-01,  -3.292143e-01, -1.849039e+00, -6.892086e-01,
                          -4.812351e-01, 9.653804e-01,  -3.725993e-01, 4.418936e-01,  1.821287e+00,  5.657398e-01,
                          2.669089e-01,  -1.441598e+00, 2.747649e-01,  -7.342960e-01, -1.784145e+00, -3.875094e-01 },
                        { 2.799028e-02,  6.513230e-02,  6.118769e-02,  -1.691852e-02, -4.568528e-03, 3.145498e-02,
                          -2.019348e-02, -3.829359e-02, 4.104801e-02,  -2.104654e-02, 2.251755e-03,  8.164302e-02,
                          6.708195e-01,  4.989700e-01,  1.775282e+00,  2.734231e-01,  2.413703e-01,  -1.394893e+00,
                          4.058931e-01,  6.153098e-01,  1.685418e+00,  -3.517106e-01, -4.556200e-01, 1.257897e+00,
                          -3.421659e-01, -7.180405e-01, -1.795847e+00, -6.260909e-01, -2.065115e-01, -1.743190e+00 },
                        { -5.829440e-02, -8.767385e-03, 1.630289e-02,  -1.749993e-01, 1.781601e-02,  1.660701e-03,
                          -4.008353e-02, 1.740385e-02,  -5.130178e-03, -6.763183e-04, -4.463677e-04, -1.182650e-02,
                          2.057888e-01,  2.767204e-01,  2.661453e-01,  1.197775e+00,  -3.383749e-01, -8.992830e-03,
                          -1.128836e+00, 3.526703e-02,  2.838178e-01,  -1.156893e+00, 2.787906e-01,  -1.432926e-02,
                          1.141153e+00,  2.268952e-02,  -2.659633e-01, 1.506547e-02,  -3.010987e-01, -2.616846e-01 },
                        { -7.156155e-02, -4.600323e-02, -4.091307e-03, 1.781601e-02,  -2.873144e-02, 6.865096e-03,
                          -3.832624e-02, -5.802039e-02, -8.650088e-04, -2.111658e-02, -1.394022e-03, -5.690664e-03,
                          -6.474251e-01, 2.054656e-02,  8.323999e-03,  5.789519e-01,  6.955848e-01,  5.064988e-03,
                          1.414928e-01,  -5.821719e-01, -1.653367e-02, -7.269866e-01, -7.424047e-01, 2.955501e-02,
                          2.950816e-02,  7.081685e-01,  -3.038359e-02, 7.376472e-01,  3.442590e-02,  7.755146e-03 },
                        { -3.767210e-02, -3.025037e-02, -1.943607e-02, 1.660701e-03, 6.865096e-03,  -3.904550e-02,
                          2.099729e-02,  -6.146021e-04, -1.949749e-02, 1.467609e-02, 2.106597e-02,  -1.870005e-03,
                          -6.310703e-01, -2.064715e-02, 4.264197e-02,  1.196381e-02, 5.242406e-03,  6.984933e-01,
                          -7.114912e-01, 5.129957e-02,  -6.602236e-01, 7.225625e-02, 2.842780e-02,  -6.960951e-01,
                          6.102957e-01,  -1.589863e-03, 6.888880e-01,  6.483839e-01, -5.979887e-02, 6.144484e-03 },
                        { -4.773030e-02, -5.326942e-02, -2.423655e-02, 1.085227e-02,  -4.228081e-02, 2.069290e-02,
                          -2.688112e-02, -3.735418e-02, -4.387431e-04, -2.094974e-02, -1.229531e-03, 1.146668e-04,
                          -6.503138e-01, 6.435140e-02,  3.257616e-03,  6.104889e-01,  6.699852e-01,  1.601299e-02,
                          8.477911e-02,  -5.999254e-01, -1.415571e-02, -6.917670e-01, -7.500692e-01, 7.220950e-02,
                          2.195800e-02,  7.316644e-01,  -6.980539e-02, 7.095637e-01,  1.812751e-02,  -3.651274e-03 },
                        { 3.085039e-03,  -7.779644e-02, -1.450817e-02, 1.581148e-02,  1.776121e-02,  1.671054e-03,
                          -3.735418e-02, -1.737967e-01, 1.847914e-02,  -2.049218e-04, -4.261693e-02, -9.783407e-03,
                          -4.970984e-02, -1.195152e+00, 2.566451e-01,  -2.497039e-01, 1.141957e+00,  1.910268e-02,
                          3.181225e-01,  2.488584e-01,  2.687809e-01,  2.817933e-01,  -1.126416e+00, 7.717178e-02,
                          -3.016081e-01, 4.947019e-02,  -2.835759e-01, 1.976866e-02,  1.157731e+00,  -3.339831e-01 },
                        { -1.203593e-03, -3.865258e-02, -3.769005e-02, -6.278239e-03, 1.972370e-03, 5.989447e-03,
                          -4.387431e-04, 1.847914e-02,  -4.158216e-02, 7.029889e-03,  1.793268e-02, -6.470327e-02,
                          -7.328076e-03, -7.417670e-01, -7.165928e-01, -4.099980e-03, 1.837354e-02, 6.773148e-01,
                          2.270162e-02,  -6.090057e-01, 4.848853e-02,  -1.685178e-02, 7.412974e-02, -6.270846e-01,
                          2.686454e-02,  7.140052e-01,  3.271827e-02,  -2.039563e-02, 5.445326e-01, 7.231418e-01 },
                        { -2.032796e-02, -1.472326e-02, -2.005662e-02, 6.134213e-04, 7.140073e-03,  -6.516401e-02,
                          7.141359e-03,  -1.002036e-04, -1.945680e-02, 1.228425e-02, 7.141359e-03,  -6.594315e-04,
                          -6.391529e-01, -5.528410e-03, 7.069715e-02,  3.741628e-03, 5.439513e-03,  7.254515e-01,
                          -6.833212e-01, 1.525999e-02,  -6.902263e-01, 2.371199e-02, 2.258173e-02,  -7.024181e-01,
                          6.321574e-01,  -1.625565e-02, 6.944673e-01,  6.631520e-01, -2.095514e-02, 7.365295e-03 },
                        { -8.283546e-03, -2.798217e-02, -4.653281e-02, -4.550976e-04, 9.253863e-04, 1.771190e-02,
                          -6.012218e-04, 1.206658e-02,  -6.529872e-02, 7.141359e-03,  1.198363e-02, -4.125041e-02,
                          -4.758454e-03, -6.999054e-01, -7.304883e-01, -3.378125e-05, 1.149687e-02, 6.650409e-01,
                          2.161980e-02,  -6.320669e-01, 1.203521e-01,  3.299711e-03,  4.753452e-02, -6.724666e-01,
                          5.191105e-03,  6.839593e-01,  3.919468e-02,  -2.311988e-02, 5.919881e-01, 7.137373e-01 },
                        { 1.088542e-02,  3.770657e-03,  -4.061562e-03, 2.153605e-02,  -4.268938e-03, 1.601074e-03,
                          -3.650692e-04, 2.042905e-02,  2.003164e-02,  -6.594315e-04, -4.125041e-02, -1.506975e-02,
                          -3.242107e-02, 2.634442e-01,  -1.101331e+00, -2.812869e-01, -2.649862e-01, 1.908532e-02,
                          2.828965e-01,  -5.760371e-02, -1.073683e+00, 2.963061e-01,  3.530625e-01,  6.528306e-02,
                          -2.949943e-01, 5.625152e-03,  1.074241e+00,  -1.897296e-03, -2.782222e-01, 1.013904e+00 }
                    };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector3D()
            {
                return expected_results_type<IsMatrixOrVector::vector>
                    //                {
                    //                    -0.464074, -0.464074, -0.464074, -0.893661,
                    //                    0.678867, 0.678867, 0.678867, -0.893661,
                    //                    0.678867, 0.678867, 0.678867, -0.893661
                    //                };
                    { 1.200612e-02,  1.481888e-02,  -3.667872e-03, -1.003214e-02, -4.012649e-03, 2.078148e-03,
                      -2.688112e-03, -1.200459e-02, 1.476671e-03,  7.141359e-04,  1.198363e-03,  1.130528e-04 };
            }


            expected_results_type<IsMatrixOrVector::vector> Vector2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


        } // namespace


    } // namespace TestNS::Pk2


} // namespace MoReFEM
