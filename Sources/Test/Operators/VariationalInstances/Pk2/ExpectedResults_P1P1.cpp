/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Mar 2018 16:50:27 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Pk2/ExpectedResults.hpp"


namespace MoReFEM
{


    namespace TestNS::Pk2
    {


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix2D();
            expected_results_type<IsMatrixOrVector::matrix> Matrix1D();
            expected_results_type<IsMatrixOrVector::vector> Vector3D();
            expected_results_type<IsMatrixOrVector::vector> Vector2D();
            expected_results_type<IsMatrixOrVector::vector> Vector1D();


        } // namespace


        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatrixP1P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Matrix3D();
            case 2u:
                return Matrix2D();
            case 1u:
                return Matrix1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorP1P1(std::size_t dimension)
        {
            switch (dimension)
            {
            case 3u:
                return Vector3D();
            case 2u:
                return Vector2D();
            case 1u:
                return Vector1D();
            default:
                assert(false && "Invalid case!");
                exit(EXIT_FAILURE);
            }
        }


        namespace // anonymous
        {


            expected_results_type<IsMatrixOrVector::matrix> Matrix3D()
            {
                return expected_results_type<IsMatrixOrVector::matrix>

                    { { 1.15923,
                        1.68971,
                        3.28419,
                        -0.0288125,
                        -0.584961,
                        -1.58309,
                        1.25911,
                        3.71166,
                        5.13138,
                        -2.38953,
                        -4.8164,
                        -6.83248 },
                      { 1.68971,
                        5.66856,
                        9.05772,
                        1.11914,
                        1.66658,
                        2.53174,
                        2.03308,
                        6.7066,
                        7.44842,
                        -4.84192,
                        -14.04174,
                        -19.03788 },
                      { 3.28419,
                        9.05772,
                        15.23546,
                        2.0445,
                        2.71089,
                        3.83645,
                        5.64333,
                        16.80386,
                        22.39913,
                        -10.97202,
                        -28.57247,
                        -41.47103 },
                      { -0.0288125,
                        1.11914,
                        2.0445,
                        0.107905,
                        -0.0204103,
                        0.137774,
                        -0.132169,
                        2.1174,
                        3.11114,
                        0.0530768,
                        -3.21613,
                        -5.29341 },
                      { -0.584961,
                        1.66658,
                        2.71089,
                        -0.0204103,
                        0.984933,
                        1.16694,
                        -1.35447,
                        2.06823,
                        2.40858,
                        1.95984,
                        -4.71974,
                        -6.2864 },
                      { -1.58309,
                        2.53174,
                        3.83645,
                        0.137774,
                        1.16694,
                        1.92849,
                        -2.19711,
                        4.58799,
                        5.55552,
                        3.64243,
                        -8.28666,
                        -11.32046 },
                      { 1.25911,
                        2.03308,
                        5.64333,
                        -0.132169,
                        -1.35447,
                        -2.19711,
                        3.12083,
                        4.63413,
                        10.04007,
                        -4.24777,
                        -5.31275,
                        -13.48629 },
                      { 3.71166,
                        6.7066,
                        16.80386,
                        2.1174,
                        2.06823,
                        4.58799,
                        4.63413,
                        9.29933,
                        18.44163,
                        -10.46320,
                        -18.07415,
                        -39.83348 },
                      { 5.13138,
                        7.44842,
                        22.39913,
                        3.11114,
                        2.40858,
                        5.55552,
                        10.04007,
                        18.44163,
                        38.09556,
                        -18.28260,
                        -28.29863,
                        -66.05021 },
                      { -2.38953,
                        -4.84192,
                        -10.97202,
                        0.0530768,
                        1.95984,
                        3.64243,
                        -4.24777,
                        -10.46320,
                        -18.28260,
                        6.58422,
                        13.34528,
                        25.61218 },
                      { -4.8164,
                        -14.041741,
                        -28.57247,
                        -3.21613,
                        -4.71974,
                        -8.28666,
                        -5.31275,
                        -18.07415,
                        -28.29863,
                        13.34528,
                        36.83563,
                        65.15777 },
                      { -6.83248,
                        -19.03788,
                        -41.47103,
                        -5.29341,
                        -6.2864,
                        -11.32046,
                        -13.48629,
                        -39.83348,
                        -66.05021,
                        25.61218,
                        65.15777,
                        118.8417 } };
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::matrix> Matrix1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector3D()
            {
                return expected_results_type<IsMatrixOrVector::vector>{
                    0.677519, 2.38524, 7.96239,  1.47276,  1.10583,   1.62663,
                    2.28727,  6.69908, 10.50512, -4.43755, -10.19014, -20.09415,
                };
            }


            expected_results_type<IsMatrixOrVector::vector> Vector2D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


            expected_results_type<IsMatrixOrVector::vector> Vector1D()
            {
                assert(false && "Not yet implemented.");
                exit(EXIT_FAILURE);
            }


        } // namespace


    } // namespace TestNS::Pk2


} // namespace MoReFEM
