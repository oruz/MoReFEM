/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_MATRIX_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_MATRIX_HPP_

#include <memory>
#include <vector>


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class Model;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        /*!
         * \brief Analyze the results of the test model and check the interpolation matrix works as intended.
         *
         * \param[in] model The dummy \a Model used in current test.
         */
        void CheckMatrix(const Model& model);


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_MATRIX_HPP_
