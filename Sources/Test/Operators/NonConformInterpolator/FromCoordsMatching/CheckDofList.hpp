/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_DOF_LIST_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_DOF_LIST_HPP_

#include <memory>
#include <vector>


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class Model;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        /*!
         * \brief Check the \a Dof from both source and target are properly related.
         *
         * The test is very simple with identity between source and target \a Coords.
         *
         * \param[in] model The dummy \a Model used in current test.
         */
        void CheckDofList(const Model& model);


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_CHECK_DOF_LIST_HPP_
