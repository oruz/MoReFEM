/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 12:27:37 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Core/InputData/Instances/Result.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            using type = NonConformInterpolatorNS::FromCoordsMatching;
            decltype(auto) morefem_data = parent::GetMoReFEMData();

            unknown_solid_2_fluid_ = std::make_unique<type>(morefem_data,
                                                            EnumUnderlyingType(CoordsMatchingInterpolator::unknown),
                                                            NonConformInterpolatorNS::store_matrix_pattern::yes);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM
