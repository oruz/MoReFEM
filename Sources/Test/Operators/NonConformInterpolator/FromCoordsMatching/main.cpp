/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE from_coords_matching
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckDofList.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckMatrix.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    using fixture_type = TestNS::FixtureNS::Model<TestNS::FromCoordsMatchingNS::Model>;


} // namespace


BOOST_FIXTURE_TEST_CASE(check_interpolation_matrix, fixture_type)
{
    decltype(auto) model = GetModel();
    CheckMatrix(model);
}


BOOST_FIXTURE_TEST_CASE(check_dof_list, fixture_type)
{
    decltype(auto) model = GetModel();
    CheckDofList(model);
}


PRAGMA_DIAGNOSTIC(pop)
