/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("NonConformProjector");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


        inline const NonConformInterpolatorNS::FromCoordsMatching&
        Model::GetOperatorUnknownFluidToSolid() const noexcept
        {
            assert(!(!unknown_solid_2_fluid_));
            return *unknown_solid_2_fluid_;
        }


        inline const GlobalMatrix& Model::GetMatrixUnknownFluidToSolid() const noexcept
        {
            return GetOperatorUnknownFluidToSolid().GetInterpolationMatrix();
        }


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HXX_
