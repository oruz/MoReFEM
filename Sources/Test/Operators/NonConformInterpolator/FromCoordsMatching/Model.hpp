/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "Model/Model.hpp"

#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        //! \copydoc doxygen_hide_model_4_test
        class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = Model;

            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize();

            //! Interpolation matrix to interpolate unknown from solid mesh to fluid mesh.
            const GlobalMatrix& GetMatrixUnknownFluidToSolid() const noexcept;

            //! Vertex matching operator to interpolate unknown from solid mesh to fluid mesh.
            const NonConformInterpolatorNS::FromCoordsMatching& GetOperatorUnknownFluidToSolid() const noexcept;

          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}

          private:
            //! Vertex matching operator to interpolate unknown from solid mesh to fluid mesh.
            NonConformInterpolatorNS::FromCoordsMatching::const_unique_ptr unknown_solid_2_fluid_ = nullptr;
        };


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM


#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_MODEL_HPP_
