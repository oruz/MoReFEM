/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "PostProcessing/PostProcessing.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckDofList.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        namespace // anonymous
        {


            Dof::vector_shared_ptr ComputeSolidProcessorWiseDofList(const GodOfDof& solid_god_of_dof,
                                                                    const NumberingSubset& solid_numbering_subset)
            {
                Dof::vector_shared_ptr ret;
                decltype(auto) dof_list = solid_god_of_dof.GetProcessorWiseDofList();
                ret.reserve(dof_list.size());

                std::copy_if(dof_list.cbegin(),
                             dof_list.cend(),
                             std::back_inserter(ret),
                             [&solid_numbering_subset](const auto& dof_ptr)
                             {
                                 assert(!(!dof_ptr));
                                 return dof_ptr->IsInNumberingSubset(solid_numbering_subset);
                             });

                std::sort(ret.begin(),
                          ret.end(),
                          [&solid_numbering_subset](const auto& lhs, const auto& rhs)
                          {
                              return lhs->GetProcessorWiseOrGhostIndex(solid_numbering_subset)
                                     < rhs->GetProcessorWiseOrGhostIndex(solid_numbering_subset);
                          });

                return ret;
            }


            void ProcessDofList(const Dof::vector_shared_ptr& dof_list,
                                const NumberingSubset& numbering_subset,
                                std::unordered_map<PetscInt, Dof::shared_ptr>& out)
            {
                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));

                    if (!dof_ptr->IsInNumberingSubset(numbering_subset))
                        continue;

                    const auto [it, is_properly_inserted] =
                        out.insert({ static_cast<PetscInt>(dof_ptr->GetProgramWiseIndex(numbering_subset)), dof_ptr });
                    assert(is_properly_inserted);
                    static_cast<void>(is_properly_inserted);
                }
            }


            std::unordered_map<PetscInt, Dof::shared_ptr>
            ComputeFluidDofPerProgramWiseIndex(const GodOfDof& god_of_dof, const NumberingSubset& numbering_subset)
            {
                std::unordered_map<PetscInt, Dof::shared_ptr> ret;
                ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

                const auto dof_list = god_of_dof.GetProcessorWiseDofList();

                ProcessDofList(god_of_dof.GetProcessorWiseDofList(), numbering_subset, ret);

                ProcessDofList(god_of_dof.GetGhostDofList(), numbering_subset, ret);

                return ret;
            }

        } // namespace


        void CheckDofList(const Model& model)
        {
            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) solid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::solid));
            decltype(auto) fluid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::fluid));

            decltype(auto) solid_numbering_subset =
                solid_god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid));
            decltype(auto) fluid_numbering_subset =
                fluid_god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid));

            decltype(auto) interpolator = model.GetOperatorUnknownFluidToSolid();

            decltype(auto) interpolator_matching = interpolator.GetNonZeroPositionPerRow();

            decltype(auto) solid_dof_list = ComputeSolidProcessorWiseDofList(solid_god_of_dof, solid_numbering_subset);

            const auto fluid_dof_per_program_wise_index =
                ComputeFluidDofPerProgramWiseIndex(fluid_god_of_dof, fluid_numbering_subset);

            const auto Nrow = interpolator_matching.size();
            BOOST_CHECK_EQUAL(Nrow, solid_dof_list.size());

            for (auto row = 0ul; row < Nrow; ++row)
            {
                const auto solid_dof_ptr = solid_dof_list[row];
                assert(!(!solid_dof_ptr));

                BOOST_CHECK_EQUAL(solid_dof_ptr->GetProcessorWiseOrGhostIndex(solid_numbering_subset), row);

                const auto fluid_program_wise_index = interpolator_matching[row];

                const auto it_fluid_dof = fluid_dof_per_program_wise_index.find(fluid_program_wise_index);

                BOOST_CHECK(it_fluid_dof != fluid_dof_per_program_wise_index.cend());

                const auto& fluid_dof_ptr = it_fluid_dof->second;

                const auto& fluid_dof = *fluid_dof_ptr;
                const auto& solid_dof = *solid_dof_ptr;

                const auto fluid_node_ptr = fluid_dof.GetNodeFromWeakPtr();
                const auto solid_node_ptr = solid_dof.GetNodeFromWeakPtr();

                const auto& fluid_node = *fluid_node_ptr;
                const auto& solid_node = *solid_node_ptr;

                BOOST_CHECK(fluid_node.GetUnknown() == solid_node.GetUnknown());
                BOOST_CHECK(fluid_node.GetShapeFunctionLabel() == solid_node.GetShapeFunctionLabel());

                BOOST_CHECK_EQUAL(fluid_node.GetDofList().size(), solid_node.GetDofList().size());

                const auto solid_node_bearer_ptr = solid_node.GetNodeBearerFromWeakPtr();
                const auto fluid_node_bearer_ptr = fluid_node.GetNodeBearerFromWeakPtr();

                decltype(auto) solid_interface = solid_node_bearer_ptr->GetInterface();
                decltype(auto) fluid_interface = fluid_node_bearer_ptr->GetInterface();

                BOOST_CHECK_EQUAL(solid_interface.GetNature(), fluid_interface.GetNature());

                decltype(auto) solid_coords_list = fluid_interface.GetVertexCoordsList();
                decltype(auto) fluid_coords_list = fluid_node_bearer_ptr->GetInterface().GetVertexCoordsList();

                BOOST_CHECK_EQUAL(solid_coords_list.size(), fluid_coords_list.size());

                for (auto i = 0ul; i < solid_coords_list.size(); ++i)
                {
                    const auto& solid_coords_ptr = solid_coords_list[i];
                    const auto& fluid_coords_ptr = fluid_coords_list[i];

                    BOOST_CHECK_EQUAL(solid_coords_ptr->x(), fluid_coords_ptr->x());
                    BOOST_CHECK_EQUAL(solid_coords_ptr->y(), fluid_coords_ptr->y());
                    BOOST_CHECK_EQUAL(solid_coords_ptr->z(), fluid_coords_ptr->z());
                }
            }
        }


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM
