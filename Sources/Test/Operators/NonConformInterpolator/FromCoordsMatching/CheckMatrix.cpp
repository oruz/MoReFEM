/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "PostProcessing/PostProcessing.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckMatrix.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"


namespace MoReFEM
{


    namespace TestNS::FromCoordsMatchingNS
    {


        namespace // anonymous
        {


            /*!
             * \brief Gennerates a vector which content reflects the \a Coords index in the original mesh.
             *
             * The point of this vector is to check whether the interpolator works as intended.
             *
             * \return A vector which is filled with 0 for \a Dof not on  a vertex, and with the \a Coords::index_from_mesh_file of the associated \a Coords
             * on which the \a Dof is located.
             */
            GlobalVector::unique_ptr GenerateVectorWithIndexFromMeshFile(const GodOfDof& god_of_dof,
                                                                         const NumberingSubset& numbering_subset);


        } // namespace


        void CheckMatrix(const Model& model)
        {
            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) solid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::solid));
            decltype(auto) fluid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::fluid));

            decltype(auto) solid_numbering_subset =
                solid_god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid));
            decltype(auto) fluid_numbering_subset =
                fluid_god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid));

            decltype(auto) interpolator = model.GetOperatorUnknownFluidToSolid();

            decltype(auto) pattern = interpolator.GetMatrixPattern();

            decltype(auto) icsr = pattern.GetICsr();
            decltype(auto) jcsr = pattern.GetJCsr();

            const auto Nicsr = icsr.size();
            const auto Njcsr = jcsr.size();

            BOOST_CHECK_EQUAL(Njcsr + 1ul, Nicsr); // if not fulfilled CSR formatting is incorrect

            for (auto i = 0ul; i < Nicsr; ++i)
                BOOST_CHECK_EQUAL(static_cast<std::size_t>(icsr[i]), i); // only one value per line expected

            {
                auto copy_jcsr = jcsr;
                Utilities::EliminateDuplicate(copy_jcsr);
                BOOST_CHECK_EQUAL(copy_jcsr.size(), jcsr.size()); // a given program-wise index is present only once.
            }

            // Init a vector and put for each value the index from mesh file of its \a Coords.
            // It is a trick to easily be able to check everything is fine.
            const auto solid_vector_ptr = GenerateVectorWithIndexFromMeshFile(solid_god_of_dof, solid_numbering_subset);
            const auto fluid_vector_ptr = GenerateVectorWithIndexFromMeshFile(fluid_god_of_dof, fluid_numbering_subset);

            GlobalVector result(*solid_vector_ptr);
            result.ZeroEntries(__FILE__, __LINE__);

            decltype(auto) interpolation_matrix = model.GetMatrixUnknownFluidToSolid();

            Wrappers::Petsc::MatMult(interpolation_matrix, *fluid_vector_ptr, result, __FILE__, __LINE__);

            std::string description;
            BOOST_CHECK(AreEqual(*solid_vector_ptr, result, 1.e-10, description, __FILE__, __LINE__));
        }


        namespace // anonymous
        {


            GlobalVector::unique_ptr GenerateVectorWithIndexFromMeshFile(const GodOfDof& god_of_dof,
                                                                         const NumberingSubset& numbering_subset)
            {
                auto ret = std::make_unique<GlobalVector>(numbering_subset);
                AllocateGlobalVector(god_of_dof, *ret);

                {
                    decltype(auto) dof_list = god_of_dof.GetProcessorWiseDofList();

                    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(
                        *ret, __FILE__, __LINE__);

                    const auto Ndof = god_of_dof.NprocessorWiseDof(numbering_subset);

                    BOOST_CHECK_EQUAL(content.GetSize(__FILE__, __LINE__), Ndof);

                    for (const auto& dof_ptr : dof_list)
                    {
                        assert(!(!dof_ptr));
                        const auto& dof = *dof_ptr;

                        if (!dof.IsInNumberingSubset(numbering_subset))
                            continue;

                        const auto dof_processor_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);

                        BOOST_CHECK(dof_processor_wise_index < Ndof);

                        const auto node_ptr = dof.GetNodeFromWeakPtr();
                        assert(!(!node_ptr));
                        const auto node_bearer_ptr = node_ptr->GetNodeBearerFromWeakPtr();
                        decltype(auto) interface = node_bearer_ptr->GetInterface();

                        if (interface.GetNature() == InterfaceNS::Nature::vertex)
                        {
                            decltype(auto) vertex_coords_list = interface.GetVertexCoordsList();
                            assert(vertex_coords_list.size() == 1ul);

                            const auto& coords_ptr = vertex_coords_list.back();
                            assert(!(!coords_ptr));

                            const auto program_wise_pos = coords_ptr->GetProgramWisePosition();

                            content[dof_processor_wise_index] = static_cast<PetscScalar>(program_wise_pos.Get());
                        }
                    }
                }

                return ret;
            }


        } // namespace


    } // namespace TestNS::FromCoordsMatchingNS


} // namespace MoReFEM
