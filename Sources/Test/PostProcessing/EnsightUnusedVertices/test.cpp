//! \file
//
//
//  test.cpp
//  MoReFEM
//
//  Created by sebastien on 12/04/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <sstream>

#define BOOST_TEST_MODULE ensight_unused_vertices
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/OutputFormat/OutputFormat.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"

#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"
#include "Test/Tools/Fixture/Mpi.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    std::size_t GenerateNewMeshId();

    void TestCase(const Wrappers::Mpi& mpi, double space_unit);


    struct fixture : public MoReFEM::TestNS::FixtureNS::Environment, public MoReFEM::TestNS::FixtureNS::Mpi
    { };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(ensight_unused_vertices, fixture)


BOOST_AUTO_TEST_CASE(space_unit_1)
{
    TestCase(GetMpi(), 1.);
}

BOOST_AUTO_TEST_CASE(space_unit_2)
{
    TestCase(GetMpi(), 2.);
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    std::size_t GenerateNewMeshId()
    {
        static auto ret = 0ul;
        return ++ret;
    }


    void TestCase(const Wrappers::Mpi& mpi, double space_unit)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        Utilities::OutputFormat::CreateOrGetInstance(__FILE__, __LINE__, false);

        std::string data_directory_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/EnsightUnusedVertices/Data");

        FilesystemNS::Directory data_directory(
            mpi, data_directory_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        auto mesh_unique_id = GenerateNewMeshId();
        std::string mesh_file = data_directory.AddFile("cylinder_unused_vertices.mesh");
        constexpr auto dimension = 3u;

        mesh_manager.Create(mesh_unique_id, mesh_file, dimension, MeshNS::Format::Medit, space_unit);

        decltype(auto) mesh = mesh_manager.GetMesh(mesh_unique_id);

        std::vector<std::size_t> numbering_subset_id_list{ 1 };
        std::vector<std::string> unknown_list{ "solid_displacement" };
        std::string ensight_directory_path =
            environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/EnsightUnusedVertices/Ensight6");

        FilesystemNS::Directory::const_unique_ptr ensight_directory =
            std::make_unique<FilesystemNS::Directory>(mpi,
                                                      ensight_directory_path,
                                                      FilesystemNS::behaviour::overwrite,
                                                      __FILE__,
                                                      __LINE__,
                                                      FilesystemNS::add_rank::no);

        PostProcessingNS::OutputFormat::Ensight6 ensight_output(data_directory,
                                                                unknown_list,
                                                                numbering_subset_id_list,
                                                                mesh,
                                                                PostProcessingNS::RefinedMesh::no,
                                                                ensight_directory.get());

        std::string ref_dir_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/EnsightUnusedVertices/"
                                         "ExpectedResult/Ensight6");

        FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        std::ostringstream oconv;
        for (auto time_iteration = 0ul; time_iteration < 2ul; ++time_iteration)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, *ensight_directory, oconv.str(), __FILE__, __LINE__, 1.e-11);
        }
    }


} // namespace
