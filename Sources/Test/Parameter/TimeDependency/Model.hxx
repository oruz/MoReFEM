/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 30 Mar 2016 15:02:08 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Parameter/TimeDependency/Model.hpp"


namespace MoReFEM
{


    namespace TestParameterTimeDependencyNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("ParameterTimeDependency");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline const Model::scalar_immutable_type& Model::GetPoissonRatio() const noexcept
        {
            assert(!(!poisson_ratio_));
            return *poisson_ratio_;
        }


        inline Model::scalar_immutable_type& Model::GetNonCstPoissonRatio() noexcept
        {
            return const_cast<scalar_immutable_type&>(GetPoissonRatio());
        }


        inline const Model::scalar_varying_type_functor& Model::GetYoungModulus() const noexcept
        {
            assert(!(!young_modulus_));
            return *young_modulus_;
        }


        inline Model::scalar_varying_type_functor& Model::GetNonCstYoungModulus() noexcept
        {
            return const_cast<scalar_varying_type_functor&>(GetYoungModulus());
        }


        inline const Model::scalar_immutable_type& Model::GetStaticPressure() const noexcept
        {
            assert(!(!static_pressure_));
            return *static_pressure_;
        }


        inline Model::scalar_immutable_type& Model::GetNonCstStaticPressure() noexcept
        {
            return const_cast<scalar_immutable_type&>(GetStaticPressure());
        }

        inline const ScalarParameterFromFile& Model::GetDynamicPressure() const noexcept
        {
            assert(!(!dynamic_pressure_));
            return *dynamic_pressure_;
        }


        inline ScalarParameterFromFile& Model::GetNonCstDynamicPressure() noexcept
        {
            return const_cast<ScalarParameterFromFile&>(GetDynamicPressure());
        }


        // clang-format off
        inline const GlobalVariationalOperatorNS::TransientSource
        <
            ParameterNS::Type::vector,
            ParameterNS::TimeDependencyFunctor
        >&
        // clang-format on
        Model::GetSourceOperator() const noexcept
        {
            assert(!(!source_operator_));
            return *source_operator_;
        }


        // clang-format off
        inline GlobalVariationalOperatorNS::TransientSource
        <
            ParameterNS::Type::vector,
            ParameterNS::TimeDependencyFunctor
        >&
        // clang-format on
        Model ::GetNonCstSourceOperator() noexcept
        {
            // clang-format off
            using type =
                GlobalVariationalOperatorNS::TransientSource
                <
                    ParameterNS::Type::vector,
                    ParameterNS::TimeDependencyFunctor
                >;
            // clang-format on

            return const_cast<type&>(GetSourceOperator());
        }


        inline const Parameter<ParameterNS::Type::vector, LocalCoords, ParameterNS::TimeDependencyFunctor>&
        Model::GetSourceParameter() const noexcept
        {
            assert(!(!source_parameter_));
            return *source_parameter_;
        }


        inline Parameter<ParameterNS::Type::vector, LocalCoords, ParameterNS::TimeDependencyFunctor>&
        Model::GetNonCstSourceParameter() noexcept
        {
            // clang-format off
            using type =
                Parameter
                <
                    ParameterNS::Type::vector,
                    LocalCoords,
                    ParameterNS::TimeDependencyFunctor
                >;
            // clang-format on

            return const_cast<type&>(GetSourceParameter());
        }


    } // namespace TestParameterTimeDependencyNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HXX_
