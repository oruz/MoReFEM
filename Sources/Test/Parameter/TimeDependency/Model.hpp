/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HPP_

#include <memory>
#include <vector>

#include "ParameterInstances/ScalarParameterFromFile/ScalarParameterFromFile.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

#include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "Model/Model.hpp"

#include "Test/Parameter/TimeDependency/InputData.hpp"


namespace MoReFEM
{


    namespace TestParameterTimeDependencyNS
    {


        //! \copydoc doxygen_hide_model_4_test
        class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = Model;

            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

            //! Convenient alias.
            using scalar_varying_type_functor =
                Parameter<ParameterNS::Type::scalar, LocalCoords, ParameterNS::TimeDependencyFunctor>;

            //! Convenient alias.
            using scalar_varying_type_from_file =
                Parameter<ParameterNS::Type::scalar, LocalCoords, ParameterNS::TimeDependencyFromFile>;

            //! Convenient alias.
            using scalar_immutable_type =
                Parameter<ParameterNS::Type::scalar, LocalCoords, ParameterNS::TimeDependencyNS::None>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize();


          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}


            //! Constant accessor to the scalar parameter that varies with time.
            const scalar_varying_type_functor& GetYoungModulus() const noexcept;

            //! Non constant accessor to the scalar parameter that varies with time.
            scalar_varying_type_functor& GetNonCstYoungModulus() noexcept;

            //! Constant accessor to the scalar parameter that does not vary with time.
            const scalar_immutable_type& GetPoissonRatio() const noexcept;

            //! Non constant accessor to scalar parameter that does not vary with time.
            scalar_immutable_type& GetNonCstPoissonRatio() noexcept;

            //! Constant accessor to the scalar parameter that varies with time.
            const scalar_immutable_type& GetStaticPressure() const noexcept;

            //! Non constant accessor to the scalar parameter that varies with time.
            scalar_immutable_type& GetNonCstStaticPressure() noexcept;

            //! Constant accessor to the scalar parameter that varies with time.
            const ScalarParameterFromFile& GetDynamicPressure() const noexcept;

            //! Non constant accessor to the scalar parameter that varies with time.
            ScalarParameterFromFile& GetNonCstDynamicPressure() noexcept;

            //! Constant accessor to the source operator.
            // clang-format off
            const GlobalVariationalOperatorNS::TransientSource
            <
                ParameterNS::Type::vector,
                ParameterNS::TimeDependencyFunctor
            >&
            GetSourceOperator() const noexcept;
            // clang-format on

            //! Non constant accessor to the source operator.
            // clang-format off
            GlobalVariationalOperatorNS::TransientSource
            <
                ParameterNS::Type::vector,
                ParameterNS::TimeDependencyFunctor
            >&
            GetNonCstSourceOperator() noexcept;
            // clang-format on

            //! Constant accessor to the underlying Parameter used to define source operator.
            // clang-format off
            const Parameter
            <
                ParameterNS::Type::vector,
                LocalCoords,
                ParameterNS::TimeDependencyFunctor
            >&
            // clang-format on
            GetSourceParameter() const noexcept;

            //! Non constant accessor to the underlying Parameter used to define source operator.
            Parameter<ParameterNS::Type::vector, LocalCoords, ParameterNS::TimeDependencyFunctor>&
            GetNonCstSourceParameter() noexcept;


          private:
            //! Output file for current rank.
            std::string output_file_;

            //! Young modulus (i.e. scalar parameter) that in the test may vary with time.
            typename scalar_varying_type_functor::unique_ptr young_modulus_ = nullptr;

            //! Poisson ratio (i.e. scalar parameter) that in the test may not vary with time.
            typename scalar_immutable_type::unique_ptr poisson_ratio_ = nullptr;

            //! Applied pressure that is static.
            typename scalar_immutable_type::unique_ptr static_pressure_ = nullptr;

            //! Applied pressure that in the test may vary with time.
            ScalarParameterFromFile::unique_ptr dynamic_pressure_ = nullptr;

            //! Source operator.
            // clang-format off
            typename GlobalVariationalOperatorNS::TransientSource
            <
                ParameterNS::Type::vector,
                ParameterNS::TimeDependencyFunctor
            >::const_unique_ptr
                // clang-format on
                source_operator_ = nullptr;


            //! Underlying Parameter used to define source operator.
            // clang-format off
            typename Parameter
            <
                ParameterNS::Type::vector,
                LocalCoords,
                ParameterNS::TimeDependencyFunctor
            >::unique_ptr source_parameter_ = nullptr;
            // clang-format on
        };


    } // namespace TestParameterTimeDependencyNS


} // namespace MoReFEM


#include "Test/Parameter/TimeDependency/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_PARAMETER_x_TIME_DEPENDENCY_x_MODEL_HPP_
