/*!
 // \file
 //
 //
 // Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE fiber_at_quad_pts
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/AtQuadPt/InputData.hpp"
#include "Test/Parameter/AtQuadPt/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::Model<TestNS::FibersAtQuadPt::Model>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(CheckFiberAtQuadPt, fixture_type)
{
    GetModel();
}
