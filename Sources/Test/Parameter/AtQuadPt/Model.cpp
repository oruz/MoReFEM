/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Parameter/AtQuadPt/Model.hpp"


namespace MoReFEM::TestNS::FibersAtQuadPt
{


    namespace // anonymous
    {


        //! Converts a ParameterAtQuadPt into a std::vector.
        template<ParameterNS::Type TypeT>
        std::vector<double>
        ConvertParameterAtQuadPoint(const FilesystemNS::Directory& result_directory_path,
                                    std::string_view filename,
                                    const ParameterAtQuadraturePoint<TypeT>& obtained_global_coords);


        //! Checks that the fibers defined at nodes have the same interpolated value as the ones defined at quad_pts.
        void CheckResults(const std::vector<double>& expected, const std::vector<double>& obtained);


    } // namespace


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    { }


    void Model::SupplInitialize()
    {
        auto quadrature_rule = std::make_unique<const QuadratureRulePerTopology>(5, 5);
        CheckScalar(quadrature_rule.get());
        CheckVectorial(quadrature_rule.get());
    }


    void Model::CheckScalar(const QuadratureRulePerTopology* const quadrature_rule) const
    {
        auto& fiber_scalar_at_node =
            FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::GetInstance(
                __FILE__, __LINE__)
                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber_scalar_at_node));

        fiber_scalar_at_node.Initialize(quadrature_rule);
        const auto& scalar_node_to_quad =
            ConvertParameterAtQuadPoint(GetOutputDirectory(), "foo.txt", fiber_scalar_at_node.GetUnderlyingParameter());

        auto& fiber_scalar_at_quad_pt =
            FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::scalar>::GetInstance(
                __FILE__, __LINE__)
                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber_scalar_at_quad_pt));

        fiber_scalar_at_quad_pt.Initialize(quadrature_rule);
        const auto& scalar_quad_to_quad = ConvertParameterAtQuadPoint(
            GetOutputDirectory(), "foo.txt", fiber_scalar_at_quad_pt.GetUnderlyingParameter());

        CheckResults(scalar_node_to_quad, scalar_quad_to_quad);
    }


    void Model::CheckVectorial(const QuadratureRulePerTopology* const quadrature_rule) const
    {
        auto& fiber_vector_at_node =
            FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::GetInstance(
                __FILE__, __LINE__)
                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber_vector_at_node));

        fiber_vector_at_node.Initialize(quadrature_rule);

        const auto& vector_node_to_quad =
            ConvertParameterAtQuadPoint(GetOutputDirectory(), "foo.txt", fiber_vector_at_node.GetUnderlyingParameter());

        auto& fiber_vector_at_quad_pt =
            FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::vector>::GetInstance(
                __FILE__, __LINE__)
                .GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber_vector_at_quad_pt));

        fiber_vector_at_quad_pt.Initialize(quadrature_rule);
        const auto& vector_quad_to_quad = ConvertParameterAtQuadPoint(
            GetOutputDirectory(), "foo.txt", fiber_vector_at_quad_pt.GetUnderlyingParameter());

        CheckResults(vector_node_to_quad, vector_quad_to_quad);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


    namespace // anonymous
    {


        /*!
         * \brief Convert the obtained global coords into a vector of double which is filled with same ordering as the
         * expected ones.
         *
         */
        template<ParameterNS::Type TypeT>
        std::vector<double> ConvertParameterAtQuadPoint(const FilesystemNS::Directory& result_directory_path,
                                                        std::string_view quadrature_order,
                                                        const ParameterAtQuadraturePoint<TypeT>& obtained_global_coords)
        {
            const std::string output_file = result_directory_path.AddFile(quadrature_order);
            obtained_global_coords.Write(output_file);

            std::ifstream file_stream;
            FilesystemNS::File::Read(file_stream, output_file, __FILE__, __LINE__);
            std::string line;

            // skip first two lines
            for (int i = 0; i < 2; ++i)
                getline(file_stream, line);

            std::vector<double> ret;

            while (getline(file_stream, line))
            {
                const auto item_list = Utilities::String::Split(line, ";");

                BOOST_CHECK_EQUAL(item_list.size(), 4ul);

                const auto coords_list = std::string(item_list.back()); // istringstream can't act upon a
                                                                        // std::string_view hence this copy.
                std::istringstream iconv(coords_list);
                double value;
                while (iconv >> value)
                    ret.push_back(value);
            }

            return ret;
        }


        void CheckResults(const std::vector<double>& expected, const std::vector<double>& obtained)
        {
            BOOST_CHECK_EQUAL(expected.size(), obtained.size());

            constexpr auto percentage_precision = 1.e-3;

            for (auto it_obtained = obtained.cbegin(), it_expected = expected.cbegin(); it_obtained != obtained.cend();
                 ++it_obtained, ++it_expected)
            {
                const auto obtained_value = *it_obtained;
                const auto expected_value = *it_expected;

                BOOST_CHECK_CLOSE(expected_value, obtained_value, percentage_precision);
            }
        }


    } // namespace


} // namespace MoReFEM::TestNS::FibersAtQuadPt
