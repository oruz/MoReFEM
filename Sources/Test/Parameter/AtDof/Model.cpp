/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 11:49:17 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Core/InputData/Instances/Result.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/Policy/AtDof/AtDof.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Test/Parameter/AtDof/Model.hpp"

#include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hpp"


namespace MoReFEM
{


    namespace TestAtDofNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data, create_domain_list_for_coords::yes)
        { }


        void Model::SupplInitialize()
        {

            void SupplInitialize();

            decltype(auto) scalar_fiber_manager =
                FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::GetInstance(
                    __FILE__, __LINE__);
            scalar_fiber_manager.GetNonCstFiberList(EnumUnderlyingType(FiberIndex::distance)).Initialize();

            decltype(auto) vectorial_fiber_manager =
                FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::GetInstance(
                    __FILE__, __LINE__);
            vectorial_fiber_manager.GetNonCstFiberList(EnumUnderlyingType(FiberIndex::fiber)).Initialize();
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        { }


    } // namespace TestAtDofNS


} // namespace MoReFEM
