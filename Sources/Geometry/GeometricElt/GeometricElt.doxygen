/*!
 * \class doxygen_hide_geom_elt_unique_id_arg
 *
 * \param[in] mesh_unique_id Unique identifier of the mesh (which is what is returned by Mesh::GetUniqueId() method).
 */ 


/*!
 * \class doxygen_hide_geom_elt_minimal_constructor
 *
 * \brief Minimal constructor: only the \a Mesh for which the current geometric element is created is provided.
 *
 * \copydoc doxygen_hide_geom_elt_unique_id_arg
 */


/*!
 * \class doxygen_hide_mesh_coords_list_arg
 *
 *  \param[in] mesh_coords_list List of ALL \a Coords list built from the mesh. 
 */

/*!
 * \class doxygen_hide_geom_elt_stream_constructor
 *
 * \brief Constructor from stream: The indexes related to \a Ncoords (as defined in enum) are read in the stream 
 * and object is built from them.
 *
 * This constructor expected integers from \a stream: it is to be used in formats in which \a Coords are read first 
 * and then their indexes are used to define all the \a GeometricElt.
 *
 *
 * \copydoc doxygen_hide_geom_elt_unique_id_arg
 * \copydoc doxygen_hide_mesh_coords_list_arg

 * \param[in,out] stream Stream from which the coordinates of the \a GeometricElt are read.
 */


/*!
 * \class doxygen_hide_geom_elt_vector_coords_constructor
 *
 * \brief Constructor from vector of coords index.
 *
 * \copydoc doxygen_hide_geom_elt_unique_id_arg
 * \copydoc doxygen_hide_mesh_coords_list_arg
 * \param[in] coords_index_list The list of \a Coords index that delimit the geometric element; they should all exist
 * in the \a mesh_coords_list.
 */