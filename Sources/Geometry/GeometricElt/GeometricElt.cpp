/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    GeometricElt::GeometricElt(const std::size_t mesh_unique_id) : mesh_identifier_(mesh_unique_id)
    { }


    GeometricElt::~GeometricElt() = default;


    GeometricElt::GeometricElt(const std::size_t mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::size_t index,
                               std::vector<CoordsNS::index_from_mesh_file>&& coords)
    : GeometricElt(mesh_unique_id)
    {
        index_ = index;
        SetCoordsList(mesh_coords_list, std::move(coords));
    }


    GeometricElt::GeometricElt(const std::size_t mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::vector<CoordsNS::index_from_mesh_file>&& coords)
    : GeometricElt(mesh_unique_id,
                   mesh_coords_list,
                   NumericNS::UninitializedIndex<decltype(index_)>(),
                   std::move(coords))
    { }


    void GeometricElt::SetCoordsList(const Coords::vector_shared_ptr& mesh_coords_list,
                                     std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_list)
    {
        assert(!mesh_coords_list.empty());
        using difference_type = Coords::vector_shared_ptr::difference_type;

        const auto begin = mesh_coords_list.cbegin();
        const auto end = mesh_coords_list.cend();

        for (auto coord_index : coords_list)
        {
            auto match_function = [coord_index](const auto& coord_ptr)
            {
                assert(!(!coord_ptr));
                return coord_ptr->GetIndexFromMeshFile() == coord_index;
            };

            // If we're lucky, the index read from the mesh file is rather sensical and index 'i' is rather close
            // to the 'i'-th position, which shorten considerably the search. Of course, it is absolutely not a given,
            // and we might be prepared for all of them.
            // (for instance currently if we run Ensight format from prepartitioned data we do not have this luck...)
            auto it_begin_guess = begin + static_cast<difference_type>(coord_index.Get()) - 1;
            if (it_begin_guess < begin || it_begin_guess >= end)
                it_begin_guess = begin;

            auto it_end_guess = begin + static_cast<difference_type>(coord_index.Get()) + 2;
            if (it_end_guess > end)
                it_end_guess = end;

            assert(it_begin_guess <= it_end_guess);

            auto it = std::find_if(it_begin_guess, it_end_guess, match_function);

            // If not in the narrow guess range, look for it in the entire container.
            if (it == it_end_guess)
                it = std::find_if(begin, end, match_function);

            assert(it != end);
            assert(*it);
            coords_list_.push_back(*it);
        }
    }


    const Coords& GeometricElt::GetCoord(std::size_t i) const
    {
        assert(i < coords_list_.size());
        assert(!(!coords_list_[i]));
        return *coords_list_[i];
    }


    template<>
    void GeometricElt::BuildInterface<Vertex>(const GeometricElt* geom_elt_ptr, Vertex::InterfaceMap& interface_list)
    {
        BuildVertexList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Edge>(const GeometricElt* geom_elt_ptr, Edge::InterfaceMap& interface_list)
    {
        BuildEdgeList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Face>(const GeometricElt* geom_elt_ptr, Face::InterfaceMap& interface_list)
    {
        BuildFaceList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Volume>(const GeometricElt* geom_elt_ptr, Volume::InterfaceMap& interface_list)
    {
        BuildVolumeList(geom_elt_ptr, interface_list);
    }


    const Advanced::GeomEltNS::EnsightName& GeometricElt::GetEnsightName() const
    {
        return GetRefGeomElt().GetEnsightName();
    }


    GmfKwdCod GeometricElt::GetMeditIdentifier() const
    {
        return GetRefGeomElt().GetMeditIdentifier();
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
