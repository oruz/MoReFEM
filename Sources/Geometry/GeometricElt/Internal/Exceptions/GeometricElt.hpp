/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_

#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace GeometricElt
        {


            //! Generic class for GeometricElt exceptions.
            class Exception : public MoReFEM::Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! \copydoc doxygen_hide_copy_constructor
                Exception(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Exception(Exception&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Exception& operator=(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Exception& operator=(Exception&& rhs) = default;
            };


            /*!
             * \brief Thrown when a geometric element is not supported by the chosen format.
             *
             * For instance some geometric elements are recognized by Ensight and not by Medit (and vice-versa).
             * So for instance if you read an Ensight mesh with a Quadrangle8 and then attempts to write the mesh
             * as a Medit one, this exception will be thrown as Medit can't cope with it.
             */
            class FormatNotSupported final : public Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered
                 (eg 'Triangle3')
                 * \param[in] format Name of the format considered  ('Ensight' or 'Medit' currently)
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit FormatNotSupported(const std::string& geometric_elt_identifier,
                                            const std::string& format,
                                            const char* invoking_file,
                                            int invoking_line);

                //! Destructor
                virtual ~FormatNotSupported() override;

                //! \copydoc doxygen_hide_copy_constructor
                FormatNotSupported(const FormatNotSupported& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                FormatNotSupported(FormatNotSupported&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                FormatNotSupported& operator=(const FormatNotSupported& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                FormatNotSupported& operator=(FormatNotSupported&& rhs) = default;
            };


            /*!
             * \brief Thrown when edges or faces are requested whereas they weren't properly built.
             *
             * They are not built automatically, so user must have called Mesh::BuildEdgeList() or
             * Mesh::BuildFaceList() beforehand.
             */
            class InterfaceTypeNotBuilt final : public Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] edge_or_face "edge" or "face"
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InterfaceTypeNotBuilt(const std::string& edge_or_face,
                                               const char* invoking_file,
                                               int invoking_line);

                //! Destructor
                virtual ~InterfaceTypeNotBuilt() override;

                //! \copydoc doxygen_hide_copy_constructor
                InterfaceTypeNotBuilt(const InterfaceTypeNotBuilt& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                InterfaceTypeNotBuilt(InterfaceTypeNotBuilt&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                InterfaceTypeNotBuilt& operator=(const InterfaceTypeNotBuilt& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                InterfaceTypeNotBuilt& operator=(InterfaceTypeNotBuilt&& rhs) = default;
            };


            /*!
             * \brief Thrown when you attempt to build an interface type while not having built first the type just
             * below.
             *
             * For instance in order to build faces you must ahve already built edges.
             */
            class InvalidInterfaceBuildOrder final : public Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] attempted_type Type which build was required by the developer.
                 * \param[in] missing_type Type that ought to be already built to build \a attempted_type.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit InvalidInterfaceBuildOrder(std::string&& attempted_type,
                                                    std::string&& missing_type,
                                                    const char* invoking_file,
                                                    int invoking_line);

                //! Destructor
                virtual ~InvalidInterfaceBuildOrder() override;

                //! \copydoc doxygen_hide_copy_constructor
                InvalidInterfaceBuildOrder(const InvalidInterfaceBuildOrder& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                InvalidInterfaceBuildOrder(InvalidInterfaceBuildOrder&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                InvalidInterfaceBuildOrder& operator=(const InvalidInterfaceBuildOrder& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                InvalidInterfaceBuildOrder& operator=(InvalidInterfaceBuildOrder&& rhs) = default;
            };


            /*!
             * \brief Thrown when no convergence was reached in Global2Local.
             *
             */
            class Global2LocalNoConvergence : public Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Global2LocalNoConvergence(const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Global2LocalNoConvergence() override;

                //! \copydoc doxygen_hide_copy_constructor
                Global2LocalNoConvergence(const Global2LocalNoConvergence& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Global2LocalNoConvergence(Global2LocalNoConvergence&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Global2LocalNoConvergence& operator=(const Global2LocalNoConvergence& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Global2LocalNoConvergence& operator=(Global2LocalNoConvergence&& rhs) = default;
            };


        } // namespace GeometricElt


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_
