/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_

#include <iosfwd>

#include "Utilities/Exceptions/Factory.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Factory
        {


            // Use some of the Factory Exception classes already defined in Utilities.
            using ExceptionNS::Factory::Exception;
            using ExceptionNS::Factory::UnableToRegister;


            namespace GeometricElt
            {


                //! Generic exception for GeometricEltFactory.
                class Exception : public Factory::Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message.
                     *
                     * \param[in] msg Message
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                    //! Destructor.
                    virtual ~Exception() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    Exception(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    Exception(Exception&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    Exception& operator=(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    Exception& operator=(Exception&& rhs) = default;
                };


                //! Thrown when the user requires from the factory an object with an unknown Ensight name (which acts as
                //! an identifier).
                class InvalidEnsightGeometricEltName final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] ensight_name Attempted Ensight name of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidEnsightGeometricEltName(const Advanced::GeomEltNS::EnsightName& ensight_name,
                                                            const char* invoking_file,
                                                            int invoking_line);

                    //! Destructor.
                    virtual ~InvalidEnsightGeometricEltName() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidEnsightGeometricEltName(const InvalidEnsightGeometricEltName& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidEnsightGeometricEltName(InvalidEnsightGeometricEltName&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidEnsightGeometricEltName& operator=(const InvalidEnsightGeometricEltName& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidEnsightGeometricEltName& operator=(InvalidEnsightGeometricEltName&& rhs) = default;
                };


                /*!
                 * \brief Thrown when the user requires from the factory an object with an unknown identifier.
                 *
                 * Said identifier is one given by the class itself and is not related to a specific IO format.
                 */
                class InvalidGeometricEltId final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] identifier Attempted identifier of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidGeometricEltId(Advanced::GeometricEltEnum identifier,
                                                   const char* invoking_file,
                                                   int invoking_line);

                    //! Destructor.
                    virtual ~InvalidGeometricEltId() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidGeometricEltId(const InvalidGeometricEltId& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidGeometricEltId(InvalidGeometricEltId&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidGeometricEltId& operator=(const InvalidGeometricEltId& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidGeometricEltId& operator=(InvalidGeometricEltId&& rhs) = default;
                };


                /*!
                 * \brief Thrown when the user requires from the factory an object with an unknown name.
                 *
                 * Said identifier is one given by the class itself and is not related to a specific IO format.
                 */
                class InvalidGeometricEltName final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] ref_geom_elt_name Attempted name of a \a RefGeomElt from which a \a GeometricElt
                     * should have been built.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidGeometricEltName(const Advanced::GeomEltNS::GenericName& ref_geom_elt_name,
                                                     const char* invoking_file,
                                                     int invoking_line);

                    //! Destructor.
                    virtual ~InvalidGeometricEltName() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidGeometricEltName(const InvalidGeometricEltName& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidGeometricEltName(InvalidGeometricEltName&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidGeometricEltName& operator=(const InvalidGeometricEltName& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidGeometricEltName& operator=(InvalidGeometricEltName&& rhs) = default;
                };


            } // namespace GeometricElt


        } // namespace Factory


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_FACTORY_HPP_
