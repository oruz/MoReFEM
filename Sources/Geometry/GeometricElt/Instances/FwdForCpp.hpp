//! \file
//
//
//  File.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 25/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_CPP_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_CPP_HPP_

#include <array>         // IWYU pragma: export
#include <functional>    // IWYU pragma: export
#include <iosfwd>        // IWYU pragma: export
#include <map>           // IWYU pragma: export
#include <memory>        // IWYU pragma: export
#include <type_traits>   // IWYU pragma: export
#include <unordered_map> // IWYU pragma: export
#include <vector>        // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"                             // IWYU pragma: export
#include "Geometry/Coords/StrongType.hpp"                         // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"       // IWYU pragma: export
#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"          // IWYU pragma: export
#include "Geometry/Interfaces/Interface.hpp"                      // IWYU pragma: export

#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_CPP_HPP_
