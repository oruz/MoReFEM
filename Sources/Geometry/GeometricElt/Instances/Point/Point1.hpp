/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================
#include "Geometry/RefGeometricElt/Instances/Point/Point1.hpp"
//# include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Point1 geometric element read in a mesh.
     *
     * We are not considering here a generic Point1 object (that's the role of MoReFEM::RefGeomEltNS::Point1), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Point1 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Point1>
    {
      public:
        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Point1(std::size_t mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Point1(std::size_t mesh_unique_id,
                        const Coords::vector_shared_ptr& mesh_coords_list,
                        std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Point1(std::size_t mesh_unique_id,
                        const Coords::vector_shared_ptr& mesh_coords_list,
                        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list);

        //! Destructor.
        ~Point1() override;

        //! \copydoc doxygen_hide_copy_constructor
        Point1(const Point1& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Point1(Point1&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Point1& operator=(const Point1& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Point1& operator=(Point1&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

      private:
        //! Reference geometric element.
        static const RefGeomEltNS::Point1& StaticRefGeomElt();
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
