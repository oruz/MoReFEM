//! \file
//
//
//  File.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 25/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_HPP_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_HPP_HPP_

#include <array>  // IWYU pragma: export
#include <iosfwd> // IWYU pragma: export
#include <map>    // IWYU pragma: export
#include <vector> // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"                                              // IWYU pragma: export
#include "Geometry/Coords/StrongType.hpp"                                          // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"                     // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"                     // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"                        // IWYU pragma: export
#include "Geometry/Interfaces/EnumInterface.hpp"                                   // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Edge.hpp"                                  // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Face.hpp"                                  // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Interface.hpp"                                       // IWYU pragma: export
#include "Geometry/Interfaces/Internal/TInterface.hpp"                             // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"         // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"                // IWYU pragma: export

#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_FWD_FOR_HPP_HPP_
