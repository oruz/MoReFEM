/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT3_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT3_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================
#include "Geometry/RefGeometricElt/Instances/Segment/Segment3.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Segment3 geometric element read in a mesh.
     *
     * We are not considering here a generic Segment3 object (that's the role of MoReFEM::RefGeomEltNS::Segment3), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Segment3 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Segment3>
    {
      public:
        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Segment3(std::size_t mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Segment3(std::size_t mesh_unique_id,
                          const Coords::vector_shared_ptr& mesh_coords_list,
                          std::istream& stream);

        //! Destructor.
        ~Segment3() override;

        //! \copydoc doxygen_hide_copy_constructor
        Segment3(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Segment3(Segment3&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Segment3& operator=(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Segment3& operator=(Segment3&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

      private:
        //! Reference geometric element.
        static const RefGeomEltNS::Segment3& StaticRefGeomElt();
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT3_HPP_
