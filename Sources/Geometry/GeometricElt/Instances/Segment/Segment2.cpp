/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForCpp.hpp"
#include "Geometry/GeometricElt/Instances/Segment/Segment2.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment2.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Segment/Segment2.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {


        auto
        CreateEnsight(std::size_t mesh_unique_id, const Coords::vector_shared_ptr& mesh_coords_list, std::istream& in)
        {
            return std::make_unique<Segment2>(mesh_unique_id, mesh_coords_list, in);
        }


        auto CreateMinimal(std::size_t mesh_unique_id)
        {
            return std::make_unique<Segment2>(mesh_unique_id);
        }


        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            Advanced::GeometricEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                .RegisterGeometricElt<RefGeomEltNS::Segment2>(CreateMinimal, CreateEnsight);


    } // anonymous namespace


    Segment2::Segment2(std::size_t mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Segment2>(mesh_unique_id)
    { }


    Segment2::Segment2(std::size_t mesh_unique_id,
                       const Coords::vector_shared_ptr& mesh_coords_list,
                       std::istream& stream)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Segment2>(mesh_unique_id, mesh_coords_list, stream)
    { }


    Segment2::Segment2(std::size_t mesh_unique_id,
                       const Coords::vector_shared_ptr& mesh_coords_list,
                       std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Segment2>(mesh_unique_id,
                                                                         mesh_coords_list,
                                                                         std::move(coords_index_list))
    { }


    Segment2::~Segment2() = default;


    const RefGeomElt& Segment2::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }


    const RefGeomEltNS::Segment2& Segment2::StaticRefGeomElt()
    {
        static RefGeomEltNS::Segment2 ret;
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
