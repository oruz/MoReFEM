/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForCpp.hpp"
#include "Geometry/GeometricElt/Instances/Quadrangle/Quadrangle9.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle9.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Quadrangle9.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {


        auto CreateMinimal(std::size_t mesh_unique_id)
        {
            return std::make_unique<Quadrangle9>(mesh_unique_id);
        }


        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            Advanced::GeometricEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                .RegisterGeometricElt<RefGeomEltNS::Quadrangle9>(CreateMinimal, nullptr);


    } // anonymous namespace


    Quadrangle9::Quadrangle9(std::size_t mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle9>(mesh_unique_id)
    { }


    Quadrangle9::Quadrangle9(std::size_t mesh_unique_id,
                             const Coords::vector_shared_ptr& mesh_coords_list,
                             std::istream& stream)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle9>(mesh_unique_id, mesh_coords_list, stream)
    { }


    Quadrangle9::Quadrangle9(std::size_t mesh_unique_id,
                             const Coords::vector_shared_ptr& mesh_coords_list,
                             std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle9>(mesh_unique_id,
                                                                            mesh_coords_list,
                                                                            std::move(coords_index_list))
    { }


    Quadrangle9::~Quadrangle9() = default;


    const RefGeomElt& Quadrangle9::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }


    const RefGeomEltNS::Quadrangle9& Quadrangle9::StaticRefGeomElt()
    {
        static RefGeomEltNS::Quadrangle9 ret;
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
