/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron4.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Tetrahedron4 geometric element read in a mesh.
     *
     * We are not considering here a generic Tetrahedron4 object (that's the role of
     * MoReFEM::RefGeomEltNS::Tetrahedron4), but rather a specific geometric element of the mesh, with for instance the
     * coordinates of its coord, the list of its interfaces and so forth.
     */
    class Tetrahedron4 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron4>
    {
      public:
        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Tetrahedron4(std::size_t mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Tetrahedron4(std::size_t mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Tetrahedron4(std::size_t mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list);

        //! Destructor.
        ~Tetrahedron4() override;

        //! \copydoc doxygen_hide_copy_constructor
        Tetrahedron4(const Tetrahedron4& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Tetrahedron4(Tetrahedron4&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Tetrahedron4& operator=(const Tetrahedron4& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Tetrahedron4& operator=(Tetrahedron4&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

      private:
        //! Reference geometric element.
        static const RefGeomEltNS::Tetrahedron4& StaticRefGeomElt();
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_
