### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/FormatStrongType.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltEnum.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/TGeometricElt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TGeometricElt.hxx"
)

