/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_

// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"

#include <vector>

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace GeomEltNS
        {


            inline LocalMatrix& ComputeJacobian::GetNonCstJacobian() noexcept
            {
                return jacobian_;
            }


            inline std::vector<double>& ComputeJacobian::GetNonCstWorkFirstDerivateShapeFctForLocalNode() noexcept
            {
                return work_first_derivate_shape_fct_for_local_node_;
            }


        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_
