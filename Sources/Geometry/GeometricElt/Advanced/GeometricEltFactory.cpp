/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 16:47:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <sstream>
#include <string> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricEltFactory.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        GeometricEltFactory::~GeometricEltFactory() = default;


        const std::string GeometricEltFactory::ClassName()
        {
            static std::string ret("GeometricEltFactory");
            return ret;
        }


        GeometricEltFactory::GeometricEltFactory()
        {
            callbacks_ensight_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            callbacks_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            geom_ref_elt_type_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            geometric_elt_name_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            ensight_name_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        }


        GeometricElt::unique_ptr
        GeometricEltFactory ::CreateFromEnsightName(std::size_t mesh_unique_id,
                                                    const Coords::vector_shared_ptr& mesh_coords_list,
                                                    const Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                                    std::istream& stream) const
        {
            CallBackEnsight::const_iterator it = callbacks_ensight_.find(geometric_elt_name);

            if (it == callbacks_ensight_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidEnsightGeometricEltName(
                    geometric_elt_name, __FILE__, __LINE__);

            auto ret = it->second(mesh_unique_id, mesh_coords_list, stream);

            // If stream state is invalid return nullptr instead!
            if (stream)
                return ret;

            return nullptr;
        }


        RefGeomElt::shared_ptr GeometricEltFactory ::GetRefGeomEltPtr(Advanced::GeometricEltEnum identifier) const
        {
            auto it = geom_ref_elt_type_list_.find(identifier);

            if (it == geom_ref_elt_type_list_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier, __FILE__, __LINE__);

            const auto& pointer = it->second;
            assert(!(!pointer));

            return pointer;
        }


        Advanced::GeometricEltEnum
        GeometricEltFactory ::GetIdentifier(const Advanced::GeomEltNS::GenericName& geometric_reference_name) const
        {
            auto it = match_name_enum_.find(geometric_reference_name);

            if (it == match_name_enum_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltName(
                    geometric_reference_name, __FILE__, __LINE__);

            return it->second;
        }


        const std::string GeometricEltFactory::EnsightGeometricEltNames() const
        {
            if (callbacks_ensight_.empty())
                throw MoReFEM::Exception("No Ensight geometric elements defined!!!", __FILE__, __LINE__);

            std::ostringstream oconv;
            Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(callbacks_ensight_,
                                                                         oconv,
                                                                         PrintNS::Delimiter::separator(", "),
                                                                         PrintNS::Delimiter::opener("["),
                                                                         PrintNS::Delimiter::closer("]"));

            return oconv.str();
        }


        GeometricElt::unique_ptr GeometricEltFactory::CreateFromIdentifier(Advanced::GeometricEltEnum identifier,
                                                                           std::size_t mesh_unique_id) const
        {
            const auto it = callbacks_.find(identifier);

            if (it == callbacks_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier, __FILE__, __LINE__);

            return it->second(mesh_unique_id);
        }


        const RefGeomElt&
        GeometricEltFactory::GetRefGeomElt(const Advanced::GeomEltNS::GenericName& ref_geom_elt_name) const
        {
            auto identifier = GetIdentifier(ref_geom_elt_name);

            return GetRefGeomElt(identifier);
        }


        const RefGeomElt& GeometricEltFactory::GetRefGeomElt(const GeomEltNS::EnsightName& ensight_name) const
        {
            auto it = ensight_name_matching_.find(ensight_name);

            assert(it != ensight_name_matching_.cend());
            return GetRefGeomElt(it->second);
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
