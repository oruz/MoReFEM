/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;
    class LocalCoords;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace GeomEltNS
        {


            /*!
             * \brief Helper class to compute Jacobian for a given \a GeometricElt at a given \a LocalCoords (and thus
             * by extension at a given \a QuadraturePoint).
             *
             * The point of this class is to avoid reallocating needlessly memory for the matrix and the list of first
             * derivates values; one such object is intended to compute jacobians for each \a GeometricElt that share
             * the same underlying \a RefGeomElt.
             */
            class ComputeJacobian
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = ComputeJacobian;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] mesh_dimension Dimension in the mesh which \a GeomElt will use current facility. This is
                 * also by construct the number of rows and columns of the jacobian matrix.
                 */
                explicit ComputeJacobian(const std::size_t mesh_dimension);

                //! Destructor.
                ~ComputeJacobian() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ComputeJacobian(const ComputeJacobian& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ComputeJacobian(ComputeJacobian&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ComputeJacobian& operator=(const ComputeJacobian& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ComputeJacobian& operator=(ComputeJacobian&& rhs) = delete;

                ///@}


                /*!
                 * \brief Compute the value of the jacobian at \a geometric_elt and \a local_coords.
                 *
                 * \param[in] geom_elt \a GeometricElt considered.
                 * \param[in] local_coords \a LocalCoords for which the computation is performed.
                 *
                 * \return Jacobian matrix.
                 */
                const LocalMatrix& Compute(const GeometricElt& geom_elt, const LocalCoords& local_coords);

              private:
                /*!
                 * \brief Non constant accessor to the matrix that holds the result of the computation; it should not
                 * be accessible publicly.
                 */
                LocalMatrix& GetNonCstJacobian() noexcept;

                /*!
                 * \brief Non constant accessor to the a work variable.
                 *
                 * It is not intended to be accessed publicly or even to be used consistently from one method to
                 * another.
                 *
                 * \return List of first derivate values computed for a given \a LocalNode in \a Compute() method.
                 */
                std::vector<double>& GetNonCstWorkFirstDerivateShapeFctForLocalNode() noexcept;


              private:
                //! Matrix that holds the result of the computation; it should not be accessible publicly except through
                //! \a Compute() result.
                LocalMatrix jacobian_;

                /*!
                 * \brief A work variable to store first derivate computation results for a given \a LocalNode without reallocating memory each time.
                 *
                 * It is not intended to be accessed publicly or even to be used consistently from one method to
                 * another.
                 */
                std::vector<double> work_first_derivate_shape_fct_for_local_node_;
            };


        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HPP_
