/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 11:48:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FREE_FUNCTIONS_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FREE_FUNCTIONS_HPP_

// #include "Geometry/Coords/Coords.hpp" // if Global2local is reactivated

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;
    class SpatialPoint;
    class LocalCoords;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace GeomEltNS
        {


            /*!
             * \brief Computes the barycenter of a geometric element.
             *
             * \param[in] geometric_elt \a GeometricElt which barycenter is computed.
             * \param[out] barycenter Computed barycenter. Whatever value might have been in input is overwritten.
             */
            void ComputeBarycenter(const GeometricElt& geometric_elt, SpatialPoint& barycenter);


            /*!
             * \brief Find the global coordinates that match the local ones.
             *
             * \param[in] geometric_elt Geometric element considered.
             * \param[in] local_coords Local coordinates to transform into global ones. The dimension of this one must
             * be the same as the dimension of \a geometric_elt. \param[out] out The image of \a local_coords in the
             * global mesh.
             */
            void Local2Global(const GeometricElt& geometric_elt, const LocalCoords& local_coords, SpatialPoint& out);


            /*!
             * \brief Find the local coordinates that match the global ones.
             *
             * Works only if the dimension of \a geometric_elt and \a coords is the same.
             *
             * \note This code stems from Ondomatic and has not been used so far.
             *
             * \param[in] geometric_elt Geometric element considered.
             * \param[in] coords The Coords object to be converted into \a LocalCoords.
             * \param[in] mesh_dimension Dimension of the mesh.
             *
             * \return LocalCoords object matching \a coords.
             */
            //            LocalCoords
            //            Global2local(const GeometricElt& geometric_elt, const Coords& coords, std::size_t
            //            mesh_dimension);


        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/GeometricElt/Advanced/FreeFunctions.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FREE_FUNCTIONS_HPP_
