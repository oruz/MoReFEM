/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 14:21:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_

// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Libmesh/Libmesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace GeomEltNS
        {


            /// Deactivate noreturn here: it would trigger false positives. )
            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(std::size_t mesh_unique_id) : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(
                std::size_t mesh_unique_id,
                const Coords::vector_shared_ptr& mesh_coords_list,
                std::size_t index,
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords)
            : GeometricElt(mesh_unique_id, mesh_coords_list, index, std::move(coords))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(
                std::size_t mesh_unique_id,
                const Coords::vector_shared_ptr& mesh_coords_list,
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
            : GeometricElt(mesh_unique_id, mesh_coords_list, std::move(coords_index_list))
            {
                StaticAssertBuiltInConsistency();
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(std::size_t mesh_unique_id,
                                                            const Coords::vector_shared_ptr& mesh_coords_list,
                                                            std::istream& stream)
            : GeometricElt(mesh_unique_id)
            {
                StaticAssertBuiltInConsistency();

                std::vector<std::size_t> coords_list(TraitsRefGeomEltT::Ncoords);

                for (std::size_t i = 0ul; i < TraitsRefGeomEltT::Ncoords; ++i)
                    stream >> coords_list[i];

                if (stream)
                {
                    std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> coords_index_list(coords_list.size());

                    std::transform(coords_list.cbegin(),
                                   coords_list.cend(),
                                   coords_index_list.begin(),
                                   [](auto index)
                                   {
                                       return ::MoReFEM::CoordsNS::index_from_mesh_file(index);
                                   });

                    // Modify geometric element only if failbit not set.
                    SetCoordsList(mesh_coords_list, std::move(coords_index_list));
                } else
                    throw Exception("Failure to construct a GeometricElt from input stream.", __FILE__, __LINE__);
            }


            template<class TraitsRefGeomEltT>
            TGeometricElt<TraitsRefGeomEltT>::~TGeometricElt() = default;


            template<class TraitsRefGeomEltT>
            Advanced::GeometricEltEnum TGeometricElt<TraitsRefGeomEltT>::GetIdentifier() const
            {
                return TraitsRefGeomEltT::Identifier();
            }


            template<class TraitsRefGeomEltT>
            const Advanced::GeomEltNS::GenericName& TGeometricElt<TraitsRefGeomEltT>::GetName() const
            {
                return TraitsRefGeomEltT::ClassName();
            }


            template<class TraitsRefGeomEltT>
            std::size_t TGeometricElt<TraitsRefGeomEltT>::Ncoords() const
            {
                return TraitsRefGeomEltT::Ncoords;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TGeometricElt<TraitsRefGeomEltT>::Nvertex() const
            {
                return TraitsRefGeomEltT::topology::Nvertex;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TGeometricElt<TraitsRefGeomEltT>::Nedge() const
            {
                return TraitsRefGeomEltT::topology::Nedge;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TGeometricElt<TraitsRefGeomEltT>::Nface() const
            {
                return TraitsRefGeomEltT::topology::Nface;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TGeometricElt<TraitsRefGeomEltT>::GetDimension() const
            {
                return TraitsRefGeomEltT::topology::dimension;
            }


            template<class TraitsRefGeomEltT>
            inline void TGeometricElt<TraitsRefGeomEltT>::WriteEnsightFormat(std::ostream& stream,
                                                                             bool do_print_index) const
            {
                if constexpr (!EnsightSupport())
                {
                    static_cast<void>(stream);
                    static_cast<void>(do_print_index);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(
                        TraitsRefGeomEltT::ClassName().Get(), "Ensight", __FILE__, __LINE__);
                } else
                {
                    if (do_print_index)
                        stream << std::setw(8) << GetIndex();

                    decltype(auto) coords_list = GetCoordsList();

                    for (const auto& coord_ptr : coords_list)
                    {
                        assert(!(!coord_ptr));
                        stream << std::setw(8) << coord_ptr->GetIndexFromMeshFile();
                    }

                    stream << '\n';
                }
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::WriteMeditFormat(
                const libmeshb_int mesh_index,
                const std::unordered_map<::MoReFEM::CoordsNS::index_from_mesh_file, int>& processor_wise_reindexing)
                const
            {
                if constexpr (!MeditSupport())
                {
                    static_cast<void>(mesh_index);
                    static_cast<void>(processor_wise_reindexing);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(
                        TraitsRefGeomEltT::ClassName().Get(), "Medit", __FILE__, __LINE__);
                } else
                {
                    auto label_ptr = GetMeshLabelPtr();
                    int label_index = (label_ptr ? static_cast<int>(label_ptr->GetIndex()) : 0);

                    const auto& coords_list = GetCoordsList();

                    // Medit assumes indexes between 1 and Ncoord, whereas MoReFEM starts at 0.
                    std::vector<int> coord_index_list;
                    coord_index_list.reserve(coords_list.size());

                    for (const auto& coord_ptr : coords_list)
                    {
                        assert(!(!coord_ptr));
                        auto it = processor_wise_reindexing.find(coord_ptr->GetIndexFromMeshFile());

                        assert(it != processor_wise_reindexing.cend()
                               && "Otherwise processor_wise_reindexing is poorly built.");

                        coord_index_list.push_back(it->second);
                    }

                    ::MoReFEM::Wrappers::Libmesh ::MeditSetLin<TraitsRefGeomEltT::Ncoords>(
                        mesh_index, GetMeditIdentifier(), coord_index_list, label_index);
                }
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::ReadMeditFormat(const Coords::vector_shared_ptr& mesh_coords_list,
                                                                   libmeshb_int libmesh_mesh_index,
                                                                   std::size_t Ncoord_in_mesh,
                                                                   int& label_index)
            {
                if constexpr (!MeditSupport())
                {
                    static_cast<void>(mesh_coords_list);
                    static_cast<void>(libmesh_mesh_index);
                    static_cast<void>(Ncoord_in_mesh);
                    static_cast<void>(label_index);
                    throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(
                        TraitsRefGeomEltT::ClassName().Get(), "Medit", __FILE__, __LINE__);
                } else
                {
                    std::vector<std::size_t> coords(TraitsRefGeomEltT::Ncoords);

                    ::MoReFEM::Wrappers::Libmesh ::MeditGetLin<TraitsRefGeomEltT::Ncoords>(
                        libmesh_mesh_index, GetMeditIdentifier(), coords, label_index);

                    // Check here the indexes of the coords are between 1 and Ncoord:
                    for (auto coord_index : coords)
                    {
                        if (coord_index == 0 || coord_index > Ncoord_in_mesh)
                            throw MoReFEM::ExceptionNS::Format::Medit::InvalidCoordIndex(
                                TraitsRefGeomEltT::ClassName().Get(), coord_index, Ncoord_in_mesh, __FILE__, __LINE__);
                    }

                    std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> coord_index_list(TraitsRefGeomEltT::Ncoords);

                    std::transform(coords.cbegin(),
                                   coords.cend(),
                                   coord_index_list.begin(),
                                   [](const auto index)
                                   {
                                       return ::MoReFEM::CoordsNS::index_from_mesh_file(index);
                                   });

                    SetCoordsList(mesh_coords_list, std::move(coord_index_list));
                }
            }


            template<class TraitsRefGeomEltT>
            inline double TGeometricElt<TraitsRefGeomEltT>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                          const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::ShapeFunction(local_node_index, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double
            TGeometricElt<TraitsRefGeomEltT>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                         Advanced::ComponentNS::index_type icoor,
                                                                         const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::FirstDerivateShapeFunction(local_node_index, icoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double
            TGeometricElt<TraitsRefGeomEltT>::SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                          Advanced::ComponentNS::index_type icoor,
                                                                          Advanced::ComponentNS::index_type jcoor,
                                                                          const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::SecondDerivateShapeFunction(local_node_index, icoor, jcoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::BuildVertexList(const GeometricElt* geom_elt_ptr,
                                                                   Vertex::InterfaceMap& existing_list)
            {
                const auto& coords_list = GetCoordsList();

                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& vertex_list =
                    Internal::InterfaceNS::Build<Vertex, Topology>::Perform(geom_elt_ptr, coords_list, existing_list);

                SetVertexList(std::move(vertex_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::BuildEdgeList(const GeometricElt* geom_elt_ptr,
                                                                 Edge::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& oriented_edge_list =
                    Internal::InterfaceNS::ComputeEdgeList<Topology>(geom_elt_ptr, GetCoordsList(), existing_list);

                SetOrientedEdgeList(std::move(oriented_edge_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::BuildFaceList(const GeometricElt* geom_elt_ptr,
                                                                 Face::InterfaceMap& existing_list)
            {
                using Topology = typename TraitsRefGeomEltT::topology;

                auto&& oriented_face_list =
                    Internal::InterfaceNS::ComputeFaceList<Topology>(geom_elt_ptr, GetCoordsList(), existing_list);

                SetOrientedFaceList(std::move(oriented_face_list));
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::BuildVolumeList(const GeometricElt* geom_elt,
                                                                   Volume::InterfaceMap& existing_list)
            {
                Volume::shared_ptr volume_ptr =
                    (TraitsRefGeomEltT::topology::Nvolume > 0ul ? std::make_shared<Volume>(shared_from_this())
                                                                : nullptr);
                SetVolume(volume_ptr);

                if (!(!volume_ptr))
                {
                    assert("One Volume shoudn't be entered twice in the list: it is proper to each GeometricElt."
                           && existing_list.find(volume_ptr) == existing_list.cend());

                    std::vector<const GeometricElt*> vector_raw;
                    vector_raw.push_back(geom_elt);

                    existing_list.insert({ volume_ptr, vector_raw });
                }
            }


            template<class TraitsRefGeomEltT>
            void TGeometricElt<TraitsRefGeomEltT>::StaticAssertBuiltInConsistency()
            {
                static_assert(static_cast<std::size_t>(TraitsRefGeomEltT::Nderivate_component_)
                                  == static_cast<std::size_t>(TraitsRefGeomEltT::topology::dimension),
                              "Shape function should be consistent with geometric element!");
            }


            // Reactivate the warning disabled at the beginning of this file.
            PRAGMA_DIAGNOSTIC(pop)


        } // namespace GeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_T_GEOMETRIC_ELT_HXX_
