//! \file
//
//
//  FormatStrongType.hpp
//  MoReFEM
//
//  Created by sebastien on 15/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FORMAT_STRONG_TYPE_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FORMAT_STRONG_TYPE_HPP_

#include <string>

#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Advanced::GeomEltNS
{


    /*!
     * \brief Strong type for the generic name of a \a GeometricElt (e.g. 'Triangle3').
     *
     * This name is not format-dependant.
     */
    using GenericName = StrongType<std::string,
                                   struct GenericNameTag,
                                   StrongTypeNS::Printable,
                                   StrongTypeNS::Hashable,
                                   StrongTypeNS::Comparable,
                                   StrongTypeNS::DefaultConstructible>;


    /*!
     * \brief Strong type for the Ensight name (if relevant) of a \a GeometricElt .
     *
     */
    using EnsightName = StrongType<std::string,
                                   struct EnsightNameTag,
                                   StrongTypeNS::Printable,
                                   StrongTypeNS::Hashable,
                                   StrongTypeNS::Comparable>;


} // namespace MoReFEM::Advanced::GeomEltNS


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_FORMAT_STRONG_TYPE_HPP_
