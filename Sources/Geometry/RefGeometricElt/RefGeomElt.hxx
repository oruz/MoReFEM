/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Sep 2014 12:57:33 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HXX_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HXX_

// IWYU pragma: private, include "Geometry/RefGeometricElt/RefGeomElt.hpp"

// IWYU pragma: no_forward_declare MoReFEM::RefGeomElt


namespace MoReFEM
{


    inline bool operator==(const RefGeomElt& element1, const RefGeomElt& element2)
    {
        return element1.GetIdentifier() == element2.GetIdentifier();
    }


    inline bool operator<(const RefGeomElt& element1, const RefGeomElt& element2)
    {
        return element1.GetIdentifier() < element2.GetIdentifier();
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_REF_GEOM_ELT_HXX_
