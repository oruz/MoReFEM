/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Oct 2014 12:29:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HPP_

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief This helper class is used to access the local edges, faces and also the vertice coordinates.
             *
             * It must be used as follow:
             * \code
             * const auto& topology_face = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);
             * \endcode
             */
            template<class TopologyT>
            struct LocalData
            {

                /*!
                 * brief Yields a local interface object related to the given \a local_vertex_index vertex of \a
                 * TopologyT.
                 *
                 * \tparam IntegerT Type of the index.
                 *
                 * \param[in] local_vertex_index Local index of the vertex considered.
                 *
                 * \return Local interface object.
                 */
                template<class IntegerT>
                static LocalInterface ComputeLocalVertexInterface(IntegerT local_vertex_index);


                /*!
                 * \brief Provide access to the \a local_edge_index edge of \a TopologyT.
                 *
                 * \tparam IntegerT Type of the index.
                 * \param[in] local_edge_index Local index of the edge considered.
                 *
                 * \return Edge content of the edge, i.e. the vertices that defines it.
                 */
                template<class IntegerT>
                static const typename TopologyT::EdgeContent& GetEdge(IntegerT local_edge_index);

                /*!
                 * brief Yields a local interface object related to the given \a local_edge_index edge of \a TopologyT.
                 *
                 * \param[in] local_edge_index Local index of the edge considered.
                 * \tparam IntegerT Type of the index.
                 *
                 * \return Local interface object.
                 */
                template<class IntegerT>
                static LocalInterface ComputeLocalEdgeInterface(IntegerT local_edge_index);

                /*!
                 * \brief Returns the number of vertices in the edge.
                 *
                 * \param[in] local_edge_index Local index of the edge considered.
                 *
                 * We can't simply take the size() of \a GetEdge(local_edge_index) as the latter might be
                 * std::false_type.
                 *
                 * \return Number of vertices in the edge.
                 */
                template<class IntegerT>
                static std::size_t NverticeInEdge(IntegerT local_edge_index);


                /*!
                 * \brief Provide access to the \a local_face_index face of \a TopologyT.
                 *
                 * \param[in] local_face_index Local index of the face considered.
                 *
                 * \return Face content of the face, i.e. the vertices that defines it.
                 */
                template<class IntegerT>
                static const typename TopologyT::FaceContent& GetFace(IntegerT local_face_index);


                /*!
                 * brief Yields a local interface object related to the given \a local_face_index edge of \a TopologyT.
                 *
                 * \tparam IntegerT Type of the index.
                 *
                 * \param[in] local_face_index Local index of the face considered.
                 *
                 * \return Local interface object.
                 */
                template<class IntegerT>
                static LocalInterface ComputeLocalFaceInterface(IntegerT local_face_index);


                /*!
                 * \brief Provide access to the LocalCoords of the vertices of \a TopologyT.
                 *
                 * \param[in] local_vertex_index Local index of the vertex considered.
                 *
                 * \return \a LocalCoords matching the vertex pointed by \a local_vertex_index
                 */
                template<class IntegerT>
                static const LocalCoords& GetVertexCoord(IntegerT local_vertex_index);


                /*!
                 * \brief Yields a local interface object related to the interior of \a TopologyT
                 *
                 * \return Local interface object.
                 */
                static LocalInterface ComputeLocalInteriorInterface();

                /*!
                 * \brief Yields the number of interface of a given \a nature.
                 *
                 * For instance Nelement(Nature::vertex) yields 4 for a quadrangle.
                 *
                 * \param[in] nature Nature of the interface considered.
                 *
                 * \return Number of interfaces of type \a nature.
                 */
                static std::size_t Nelement(InterfaceNS::Nature nature) noexcept;


                /*!
                 * \brief Returns a local interface of type \a nature upon which the \a local_coords is located.
                 *
                 * Throw an exception if none matches.
                 *
                 * \param[in] nature Nature of the interface considered.
                 * \param[in] local_coords LocalCoords for which a match is sought.
                 *
                 * \return Local interface of type \a nature onto which \a local_coords may be found.
                 */
                static LocalInterface FindLocalInterface(InterfaceNS::Nature nature, const LocalCoords& local_coords);


              private:
                /*!
                 * \brief Helper function of NverticeInEdgeHelper in the case TopologyT::EdgeContent is not
                 * std::false_type.
                 *
                 * \param[in] local_edge_index Local index of the edge considered.
                 * \param[in] placeholder Only its type matter: it is used to dispatch correctly the topologies for
                 * which calling  NverticeInEdge() is relevant from those for which it isn't ((for instance a Point...).
                 *
                 * \return Number of vertices on the edge.
                 */
                template<class IntegerT>
                static std::size_t NverticeInEdgeHelper(IntegerT local_edge_index, std::true_type placeholder);


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                // Just there to make the code compile!
                template<class IntegerT>
                [[noreturn]] static std::size_t NverticeInEdgeHelper(IntegerT local_edge_index, std::false_type);


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================
            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HPP_
