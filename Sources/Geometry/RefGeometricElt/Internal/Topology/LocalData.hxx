/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Oct 2014 12:05:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HXX_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HXX_

// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            template<class TopologyT>
            template<class IntegerT>
            LocalInterface LocalData<TopologyT>::ComputeLocalVertexInterface(IntegerT local_vertex_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                assert(local_vertex_index < TopologyT::Nvertex);

                std::array<std::size_t, 1> interface_content{ { local_vertex_index } };

                return LocalInterface(interface_content, InterfaceNS::Nature::vertex, IsInterior::no);
            }


            template<class TopologyT>
            template<class IntegerT>
            inline const typename TopologyT::EdgeContent& LocalData<TopologyT>::GetEdge(IntegerT local_edge_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                const auto& local_edge_list = TopologyT::GetEdgeList();

                const auto size_t_index = local_edge_index;
                assert(size_t_index < local_edge_list.size());

                return local_edge_list[size_t_index];
            }


            template<class TopologyT>
            template<class IntegerT>
            LocalInterface LocalData<TopologyT>::ComputeLocalEdgeInterface(IntegerT local_edge_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                return LocalInterface(GetEdge(local_edge_index), InterfaceNS::Nature::edge, IsInterior::no);
            }


            template<class TopologyT>
            template<class IntegerT>
            std::size_t LocalData<TopologyT>::NverticeInEdge(IntegerT local_edge_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                using EdgeContent = typename TopologyT::EdgeContent;

                // clang-format off
                return NverticeInEdgeHelper(local_edge_index,
                                            std::conditional_t
                                            <
                                                std::is_same<EdgeContent, std::false_type>::value,
                                                std::false_type,
                                                std::true_type
                                            >());
                // clang-format on
            }


            template<class TopologyT>
            template<class IntegerT>
            inline const typename TopologyT::FaceContent& LocalData<TopologyT>::GetFace(IntegerT local_face_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                const auto& local_face_list = TopologyT::GetFaceList();
                const auto size_t_index = local_face_index;
                assert(size_t_index < local_face_list.size());

                return local_face_list[size_t_index];
            }


            template<class TopologyT>
            template<class IntegerT>
            LocalInterface LocalData<TopologyT>::ComputeLocalFaceInterface(IntegerT local_face_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                return LocalInterface(GetFace(local_face_index), InterfaceNS::Nature::face, IsInterior::no);
            }


            template<class TopologyT>
            template<class IntegerT>
            const LocalCoords& LocalData<TopologyT>::GetVertexCoord(IntegerT local_vertex_index)
            {
                static_assert(std::is_integral<IntegerT>::value,
                              "Template argument is expected to be an integral type!");

                const auto& local_vertex_coord_list = TopologyT::GetVertexLocalCoordsList();

                const auto size_t_index = local_vertex_index;
                assert(size_t_index < local_vertex_coord_list.size());

                return local_vertex_coord_list[size_t_index];
            }


            template<class TopologyT>
            template<class IntegerT>
            std::size_t LocalData<TopologyT>::NverticeInEdgeHelper(IntegerT local_edge_index, std::true_type)
            {
                static_assert(!std::is_same<typename TopologyT::EdgeContent, std::false_type>::value,
                              "Second argument of the function is expected to be the result of the is_same above!");

                return GetEdge(local_edge_index).size();
            }


            template<class TopologyT>
            template<class IntegerT>
            std::size_t LocalData<TopologyT>::NverticeInEdgeHelper(IntegerT local_edge_index, std::false_type)
            {
                static_cast<void>(local_edge_index);

                static_assert(std::is_same<typename TopologyT::EdgeContent, std::false_type>::value,
                              "Second argument of the function is expected to be the result of the is_same above!");

                assert(false && "This overload should never be called in runtime!");
            }


            template<class TopologyT>
            LocalInterface LocalData<TopologyT>::ComputeLocalInteriorInterface()
            {
                std::array<std::size_t, 0> empty;

                return LocalInterface(empty, TopologyT::GetInteriorInterface(), IsInterior::yes);
            }


            template<class TopologyT>
            std::size_t LocalData<TopologyT>::Nelement(InterfaceNS::Nature nature) noexcept
            {
                switch (nature)
                {
                case InterfaceNS::Nature::vertex:
                    return TopologyT::Nvertex;
                case InterfaceNS::Nature::edge:
                    return TopologyT::Nedge;
                case InterfaceNS::Nature::face:
                    return TopologyT::Nface;
                case InterfaceNS::Nature::volume:
                    return TopologyT::Nvolume;
                case InterfaceNS::Nature::none:
                case InterfaceNS::Nature::undefined:
                    assert(false);
                    break;
                }

                assert(false);
                return NumericNS::UninitializedIndex<std::size_t>();
            }


            template<class TopologyT>
            LocalInterface LocalData<TopologyT>::FindLocalInterface(InterfaceNS::Nature nature,
                                                                    const LocalCoords& local_coords)
            {
                switch (nature)
                {
                case InterfaceNS::Nature::vertex:
                {
                    for (std::size_t i = 0ul; i < TopologyT::Nvertex; ++i)
                    {
                        if (TopologyT::IsOnVertex(i, local_coords))
                            return ComputeLocalVertexInterface(i);
                    }
                    break;
                }
                case InterfaceNS::Nature::edge:
                {
                    for (std::size_t i = 0ul; i < TopologyT::Nedge; ++i)
                    {
                        if (TopologyT::IsOnEdge(i, local_coords))
                            return ComputeLocalEdgeInterface(i);
                    }
                    break;
                }
                case InterfaceNS::Nature::face:
                {
                    for (std::size_t i = 0ul; i < TopologyT::Nface; ++i)
                    {
                        if (TopologyT::IsOnFace(i, local_coords))
                            return ComputeLocalFaceInterface(i);
                    }
                    break;
                }
                case InterfaceNS::Nature::volume:
                {
                    if (TopologyT::IsInside(local_coords))
                        return ComputeLocalInteriorInterface();
                    break;
                }
                case InterfaceNS::Nature::none:
                case InterfaceNS::Nature::undefined:
                    break;
                }

                std::ostringstream oconv;
                oconv << "Local coords " << local_coords << " couldn't be associated to any interface of type "
                      << nature << '.';

                throw Exception(oconv.str(), __FILE__, __LINE__);
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_LOCAL_DATA_HXX_
