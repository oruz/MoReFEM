/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 May 2013 13:43:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_CONSTANT_SHAPE_FUNCTION_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_CONSTANT_SHAPE_FUNCTION_HPP_


#include <functional>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class LocalCoords;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            /*!
             * \brief This function is used to define a ShapeFunctionType that returns a (rational) constant.
             *
             * It doesn't work with irrational values due to the template nature of the instantiation.
             *
             * \return Function that returns the same integer or rational value for all spatial positions.
             */
            template<int NumT, int DenomT = 1>
            static const ShapeFunctionType Constant()
            {

                return [](const LocalCoords&) -> double
                {
                    return static_cast<double>(NumT) / static_cast<double>(DenomT);
                };
            }


        } // namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_CONSTANT_SHAPE_FUNCTION_HPP_
