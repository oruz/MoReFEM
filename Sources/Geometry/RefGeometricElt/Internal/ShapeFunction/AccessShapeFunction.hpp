/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 12:26:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_

#include <cassert>

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            namespace Crtp
            {


                /*!
                 * \brief Curiously recurrent template pattern (CRTP) that provides two accessors to the value
                 * of a shape function and its derivates.
                 *
                 * \tparam DerivedT Any element of RefGeomEltNS::ShapeFunctionNS namespace.
                 */
                template<class DerivedT>
                struct AccessShapeFunction
                {

                    /*!
                     * \copydoc doxygen_hide_shape_function
                     */
                    static double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                const LocalCoords& local_coords);


                    //! \copydoc doxygen_hide_first_derivate_shape_function
                    static double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                             Advanced::ComponentNS::index_type component,
                                                             const LocalCoords& local_coords);
                };


            } // namespace Crtp


        } // namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HPP_
