/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 10:56:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <deque>

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            /*!
             * \brief Derived from 'RefGeomElt', for which it defines all the virtual methods.
             *
             * 'TRefGeomElt' stands for templatized FeomRefElt.
             *
             * \tparam TraitsRefGeomEltT A trait class that defines RefGeomElt behaviour. This is a class that must be
             * defined within RefGeomEltNS::Traits namespace, e.g. RefGeomEltNS::Traits::Triangle3.
             */
            template<class TraitsRefGeomEltT>
            class TRefGeomElt : public RefGeomElt
            {

              public:
                //! Alias to the template parameter of the class.
                using traits = TraitsRefGeomEltT;

                /*!
                 * \brief Object that store the information about Medit support.
                 *
                 * This object inherits from std::true_type if Medit is supported, std::false_type otherwise.
                 */
                using MeditSupport =
                    Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit, traits::Identifier()>;

                /*!
                 * \brief Object that store the information about Ensight support.
                 *
                 * This object inherits from std::true_type if Ensight is supported, std::false_type otherwise.
                 */
                using EnsightSupport =
                    Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight, traits::Identifier()>;

              protected:
                ///@{

                //! Default constructor.
                TRefGeomElt() = default;

                //! Destructor.
                virtual ~TRefGeomElt() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                TRefGeomElt(const TRefGeomElt& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                TRefGeomElt(TRefGeomElt&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                TRefGeomElt& operator=(const TRefGeomElt& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                TRefGeomElt& operator=(TRefGeomElt&& rhs) = delete;


              public:
                /*!
                 * \brief Get the identifier of the geometric element.
                 *
                 * \return The identifier of a GeometricElt as defined within MoReFEM (independant of IO format).
                 */
                virtual Advanced::GeometricEltEnum GetIdentifier() const override final;

                //! Get the dimension of the geometric element.
                virtual std::size_t GetDimension() const override final;

                /*!
                 * \brief Get the number of Coords object required to characterize completely a GeometricElt of this
                 * type.
                 *
                 * For instance 27 for an Hexahedron27.
                 *
                 * \return Number of Coords object required to characterize completely a GeometricElt of this type.
                 */
                virtual std::size_t Ncoords() const override final;

                //! Returns the name of the geometric element (for instance 'Triangle3').
                virtual const Advanced::GeomEltNS::GenericName& GetName() const override final;

                //! Get the name associated to the Topology (e.g. 'Triangle').
                virtual const std::string& GetTopologyName() const override final;

                //! Get the enum value associated to the Topology (e.g. 'RefGeomEltNS::TopologyNS::Type::tetrahedron').
                virtual ::MoReFEM::RefGeomEltNS::TopologyNS::Type GetTopologyIdentifier() const override final;

                //! Get the local coordinates of the barycenter.
                virtual const LocalCoords& GetBarycenter() const override final;

                //! Get the list of local coordinates of the vertices.
                virtual const std::vector<LocalCoords>& GetVertexLocalCoordsList() const override final;

                //! \copydoc doxygen_hide_shape_function
                virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                             const LocalCoords& local_coords) const override final;

                //! \copydoc doxygen_hide_first_derivate_shape_function
                virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                          Advanced::ComponentNS::index_type component,
                                                          const LocalCoords& local_coords) const override final;

                //! \copydoc doxygen_hide_second_derivate_shape_function
                virtual double SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                           Advanced::ComponentNS::index_type component1,
                                                           Advanced::ComponentNS::index_type component2,
                                                           const LocalCoords& local_coords) const override final;

                //! Return the number of vertices.
                virtual std::size_t Nvertex() const noexcept override final;

                //! Return the number of edges.
                virtual std::size_t Nedge() const noexcept override final;

                //! Return the number of faces.
                virtual std::size_t Nface() const noexcept override final;

                //! Returns the nature of the interior interface.
                virtual ::MoReFEM::InterfaceNS::Nature GetInteriorInterfaceNature() const noexcept override final;

                /*!
                 * \brief Get the identifier Medit use to tag the geometric element.
                 *
                 * An exception is thrown if Medit format is not supported.
                 *
                 * \return Identifier.
                 */
                virtual GmfKwdCod GetMeditIdentifier() const override final;

                //! Get the Ensight name. If Ensight doesn't support the type empty string is returned.
                virtual const Advanced::GeomEltNS::EnsightName& GetEnsightName() const override final;
            };


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_
