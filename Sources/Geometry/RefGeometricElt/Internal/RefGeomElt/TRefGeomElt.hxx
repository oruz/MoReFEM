/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 10:04:38 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_

// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            template<class TraitsRefGeomEltT>
            inline Advanced::GeometricEltEnum TRefGeomElt<TraitsRefGeomEltT>::GetIdentifier() const
            {
                return TraitsRefGeomEltT::Identifier();
            }


            template<class TraitsRefGeomEltT>
            inline std::size_t TRefGeomElt<TraitsRefGeomEltT>::GetDimension() const
            {
                return TraitsRefGeomEltT::topology::dimension;
            }


            template<class TraitsRefGeomEltT>
            inline const Advanced::GeomEltNS::GenericName& TRefGeomElt<TraitsRefGeomEltT>::GetName() const
            {
                return TraitsRefGeomEltT::ClassName();
            }


            template<class TraitsRefGeomEltT>
            inline const std::string& TRefGeomElt<TraitsRefGeomEltT>::GetTopologyName() const
            {
                return TraitsRefGeomEltT::topology::ClassName();
            }


            template<class TraitsRefGeomEltT>
            inline ::MoReFEM::RefGeomEltNS::TopologyNS::Type
            TRefGeomElt<TraitsRefGeomEltT>::GetTopologyIdentifier() const
            {
                return TraitsRefGeomEltT::topology::GetType();
            }


            template<class TraitsRefGeomEltT>
            inline std::size_t TRefGeomElt<TraitsRefGeomEltT>::Ncoords() const
            {
                return TraitsRefGeomEltT::Ncoords;
            }


            template<class TraitsRefGeomEltT>
            const LocalCoords& TRefGeomElt<TraitsRefGeomEltT>::GetBarycenter() const
            {
                return TraitsRefGeomEltT::GetBarycenter();
            }


            template<class TraitsRefGeomEltT>
            const std::vector<LocalCoords>& TRefGeomElt<TraitsRefGeomEltT>::GetVertexLocalCoordsList() const
            {
                return TraitsRefGeomEltT::topology::GetVertexLocalCoordsList();
            }


            template<class TraitsRefGeomEltT>
            inline double TRefGeomElt<TraitsRefGeomEltT>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                        const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::ShapeFunction(local_node_index, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double
            TRefGeomElt<TraitsRefGeomEltT>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                       Advanced::ComponentNS::index_type icoor,
                                                                       const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::FirstDerivateShapeFunction(local_node_index, icoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double
            TRefGeomElt<TraitsRefGeomEltT>::SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                        Advanced::ComponentNS::index_type icoor,
                                                                        Advanced::ComponentNS::index_type jcoor,
                                                                        const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::SecondDerivateShapeFunction(local_node_index, icoor, jcoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nvertex() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nvertex;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nedge() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nedge;
            }


            template<class TraitsRefGeomEltT>
            std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nface() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nface;
            }


            template<class TraitsRefGeomEltT>
            ::MoReFEM::InterfaceNS::Nature TRefGeomElt<TraitsRefGeomEltT>::GetInteriorInterfaceNature() const noexcept
            {
                return TraitsRefGeomEltT::topology::GetInteriorInterface();
            }


            template<class TraitsRefGeomEltT>
            GmfKwdCod TRefGeomElt<TraitsRefGeomEltT>::GetMeditIdentifier() const
            {
                if constexpr (!MeditSupport())
                {
                    throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(
                        TraitsRefGeomEltT::ClassName().Get(), "Medit", __FILE__, __LINE__);
                } else
                {
                    return Internal::MeshNS::FormatNS ::Support<::MoReFEM::MeshNS::Format::Medit,
                                                                TraitsRefGeomEltT::Identifier()>::MeditId();
                }
            }


            template<class TraitsRefGeomEltT>
            inline const Advanced::GeomEltNS::EnsightName& TRefGeomElt<TraitsRefGeomEltT>::GetEnsightName() const
            {
                if constexpr (!EnsightSupport())
                {
                    throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(
                        TraitsRefGeomEltT::ClassName().Get(), "Ensight", __FILE__, __LINE__);
                } else
                {
                    static auto ret =
                        Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                            TraitsRefGeomEltT::Identifier()>::EnsightName();

                    return ret;
                }
            }


            namespace Impl
            {


                template<class ListT>
                std::deque<bool> IsNodeOnReferenceInterface(const ListT& reference_interface_list)
                {
                    std::deque<bool> ret;

                    for (const auto& reference_interface : reference_interface_list)
                        ret.push_back(reference_interface.IsNode());

                    return ret;
                }

            } // namespace Impl


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_
