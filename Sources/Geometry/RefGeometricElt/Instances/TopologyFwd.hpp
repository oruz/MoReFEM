//! \file
//
//
//  TopologyFwd.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_FWD_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_FWD_HPP_

#include <string> // IWYU pragma: keep // IWYU pragma: export
// IWYU pragma: no_include <iosfwd> // IWYU pragma: export

#include <algorithm>   // IWYU pragma: export
#include <cassert>     // IWYU pragma: export
#include <limits>      // IWYU pragma: export
#include <tuple>       // IWYU pragma: export
#include <type_traits> // IWYU pragma: export
#include <utility>     // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "Geometry/Interfaces/LocalInterface/IsOnLocalInterface.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"  // IWYU pragma: export

#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_FWD_HPP_
