/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            const std::string& Triangle::ClassName()
            {
                static std::string ret("Triangle");
                return ret;
            }


            /************************************************************************
             *
             *     2
             *     | \
             *     |   \
             *     |     \
             *     |       \
             *     |         \
             *     0-----------1
             *
             *************************************************************************/


            const std::array<Triangle::EdgeContent, Triangle::Nedge>& Triangle::GetEdgeList()
            {
                static std::array<Triangle::EdgeContent, Triangle::Nedge> ret{
                    { { { 0ul, 1u } }, { { 1u, 2u } }, { { 0ul, 2u } } }
                };

                return ret;
            }


            const std::array<Triangle::FaceContent, Triangle::Nface>& Triangle::GetFaceList()
            {
                static std::array<Triangle::FaceContent, Triangle::Nface> ret{ { { { 0ul, 1u, 2u } } } };

                return ret;
            }


            bool Triangle::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
            {
                return Internal::RefGeomEltNS::TopologyNS::IsOnVertex<Triangle>(vertex_index, coords);
            }


            bool Triangle::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
            {
                if (!IsInside(coords))
                    return false;

                assert(edge_index < 3u);
                assert(coords.GetDimension() == 2u);
                const double r = coords.r();
                const double s = coords.s();

                switch (edge_index)
                {
                case 0ul:
                    return NumericNS::IsZero(s);
                case 1u:
                    return NumericNS::AreEqual(1., r + s);
                case 2u:
                    return NumericNS::IsZero(r);
                }

                assert(false);
                return false;
            }


            bool Triangle::IsOnFace(std::size_t face_index, const LocalCoords& coords)
            {
                static_cast<void>(face_index);
                assert(face_index == 0ul);
                return IsInside(coords);
            }


            LocalCoords
            Triangle::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
            {
                assert(face_index == 0ul && "Only choice for a 2D triangle...");
                assert(IsOnFace(face_index, coords));
                assert((orientation < 6u));
                static_cast<void>(face_index); // to avoid warning in release mode.

                const double x_before = coords[0];
                const double y_before = coords[1];

                assert(!NumericNS::AreEqual(x_before, std::numeric_limits<double>::lowest()));
                assert(!NumericNS::AreEqual(y_before, std::numeric_limits<double>::lowest()));

                double x_after = std::numeric_limits<double>::lowest();
                double y_after = std::numeric_limits<double>::lowest();

                switch (orientation)
                {
                case 0ul:
                {
                    x_after = x_before;
                    y_after = y_before;
                    break;
                }
                case 1u:
                {
                    x_after = 1u - x_before - y_before;
                    y_after = x_before;
                    break;
                }
                case 2u:
                {
                    x_after = y_before;
                    y_after = 1 - x_before - y_before;
                    break;
                }
                case 3u:
                {
                    x_after = y_before;
                    y_after = x_before;
                    break;
                }
                case 4u:
                {
                    x_after = 1 - x_before - y_before;
                    y_after = y_before;
                    break;
                }
                case 5u:
                {
                    x_after = x_before;
                    y_after = 1 - x_before - y_before;
                    break;
                }
                }

                assert(!NumericNS::AreEqual(x_after, std::numeric_limits<double>::lowest()));
                assert(!NumericNS::AreEqual(y_after, std::numeric_limits<double>::lowest()));

                LocalCoords ret({ x_after, y_after });
                return ret;
            }


            const std::vector<LocalCoords>& Triangle::GetVertexLocalCoordsList()
            {
                static std::vector<LocalCoords> ret{ { 0., 0. }, { 1., 0. }, { 0., 1. } };

                return ret;
            }


            bool Triangle::IsInside(const LocalCoords& coords)
            {
                assert(coords.GetDimension() == 2u);

                const double r = coords.r();
                const double s = coords.s();

                if (r < 0. || s < 0.)
                    return false;

                if (r + s <= 1.)
                    return true;

                return false;
            }


            InterfaceNS::Nature Triangle::GetInteriorInterface()
            {
                return InterfaceNS::Nature::face;
            }


            Type Triangle::GetType() noexcept
            {
                return Type::triangle;
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
