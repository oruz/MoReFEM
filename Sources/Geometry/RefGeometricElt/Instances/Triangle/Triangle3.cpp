/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 09:23:03 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Triangle/Triangle3.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Triangle3::~Triangle3() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Triangle3::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Triangle3");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
