/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 16:20:29 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle6.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {

            const std::array<ShapeFunctionType, 6>& Triangle6::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 6> ret{
                    { [](const auto& local_coords)
                      {
                          return (1. - local_coords.r() - local_coords.s())
                                 * (1. - local_coords.r() - local_coords.r() - local_coords.s() - local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return -local_coords.r() * (1. - local_coords.r() - local_coords.r());
                      },
                      [](const auto& local_coords)
                      {
                          return -local_coords.s() * (1. - local_coords.s() - local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return (4. * local_coords.r() * (1. - local_coords.r() - local_coords.s()));
                      },
                      [](const auto& local_coords)
                      {
                          return (4. * local_coords.r() * local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return (4. * local_coords.s() * (1. - local_coords.r() - local_coords.s()));
                      } }
                };

                return ret;
            };


            const std::array<ShapeFunctionType, 12>& Triangle6::FirstDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 12> ret{
                    { [](const auto& local_coords)
                      {
                          return 4. * (local_coords.r() + local_coords.s()) - 3.;
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * (local_coords.r() + local_coords.s()) - 3.;
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.r() - 1.;
                      },
                      Constant<0>(),
                      Constant<0>(),
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.s() - 1.;
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * (1. - local_coords.r() - local_coords.r() - local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return -4. * local_coords.r();
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.s();
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.r();
                      },
                      [](const auto& local_coords)
                      {
                          return -4. * local_coords.s();
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * (1. - local_coords.r() - local_coords.s() - local_coords.s());
                      } }
                };

                return ret;
            };


            const std::array<ShapeFunctionType, 24>& Triangle6::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 24> ret{
                    { Constant<0>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<0>(),
                      Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),
                      Constant<-8>(), Constant<-4>(), Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<4>(),
                      Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<-4>(), Constant<-4>(), Constant<-8>() }
                };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
