/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Triangle/Triangle6.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Triangle6::~Triangle6() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Triangle6::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Triangle6");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
