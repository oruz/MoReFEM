/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Quadrangle/Quadrangle9.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Quadrangle9::~Quadrangle9() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Quadrangle9::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Quadrangle9");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
