/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle8.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                const Advanced::GeomEltNS::EnsightName&
                Support<::MoReFEM::MeshNS::Format::Ensight, Advanced::GeometricEltEnum::Quadrangle8>::EnsightName()
                {
                    static Advanced::GeomEltNS::EnsightName ret("quad8");
                    return ret;
                };


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
