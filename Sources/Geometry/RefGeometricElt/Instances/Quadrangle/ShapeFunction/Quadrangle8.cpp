/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Mar 2014 16:15:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle8.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep


namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS
{


    const std::array<ShapeFunctionType, 8>& Quadrangle8::ShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 8> ret{
            { [](const auto& local)
              {
                  return 0.25 * (-1. - local.r() - local.s()) * (1. - local.r()) * (1. - local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (-1. + local.r() - local.s()) * (1. + local.r()) * (1. - local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (-1. + local.r() + local.s()) * (1. + local.r()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (-1. - local.r() + local.s()) * (1. - local.r()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - (local.r() * local.r())) * (1. - local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * (1. - (local.s() * local.s()));
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - (local.r() * local.r())) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r()) * (1. - (local.s() * local.s()));
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 16>& Quadrangle8::FirstDerivateShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 16> ret{
            { [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * (-2. * local.r() + local.s());
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * (-local.r() - 2. * local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (1. - local.s()) * (2. * local.r() + local.s());
              },
              [](const auto& local)
              {
                  return -0.25 * (1. + local.r()) * (local.r() - 2. * local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * (2. * local.r() - local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * (local.r() + 2. * local.s());
              },
              [](const auto& local)
              {
                  return -0.25 * (1. + local.s()) * (-2. * local.r() - local.s());
              },
              [](const auto& local)
              {
                  return 0.25 * (1. - local.r()) * (-local.r() + 2. * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. - local.s());
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r() * local.r());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. + local.r());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r());
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. - local.r());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 32>& Quadrangle8::SecondDerivateShapeFunctionList()
    {

        static std::array<ShapeFunctionType, 32> ret{ { [](const auto& local)
                                                        {
                                                            return 0.5 * (1. - local.s());
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.25 * (-2. * local.r() + 2. * local.s() - 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.25 * (-2. * local.r() - 2. * local.s() + 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. - local.r());
                                                        },

                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. - local.s());
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -0.25 * (2. * local.r() + 2. * local.s() - 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -0.25 * (2. * local.r() - 2. * local.s() + 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. + local.r());
                                                        },

                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. + local.s());
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.25 * (2. * local.r() - 2. * local.s() - 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.25 * (2. * local.r() + 2. * local.s() + 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. + local.r());
                                                        },

                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. + local.s());
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -0.25 * (-2. * local.r() - 2. * local.s() - 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -0.25 * (-2. * local.r() + 2. * local.s() + 1.);
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return 0.5 * (1. - local.r());
                                                        },

                                                        [](const auto& local)
                                                        {
                                                            return -1. + local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return local.r();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return local.r();
                                                        },
                                                        Constant<0>(),

                                                        Constant<0>(),
                                                        [](const auto& local)
                                                        {
                                                            return -local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -1. - local.r();
                                                        },

                                                        [](const auto& local)
                                                        {
                                                            return -1. - local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -local.r();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -local.r();
                                                        },
                                                        Constant<0>(),

                                                        Constant<0>(),
                                                        [](const auto& local)
                                                        {
                                                            return local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return local.s();
                                                        },
                                                        [](const auto& local)
                                                        {
                                                            return -1. + local.r();
                                                        } } };

        return ret;
    };


} // namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS


/// @} // addtogroup GeometryGroup
