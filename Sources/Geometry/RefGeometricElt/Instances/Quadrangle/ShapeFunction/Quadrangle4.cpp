/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Mar 2014 16:06:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle4.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            const std::array<ShapeFunctionType, 4>& Quadrangle4::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 4> ret{
                    { [](const auto& local_coords)
                      {
                          return 0.25 * (1. - local_coords.r()) * (1. - local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.25 * (1. + local_coords.r()) * (1. - local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.25 * (1. + local_coords.r()) * (1. + local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.25 * (1. - local_coords.r()) * (1. + local_coords.s());
                      } }
                };

                return ret;
            };


            const std::array<ShapeFunctionType, 8>& Quadrangle4::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 8> ret{ { [](const auto& local_coords)
                                                               {
                                                                   return -0.25 * (1. - local_coords.s());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return -0.25 * (1. - local_coords.r());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return 0.25 * (1. - local_coords.s());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return -0.25 * (1. + local_coords.r());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return 0.25 * (1. + local_coords.s());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return 0.25 * (1. + local_coords.r());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return -0.25 * (1. + local_coords.s());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return 0.25 * (1. - local_coords.r());
                                                               } } };

                return ret;
            };


            const std::array<ShapeFunctionType, 16>& Quadrangle4::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 16> ret{ {
                    Constant<0>(),
                    Constant<1, 4>(),
                    Constant<1, 4>(),
                    Constant<0>(),
                    Constant<0>(),
                    Constant<-1, 4>(),
                    Constant<-1, 4>(),
                    Constant<0>(),
                    Constant<0>(),
                    Constant<1, 4>(),
                    Constant<1, 4>(),
                    Constant<0>(),
                    Constant<0>(),
                    Constant<-1, 4>(),
                    Constant<-1, 4>(),
                    Constant<0>(),
                } };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
