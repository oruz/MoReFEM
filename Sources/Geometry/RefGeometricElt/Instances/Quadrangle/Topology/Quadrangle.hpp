/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TOPOLOGY_x_QUADRANGLE_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TOPOLOGY_x_QUADRANGLE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyCommon.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief Topology of a quadrangle.
             */
            class Quadrangle final
            {

              public:
                //! Data.
                enum : std::size_t
                {
                    dimension = 2u,
                    Nvertex = 4u,
                    Nedge = 4u,
                    Nface = 1u,
                    Nvolume = 0ul,
                };


                //! Topology of an edge.
                using EdgeTopology = Segment;

                //! Topology of a face.
                using FaceTopology = Quadrangle;

                //! Container used to store all the points of an edge. The index of the points are actually stored.
                using EdgeContent = std::array<std::size_t, EdgeTopology::Nvertex>;

                //! Container used to store all the points of a face. The index of the points are actually stored.
                using FaceContent = std::array<std::size_t, FaceTopology::Nvertex>;

                //! Container used to store the local coordinates of the vertices.
                using VerticeCoordListType = std::vector<LocalCoords>;


                /*!
                 * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
                 * stored.
                 */
                static InterfaceNS::Nature GetInteriorInterface();

                //! Returns the enum that tags the topology.
                static Type GetType() noexcept;


              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit Quadrangle() = default;

              protected:
                //! Destructor.
                ~Quadrangle() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Quadrangle(const Quadrangle& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Quadrangle(Quadrangle&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Quadrangle& operator=(const Quadrangle& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Quadrangle& operator=(Quadrangle&& rhs) = delete;

                ///@}

              public:
                //! Return the name of the topology.
                static const std::string& ClassName();

                /*!
                 * \copydoc doxygen_hide_is_on_local_vertex
                 */
                static bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);

                /*!
                 * \copydoc doxygen_hide_is_on_local_edge
                 */
                static bool IsOnEdge(std::size_t local_edge_index, const LocalCoords& local_coords);

                /*!
                 * \copydoc doxygen_hide_is_on_local_face
                 */
                static bool IsOnFace(std::size_t local_face_index, const LocalCoords& local_coords);

                //! \copydoc doxygen_hide_transform_face_point
                static LocalCoords
                TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation);


                //! Return the local coordinates of the vertices.
                static const VerticeCoordListType& GetVertexLocalCoordsList();

              private:
                //! Whether a \a local_coords is inside the reference quadrangle or not.
                //! \param[in] local_coords \a LocalCoords which status related to the \a RefGeomElt is sought.
                static bool IsInside(const LocalCoords& local_coords);

                //! Return the list of edges.
                static const std::array<EdgeContent, Nedge>& GetEdgeList();

                //! Return the list of faces.
                static const std::array<FaceContent, Nface>& GetFaceList();

                /*!
                 * \brief Friendship to LocalData.
                 *
                 * As you can see above, list of local edges, faces and list of vertex coordinates are private
                 * members; to access them you must use the LocalData class.
                 *
                 * For instance:
                 *
                 * \code
                 * const auto& topology_face =
                 * RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index); \endcode
                 *
                 */
                template<class TopologyT>
                friend struct LocalData;
            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TOPOLOGY_x_QUADRANGLE_HPP_
