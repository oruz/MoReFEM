/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"

#include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment2.hpp"        // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment2.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"       // IWYU pragma: keep
// < absolutely required to let MoReFEM know which format are actually supported!

#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace Traits
        {


            /*!
             * \brief Traits class that holds the static functions related to shape functions, interface and topology.
             *
             * It can't be instantiated directly: its purpose is either to provide directly a data through
             * static function:
             *
             * \code
             * constexpr auto Nshape_function = Segment2::NshapeFunction();
             * \endcode
             *
             * or to be a template parameter to MoReFEM::RefGeomEltNS::Segment2 class (current class is in an
             * additional layer of namespace):
             *
             * \code
             * MoReFEM::RefGeomEltNS::Traits::Segment2
             * \endcode
             *
             */
            class Segment2 final : public ::MoReFEM::Internal::RefGeomEltNS::
                                       RefGeomEltImpl<Segment2, ShapeFunctionNS::Segment2, TopologyNS::Segment>
            {


              protected:
                //! Convenient alias.
                using self = Segment2;

                /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
                ///@{

                //! Constructor.
                Segment2() = delete;

                //! Destructor.
                ~Segment2() = delete;

                //! \copydoc doxygen_hide_copy_constructor
                Segment2(const Segment2& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Segment2(Segment2&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Segment2& operator=(const Segment2& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                self& operator=(self&& rhs) = delete;

                ///@}


              public:
                /*!
                 * \brief Name associated to the RefGeomElt.
                 *
                 * \return Name that is guaranteed to be unique (throught the GeometricEltFactory) and can
                 * therefore also act as an identifier.
                 */
                static const Advanced::GeomEltNS::GenericName& ClassName();

                //! Number of Coords required to describe fully a GeometricElt of this type.
                enum : std::size_t
                {
                    Ncoords = 2u
                };

                /*!
                 * \brief Enum associated to the RefGeomElt.
                 *
                 * \return Enum value guaranteed to be unique (throught the GeometricEltFactory); its
                 * raison d'être is that many operations are much faster on an enumeration than on a string.
                 */
                static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
                {
                    return MoReFEM::Advanced::GeometricEltEnum::Segment2;
                }


              private:
                // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!
            };


        } // namespace Traits


        /*!
         * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Segment2.
         *
         * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
         * store in one dynamic container all the kinds of GeometricElt present in a mesh.
         *
         * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
         * can be included in:
         *
         * \code
         * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
         * \endcode
         *
         */
        class Segment2 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Segment2>
        {
          public:
            //! Constructor.
            Segment2() = default;

            //! Destructor.
            virtual ~Segment2() override;

            //! \copydoc doxygen_hide_copy_constructor
            Segment2(const Segment2& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Segment2(Segment2&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Segment2& operator=(const Segment2& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Segment2& operator=(Segment2&& rhs) = delete;


          private:
            // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
        };


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_
