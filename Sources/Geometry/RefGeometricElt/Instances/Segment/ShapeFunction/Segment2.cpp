/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment2.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            const std::array<ShapeFunctionType, 2>& Segment2::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 2> ret{ { [](const auto& local_coords)
                                                               {
                                                                   return 0.5 * (1. - local_coords.r());
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return 0.5 * (1. + local_coords.r());
                                                               } } };

                return ret;
            };


            const std::array<ShapeFunctionType, 2>& Segment2::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 2> ret{ { Constant<-1, 2>(), Constant<1, 2>() } };

                return ret;
            };


            const std::array<ShapeFunctionType, 2>& Segment2::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 2> ret{ { Constant<0>(), Constant<0>() } };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
