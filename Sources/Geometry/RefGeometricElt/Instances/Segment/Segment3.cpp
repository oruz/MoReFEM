/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Segment/Segment3.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Segment3::~Segment3() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Segment3::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Segment3");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
