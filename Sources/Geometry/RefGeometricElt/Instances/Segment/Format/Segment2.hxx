/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Apr 2016 22:46:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_FORMAT_x_SEGMENT2_HXX_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_FORMAT_x_SEGMENT2_HXX_

// IWYU pragma: private, include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment2.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                inline constexpr GmfKwdCod
                Support<::MoReFEM::MeshNS::Format::Medit, Advanced::GeometricEltEnum::Segment2>::MeditId()
                {
                    return GmfEdges;
                }


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_FORMAT_x_SEGMENT2_HXX_
