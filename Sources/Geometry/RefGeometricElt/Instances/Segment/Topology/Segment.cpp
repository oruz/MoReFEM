/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            const std::string& Segment::ClassName()
            {
                static std::string ret("Segment");
                return ret;
            }


            /************************************************************************
             *   0-----------1         *
             *************************************************************************/


            const std::array<Segment::EdgeContent, Segment::Nedge>& Segment::GetEdgeList()
            {
                static std::array<Segment::EdgeContent, Segment::Nedge> ret{ { { { 0ul, 1u } } } };

                return ret;
            }


            const std::vector<Segment::FaceContent>& Segment::GetFaceList()
            {
                static std::vector<Segment::FaceContent> empty;
                return empty;
            }


            const std::vector<LocalCoords>& Segment::GetVertexLocalCoordsList()
            {
                static std::vector<LocalCoords> ret{ { -1. }, { 1. } };

                return ret;
            }


            bool Segment::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
            {
                return Internal::RefGeomEltNS::TopologyNS::IsOnVertex<Segment>(vertex_index, coords);
            }


            bool Segment::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
            {
                assert(edge_index == 0ul); // the only relevant value for a \a Segment...
                static_cast<void>(edge_index);

                static_assert(dimension == 1u);

                assert(coords.GetDimension() == dimension);
                const auto& vertex_coords = GetVertexLocalCoordsList();
                assert(vertex_coords.size() == 2ul);

                const double min = std::min(vertex_coords[0][0], vertex_coords[1][0]);
                const double max = std::max(vertex_coords[0][0], vertex_coords[1][0]);

                // Check whether the coordinates in between vertices.
                if (coords[0] < min || coords[0] > max)
                    return false;

                return true;
            }


            [[noreturn]] bool Segment::IsOnFace(std::size_t face_index, const LocalCoords& coords)
            {
                static_cast<void>(face_index);
                static_cast<void>(coords);
                assert(false);
                throw; // to avoid warning.
            }


            [[noreturn]] LocalCoords
            Segment::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
            {
                static_cast<void>(face_index);
                static_cast<void>(coords);
                static_cast<void>(orientation);

                assert(false && "Should never be called!");
                throw; // to avoid warning.
            }


            InterfaceNS::Nature Segment::GetInteriorInterface()
            {
                return InterfaceNS::Nature::edge;
            }


            Type Segment::GetType() noexcept
            {
                return Type::segment;
            }


            bool Segment::IsInside(const LocalCoords& coords)
            {
                const auto& vertex_coords_list = GetVertexLocalCoordsList();
                assert(vertex_coords_list.size() == 2ul);

                return coords.r() >= vertex_coords_list[0].r() && coords.r() <= vertex_coords_list[1].r();
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
