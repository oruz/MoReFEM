### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron10.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron4.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron10.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron4.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
