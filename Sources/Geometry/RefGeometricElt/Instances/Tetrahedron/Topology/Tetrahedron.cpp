/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            const std::string& Tetrahedron::ClassName()
            {
                static std::string ret("Tetrahedron");
                return ret;
            }


            /************************************************************************
             *
             *           3
             *          /.\
             *         / . \
             *        /  2  \
             *       / .  .  \
             *      /.      . \
             *     0-----------1
             *
             *************************************************************************/


            const std::array<Tetrahedron::EdgeContent, Tetrahedron::Nedge>& Tetrahedron::GetEdgeList()
            {
                static std::array<Tetrahedron::EdgeContent, Tetrahedron::Nedge> ret{ { { { 0ul, 1u } },
                                                                                       { { 1u, 2u } },
                                                                                       { { 0ul, 2u } },
                                                                                       { { 0ul, 3u } },
                                                                                       { { 1u, 3u } },
                                                                                       { { 2u, 3u } } } };

                return ret;
            }


            const std::array<Tetrahedron::FaceContent, Tetrahedron::Nface>& Tetrahedron::GetFaceList()
            {
                static std::array<Tetrahedron::FaceContent, Tetrahedron::Nface> ret{
                    { { { 0, 1, 2 } }, { { 0, 1, 3 } }, { { 0, 2, 3 } }, { { 1, 2, 3 } } }
                };

                return ret;
            }


            bool Tetrahedron::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
            {
                return Internal::RefGeomEltNS::TopologyNS::IsOnVertex<Tetrahedron>(vertex_index, coords);
            }


            bool Tetrahedron::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
            {
                if (!IsInside(coords))
                    return false;

                assert(edge_index < 6u);
                assert(coords.GetDimension() == 3u);
                const double r = coords.r();
                const double s = coords.s();
                const double t = coords.t();

                switch (edge_index)
                {
                case 0ul:
                    return NumericNS::IsZero(s) && NumericNS::IsZero(t);
                case 1u:
                    return NumericNS::AreEqual(1., r + s) && NumericNS::IsZero(t);
                case 2u:
                    return NumericNS::IsZero(r) && NumericNS::IsZero(t);
                case 3u:
                    return NumericNS::IsZero(r) && NumericNS::IsZero(s);
                case 4u:
                    return NumericNS::AreEqual(1., r + t) && NumericNS::IsZero(s);
                case 5u:
                    return NumericNS::AreEqual(1., s + t) && NumericNS::IsZero(r);
                }

                assert(false);
                return false;
            }


            bool Tetrahedron::IsOnFace(std::size_t face_index, const LocalCoords& coords)
            {
                if (!IsInside(coords))
                    return false;

                assert(face_index < 4u);
                assert(coords.GetDimension() == 3u);

                const double r = coords.r();
                const double s = coords.s();
                const double t = coords.t();

                switch (face_index)
                {
                case 0ul:
                    return NumericNS::IsZero(t);
                case 1u:
                    return NumericNS::IsZero(s);
                case 2u:
                    return NumericNS::IsZero(r);
                case 3u:
                    return NumericNS::AreEqual(1., r + s + t);
                }

                assert(false);
                return false;
            }


            LocalCoords
            Tetrahedron::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
            {
                assert(face_index < 4u);
                assert(IsOnFace(face_index, coords));
                assert(orientation < 6u);

                const double r = coords.r();
                const double s = coords.s();
                const double t = coords.t();


                // Create a dummy 2D LocalCoords that acts upon the face-triangle.
                std::vector<double> coords_2d_buf(2);

                switch (face_index)
                {
                case 0ul:
                    coords_2d_buf[0] = r;
                    coords_2d_buf[1] = s;
                    break;
                case 1u:
                    coords_2d_buf[0] = r;
                    coords_2d_buf[1] = t;
                    break;
                case 2u:
                case 3u:
                    coords_2d_buf[0] = s;
                    coords_2d_buf[1] = t;
                    break;
                }

                LocalCoords coords_2d_before_transformation(coords_2d_buf);

                // Use these 2D coords with the namesake method in Triangle.
                auto coords_2d_after_transformation =
                    Triangle::TransformFacePoint(coords_2d_before_transformation, 0ul, orientation);

                // Then report the 2D result into a 3D LocalCoords.
                const double r_2d = coords_2d_after_transformation[0];
                const double s_2d = coords_2d_after_transformation[1];

                switch (face_index)
                {
                case 0ul:
                    return LocalCoords({ r_2d, s_2d, 0. });
                case 1u:
                    return LocalCoords({ r_2d, 0., s_2d });
                case 2u:
                    return LocalCoords({ 0., s_2d, r_2d });
                case 3u:
                    return LocalCoords({ 1. - r_2d - s_2d, r_2d, s_2d });
                }

                assert(false);
                return coords_2d_after_transformation; // dummy return to avoid compilation error.
            }


            const std::vector<LocalCoords>& Tetrahedron::GetVertexLocalCoordsList()
            {
                static std::vector<LocalCoords> ret{ { 0., 0., 0. }, { 1., 0., 0. }, { 0., 1., 0. }, { 0., 0., 1. } };

                return ret;
            }


            bool Tetrahedron::IsInside(const LocalCoords& coords)
            {
                assert(coords.GetDimension() == 3u);

                const double r = coords.r();
                const double s = coords.s();
                const double t = coords.t();

                if (r < 0. || s < 0. || t < 0.)
                    return false;

                if (r + s + t <= 1.)
                    return true;

                return false;
            }


            InterfaceNS::Nature Tetrahedron::GetInteriorInterface()
            {
                return InterfaceNS::Nature::volume;
            }


            Type Tetrahedron::GetType() noexcept
            {
                return Type::tetrahedron;
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
