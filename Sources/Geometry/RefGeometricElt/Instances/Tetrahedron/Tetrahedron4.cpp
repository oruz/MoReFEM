/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron4.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Tetrahedron4::~Tetrahedron4() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Tetrahedron4::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Tetrahedron4");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
