/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Format/Tetrahedron10.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                const Advanced::GeomEltNS::EnsightName&
                Support<::MoReFEM::MeshNS::Format::Ensight, Advanced::GeometricEltEnum::Tetrahedron10>::EnsightName()
                {
                    static Advanced::GeomEltNS::EnsightName ret("tetra10");
                    return ret;
                };


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
