/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron4.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            const std::array<ShapeFunctionType, 4>& Tetrahedron4::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 4> ret{ { [](const auto& local_coords)
                                                               {
                                                                   return 1. - local_coords.r() - local_coords.s()
                                                                          - local_coords.t();
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return local_coords.r();
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return local_coords.s();
                                                               },
                                                               [](const auto& local_coords)
                                                               {
                                                                   return local_coords.t();
                                                               } } };

                return ret;
            };


            const std::array<ShapeFunctionType, 12>& Tetrahedron4::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 12> ret{ { Constant<-1>(),
                                                                Constant<-1>(),
                                                                Constant<-1>(),
                                                                Constant<1>(),
                                                                Constant<0>(),
                                                                Constant<0>(),
                                                                Constant<0>(),
                                                                Constant<1>(),
                                                                Constant<0>(),
                                                                Constant<0>(),
                                                                Constant<0>(),
                                                                Constant<1>() } };

                return ret;
            };


            const std::array<ShapeFunctionType, 36>& Tetrahedron4::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 36> ret{
                    { Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
                      Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
                      Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
                      Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
                      Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
                      Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>() }
                };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
