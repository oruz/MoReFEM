/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Mar 2014 15:54:30 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron10.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            const std::array<ShapeFunctionType, 10>& Tetrahedron10::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 10> ret{
                    { [](const auto& local_coords)
                      {
                          return -(1. - local_coords.r() - local_coords.s() - local_coords.t())
                                 * (1. - 2. * (1. - local_coords.r() - local_coords.s() - local_coords.t()));
                      },
                      [](const auto& local_coords)
                      {
                          return -local_coords.r() * (1. - 2. * local_coords.r());
                      },
                      [](const auto& local_coords)
                      {
                          return -local_coords.s() * (1. - 2. * local_coords.s());
                      },
                      [](const auto& local_coords)
                      {
                          return -local_coords.t() * (1. - 2. * local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.r() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.r() * local_coords.s();
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.s() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.t() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.r() * local_coords.t();
                      },
                      [](const auto& local_coords)
                      {
                          return 4. * local_coords.s() * local_coords.t();
                      } }
                };

                return ret;
            };


            const std::array<ShapeFunctionType, 30>& Tetrahedron10::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 30> ret{ {
                    [](const auto& local_coords)
                    {
                        return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
                    },

                    [](const auto& local_coords)
                    {
                        return -1. + 4. * local_coords.r();
                    },
                    Constant<0>(),
                    Constant<0>(),

                    Constant<0>(),
                    [](const auto& local_coords)
                    {
                        return -1. + 4. * local_coords.s();
                    },
                    Constant<0>(),

                    Constant<0>(),
                    Constant<0>(),
                    [](const auto& local_coords)
                    {
                        return -1. + 4. * local_coords.t();
                    },

                    [](const auto& local_coords)
                    {
                        return 4. - 8. * local_coords.r() - 4. * local_coords.s() - 4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.r();
                    },
                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.r();
                    },

                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.s();
                    },
                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.r();
                    },
                    Constant<0>(),

                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.s();
                    },
                    [](const auto& local_coords)
                    {
                        return 4. - 4. * local_coords.r() - 8. * local_coords.s() - 4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.s();
                    },

                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return -4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return 4. - 4. * local_coords.r() - 4. * local_coords.s() - 8. * local_coords.t();
                    },

                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.t();
                    },
                    Constant<0>(),
                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.r();
                    },

                    Constant<0>(),
                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.t();
                    },
                    [](const auto& local_coords)
                    {
                        return 4. * local_coords.s();
                    },
                } };

                return ret;
            };


            const std::array<ShapeFunctionType, 90>& Tetrahedron10::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 90> ret{ {
                    Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),
                    Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<0>(),  Constant<0>(),
                    Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
                    Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
                    Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
                    Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),
                    Constant<-8>(), Constant<-4>(), Constant<-4>(), Constant<-4>(), Constant<0>(),  Constant<0>(),
                    Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
                    Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
                    Constant<0>(),  Constant<-4>(), Constant<0>(),  Constant<-4>(), Constant<-8>(), Constant<-4>(),
                    Constant<0>(),  Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<-4>(),
                    Constant<0>(),  Constant<0>(),  Constant<-4>(), Constant<-4>(), Constant<-4>(), Constant<-8>(),
                    Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
                    Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
                    Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
                } };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
