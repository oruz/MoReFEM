/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_TOPOLOGY_x_POINT_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_TOPOLOGY_x_POINT_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/TopologyCommon.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief Topology of a point.
             */
            class Point final
            {

              public:
                //! Data.
                enum : std::size_t
                {
                    dimension = 0ul,
                    Nvertex = 1u,
                    Nedge = 0ul,
                    Nface = 0ul,
                    Nvolume = 0ul,
                };


                //! Topology of an edge (irrelevant here...).
                using EdgeTopology = std::false_type;

                //! Topology of a face (irrelevant here...).
                using FaceTopology = std::false_type;

                //! Container used to store all the points of an edge. The index of the points are actually stored.
                using EdgeContent = std::false_type;

                //! Container used to store all the points of a face. The index of the points are actually stored.
                using FaceContent = std::false_type;

                //! Container used to store the local coordinates of the vertices.
                using VerticeCoordListType = std::vector<LocalCoords>;

                /*!
                 * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
                 * stored.
                 */
                static InterfaceNS::Nature GetInteriorInterface();


                //! Returns the enum that tags the topology.
                static Type GetType() noexcept;


              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit Point() = default;

              protected:
                //! Destructor.
                ~Point() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Point(const Point& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Point(Point&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Point& operator=(const Point& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Point& operator=(Point&& rhs) = delete;

                ///@}

              public:
                //! Return the name of the topology.
                static const std::string& ClassName();

                /*!
                 * \copydoc doxygen_hide_is_on_local_vertex
                 */
                static bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);

                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                // Just there to make the code compile; should never be called!
                [[noreturn]] static bool IsOnEdge(std::size_t, const LocalCoords&);

                // Just there to make the code compile; should never be called!
                [[noreturn]] static bool IsOnFace(std::size_t, const LocalCoords&);

                // Just there to make the code compile; should never be called!
                [[noreturn]] static LocalCoords
                TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation);

                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                //! Return the local coordinates of the vertices.
                static const VerticeCoordListType& GetVertexLocalCoordsList();

              private:
                //! Whether a \a local_coords is the coords of the point.
                //! \param[in] local_coords \a LocalCoords which status related to the \a RefGeomElt is sought.
                static bool IsInside(const LocalCoords& local_coords);

                //! Return the list of edges (irrelevant here...)
                static const std::vector<EdgeContent>& GetEdgeList();

                //! Return the list of faces (irrelevant here...)
                static const std::vector<FaceContent>& GetFaceList();

                /*!
                 * \brief Friendship to LocalData.
                 *
                 * As you can see above, list of local edges, faces and list of vertex coordinates are private
                 * members; to access them you must use the LocalData class.
                 *
                 * For instance:
                 *
                 * \code
                 * const auto& topology_face =
                 * RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index); \endcode
                 *
                 */
                template<class TopologyT>
                friend struct LocalData;
            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_TOPOLOGY_x_POINT_HPP_
