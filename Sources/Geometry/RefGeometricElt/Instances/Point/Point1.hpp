/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"

#include "Geometry/RefGeometricElt/Instances/Point/Format/Point1.hpp"        // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"       // IWYU pragma: keep
// < absolutely required to let MoReFEM know which format are actually supported!

#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"    // IWYU pragma: export


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace Traits
        {


            /*!
             * \brief Traits class that holds the static functions related to shape functions, interface and topology.
             *
             * It can't be instantiated directly: its purpose is either to provide directly a data through
             * static function:
             *
             * \code
             * constexpr auto Nshape_function = Point1::NshapeFunction();
             * \endcode
             *
             * or to be a template parameter to MoReFEM::RefGeomEltNS::Point1 class (current class is in an
             * additional layer of namespace):
             *
             * \code
             * MoReFEM::RefGeomEltNS::Traits::Point1
             * \endcode
             *
             */
            class Point1 final : public ::MoReFEM::Internal::RefGeomEltNS::
                                     RefGeomEltImpl<Point1, ShapeFunctionNS::Point1, TopologyNS::Point>
            {

              protected:
                //! Convenient alias.
                using self = Point1;

                /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
                ///@{

                //! Constructor.
                Point1() = delete;

                //! Destructor.
                ~Point1() = delete;

                //! \copydoc doxygen_hide_copy_constructor
                Point1(const Point1& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Point1(Point1&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Point1& operator=(const Point1& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                self& operator=(self&& rhs) = delete;
                ///@}


              public:
                /*!
                 * \brief Name associated to the RefGeomElt.
                 *
                 * \return Name that is guaranteed to be unique (throught the GeometricEltFactory) and can
                 * therefore also act as an identifier.
                 */
                static const Advanced::GeomEltNS::GenericName& ClassName();

                //! Number of Coords required to describe fully a GeometricElt of this type.
                enum : std::size_t
                {
                    Ncoords = 1u
                };

                /*!
                 * \brief Enum associated to the RefGeomElt.
                 *
                 * \return Enum value guaranteed to be unique (throught the GeometricEltFactory); its
                 * raison d'être is that many operations are much faster on an enumeration than on a string.
                 */
                static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
                {
                    return MoReFEM::Advanced::GeometricEltEnum::Point1;
                }


              private:
                // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!
            };


        } // namespace Traits


        /*!
         * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Point1.
         *
         * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
         * store in one dynamic container all the kinds of GeometricElt present in a mesh.
         *
         * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
         * can be included in:
         *
         * \code
         * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
         * \endcode
         *
         */
        class Point1 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Point1>
        {
          public:
            //! Constructor.
            Point1() = default;

            //! Destructor.
            virtual ~Point1() override;

            //! \copydoc doxygen_hide_copy_constructor
            Point1(const Point1& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Point1(Point1&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Point1& operator=(const Point1& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Point1& operator=(Point1&& rhs) = delete;


          private:
            // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
        };


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
