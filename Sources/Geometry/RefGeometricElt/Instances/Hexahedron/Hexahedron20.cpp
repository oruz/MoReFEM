/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron20.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Hexahedron20::~Hexahedron20() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Hexahedron20::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Hexahedron20");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
