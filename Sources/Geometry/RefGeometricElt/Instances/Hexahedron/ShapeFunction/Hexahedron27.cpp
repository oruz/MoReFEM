/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Mar 2014 10:16:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <array>
#include <cassert>
#include <functional>
#include <vector>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron27.hpp"
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            namespace // anonymous
            {


                //! Calculates the functions for _phi
                template<int I>
                double ShapeFunctionHelper(const LocalCoords& local_coords);


                enum DerivativeComponent
                {
                    R,
                    S,
                    T
                };


                //! Calculates the functions for _FirstDerivateBasisFunction
                template<DerivativeComponent ComponentT, int I>
                double FirstDerivativeHelper(const LocalCoords& local_coords);


                //! Calculates the functions for _SecondDerivateBasisFunction
                template<DerivativeComponent FirstComponentT, DerivativeComponent SecondComponentT, int I>
                double SecondDerivativeHelper(const LocalCoords& local_coords);


                using Topology = TopologyNS::Hexahedron;


            } // namespace


            const std::array<ShapeFunctionType, 27>& Hexahedron27::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 27> ret{ { [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<0>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<1>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<2>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<3>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<4>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<5>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<6>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<7>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<8>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<9>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<10>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<11>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<12>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<13>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<14>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<15>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<16>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<17>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<18>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<19>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<20>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<21>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<22>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<23>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<24>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<25>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return ShapeFunctionHelper<26>(local_coords);
                                                                } } };

                return ret;
            };


            const std::array<ShapeFunctionType, 81>& Hexahedron27::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 81> ret{ { [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 0>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 0>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 0>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 1>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 1>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 1>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 2>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 2>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 2>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 3>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 3>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 3>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 4>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 4>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 4>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 5>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 5>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 5>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 6>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 6>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 6>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 7>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 7>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 7>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 8>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 8>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 8>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 9>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 9>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 9>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 10>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 10>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 10>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 11>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 11>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 11>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 12>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 12>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 12>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 13>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 13>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 13>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 14>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 14>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 14>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 15>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 15>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 15>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 16>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 16>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 16>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 17>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 17>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 17>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 18>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 18>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 18>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 19>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 19>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 19>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 20>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 20>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 20>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 21>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 21>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 21>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 22>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 22>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 22>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 23>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 23>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 23>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 24>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 24>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 24>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 25>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 25>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 25>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<R, 26>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<S, 26>(local_coords);
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return FirstDerivativeHelper<T, 26>(local_coords);
                                                                } } };

                return ret;
            };


            const std::array<ShapeFunctionType, 243>& Hexahedron27::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 243> ret{
                    { [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 0>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 1>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 2>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 3>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 4>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 5>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 6>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 7>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 8>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 9>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 10>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 11>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 12>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 13>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 14>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 15>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 16>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 17>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 18>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 19>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 20>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 21>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 22>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 23>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 24>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 25>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, R, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, S, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<R, T, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, R, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, S, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<S, T, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, R, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, S, 26>(local_coords);
                      },
                      [](const auto& local_coords)
                      {
                          return SecondDerivativeHelper<T, T, 26>(local_coords);
                      } }
                };

                return ret;
            };


            namespace // anonymous
            {


                //! Calculates the functions for _phi
                template<int I>
                double ShapeFunctionHelper(const LocalCoords& local_coords)
                {
                    static_assert(I < 27, "Helper function called with an invalid template parameter!");
                    const auto& reference_coor = Topology::GetVertexLocalCoordsList();
                    assert(reference_coor.size() == 27);

                    const auto r = local_coords.r();
                    const auto s = local_coords.s();
                    const auto t = local_coords.t();
                    const auto& ref = reference_coor[I];

                    if (I < 8)
                        return 0.125 * (1. + ref.r() * r) * ref.r() * r * (1. + ref.s() * s) * ref.s() * s
                               * (1. + ref.t() * t) * ref.t() * t;
                    else if (I == 8 || I == 10 || I == 16 || I == 18)
                        return .25 * (1. - r * r) * (1. + ref.s() * s) * ref.s() * s * (1. + ref.t() * t) * ref.t() * t;
                    else if (I == 9 || I == 11 || I == 17 || I == 19)
                        return .25 * (1. + r * ref.r()) * r * ref.r() * (1. - s * s) * (1. + ref.t() * t) * ref.t() * t;
                    else if (I == 12 || I == 13 || I == 14 || I == 15)
                        return .25 * (1. + r * ref.r()) * r * ref.r() * (1. + ref.s() * s) * ref.s() * s * (1. - t * t);
                    else if (I == 20 || I == 23)
                        return .5 * (1. - r * r) * (1. - s * s) * ref.t() * t * (1. + ref.t() * t);
                    else if (I == 21 || I == 24)
                        return .5 * (1. + r * ref.r()) * ref.r() * r * (1. - s * s) * (1. - t * t);
                    else if (I == 22 || I == 25)
                        return .5 * (1. - r * r) * ref.s() * s * (1. + ref.s() * s) * (1. - t * t);
                    else if (I == 26)
                        return (1. - r * r) * (1. - s * s) * (1. - t * t);
                }


                template<DerivativeComponent ComponentT, int I>
                double FirstDerivativeHelper(const LocalCoords& local_coords)
                {
                    static_assert(I < 27, "Helper function called with an invalid template parameter!");
                    const auto& reference_coor = Topology::GetVertexLocalCoordsList();
                    assert(reference_coor.size() == 27);

                    const auto r = local_coords.r();
                    const auto s = local_coords.s();
                    const auto t = local_coords.t();
                    const auto& ref = reference_coor[I];

                    switch (ComponentT)
                    {
                    case R:
                    {
                        if (I < 8)
                            return 0.125 * ref.r() * (1. + 2. * ref.r() * r) * (1. + ref.s() * s) * ref.s() * s
                                   * (1. + ref.t() * t) * ref.t() * t;
                        else if (I == 8 || I == 10 || I == 16 || I == 18)
                            return -.5 * r * (1. + ref.s() * s) * ref.s() * s * (1. + ref.t() * t) * ref.t() * t;
                        else if (I == 9 || I == 11 || I == 17 || I == 19)
                            return .25 * (1. + 2. * r * ref.r()) * ref.r() * (1. - s * s) * (1. + ref.t() * t) * ref.t()
                                   * t;
                        else if (I == 12 || I == 13 || I == 14 || I == 15)
                            return .25 * (1. + 2. * r * ref.r()) * ref.r() * (1. + ref.s() * s) * ref.s() * s
                                   * (1. - t * t);
                        else if (I == 20 || I == 23)
                            return -r * (1. - s * s) * ref.t() * t * (1. + ref.t() * t);
                        else if (I == 21 || I == 24)
                            return .5 * (1. + 2. * r * ref.r()) * ref.r() * (1. - s * s) * (1. - t * t);
                        else if (I == 22 || I == 25)
                            return -r * ref.s() * s * (1. + ref.s() * s) * (1. - t * t);
                        else if (I == 26)
                            return -2 * r * (1. - s * s) * (1. - t * t);

                        break;
                    }
                    case S:
                    {
                        if (I < 8)
                            return 0.125 * (1. + ref.r() * r) * ref.r() * r * (1. + 2. * ref.s() * s) * ref.s()
                                   * (1. + ref.t() * t) * ref.t() * t;
                        else if (I == 8 || I == 10 || I == 16 || I == 18)
                            return .25 * (1. - r * r) * (1. + 2. * ref.s() * s) * ref.s() * (1. + ref.t() * t) * ref.t()
                                   * t;
                        else if (I == 9 || I == 11 || I == 17 || I == 19)
                            return -.5 * s * (1. + r * ref.r()) * r * ref.r() * (1. + ref.t() * t) * ref.t() * t;
                        else if (I == 12 || I == 13 || I == 14 || I == 15)
                            return .25 * (1. + r * ref.r()) * r * ref.r() * (1. + 2. * ref.s() * s) * ref.s()
                                   * (1. - t * t);
                        else if (I == 20 || I == 23)
                            return -s * (1. - r * r) * ref.t() * t * (1. + ref.t() * t);
                        else if (I == 21 || I == 24)
                            return -s * (1. + r * ref.r()) * ref.r() * r * (1. - t * t);
                        else if (I == 22 || I == 25)
                            return .5 * (1. - r * r) * ref.s() * (1. + 2. * ref.s() * s) * (1. - t * t);
                        else if (I == 26)
                            return -2 * s * (1. - r * r) * (1. - t * t);

                        break;
                    }
                    case T:
                    {
                        if (I < 8)
                            return 0.125 * (1. + ref.r() * r) * ref.r() * r * (1. + ref.s() * s) * ref.s() * s
                                   * (1. + 2. * ref.t() * t) * ref.t();
                        else if (I == 8 || I == 10 || I == 16 || I == 18)
                            return .25 * (1. - r * r) * (1. + ref.s() * s) * ref.s() * s * (1. + 2. * ref.t() * t)
                                   * ref.t();
                        else if (I == 9 || I == 11 || I == 17 || I == 19)
                            return .25 * (1. + r * ref.r()) * r * ref.r() * (1. - s * s) * (1. + 2. * ref.t() * t)
                                   * ref.t();
                        else if (I == 12 || I == 13 || I == 14 || I == 15)
                            return -.5 * t * (1. + r * ref.r()) * r * ref.r() * (1. + ref.s() * s) * ref.s() * s;
                        else if (I == 20 || I == 23)
                            return .5 * (1. - r * r) * (1. - s * s) * ref.t() * (1. + 2. * ref.t() * t);
                        else if (I == 21 || I == 24)
                            return -t * (1. + r * ref.r()) * ref.r() * r * (1. - s * s);
                        else if (I == 22 || I == 25)
                            return -t * (1. - r * r) * ref.s() * s * (1. + ref.s() * s);
                        else if (I == 26)
                            return -2. * t * (1. - r * r) * (1. - s * s);

                        break;
                    }
                    } // switch
                }


                //! Calculates the functions for _SecondDerivateBasisFunction
                template<DerivativeComponent FirstComponentT, DerivativeComponent SecondComponentT, int I>
                double SecondDerivativeHelper(const LocalCoords& local_coords)
                {
                    static_assert(I < 27, "Helper function called with an invalid template parameter!");
                    const auto& reference_coor = Topology::GetVertexLocalCoordsList();
                    assert(reference_coor.size() == 27);

                    const auto r = local_coords.r();
                    const auto s = local_coords.s();
                    const auto t = local_coords.t();
                    const auto& ref = reference_coor[I];

                    switch (FirstComponentT)
                    {
                    case R:
                    {
                        switch (SecondComponentT)
                        {
                        case R:
                        {
                            if (I < 8)
                                return 0.25 * ref.r() * ref.r() * ref.s() * ref.t() * s * t * (1. + ref.s() * s)
                                       * (1. + ref.t() * t);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return -0.5 * ref.s() * ref.t() * s * t * (1. + ref.s() * s) * (1. + ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return 0.5 * ref.r() * ref.r() * ref.t() * t * (1. - s * s) * (1. + ref.t() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return 0.5 * ref.r() * ref.r() * ref.s() * (1. + ref.s() * s) * (1. - t * t);
                            else if (I == 20 || I == 23)
                                return -ref.t() * t * (1. - s * s) * (1. + ref.t() * t);
                            else if (I == 21 || I == 24)
                                return ref.r() * ref.r() * (1. - s * s) * (1. - t * t);
                            else if (I == 22 || I == 25)
                                return -ref.s() * s * (1. + ref.s() * s) * (1. - t * t);
                            else if (I == 26)
                                return -2. * (1. - s * s) * (1. - t * t);

                            break;
                        }
                        case S:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * t * (1. + 2. * ref.r() * r)
                                       * (1. + ref.t() * t) * (1. + 2. * ref.s() * s);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return -0.5 * ref.s() * ref.t() * r * (1. + ref.t() * t) * (1. + 2. * ref.s() * s);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return -0.5 * ref.r() * ref.t() * s * t * (1. + 2. * ref.r() * r) * (1. + ref.r() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return 0.25 * ref.r() * ref.s() * (1. + 2. * ref.r() * r) * (1. - t * t)
                                       * (1. + 2. * ref.s() * s);
                            else if (I == 20 || I == 23)
                                return 2. * ref.t() * r * s * t * (1. + ref.t() * t);
                            else if (I == 21 || I == 24)
                                return -ref.r() * s * (1. + 2. * ref.r() * r) * (1. - t * t);
                            else if (I == 22 || I == 25)
                                return -ref.s() * r * (1. - t * t) * (1. + 2. * ref.s() * s);
                            else if (I == 26)
                                return 4. * r * s * (1. - t * t);

                            break;
                        }
                        case T:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * s * (1. + 2. * ref.r() * r)
                                       * (1. + ref.s() * s) * (1. + 2. * ref.t() * t);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return -0.5 * ref.s() * ref.t() * r * s * (1. + ref.s() * s) * (1. + 2. * ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return 0.25 * ref.r() * ref.t() * (1. + 2. * ref.r() * r) * (1. - s * s)
                                       * (1. + 2. * ref.t() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return -0.5 * ref.r() * ref.s() * t * s * (1. + 2. * ref.r() * r) * (1. + ref.s() * s);
                            else if (I == 20 || I == 23)
                                return -ref.t() * r * (1. - s * s) * (1. + 2. * ref.t() * t);
                            else if (I == 21 || I == 24)
                                return -ref.r() * t * (1. + 2. * ref.r() * r) * (1. - s * s);
                            else if (I == 22 || I == 25)
                                return 2. * ref.s() * r * s * t * (1. + ref.s() * s);
                            else if (I == 26)
                                return 4. * r * t * (1. - s * s);

                            break;
                        }
                        } // switch SecondComponentT

                        break;
                    }
                    case S:
                    {
                        switch (SecondComponentT)
                        {
                        case R:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * t * (1. + 2. * ref.s() * s)
                                       * (1. + ref.t() * t) * (1. + 2. * ref.r() * r);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return -0.5 * ref.s() * ref.t() * t * (1. + 2. * ref.s() * s) * (1. + ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return -0.5 * ref.r() * ref.t() * s * t * (1. + ref.t() * t) * (1. + 2. * ref.r() * r);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return 0.25 * ref.r() * ref.s() * (1. + 2. * ref.s() * s) * (1. - t * t)
                                       * (1. + 2. * ref.r() * r);
                            else if (I == 20 || I == 23)
                                return 2. * ref.t() * r * s * t * (1. + ref.t() * t);
                            else if (I == 21 || I == 24)
                                return -ref.r() * s * (1. - t * t) * (1. + 2. * ref.r() * r);
                            else if (I == 22 || I == 25)
                                return -ref.s() * r * (1. + 2. * ref.s() * s) * (1. - t * t);
                            else if (I == 26)
                                return 4. * r * s * (1. - t * t);

                            break;
                        }
                        case S:
                        {
                            if (I < 8)
                                return 0.25 * ref.r() * ref.s() * ref.s() * r * t * (1. + ref.r() * r)
                                       * (1. + ref.t() * t);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return 0.5 * ref.s() * ref.s() * ref.t() * s * t * (1. - r * r) * (1. + ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return -0.5 * ref.r() * ref.t() * r * t * (1. + ref.r() * r) * (1. + ref.t() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return 0.5 * ref.r() * ref.s() * ref.s() * r * (1. + ref.r() * r) * (1. - t * t);
                            else if (I == 20 || I == 23)
                                return -ref.t() * t * (1. - r * r) * (1. + ref.t() * t);
                            else if (I == 21 || I == 24)
                                return -ref.r() * r * (1. + ref.r() * r) * (1. - t * t);
                            else if (I == 22 || I == 25)
                                return ref.s() * ref.s() * (1. - r * r) * (1. - t * t);
                            else if (I == 26)
                                return -2. * (1. - r * r) * (1. - t * t);

                            break;
                        }
                        case T:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * r * (1. + ref.r() * r)
                                       * (1. + 2. * ref.s() * s) * (1. + 2. * ref.t() * t);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return 0.25 * ref.s() * ref.t() * (1. - r * r) * (1. + 2. * ref.s() * s)
                                       * (1. + 2. * ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return -0.5 * ref.r() * ref.t() * r * s * (1. + ref.r() * r) * (1. + 2. * ref.t() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return -0.5 * ref.r() * ref.s() * r * t * (1. + ref.r() * r) * (1. + 2. * ref.s() * s);
                            else if (I == 20 || I == 23)
                                return -ref.t() * s * (1. - r * r) * (1. + 2. * ref.t() * t);
                            else if (I == 21 || I == 24)
                                return 2. * ref.r() * r * s * t * (1. + ref.r() * r);
                            else if (I == 22 || I == 25)
                                return -ref.s() * t * (1. - r * r) * (1. + 2. * ref.s() * s);
                            else if (I == 26)
                                return 4. * s * t * (1. - r * r);

                            break;
                        }
                        } // switch SecondComponentT

                        break;
                    }
                    case T:
                    {
                        switch (SecondComponentT)
                        {
                        case R:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * s * (1. + ref.s() * s) * (1. + ref.t() * t)
                                       * (1. + 2. * ref.r() * r);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return -0.25 * ref.s() * ref.t() * r * s * (1. + ref.s() * s) * (1. + 2. * ref.t() * t);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return 0.25 * ref.r() * ref.t() * (1. - s * s) * (1. + 2. * ref.t() * t)
                                       * (1. + 2. * ref.r() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return -0.5 * ref.r() * ref.s() * s * t * (1. + ref.s() * s) * (1. + 2. * ref.r() * r);
                            else if (I == 20 || I == 23)
                                return -ref.t() * r * (1. - s * s) * (1. + 2. * ref.t() * t);
                            else if (I == 21 || I == 24)
                                return -ref.r() * t * (1. - s * s) * (1. + 2. * ref.r() * r);
                            else if (I == 22 || I == 25)
                                return 2. * ref.s() * r * s * t * (1. + ref.s() * s);
                            else if (I == 26)
                                return 4. * r * t * (1. - s * s);

                            break;
                        }
                        case S:
                        {
                            if (I < 8)
                                return 0.125 * ref.r() * ref.s() * ref.t() * r * (1. + ref.r() * r)
                                       * (1. + 2. * ref.t() * t) * (1. + 2. * ref.s() * s);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return 0.125 * ref.s() * ref.t() * (1. - r * r) * (1. + 2. * ref.t() * t)
                                       * (1. + 2. * ref.s() * s);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return -0.5 * ref.r() * ref.t() * r * s * (1. + ref.r() * r) * (1. + 2. * ref.t() * t);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return -0.5 * ref.r() * ref.s() * r * t * (1. + ref.r() * r) * (1. + 2. * ref.s() * s);
                            else if (I == 20 || I == 23)
                                return -ref.t() * s * (1. - r * r) * (1. + 2. * ref.t() * t);
                            else if (I == 21 || I == 24)
                                return 2. * ref.r() * r * s * t * (1. + ref.r() * r);
                            else if (I == 22 || I == 25)
                                return -ref.s() * t * (1. - r * r) * (1. + 2. * ref.s() * s);
                            else if (I == 26)
                                return 4. * s * t * (1. - r * r);

                            break;
                        }
                        case T:
                        {
                            if (I < 8)
                                return 0.25 * ref.r() * ref.s() * ref.t() * ref.t() * r * s * (1. + ref.r() * r)
                                       * (1. + ref.s() * s);
                            else if (I == 8 || I == 10 || I == 16 || I == 18)
                                return 0.25 * ref.s() * ref.t() * ref.t() * s * t * (1. - r * r) * (1. + ref.s() * s);
                            else if (I == 9 || I == 11 || I == 17 || I == 19)
                                return 0.5 * ref.r() * ref.t() * ref.t() * r * (1. + ref.r() * r) * (1. - s * s);
                            else if (I == 12 || I == 13 || I == 14 || I == 15)
                                return -0.5 * ref.r() * ref.s() * r * s * (1. + ref.r() * r) * (1. + ref.s() * s);
                            else if (I == 20 || I == 23)
                                return -2. * ref.t() * ref.t() * r * (1. - s * s);
                            else if (I == 21 || I == 24)
                                return -ref.r() * r * (1. + ref.r() * r) * (1. - s * s);
                            else if (I == 22 || I == 25)
                                return -ref.s() * s * (1. - r * r) * (1. + ref.s() * s);
                            else if (I == 26)
                                return -2. * (1. - r * r) * (1. - s * s);

                            break;
                        }
                        } // switch SecondComponentT

                        break;
                    }

                    } // switch FirstComponentT


                } // SecondDerivativeHelper()


            } // namespace


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
