/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Mar 2014 09:14:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron8.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            const std::array<ShapeFunctionType, 8>& Hexahedron8::ShapeFunctionList()
            {


                static std::array<ShapeFunctionType, 8> ret{
                    { [](const auto& local_coords)
                      {
                          return 0.125 * (1. - local_coords.r()) * (1. - local_coords.s()) * (1. - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. + local_coords.r()) * (1. - local_coords.s()) * (1. - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. + local_coords.r()) * (1. + local_coords.s()) * (1. - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. - local_coords.r()) * (1. + local_coords.s()) * (1. - local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. - local_coords.r()) * (1. - local_coords.s()) * (1. + local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. + local_coords.r()) * (1. - local_coords.s()) * (1. + local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. + local_coords.r()) * (1. + local_coords.s()) * (1. + local_coords.t());
                      },
                      [](const auto& local_coords)
                      {
                          return 0.125 * (1. - local_coords.r()) * (1. + local_coords.s()) * (1. + local_coords.t());
                      } }
                };

                return ret;
            };


            const std::array<ShapeFunctionType, 24>& Hexahedron8::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 24> ret{ {
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.s()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.r()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.r()) * (1. - local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.s()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.r()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.r()) * (1. - local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.s()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.r()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.r()) * (1. + local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.s()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.r()) * (1. - local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.r()) * (1. + local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.s()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. - local_coords.r()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.r()) * (1. - local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.s()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.r()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.r()) * (1. - local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.s()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.r()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. + local_coords.r()) * (1. + local_coords.s());
                    },

                    [](const auto& local_coords)
                    {
                        return -0.125 * (1. + local_coords.s()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.r()) * (1. + local_coords.t());
                    },
                    [](const auto& local_coords)
                    {
                        return 0.125 * (1. - local_coords.r()) * (1. + local_coords.s());
                    },
                } };

                return ret;
            };


            const std::array<ShapeFunctionType, 72>& Hexahedron8::SecondDerivateShapeFunctionList()
            {

                static std::array<ShapeFunctionType, 72> ret{ { Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. - local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.t());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. + local_coords.r());
                                                                },
                                                                Constant<0>(),

                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.t());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.s());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.t());
                                                                },
                                                                Constant<0>(),
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.r());
                                                                },

                                                                [](const auto& local_coords)
                                                                {
                                                                    return -0.125 * (1. + local_coords.s());
                                                                },
                                                                [](const auto& local_coords)
                                                                {
                                                                    return 0.125 * (1. - local_coords.r());
                                                                },
                                                                Constant<0>() } };

                return ret;
            };


        } //  namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
