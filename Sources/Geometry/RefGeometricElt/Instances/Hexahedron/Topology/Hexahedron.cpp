/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            namespace // anonymous
            {

                std::pair<Advanced::ComponentNS::index_type, double> AnalyseFace(std::size_t face_index);

                LocalCoords Extract2DCoordinates(Advanced::ComponentNS::index_type face_constant_component,
                                                 const LocalCoords& coords);


            } // namespace


            const std::string& Hexahedron::ClassName()
            {
                static std::string ret("Hexahedron");
                return ret;
            }


            /************************************************************************
             *
             *         7--------6
             *        /.       /|
             *       / .      / |
             *      4________5  |
             *   	|  .     |  |
             *   	|  3.....|..2
             *   	| .      | /
             *   	|.       |/
             *   	0________1
             *
             *
             *************************************************************************/


            const std::array<Hexahedron::EdgeContent, Hexahedron::Nedge>& Hexahedron::GetEdgeList()
            {
                static std::array<Hexahedron::EdgeContent, Hexahedron::Nedge> ret{ { { { 0ul, 1u } },
                                                                                     { { 1u, 2u } },
                                                                                     { { 3u, 2u } },
                                                                                     { { 0ul, 3u } },
                                                                                     { { 0ul, 4u } },
                                                                                     { { 1u, 5u } },
                                                                                     { { 2u, 6u } },
                                                                                     { { 3u, 7u } },
                                                                                     { { 4u, 5u } },
                                                                                     { { 5u, 6u } },
                                                                                     { { 7u, 6u } },
                                                                                     { { 4u, 7u } }

                } };

                return ret;
            }


            const std::array<Hexahedron::FaceContent, Hexahedron::Nface>& Hexahedron::GetFaceList()
            {
                static std::array<Hexahedron::FaceContent, Hexahedron::Nface> ret{ { { { 0ul, 1u, 2u, 3u } },
                                                                                     { { 0ul, 1u, 5u, 4u } },
                                                                                     { { 0ul, 3u, 7u, 4u } },
                                                                                     { { 1u, 2u, 6u, 5u } },
                                                                                     { { 3u, 2u, 6u, 7u } },
                                                                                     { { 4u, 5u, 6u, 7u } } } };

                return ret;
            }


            const std::vector<LocalCoords>& Hexahedron::GetVertexLocalCoordsList()
            {
                static std::vector<LocalCoords> ret{ { -1., -1., -1. }, { 1., -1., -1. }, { 1., 1., -1. },
                                                     { -1., 1., -1. },  { -1., -1., 1. }, { 1., -1., 1. },
                                                     { 1., 1., 1. },    { -1., 1., 1. }

                };

                return ret;
            }


            bool Hexahedron::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
            {
                return Internal::RefGeomEltNS::TopologyNS::IsOnVertex<Hexahedron>(vertex_index, coords);
            }


            bool Hexahedron::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
            {
                return Internal::RefGeomEltNS::TopologyNS::IsOnEdge_Spectral<Hexahedron>(edge_index, coords);
            }


            bool Hexahedron::IsOnFace(std::size_t face_index, const LocalCoords& coords)
            {
                auto [face_constant_component, constant_component_value] = AnalyseFace(face_index);

                assert(dimension == coords.GetDimension());

                if (!NumericNS::AreEqual(coords[face_constant_component.Get()], constant_component_value))
                    return false;

                // At this stage we know coords is on the right plane but not whether it is inside the quadrangle.
                auto&& coords2d = Extract2DCoordinates(face_constant_component, coords);

                return Quadrangle::IsOnFace(0ul, std::move(coords2d));
            }


            LocalCoords
            Hexahedron::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
            {
                assert(IsOnFace(face_index, coords));
                assert((orientation < 8u));

                auto [face_constant_component, constant_component_value] = AnalyseFace(face_index);

                // Reduce coords to 2d on the face plane, and use Quadrangle nameskake function for the calculation.
                auto&& coords2d = Extract2DCoordinates(face_constant_component, coords);

                auto&& coords2d_after_transformation = Quadrangle::TransformFacePoint(coords2d, 0ul, orientation);

                // Now grow back the third coordinate!
                std::vector<double> buf;
                std::size_t index_2d = 0ul;

                for (Advanced::ComponentNS::index_type i{ 0ul }; i < Advanced::ComponentNS::index_type{ dimension };
                     ++i)
                {
                    if (i == face_constant_component)
                        buf.push_back(constant_component_value);
                    else
                        buf.push_back(coords2d_after_transformation[index_2d++]);
                }

                assert(buf.size() == 3ul);

                LocalCoords ret(buf);
                return ret;
            }


            InterfaceNS::Nature Hexahedron::GetInteriorInterface()
            {
                return InterfaceNS::Nature::volume;
            }


            Type Hexahedron::GetType() noexcept
            {
                return Type::hexahedron;
            }


            bool Hexahedron::IsInside(const LocalCoords& coords)
            {
                const auto& vertex_coords_list = GetVertexLocalCoordsList();
                assert(vertex_coords_list.size() == 8ul);

                const auto r = coords.r();
                const auto s = coords.s();
                const auto t = coords.t();

                {
                    const auto& bottom_left_front = vertex_coords_list[0];

                    if (r < bottom_left_front.r() || s < bottom_left_front.s() || t < bottom_left_front.t())
                        return false;
                }

                {
                    const auto& top_right_back = vertex_coords_list[6];

                    if (r > top_right_back.r() || s > top_right_back.s() || t > top_right_back.t())
                        return false;
                }

                return true;
            }


            namespace // anonymous
            {


                /*!
                 * \brief Extract for an hexahedron face which of its component is constant and what is its value.
                 */
                std::pair<Advanced::ComponentNS::index_type, double> AnalyseFace(std::size_t face_index)
                {
                    // Extract the vertices that delimits the face.
                    const auto& vertex_on_face = LocalData<Hexahedron>::GetFace(face_index);
                    assert(vertex_on_face.size() == 4ul);

                    // Then their coordinates.
                    std::vector<LocalCoords> vertex_local_coords_list;

                    for (auto vertex_index : vertex_on_face)
                        vertex_local_coords_list.push_back(LocalData<Hexahedron>::GetVertexCoord(vertex_index));

                    auto pair = ExtractIdenticalComponentIndex(vertex_local_coords_list);

                    assert(pair.first.Get() < Hexahedron::dimension);

                    return pair;
                }


                /*!
                 * \brief 'Transform' a 3d coords in the hexahedron from into a 2d one.
                 *
                 * The dropped dimension is the one constant for the given face.
                 */
                LocalCoords Extract2DCoordinates(Advanced::ComponentNS::index_type face_constant_component,
                                                 const LocalCoords& coords)
                {
                    const auto Ncomponent = Advanced::ComponentNS::index_type{ coords.GetDimension() };
                    assert(Ncomponent.Get() == 3ul);
                    std::vector<double> coords2d;

                    for (Advanced::ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                    {
                        if (component == face_constant_component)
                            continue;

                        coords2d.push_back(coords[component.Get()]);
                    }

                    assert(coords2d.size() == 2ul);
                    LocalCoords ret(coords2d);

                    return ret;
                }


            } // namespace


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
