/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Format/Hexahedron8.hpp"        // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron8.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"       // IWYU pragma: keep
// < absolutely required to let MoReFEM know which format are actually supported!

#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace Traits
        {


            /*!
             * \brief Traits class that holds the static functions related to shape functions, interface and topology.
             *
             * It can't be instantiated directly: its purpose is either to provide directly a data through
             * static function:
             *
             * \code
             * constexpr auto Nshape_function = Hexahedron8::NshapeFunction();
             * \endcode
             *
             * or to be a template parameter to MoReFEM::RefGeomEltNS::Hexahedron8 class (current class is in an
             * additional layer of namespace):
             *
             * \code
             * MoReFEM::RefGeomEltNS::Traits::Hexahedron8
             * \endcode
             *
             */
            class Hexahedron8 final
            : public ::MoReFEM::Internal::RefGeomEltNS::
                  RefGeomEltImpl<Hexahedron8, ShapeFunctionNS::Hexahedron8, TopologyNS::Hexahedron>
            {


              protected:
                //! Convenient alias.
                using self = Hexahedron8;

                /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
                ///@{

                //! Constructor.
                Hexahedron8() = delete;

                //! Destructor.
                ~Hexahedron8() = delete;

                //! \copydoc doxygen_hide_copy_constructor
                Hexahedron8(const Hexahedron8& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Hexahedron8(Hexahedron8&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Hexahedron8& operator=(const Hexahedron8& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                self& operator=(self&& rhs) = delete;

                ///@}


              public:
                /*!
                 * \brief Name associated to the RefGeomElt.
                 *
                 * \return Name that is guaranteed to be unique (throught the GeometricEltFactory) and can
                 * therefore also act as an identifier.
                 */
                static const Advanced::GeomEltNS::GenericName& ClassName();

                //! Number of Coords required to describe fully a GeometricElt of this type.
                enum : std::size_t
                {
                    Ncoords = 8u
                };


                /*!
                 * \brief Enum associated to the RefGeomElt.
                 *
                 * \return Enum value guaranteed to be unique (throught the GeometricEltFactory); its
                 * raison d'être is that many operations are much faster on an enumeration than on a string.
                 */
                static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
                {
                    return MoReFEM::Advanced::GeometricEltEnum::Hexahedron8;
                }


              private:
                // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!
            };


        } // namespace Traits


        /*!
         * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Hexahedron8.
         *
         * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
         * store in one dynamic container all the kinds of GeometricElt present in a mesh.
         *
         * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
         * can be included in:
         *
         * \code
         * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
         * \endcode
         *
         */
        class Hexahedron8 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Hexahedron8>
        {
          public:
            //! Constructor.
            Hexahedron8() = default;

            //! Destructor.
            virtual ~Hexahedron8() override;

            //! \copydoc doxygen_hide_copy_constructor
            Hexahedron8(const Hexahedron8& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Hexahedron8(Hexahedron8&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Hexahedron8& operator=(const Hexahedron8& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Hexahedron8& operator=(Hexahedron8&& rhs) = delete;


          private:
            // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
        };


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
