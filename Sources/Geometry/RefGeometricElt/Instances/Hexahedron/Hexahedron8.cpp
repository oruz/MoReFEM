/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron8.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        Hexahedron8::~Hexahedron8() = default;


        namespace Traits
        {


            const Advanced::GeomEltNS::GenericName& Hexahedron8::ClassName()
            {
                static Advanced::GeomEltNS::GenericName ret("Hexahedron8");
                return ret;
            }


        } // namespace Traits


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
