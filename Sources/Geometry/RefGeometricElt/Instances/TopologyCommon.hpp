//! \file
//
//
//  TopologyCommon.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_COMMON_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_COMMON_HPP_

#include <array>       // IWYU pragma: export
#include <iosfwd>      // IWYU pragma: export
#include <type_traits> // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"                             // IWYU pragma: export
#include "Geometry/Interfaces/EnumInterface.hpp"                       // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"        // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp" // IWYU pragma: export

#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TOPOLOGY_COMMON_HPP_
