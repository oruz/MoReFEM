//! \file
//
//
//  ComponentIndex.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/05/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_COMPONENT_INDEX_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_COMPONENT_INDEX_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Advanced::ComponentNS
{

    //! Strong type for \a Component index.
    // clang-format off
    using index_type =
        StrongType
        <
            std::size_t,
            struct index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::Advanced::ComponentNS

#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_COMPONENT_INDEX_HPP_
