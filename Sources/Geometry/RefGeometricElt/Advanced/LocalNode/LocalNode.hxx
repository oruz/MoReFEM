/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HXX_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HXX_

// IWYU pragma: private, include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "Utilities/Type/StrongType/Skills/Comparable.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }
namespace MoReFEM::Advanced { class LocalNode; }
namespace MoReFEM::RefGeomEltNS::TopologyNS { class LocalInterface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    inline const RefGeomEltNS::TopologyNS::LocalInterface& LocalNode::GetLocalInterface() const noexcept
    {
        return local_interface_;
    }


    inline LocalNodeNS::index_type LocalNode::GetIndex() const noexcept
    {
        return index_;
    }


    inline const LocalCoords& LocalNode::GetLocalCoords() const noexcept
    {
        return local_coords_;
    }


    inline bool operator<(const LocalNode& lhs, const LocalNode& rhs)
    {
        return lhs.GetIndex() < rhs.GetIndex();
    }


} // namespace MoReFEM::Advanced


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HXX_
