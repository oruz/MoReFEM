/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"                       // IWYU pragma: export
#include "Geometry/Interfaces/EnumInterface.hpp"                 // IWYU pragma: export
#include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp" // IWYU pragma: export


namespace MoReFEM::LocalNodeNS
{

    //! Strong type for \a LocalNode index.
    // clang-format off
    using index_type =
        StrongType
        <
            std::size_t,
            struct index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible,
            StrongTypeNS::Divisible
        >;

    // clang-format on


} // namespace MoReFEM::LocalNodeNS


namespace MoReFEM::Advanced
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief A \a LocalNode is mostly an extension of a \a LocalCoords, which additionally is attributed an index and keeps track on the \a LocalInterface
     * on which it is located.
     *
     * So there might be several \a LocalNode on a given \a LocalInterface (there are no local equivalent on \a
     * NodeBearer though).
     *
     */
    class LocalNode final
    {

      public:
        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const LocalNode>;

        //! Alias to vector of shared_pointer.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] local_interface Local interface upon which the LocalNode is built.
         * \param[in] index Incremental index of the local nodes in the BasicRefFElt. All local nodes on vertices
         * are numbered first; then all edges, and so on.
         * \param[in] local_coords Approximate position of the local node.
         */
        LocalNode(RefGeomEltNS::TopologyNS::LocalInterface&& local_interface,
                  LocalNodeNS::index_type index,
                  const LocalCoords& local_coords);

        //! Destructor.
        ~LocalNode() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalNode(const LocalNode& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalNode(LocalNode&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalNode& operator=(const LocalNode& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalNode& operator=(LocalNode&& rhs) = delete;

        ///@}


        //! Get the local interface.
        const RefGeomEltNS::TopologyNS::LocalInterface& GetLocalInterface() const noexcept;

        //! Get the local index of the dof.
        LocalNodeNS::index_type GetIndex() const noexcept;

        //! Get the local coordinates of the dof.
        const LocalCoords& GetLocalCoords() const noexcept;

        //! Print the informations of the local dof.
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;


      private:
        //! Local interface.
        const RefGeomEltNS::TopologyNS::LocalInterface local_interface_;

        //! Local index.
        const LocalNodeNS::index_type index_;

        //! Local coordinates.
        LocalCoords local_coords_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! The criterion used is the index as returned by \a LocalNode::GetIndex().
    bool operator<(const LocalNode& lhs, const LocalNode& rhs);


    ///@} // \addtogroup


} // namespace MoReFEM::Advanced


/// @} // addtogroup FiniteElementGroup


#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_LOCAL_NODE_HPP_
