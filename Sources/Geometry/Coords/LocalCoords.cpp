/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Mar 2017 22:51:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <initializer_list>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"


namespace MoReFEM
{


    LocalCoords::LocalCoords(std::initializer_list<double>&& coor) : coordinate_list_(std::move(coor))
    { }


    LocalCoords::~LocalCoords() = default;


    void LocalCoords::Print(std::ostream& out) const
    {
        Utilities::PrintContainer<>::Do(coordinate_list_, out);
    }


    std::vector<Advanced::ComponentNS::index_type> ExtractMismatchedComponentIndexes(const LocalCoords& coords1,
                                                                                     const LocalCoords& coords2)
    {
        const auto& component_coordinates1 = coords1.GetCoordinates();
        const auto& component_coordinates2 = coords2.GetCoordinates();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ component_coordinates1.size() };
        assert(Ncomponent.Get() == component_coordinates2.size());

        std::vector<Advanced::ComponentNS::index_type> ret;

        for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1[i.Get()], component_coordinates2[i.Get()]))
                ret.push_back(i);
        }

        return ret;
    }


    Advanced::ComponentNS::index_type ExtractMismatchedComponentIndex(const LocalCoords& coords1,
                                                                      const LocalCoords& coords2)
    {
        const auto& component_coordinates1 = coords1.GetCoordinates();
        const auto& component_coordinates2 = coords2.GetCoordinates();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ component_coordinates1.size() };
        assert(Ncomponent.Get() == component_coordinates2.size());

        for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1[i.Get()], component_coordinates2[i.Get()]))
            {
#ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractMismatchedComponentIndexes(coords1, coords2);
                assert("When the current function is called it is implicitly expected to be for an edge of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1ul);
                assert(output_more_generic_function.back() == i);
#endif // NDEBUG

                return i;
            }
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    std::vector<Advanced::ComponentNS::index_type>
    ExtractIdenticalComponentIndexes(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1ul);
        std::vector<std::vector<double>> coords_component_list;

        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());

        const auto& last_coord = coords_component_list.back();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ coords_component_list.back().size() };

#ifndef NDEBUG
        for (auto coords_component : coords_component_list)
            assert(coords_component.size() == Ncomponent.Get());
#endif // NDEBUG

        std::vector<Advanced::ComponentNS::index_type> ret;

        for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
        {
            bool still_identical = true;

            for (std::size_t j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j][i.Get()], last_coord[i.Get()]))
                    still_identical = false;
            }

            if (still_identical)
                ret.push_back(i);
        }

        return ret;
    }


    std::pair<Advanced::ComponentNS::index_type, double>
    ExtractIdenticalComponentIndex(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1ul);
        std::vector<std::vector<double>> coords_component_list;

        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());

        const auto& last_coord = coords_component_list.back();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ coords_component_list.back().size() };

#ifndef NDEBUG
        for (auto coords_component : coords_component_list)
            assert(coords_component.size() == Ncomponent.Get());
#endif // NDEBUG

        for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
        {
            bool still_identical = true;

            for (std::size_t j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j][i.Get()], last_coord[i.Get()]))
                    still_identical = false;
            }

            if (still_identical)
            {
#ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractIdenticalComponentIndexes(coords);
                assert("When the current function is called it is implicitly expected to be for a face of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1ul);
                assert(output_more_generic_function.back() == i);
#endif // NDEBUG
                return std::make_pair(i, last_coord[i.Get()]);
            }
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    bool operator==(const LocalCoords& coords1, const LocalCoords& coords2)
    {
        const auto& components1 = coords1.GetCoordinates();
        const auto& components2 = coords2.GetCoordinates();

        const std::size_t Ncomponent = components1.size();
        assert(components1.size() == components2.size());

        for (std::size_t i = 0ul; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(components1[i], components2[i]))
                return false;
        }

        return true;
    }


    std::ostream& operator<<(std::ostream& stream, const LocalCoords& point)
    {
        point.Print(stream);
        return stream;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
