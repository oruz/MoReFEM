/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Aug 2017 18:24:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HPP_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HPP_


#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Define a spatial three-dimensional point.
     *
     * If the point is actually part of a mesh, you should look for derived class \a Coords.
     *
     * \todo #887 Should probably be advanced, but currently FindSpatialPointOfGlobalVector requires public and casual
     * access to it. However this class was avoided in Poromechanics; I'll have to check whether the new mechanism would
     * work in CardiacMechanics where FindSpatialPointOfGlobalVector is applied (the priority of this task is not very
     * high, and I have to retrieve what I did in Poromechanics.
     */
    class SpatialPoint
    {
      public:
        //! Alias to self.
        using self = SpatialPoint;

        //! Convenient smart pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Vector of unique_ptr.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special member functions.
        ///@{


        /*!
         * \brief Default constructor; all coordinates are set to 0.
         *
         */
        explicit SpatialPoint();

        /*!
         * \brief Constructor from three scalars.
         *
         * \param[in] x Value for first component.
         * \param[in] y Value for second component.
         * \param[in] z Value for third component.
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit SpatialPoint(double x, double y, double z, const double space_unit);

        /*!
         * \brief Constructor from an array.
         *
         * \tparam T Must be std::array<Floating-point type, 3>.
         *
         * \param[in] value Value of the array to set.
         * \copydoc doxygen_hide_space_unit_arg
         */
        template<typename T>
        explicit SpatialPoint(T&& value, const double space_unit);


        /*!
         * \brief Constructor from a input stream.
         *
         * \param[in] Ncoor Number of coordinates to be read. Expected to be at most 3.
         * \param[in,out] stream Stream from which the point is read. Coordinates are expected to be separated by tabs
         * or spaces.
         * Stream is read until failbit is met; then it is put back at the position just before that failure.
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit SpatialPoint(std::size_t Ncoor, std::istream& stream, const double space_unit);

        //! \copydoc doxygen_hide_copy_constructor
        SpatialPoint(const SpatialPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SpatialPoint(SpatialPoint&& rhs) = delete;

        /*!
         * \brief Destructor.
         *
         * \internal Virtual status is avoided as \a Coords objects are not supposed to be used through a
         * \a SpatialPoint pointer, and at the time being no other child class is expected.
         * \endinternal
         */
        ~SpatialPoint();

        //! \copydoc doxygen_hide_copy_affectation
        SpatialPoint& operator=(const SpatialPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SpatialPoint& operator=(SpatialPoint&& rhs) = delete;


        ///@}


      public:
        //! Return the first component of the point.
        double x() const;

        //! Return the second component of the point (if relevant; if not an exception is thrown).
        double y() const;

        //! Return the third component of the point (if relevant; if not an exception is thrown).
        double z() const;

        /*!
         * \copydoc doxygen_hide_const_subscript_operator
         *
         * \a i might be 0 (for 'x' component), 1 (for 'y') or 2 (for 'z').
         */
        double operator[](std::size_t i) const;

        /*!
         * \brief Non constant access to the value for \a component.
         *
         * \param[in] index Index of the sought component: 0 for x, 1 for y and 2 for z.
         */
        double& GetNonCstValue(std::size_t index);

        //! Print function: display the coordinates.
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

        //! Get the position as a std::array.
        const std::array<double, 3>& GetCoordinateList() const noexcept;

        //! Reset the coords to 0.
        void Reset();


      private:
        //! List of coordinates.
        std::array<double, 3> coordinate_list_;

        //! Friendship.
        friend double Distance(const SpatialPoint& lhs, const SpatialPoint& rhs);
    };


    /*!
     * \brief Calculates the distance between two points, using a L2 norm.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Distance between two points following L2 norm.
     */
    double Distance(const SpatialPoint& lhs, const SpatialPoint& rhs);


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const SpatialPoint& rhs);


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Coords/SpatialPoint.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HPP_
