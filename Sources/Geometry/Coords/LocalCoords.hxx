/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Mar 2017 22:51:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HXX_

// IWYU pragma: private, include "Geometry/Coords/LocalCoords.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <numeric>
#include <type_traits>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class T>
    LocalCoords::LocalCoords(T&& coor)
    {
        static_assert(std::is_same<std::remove_reference_t<T>, std::vector<double>>::value,
                      "Forwarding reference template argument.");

        coordinate_list_ = std::move(coor);
    }


    inline double LocalCoords::r() const noexcept
    {
        return operator[](0);
    }


    inline double LocalCoords::s() const noexcept
    {
        return operator[](1);
    }


    inline double LocalCoords::t() const noexcept
    {
        return operator[](2);
    }


    inline std::size_t LocalCoords::GetDimension() const noexcept
    {
        return coordinate_list_.size();
    }


    inline double LocalCoords::operator[](std::size_t index) const noexcept
    {
        assert(index < GetDimension());
        return coordinate_list_[index];
    }


    inline double& LocalCoords::GetNonCstValue(std::size_t index) noexcept
    {
        assert(index < GetDimension());
        return coordinate_list_[index];
    }


    inline double LocalCoords::GetValueOrZero(std::size_t index) const noexcept
    {
        assert(index < 3);

        if (index >= GetDimension())
            return 0.;

        return coordinate_list_[index];
    }


    template<class ContainerT>
    LocalCoords ComputeCenterOfGravity(const ContainerT& coords_list)
    {
        static_assert(std::is_same<typename ContainerT::value_type, LocalCoords>(),
                      "ContainerT must be a container of LocalCoords objects.");

        assert(!coords_list.empty());
        const std::size_t Ncomponent = coords_list.back().GetDimension();

        auto begin = coords_list.cbegin();
        auto end = coords_list.cend();

        assert(std::all_of(begin,
                           end,
                           [Ncomponent](const LocalCoords& local_coord)
                           {
                               return local_coord.GetDimension() == Ncomponent;
                           }));

        std::vector<double> ret_values(Ncomponent);

        const double inv = 1. / static_cast<double>(coords_list.size());

        for (std::size_t i = 0ul; i < Ncomponent; ++i)
        {
            ret_values[i] = inv
                            * std::accumulate(begin,
                                              end,
                                              0.,
                                              [i](double sum, const LocalCoords& local_coord)
                                              {
                                                  return sum + local_coord[i];
                                              });
        }


        return LocalCoords(ret_values);
    }


    inline const std::vector<double>& LocalCoords::GetCoordinates() const noexcept
    {
        return coordinate_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HXX_
