/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Aug 2017 18:24:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HXX_

// IWYU pragma: private, include "Geometry/Coords/SpatialPoint.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <tuple>
#include <type_traits>


namespace MoReFEM
{


    template<class T>
    SpatialPoint::SpatialPoint(T&& array, const double space_unit)
    {
        using T_without_reference = typename std::remove_reference_t<T>;

        static_assert(std::is_floating_point<typename T_without_reference::value_type>::value,
                      "T must be an array of floating-point type!");

        static_assert(std::tuple_size<T_without_reference>::value == 3u,
                      "T must be a std::array of size 3. The condition is necessary but not "
                      "enough to ensure this, but it's a good start (the existence of iterators also helps below).");

        std::transform(array.cbegin(),
                       array.cend(),
                       coordinate_list_.begin(),
                       [space_unit](auto value)
                       {
                           return static_cast<double>(value) * space_unit;
                       });
    }


    inline double SpatialPoint::x() const
    {
        return coordinate_list_[0];
    }


    inline double SpatialPoint::y() const
    {
        return coordinate_list_[1];
    }


    inline double SpatialPoint::z() const
    {
        return coordinate_list_[2];
    }


    inline double SpatialPoint::operator[](std::size_t index) const
    {
        assert(index < coordinate_list_.size());
        return coordinate_list_[index];
    }


    inline double& SpatialPoint::GetNonCstValue(std::size_t index)
    {
        assert(index < coordinate_list_.size());
        return coordinate_list_[index];
    }


    inline const std::array<double, 3>& SpatialPoint::GetCoordinateList() const noexcept
    {
        return coordinate_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup

#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_SPATIAL_POINT_HXX_
