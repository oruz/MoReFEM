//! \file
//
//
//  StrongType.hpp
//  MoReFEM
//
//  Created by sebastien on 07/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_STRONG_TYPE_HPP_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_STRONG_TYPE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::CoordsNS
{


    /*!
     * \brief Enum class to choose which kind of \a Coords index is to be used.
     */
    enum class index_enum
    {
        program_wise_position,
        processor_wise_position,
        from_mesh_file
    };


    /*!
     * \brief Strong type for index that gives the program-wise position.
     */
    using program_wise_position = StrongType<std::size_t,
                                             struct ProgramWisePositionTag,
                                             StrongTypeNS::Comparable,
                                             StrongTypeNS::Hashable,
                                             StrongTypeNS::DefaultConstructible,
                                             StrongTypeNS::Printable,
                                             StrongTypeNS::AsMpiDatatype,
                                             StrongTypeNS::Incrementable>;


    /*!
     * \brief Strong type for index that gives the processor-wise position.
     */
    using processor_wise_position = StrongType<std::size_t,
                                               struct ProcessorWisePositionTag,
                                               StrongTypeNS::Comparable,
                                               StrongTypeNS::Hashable,
                                               StrongTypeNS::DefaultConstructible,
                                               StrongTypeNS::Printable,
                                               StrongTypeNS::AsMpiDatatype,
                                               StrongTypeNS::Incrementable>;


    /*!
     * \brief Strong type for the \a Coords index that was read in the input mesh file (and is format-dependant).
     */
    using index_from_mesh_file = StrongType<std::size_t,
                                            struct IndexFromMeshFileTag,
                                            StrongTypeNS::Comparable,
                                            StrongTypeNS::Hashable,
                                            StrongTypeNS::DefaultConstructible,
                                            StrongTypeNS::Printable,
                                            StrongTypeNS::AsMpiDatatype,
                                            StrongTypeNS::Incrementable>;


    // clang-format off
    //! Convenient alias to find which \a StrongType to use depending on the enum  value template parameter.
    //! \tparam TypeT Tells which kind of \a Coords index is actually requested.
    template<index_enum TypeT>
    using index_type =
    std::conditional_t
    <
        TypeT == index_enum::program_wise_position,
        program_wise_position,
        std::conditional_t
        <
            TypeT == index_enum::processor_wise_position,
            processor_wise_position,
            index_from_mesh_file
        >
    >;
    // clang-format on

    static_assert(std::is_same<index_type<index_enum::program_wise_position>, program_wise_position>());

    static_assert(std::is_same<index_type<index_enum::processor_wise_position>, processor_wise_position>());

    static_assert(std::is_same<index_type<index_enum::from_mesh_file>, index_from_mesh_file>());


} // namespace MoReFEM::CoordsNS


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_STRONG_TYPE_HPP_
