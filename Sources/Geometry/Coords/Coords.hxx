/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Jan 2014 17:51:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_

// IWYU pragma: private, include "Geometry/Coords/Coords.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iomanip>
#include <memory>
#include <ostream>
#include <set>
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM
{


    template<class T>
    Coords::Coords(T&& array, const double space_unit) : parent(array, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    inline CoordsNS::index_from_mesh_file Coords::GetIndexFromMeshFile() const noexcept
    {
        return GetCoordIndexes().GetFileIndex();
    }


    inline void Coords::SetIndexFromMeshFile(::MoReFEM::CoordsNS::index_from_mesh_file index,
                                             bool do_allow_second_call) noexcept
    {
        GetNonCstCoordIndexes().SetFileIndex(index, do_allow_second_call);
    }


    inline void Coords::SetMeshLabel(const MeshLabel::const_shared_ptr& mesh_label)
    {
        mesh_label_ = mesh_label;
    }


    inline MeshLabel::const_shared_ptr Coords::GetMeshLabelPtr() const
    {
        return mesh_label_;
    }


    inline bool operator<(const Coords& lhs, const Coords& rhs)
    {
        return lhs.GetProcessorWisePosition() < rhs.GetProcessorWisePosition();
    }


    inline bool operator==(const Coords& lhs, const Coords& rhs)
    {
#ifndef NDEBUG
        if (lhs.GetProcessorWisePosition() == rhs.GetProcessorWisePosition())
        {
            assert("Coordinates should also be the same, even if for efficiency reason I stick to index comparison!"
                   && Distance(lhs, rhs) < 1.e-12);
        }
#endif // NDEBUG

        return lhs.GetProcessorWisePosition() == rhs.GetProcessorWisePosition();
    }


    template<bool DoPrintIndexT>
    void WriteEnsightFormat(const Coords& point, std::ostream& stream)
    {
        if (DoPrintIndexT)
        {

            stream << std::setw(8) << point.GetIndexFromMeshFile();
        }

        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.x();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.y();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.z();
        stream << std::endl;
    }


    template<class FloatT>
    void WriteMeditFormat(const std::size_t dimension, const Coords& point, libmeshb_int lm_mesh_index)
    {

        int label_index(0);

        auto label_ptr = point.GetMeshLabelPtr();
        if (label_ptr)
            label_index = static_cast<int>(label_ptr->GetIndex());

        switch (dimension)
        {
        case 1:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), 0., label_index);
            break;
        case 2:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), point.y(), label_index);
            break;
        case 3:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), point.y(), point.z(), label_index);
            break;
        default:
            assert(false && "Dimension should be 1, 2, or 3!");
            break;
        }
    }


    inline InterfaceNS::Nature Coords::GetInterfaceNature() const
    {
        return interface_nature_;
    }


    inline const Internal::CoordsNS::CoordIndexes& Coords::GetCoordIndexes() const noexcept
    {
        return coord_indexes_;
    }


    inline Internal::CoordsNS::CoordIndexes& Coords::GetNonCstCoordIndexes() noexcept
    {
        return const_cast<Internal::CoordsNS::CoordIndexes&>(GetCoordIndexes());
    }


    inline CoordsNS::program_wise_position Coords::GetProgramWisePosition() const noexcept
    {
        return GetCoordIndexes().GetProgramWisePosition();
    }


    inline CoordsNS::processor_wise_position Coords::GetProcessorWisePosition() const noexcept
    {
        return GetCoordIndexes().GetProcessorWisePosition();
    }


    template<Internal::CoordsNS::DoCheckFirstCall DoCheckFirstCallT>
    void Coords::SetProgramWisePosition(CoordsNS::program_wise_position position) noexcept
    {
        GetNonCstCoordIndexes().SetProgramWisePosition<DoCheckFirstCallT>(position);
    }


    template<Internal::CoordsNS::DoCheckFirstCall DoCheckFirstCallT>
    void Coords::SetProcessorWisePosition(CoordsNS::processor_wise_position position) noexcept
    {
        GetNonCstCoordIndexes().SetProcessorWisePosition<DoCheckFirstCallT>(position);
    }


    inline void Coords::AddDomainContainingCoords(std::size_t domain_unique_id)
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of your "
                  "model. See the TestDomainListInCoords model for example.");
        domain_list_.insert(domain_unique_id);
    }


    template<CoordsNS::index_enum TypeT>
    CoordsNS::index_type<TypeT> Coords::GetIndex() const noexcept
    {
        if constexpr (TypeT == CoordsNS::index_enum::program_wise_position)
            return GetProgramWisePosition();
        else if constexpr (TypeT == CoordsNS::index_enum::processor_wise_position)
            return GetProcessorWisePosition();
        else if constexpr (TypeT == CoordsNS::index_enum::from_mesh_file)
            return GetIndexFromMeshFile();
        else
        {
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup

#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_
