/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <set>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <unordered_map>
// IWYU pragma: no_include <__tree>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/Coords/Coords.hpp" // IWYU pragma: associated
#include "Geometry/Coords/Exceptions/Coords.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    Coords::~Coords()
    {
#ifndef NDEBUG
        GetNonCstNobjects()--;
#endif // NDEBUG
    }


    Coords::Coords() : parent()
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    Coords::Coords(double x, double y, double z, const double space_unit) : parent(x, y, z, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    Coords::Coords(std::size_t Ncoor, std::istream& stream, const double space_unit) : parent(Ncoor, stream, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    void Coords::ExtendedPrint(std::ostream& out) const
    {
        parent::Print(out);
        out << " [" << GetProgramWisePosition() << ']';
    }


    void Coords::SetInterfaceNature(InterfaceNS::Nature interface_nature)
    {
        if (interface_nature_ == InterfaceNS::Nature::undefined)
            interface_nature_ = interface_nature;
        else if (interface_nature_ != interface_nature)
            throw ExceptionNS::CoordsNS::InconsistentType(GetIndexFromMeshFile(), __FILE__, __LINE__);
    }


    void WriteVTK_PolygonalDataFormat(const Coords& point, std::ostream& stream)
    {
        stream << point.x() << ' ';
        stream << point.y() << ' ';
        stream << point.z() << ' ';
        stream << std::endl;
    }


    bool Coords::IsInDomain(std::size_t domain_unique_id) const
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of "
                  "your model. See the TestDomainListInCoords model for example.");

        auto begin = domain_list_.cbegin();
        auto end = domain_list_.cend();

        auto it = std::find(begin, end, domain_unique_id);

        return it == end ? false : true;
    }


    bool Coords::IsInDomain(const Domain& domain) const
    {
        const auto domain_id = domain.GetUniqueId();
        return IsInDomain(domain_id);
    }


    create_domain_list_for_coords& Coords::GetNonCstCreateDomainListForCoords()
    {
        // Static is initialized only during first call.
        static create_domain_list_for_coords create_domain_list_for_coords_ = create_domain_list_for_coords::no;
        return create_domain_list_for_coords_;
    }


    create_domain_list_for_coords Coords::GetCreateDomainListForCoords()
    {
        return GetNonCstCreateDomainListForCoords();
    }


    void Coords::SetCreateDomainListForCoords()
    {
        GetNonCstCreateDomainListForCoords() = create_domain_list_for_coords::yes;
    }


#ifndef NDEBUG
    std::size_t& Coords::GetNonCstNobjects()
    {
        static std::size_t ret = 0ul;
        return ret;
    }


    std::size_t Coords::Nobjects()
    {
        return GetNonCstNobjects();
    }


#endif // NDEBUG


    void Coords::SetIsLowestProcessor(bool value)
    {
        is_lowest_processor_ = value;
    }


    bool Coords::IsLowestProcessorRank() const noexcept
    {
        return is_lowest_processor_;
    }


    std::ostream& operator<<(std::ostream& stream, const Coords& point)
    {
        point.Print(stream);
        return stream;
    }


#ifndef NDEBUG
    void AssertNCoordsConsistency(const Wrappers::Mpi& mpi)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) mesh_storage = mesh_manager.GetStorage();

        auto Ncoords_in_meshes = 0ul;

        for (const auto& [mesh_id, mesh_ptr] : mesh_storage)
        {
            static_cast<void>(mesh_id);
            assert(!(!mesh_ptr));
            const auto& mesh = *mesh_ptr;

            Ncoords_in_meshes += mesh.NprocessorWiseCoord();
            Ncoords_in_meshes += mesh.NghostCoord();
        }


        if (Ncoords_in_meshes != Coords::Nobjects())
        {
            std::ostringstream oconv;
            oconv << "Inconsistent number of Coords on processor " << mpi.GetRank<int>()
                  << ": all meshes "
                     "encompass "
                  << Ncoords_in_meshes << " whereas there are " << Coords::Nobjects()
                  << " Coords "
                     "object currently alive on the processor.";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }

#endif // NDEBUG


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
