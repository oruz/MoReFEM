/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
#include <optional>

#include "Geometry/Coords/Internal/CoordIndexes.hpp"
#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            void CoordIndexes::SetFileIndex(::MoReFEM::CoordsNS::index_from_mesh_file value,
                                            bool do_allow_second_call) noexcept
            {
                static_cast<void>(do_allow_second_call);

#ifndef NDEBUG
                {
                    if (!do_allow_second_call)
                        assert(!file_index_.has_value() && "Should be assigned only once!");
                }
#endif // NDEBUG

                file_index_ = value;
            }


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
