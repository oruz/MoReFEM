/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Aug 2017 17:16:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Internal/Factory.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            Coords::shared_ptr Factory::Origin()
            {
                auto ptr = new Coords;
                return Coords::shared_ptr(ptr);
            }


            Coords::shared_ptr Factory::FromComponents(double x, double y, double z, const double space_unit)
            {
                auto ptr = new Coords(x, y, z, space_unit);
                return Coords::shared_ptr(ptr);
            }


            Coords::shared_ptr Factory::FromStream(std::size_t Ncoor, std::istream& stream, const double space_unit)
            {
                auto ptr = new Coords(Ncoor, stream, space_unit);
                return Coords::shared_ptr(ptr);
            }


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
