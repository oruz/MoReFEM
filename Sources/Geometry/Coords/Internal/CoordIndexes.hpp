/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_

#include <optional>

#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            //! Convenient enum class.
            enum class DoCheckFirstCall
            {
                no,
                yes
            };


            /*!
             * \brief Inner object in Coords in charge of storing relevant indexes.
             *
             * We in fact need three kind of indexes for a given Coord:
             *
             * - The index as read in the original mesh file. No assumption is made at all on this one: it might be
             * contiguous or not, and might start at whichever value it wants. It's kept only to be able to generate
             * back the mesh in file format; it is rather impractical otherwise.
             * - A contiguous index that gives away the position of the Coord object within Mesh object.
             * There are two flavors for such an index:
             *   . The original one, computed before any processor-wise reduction and that gives the position in the
             * initial list. . One recomputed after processor-wise reduction, that gives away the position in the
             * updated list.
             */
            class CoordIndexes
            {


              public:
                //! \copydoc doxygen_hide_alias_self
                using self = CoordIndexes;

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit CoordIndexes() = default;

                //! Destructor.
                ~CoordIndexes() = default;

                //! \copydoc doxygen_hide_copy_constructor
                CoordIndexes(const CoordIndexes& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                CoordIndexes(CoordIndexes&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                CoordIndexes& operator=(const CoordIndexes& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                CoordIndexes& operator=(CoordIndexes&& rhs) = default;

                ///@}

              public:
                //! Index given in the mesh file.
                ::MoReFEM::CoordsNS::index_from_mesh_file GetFileIndex() const noexcept;

                //! Position in Mesh::coords_list_ before processor-wise reduction.
                ::MoReFEM::CoordsNS::program_wise_position GetProgramWisePosition() const noexcept;

                //! Position in Mesh::coords_list_ after processor-wise reduction.
                ::MoReFEM::CoordsNS::processor_wise_position GetProcessorWisePosition() const noexcept;


                //! Set index given in the mesh file.
                //! \param[in] value Value of file index computed outside the class.
                //! \param[in] do_allow_second_call False is the default value: the method is expected to be called
                //! once per \a Coords, and it's there checked through an assert. However, when reloading pre-
                //! partitioned data we need a second call; only for this specific call this argument should be
                //! set to true (this skips the assert).
                void SetFileIndex(::MoReFEM::CoordsNS::index_from_mesh_file value,
                                  bool do_allow_second_call = false) noexcept;

                //! Set position in Mesh::coords_list_ before processor-wise reduction.
                //! \param[in] value Value computed outside the class.
                //! \tparam DoCheckFirstCallT Boolean value which is true by default - false is needed only in the
                //! specific case of loading from prepartitioned data.
                template<DoCheckFirstCall DoCheckFirstCallT>
                void SetProgramWisePosition(::MoReFEM::CoordsNS::program_wise_position value) noexcept;

                //! Set position in Mesh::coords_list_ after processor-wise reduction.
                //! \param[in] value Value computed outside the class.
                //! \tparam DoCheckFirstCallT Boolean value which is true by default - false is needed only in the
                //! specific case of loading
                //!  from prepartitioned data.
                template<DoCheckFirstCall DoCheckFirstCallT>
                void SetProcessorWisePosition(::MoReFEM::CoordsNS::processor_wise_position value) noexcept;


              private:
                //! Index given in the mesh file.
                std::optional<::MoReFEM::CoordsNS::index_from_mesh_file> file_index_ = std::nullopt;

                //! Position in Mesh::coords_list_ before processor-wise reduction.
                std::optional<::MoReFEM::CoordsNS::program_wise_position> program_wise_position_ = std::nullopt;

                /*!
                 * \brief Position in Mesh::coords_list_ after processor-wise reduction.
                 *
                 * Note: before reduction or in sequential case, the convention is to set the value below with the same
                 * value as position_in_original_coord_list_.
                 */
                std::optional<::MoReFEM::CoordsNS::processor_wise_position> processor_wise_position_ = std::nullopt;


#ifndef NDEBUG
                //! To check SetPositionInReducedCoordList() is called at most once.
                bool was_set_position_in_reduced_coord_list_already_called_ = false;
#endif // NDEBUG
            };


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Coords/Internal/CoordIndexes.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_
