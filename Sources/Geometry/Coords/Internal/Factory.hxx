/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Aug 2017 17:16:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_

// IWYU pragma: private, include "Geometry/Coords/Internal/Factory.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            template<typename T>
            Coords::shared_ptr Factory::FromArray(T&& value, const double space_unit)
            {
                auto ptr = new Coords(value, space_unit);
                return Coords::shared_ptr(ptr);
            }


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_
