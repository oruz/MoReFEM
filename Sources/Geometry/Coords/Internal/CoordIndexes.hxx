/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_

// IWYU pragma: private, include "Geometry/Coords/Internal/CoordIndexes.hpp"

#include <cassert>
#include <optional>

#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            template<DoCheckFirstCall DoCheckFirstCallT>
            void CoordIndexes::SetProgramWisePosition(::MoReFEM::CoordsNS::program_wise_position value) noexcept
            {
                if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
                    assert(!program_wise_position_.has_value() && "Should be assigned only once!");

                program_wise_position_ = value;

                if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
                    assert(!processor_wise_position_.has_value() && "Should also not have been assigned before!");

                processor_wise_position_ = ::MoReFEM::CoordsNS::processor_wise_position(value.Get());
            }


            template<DoCheckFirstCall DoCheckFirstCallT>
            void CoordIndexes::SetProcessorWisePosition(::MoReFEM::CoordsNS::processor_wise_position value) noexcept
            {
                if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
                {
                    assert(was_set_position_in_reduced_coord_list_already_called_ == false
                           && "Should be assigned only once!");

#ifndef NDEBUG
                    was_set_position_in_reduced_coord_list_already_called_ = true;
#endif // NDEBUG
                }

                processor_wise_position_ = value;
            }


            inline ::MoReFEM::CoordsNS::index_from_mesh_file CoordIndexes::GetFileIndex() const noexcept
            {
                assert(file_index_.has_value());
                return file_index_.value();
            }


            inline ::MoReFEM::CoordsNS::program_wise_position CoordIndexes::GetProgramWisePosition() const noexcept
            {
                assert(program_wise_position_ != std::nullopt);
                return program_wise_position_.value();
            }


            inline ::MoReFEM::CoordsNS::processor_wise_position CoordIndexes::GetProcessorWisePosition() const noexcept
            {
                assert(processor_wise_position_.has_value());
                return processor_wise_position_.value();
            }


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_
