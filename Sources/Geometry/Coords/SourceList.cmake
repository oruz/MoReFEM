### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coords.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coords.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Coords.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/StrongType.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
