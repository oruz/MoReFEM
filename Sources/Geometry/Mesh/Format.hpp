/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Jun 2017 14:51:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_FORMAT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_FORMAT_HPP_


namespace MoReFEM
{


    namespace MeshNS
    {


        /*!
         * \brief List of all available formats (plus None when uninitialized).
         *
         * Each of them should get a namesake struct in the current namespace (see Medit and Ensight examples).
         */
        enum class Format
        {
            Begin = 0,
            Medit = Begin,
            Ensight,
            VTK_PolygonalData,
            End, // plays two roles: provide a default value in initialisation and may be used also to
            // stop a loop over all enum types.
            None = End

        };


    } // namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_FORMAT_HPP_
