### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh__Init.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
