/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_COMPUTE_COLORING_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_COMPUTE_COLORING_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Geometry/GeometricElt/GeometricElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Computes coloring for a \a mesh.
     *
     * The present algorithm attributes 'colors' to each GeometricElt so that two adjacent GeometricElt never
     * share the same color. As few colors as possible are used here.
     *
     * \param[in] mesh Mesh for which the coloring is computed.
     * \param[in] dimension Only geometric elements of a given dimension are considered here.
     * \param[out] Ngeometric_elt_with_color For each index/color, the number of geometric elements. For instance
     * the third element of the vector is the number of geometric elements with color 2.
     *
     * Beware: this algorithm acts processor-wise: if it is called after the reduction to processor-wise data,
     * it will actupon them alone and not on the whole initial object.
     *
     * \return The coloring attributed for each geometric elements.
     *
     * \internal <b><tt>[internal]</tt></b> Unordered map use the address and not the GeometricElt object in its hash
     * table.
     * \endinternal
     */
    std::unordered_map<GeometricElt::shared_ptr, std::size_t>
    ComputeColoring(const Mesh& mesh, std::size_t dimension, std::vector<std::size_t>& Ngeometric_elt_with_color);


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_COMPUTE_COLORING_HPP_
