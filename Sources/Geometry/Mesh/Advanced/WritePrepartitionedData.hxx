//! \file
//
//
//  WritePrepartitionedData.hxx
//  MoReFEM
//
//  Created by sebastien on 15/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"

#include <cassert>
#include <iosfwd>


namespace MoReFEM::Advanced::MeshNS
{


    inline const std::string& WritePrepartitionedData::GetReducedMeshFile() const noexcept
    {
        assert(!reduced_mesh_file_.empty());
        return reduced_mesh_file_;
    }


    inline const std::string& WritePrepartitionedData::GetPartitionDataFile() const noexcept
    {
        assert(!partition_data_file_.empty());
        return partition_data_file_;
    }


} // namespace MoReFEM::Advanced::MeshNS


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_
