//! \file
//
//
//  WritePrepartitionedData.cpp
//  MoReFEM
//
//  Created by sebastien on 15/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <sstream>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Advanced::MeshNS
{


    namespace // anonymous
    {


        template<::MoReFEM::MeshNS::Format FormatT>
        using Informations = ::MoReFEM::Internal::MeshNS::FormatNS::Informations<FormatT>;


        //! \param[in,out] reduced_mesh_path In input, the path to the reduced mesh file without its extension (the '.'
        //! however is already there. In
        //!  output, the extension is properly added.
        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormat(const Mesh& mesh, std::ofstream& out, std::string& reduced_mesh_path);


        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormatSpecificInformations(const Mesh& mesh, std::ofstream& out);

    } // namespace


    WritePrepartitionedData::WritePrepartitionedData(const Mesh& mesh,
                                                     const std::string& output_directory,
                                                     ::MoReFEM::MeshNS::Format format)
    {
        std::ostringstream oconv;

        oconv << output_directory << "/mesh_data.lua";
        partition_data_file_ = oconv.str();

        if (::MoReFEM::FilesystemNS::File::DoExist(partition_data_file_))
        {
            oconv.str("");
            oconv << "File " << partition_data_file_ << " already exists!";
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }

        std::ofstream out;
        ::MoReFEM::FilesystemNS::File::Create(out, partition_data_file_, __FILE__, __LINE__);

        oconv.str("");
        oconv << output_directory << "/mesh" << '.';
        reduced_mesh_file_ = oconv.str();

        using namespace ::MoReFEM::MeshNS;

        // Note: I could have made that more elegant by making the function template, but I didn't deem it that
        // useful.
        switch (format)
        {
        case ::MoReFEM::MeshNS::Format::Medit:
        {
            WriteFormat<::MoReFEM::MeshNS::Format::Medit>(mesh, out, reduced_mesh_file_);
            break;
        }
        case ::MoReFEM::MeshNS::Format::Ensight:
        {
            WriteFormat<::MoReFEM::MeshNS::Format::Ensight>(mesh, out, reduced_mesh_file_);
            break;
        }
        case ::MoReFEM::MeshNS::Format::VTK_PolygonalData:
        case ::MoReFEM::MeshNS::Format::End:
        {
            throw Exception("Format not supported for pre-partition.", __FILE__, __LINE__);
        }
        } // switch


        {
            out << "Nprocessor_wise_coord = " << mesh.NprocessorWiseCoord() << std::endl;
            out << "Nghost_coord = " << mesh.NghostCoord() << std::endl;
            out << "space_unit = " << mesh.GetSpaceUnit() << std::endl;
            out << "Nprocessor_wise_vertex = " << mesh.Nvertex<RoleOnProcessor::processor_wise>() << std::endl;
            out << "Nprogram_wise_vertex = " << mesh.NprogramWiseVertex() << std::endl;
            out << "Nprocessor_wise_edge = " << mesh.Nedge<RoleOnProcessor::processor_wise>() << std::endl;
            out << "Nprocessor_wise_face = " << mesh.Nface<RoleOnProcessor::processor_wise>() << std::endl;
            out << "Nprocessor_wise_volume = " << mesh.Nvolume<RoleOnProcessor::processor_wise>() << std::endl;
            out << "do_build_edges = " << (mesh.AreEdgesBuilt() ? "true" : "false") << std::endl;
            out << "do_build_faces = " << (mesh.AreFacesBuilt() ? "true" : "false") << std::endl;
            out << "do_build_volumes = " << (mesh.AreVolumesBuilt() ? "true" : "false") << std::endl;


            {
                decltype(auto) list = mesh.ComputeProcessorWiseAndGhostGeometricEltList();

                std::vector<std::size_t> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetIndex());
                }

                out << "\n-- List of the indexes of the GeometricElt (both the processor-wise and the ghost ones - the "
                       "processor-wise ones are written first)"
                    << std::endl;
                out << "geometric_elt_index_list = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << std::endl;
            }

            {
                decltype(auto) list = mesh.GetProcessorWiseCoordsList();

                std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetProgramWisePosition());
                }

                out << "\n-- The program wise indexes of the processor-wise Coords" << std::endl;
                out << "program_wise_coords_index_list_proc = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << std::endl;
            }

            {
                decltype(auto) list = mesh.GetGhostCoordsList();

                std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetProgramWisePosition());
                }

                out << "\n-- The program wise indexes of the ghost Coords" << std::endl;
                out << "program_wise_coords_index_list_ghost = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << std::endl << std::endl;
            }

            {
                Internal::MeshNS::ComputeInterfaceListInMesh original_interface_list(mesh);

                {
                    decltype(auto) list = original_interface_list.GetVertexList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nvertex (processor-wise + ghost) = " << index_list.size() << std::endl;
                    out << "vertex_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << std::endl << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetEdgeList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nedge (processor-wise + ghost) = " << index_list.size() << std::endl;
                    out << "edge_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << std::endl << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetFaceList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nface (processor-wise + ghost) = " << index_list.size() << std::endl;
                    out << "face_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << std::endl << std::endl;
                }

                {
                    decltype(auto) list = original_interface_list.GetVolumeList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nvolume (processor-wise + ghost) = " << index_list.size() << std::endl;
                    out << "volume_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << std::endl << std::endl;
                }
            }
        }
    }


    namespace // anonymous
    {


        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormat(const Mesh& mesh, std::ofstream& out, std::string& reduced_mesh_path)
        {
            WriteFormatSpecificInformations<FormatT>(mesh, out);

            reduced_mesh_path += Informations<FormatT>::Extension();
            mesh.Write<FormatT>(reduced_mesh_path);
        }


        template<>
        void WriteFormatSpecificInformations<::MoReFEM::MeshNS::Format::Medit>(const Mesh& mesh, std::ofstream& out)
        {
            out << "-- Medit needs to know how many geometric elements were processor-wise for each type to be "
                   "able to reconstruct properly from prepartitioned data - as the partial mesh used contains both "
                   "processor-wise and ghost data."
                << std::endl;

            out << "Nprocessor_wise_geom_elt_per_type = ";

            using lua_map_policy =
                Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

            decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();
            std::map<std::size_t, std::size_t> Nprocessor_wise_geom_elt_per_type;

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                const auto Nelt_of_type = mesh.NgeometricElt<RoleOnProcessor::processor_wise>(ref_geom_elt);

                auto [it, is_inserted] = Nprocessor_wise_geom_elt_per_type.insert(
                    std::pair(ref_geom_elt.GetMeditIdentifier(), Nelt_of_type));

                static_cast<void>(it);
                assert(is_inserted);
            }

            Utilities::PrintContainer<lua_map_policy>::Do(Nprocessor_wise_geom_elt_per_type,
                                                          out,
                                                          ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                          ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                          ::MoReFEM::PrintNS::Delimiter::closer(" }\n\n"));
        }


        template<>
        void WriteFormatSpecificInformations<::MoReFEM::MeshNS::Format::Ensight>(const Mesh& mesh, std::ofstream& out)
        {
            out << "-- Ensight needs to know how many geometric elements were processor-wise for each combination "
                   "type / mesh label to be able to reconstruct properly from prepartitioned data - as the partial "
                   "mesh "
                   "used contains both processor-wise and ghost data."
                << std::endl;

            out << "Nprocessor_wise_geom_elt_per_type_and_label = ";

            using lua_map_policy =
                Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

            decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t> Nprocessor_wise_geom_elt_per_type_and_label;

            decltype(auto) mesh_label_list = mesh.GetLabelList();

            for (const auto& mesh_label_ptr : mesh_label_list)
            {
                assert(!(!mesh_label_ptr));
                const auto& mesh_label = *mesh_label_ptr;

                for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
                {
                    assert(!(!ref_geom_elt_ptr));
                    const auto& ref_geom_elt = *ref_geom_elt_ptr;

                    const auto Nelt_of_type =
                        mesh.NgeometricElt<RoleOnProcessor::processor_wise>(ref_geom_elt, mesh_label);

                    if (Nelt_of_type == 0)
                        continue;

                    Internal::MeshNS::RefGeomEltAndMeshLabel ref_geom_elt_and_mesh_label(ref_geom_elt, mesh_label);

                    auto [it, is_inserted] = Nprocessor_wise_geom_elt_per_type_and_label.insert(
                        std::pair(ref_geom_elt_and_mesh_label, Nelt_of_type));

                    static_cast<void>(it);
                    assert(is_inserted);
                }
            }

            Utilities::PrintContainer<lua_map_policy>::Do(Nprocessor_wise_geom_elt_per_type_and_label,
                                                          out,
                                                          ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                          ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                          ::MoReFEM::PrintNS::Delimiter::closer(" }\n\n"));
        }


    } // namespace


} // namespace MoReFEM::Advanced::MeshNS
