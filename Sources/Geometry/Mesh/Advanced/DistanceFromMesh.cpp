/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 16 Jun 2016 16:11:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace MeshNS
        {


            namespace // anonymous
            {


                double DotProduct(const SpatialPoint& point1, const SpatialPoint& point2);

                void ComputeVectorP1P2(const SpatialPoint& point1, const SpatialPoint& point2, SpatialPoint& vector);


            } // namespace


            DistanceFromMesh::DistanceFromMesh()
            {
                point_on_plane_ = std::make_unique<SpatialPoint>();
                point_on_plane_point_ = std::make_unique<SpatialPoint>();
                point_close_ = std::make_unique<SpatialPoint>();
                point_point1_ = std::make_unique<SpatialPoint>();
                point1_point2_ = std::make_unique<SpatialPoint>();
                point1_point3_ = std::make_unique<SpatialPoint>();
                point1_point_on_plane_ = std::make_unique<SpatialPoint>();
                point3_proj_ = std::make_unique<SpatialPoint>();
                point_proj_ = std::make_unique<SpatialPoint>();
                point3_point3_proj_ = std::make_unique<SpatialPoint>();
                point_point_proj_ = std::make_unique<SpatialPoint>();
                point1_point_proj_ = std::make_unique<SpatialPoint>();
                point2_point_proj_ = std::make_unique<SpatialPoint>();
            }


            double DistanceFromMesh::ComputeDistance(const SpatialPoint& point,
                                                     const Mesh& mesh,
                                                     SpatialPoint& point_projection,
                                                     SpatialPoint& normal_on_point_projection)
            {
                if (mesh.GetDimension() != 3)
                    throw Exception("The distance function only works in 3D.", __FILE__, __LINE__);

                point_projection.Reset();
                normal_on_point_projection.Reset();

                decltype(auto) geometric_type = Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__)
                                                    .GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);

                auto subset = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(geometric_type);
                double absolute_distance = 0.;
                double distance = 0.;
                double signed_distance = -100.;
                std::size_t counter = 0;
                std::size_t region = 0;
                double dot2 = 0.;

                auto& point_on_plane = GetNonCstPointOnPlane();
                auto& point_close = GetNonCstPointClose();
                auto& point_on_plane_point = GetNonCstPointOnPlanePoint();
                auto& point_point1 = GetNonCstPointPoint1();

                point_on_plane.Reset();
                point_close.Reset();
                point_on_plane_point.Reset();
                point_point1.Reset();

                for (auto it = subset.first; it != subset.second; ++it)
                {
                    const auto& geom_elt_ptr = *it;
                    assert(!(!geom_elt_ptr));
                    const auto& geom_elt = *geom_elt_ptr;
                    assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);

                    const auto& geom_elt_coords_list = geom_elt.GetCoordsList();

                    const auto& face_list = geom_elt.GetOrientedFaceList();
                    const std::size_t face_list_size = face_list.size();

                    if (face_list_size > 1)
                        throw Exception(
                            "A triangle has more than one face. This should not happen.", __FILE__, __LINE__);

                    const auto& face = *face_list[0];

                    const auto& pseudo_normal_triangle_ptr = face.GetUnorientedInterface().GetPseudoNormalPtr();
                    const auto& pseudo_normal_triangle = *pseudo_normal_triangle_ptr;
                    assert(pseudo_normal_triangle.size() == 3);

                    const auto& edge_list = geom_elt.GetOrientedEdgeList();
                    const auto& vertex_list = geom_elt.GetVertexList();

                    assert(geom_elt_coords_list.size() == 3);
                    const auto& point1 = *geom_elt_coords_list[0];
                    const auto& point2 = *geom_elt_coords_list[1];
                    const auto& point3 = *geom_elt_coords_list[2];

                    ComputeVectorP1P2(point, point1, point_point1);

                    const double dot1 = point_point1[0] * pseudo_normal_triangle(0)
                                        + point_point1[1] * pseudo_normal_triangle(1)
                                        + point_point1[2] * pseudo_normal_triangle(2);

                    for (std::size_t i = 0; i < 3; ++i)
                        point_on_plane.GetNonCstValue(i) =
                            point[i] + dot1 * pseudo_normal_triangle(static_cast<int>(i));

                    ComputeVectorP1P2(point_on_plane, point, point_on_plane_point);

                    const double distance_proj = std::sqrt(DotProduct(point_on_plane_point, point_on_plane_point));

                    if ((counter == 0) || (distance_proj < absolute_distance))
                    {
                        region = 7;

                        /* distFromTriangle core function */
                        region =
                            DistanceFromTriangleCaseK(point_on_plane, point1, point2, point3, 1, region, point_close);
                        region =
                            DistanceFromTriangleCaseK(point_on_plane, point2, point3, point1, 2, region, point_close);
                        region =
                            DistanceFromTriangleCaseK(point_on_plane, point3, point1, point2, 3, region, point_close);

                        if (region == 7)
                        {
                            distance = 0.;

                            for (std::size_t i = 0; i < 3; ++i)
                            {
                                point_close.GetNonCstValue(i) = point_on_plane[i];
                            }
                        } else
                        {
                            if ((region == 1) || (region == 3) || (region == 5))
                            {
                                distance = std::sqrt(
                                    (point_on_plane[0] - point_close[0]) * (point_on_plane[0] - point_close[0])
                                    + (point_on_plane[1] - point_close[1]) * (point_on_plane[1] - point_close[1])
                                    + (point_on_plane[2] - point_close[2]) * (point_on_plane[2] - point_close[2]));
                            } else
                            {
                                distance = std::sqrt(
                                    (point_on_plane[0] - point_close[0]) * (point_on_plane[0] - point_close[0])
                                    + (point_on_plane[1] - point_close[1]) * (point_on_plane[1] - point_close[1])
                                    + (point_on_plane[2] - point_close[2]) * (point_on_plane[2] - point_close[2]));
                            }
                        }
                        /* distance computation */

                        distance = std::sqrt(distance_proj * distance_proj + distance * distance);

                        if ((counter == 0) || (distance < absolute_distance))
                        {
                            absolute_distance = distance;

                            dot2 = 0.;

                            if (region == 1)
                            {
                                // pcloseType = "Vertex 1";
                                const auto& vertex1 = *vertex_list[0];
                                assert(vertex1.GetVertexCoordsList().size() == 1);
                                assert(point1 == *vertex1.GetVertexCoordsList()[0]);
                                const auto& normal = vertex1.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 2)
                            {
                                // pcloseType = "Edge 2";
                                const auto& edge1 = edge_list[0]->GetUnorientedInterface();
                                assert(edge1.GetVertexCoordsList().size() == 2);
                                // assert(*edge1.GetVertexCoordsList()[0] == point1);
                                // assert(*edge1.GetVertexCoordsList()[1] == point2);
                                const auto& normal = edge1.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 3)
                            {
                                // pcloseType = "Vertex 3";
                                const auto& vertex2 = *vertex_list[1];
                                assert(vertex2.GetVertexCoordsList().size() == 1);
                                assert(point2 == *vertex2.GetVertexCoordsList()[0]);
                                const auto& normal = vertex2.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 4)
                            {
                                // pcloseType = "Edge 4";
                                const auto& edge2 = edge_list[1]->GetUnorientedInterface();
                                assert(edge2.GetVertexCoordsList().size() == 2);
                                // assert(*edge2.GetVertexCoordsList()[0] == point2);
                                // assert(*edge2.GetVertexCoordsList()[1] == point3);
                                const auto& normal = edge2.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 5)
                            {
                                // pcloseType = "Vertex 5";
                                const auto& vertex3 = *vertex_list[2];
                                assert(vertex3.GetVertexCoordsList().size() == 1);
                                assert(point3 == *vertex3.GetVertexCoordsList()[0]);
                                const auto& normal = vertex3.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 6)
                            {
                                // pcloseType = "Edge 6";
                                const auto& edge3 = edge_list[2]->GetUnorientedInterface();
                                assert(edge3.GetVertexCoordsList().size() == 2);
                                // assert(*edge3.GetVertexCoordsList()[0] == point1);
                                // assert(*edge3.GetVertexCoordsList()[1] == point3);
                                const auto& normal = edge3.GetPseudoNormal();
                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) = normal(static_cast<int>(i));
                                }
                            } else if (region == 7)
                            {
                                // pcloseType = "Triangle 7";

                                for (std::size_t i = 0; i < 3; ++i)
                                {
                                    normal_on_point_projection.GetNonCstValue(i) =
                                        pseudo_normal_triangle(static_cast<int>(i));
                                }
                            }

                            for (std::size_t i = 0; i < 3; ++i)
                            {
                                dot2 += (point[i] - point_close[i]) * normal_on_point_projection[i];
                            }

                            signed_distance = NumericNS::Sign(dot2) * distance;

                            for (std::size_t i = 0; i < 3; ++i)
                            {
                                point_projection.GetNonCstValue(i) = point_close[i];
                            }
                        }
                    }

                    ++counter;
                }

                return signed_distance;
            }


            std::size_t DistanceFromMesh::DistanceFromTriangleCaseK(const SpatialPoint& point_on_plane,
                                                                    const SpatialPoint& point1,
                                                                    const SpatialPoint& point2,
                                                                    const SpatialPoint& point3,
                                                                    const std::size_t k,
                                                                    std::size_t region,
                                                                    SpatialPoint& point_close)
            {
                auto& point1_point2 = GetNonCstPoint1Point2();
                auto& point1_point3 = GetNonCstPoint1Point3();
                auto& point1_point_on_plane = GetNonCstPoint1PointOnPlane();
                auto& point3_proj = GetNonCstPoint3Proj();
                auto& point_proj = GetNonCstPointProj();
                auto& point3_point3_proj = GetNonCstPoint3Point3Proj();
                auto& point_point_proj = GetNonCstPointPointProj();
                auto& point1_point_proj = GetNonCstPoint1PointProj();
                auto& point2_point_proj = GetNonCstPoint2PointProj();

                for (std::size_t i = 0; i < 3; ++i)
                {
                    point1_point2.GetNonCstValue(i) = point2[i] - point1[i];
                    point1_point3.GetNonCstValue(i) = point3[i] - point1[i];
                    point1_point_on_plane.GetNonCstValue(i) = point_on_plane[i] - point1[i];
                }

                const double norm_point1_point2_square = DotProduct(point1_point2, point1_point2);
                const double dot1 = DotProduct(point1_point2, point1_point3);
                const double dot2 = DotProduct(point1_point2, point1_point_on_plane);

                if (NumericNS::IsZero(norm_point1_point2_square))
                {
                    throw Exception("The norm of the vector Point1Point2 of triangle is zero and it should not happen "
                                    "if the triangle is well defined ie the two points are distinct.",
                                    __FILE__,
                                    __LINE__);
                }

                const double one_over_norm_point1_point2_square = 1. / norm_point1_point2_square;

                for (std::size_t i = 0; i < 3; ++i)
                {
                    point3_proj.GetNonCstValue(i) =
                        point1[i] + dot1 * one_over_norm_point1_point2_square * point1_point2[i];
                    point_proj.GetNonCstValue(i) =
                        point1[i] + dot2 * one_over_norm_point1_point2_square * point1_point2[i];
                    point3_point3_proj.GetNonCstValue(i) = point3_proj[i] - point3[i];
                    point_point_proj.GetNonCstValue(i) = point_proj[i] - point_on_plane[i];
                }

                if (!(NumericNS::Sign(DotProduct(point_point_proj, point3_point3_proj)) > 0.))
                {
                    ComputeVectorP1P2(point1, point_proj, point1_point_proj);

                    if (!(NumericNS::Sign(DotProduct(point1_point_proj, point1_point2)) > 0.))
                    {
                        region = std::min(region, 2 * k - 1);

                        for (std::size_t i = 0ul; i < 3; ++i)
                        {
                            point_close.GetNonCstValue(i) = point1[i];
                        }
                    } else
                    {
                        ComputeVectorP1P2(point2, point_proj, point2_point_proj);

                        if (!(NumericNS::Sign(DotProduct(point2_point_proj, point1_point2)) < 0.))
                        {
                            if (k == 3)
                            {
                                region = 1;
                            } else
                            {
                                region = 2 * k + 1;
                            }

                            for (std::size_t i = 0; i < 3; ++i)
                            {
                                point_close.GetNonCstValue(i) = point2[i];
                            }
                        } else
                        {
                            region = std::min(region, 2 * k);

                            for (std::size_t i = 0; i < 3; ++i)
                            {
                                point_close.GetNonCstValue(i) = point_proj[i];
                            }
                        }
                    }
                }

                return region;
            }


            namespace // anonymous
            {


                void ComputeVectorP1P2(const SpatialPoint& point1, const SpatialPoint& point2, SpatialPoint& vector)
                {
                    for (std::size_t i = 0; i < 3; ++i)
                    {
                        vector.GetNonCstValue(i) = point2[i] - point1[i];
                    }
                }

                double DotProduct(const SpatialPoint& point1, const SpatialPoint& point2)
                {
                    return (point1[0] * point2[0] + point1[1] * point2[1] + point1[2] * point2[2]);
                }


            } // namespace


        } // namespace MeshNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
