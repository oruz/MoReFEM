/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <sstream>
#include <type_traits>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/Mesh.hpp" // IWYU pragma: associated

#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"


namespace MoReFEM
{


    const std::string& Mesh::ClassName()
    {
        static std::string ret("Mesh");
        return ret;
    }


    Mesh::~Mesh() = default;


    Mesh::Mesh(const std::size_t unique_id,
               const std::string& mesh_file,
               std::size_t dimension,
               MeshNS::Format format,
               const double space_unit,
               std::optional<std::reference_wrapper<LuaOptionFile>> prepartitioned_data,
               BuildEdge do_build_edge,
               BuildFace do_build_face,
               BuildVolume do_build_volume,
               BuildPseudoNormals do_build_pseudo_normals)
    : unique_id_parent(unique_id), dimension_(dimension), space_unit_(space_unit), initial_format_(format)
    {
        GeometricElt::vector_shared_ptr unsort_processor_wise_geom_elt_list;
        GeometricElt::vector_shared_ptr unsort_ghost_geom_elt_list;
        Coords::vector_shared_ptr coords_list;
        MeshLabel::vector_const_shared_ptr mesh_label_list;

        Read(mesh_file,
             format,
             space_unit,
             prepartitioned_data,
             unsort_processor_wise_geom_elt_list,
             unsort_ghost_geom_elt_list,
             coords_list,
             mesh_label_list);

        Construct(std::move(unsort_processor_wise_geom_elt_list),
                  std::move(unsort_ghost_geom_elt_list),
                  std::move(coords_list),
                  std::move(mesh_label_list),
                  do_build_edge,
                  do_build_face,
                  do_build_volume,
                  do_build_pseudo_normals);
    }


    Mesh::Mesh(const std::size_t unique_id,
               std::size_t dimension,
               const double space_unit,
               GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
               GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
               Coords::vector_shared_ptr&& coords_list,
               MeshLabel::vector_const_shared_ptr&& mesh_label_list,
               BuildEdge do_build_edge,
               BuildFace do_build_face,
               BuildVolume do_build_volume,
               BuildPseudoNormals do_build_pseudo_normals)
    : unique_id_parent(unique_id), dimension_(dimension), space_unit_(space_unit)
    {
        Construct(std::move(unsort_processor_wise_geom_elt_list),
                  std::move(unsort_ghost_geom_elt_list),
                  std::move(coords_list),
                  std::move(mesh_label_list),
                  do_build_edge,
                  do_build_face,
                  do_build_volume,
                  do_build_pseudo_normals);
    }


    Mesh::Mesh(const Wrappers::Mpi& mpi,
               std::size_t unique_id,
               const std::string& mesh_file,
               LuaOptionFile& prepartitioned_data,
               std::size_t dimension,
               ::MoReFEM::MeshNS::Format format,
               const double space_unit,
               BuildEdge do_build_edge,
               BuildFace do_build_face,
               BuildVolume do_build_volume,
               BuildPseudoNormals do_build_pseudo_normals)
    : Mesh(unique_id,
           mesh_file,
           dimension,
           format,
           space_unit,
           std::make_optional(std::ref(prepartitioned_data)),
           do_build_edge,
           do_build_face,
           do_build_volume,
           do_build_pseudo_normals)
    {
        std::size_t Nprocessor_wise_coord{}, Nghost_coord{}, Nprocessor_wise_vertex{}, Nprocessor_wise_face{},
            Nprocessor_wise_edge{}, Nprocessor_wise_volume{};
        std::vector<std::size_t> geometric_elt_index_list;
        std::vector<std::size_t> processor_wise_coords_index_list;
        std::vector<std::size_t> ghost_coords_index_list;

        prepartitioned_data.Read("Nprocessor_wise_coord", "", Nprocessor_wise_coord, __FILE__, __LINE__);
        prepartitioned_data.Read("Nghost_coord", "", Nghost_coord, __FILE__, __LINE__);
        prepartitioned_data.Read("Nprocessor_wise_vertex", "", Nprocessor_wise_vertex, __FILE__, __LINE__);
        prepartitioned_data.Read("Nprocessor_wise_edge", "", Nprocessor_wise_edge, __FILE__, __LINE__);
        prepartitioned_data.Read("Nprocessor_wise_face", "", Nprocessor_wise_face, __FILE__, __LINE__);
        prepartitioned_data.Read("Nprocessor_wise_volume", "", Nprocessor_wise_volume, __FILE__, __LINE__);
        prepartitioned_data.Read("geometric_elt_index_list", "", geometric_elt_index_list, __FILE__, __LINE__);
        prepartitioned_data.Read(
            "program_wise_coords_index_list_proc", "", processor_wise_coords_index_list, __FILE__, __LINE__);
        prepartitioned_data.Read(
            "program_wise_coords_index_list_ghost", "", ghost_coords_index_list, __FILE__, __LINE__);

        // We intentionally overwrite the current value which is in fact false (as this constructor reads only a
        // part of the true mesh considered in the model).
        prepartitioned_data.Read("Nprogram_wise_vertex", "", Nprogram_wise_vertex_, __FILE__, __LINE__);

        {
            // Despite the name of the accessor, we read here all the \a Coords from the mesh file (which included
            // both processor-wise and ghost \a Coords - but not the a Coords irrelevant for current rank).
            decltype(auto) coords_list_in_mesh = GetProcessorWiseCoordsList();

            if (Nprocessor_wise_coord + Nghost_coord != coords_list_in_mesh.size())
            {
                std::ostringstream oconv;
                oconv << "Invalid preprocessed data for rank " << mpi.GetRank<int>()
                      << ": expected number of known "
                         "Coords from data was "
                      << Nprocessor_wise_coord + Nghost_coord << " but " << coords_list_in_mesh.size()
                      << " were actually loaded from mesh file.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            SortPrepartitionedCoords(Nprocessor_wise_coord);
        }

        {
            decltype(auto) processor_wise_geom_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();
            decltype(auto) ghost_geom_elt_list = GetGeometricEltList<RoleOnProcessor::ghost>();

            const auto proc_wise_size = processor_wise_geom_elt_list.size();
            const auto total_size = proc_wise_size + ghost_geom_elt_list.size();

            assert(total_size == geometric_elt_index_list.size());

            for (auto i = 0ul; i < proc_wise_size; ++i)
            {
                const auto& geom_elt_ptr = processor_wise_geom_elt_list[i];
                assert(!(!geom_elt_ptr));
                geom_elt_ptr->SetIndex(geometric_elt_index_list[i]);
            }

            for (auto i = proc_wise_size; i < total_size; ++i)
            {
                const auto& geom_elt_ptr = ghost_geom_elt_list[i - proc_wise_size];
                assert(!(!geom_elt_ptr));
                geom_elt_ptr->SetIndex(geometric_elt_index_list[i]);
            }
        }

        {
            ::MoReFEM::CoordsNS::processor_wise_position processor_wise_coords_index{ 0ul };
            {
                decltype(auto) list = GetProcessorWiseCoordsList();

                const auto size = list.size();
                assert(size == processor_wise_coords_index_list.size());

                for (auto i = 0ul; i < size; ++i)
                {
                    const auto& ptr = list[i];
                    assert(!(!ptr));

                    constexpr auto no = Internal::CoordsNS::DoCheckFirstCall::no;

                    ptr->SetProgramWisePosition<no>(
                        CoordsNS::program_wise_position(processor_wise_coords_index_list[i]));
                    ptr->SetProcessorWisePosition<no>(processor_wise_coords_index++);
                }
            }

            {
                decltype(auto) list = GetGhostCoordsList();

                const auto size = list.size();
                assert(size == ghost_coords_index_list.size());

                for (auto i = 0ul; i < size; ++i)
                {
                    const auto& ptr = list[i];
                    assert(!(!ptr));

                    constexpr auto no = Internal::CoordsNS::DoCheckFirstCall::no;

                    ptr->SetProgramWisePosition<no>(CoordsNS::program_wise_position(ghost_coords_index_list[i]));
                    ptr->SetProcessorWisePosition<no>(processor_wise_coords_index++);
                }
            }
        }

        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> prepartitioned_vertex_index_list;
        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> prepartitioned_edge_index_list;
        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> prepartitioned_face_index_list;
        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> prepartitioned_volume_index_list;

        prepartitioned_data.Read("vertex_index_list", "", prepartitioned_vertex_index_list, __FILE__, __LINE__);
        prepartitioned_data.Read("edge_index_list", "", prepartitioned_edge_index_list, __FILE__, __LINE__);
        prepartitioned_data.Read("face_index_list", "", prepartitioned_face_index_list, __FILE__, __LINE__);
        prepartitioned_data.Read("volume_index_list", "", prepartitioned_volume_index_list, __FILE__, __LINE__);

        {
            Internal::MeshNS::ComputeInterfaceListInMesh interface_list_helper(*this);

            {
                decltype(auto) list = interface_list_helper.GetVertexList();

                assert(list.size() == prepartitioned_vertex_index_list.size());

                auto it = prepartitioned_vertex_index_list.cbegin();

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    assert(it != prepartitioned_vertex_index_list.cend());

                    ptr->SetProgramWiseIndex<allow_reset_index::yes>(*it);
                    ++it;
                }
            }

            {
                decltype(auto) list = interface_list_helper.GetEdgeList();

                assert(list.size() == prepartitioned_edge_index_list.size());

                auto it = prepartitioned_edge_index_list.cbegin();

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    assert(it != prepartitioned_edge_index_list.cend());
                    ptr->SetProgramWiseIndex<allow_reset_index::yes>(*it);
                    ++it;
                }
            }

            {
                decltype(auto) list = interface_list_helper.GetFaceList();

                assert(list.size() == prepartitioned_face_index_list.size());

                auto it = prepartitioned_face_index_list.cbegin();

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    assert(it != prepartitioned_face_index_list.cend());
                    ptr->SetProgramWiseIndex<allow_reset_index::yes>(*it);
                    ++it;
                }
            }

            {
                decltype(auto) list = interface_list_helper.GetVolumeList();

                assert(list.size() == prepartitioned_volume_index_list.size());

                auto it = prepartitioned_volume_index_list.cbegin();

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    assert(it != prepartitioned_volume_index_list.cend());
                    ptr->SetProgramWiseIndex<allow_reset_index::yes>(*it);
                    ++it;
                }
            }
        }
    }


    void Mesh::Construct(GeometricElt::vector_shared_ptr&& processor_wise_geom_elt_list,
                         GeometricElt::vector_shared_ptr&& ghost_geom_elt_list,
                         Coords::vector_shared_ptr&& a_coords_list,
                         MeshLabel::vector_const_shared_ptr&& a_mesh_label_list,
                         BuildEdge do_build_edge,
                         BuildFace do_build_face,
                         BuildVolume do_build_volume,
                         BuildPseudoNormals do_build_pseudo_normals)
    {
        {
            auto& coords_list = GetNonCstProcessorWiseCoordsList();
            assert(coords_list.empty());
            coords_list = std::move(a_coords_list);

            CoordsNS::program_wise_position index{ 0ul };

            for (auto& coords_ptr : coords_list)
            {
                assert(!(!coords_ptr));
                coords_ptr->SetProgramWisePosition(index++);
            }
        }

        {
            auto& mesh_label_list = GetNonCstLabelList();
            assert(mesh_label_list.empty());
            mesh_label_list = std::move(a_mesh_label_list);

            std::sort(mesh_label_list.begin(),
                      mesh_label_list.end(),
                      Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());
        }

        processor_wise_geometric_elt_list_.Init(processor_wise_geom_elt_list, false);
        ghost_geometric_elt_list_.Init(ghost_geom_elt_list, false);

        Vertex::InterfaceMap vertex_interface_list;
        Edge::InterfaceMap edge_interface_list;
        Face::InterfaceMap face_interface_list;
        Volume::InterfaceMap volume_interface_list;

        BuildInterface<Vertex>(vertex_interface_list);
        Nprogram_wise_vertex_ = Nvertex<RoleOnProcessor::processor_wise>();

        if (do_build_edge == BuildEdge::yes)
            BuildInterface<Edge>(edge_interface_list);

        if (do_build_face == BuildFace::yes)
            BuildInterface<Face>(face_interface_list);

        if (do_build_volume == BuildVolume::yes)
            BuildInterface<Volume>(volume_interface_list);

        if (do_build_pseudo_normals == BuildPseudoNormals::yes)
        {
            assert(do_build_edge == BuildEdge::yes);
            assert(do_build_face == BuildFace::yes);

            // For now I take all the labels as we are still not able to really do it thanks to a given domain at this
            // point.
            std::vector<std::size_t> label_list_index;

            const auto& mesh_label_list = GetLabelList();

            const std::size_t mesh_label_list_size = mesh_label_list.size();

            assert(std::none_of(
                mesh_label_list.cbegin(), mesh_label_list.cend(), Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

            for (std::size_t i = 0ul; i < mesh_label_list_size; ++i)
            {
                const auto& mesh_label = *mesh_label_list[i];
                label_list_index.push_back(mesh_label.GetIndex());
            }

            ComputePseudoNormals(label_list_index, vertex_interface_list, edge_interface_list, face_interface_list);
        }
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
