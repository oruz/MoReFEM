/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <numeric>
#include <queue>
#include <unordered_map>
#include <utility>
#include <vector>
// IWYU pragma: no_include <__hash_table>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Miscellaneous.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/ComputeColoring.hpp"
#include "Geometry/Mesh/Internal/Coloring.hpp"

namespace MoReFEM
{
    class Mesh;
}


namespace MoReFEM
{


    std::unordered_map<GeometricElt::shared_ptr, std::size_t>
    ComputeColoring(const Mesh& mesh, std::size_t dimension, std::vector<std::size_t>& Ngeometric_elt_with_color)
    {
        std::unordered_map<GeometricElt::shared_ptr, std::size_t> ret;
        ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

        const auto& connectivity = Internal::ColoringNS::ComputeConnectivity(mesh, dimension);
        // Internal::ColoringNS::PrintConnectivity(connectivity);

        const std::size_t Ngeometric_elt_considered = connectivity.size();

        ret.reserve(Ngeometric_elt_considered);

        auto begin_connectivity = connectivity.cbegin();
        auto end_connectivity = connectivity.cend();

        using pair_type = std::pair<GeometricElt::shared_ptr, GeometricElt::vector_shared_ptr>;

        // Find the geometric elements with the higher connectivity (i.e. the highest number of neighbors).
        // If several, the first is taken and put into a FIFO.
        std::queue<GeometricElt::shared_ptr> fifo;

        {
            auto it = std::max_element(begin_connectivity,
                                       end_connectivity,
                                       [](const pair_type& lhs, const pair_type& rhs)
                                       {
                                           return lhs.second.size() < rhs.second.size();
                                       });
            assert(it != end_connectivity);

            fifo.push(it->first);
        }

        std::size_t Ncolor = 0ul;


        while (!fifo.empty())
        {
            auto current_geometric_elt_ptr = fifo.front();
            fifo.pop();

            // Check if the current element has already a color.
            auto it_color = ret.find(current_geometric_elt_ptr);

            // If current element has already a color, skip to next one.
            if (it_color != ret.cend())
                continue;

            // We hereby look at the neighbors to determine its color.
            // We try giving it the lowest indexed color, and increase it if a neighbor already got the same.
            // If we use up thus all the existing colors, a new one is created.
            {
                auto it_neighbors = connectivity.find(current_geometric_elt_ptr);
                assert(it_neighbors != end_connectivity);

                const auto& neighbor_list = it_neighbors->second;
                assert(std::none_of(
                    neighbor_list.cbegin(), neighbor_list.cend(), Utilities::IsNullptr<GeometricElt::shared_ptr>));

                std::vector<std::size_t> neighbor_color_list;

                // Iterate through all neighbors and fetch their color. If none, add the neighbor to the fifo.
                for (auto neighbor_ptr : neighbor_list)
                {
                    auto it_neighbor_color = ret.find(neighbor_ptr);

                    if (it_neighbor_color == ret.cend())
                        fifo.push(neighbor_ptr);
                    else
                        neighbor_color_list.push_back(it_neighbor_color->second);
                }

                Utilities::EliminateDuplicate(neighbor_color_list);

                auto neighbor_color_list_begin = neighbor_color_list.cbegin();
                auto neighbor_color_list_end = neighbor_color_list.cend();

                // Assign color to the current geometric elements, choosing the lowest possible value not already
                // used by one of its neighbors.
                std::size_t color = 0ul;

                while (std::binary_search(neighbor_color_list_begin, neighbor_color_list_end, color))
                    ++color;

                ret[current_geometric_elt_ptr] = color;

                assert(color <= Ncolor && "If new color, color == Ncolor.");

                if (color == Ncolor)
                {
                    Ngeometric_elt_with_color.push_back(1u);
                    ++Ncolor;
                } else
                {
                    assert(color < Ngeometric_elt_with_color.size());
                    ++Ngeometric_elt_with_color[color];
                }

                assert(Ncolor == Ngeometric_elt_with_color.size());
            }
        }

        assert(Ngeometric_elt_considered == ret.size());


        assert(std::accumulate(Ngeometric_elt_with_color.cbegin(), Ngeometric_elt_with_color.cend(), 0ul)
               == Ngeometric_elt_considered);


        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
