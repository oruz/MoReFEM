/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>

#include "Geometry/GeometricElt/GeometricElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::Utilities::PointerComparison { template <class T> struct Equal; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace ColoringNS
        {

            //! Convenient alias.
            using connecticity_helper_type =
                std::unordered_map<GeometricElt::shared_ptr,
                                   GeometricElt::vector_shared_ptr,
                                   std::hash<GeometricElt::shared_ptr>,
                                   Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>>;


            /*!
             * \brief Compute the connectivity of the geometric elements within the \a mesh.
             *
             * \param[in] mesh The mesh for which the connectivity is computed.
             * \param[in] dimension Consider only the geometric elements of this dimension.
             *
             * \return Key is each geometric element, value the list of contiguous geometric elements. Self connexion
             * is rejected here.
             */
            connecticity_helper_type ComputeConnectivity(const Mesh& mesh, std::size_t dimension);


            //! A function to print the results of the connectivity, very useful in debug mode.
            //! \param[in] connectivity The computed connectivity.
            void PrintConnectivity(const connecticity_helper_type& connectivity);


            /*!
             * \brief Compute for each vertex the list of \a GeometricElt of a given dimension to which it belongs to.
             *
             * \param[in] mesh \a Mesh in which the computation is performed.
             * \param[in] dimension Only geometric elements of this dimension are considered.
             *
             * \return Key is the \a Vertex, value the list of \a GeometricElt to which the \a Vertex belongs to.
             */
            std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
            ComputeGeometricElementForEachVertex(const Mesh& mesh, const std::size_t dimension);


        } // namespace ColoringNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/Coloring.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_
