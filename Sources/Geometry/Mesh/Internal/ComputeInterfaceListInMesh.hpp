/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 14:59:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_

#include <iosfwd>
#include <unordered_set>

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Edge.hpp"
#include "Geometry/Interfaces/Instances/Face.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"
#include "Geometry/Interfaces/Instances/Volume.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            //! Helper structure during construction of the \a ComputeInterfaceListInMesh object.
            struct InterfaceIndexListManager
            {
                //! Constructor.
                InterfaceIndexListManager();

                //! Storage for \a Vertex index.
                std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type> mesh_vertex_index_list;

                //! Storage for \a Edge index.
                std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type> mesh_edge_index_list;

                //! Storage for \a Face index.
                std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type> mesh_face_index_list;
            };


            /*!
             * \brief Compute the list of all interfaces to consider at the level of a \a Mesh.
             *
             * Natively there are no such list, as \a GeometricElt are in charge of storing pointers to their
             * references, but it's not a big deal to compute it.
             *
             * \attention The list of interfaces hence computed does not split interfaces between processor-wise and
             * ghost; so there is no guarantee there are actually a NodeBearer associated to the interface.
             */
            class ComputeInterfaceListInMesh final
            {
              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                //! \param[in] mesh Mesh for which the interfaces are built.
                explicit ComputeInterfaceListInMesh(const Mesh& mesh);

                //! Destructor.
                ~ComputeInterfaceListInMesh() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ComputeInterfaceListInMesh(const ComputeInterfaceListInMesh& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ComputeInterfaceListInMesh(ComputeInterfaceListInMesh&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ComputeInterfaceListInMesh& operator=(const ComputeInterfaceListInMesh& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ComputeInterfaceListInMesh& operator=(ComputeInterfaceListInMesh&& rhs) = delete;

                ///@}

              public:
                //! Print the list of interfaces in an output file (the whole purpose when this class was designed...).
                //! \copydoc doxygen_hide_stream_inout
                void Print(std::ostream& stream) const;

                //! Reorder the list of \a Coords in \a Edge and \a Face list.
                //! This should be called only after the \a Coords have been reindexed during the reduction process.
                void ReorderCoordsInEdgesAndFaces();

              public:
                //! List of all Vertices at mesh level.
                const Vertex::vector_shared_ptr& GetVertexList() const noexcept;

                //! List of all Edges at mesh level.
                const Edge::vector_shared_ptr& GetEdgeList() const noexcept;

                //! List of all Faces at mesh level.
                const Face::vector_shared_ptr& GetFaceList() const noexcept;

                //! List of all Volumes at mesh level.
                const Volume::vector_shared_ptr& GetVolumeList() const noexcept;

              private:
                //! List of all Edges at mesh level.
                Edge::vector_shared_ptr& GetNonCstEdgeList() noexcept;

                //! List of all Faces at mesh level.
                Face::vector_shared_ptr& GetNonCstFaceList() noexcept;

              private:
                /*!
                 * \brief Helper method to opulate the data in the class.
                 *
                 * This method should be used twice: once upon the processor-wise \a GeometricElt list, and another on
                 * the ghost one. The calls must eb done in that exact order (an interface found in both processor-wise
                 * and ghost \a GeometricElt MUST be counted as processor-wise).
                 *
                 * \param[in] geom_elt_list The \a GeometricElt list under scrutiny,
                 * \param[in,out] interface_index_list_manager An helper object to help during the construction.
                 */
                void ComputeForGeomEltList(const GeometricElt::vector_shared_ptr& geom_elt_list,
                                           InterfaceIndexListManager& interface_index_list_manager);

              private:
                //! List of all Vertices at mesh level.
                Vertex::vector_shared_ptr vertex_list_;

                //! List of all Edges at mesh level.
                Edge::vector_shared_ptr edge_list_;

                //! List of all Faces at mesh level.
                Face::vector_shared_ptr face_list_;

                //! List of all Volumes at mesh level.
                Volume::vector_shared_ptr volume_list_;
            };


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_
