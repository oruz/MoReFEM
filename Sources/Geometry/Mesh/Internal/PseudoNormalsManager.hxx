/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 13 Jun 2016 17:23:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        template<class PseudoNormalsSectionT>
        void PseudoNormalsManager::Create(const PseudoNormalsSectionT& section)
        {
            namespace ipl = Internal::InputDataNS;

            decltype(auto) mesh_index = ipl::ExtractLeaf<typename PseudoNormalsSectionT::MeshIndex>(section);
            decltype(auto) domain_index_list =
                ipl::ExtractLeaf<typename PseudoNormalsSectionT::DomainIndexList>(section);

            auto& mesh = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetNonCstMesh(mesh_index);

            Create(domain_index_list, mesh);
        }


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HXX_
