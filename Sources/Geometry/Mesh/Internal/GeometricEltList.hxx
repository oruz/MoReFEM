/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Apr 2013 14:56:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/GeometricEltList.hpp"
#include <cstddef> // IWYU pragma: keep


#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            inline std::size_t GeometricEltList::NgeometricElt() const
            {
                return data_.size();
            }


            inline const GeometricElt::vector_shared_ptr& GeometricEltList::All() const noexcept
            {
                return data_;
            }


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HXX_
