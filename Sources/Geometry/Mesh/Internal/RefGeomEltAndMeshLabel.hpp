//! \file
//
//
//  RefGeomEltAndMeshLabel.hpp
//  MoReFEM
//
//  Created by sebastien on 26/08/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HPP_

#include <iostream>
#include <string_view>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class MeshLabel;
    class RefGeomElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal::MeshNS
    {


        /*!
         * \brief An ad hoc objet to serve as key in some containers.
         *
         * This is useful to amalgamate \a RefGeomElt and \a MeshLabel into a single quantity that can be used as key in
         * an ordered container (this is needed for instance for Ensight format, for which we want to store for
         * prepartitioned data how many processor-wise \a GeometricElt are associated with a given \a RefGeomElt and \a
         * MeshLabel).
         */
        class RefGeomEltAndMeshLabel
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = RefGeomEltAndMeshLabel;

            //! With this declaration, the class yields true regarding IsString<RefGeomEltAndMeshLabel>::value.
            static constexpr bool identified_as_string = true;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] ref_geom_elt The \a RefGeomElt we want to refer with the created object.
             * \param[in] mesh_label The \a MeshLabel we want to refer with the created object.
             */
            explicit RefGeomEltAndMeshLabel(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label);

            /*!
             * \brief Constructor for deserialization.
             *
             * \param[in] identifier Identifier from which an object is to be created (typically this identifier should
             * have been generated through GetIdentifier() in an earlier run of MoReFEM).
             */
            explicit RefGeomEltAndMeshLabel(std::string_view identifier);

            //! Destructor.
            ~RefGeomEltAndMeshLabel() = default;

            //! \copydoc doxygen_hide_copy_constructor
            RefGeomEltAndMeshLabel(const RefGeomEltAndMeshLabel& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            RefGeomEltAndMeshLabel(RefGeomEltAndMeshLabel&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            RefGeomEltAndMeshLabel& operator=(const RefGeomEltAndMeshLabel& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            RefGeomEltAndMeshLabel& operator=(RefGeomEltAndMeshLabel&& rhs) = delete;

            ///@}

            //! Accessor to the generated identifier.
            const std::string& GetIdentifier() const noexcept;

          private:
            //! Identifier to use in the map.
            const std::string identifier_;
        };


        /*!
         * \copydoc doxygen_hide_std_stream_out_overload
         */
        std::ostream& operator<<(std::ostream& stream, const RefGeomEltAndMeshLabel& rhs);


        //! \copydoc doxygen_hide_operator_less
        bool operator<(const RefGeomEltAndMeshLabel& lhs, const RefGeomEltAndMeshLabel& rhs);


    } // namespace Internal::MeshNS


} // namespace MoReFEM


#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HPP_
