/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"


#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            inline const Vertex::vector_shared_ptr& ComputeInterfaceListInMesh::GetVertexList() const noexcept
            {
                return vertex_list_;
            }


            inline const Edge::vector_shared_ptr& ComputeInterfaceListInMesh::GetEdgeList() const noexcept
            {
                return edge_list_;
            }


            inline const Face::vector_shared_ptr& ComputeInterfaceListInMesh::GetFaceList() const noexcept
            {
                return face_list_;
            }


            inline const Volume::vector_shared_ptr& ComputeInterfaceListInMesh::GetVolumeList() const noexcept
            {
                return volume_list_;
            }


            inline Edge::vector_shared_ptr& ComputeInterfaceListInMesh::GetNonCstEdgeList() noexcept
            {
                return const_cast<Edge::vector_shared_ptr&>(GetEdgeList());
            }


            inline Face::vector_shared_ptr& ComputeInterfaceListInMesh::GetNonCstFaceList() noexcept
            {
                return const_cast<Face::vector_shared_ptr&>(GetFaceList());
            }


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HXX_
