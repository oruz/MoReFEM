/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Apr 2013 15:06:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Internal/Exceptions/GeometricEltList.hpp"
#include "Geometry/Mesh/Internal/GeometricEltList.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace // anonymous
            {


                /*!
                 * \brief Create the quick accessor containers.
                 *
                 * \param[out] geometric_elt_lookup_helper The attribute geometric_elt_lookup_helper_ of
                 * GeometricEltList. \param[out] dimension_accessor Key is the dimension, value is the pair <First
                 * index, number of elements>.
                 */
                void
                CreateQuickAccessor(const GeometricElt::vector_shared_ptr& geometric_elt_list,
                                    geom_elt_lookup_type& geometric_elt_lookup_helper,
                                    std::map<std::size_t, std::pair<std::size_t, std::size_t>>& dimension_accessor);


                /*!
                 * \brief Print the content of a Internal::MeshNS::geom_elt_lookup_type object.
                 *
                 * Obviously for debug purposes...
                 */
                void PrintGeometricEltLookupHelper(const geom_elt_lookup_type& object);


            } // namespace


            void GeometricEltList::Init(const GeometricElt::vector_shared_ptr& unsort_list, bool do_print)
            {
                // The role of this method is precisely to init them!
                assert(geometric_elt_lookup_helper_.empty());
                assert(data_.empty());
                assert(dimension_accessor_.empty());

                geometric_elt_lookup_helper_.max_load_factor(Utilities::DefaultMaxLoadFactor());

                if (!data_.empty())
                    throw ExceptionNS::GeometricEltList::InitNotCleared(__FILE__, __LINE__);

                // Put all the geometric elements into data_. Check in the process indexes are unique.
                {
                    std::vector<std::size_t> indexes;
                    const auto Ngeometric_element = unsort_list.size();
                    indexes.reserve(Ngeometric_element);

                    for (const auto& geometric_element : unsort_list)
                    {
                        assert(!(!geometric_element));
                        data_.push_back(geometric_element);
                        indexes.push_back(geometric_element->GetIndex());
                    }

                    std::sort(indexes.begin(), indexes.end());
                    auto logical_end = std::unique(indexes.begin(), indexes.end());

                    if (logical_end != indexes.end())
                        throw ExceptionNS::GeometricEltList::DuplicateInGeometricEltIndex(
                            Ngeometric_element,
                            static_cast<std::size_t>(logical_end - indexes.cbegin()),
                            __FILE__,
                            __LINE__);
                }

                // Sort them.
                namespace sc = GeometricEltNS::SortingCriterion;

                std::stable_sort(
                    data_.begin(),
                    data_.end(),
                    Utilities::Sort<GeometricElt::shared_ptr, sc::Dimension<>, sc::Type<>, sc::SurfaceRef<>>);


                // Now fill the attributes that will allow to fetch quickly elements.
                assert(geometric_elt_lookup_helper_.empty());
                CreateQuickAccessor(data_, geometric_elt_lookup_helper_, dimension_accessor_);

                if (do_print)
                    PrintGeometricEltLookupHelper(geometric_elt_lookup_helper_);
            }


            void GeometricEltList::Clear()
            {
                data_.clear();
                geometric_elt_lookup_helper_.clear();
                dimension_accessor_.clear();
            }


            std::pair<std::size_t, std::size_t>
            GeometricEltList ::GetLocationInEltList(const RefGeomElt& ref_geom_elt) const
            {
                auto geometric_elt_identifier = ref_geom_elt.GetIdentifier();

                const auto it = geometric_elt_lookup_helper_.find(geometric_elt_identifier);

                if (it == geometric_elt_lookup_helper_.cend())
                    return { 0, 0 };

                const auto& second_map = it->second;

                // The second (unordered) map stores for each label the first index and the number of elements.
                // Elts of the same type are assumed to be contiguous, so it is relatively straightforward to stack up
                // all the data available in look-up (the only trick is not to assume ordering!)

                std::size_t Nmatch_element(0ul); // number of geometric elements that match the identifier
                std::vector<std::size_t>
                    begin_index; // begin index for each label; the minimum of it is what is of interest.
                begin_index.reserve(second_map.size());

                for (const auto& item : second_map)
                {
                    const auto& for_current_label = item.second;
                    Nmatch_element += for_current_label.second;
                    begin_index.push_back(for_current_label.first);
                }

                assert(begin_index.size() == second_map.size());

                std::size_t first_index;
                {
                    const auto& it_min = std::min_element(begin_index.begin(), begin_index.end());
                    assert(it_min != begin_index.cend());
                    first_index = *it_min;
                }

                return std::make_pair(first_index, Nmatch_element);
            }


            const std::pair<std::size_t, std::size_t>&
            GeometricEltList::GetLocationInEltList(std::size_t dimension) const
            {
                const auto it = dimension_accessor_.find(dimension);

                if (it == dimension_accessor_.cend())
                    throw ExceptionNS::GeometricEltList::InvalidRequest(dimension, __FILE__, __LINE__);

                return it->second;
            }


            bool GeometricEltList::GetLocationInEltList(const RefGeomElt& ref_geom_elt,
                                                        std::size_t label_index,
                                                        std::pair<std::size_t, std::size_t>& out) const
            {
                auto geometric_elt_identifier = ref_geom_elt.GetIdentifier();

                const auto it = geometric_elt_lookup_helper_.find(geometric_elt_identifier);

                if (it == geometric_elt_lookup_helper_.cend())
                    throw ExceptionNS::GeometricEltList::InvalidRequest(ref_geom_elt, label_index, __FILE__, __LINE__);

                const auto& second_map = it->second;

                const auto it2 = second_map.find(label_index);

                if (it2 == second_map.cend())
                    return false;

                out = it2->second;
                return true;
            }


            const geom_elt_per_label_id_type&
            GeometricEltList::GetLocationInEltListPerLabel(const RefGeomElt& ref_geom_elt) const
            {
                auto geometric_elt_identifier = ref_geom_elt.GetIdentifier();

                const auto& it = geometric_elt_lookup_helper_.find(geometric_elt_identifier);

                if (it == geometric_elt_lookup_helper_.cend())
                    throw ExceptionNS::GeometricEltList::InvalidRequest(ref_geom_elt, __FILE__, __LINE__);

                return it->second;
            }


            RefGeomElt::vector_shared_ptr GeometricEltList::BagOfEltType() const
            {
                RefGeomElt::vector_shared_ptr ret;

                const auto& factory = Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__);

                for (const auto& item : geometric_elt_lookup_helper_)
                {
                    auto geo_type_ptr = factory.GetRefGeomEltPtr(item.first);
                    ret.push_back(geo_type_ptr);
                }

                return ret;
            }


            RefGeomElt::vector_shared_ptr GeometricEltList::BagOfEltType(std::size_t dimension) const
            {
                assert(dimension < 4 && "Dimension above 3 are meaningless!");

                RefGeomElt::vector_shared_ptr&& bag_all_dimensions = BagOfEltType();

                RefGeomElt::vector_shared_ptr ret;
                ret.reserve(bag_all_dimensions.size());

                for (auto type : bag_all_dimensions)
                {
                    if (type->GetDimension() == dimension)
                        ret.push_back(type);
                }

                return ret;
            }


            GeometricElt::vector_shared_ptr
            GeometricEltList ::GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const
            {
                GeometricElt::vector_shared_ptr ret;
                assert(!(!label));
                std::size_t label_index = label->GetIndex();


                for (auto per_ref_geom_elt : geometric_elt_lookup_helper_)
                {
                    const geom_elt_per_label_id_type& data_sort_by_label = per_ref_geom_elt.second; // alias

                    auto it = data_sort_by_label.find(label_index);

                    if (it == data_sort_by_label.cend())
                        continue;

                    std::size_t first_index = it->second.first;
                    std::size_t last_index = first_index + it->second.second - 1u;

                    assert(last_index < data_.size());

                    for (std::size_t i = first_index; i <= last_index; ++i)
                        ret.push_back(data_[i]);
                }

                return ret;
            }


            std::size_t GeometricEltList::NgeometricElt(std::size_t dimension) const
            {
                std::size_t ret = 0ul;

                const auto& factory = Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__);

                for (auto item : geometric_elt_lookup_helper_)
                {
                    const auto& geo_type = factory.GetRefGeomElt(item.first);

                    if (geo_type.GetDimension() != dimension)
                        continue;

                    for (auto label : item.second)
                        ret += label.second.second;
                }

                return ret;
            }


            const GeometricElt& GeometricEltList::GetGeometricEltFromIndex(std::size_t index) const
            {
                const auto& list = All();
                const auto end = list.cend();

                const auto it = std::find_if(list.cbegin(),
                                             end,
                                             [index](const auto& geom_elt_ptr)
                                             {
                                                 assert(!(!geom_elt_ptr));
                                                 return geom_elt_ptr->GetIndex() == index;
                                             });

                assert(it != end);
                assert(!(!(*it)));
                return *(*it);
            }


            const GeometricElt& GeometricEltList::GetGeometricEltFromIndex(std::size_t index,
                                                                           const RefGeomElt& ref_geom_elt) const
            {
                const auto pair = GetLocationInEltList(ref_geom_elt);

                const auto& list = All();

                using difference_type = decltype(list.cbegin())::difference_type;

                const auto first_index = static_cast<difference_type>(pair.first);
                const auto Nelt_of_type = static_cast<difference_type>(pair.second);

                assert(first_index + Nelt_of_type < static_cast<difference_type>(list.size()));

                const auto begin = list.cbegin() + first_index;
                const auto end = begin + Nelt_of_type;

                assert(std::is_sorted(begin, end));

                const auto it = std::find_if(list.cbegin(),
                                             end,
                                             [index](const auto& geom_elt_ptr)
                                             {
                                                 assert(!(!geom_elt_ptr));
                                                 return geom_elt_ptr->GetIndex() == index;
                                             });

                assert(it != end);
                assert(!(!(*it)));
                return *(*it);
            }


            subset_range SubsetRange(const std::pair<std::size_t, std::size_t>& pair,
                                     const Internal::MeshNS::GeometricEltList& geometric_elt_list)
            {
                std::size_t first_element, Nelt;
                std::tie(first_element, Nelt) = pair;

                const auto& vector_geometric_element = geometric_elt_list.All();
                assert(first_element + Nelt <= vector_geometric_element.size());

                using difference_type = iterator_geometric_element::difference_type;

                auto it_begin = vector_geometric_element.cbegin() + static_cast<difference_type>(first_element);

                auto it_end = it_begin + static_cast<difference_type>(Nelt);

                subset_range ret({ it_begin, it_end });

                return ret;
            }


            std::size_t GeometricEltList::NgeometricElt(const RefGeomElt& ref_geom_elt,
                                                        const MeshLabel& mesh_label) const
            {
                std::pair<std::size_t, std::size_t> location;

                auto are_some = GetLocationInEltList(ref_geom_elt, mesh_label.GetIndex(), location);

                if (!are_some)
                    return 0ul;

                return location.second;
            }


            namespace // anonymous
            {


                void CreateQuickAccessor(const GeometricElt::vector_shared_ptr& geometric_elt_list,
                                         geom_elt_lookup_type& lookup_helper,
                                         std::map<std::size_t, std::pair<std::size_t, std::size_t>>& dimension_accessor)
                {
                    const auto Ngeometric_elements = geometric_elt_list.size();

                    std::size_t begin_current_pair = NumericNS::UninitializedIndex<std::size_t>();
                    std::size_t Nelts_in_current_pair(0ul);
                    Advanced::GeometricEltEnum previous_identifier(Advanced::GeometricEltEnum::Undefined);
                    std::size_t previous_label = NumericNS::UninitializedIndex<std::size_t>();

                    std::unordered_map<std::size_t, std::pair<std::size_t, std::size_t>> current_geometric_elt_layout;
                    current_geometric_elt_layout.max_load_factor(Utilities::DefaultMaxLoadFactor());

                    std::size_t current_dimension = NumericNS::UninitializedIndex<std::size_t>();
                    std::size_t Ngeom_elt_of_dim = 0ul;
                    std::size_t first_index_geom_elt_current_dimension = 0ul;

                    for (std::size_t i = 0ul; i < Ngeometric_elements; ++i)
                    {
                        assert(geometric_elt_list[i]);
                        const GeometricElt& geometric_element = *geometric_elt_list[i];

                        if (geometric_element.GetDimension() != current_dimension)
                        {
                            if (Ngeom_elt_of_dim > 0ul)
                            {
                                assert(current_dimension != NumericNS::UninitializedIndex<std::size_t>());
                                assert(first_index_geom_elt_current_dimension
                                       != NumericNS::UninitializedIndex<std::size_t>());

                                const bool is_new_key =
                                    dimension_accessor
                                        .insert(std::make_pair(
                                            current_dimension,
                                            std::make_pair(first_index_geom_elt_current_dimension, Ngeom_elt_of_dim)))
                                        .second;

                                static_cast<void>(is_new_key);
                                assert(is_new_key
                                       && "If not, the first sorting criterion (dimension) is not respected!");

                                first_index_geom_elt_current_dimension = i;
                                Ngeom_elt_of_dim = 0ul;
                            }

                            current_dimension = geometric_element.GetDimension();
                        }

                        ++Ngeom_elt_of_dim;

                        // std::string name(geometric_element.GetName());
                        MeshLabel::const_shared_ptr label_ptr = geometric_element.GetMeshLabelPtr();
                        assert(!(!label_ptr));
                        Advanced::GeometricEltEnum identifier = geometric_element.GetIdentifier();

                        if (identifier != previous_identifier)
                        {
                            if (previous_identifier != Advanced::GeometricEltEnum::Undefined)
                            {
                                assert(begin_current_pair != NumericNS::UninitializedIndex<std::size_t>());

                                current_geometric_elt_layout.insert(std::make_pair(
                                    previous_label, std::make_pair(begin_current_pair, Nelts_in_current_pair)));
                                lookup_helper.insert(std::make_pair(previous_identifier, current_geometric_elt_layout));
                                current_geometric_elt_layout.clear();
                            }

                            previous_identifier = identifier;
                            begin_current_pair = i;
                            Nelts_in_current_pair = 1u;
                            previous_label = label_ptr->GetIndex();
                        } else
                        {
                            if (label_ptr->GetIndex() != previous_label)
                            {
                                assert(previous_label != NumericNS::UninitializedIndex<std::size_t>());
                                current_geometric_elt_layout.insert(std::make_pair(
                                    previous_label, std::make_pair(begin_current_pair, Nelts_in_current_pair)));

                                begin_current_pair = i;
                                Nelts_in_current_pair = 1u;
                                previous_label = label_ptr->GetIndex();
                            } else
                                ++Nelts_in_current_pair;
                        }
                    }


                    if (Ngeom_elt_of_dim > 0ul)
                    {
                        assert(current_dimension != NumericNS::UninitializedIndex<std::size_t>());
                        assert(first_index_geom_elt_current_dimension != NumericNS::UninitializedIndex<std::size_t>());

                        const bool is_new_key =
                            dimension_accessor
                                .insert(std::make_pair(
                                    current_dimension,
                                    std::make_pair(first_index_geom_elt_current_dimension, Ngeom_elt_of_dim)))
                                .second;

                        static_cast<void>(is_new_key);
                        assert(is_new_key && "If not, the first sorting criterion (dimension) is not respected!");
                    }


                    if (Nelts_in_current_pair)
                    {
                        current_geometric_elt_layout.insert(
                            std::make_pair(previous_label, std::make_pair(begin_current_pair, Nelts_in_current_pair)));
                        lookup_helper.insert(std::make_pair(previous_identifier, current_geometric_elt_layout));
                    }

                    // PrintGeometricEltLookupHelper(lookup_helper);
                }


                void PrintGeometricEltLookupHelper(const geom_elt_lookup_type& object)
                {
                    for (const auto& item1 : object)
                    {
                        std::cout << "For geometric element " << static_cast<std::size_t>(item1.first) << ':'
                                  << std::endl;

                        for (const auto& item2 : item1.second)
                        {
                            std::cout << '\t' << "For label " << item2.first << ':' << std::endl;

                            const auto& item3 = item2.second;
                            std::cout << "\t\t(First, Number) = (" << item3.first << ", " << item3.second << ')'
                                      << std::endl;
                        }
                    }
                }


            } // namespace


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
