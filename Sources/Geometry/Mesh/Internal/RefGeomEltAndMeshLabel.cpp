//! \file
//
//
//  RefGeomEltAndMeshLabel.cpp
//  MoReFEM
//
//  Created by sebastien on 26/08/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#include <sstream>
#include <string>
#include <string_view>

#include "Utilities/Type/StrongType/StrongType.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM::Internal::MeshNS
{


    namespace // anonymous
    {


        std::string GenerateIdentifier(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label)
        {
            std::ostringstream oconv;
            oconv << ref_geom_elt.GetName() << "_mesh_label_" << mesh_label.GetIndex();

            return oconv.str();
        }


    } // namespace


    RefGeomEltAndMeshLabel::RefGeomEltAndMeshLabel(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label)
    : identifier_(GenerateIdentifier(ref_geom_elt, mesh_label))
    { }


    RefGeomEltAndMeshLabel::RefGeomEltAndMeshLabel(std::string_view identifier) : identifier_(identifier)
    { }


    std::ostream& operator<<(std::ostream& stream, const RefGeomEltAndMeshLabel& ref_geom_elt_and_mesh_label)
    {
        stream << ref_geom_elt_and_mesh_label.GetIdentifier();
        return stream;
    }


    bool operator<(const RefGeomEltAndMeshLabel& lhs, const RefGeomEltAndMeshLabel& rhs)
    {
        return lhs.GetIdentifier() < rhs.GetIdentifier();
    }


} // namespace MoReFEM::Internal::MeshNS
