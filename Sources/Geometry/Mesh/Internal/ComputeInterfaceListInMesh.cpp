/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 14:59:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iterator>
#include <memory>
#include <ostream>
#include <type_traits>
#include <unordered_set>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace // anonymous
            {


                template<class InterfaceT>
                void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                          std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type>& set,
                                          typename InterfaceT::vector_shared_ptr& mesh_list);

                void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt,
                                                   Volume::vector_shared_ptr& mesh_volume_list);

                template<class InterfaceT>
                void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out);


                //! Extract the proper index for tagging a \a Coords object.
                ::MoReFEM::CoordsNS::program_wise_position ExtractIndex(const Coords& coords);


                //! Returns a copy of the list with \a Interface sort by increasing program-wise index.
                template<class InterfaceT>
                typename InterfaceT::vector_shared_ptr
                SortPerProgramWiseIndex(typename InterfaceT::vector_shared_ptr original);


            } // namespace


            ComputeInterfaceListInMesh::ComputeInterfaceListInMesh(const Mesh& mesh)
            {
                // The list of interfaces is not stored as such in mesh...
                vertex_list_.reserve(mesh.Nvertex<RoleOnProcessor::processor_wise>()
                                     + mesh.Nvertex<RoleOnProcessor::ghost>());
                edge_list_.reserve(mesh.Nedge<RoleOnProcessor::processor_wise>()
                                   + mesh.Nedge<RoleOnProcessor::ghost>());
                face_list_.reserve(mesh.Nface<RoleOnProcessor::processor_wise>()
                                   + mesh.Nface<RoleOnProcessor::ghost>());
                volume_list_.reserve(mesh.Nvolume<RoleOnProcessor::processor_wise>()
                                     + mesh.Nvolume<RoleOnProcessor::ghost>());

                std::unordered_set<std::size_t> mesh_vertex_index_list, mesh_edge_index_list, mesh_face_index_list;
                mesh_vertex_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_edge_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_face_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

                InterfaceIndexListManager index_list;

                ComputeForGeomEltList(mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>(), index_list);

                assert(vertex_list_.size() == mesh.Nvertex<RoleOnProcessor::processor_wise>());
                assert(edge_list_.size() == mesh.Nedge<RoleOnProcessor::processor_wise>());
                assert(face_list_.size() == mesh.Nface<RoleOnProcessor::processor_wise>());
                assert(volume_list_.size() == mesh.Nvolume<RoleOnProcessor::processor_wise>());

                ComputeForGeomEltList(mesh.GetGeometricEltList<RoleOnProcessor::ghost>(), index_list);

                assert(vertex_list_.size()
                       == mesh.Nvertex<RoleOnProcessor::processor_wise>() + mesh.Nvertex<RoleOnProcessor::ghost>());

                assert(edge_list_.size()
                       == mesh.Nedge<RoleOnProcessor::processor_wise>() + mesh.Nedge<RoleOnProcessor::ghost>());
                assert(face_list_.size()
                       == mesh.Nface<RoleOnProcessor::processor_wise>() + mesh.Nface<RoleOnProcessor::ghost>());
                assert(volume_list_.size()
                       == mesh.Nvolume<RoleOnProcessor::processor_wise>() + mesh.Nvolume<RoleOnProcessor::ghost>());
            }


            void
            ComputeInterfaceListInMesh::ComputeForGeomEltList(const GeometricElt::vector_shared_ptr& geom_elt_list,
                                                              InterfaceIndexListManager& interface_index_list_manager)
            {
                auto interface_without_orientation = [](const auto& oriented_interface_ptr)
                {
                    assert(!(!oriented_interface_ptr));
                    return oriented_interface_ptr->GetUnorientedInterfacePtr();
                };

                // Work variables. Should not lead to too much allocation: std::vector<>::clear() leaves the
                // capacity unchanged.
                Edge::vector_shared_ptr geom_elt_edge_list;
                Face::vector_shared_ptr geom_elt_face_list;


                for (const auto& geom_elt_ptr : geom_elt_list)
                {
                    assert(!(!geom_elt_ptr));

                    const auto& geom_elt = *geom_elt_ptr;
                    ComputeMeshLevelList<Vertex>(
                        geom_elt.GetVertexList(), interface_index_list_manager.mesh_vertex_index_list, vertex_list_);

                    geom_elt_edge_list.clear();
                    decltype(auto) oriented_edge_list = geom_elt.GetOrientedEdgeList();

                    std::transform(oriented_edge_list.cbegin(),
                                   oriented_edge_list.cend(),
                                   std::back_inserter(geom_elt_edge_list),
                                   interface_without_orientation);

                    ComputeMeshLevelList<Edge>(
                        geom_elt_edge_list, interface_index_list_manager.mesh_edge_index_list, edge_list_);

                    geom_elt_face_list.clear();
                    decltype(auto) oriented_face_list = geom_elt.GetOrientedFaceList();

                    std::transform(oriented_face_list.cbegin(),
                                   oriented_face_list.cend(),
                                   std::back_inserter(geom_elt_face_list),
                                   interface_without_orientation);

                    ComputeMeshLevelList<Face>(
                        geom_elt_face_list, interface_index_list_manager.mesh_face_index_list, face_list_);

                    ComputeMeshLevelListForVolume(geom_elt, volume_list_);
                }
            }


            void ComputeInterfaceListInMesh::Print(std::ostream& out) const
            {
                out << "# For each geometric interface, give the list of the Coords that delimits it.\n"
                       "# The index associated to the interface (e.g. 4 in 'Vertex 4') is the program-wise index "
                       "associated to "
                       "the interface.\n"
                       "# The indexes in the [] are Coords program-wise indexes, which are the position of the Coords "
                       "in the "
                       " Mesh object\n# regardless of the convention used in the original mesh format."
                    << std::endl;

                {
                    // The copy is not a mistake: for convenience we want to write the index by increasing order.
                    auto vertex_list = SortPerProgramWiseIndex<Vertex>(GetVertexList());

                    for (auto vertex_ptr : vertex_list)
                    {
                        assert(!(!vertex_ptr));
                        out << vertex_ptr->GetNature() << ' ' << vertex_ptr->GetProgramWiseIndex() << ";";

                        // Put it into a vector solely to ensure same print format as for the other interfaces.
                        const auto& vertex_coords_list = vertex_ptr->GetVertexCoordsList();
                        assert(vertex_coords_list.size() == 1ul);
                        assert(!(!vertex_coords_list.back()));

                        std::vector<::MoReFEM::CoordsNS::program_wise_position> index{ ExtractIndex(
                            *(vertex_coords_list.back())) };

                        Utilities::PrintContainer<>::Do(index, out);
                    }
                }

                {
                    PrintTypeOfInterface<Edge>(GetEdgeList(), out);
                    PrintTypeOfInterface<Face>(GetFaceList(), out);
                    PrintTypeOfInterface<Volume>(GetVolumeList(), out);
                }
            }


            InterfaceIndexListManager::InterfaceIndexListManager()
            {
                mesh_vertex_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_edge_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                mesh_face_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }


            void ComputeInterfaceListInMesh::ReorderCoordsInEdgesAndFaces()
            {
                decltype(auto) edge_list = GetNonCstEdgeList();

                for (const auto& edge_ptr : edge_list)
                {
                    assert(!(!edge_ptr));
                    Internal::InterfaceNS::OrderCoordsList(edge_ptr->GetNonCstVertexCoordsList());
                }

                decltype(auto) face_list = GetNonCstFaceList();

                for (const auto& face_ptr : face_list)
                {
                    assert(!(!face_ptr));
                    Internal::InterfaceNS::OrderCoordsList(face_ptr->GetNonCstVertexCoordsList());
                }
            }


            namespace // anonymous
            {


                template<class InterfaceT>
                void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                          std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type>& set,
                                          typename InterfaceT::vector_shared_ptr& mesh_list)

                {
                    for (const typename InterfaceT::shared_ptr& interface_ptr : geom_elt_interface_list)
                    {
                        assert(!(!interface_ptr));
                        const auto index = interface_ptr->GetProgramWiseIndex();

                        const auto it = set.find(index);

                        if (it == set.cend())
                        {
                            mesh_list.push_back(interface_ptr);
                            set.insert(index);
                        }
                    }
                }


                void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt,
                                                   Volume::vector_shared_ptr& mesh_volume_list)
                {
                    auto volume_ptr = geom_elt.GetVolumePtr();

                    if (!(!volume_ptr))
                        mesh_volume_list.push_back(volume_ptr);
                }


                template<class InterfaceT>
                void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list,
                                          std::ostream& out)
                {
                    auto sorted_interface_list = SortPerProgramWiseIndex<InterfaceT>(interface_list);

                    for (auto interface_ptr : sorted_interface_list)
                    {
                        assert(!(!interface_ptr));
                        const auto& interface = *interface_ptr;

                        out << interface.StaticNature() << ' ' << interface.GetProgramWiseIndex() << ";";

                        const auto& coords_list = interface.GetVertexCoordsList();
                        std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list(coords_list.size());

                        std::transform(coords_list.cbegin(),
                                       coords_list.cend(),
                                       index_list.begin(),
                                       [](const auto& coords_ptr)
                                       {
                                           assert(!(!coords_ptr));
                                           return ExtractIndex(*coords_ptr);
                                       });

                        std::sort(index_list.begin(), index_list.end());

                        Utilities::PrintContainer<>::Do(index_list, out);
                    }
                }


                ::MoReFEM::CoordsNS::program_wise_position ExtractIndex(const Coords& coords)
                {
                    return coords.GetProgramWisePosition();
                }


                template<class InterfaceT>
                typename InterfaceT::vector_shared_ptr
                SortPerProgramWiseIndex(typename InterfaceT::vector_shared_ptr interface_list)
                {
                    auto program_wise_comp = [](const auto& lhs, const auto& rhs)
                    {
                        assert(!(!lhs));
                        assert(!(!rhs));

                        return lhs->GetProgramWiseIndex() < rhs->GetProgramWiseIndex();
                    };

                    std::sort(interface_list.begin(), interface_list.end(), program_wise_comp);

                    return interface_list;
                }


            } // namespace


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
