/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iosfwd>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            MeshManager::~MeshManager() = default;


            const std::string& MeshManager::ClassName()
            {
                static std::string ret("MeshManager");
                return ret;
            }


            MeshManager::MeshManager()
            {
                storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }


            void MeshManager::Create(const std::size_t unique_id,
                                     const std::string& mesh_file,
                                     std::size_t dimension,
                                     ::MoReFEM::MeshNS::Format format,
                                     const double space_unit,
                                     Mesh::BuildEdge do_build_edge,
                                     Mesh::BuildFace do_build_face,
                                     Mesh::BuildVolume do_build_volume,
                                     Mesh::BuildPseudoNormals do_build_pseudo_normals,
                                     std::optional<std::reference_wrapper<LuaOptionFile>> prepartitioned_data)
            {
                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                Mesh* buf = new Mesh(unique_id,
                                     mesh_file,
                                     dimension,
                                     format,
                                     space_unit,
                                     prepartitioned_data,
                                     do_build_edge,
                                     do_build_face,
                                     do_build_volume,
                                     do_build_pseudo_normals);

                InsertMesh<is_unique_id_known::no>(buf);
            }


            void MeshManager ::Create(std::size_t dimension,
                                      const double space_unit,
                                      GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                                      GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                                      Coords::vector_shared_ptr&& coords_list,
                                      MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                                      Mesh::BuildEdge do_build_edge,
                                      Mesh::BuildFace do_build_face,
                                      Mesh::BuildVolume do_build_volume,
                                      Mesh::BuildPseudoNormals do_build_pseudo_normals)
            {
                if (unsort_processor_wise_geom_elt_list.empty())
                    throw Exception("A mesh must contain at least one GeometricElement", __FILE__, __LINE__);

                const auto& any_elt_ptr = unsort_processor_wise_geom_elt_list.back();
                assert(!(!any_elt_ptr));
                const auto unique_id = any_elt_ptr->GetMeshIdentifier();

                assert(std::all_of(unsort_processor_wise_geom_elt_list.cbegin(),
                                   unsort_processor_wise_geom_elt_list.cend(),
                                   [unique_id](const auto& elt_ptr)
                                   {
                                       assert(!(!elt_ptr));
                                       return elt_ptr->GetMeshIdentifier() == unique_id;
                                   }));

                assert(std::all_of(unsort_ghost_geom_elt_list.cbegin(),
                                   unsort_ghost_geom_elt_list.cend(),
                                   [unique_id](const auto& elt_ptr)
                                   {
                                       assert(!(!elt_ptr));
                                       return elt_ptr->GetMeshIdentifier() == unique_id;
                                   }));

                Mesh* buf = new Mesh(unique_id,
                                     dimension,
                                     space_unit,
                                     std::move(unsort_processor_wise_geom_elt_list),
                                     std::move(unsort_ghost_geom_elt_list),
                                     std::move(coords_list),
                                     std::move(mesh_label_list),
                                     do_build_edge,
                                     do_build_face,
                                     do_build_volume,
                                     do_build_pseudo_normals);

                InsertMesh<is_unique_id_known::yes>(buf);
            }


            void MeshManager::LoadFromPrepartitionedData(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                         std::size_t unique_id,
                                                         const std::string& mesh_file,
                                                         LuaOptionFile& prepartitioned_data,
                                                         std::size_t dimension,
                                                         ::MoReFEM::MeshNS::Format format)
            {
                double space_unit{};
                bool do_build_edges{}, do_build_faces{}, do_build_volumes{};

                prepartitioned_data.Read("space_unit", "", space_unit, __FILE__, __LINE__);
                prepartitioned_data.Read("do_build_edges", "", do_build_edges, __FILE__, __LINE__);
                prepartitioned_data.Read("do_build_faces", "", do_build_faces, __FILE__, __LINE__);
                prepartitioned_data.Read("do_build_volumes", "", do_build_volumes, __FILE__, __LINE__);

                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                Mesh* buf = new Mesh(mpi,
                                     unique_id,
                                     mesh_file,
                                     prepartitioned_data,
                                     dimension,
                                     format,
                                     space_unit,
                                     do_build_edges ? Mesh::BuildEdge::yes : Mesh::BuildEdge::no,
                                     do_build_faces ? Mesh::BuildFace::yes : Mesh::BuildFace::no,
                                     do_build_volumes ? Mesh::BuildVolume::yes : Mesh::BuildVolume::no);

                InsertMesh<is_unique_id_known::no>(buf);
            }


            const Mesh& MeshManager::GetMesh(std::size_t unique_id) const
            {
                decltype(auto) storage = GetStorage();
                auto it = storage.find(unique_id);

                assert(it != storage.cend());
                assert(!(!(it->second)));

                return *(it->second);
            }


            void WriteInterfaceListForEachMesh(
                const std::map<std::size_t, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&
                    mesh_output_directory_storage,
                std::string_view filename)
            {
                for (const auto& pair : mesh_output_directory_storage)
                    WriteInterfaceList(pair, filename);
            }


            std::size_t MeshManager::GenerateUniqueId()
            {
                auto& unique_id_list = GetNonCstUniqueIdList();

                std::size_t ret = 0ul;

                if (!unique_id_list.empty())
                    ret = *(unique_id_list.rbegin()) + 1u;

                unique_id_list.insert(ret);

                return ret;
            }


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
