/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Mesh/Internal/Coloring.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ColoringNS
        {


            namespace // anonymous
            {


                /*
                 * \brief Same as the namesake function in MoReFEM namespace, with different call arguments.
                 *
                 * This form is used in ComputeConnectivity(): the subset of GeometricElements has already been computed
                 * there and it would be a waste to perform againe xactly the same operation.
                 *
                 * \param[in] geometric_elt_range A pair of iterators that indicates which geometric elements must be
                 * considered. To go through all of them, use: \code for (auto it = geometric_elt_range.first; it !=
                 * geometric_elt_range.second; ++it) { ... } \endcode
                 */
                std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
                ComputeGeometricElementForEachVertex(const Mesh::subset_range& geometric_elt_range,
                                                     const std::size_t Nvertex);


            } // namespace


            std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
            ComputeGeometricElementForEachVertex(const Mesh& mesh, const std::size_t dimension)
            {
                auto geometric_elt_range = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(dimension);
                assert(geometric_elt_range.second > geometric_elt_range.first);

                return ComputeGeometricElementForEachVertex(geometric_elt_range,
                                                            mesh.Nvertex<RoleOnProcessor::processor_wise>());
            }


            connecticity_helper_type ComputeConnectivity(const Mesh& mesh, const std::size_t dimension)
            {
                connecticity_helper_type ret;
                ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

                auto geometric_elt_range = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(dimension);
                assert(geometric_elt_range.second > geometric_elt_range.first);
                const auto& end = geometric_elt_range.second;
                const auto Ngeometric_elt = static_cast<std::size_t>(end - geometric_elt_range.first);
                ret.reserve(Ngeometric_elt);

                // For each vertex, determine the list of related geometric elements.
                std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>&&
                    geometric_elt_list_per_vertex = ComputeGeometricElementForEachVertex(
                        geometric_elt_range, mesh.Nvertex<RoleOnProcessor::processor_wise>());


                // From this, deduce the expected returned value.
                {
                    for (auto it = geometric_elt_range.first; it != end; ++it)
                    {
                        auto geometric_elt_ptr = *it;
                        assert(!(!geometric_elt_ptr));
                        const auto& vertex_list = geometric_elt_ptr->GetVertexList();

                        GeometricElt::vector_shared_ptr buf;

                        for (const auto& vertex_ptr : vertex_list)
                        {
                            assert(!(!vertex_ptr));
                            auto it_in_vertex = geometric_elt_list_per_vertex.find(vertex_ptr);
                            assert(it_in_vertex != geometric_elt_list_per_vertex.cend());

                            std::copy_if(it_in_vertex->second.cbegin(),
                                         it_in_vertex->second.cend(),
                                         std::back_inserter(buf),
                                         [&geometric_elt_ptr](GeometricElt::shared_ptr current_geo_elt_ptr)
                                         {
                                             assert(!(!current_geo_elt_ptr));
                                             return current_geo_elt_ptr != geometric_elt_ptr;
                                         });
                        }

                        Utilities::EliminateDuplicate(buf,
                                                      Utilities::PointerComparison::Less<GeometricElt::shared_ptr>(),
                                                      Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>());


                        ret.insert({ geometric_elt_ptr, std::move(buf) });
                    }
                }

                assert(ret.size() == Ngeometric_elt);
                return ret;
            }


            void PrintConnectivity(const connecticity_helper_type& connectivity)
            {

                for (const auto& pair : connectivity)
                {
                    std::cout << pair.first->GetIndex() << '\t';
                    const auto& connected_geom_elt_list = pair.second;

                    std::vector<std::size_t> buf(connected_geom_elt_list.size());

                    std::transform(connected_geom_elt_list.cbegin(),
                                   connected_geom_elt_list.cend(),
                                   buf.begin(),
                                   [](const GeometricElt::shared_ptr& geometric_elt_ptr)
                                   {
                                       assert(!(!geometric_elt_ptr));
                                       return geometric_elt_ptr->GetIndex();
                                   });

                    Utilities::PrintContainer<>::Do(buf);
                }
            }


            namespace // anonymous
            {


                std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
                ComputeGeometricElementForEachVertex(const Mesh::subset_range& geometric_elt_range,
                                                     const std::size_t Nvertex)
                {
                    std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr> ret;

                    ret.max_load_factor(Utilities::DefaultMaxLoadFactor());
                    ret.reserve(Nvertex);
                    const auto& end = geometric_elt_range.second;
                    assert(geometric_elt_range.first < end);

                    for (auto it = geometric_elt_range.first; it != end; ++it)
                    {
                        const auto& geometric_elt_ptr = *it;
                        assert(geometric_elt_ptr != nullptr);

                        const auto& vertex_list = geometric_elt_ptr->GetVertexList();

                        for (const auto& vertex_ptr : vertex_list)
                        {
                            assert(!(!vertex_ptr));
                            auto it_in_vertex = ret.find(vertex_ptr);

                            if (it_in_vertex == ret.cend())
                                ret.insert({ vertex_ptr, { geometric_elt_ptr } });
                            else
                                it_in_vertex->second.push_back(geometric_elt_ptr);
                        }
                    }

                    return ret;
                }


            } // namespace


        } // namespace ColoringNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
