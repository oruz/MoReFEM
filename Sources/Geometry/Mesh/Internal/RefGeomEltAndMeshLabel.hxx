//! \file
//
//
//  RefGeomEltAndMeshLabel.hxx
//  MoReFEM
//
//  Created by sebastien on 26/08/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"

#include <iosfwd>


namespace MoReFEM::Internal::MeshNS
{


    inline const std::string& RefGeomEltAndMeshLabel::GetIdentifier() const noexcept
    {
        return identifier_;
    }


} // namespace MoReFEM::Internal::MeshNS


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_REF_GEOM_ELT_AND_MESH_LABEL_HXX_
