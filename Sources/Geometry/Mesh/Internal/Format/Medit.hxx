/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Medit.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Medit
                {


                } // namespace Medit


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HXX_
