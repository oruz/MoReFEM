/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Apr 2016 22:46:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_FORMAT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_FORMAT_HPP_

#include <iosfwd>
#include <type_traits>

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                /*!
                 * \brief Helper class to provide the informations related to a known format.
                 *
                 */
                template<::MoReFEM::MeshNS::Format TypeT>
                struct Informations
                { };


                /*!
                 * \brief Get the format associated to \a format_name.
                 *
                 * \param[in] format_name Name of the format (e.g. 'Medit'). This name must be provided by a method
                 * Name() of a specialization of \a Informations template class.
                 *
                 * \return Enum value associated to the format.
                 */
                ::MoReFEM::MeshNS::Format GetType(const std::string& format_name);


                /*!
                 * \brief Default behaviour for TypeT support: no support.
                 *
                 * By default TypeT is not supported by a GeometricElt; you must specialize this class to
                 * indicate TypeT is appropriate for a GeometricElt. The specialization must inherit from
                 * std::true_type, and the body gives the identifier in TypeT (Medit in the following example):
                 *
                 * \code
                 * template<>
                 * struct Medit<Advanced::GeometricEltEnum::Segment2> : public std::true_type
                 * {
                 * //! Medit code for this object
                 * static constexpr GmfKwdCod MeditId() { return GmfEdges; }
                 * };
                 *
                 * \endcode
                 *
                 */
                template<::MoReFEM::MeshNS::Format TypeT, Advanced::GeometricEltEnum NatureT>
                struct Support : public std::false_type
                { };


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/Format/Ensight.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_FORMAT_HPP_
