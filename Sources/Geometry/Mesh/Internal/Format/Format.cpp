/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Apr 2016 22:46:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
// IWYU pragma: no_include <__tree>
#include <map>
#include <sstream>
#include <string>
#include <utility>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace // anonymous
                {


                    /*!
                     * \brief Recursive function to generate the mapping between format names and the enum class.
                     */
                    template<::MoReFEM::MeshNS::Format CurrentType>
                    void GenerateMappingImpl(std::map<std::string, ::MoReFEM::MeshNS::Format>& name_mapping)
                    {
                        using TypeIterator = Utilities::EnumClass::Iterator<::MoReFEM::MeshNS::Format, CurrentType>;
                        auto ret = name_mapping.insert({ Informations<CurrentType>::Name(), CurrentType });

                        assert(ret.second && "No name should be present twice!");
                        static_cast<void>(ret); // avoid warning in release mode.

                        GenerateMappingImpl<TypeIterator::Increment()>(name_mapping);
                    }


                    //! End the recursion.
                    template<>
                    void GenerateMappingImpl<::MoReFEM::MeshNS::Format::End>(
                        std::map<std::string, ::MoReFEM::MeshNS::Format>&)
                    { }


                    /*!
                     * \brief Generate the mapping between format names and the enum class.
                     */
                    std::map<std::string, ::MoReFEM::MeshNS::Format> GenerateMapping()
                    {
                        std::map<std::string, ::MoReFEM::MeshNS::Format> ret;
                        GenerateMappingImpl<::MoReFEM::MeshNS::Format::Begin>(ret);
                        return ret;
                    }


                } // namespace


                ::MoReFEM::MeshNS::Format GetType(const std::string& format_name)
                {
                    static auto mapping = GenerateMapping();

                    auto it = mapping.find(format_name);

                    if (it == mapping.cend())
                    {
                        std::ostringstream oconv;

                        oconv << "No type matching format name " << format_name
                              << " were found. The possible values are: " << std::endl;
                        Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(
                            mapping,
                            oconv,
                            ::MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                            ::MoReFEM::PrintNS::Delimiter::opener("\t- "),
                            ::MoReFEM::PrintNS::Delimiter::closer("\n"));

                        throw MoReFEM::Exception(oconv.str(), __FILE__, __LINE__);
                    }

                    return it->second;
                }


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
