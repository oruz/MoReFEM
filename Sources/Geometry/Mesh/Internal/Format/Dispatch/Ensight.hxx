/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Dispatch/Ensight.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Ensight
                {


                    namespace Dispatch
                    {


                    } // namespace Dispatch


                } // namespace Ensight


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HXX_
