/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 22:11:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HPP_

#include <memory>
#include <vector>

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace Ensight
                {


                    namespace Dispatch
                    {


                        // ============================
                        //! \cond IGNORE_BLOCK_IN_DOXYGEN
                        // ============================


                        template<class GeoRefEltT>
                        [[noreturn]] const std::string& GetName(std::false_type);

                        template<class GeoRefEltT>
                        const std::string& GetName(std::true_type);


                        template<class GeoRefEltT, unsigned int NcoordT>
                        void WriteFormat(std::true_type,
                                         std::ostream& stream,
                                         bool do_print_index,
                                         unsigned int index,
                                         const Coords::vector_raw_ptr& coords_list);

                        template<class GeoRefEltT, unsigned int NcoordT>
                        [[noreturn]] void WriteFormat(std::false_type,
                                                      std::ostream& stream,
                                                      bool do_print_index,
                                                      unsigned int index,
                                                      const Coords::vector_raw_ptr& coords_list);


                        // ============================
                        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                        // ============================


                    } // namespace Dispatch


                } // namespace Ensight


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/Format/Dispatch/Ensight.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_DISPATCH_x_ENSIGHT_HPP_
