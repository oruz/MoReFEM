/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 17 Aug 2016 11:17:47 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                namespace VTK_PolygonalData
                {


                } // namespace VTK_PolygonalData


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HXX_
