/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_

#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            //! Generic exception.
            class Exception : public MoReFEM::Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! \copydoc doxygen_hide_copy_constructor
                Exception(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Exception(Exception&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Exception& operator=(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Exception& operator=(Exception&& rhs) = default;
            };


            //! Thrown when mesh file couldn't be opened.
            class UnableToOpenFile final : public Exception
            {

              public:
                /*!
                 * \brief Constructor
                 *
                 * \param[in] mesh_file Mesh file being read
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnableToOpenFile(const std::string& mesh_file, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~UnableToOpenFile() override;

                //! \copydoc doxygen_hide_copy_constructor
                UnableToOpenFile(const UnableToOpenFile& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                UnableToOpenFile(UnableToOpenFile&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                UnableToOpenFile& operator=(const UnableToOpenFile& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                UnableToOpenFile& operator=(UnableToOpenFile&& rhs) = default;
            };


            //! Called when there is an attempt to write in Medit format a geometric elementtype not supported
            class UnsupportedGeometricElt final : public Exception
            {

              public:
                /*!
                 * \brief Constructor
                 *
                 * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered
                 (eg 'Triangle3')
                 * \param[in] format Name of the format which doesn't support the element (e.g. 'Ensight')
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnsupportedGeometricElt(const std::string& geometric_elt_identifier,
                                                 const std::string& format,
                                                 const char* invoking_file,
                                                 int invoking_line);

                //! Destructor
                virtual ~UnsupportedGeometricElt() override;

                //! \copydoc doxygen_hide_copy_constructor
                UnsupportedGeometricElt(const UnsupportedGeometricElt& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                UnsupportedGeometricElt(UnsupportedGeometricElt&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                UnsupportedGeometricElt& operator=(const UnsupportedGeometricElt& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                UnsupportedGeometricElt& operator=(UnsupportedGeometricElt&& rhs) = default;
            };


        } // namespace Format


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_
