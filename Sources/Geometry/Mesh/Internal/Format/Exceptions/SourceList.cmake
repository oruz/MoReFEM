### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Format.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Medit.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Format_fwd.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Medit.hpp"
)

