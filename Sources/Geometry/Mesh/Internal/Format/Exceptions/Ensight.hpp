/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

// IWYU pragma: private,  include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            namespace Ensight
            {


                /*!
                 * \brief Thrown if third or fourth line is not what is expected
                 *
                 * Expected format is "XXX id YYY" where XXX is 'node' or 'element' and YYY is among
                 * <off/given/assign/ignore>
                 */
                class InvalidThirdOrFourthLine final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] line_number 3 or 4
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidThirdOrFourthLine(const std::string& ensight_file,
                                                      std::size_t line_number,
                                                      const char* invoking_file,
                                                      int invoking_line);

                    //! Destructor
                    virtual ~InvalidThirdOrFourthLine() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidThirdOrFourthLine(const InvalidThirdOrFourthLine& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidThirdOrFourthLine(InvalidThirdOrFourthLine&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidThirdOrFourthLine& operator=(const InvalidThirdOrFourthLine& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidThirdOrFourthLine& operator=(InvalidThirdOrFourthLine&& rhs) = default;
                };


                /*!
                 * \brief Thrown when the announced number of coords has not been retrieved.
                 */
                class InvalidCoords final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] expected_number Number of coords plainly written in the Ensight file
                     * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidCoords(const std::string& ensight_file,
                                           std::size_t expected_number,
                                           std::size_t number_found,
                                           const char* invoking_file,
                                           int invoking_line);

                    //! Destructor
                    virtual ~InvalidCoords() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidCoords(const InvalidCoords& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidCoords(InvalidCoords&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidCoords& operator=(const InvalidCoords& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidCoords& operator=(InvalidCoords&& rhs) = default;
                };

                /*!
                 * \brief Thrown when the announced number of geometric elements has not been retrieved.
                 */
                class BadNumberOfEltsInLabel final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] index Index of the block considered (the 'part ###' bit in the Ensign=ht file)
                     * \param[in] description Name associated to the label considered.
                     * \param[in] expected_number Number of coords plainly written in the Ensight file
                     * \param[in] geometric_elt_name Name of the geometric element being currently read.
                     * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit BadNumberOfEltsInLabel(
                        const std::string& ensight_file,
                        std::size_t index,
                        const std::string& description,
                        const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                        std::size_t expected_number,
                        std::size_t number_found,
                        const char* invoking_file,
                        int invoking_line);

                    //! Destructor
                    virtual ~BadNumberOfEltsInLabel() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    BadNumberOfEltsInLabel(const BadNumberOfEltsInLabel& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    BadNumberOfEltsInLabel(BadNumberOfEltsInLabel&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    BadNumberOfEltsInLabel& operator=(const BadNumberOfEltsInLabel& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    BadNumberOfEltsInLabel& operator=(BadNumberOfEltsInLabel&& rhs) = default;
                };


                /*!
                 * \brief Thrown when a block doesn't begin with the expected string
                 */
                class InvalidLabelBlock final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] ensight_file Ensight file being read\
                     * \param[in] stringRead String read where "part" was expected
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidLabelBlock(const std::string& ensight_file,
                                               const std::string& stringRead,
                                               const char* invoking_file,
                                               int invoking_line);

                    //! Destructor
                    virtual ~InvalidLabelBlock() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidLabelBlock(const InvalidLabelBlock& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidLabelBlock(InvalidLabelBlock&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidLabelBlock& operator=(const InvalidLabelBlock& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidLabelBlock& operator=(InvalidLabelBlock&& rhs) = default;
                };


                /*!
                 * \brief Thrown when number of geometric elements can't be read properly
                 */
                class InvalidNumberOfGeometricElts final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] ensight_file Ensight file being read
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidNumberOfGeometricElts(const std::string& ensight_file,
                                                          const char* invoking_file,
                                                          int invoking_line);

                    //! Destructor
                    virtual ~InvalidNumberOfGeometricElts() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidNumberOfGeometricElts(const InvalidNumberOfGeometricElts& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidNumberOfGeometricElts(InvalidNumberOfGeometricElts&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidNumberOfGeometricElts& operator=(const InvalidNumberOfGeometricElts& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidNumberOfGeometricElts& operator=(InvalidNumberOfGeometricElts&& rhs) = default;
                };


            } // namespace Ensight


        } // namespace Format


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_ENSIGHT_HPP_
