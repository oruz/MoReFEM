/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <sstream>

// IWYU pragma: no_include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Ensight.hpp" // IWYU pragma: associated


namespace // anonymous
{

    // Declaration here; definitions at the end of the file
    std::string
    InvalidCoordsMsg(const std::string& ensight_file, std::size_t expected_number, std::size_t number_found);
    std::string InvalidThirdOrFourthLineMsg(const std::string& ensight_file, std::size_t line_number);
    std::string BadNumberOfEltsInLabelMsg(const std::string& ensight_file,
                                          std::size_t index,
                                          const std::string& description,
                                          const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                          std::size_t expected_number,
                                          std::size_t number_found);
    std::string InvalidLabelBlockMsg(const std::string& ensight_file, const std::string& string_read);
    std::string InvalidNumberOfGeometricEltsMsg(const std::string& ensight_file);

} // namespace


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            namespace Ensight
            {

                InvalidThirdOrFourthLine::~InvalidThirdOrFourthLine() = default;


                InvalidThirdOrFourthLine::InvalidThirdOrFourthLine(const std::string& ensight_file,
                                                                   std::size_t line_number,
                                                                   const char* invoking_file,
                                                                   int invoking_line)
                : Exception(InvalidThirdOrFourthLineMsg(ensight_file, line_number), invoking_file, invoking_line)
                { }


                InvalidCoords::~InvalidCoords() = default;


                InvalidCoords::InvalidCoords(const std::string& ensight_file,
                                             std::size_t expected_number,
                                             std::size_t number_found,
                                             const char* invoking_file,
                                             int invoking_line)
                : Exception(InvalidCoordsMsg(ensight_file, expected_number, number_found), invoking_file, invoking_line)
                { }


                BadNumberOfEltsInLabel::~BadNumberOfEltsInLabel() = default;


                BadNumberOfEltsInLabel::BadNumberOfEltsInLabel(
                    const std::string& ensight_file,
                    std::size_t index,
                    const std::string& description,
                    const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                    std::size_t expected_number,
                    std::size_t number_found,
                    const char* invoking_file,
                    int invoking_line)
                : Exception(BadNumberOfEltsInLabelMsg(ensight_file,
                                                      index,
                                                      description,
                                                      geometric_elt_name,
                                                      expected_number,
                                                      number_found),
                            invoking_file,
                            invoking_line)
                { }


                InvalidLabelBlock::~InvalidLabelBlock() = default;


                InvalidLabelBlock::InvalidLabelBlock(const std::string& ensight_file,
                                                     const std::string& string_read,
                                                     const char* invoking_file,
                                                     int invoking_line)
                : Exception(InvalidLabelBlockMsg(ensight_file, string_read), invoking_file, invoking_line)
                { }


                InvalidNumberOfGeometricElts::~InvalidNumberOfGeometricElts() = default;


                InvalidNumberOfGeometricElts::InvalidNumberOfGeometricElts(const std::string& ensight_file,
                                                                           const char* invoking_file,
                                                                           int invoking_line)
                : Exception(InvalidNumberOfGeometricEltsMsg(ensight_file), invoking_file, invoking_line)
                { }


            } // namespace Ensight


        } // namespace Format


    } // namespace ExceptionNS


} // namespace MoReFEM


namespace // anonymous
{


    std::string FileInformation(const std::string& ensight_file)
    {
        std::ostringstream oconv;
        oconv << "Error in Ensight file ";
        oconv << ensight_file << ": ";
        return oconv.str();
    }


    std::string InvalidCoordsMsg(const std::string& ensight_file, std::size_t expected_number, std::size_t number_found)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "there were " << expected_number << " coords announced in the Ensight file but " << number_found
              << " were actually found.";
        return oconv.str();
    }


    std::string InvalidThirdOrFourthLineMsg(const std::string& ensight_file, std::size_t line_number)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);

        switch (line_number)
        {
        case 3:
            oconv << "invalid third line: expected format was 'node id *status*'";
            break;
        case 4:
            oconv << "invalid fourth line: expected format was 'element id *status*'";
            break;
        default:
            // Should never occur: if so this is clearly a bug
            std::cerr << "Line number should be 3 or 4, not " << line_number << std::endl;
            assert(false);
        }

        oconv << " where status is among 'off', 'given', 'assign' or 'ignore'";
        return oconv.str();
    }


    std::string BadNumberOfEltsInLabelMsg(const std::string& ensight_file,
                                          std::size_t index,
                                          const std::string& description,
                                          const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                          std::size_t expected_number,
                                          std::size_t number_found)
    {
        std::ostringstream oconv;
        oconv << FileInformation((ensight_file));
        oconv << "there were " << expected_number << ' ' << geometric_elt_name
              << " announced in the Ensight file for label " << description << " (index = " << index << ") but "
              << number_found << " were actually found.";
        return oconv.str();
    }


    std::string InvalidLabelBlockMsg(const std::string& ensight_file, const std::string& string_read)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "Label block is expected to begin with a line 'part ###'";
        oconv << "; instead the string '" << string_read << " 'was read";
        return oconv.str();
    }


    std::string InvalidNumberOfGeometricEltsMsg(const std::string& ensight_file)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "Number of geometric elements expected in current label couldn't be read properly";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup GeometryGroup
