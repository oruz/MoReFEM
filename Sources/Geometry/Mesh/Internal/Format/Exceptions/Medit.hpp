/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_MEDIT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_MEDIT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            namespace Medit
            {


                //! Thrown when the file was found but couldn't be interpreted as a Medit file.
                class UnableToOpen final : public Exception
                {

                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] medit_filename Medit file being read
                     * \param[in] mesh_version Libmesh5 format.
                     * Libmesh5 handles 3 different formats:
                     *    - '1' supports only floats
                     *    - '2' throws in doubles
                     *    - '3' removes the 2Go cap for a given file
                     * The version has been read by dedicated libmesh function (or not if the file doesn't exist...).
                     * It is used because the version may explain some reading failure
                     * \param[in] action Either "read" or "write" expected.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit UnableToOpen(const std::string& medit_filename,
                                          int mesh_version,
                                          const std::string& action,
                                          const char* invoking_file,
                                          int invoking_line);

                    //! Destructor
                    virtual ~UnableToOpen() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    UnableToOpen(const UnableToOpen& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    UnableToOpen(UnableToOpen&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    UnableToOpen& operator=(const UnableToOpen& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    UnableToOpen& operator=(UnableToOpen&& rhs) = default;
                };


                //! Thrown when file extension is invalid.
                class InvalidExtension final : public Exception
                {

                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] medit_filename Medit file being read
                     * \param[in] action Either "read" or "write" expected.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidExtension(const std::string& medit_filename,
                                              const std::string& action,
                                              const char* invoking_file,
                                              int invoking_line);

                    //! Destructor
                    virtual ~InvalidExtension() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidExtension(const InvalidExtension& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidExtension(InvalidExtension&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidExtension& operator=(const InvalidExtension& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidExtension& operator=(InvalidExtension&& rhs) = default;
                };


                //! Thrown when the path doesn't exist.
                class InvalidPath final : public Exception
                {

                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] medit_filename Medit file being read
                     * \param[in] action Either "read" or "write" expected.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit InvalidPath(const std::string& medit_filename,
                                         const std::string& action,
                                         const char* invoking_file,
                                         int invoking_line);

                    //! Destructor
                    virtual ~InvalidPath() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidPath(const InvalidPath& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidPath(InvalidPath&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidPath& operator=(const InvalidPath& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidPath& operator=(InvalidPath&& rhs) = default;
                };


                //! Thrown when dimension is incorrect.
                class InvalidDimension final : public Exception
                {

                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] medit_filename Medit file being read
                     * \param[in] dimension Dimension read in the file
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidDimension(const std::string& medit_filename,
                                              int dimension,
                                              const char* invoking_file,
                                              int invoking_line);

                    //! Destructor
                    virtual ~InvalidDimension() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidDimension(const InvalidDimension& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidDimension(InvalidDimension&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidDimension& operator=(const InvalidDimension& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidDimension& operator=(InvalidDimension&& rhs) = default;
                };


                /*!
                 * \brief Thrown when the coord index read are not correct.
                 */
                class InvalidCoordIndex final : public Exception
                {

                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] medit_filename Medit file being read
                     * \param[in] index Index read.
                     * \param[in] Ncoord Total number of coord in the mesh.
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit InvalidCoordIndex(const std::string& medit_filename,
                                               std::size_t index,
                                               std::size_t Ncoord,
                                               const char* invoking_file,
                                               int invoking_line);

                    //! Destructor
                    virtual ~InvalidCoordIndex() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    InvalidCoordIndex(const InvalidCoordIndex& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    InvalidCoordIndex(InvalidCoordIndex&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InvalidCoordIndex& operator=(const InvalidCoordIndex& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    InvalidCoordIndex& operator=(InvalidCoordIndex&& rhs) = default;
                };


            } // namespace Medit


        } // namespace Format


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_MEDIT_HPP_
