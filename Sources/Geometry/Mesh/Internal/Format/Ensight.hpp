/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Format.hpp"

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <optional>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Format.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::Internal::MeshNS { class RefGeomEltAndMeshLabel; }
namespace MoReFEM::Internal::MeshNS::FormatNS { template <::MoReFEM::MeshNS::Format TypeT> struct Informations; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                /*!
                 * \brief Specialization of Informations struct that provides generic informations about Ensight format.
                 */
                template<>
                struct Informations<::MoReFEM::MeshNS::Format::Ensight>
                {

                    //! Name of the format.
                    static const std::string& Name();

                    //! Extension of the Ensight files.
                    static const std::string& Extension();
                };


                namespace Ensight
                {


                    /*!
                     * \brief Read an Ensight 6 geometric file.
                     *
                     * It reads Ensight 6 format file; there are absolutely no guarantee it is able to read any valid
                     * Ensight file (on the contrary, I'm positive it does not). However it is able to read the Ensight
                     * files likely to be given as input in MoReFEM.
                     *
                     * An Ensight case file is also required to read it with Ensight client.
                     *
                     *
                     * \copydoc doxygen_hide_mesh_constructor_3_bis
                     * \copydoc doxygen_hide_mesh_constructor_4
                     * \copydoc doxygen_hide_space_unit_arg
                     * \param[out] dimension Dimension of the mesh, as read in the input file.
                     * \param[in] mesh_id Unique identifier of the \a Mesh for the construction of which
                     * present function is called.
                     * \param[in] Nprocessor_wise_geom_elt_per_type_and_label Number of processor-wise \a GeometricElt
                     * sort per \a RefGeomElt and \a MeshLabel. This optional parameter is required only when we rebuild
                     * from prepartitioned data; it is used to tag whether a given \a GeometricElt is processor-wise or
                     * ghost.
                     *
                     */
                    void ReadFile(const std::size_t mesh_id,
                                  const std::string& mesh_file,
                                  double space_unit,
                                  std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>
                                      Nprocessor_wise_geom_elt_per_type_and_label,
                                  std::size_t& dimension,
                                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                                  Coords::vector_shared_ptr& coords_list,
                                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


                    /*!
                     * \brief Write a mesh in Ensight format.
                     *
                     * \copydoc doxygen_hide_geometry_format_write_common_arg
                     */
                    void WriteFile(const Mesh& mesh, const std::string& mesh_file);


                } // namespace Ensight


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/Format/Ensight.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_
