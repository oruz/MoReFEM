/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iosfwd>
#include <map>
#include <optional>
#include <set>
#include <string_view>
#include <unordered_map>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp"                        // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Mesh.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp" // IWYU pragma: keep
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LuaOptionFile; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            /*!
             * \brief This class is used to create and retrieve Mesh objects.
             *
             * Mesh objects get private constructor and can only be created through this class. In addition
             * to their creation, this class keeps their address, so it's possible from instance to retrieve a
             * Mesh object given its unique id (which is the one that appears in the input data file).
             *
             */
            class MeshManager : public Utilities::Singleton<MeshManager>
            {

              public:
                //! Alias to storage.
                using storage_type = std::unordered_map<std::size_t, Mesh::const_unique_ptr>;


                /*!
                 * \brief Returns the name of the class (required for some Singleton-related errors).
                 *
                 * \return Name of the class.
                 */
                static const std::string& ClassName();

                //! Base type of Mesh as input parameter (requested to identify domains in the input parameter data).
                using input_data_type = ::MoReFEM::InputDataNS::BaseNS::Mesh;

                /*!
                 * \brief Create a new Mesh object from the data of the input data file.
                 *
                 * \param[in] section The mesh section from the input data file.
                 */
                template<class MeshSectionT>
                void Create(const MeshSectionT& section);

                /*!
                 * \brief Load the mesh related data from pre-partitioned data.
                 *
                 * \copydoc doxygen_hide_morefem_data_param
                 * \param[in] section The mesh section from the input data file.
                 */
                template<class MoReFEMDataT, class MeshSectionT>
                void LoadFromPrepartitionedData(const MoReFEMDataT& morefem_data, const MeshSectionT& section);

                //! Fetch the mesh object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{Mesh}
                const Mesh& GetMesh(std::size_t unique_id) const;

                //! Fetch the mesh object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{Mesh}
                Mesh& GetNonCstMesh(std::size_t unique_id);

                //! Fetch the mesh object associated with \a UniqueIdT unique identifier.
                //! \tparam UniqueIdT Unique identifier of the sought mesh (as returned by Mesh::GetUniqueId()).
                template<std::size_t UniqueIdT>
                const Mesh& GetMesh() const;

                /*!
                 * \brief Create a brand new Mesh and returns a reference to it.
                 *
                 * \copydoc doxygen_hide_mesh_constructor_5
                 * \copydoc doxygen_hide_mesh_constructor_1
                 * \copydoc doxygen_hide_mesh_constructor_2
                 * \copydoc doxygen_hide_mesh_optional_prepartitioned_data_arg
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test
                 * executables; however in full-fledged model instances you should not use this constructor at all:
                 * the other one above calls it with the correct data from the input data file.
                 * \endinternal
                 */
                void Create(const std::size_t mesh_id,
                            const std::string& mesh_file,
                            std::size_t dimension,
                            ::MoReFEM::MeshNS::Format format,
                            const double space_unit,
                            Mesh::BuildEdge do_build_edge = Mesh::BuildEdge::no,
                            Mesh::BuildFace do_build_face = Mesh::BuildFace::no,
                            Mesh::BuildVolume do_build_volume = Mesh::BuildVolume::no,
                            Mesh::BuildPseudoNormals do_build_pseudo_normals = Mesh::BuildPseudoNormals::no,
                            std::optional<std::reference_wrapper<LuaOptionFile>> prepartitioned_data = std::nullopt);


                /*!
                 * \brief Create a brand new Mesh and returns a reference to it.
                 *
                 * \copydoc doxygen_hide_space_unit_arg
                 * \param[in] dimension Dimension of the mesh.
                 * \copydoc doxygen_hide_mesh_constructor_2
                 * \copydoc doxygen_hide_mesh_constructor_3
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test
                 * executables; however in full-fledged model instances you should not use this constructor at all:
                 * the other one above calls it with the correct data from the input data file.
                 * \endinternal
                 */
                void Create(std::size_t dimension,
                            double space_unit,
                            GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                            GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                            Coords::vector_shared_ptr&& coords_list,
                            MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                            Mesh::BuildEdge do_build_edge,
                            Mesh::BuildFace do_build_face,
                            Mesh::BuildVolume do_build_volume,
                            Mesh::BuildPseudoNormals do_build_pseudo_normals = Mesh::BuildPseudoNormals::no);


                /*!
                 * \brief Create a new Mesh from data that were computed in a previous run.
                 *
                 * \copydoc doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_mesh_constructor_5
                 * \param[in] unique_id Unique identifier of the \a Mesh to be created.
                 * \param[in] prepartitioned_data Lua file which gives the data needed to reconstruct the data
                 * from pre-computed partitioned data. Note: it is not const as such objects relies on a Lua stack
                 * which is modified for virtually every operation.
                 * \param[in,out] dimension Dimension of the mesh read in the input file.
                 *
                 */
                void LoadFromPrepartitionedData(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                std::size_t unique_id,
                                                const std::string& mesh_file,
                                                LuaOptionFile& prepartitioned_data,
                                                std::size_t dimension,
                                                ::MoReFEM::MeshNS::Format format);


                /*!
                 * \brief Constant accessor to the store the mesh objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 *
                 * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
                 */
                const storage_type& GetStorage() const noexcept;

                /*!
                 * \brief Yields a unique id currently not used.
                 *
                 * \return Unique id not currently used.
                 */
                std::size_t GenerateUniqueId();

                //! Constant accessor to the list of unique ids already in use.
                const std::set<std::size_t>& GetUniqueIdList() const noexcept;


              private:
                /*!
                 * \brief Non constant accessor to the store the mesh objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 *
                 * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
                 */
                storage_type& GetNonCstStorage() noexcept;


                //! Non constant accessor to the list of unique ids already in use.
                std::set<std::size_t>& GetNonCstUniqueIdList() noexcept;


                //! Enum class to determine whether the unique id is already known in \a unique_id_list_ or not.
                enum class is_unique_id_known
                {
                    no,
                    yes
                };


                /*!
                 * \brief Helper method to Create() in charge of adding new mesh to storage.
                 *
                 * \param[in] mesh Pointer to the mesh to be introduced. This pointer will be mutated into
                 * a unique_ptr in the storage and must not therefore be deleted.
                 */
                template<is_unique_id_known IsUniqueIdKnownT>
                void InsertMesh(const Mesh* const mesh);


              private:
                //! \name Singleton requirements.
                ///@{

                //! Constructor.
                MeshManager();

                //! Destructor.
                virtual ~MeshManager() override;

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<MeshManager>;
                ///@}


              private:
                /*!
                 * \brief Store the mesh objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 */
                storage_type storage_;

                /*!
                 * \brief List of unique ids already in use.
                 *
                 * In most cases it's redundant with keys of \a storage_; however in some functions in which Create()
                 * method is to be called there might be temporarily several more items here.
                 */
                std::set<std::size_t> unique_id_list_;
            };


            /*!
             * \brief Write the interface list for each mesh.
             *
             * Should be called only on root processor.
             * \param[in] mesh_output_directory_storage Key is the unique id of the meshes, value the path to the
             * associated output directory, which should already exist.
             *  \param[in] filename Name if the file to be written in output directory; default value is what is
             * expected by facilities such as tests or post-processing.(the functionality to modify it has been
             * introduced mostly for dev purposes; tests and post-processing expect the default value).
             */
            // clang-format off
            void WriteInterfaceListForEachMesh(const std::map
                                               <
                                                    std::size_t,
                                                    ::MoReFEM::FilesystemNS::Directory::const_unique_ptr
                                               >& mesh_output_directory_storage,
                                               std::string_view filename = "interfaces.hhdata");
            // clang-format on


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/MeshManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HPP_
