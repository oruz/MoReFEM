/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/MeshManager.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
// IWYU pragma: no_include <__tree>
#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            template<class MeshSectionT>
            void MeshManager::Create(const MeshSectionT& section)
            {
                namespace ipl = Internal::InputDataNS;

                decltype(auto) mesh_file = ipl::ExtractPathLeaf<typename MeshSectionT::Path>(section);
                decltype(auto) dimension = ipl::ExtractLeaf<typename MeshSectionT::Dimension>(section);
                const auto format = FormatNS::GetType(ipl::ExtractLeaf<typename MeshSectionT::Format>(section));
                const auto space_unit = ipl::ExtractLeaf<typename MeshSectionT::SpaceUnit>(section);

                Create(section.GetUniqueId(),
                       mesh_file,
                       dimension,
                       format,
                       space_unit,
                       Mesh::BuildEdge::yes,
                       Mesh::BuildFace::yes,
                       Mesh::BuildVolume::yes,
                       Mesh::BuildPseudoNormals::no,
                       std::nullopt); // \todo #57
            }


            template<class MoReFEMDataT, class MeshSectionT>
            void MeshManager::LoadFromPrepartitionedData(const MoReFEMDataT& morefem_data, const MeshSectionT& section)
            {
                namespace ipl = Internal::InputDataNS;

                decltype(auto) dimension = ipl::ExtractLeaf<typename MeshSectionT::Dimension>(section);
                const auto format = FormatNS::GetType(ipl::ExtractLeaf<typename MeshSectionT::Format>(section));

                decltype(auto) parallelism_ptr = morefem_data.GetParallelismPtr();

                assert(!(!parallelism_ptr)
                       && "This method should not be called in no parallelism defined in input "
                          "data file!");

                decltype(auto) parallelism_dir = parallelism_ptr->GetDirectory();

                std::ostringstream oconv;
                oconv << parallelism_dir << "/Mesh_" << section.GetUniqueId() << '/';

                const auto mesh_dir = oconv.str();

                LuaOptionFile prepartitioned_data(mesh_dir + "mesh_data.lua", __FILE__, __LINE__);

                const auto mesh_file = mesh_dir + "mesh.mesh";

                LoadFromPrepartitionedData(
                    morefem_data.GetMpi(), section.GetUniqueId(), mesh_file, prepartitioned_data, dimension, format);
            }


            inline Mesh& MeshManager::GetNonCstMesh(std::size_t unique_id)
            {
                return const_cast<Mesh&>(GetMesh(unique_id));
            }


            template<std::size_t UniqueIdT>
            inline const Mesh& MeshManager::GetMesh() const
            {
                return GetMesh(UniqueIdT);
            }


            inline const MeshManager::storage_type& MeshManager ::GetStorage() const noexcept
            {
                return storage_;
            }


            inline MeshManager::storage_type& MeshManager ::GetNonCstStorage() noexcept
            {
                return const_cast<storage_type&>(GetStorage());
            }


            template<MeshManager::is_unique_id_known IsUniqueIdKnownT>
            void MeshManager::InsertMesh(const Mesh* const mesh)
            {
                assert(!(!mesh));
                auto&& ptr = Mesh::const_unique_ptr(mesh);

                const auto unique_id = ptr->GetUniqueId();

                auto&& pair = std::make_pair(unique_id, std::move(ptr));

                auto insert_return_value = storage_.insert(std::move(pair));

                if (!insert_return_value.second)
                    throw Exception("Two mesh objects can't share the same unique identifier! (namely "
                                        + std::to_string(unique_id) + ").",
                                    __FILE__,
                                    __LINE__);

                switch (IsUniqueIdKnownT)
                {
                case is_unique_id_known::yes:
                {
#ifndef NDEBUG
                    {
                        decltype(auto) unique_id_list = GetUniqueIdList();
                        assert(unique_id_list.find(unique_id) != unique_id_list.cend());
                    }
#endif // NDEBUG

                    break;
                }
                case is_unique_id_known::no:
                {
                    auto& unique_id_list = GetNonCstUniqueIdList();

                    assert(unique_id_list.find(unique_id) == unique_id_list.cend());
                    unique_id_list.insert(unique_id);
                }
                }
            }


            inline const std::set<std::size_t>& MeshManager::GetUniqueIdList() const noexcept
            {
#ifndef NDEBUG
                {
                    // Check all keys of storage_ are in unique_id_list_ (revert might be false).
                    decltype(auto) storage = GetStorage();

                    for (const auto& pair : storage)
                    {
                        const auto key = pair.first;
                        assert(unique_id_list_.find(key) != unique_id_list_.cend());
                    }
                }
#endif // NDEBUG


                return unique_id_list_;
            }


            inline std::set<std::size_t>& MeshManager::GetNonCstUniqueIdList() noexcept
            {
                // Here do not build it on top of GetUniqueIdList(): the assert would not pass during the InsertMesh()
                // call...
                return unique_id_list_;
            }


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_
