/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    namespace MeshNS
    {


        namespace InterpolationNS
        {


            namespace // anonymous
            {

                std::string meshes_line_moniker = "Meshes:";


            } // namespace


            void CoordsMatching::Read(const std::string& filename)
            {
                source_to_target_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                target_to_source_.max_load_factor(Utilities::DefaultMaxLoadFactor());

                std::ifstream in;
                FilesystemNS::File::Read(in, filename, __FILE__, __LINE__);

                std::string line;

                while (getline(in, line))
                {
                    Utilities::String::Strip(line);

                    // Ignore empty lines and comment lines.
                    if (line.empty())
                        continue;

                    if (Utilities::String::StartsWith(line, "#"))
                        continue;

                    if (Utilities::String::StartsWith(line, meshes_line_moniker))
                    {
                        SetMeshes(filename, line);
                        continue;
                    }

                    // A valid line should include two integers, that are vertex indexes on each of the mesh.
                    std::istringstream iconv(line);

                    std::size_t tmp;

                    iconv >> tmp;
                    ::MoReFEM::CoordsNS::index_from_mesh_file new_source_index(tmp);

                    if (iconv.fail())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__,
                                        __LINE__);

                    iconv >> tmp;
                    ::MoReFEM::CoordsNS::index_from_mesh_file new_target_index(tmp);

                    if (iconv.fail())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__,
                                        __LINE__);

                    if (!iconv.eof())
                        throw Exception("Invalid line in interpolation file " + filename + ": \n" + line + "\n",
                                        __FILE__,
                                        __LINE__);

                    {
                        auto [iterator, is_properly_inserted] =
                            source_to_target_.insert({ new_source_index, new_target_index });

                        if (!is_properly_inserted)
                        {
                            std::ostringstream oconv;
                            oconv << "Invalid interpolation file " << filename << ": source value " << new_source_index
                                  << " was found twice!";

                            throw Exception(oconv.str(), __FILE__, __LINE__);
                        }
                    }

                    {
                        auto [iterator, is_properly_inserted] =
                            target_to_source_.insert({ new_target_index, new_source_index });

                        if (!is_properly_inserted)
                        {
                            std::ostringstream oconv;
                            oconv << "Invalid interpolation file " << filename << ": target value " << new_target_index
                                  << " was found twice!";

                            throw Exception(oconv.str(), __FILE__, __LINE__);
                        }
                    }
                } // while

                if (std::any_of(mesh_ids_.cbegin(),
                                mesh_ids_.cend(),
                                [](const auto id)
                                {
                                    return id == NumericNS::UninitializedIndex<decltype(id)>();
                                }))
                {
                    std::ostringstream oconv;
                    oconv << "Invalid interpolation file " << filename
                          << ": the meshes id were not properly set "
                             "(there should be a line '"
                          << meshes_line_moniker
                          << "a b' where a and b are the integer unique "
                             "ids of the meshes involved)";

                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }


            ::MoReFEM::CoordsNS::index_from_mesh_file
            CoordsMatching ::FindSourceIndex(::MoReFEM::CoordsNS::index_from_mesh_file target_index) const
            {
                decltype(auto) target_to_source = GetTargetToSource();
                auto it = target_to_source.find(target_index);

                assert(it != target_to_source.cend()
                       && "Check your interpolation file (given in input parameter) "
                          "is correct... ");

                return it->second;
            }


            ::MoReFEM::CoordsNS::index_from_mesh_file
            CoordsMatching ::FindTargetIndex(::MoReFEM::CoordsNS::index_from_mesh_file source_index) const
            {
                decltype(auto) source_to_target = GetSourceToTarget();
                auto it = source_to_target.find(source_index);

                assert(it != source_to_target.cend()
                       && "Check your interpolation file (given in input parameter) "
                          "is correct... ");

                return it->second;
            }


            void CoordsMatching::SetMeshes(const std::string& filename, std::string line)
            {
                assert(Utilities::String::StartsWith(line, meshes_line_moniker)
                       && "It's an assert as this condition is the "
                          "only case in which this method should be invoked");

                if (!std::all_of(mesh_ids_.cbegin(),
                                 mesh_ids_.cend(),
                                 [](const auto id)
                                 {
                                     return id == NumericNS::UninitializedIndex<decltype(id)>();
                                 }))
                {
                    std::ostringstream oconv;
                    oconv << "Invalid interpolation file " << filename
                          << ": the meshes id are read at least twice in "
                             "the interpolation file (there should be only one line with the format '"
                          << meshes_line_moniker
                          << "a b' where a and b are the integer unique ids of the meshes involved)";

                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                line.erase(0, meshes_line_moniker.size());

                std::istringstream iconv(line);
                iconv >> mesh_ids_[0];

                if (iconv.fail())
                    throw Exception("Invalid line in interpolation file " + filename
                                        + ": the line giving the meshes "
                                          "involved couldn't be interpreted properly.",
                                    __FILE__,
                                    __LINE__);

                iconv >> mesh_ids_[1];

                if (iconv.fail())
                    throw Exception("Invalid line in interpolation file " + filename
                                        + ": the line giving the meshes "
                                          "involved couldn't be interpreted properly.",
                                    __FILE__,
                                    __LINE__);

                if (!iconv.eof())
                    throw Exception(
                        "Invalid line in interpolation file " + filename + ": \n" + line + "\n", __FILE__, __LINE__);
            }


            std::vector<CoordsNS::index_from_mesh_file>
            CoordsMatching::FindSourceIndex(const std::vector<CoordsNS::index_from_mesh_file>& target_indexes) const
            {
                std::vector<CoordsNS::index_from_mesh_file> ret(target_indexes.size());

                std::transform(target_indexes.cbegin(),
                               target_indexes.cend(),
                               ret.begin(),
                               [this](CoordsNS::index_from_mesh_file target_index)
                               {
                                   return this->FindSourceIndex(target_index);
                               });

                Internal::InterfaceNS::OrderCoordsList(ret, std::less<CoordsNS::index_from_mesh_file>());

                return ret;
            }


            std::vector<CoordsNS::index_from_mesh_file>
            CoordsMatching::FindTargetIndex(const std::vector<CoordsNS::index_from_mesh_file>& source_indexes) const
            {
                std::vector<CoordsNS::index_from_mesh_file> ret(source_indexes.size());

                std::transform(source_indexes.cbegin(),
                               source_indexes.cend(),
                               ret.begin(),
                               [this](CoordsNS::index_from_mesh_file source_index)
                               {
                                   return this->FindTargetIndex(source_index);
                               });

                Internal::InterfaceNS::OrderCoordsList(ret, std::less<CoordsNS::index_from_mesh_file>());


                return ret;
            }


        } // namespace InterpolationNS


    } //  namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
