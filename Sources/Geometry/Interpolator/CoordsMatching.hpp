/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/Array.hpp"

#include "Geometry/Coords/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::NonConformInterpolatorNS { class FromCoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace MeshNS
    {


        namespace InterpolationNS
        {


            /*!
             * \brief Interpolation between vertices of two meshes as given by an ad hoc file.
             *
             * This class is built upon the data read in the input data file, and is used only within
             * \a FromCoordsMatching interpolator.
             *
             * \attention #1630 Works only for P1 geometry!
             *
             * \attention Currently MoReFEM may handle only one \a CoordsMatching per \a Model. This could be extended with few hours of work
             * without major difficulty, but as the need is not there at the moment it hasn't been done.
             */
            class CoordsMatching
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = CoordsMatching;

                //! Frienship to the class that actually requires vertex matching informations.
                friend class NonConformInterpolatorNS::FromCoordsMatching;

                //! The type of the underlying map between source and target indexes.
                // clang-format off
                using mapping_type =
                    std::unordered_map
                    <
                        ::MoReFEM::CoordsNS::index_from_mesh_file,
                        ::MoReFEM::CoordsNS::index_from_mesh_file
                    >;
                // clang-format on

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor from an input data file.
                 *
                 * \copydoc doxygen_hide_input_data_arg
                 */
                template<class InputDataT>
                explicit CoordsMatching(const InputDataT& input_data);

                //! Destructor.
                ~CoordsMatching() = default;

                //! \copydoc doxygen_hide_copy_constructor
                CoordsMatching(const CoordsMatching& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                CoordsMatching(CoordsMatching&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                CoordsMatching& operator=(const CoordsMatching& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                CoordsMatching& operator=(CoordsMatching&& rhs) = delete;

                ///@}


                //! Returns the source index that matches the given \a target_index.
                //! \param[in] target_index Index used as filter.
                CoordsNS::index_from_mesh_file FindSourceIndex(CoordsNS::index_from_mesh_file target_index) const;

                //! Returns thetarget index that matches the given \a source_index.
                //! \param[in] source_index Index used as filter.
                CoordsNS::index_from_mesh_file FindTargetIndex(CoordsNS::index_from_mesh_file source_index) const;

                /*!
                 * \brief Returns the source indexes that matches the given \a target_indexes.
                 *
                 * \param[in] target_indexes Indexes used as filter.
                 *
                 * \return The match of \a target_indexes on the source side, in exactly the same order.
                 */
                std::vector<CoordsNS::index_from_mesh_file>
                FindSourceIndex(const std::vector<CoordsNS::index_from_mesh_file>& target_indexes) const;

                /*!
                 * \brief Returns the target indexes that matches the given \a source_indexes.
                 *
                 * \param[in] source_indexes Indexes used as filter.
                 *
                 * \return The match of \a source_indexes on the target side, in exactly the same order.
                 */
                std::vector<CoordsNS::index_from_mesh_file>
                FindTargetIndex(const std::vector<CoordsNS::index_from_mesh_file>& source_indexes) const;

                //! Returns the unique id of the "source" mesh.
                std::size_t GetSourceMeshId() const noexcept;

                //! Returns the unique id of the "target" mesh.
                std::size_t GetTargetMeshId() const noexcept;

              private:
                //! Mapping source -> target (i.e. given a source index what is the mapped \a Coords in the target
                //! mesh).
                const mapping_type& GetSourceToTarget() const noexcept;

                //! Mapping target -> source (i.e. given a target index what is the mapped \a Coords in the source
                //! mesh).
                const mapping_type& GetTargetToSource() const noexcept;


                //! Method that does the bulk of constructor job. Should not be called out of constructor.
                //! \param[in] filename Name of the file in which the informations are stored.
                void Read(const std::string& filename);

                /*!
                 * \brief Set the meshes involved in the interpolation from the line in the interpolation file.
                 *
                 * \param[in] filename Filename of the interpolation file.Used only to enrich message in exceptions.
                 * \param[in] line The expected line in the interpolation file.
                 *
                 * The line should look like:
                 * 'Meshes: 1 2'
                 * where 1 and 2 are the unique ids of the meshes involved.
                 */
                void SetMeshes(const std::string& filename, std::string line);

              private:
                //! Relationship between vertices of both meshes. The \a i-th element of this vector is matched with the
                //! \a i-th element of \a target_index_list_.
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> source_index_list_;

                //! Must be the same size as \a source_index_list_.
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> target_index_list_;

                //! Mapping source -> target (i.e. given a source index what is the mapped \a Coords in the target
                //! mesh).
                mapping_type source_to_target_;

                //! Mapping target -> source (i.e. given a target index what is the mapped \a Coords in the source
                //! mesh).
                mapping_type target_to_source_;

                //! Unique ids of the "source"  and "target" meshes respectively..
                std::array<std::size_t, 2ul> mesh_ids_ = Utilities::FilledWithUninitializedIndex<std::size_t, 2ul>();
            };


        } // namespace InterpolationNS


    } // namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interpolator/CoordsMatching.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HPP_
