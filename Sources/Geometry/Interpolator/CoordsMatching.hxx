/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_

// IWYU pragma: private, include "Geometry/Interpolator/CoordsMatching.hpp"


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Geometry/InterpolationFile.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace MeshNS
    {


        namespace InterpolationNS
        {


            template<class InputDataT>
            CoordsMatching::CoordsMatching(const InputDataT& input_data)
            {
                decltype(auto) interpolation_file =
                    Utilities::InputDataNS::Extract<InputDataNS::InterpolationFile>::Path(input_data);

                Read(interpolation_file);
            }


            inline const CoordsMatching::mapping_type& CoordsMatching::GetSourceToTarget() const noexcept
            {
                assert(source_to_target_.size() == target_to_source_.size());
                return source_to_target_;
            }


            inline const CoordsMatching::mapping_type& CoordsMatching::GetTargetToSource() const noexcept
            {
                assert(source_to_target_.size() == target_to_source_.size());
                return target_to_source_;
            }


            inline std::size_t CoordsMatching::GetSourceMeshId() const noexcept
            {
                assert(mesh_ids_[0] != NumericNS::UninitializedIndex<std::size_t>());
                return mesh_ids_[0];
            }


            inline std::size_t CoordsMatching::GetTargetMeshId() const noexcept
            {
                assert(mesh_ids_[1] != NumericNS::UninitializedIndex<std::size_t>());
                return mesh_ids_[1];
            }


        } // namespace InterpolationNS


    } // namespace MeshNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_
