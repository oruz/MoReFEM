/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
// IWYU pragma: no_include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"


namespace MoReFEM
{


    DomainManager::~DomainManager() = default;


    const std::string& DomainManager::ClassName()
    {
        static std::string ret("DomainManager");
        return ret;
    }


    DomainManager::DomainManager() = default;


    void DomainManager::Create(std::size_t unique_id,
                               const std::vector<std::size_t>& mesh_index,
                               const std::vector<std::size_t>& dimension_list,
                               const std::vector<std::size_t>& mesh_label_list,
                               const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list)
    {
        // make_unique is not accepted here: it makes the code yell about private status of the constructor
        // with both clang and gcc.
        Domain* buf = new Domain(unique_id, mesh_index, dimension_list, mesh_label_list, geometric_type_list);

        auto&& ptr = Domain::const_unique_ptr(buf);

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto insert_return_value = list_.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two Domain objects can't share the same unique identifier! (namely "
                                + std::to_string(unique_id) + ").",
                            __FILE__,
                            __LINE__);
    }


    const Domain& DomainManager::GetDomain(std::size_t unique_id, const char* invoking_file, int invoking_line) const
    {
        auto it = list_.find(unique_id);

        if (it == list_.cend())
            throw Exception("Domain " + std::to_string(unique_id) + " is not defined!", invoking_file, invoking_line);

        assert(!(!(it->second)));

        return *(it->second);
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
