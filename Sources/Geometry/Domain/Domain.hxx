/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Jul 2014 09:57:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HXX_

// IWYU pragma: private, include "Geometry/Domain/Domain.hpp"

#include <algorithm>
#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>
#include <vector>

#include "Geometry/Domain/Internal/DomainHelper.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const std::vector<std::size_t>& Domain::GetDimensionList() const noexcept
    {
        assert(IsConstraintOn<DomainNS::Criterion::dimension>());
        return dimension_list_;
    }


    inline const MeshLabel::vector_const_shared_ptr& Domain::GetMeshLabelList() const noexcept
    {
        assert(IsConstraintOn<DomainNS::Criterion::label>());
        return mesh_label_list_;
    }


    inline const std::vector<Advanced::GeometricEltEnum>& Domain::GetRefGeometricEltIdList() const noexcept
    {
        assert(IsConstraintOn<DomainNS::Criterion::geometric_elt_type>());
        return geometric_type_list_;
    }


    inline std::size_t Domain::GetMeshIdentifier() const noexcept
    {
        assert(IsConstraintOn<DomainNS::Criterion::mesh>());
        assert(mesh_identifier_ != NumericNS::UninitializedIndex<std::size_t>());
        return mesh_identifier_;
    }


    template<DomainNS::Criterion CriterionT>
    inline bool Domain::IsConstraintOn() const noexcept
    {
        constexpr std::size_t index = static_cast<std::size_t>(CriterionT);
        static_assert(index < static_cast<std::size_t>(DomainNS::Criterion::End),
                      "Index must belong to the enum class scope!");

        return are_constraints_on_[index];
    }


    template<DomainNS::Criterion CriterionT, class GeometricObjectT>
    bool Domain::IsConstraintFulfilled(const GeometricObjectT& object) const
    {
        assert(IsConstraintOn<CriterionT>());
        static_assert(!std::is_pointer<GeometricObjectT>::value, "Should be a bare object!");


        switch (CriterionT)
        {
        case DomainNS::Criterion::mesh:
        {
            return Internal::DomainNS::IsObjectInMesh(object, GetMeshIdentifier());
        }
        case DomainNS::Criterion::dimension:
        {
            const auto& dimension_list = GetDimensionList();

            return std::binary_search(dimension_list.cbegin(), dimension_list.cend(), object.GetDimension());
        }
        case DomainNS::Criterion::label:
        {
            return Internal::DomainNS::IsMeshLabelInList(object, GetMeshLabelList());
        }
        case DomainNS::Criterion::geometric_elt_type:
        {
            const auto& type_list = GetRefGeometricEltIdList();

            return std::binary_search(type_list.cbegin(), type_list.cend(), object.GetIdentifier());
        }
        }

        assert(false && "One of the criterion should have been chosen!");
        return false; // to avoid compilation warning.
    }


    template<DomainNS::Criterion CriterionT, class GeometricObjectT>
    bool Domain::CheckConstraintIfRelevant(const GeometricObjectT& object) const
    {
        if (!IsConstraintOn<CriterionT>())
            return true;

        return IsConstraintFulfilled<CriterionT>(object);
    }


    inline bool operator==(const Domain& lhs, const Domain& rhs) noexcept
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HXX_
