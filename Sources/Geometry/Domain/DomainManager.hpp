/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp"                        // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Domain.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Advanced
    {


        class LightweightDomainList;


    } // namespace Advanced


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief This class is used to create and retrieve Domain objects.
     *
     * Domain objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * Domain object given its unique id (which is the one that appears in the input data file).
     *
     */
    class DomainManager : public Utilities::Singleton<DomainManager>
    {

      private:
        //! Convenient alias to avoid repeating the type.
        using storage_type = std::unordered_map<std::size_t, Domain::const_unique_ptr>;

        /*!
         * \brief Friendship for tests purposes.
         *
         * In tests, we might want to create objects without a full-fledged model:
         *
         \code
         namespace MoReFEM
         {

             struct TestHelper
             {
                TestHelper()
                {
                    const std::size_t id = 0ul;
                    const auto mesh_index = 1u;
                    const auto dimension = 2u;

                    DomainManager::CreateOrGetInstance(__FILE__, __LINE__).Create(id,
                                                                                  { mesh_index },
                                                                                  { dimension },
                                                                                  { }, { });
                }

             };

         } // namespace MoReFEM
         \endcode
         *
         * for instance creates directly a domain labelled 0 for the mesh 1 with geometric elements of dimension 2.
         *
         * \attention This trick should be used only while writing lightweight tests; do not use this in a real
         * model!
         */
        friend struct TestHelper;

      public:
        //! Base type of Domain as input parameter (requested to identify domains in the input parameter data).
        using input_data_type = InputDataNS::BaseNS::Domain;

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        /*!
         * \brief Create a new Domain object from the data of the input data file.
         *
         * \param[in] section Section in an input data file that describes content of this \a Domain.
         */
        template<class DomainSectionT>
        void Create(const DomainSectionT& section);

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \unique_id_param_in_accessor{Domain}
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return Domain which GetUniqueId() method yield \a unique_id.
         */
        const Domain& GetDomain(std::size_t unique_id, const char* invoking_file, int invoking_line) const;

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \unique_id_param_in_accessor{Domain}
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return Non constant reference to the domain which GetUniqueId() method yield \a unique_id.
         */
        Domain& GetNonCstDomain(std::size_t unique_id, const char* invoking_file, int invoking_line);

        //! Friendship to a class that needs access to \a CreateLightweightDomain method.
        friend class Advanced::LightweightDomainList;

        //! Constant accessor to the domain list.
        const storage_type& GetDomainList() const noexcept;

      private:
        /*!
         * \brief Create a brand new Domain.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique. It is in the input data file
         * the figure that is in the block name, e.g. 1 for Domain1 = { .... }.
         * \param[in] mesh_index_list There might be here one index, that indicates in which mesh the domain is defined.
         * If the domain is not limited to one mesh, leave it empty.
         * \param[in] dimension_list List of dimensions to consider. If empty, no restriction on dimension.
         * \param[in] mesh_label_list List of mesh labels to consider. If empty, no restriction on it. This argument
         * must mandatorily be empty if \a mesh_index is empty: a mesh label is closely related to one given mesh.
         * \param[in] geometric_type_list List of geometric element types to consider in the domain. List of elements
         * available is given by Advanced::GeometricEltFactory::GetNameList(); most if not all of them should been
         * displayed in the comment in the input data file.
         *
         */
        void Create(std::size_t unique_id,
                    const std::vector<std::size_t>& mesh_index_list,
                    const std::vector<std::size_t>& dimension_list,
                    const std::vector<std::size_t>& mesh_label_list,
                    const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list);


        /*!
         * \brief Create a brand new lightweight \a Domain.
         *
         * Such a \a Domain, more restricted than a full-fledged one, should be created only by a \a
         * LightweightDomainList object.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique.
         * \param[in] mesh_index Index of the mesh onto which \a Domain must be created.
         * \param[in] mesh_label_list List of mesh labels to consider.
         *
         */
        void CreateLightweightDomain(std::size_t unique_id,
                                     std::size_t mesh_index,
                                     const std::vector<std::size_t>& mesh_label_list);


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        DomainManager();

        //! Destructor.
        virtual ~DomainManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<DomainManager>;
        ///@}


      private:
        //! Store the domain objects by their unique identifier.
        storage_type list_;
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/DomainManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_
