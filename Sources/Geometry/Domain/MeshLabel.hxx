/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HXX_

// IWYU pragma: private, include "Geometry/Domain/MeshLabel.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class MeshLabel; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const std::string& MeshLabel::GetDescription() const noexcept
    {
        return description_;
    }


    inline std::size_t MeshLabel::GetMeshIdentifier() const noexcept
    {
        return mesh_identifier_;
    }


    inline std::size_t MeshLabel::GetIndex() const noexcept
    {
        return index_;
    }


    inline bool operator<(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        assert("One should compare only labels that refer to the same mesh."
               && lhs.GetMeshIdentifier() == rhs.GetMeshIdentifier());

        return lhs.GetIndex() < rhs.GetIndex();
    }


    inline bool operator==(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        assert("One should compare only labels that refer to the same mesh."
               && lhs.GetMeshIdentifier() == rhs.GetMeshIdentifier());

#ifndef NDEBUG
        // In release mode, indexes are compared
        return lhs.GetIndex() == rhs.GetIndex();
#else // NDEBUG
      // In debug mode, we check the descriptions matches. If not, it highlight a bug in the attribution of
      // the indexes
        bool ret = lhs.GetIndex() == rhs.GetIndex();

        if (ret)
            assert(lhs.GetDescription() == rhs.GetDescription());

        return ret;
#endif // NDEBUG
    }


    inline bool operator!=(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        return !operator==(lhs, rhs);
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HXX_
