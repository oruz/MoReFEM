/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Domain/DomainManager.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"
#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class DomainSectionT>
    void DomainManager::Create(const DomainSectionT& section)
    {
        namespace ipl = Internal::InputDataNS;

        decltype(auto) mesh_index_list = ipl::ExtractLeaf<typename DomainSectionT::MeshIndexList>(section);
        decltype(auto) dimension_list = ipl::ExtractLeaf<typename DomainSectionT::DimensionList>(section);
        decltype(auto) mesh_label_list = ipl::ExtractLeaf<typename DomainSectionT::MeshLabelList>(section);
        decltype(auto) geometric_type_list = ipl::ExtractLeaf<typename DomainSectionT::GeomEltTypeList>(section);

        std::vector<Advanced::GeomEltNS::GenericName> geom_list(geometric_type_list.size());

        std::transform(geometric_type_list.cbegin(),
                       geometric_type_list.cend(),
                       geom_list.begin(),
                       [](const auto& str)
                       {
                           return Advanced::GeomEltNS::GenericName(str);
                       });

        Create(section.GetUniqueId(), mesh_index_list, dimension_list, mesh_label_list, geom_list);
    }


    inline Domain& DomainManager::GetNonCstDomain(std::size_t unique_id, const char* invoking_file, int invoking_line)
    {
        return const_cast<Domain&>(GetDomain(unique_id, invoking_file, invoking_line));
    }


    inline void DomainManager::CreateLightweightDomain(std::size_t unique_id,
                                                       std::size_t mesh_index,
                                                       const std::vector<std::size_t>& mesh_label_list)
    {
        Create(unique_id, { mesh_index }, {}, mesh_label_list, {});
    }


    inline const DomainManager::storage_type& DomainManager::GetDomainList() const noexcept
    {
        return list_;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
