/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Nov 2014 15:49:24 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/Internal/DomainHelper.hpp"
#include "Geometry/Domain/MeshLabel.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace Internal
    {


        namespace DomainNS
        {


            [[noreturn]] bool IsObjectInMesh(const RefGeomElt&, std::size_t)
            {
                assert("Should never be called in runtime!" && false);
                throw; // to avoid compilation warning.
            }


            [[noreturn]] bool IsMeshLabelInList(const RefGeomElt&, const MeshLabel::vector_const_shared_ptr&)
            {
                assert("Should never be called in runtime!" && false);
                throw; // to avoid compilation warning.
            }


        } // namespace DomainNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
