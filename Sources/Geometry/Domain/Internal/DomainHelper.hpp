/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Aug 2014 14:43:07 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/MeshLabel.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace DomainNS
        {


            /*!
             * \brief Returns whether the \a geometric_elt belongs to the mesh identified by
             * \a mesh_identifier.
             *
             * \param[in] geometric_elt Geometric element upon which the test is performed.
             * \param[in] mesh_identifier Identifier of the mesh tested.
             *
             * \return Whether \a geometric_elt belongs to the \a Mesh which id is
             * mesh_identifier.
             */
            bool IsObjectInMesh(const GeometricElt& geometric_elt, std::size_t mesh_identifier);

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            /*!
             * \brief A function to make the code compile.
             *
             * It should never be called in runtime!
             */
            [[noreturn]] bool IsObjectInMesh(const RefGeomElt&, std::size_t);
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Returns whether \a geometric_element's MeshLabel is in \a mesh_label_list.
             *
             * \param[in] geometric_elt Geometric element upon which the test is performed.
             * \param[in] mesh_label_list Mesh labels in the domain.
             *
             * \return Whether \a geometric_elt belongs to any of the \a MeshLabel cited in \a mesh_label_list.
             */
            bool IsMeshLabelInList(const GeometricElt& geometric_elt,
                                   const MeshLabel::vector_const_shared_ptr& mesh_label_list);


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            /*!
             * \brief A function to make the code compile.
             *
             * It should never be called in runtime!
             */
            [[noreturn]] bool IsMeshLabelInList(const RefGeomElt&, const MeshLabel::vector_const_shared_ptr&);
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        } // namespace DomainNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/Internal/DomainHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_
