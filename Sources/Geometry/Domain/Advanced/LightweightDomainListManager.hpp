/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp"                        // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        /*!
         * \brief Manager that is aware of all \a LightweightDomainListManager.
         *
         * Please notice that all \a Domain defined therein are known (and actually managed by the \a DomainManager).
         */
        class LightweightDomainListManager : public Utilities::Singleton<LightweightDomainListManager>
        {

          private:
            //! Convenient alias to avoid repeating the type.
            using storage_type = std::unordered_map<std::size_t, LightweightDomainList::const_unique_ptr>;

          public:
            //! Base type of Domain as input parameter (requested to identify domains in the input parameter data).
            using input_data_type = ::MoReFEM::InputDataNS::BaseNS::LightweightDomainList;

            /*!
             * \brief Returns the name of the class (required for some Singleton-related errors).
             *
             * \return Name of the class.
             */
            static const std::string& ClassName();

            /*!
             * \brief Create a new \a LightweightDomainList object from the data of the input data file.
             *
             * \param[in] section Section in an input data file that describes content of this \a LightweightDomainList.
             */
            template<class SectionT>
            void Create(const SectionT& section);


            //! Fetch the domain object associated with \a unique_id unique identifier.
            //! \unique_id_param_in_accessor{LightweightDomainList}
            const LightweightDomainList& GetLightweightDomainList(std::size_t unique_id) const;

            //! Fetch the domain region object associated with \a unique_id unique identifier.
            //! \unique_id_param_in_accessor{LightweightDomainList}
            LightweightDomainList& GetNonCstLightweightDomainList(std::size_t unique_id);

            //! Constant accessor to the complete lightweight domain list list.
            const storage_type& GetLightweightDomainListStorage() const noexcept;

          private:
            /*!
             * \brief Method in charge of adding a new \a LightweightDomainList to the constructor.
             *
             * \copydetails doxygen_hide_lightweight_domain_list_constructor
             */
            void Create(std::size_t unique_id,
                        std::size_t mesh_index,
                        const std::vector<std::size_t>& domain_index_list,
                        const std::vector<std::size_t>& mesh_label_list,
                        const std::vector<std::size_t>& number_in_domain_list);

          private:
            //! \name Singleton requirements.
            ///@{

            //! Constructor.
            LightweightDomainListManager();

            //! Destructor.
            virtual ~LightweightDomainListManager() override;

            //! Friendship declaration to Singleton template class (to enable call to constructor).
            friend class Utilities::Singleton<LightweightDomainListManager>;
            ///@}


          private:
            //! Store the variable \a LightweightDomainList objects by their unique identifier.
            storage_type lightweight_domain_list_storage_;
        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/Advanced/LightweightDomainListManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_
