/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
// IWYU pragma: no_include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/Domain.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        LightweightDomainListManager::~LightweightDomainListManager() = default;


        const std::string& LightweightDomainListManager::ClassName()
        {
            static std::string ret("LightweightDomainListManager");
            return ret;
        }


        LightweightDomainListManager::LightweightDomainListManager() = default;


        void LightweightDomainListManager::Create(std::size_t unique_id,
                                                  std::size_t mesh_index,
                                                  const std::vector<std::size_t>& domain_index_list,
                                                  const std::vector<std::size_t>& mesh_label_list,
                                                  const std::vector<std::size_t>& number_in_domain_list)
        {
            LightweightDomainList* buf = new LightweightDomainList(
                unique_id, mesh_index, domain_index_list, mesh_label_list, number_in_domain_list);

            auto&& ptr = LightweightDomainList::const_unique_ptr(buf);
            assert(ptr->GetUniqueId() == unique_id);

            auto&& pair = std::make_pair(unique_id, std::move(ptr));

            auto insert_return_value = lightweight_domain_list_storage_.insert(std::move(pair));

            if (!insert_return_value.second)
                throw Exception("Two LightweightDomainList objects can't share the same unique identifier! (namely "
                                    + std::to_string(unique_id) + ").",
                                __FILE__,
                                __LINE__);
        }


        const LightweightDomainList& LightweightDomainListManager::GetLightweightDomainList(std::size_t unique_id) const
        {
            auto it = lightweight_domain_list_storage_.find(unique_id);

            assert(it != lightweight_domain_list_storage_.cend());
            assert(!(!(it->second)));

            return *(it->second);
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
