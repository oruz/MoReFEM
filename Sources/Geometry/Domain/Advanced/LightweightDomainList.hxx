/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_

// IWYU pragma: private, include "Geometry/Domain/Advanced/LightweightDomainList.hpp"

#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        inline const std::vector<const Domain*>& LightweightDomainList::GetList() const noexcept
        {
            return domain_storage_;
        }


        inline const Mesh& LightweightDomainList::GetMesh() const noexcept
        {
            return mesh_;
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_
