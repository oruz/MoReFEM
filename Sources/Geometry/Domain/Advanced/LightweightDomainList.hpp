/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Geometry/Domain/Domain.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace Advanced
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class LightweightDomainListManager;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        /*!
         * \class doxygen_hide_lightweight_domain_list
         *
         * \brief Facility to handle a variable number of domains.
         *
         * Usual way to deal with \a Domain is to define them one by one in the input data file; the number of
         * \a Domain is usually known by the writer of a \a Model. However that is not always true: for some models
         * that are for instance patient specific, the number of \a Domain to consider may vary from one domain to
         * another; in these case we generally seek to apply the same computation on all the \a Domain.
         *
         * This is precisely the goal of the current class, which interprets data from the input data file for
         * an unknown number of \a Domain. These \a Domain are most restricted that usual \a Domain
         * - They must be related to exactly one mesh.
         * - No restriction upon dimension or constitutive \a GeometricElt inside.
         * - Must feature \a MeshLabel.
         *
         * These constraints might be bypassed if required by rewriting a bit this library; thet have been set to avoid
         * an overly complicated interface.
         *
         * An instance of \a LightweightDomainList keeps track of the \a Domain defined therein, to ease the application
         * of a same computation over all \a Domain within a same \a LightweightDomainList.
         *
         * The \a Domain defined here are actually managed by the \a DomainManager.
         */

        //! \copydoc doxygen_hide_lightweight_domain_list
        class LightweightDomainList
        : public Crtp::UniqueId<LightweightDomainList, UniqueIdNS::AssignationMode::manual, UniqueIdNS::DoAllowNoId::no>
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = LightweightDomainList;

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias to vector of unique pointers.
            using vector_const_unique_ptr = std::vector<const_unique_ptr>;

            //! Convenient alias.
            using unique_id_parent =
                Crtp::UniqueId<LightweightDomainList, UniqueIdNS::AssignationMode::manual, UniqueIdNS::DoAllowNoId::no>;

            //! Name of the class.
            static const std::string& ClassName();

            //! Friendship to the associated manager class.
            friend LightweightDomainListManager;


          private:
            /// \name Special members.
            ///@{

            /*!
             * \class doxygen_hide_lightweight_domain_list_constructor
             *
             * \brief Constructor.
             *
             * \param[in] unique_id Unique identifier of the \a LightweightDomainList (same as index used in the
             * input data file).
             * \param[in] mesh_index Index of the mesh for which all \a Domain therein are defined.
             * \param[in] domain_index_list For each \a Domain define here, specify a unique identifier.
             * \param[in] mesh_label_list List of all mesh indexes used to define the domains. There might be more
             * elements than the number of domains: a same \a Domain may encompass several \a MeshLabel. See
             * \a number_in_domain_list to see how they are properly linked to each domain.
             * \param[in] number_in_domain_list For each domain, the number of \a MeshLabel to consider.
             *
             * Let's illustrate this by an example:
             *
             * \verbatim
             LightweightDomainList1 = {

                   mesh_label_list = { 0, 1, 1, 2, 3, 2, 3 },
                   domain_index_list = { 3, 4, 5, 6 },
                   number_in_domain_list = { 2, 3, 1, 1 }
             } -- LightweightDomainList1
             \endverbatim
             *
             * Means that:
             * - Domain 3 is composed of \a MeshLabel 0 and 1.
             * - Domain 4 is composed of \a MeshLabel 1, 2 and 3.
             * - Domain 5 is composed of \a MeshLabel 2.
             * - Domain 6 is composed of \a MeshLabel 3.
             *
             */

            //! \copydoc doxygen_hide_lightweight_domain_list_constructor
            explicit LightweightDomainList(std::size_t unique_id,
                                           std::size_t mesh_index,
                                           const std::vector<std::size_t>& domain_index_list,
                                           const std::vector<std::size_t>& mesh_label_list,
                                           const std::vector<std::size_t>& number_in_domain_list);

          public:
            //! Destructor.
            ~LightweightDomainList() = default;

            //! \copydoc doxygen_hide_copy_constructor
            LightweightDomainList(const LightweightDomainList& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            LightweightDomainList(LightweightDomainList&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            LightweightDomainList& operator=(const LightweightDomainList& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            LightweightDomainList& operator=(LightweightDomainList&& rhs) = delete;

            ///@}


            //! Accessor to the \a Domain encompassed by the current object.
            const std::vector<const Domain*>& GetList() const noexcept;

            /*!
             * \brief Return the \a Mesh.
             *
             * \return \a Mesh into which current \a LightweightDomainList is defined.
             */
            const Mesh& GetMesh() const noexcept;

          private:
            //! \a Domain encompassed by the current object.
            std::vector<const Domain*> domain_storage_;

            //! Mesh for which all the \a Domain in current object are defined.
            const Mesh& mesh_;
        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/Advanced/LightweightDomainList.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
