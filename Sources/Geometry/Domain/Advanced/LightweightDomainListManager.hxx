/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced { class LightweightDomainList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        template<class SectionT>
        void LightweightDomainListManager::Create(const SectionT& section)
        {
            namespace ipl = Internal::InputDataNS;

            decltype(auto) mesh_index = ipl::ExtractLeaf<typename SectionT::MeshIndex>(section);
            decltype(auto) number_in_domain_list = ipl::ExtractLeaf<typename SectionT::NumberInDomainList>(section);
            decltype(auto) domain_index_list = ipl::ExtractLeaf<typename SectionT::DomainIndexList>(section);
            decltype(auto) mesh_label_list = ipl::ExtractLeaf<typename SectionT::MeshLabelList>(section);

            Create(section.GetUniqueId(), mesh_index, domain_index_list, mesh_label_list, number_in_domain_list);
        }


        inline LightweightDomainList&
        LightweightDomainListManager::GetNonCstLightweightDomainList(std::size_t unique_id)
        {
            return const_cast<LightweightDomainList&>(GetLightweightDomainList(unique_id));
        }


        inline const LightweightDomainListManager::storage_type&
        LightweightDomainListManager::GetLightweightDomainListStorage() const noexcept
        {
            return lightweight_domain_list_storage_;
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HXX_
