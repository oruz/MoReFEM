/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Jul 2014 09:57:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/Internal/DomainHelper.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    const std::string& Domain::ClassName()
    {
        static std::string ret("Domain");
        return ret;
    }


    Domain::Domain() : unique_id_parent(nullptr)
    { }


    Domain::Domain(const std::size_t unique_id,
                   const std::vector<std::size_t>& mesh_index,
                   const std::vector<std::size_t>& dimension_list,
                   const std::vector<std::size_t>& mesh_label_index_list,
                   const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list)
    : unique_id_parent(unique_id)
    {
        if (!mesh_index.empty())
        {
            SetConditionType(DomainNS::Criterion::mesh);
            mesh_identifier_ = mesh_index.back();
        }

        if (!dimension_list.empty())
            SetDimensionList(dimension_list);

        if (!mesh_label_index_list.empty())
            SetLabelList(mesh_label_index_list);

        if (!geometric_type_list.empty())
            SetRefGeometricEltIdList(geometric_type_list);
    }


    Domain::Domain(std::size_t unique_id,
                   const std::size_t mesh_index,
                   const std::vector<std::size_t>& mesh_label_index_list)
    : unique_id_parent(unique_id)
    {
        SetConditionType(DomainNS::Criterion::mesh);
        mesh_identifier_ = mesh_index;

        SetLabelList(mesh_label_index_list);
    }


    bool Domain::IsGeometricEltInside(const GeometricElt& geometric_element) const
    {
        if (!CheckConstraintIfRelevant<DomainNS::Criterion::mesh>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<DomainNS::Criterion::dimension>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<DomainNS::Criterion::label>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<DomainNS::Criterion::geometric_elt_type>(geometric_element))
            return false;

        return true;
    }


    bool Domain::DoRefGeomEltMatchCriteria(const RefGeomElt& geom_ref_element) const
    {
        if (!CheckConstraintIfRelevant<DomainNS::Criterion::dimension>(geom_ref_element))
            return false;

        if (!CheckConstraintIfRelevant<DomainNS::Criterion::geometric_elt_type>(geom_ref_element))
            return false;

        return true;
    }


    void Domain::SetDimensionList(const std::vector<std::size_t>& dimension_list)
    {
        dimension_list_ = dimension_list;
        std::sort(dimension_list_.begin(), dimension_list_.end());
        SetConditionType(DomainNS::Criterion::dimension);
    }


    void Domain::SetRefGeometricEltIdList(const std::vector<Advanced::GeomEltNS::GenericName>& name_list)
    {
        assert(!name_list.empty() && "Shouldn't have been called!");

        {
            geometric_type_list_.reserve(name_list.size());

            const auto& factory = Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__);

            for (const auto& name : name_list)
                geometric_type_list_.push_back(factory.GetIdentifier(name));

            std::sort(geometric_type_list_.begin(), geometric_type_list_.end());
        }

        SetConditionType(DomainNS::Criterion::geometric_elt_type);
    }


    void Domain::SetMesh(const std::vector<std::size_t>& mesh_index_list)
    {
        assert(!mesh_index_list.empty() && "Shouldn't have been called!");

        if (mesh_index_list.size() > 2ul)
            throw Exception("A domain was defined with two or more meshes; it is not currently foreseen! "
                            "(currently either only one or no mesh restriction at all are accepted).",
                            __FILE__,
                            __LINE__);

        assert(mesh_identifier_ == NumericNS::UninitializedIndex<std::size_t>()
               && "This private method should be called only once in the constructor!");
        SetConditionType(DomainNS::Criterion::mesh);
        mesh_identifier_ = mesh_index_list.back();
    }


    void Domain::SetLabelList(const std::vector<std::size_t>& label_index_list)
    {
        assert(!label_index_list.empty() && "Shouldn't have been called!");

        if (!IsConstraintOn<DomainNS::Criterion::mesh>())
            throw Exception(
                "As mesh labels are very closely related to a mesh, a mesh must be defined!", __FILE__, __LINE__);

        const auto& mesh = GetMesh();

        const auto& label_list = mesh.GetLabelList();

        const auto begin = label_list.cbegin();
        const auto end = label_list.cend();

        for (auto mesh_label_index_to_keep : label_index_list)
        {
            auto it = std::find_if(begin,
                                   end,
                                   [mesh_label_index_to_keep](const MeshLabel::const_shared_ptr& label_ptr)
                                   {
                                       assert(!(!label_ptr));
                                       return label_ptr->GetIndex() == mesh_label_index_to_keep;
                                   });

            if (it != end)
                mesh_label_list_.push_back(*it);
            else
                std::cout << "[WARNING] Mesh label " << mesh_label_index_to_keep
                          << " isn't actually present in the "
                             "mesh "
                          << mesh.GetUniqueId()
                          << "! (if run in parallel and the label is found on at least "
                             "one other rank you may dismiss this warning)."
                          << std::endl;
        }

        std::sort(mesh_label_list_.begin(),
                  mesh_label_list_.end(),
                  Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());

        SetConditionType(DomainNS::Criterion::label);
    }


    void Domain::SetConditionType(DomainNS::Criterion constraint_type)
    {
        are_constraints_on_.set(static_cast<std::size_t>(constraint_type));
    }


    const Mesh& Domain::GetMesh() const
    {
        return Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetMesh(GetMeshIdentifier());
    }


    namespace // anonymous
    {


        /*!
         * \brief Compute the number of processor-wise \a Coords in a \a Domain.
         *
         * \param[in] domain Domain considered.
         * \param[in] mesh Mesh in which the domain is enclosed.
         * \param[in] only_lowest_rank If true, a \a Coords is counted only if it is the lowest-rank processor on which
         * it appears as processor-wise.
         *
         * \return Number of Coords in the domain.
         */
        std::size_t NprocWiseCoordsInDomain(const Domain& domain, const Mesh& mesh, const bool only_lowest_rank)
        {
            const auto& processor_wise_coords_list = mesh.GetProcessorWiseCoordsList();
            const auto domain_unique_id = domain.GetUniqueId();

            std::size_t Ncoords_in_domain = 0ul;

            for (const auto& coords_ptr : processor_wise_coords_list)
            {
                assert(!(!coords_ptr));
                const auto& coords = *coords_ptr;

                if (coords.IsInDomain(domain_unique_id))
                {
                    if (!only_lowest_rank || coords.IsLowestProcessorRank())
                        ++Ncoords_in_domain;
                }
            }

            return Ncoords_in_domain;
        }


    } // namespace


    template<>
    std::size_t NcoordsInDomain<MpiScale::processor_wise>(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                          const Domain& domain,
                                                          const Mesh& mesh)
    {
        static_cast<void>(mpi);
        return NprocWiseCoordsInDomain(domain, mesh, false);
    }


    template<>
    std::size_t
    NcoordsInDomain<MpiScale::program_wise>(const ::MoReFEM::Wrappers::Mpi& mpi, const Domain& domain, const Mesh& mesh)
    {
        const auto local_Ncoords_in_domain = NprocWiseCoordsInDomain(domain, mesh, true);

        return mpi.AllReduce(local_Ncoords_in_domain, Wrappers::MpiNS::Op::Sum);
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
