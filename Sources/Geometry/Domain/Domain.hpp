/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Jul 2014 09:57:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HPP_

#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"   // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace DomainNS
    {


        /*!
         * \brief List of possible criteriathat might be used to describe a Domain.
         */
        enum class Criterion
        {
            Begin = 0,
            mesh = Begin,
            dimension,
            geometric_elt_type,
            label,
            End
        };


    } // namespace DomainNS


    /*!
     * \brief This class holds descriptors that can be used to tailor a sub-domain of a Mesh.
     *
     * \internal <b><tt>[internal]</tt></b> At the moment this is merely a prototype; full Domain class should be much
     * more complete and for instance allow intersections, unions of domains. See #162.
     * \endinternal
     *
     * The principle is that for each criterion (dimension, geometric element type, mesh labels at the moment)
     * either Domain includes the list of all values supported, or there is an information inside a bitset that
     * tells there are no constraints on this topic.
     *
     * Objects of this class can only be created through the DomainManager (except the Domain that impose absolutely
     * no restriction).
     */
    class Domain : public Crtp::UniqueId<Domain, UniqueIdNS::AssignationMode::manual, UniqueIdNS::DoAllowNoId::yes>
    {

      public:
        //! Convenient alias.
        using unique_id_parent =
            Crtp::UniqueId<Domain, UniqueIdNS::AssignationMode::manual, UniqueIdNS::DoAllowNoId::yes>;

        //! Alias for unique_ptr.
        using const_unique_ptr = std::unique_ptr<const Domain>;

        //! Vector of unique pointers.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

        //! Frienship to Domain manager, to allow it to create domain objects.
        friend class DomainManager;

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        /// \name Special members.
        ///@{

        //! Default constructor that restricts nothing.
        Domain();

      private:
        /*!
         * \brief Constructor from input data file.
         *
         * This constructor is intended to be used when constructing a domain from the input data file;
         * otherwise it would have been structured differently to avoid many adjacent arguments with the exact
         * same type.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique. It is in the input data file
         * the figure that is in the block name, e.g. 1 for Domain1 = { .... }.
         * \param[in] mesh_index There might be here one index, that indicates in which mesh the domain is defined.
         * If the domain is not limited to one mesh, leave it empty.
         * \param[in] dimension_list List of dimensions to consider. If empty, no restriction on dimension.
         * \param[in] mesh_label_list List of mesh labels to consider. If empty, no restriction on it. This argument
         * must mandatorily be empty if \a mesh_index is empty: a mesh label is closely related to one given mesh.
         * \param[in] geometric_type_list List of geometric element types to consider in the domain. List of elements
         * available is given by Advanced::GeometricEltFactory::GetNameList(); most if not all of them should been
         * displayed in the comment in the input data file.
         */
        Domain(std::size_t unique_id,
               const std::vector<std::size_t>& mesh_index,
               const std::vector<std::size_t>& dimension_list,
               const std::vector<std::size_t>& mesh_label_list,
               const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list);

        /*!
         * \brief Constructor used by \a LightweightDomainList.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique.
         * \param[in] mesh_index Index of the mesh to which the \a Domain is related (mandatory in this case).
         * \param[in] mesh_label_index_list List of mesh labels to consider.
         *
         */
        Domain(std::size_t unique_id,
               const std::size_t mesh_index,
               const std::vector<std::size_t>& mesh_label_index_list);

      public:
        //! Destructor.
        ~Domain() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Domain(const Domain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Domain(Domain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Domain& operator=(const Domain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Domain& operator=(Domain&& rhs) = delete;

        ///@}

        /*!
         * \brief Whether the given geometric element belongs to the domain or not.
         *
         * \param[in] geometric_element \a GeometricElement which status related to the current \a Domain is sought.
         *
         * \return True if it belongs to the current \a Domain.
         */
        bool IsGeometricEltInside(const GeometricElt& geometric_element) const;


        /*!
         * \brief Whether \a geom_ref_element match the criteria of Domain.
         *
         * \param[in] ref_geom_elt RefGeometricElt under investigation.
         *
         * \attention This method does not solely check the geometric_elt_type criterion; it might also
         * check for instance the dimension.
         *
         * The main usage is for instance when a GlobalVariationalOperator is defined: it is handy to be able
         * to restrict an operator for instance to a given set of dimensions (for instance elastic and hyperelastic
         * stiffness make sense only for dimension >= 2).
         *
         * \return True if the \a ref_geom_elt meets all requirements of the domain.
         */
        bool DoRefGeomEltMatchCriteria(const RefGeomElt& ref_geom_elt) const;


        /*!
         * \brief Dimensions consider inside the domain.
         *
         * Relevant only if Criterion::dimension is enforced.
         *
         * \internal <b><tt>[internal]</tt></b> This list is sort in increasing order.
         * \endinternal
         *
         * \return List of all dimensions considered in the Domain.
         */
        const std::vector<std::size_t>& GetDimensionList() const noexcept;


        /// \name Accessors.

        ///@{

      public:
        /*!
         * \brief Mesh labels in the domain.
         *
         * Relevant only if Criterion::label is enforced.
         *
         * \internal <b><tt>[internal]</tt></b> This list is sort in increasing order.
         * \endinternal
         *
         * \return List of all mesh labels considered in the Domain.
         */
        const MeshLabel::vector_const_shared_ptr& GetMeshLabelList() const noexcept;

        /*!
         * \brief Returns the mesh identifier.
         *
         * Relevant only if Criterion::mesh is enforced.
         *
         * \return Unique identifier of the mesh (provided Criterion::mesh is enforced).
         */
        std::size_t GetMeshIdentifier() const noexcept;

        /*!
         * \brief Return the \a Mesh.
         *
         * Relevant only if Criterion::mesh is enforced.
         *
         * \return \a Mesh into which current \a Domain is defined.
         */
        const Mesh& GetMesh() const;

        /*!
         * \brief List of geometric element type handled in the domain.
         *
         * Relevant only if Criterion::geometric_elt_type is enforced.
         *
         * \return List of identifiers of the \a RefGeomElt considered in the Domain.
         */
        const std::vector<Advanced::GeometricEltEnum>& GetRefGeometricEltIdList() const noexcept;


      public:
        /*!
         * \brief Whether the domain imposes a constraint upon the selected criterion.
         *
         * \return True if there is \a CriterionT constraint applied on the domain.
         *
         * \internal By all means, you should treat this as a private member: it is public only because:
         * - \a Parameter needs it for a consistency check.
         * - Defining a friendship to \a Parameter would be cumbersome given its template parameters, relying on
         * non-types defined in Parameter library.
         * \endinternal
         */
        template<DomainNS::Criterion CriterionT>
        bool IsConstraintOn() const noexcept;


      private:
        ///@}


      private:
        /// \name Mutators.

        ///@{

        /*!
         * \brief Set the dimensions to consider in the domain.
         *
         * \param[in] dimension_list List of dimensions to be covered by the domain.
         */
        void SetDimensionList(const std::vector<std::size_t>& dimension_list);


        /*!
         * \brief Set the mesh onto which the domain is defined (if any).
         *
         * \param[in] mesh_unique_id Unique id of the mesh to be covered by the domain.
         * \warning This argument is a vector for conveniency in \a LuaOptionFile but at most one value is expected
         * (and in case this method is called exactly one in fact...).
         */
        void SetMesh(const std::vector<std::size_t>& mesh_unique_id);


        /*!
         * \brief Set the list of geometric element type to consider.
         *
         * \param[in] name_list List of names of the \a RefGeomElt to be considered in the Domain.
         *
         * \internal <b><tt>[internal]</tt></b> "Convert" the list of strings that give the geometric element types to
         * consider in the domain into something more efficient to use.
         * The elements are ordered to fasten access through binary_search.
         * \endinternal
         */
        void SetRefGeometricEltIdList(const std::vector<Advanced::GeomEltNS::GenericName>& name_list);

        /*!
         * \brief Set label list.
         *
         * This operation makes sense only when a mesh has been defined: there is little chance several mesh would share
         * the same mesh labels... So SetMesh() must have been called beforehand.
         *
         * \param[in] label_index_list List of unique ids of the \a MeshLabel to be covered by the domain.
         */
        void SetLabelList(const std::vector<std::size_t>& label_index_list);

        ///@}


      private:
        /// \name Low-level methods of the class, to check validity of operations.


        ///@{

        //! Tells the domain defines a specific kind of constraints.
        //! \param[in] constraint_type A type of constraint the \a current Domain should consider.
        void SetConditionType(DomainNS::Criterion constraint_type);


        /*!
         * \brief Whether a given constraint is fulfilled or not.
         *
         * It is assumed here that \a CriterionT is enforced in the domain.
         *
         * \param[in] object Geometric object upon which the constraint is tested. It might be a GeometricElt
         * or a RefGeomElt.
         *
         * \return True if current constraint is fulfilled in the domain.
         */
        template<DomainNS::Criterion CriterionT, class GeometricObjectT>
        bool IsConstraintFulfilled(const GeometricObjectT& object) const;

        /*!
         * \brief Check if appropriate whether a constraint has been fulfilled for a given geometric object.
         *
         * \tparam CriterionT The criterion being investigated.
         * \tparam GeometricObjectT Either GeometricElt or RefGeomElt.
         *
         * \param[in] object Geometric object upon which the test is performed.
         *
         * \return True either if there are no constraint on this criterion OR if there is one and the \a object
         * passed it correctly.
         */

        template<DomainNS::Criterion CriterionT, class GeometricObjectT>
        bool CheckConstraintIfRelevant(const GeometricObjectT& object) const;


        ///@}


      private:
        /*!
         * \brief Identifier of the mesh upon which the domain is defined.
         *
         * Relevant only if DomainNS::Criterion::mesh is enforced.
         */
        std::size_t mesh_identifier_ = NumericNS::UninitializedIndex<std::size_t>();

        /*!
         * \brief Dimensions consider inside the domain.
         *
         * Relevant only if DomainNS::Criterion::dimension is enforced.
         *
         * \internal <b><tt>[internal]</tt></b> This list is sort in increasing order.
         * \endinternal
         */
        std::vector<std::size_t> dimension_list_;

        /*!
         * \brief Mesh labels in the domain.
         *
         * Relevant only if DomainNS::Criterion::label is enforced.
         *
         * \internal <b><tt>[internal]</tt></b> This list is sort in increasing order.
         * \endinternal
         */
        MeshLabel::vector_const_shared_ptr mesh_label_list_;

        /*!
         * \brief List of geometric element type handled in the domain.
         *
         * Relevant only if DomainNS::Criterion::geometric_elt_type is enforced.
         */
        std::vector<Advanced::GeometricEltEnum> geometric_type_list_;

        /*!
         * \brief Keep track of the conditions upon which the current domain imposes constraints.
         *
         * enum class IsConditionOn is used to access the elements within.
         */
        std::bitset<static_cast<std::size_t>(DomainNS::Criterion::End)> are_constraints_on_;
    };


    /*!
     *
     * \copydoc doxygen_hide_operator_equal
     *
     * Equality is ensured here only with unique ids.
     */
    bool operator==(const Domain& lhs, const Domain& rhs) noexcept;


    /*!
     * \brief Compute the number of \a Coords in a \a Domain.
     *
     * \tparam MpiScaleT Either program_wise or processor_wise.
     *
     * \param[in] mpi Mpi object.
     * \param[in] domain Domain considered.
     * \param[in] mesh Mesh in which the domain is enclosed.
     *
     * \return Number of Coords in the domain.
     *
     * \attention In processor-wise case, \a Coords only related to ghost \a NodeBearer aren't considered in the
     * count here!
     */
    template<MpiScale MpiScaleT>
    std::size_t NcoordsInDomain(const ::MoReFEM::Wrappers::Mpi& mpi, const Domain& domain, const Mesh& mesh);

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/Domain.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_HPP_
