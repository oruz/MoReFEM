/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:54:47 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <utility>

#include "Geometry/Interfaces/EnumInterface.hpp"


namespace // anonymous
{


    // Match a string name to each interface nature.
    const std::map<MoReFEM::InterfaceNS::Nature, std::string>& InterfaceNameMap()
    {
        using namespace MoReFEM::InterfaceNS;

        static std::map<Nature, std::string> ret{ {
            { Nature::vertex, "Vertex" },
            { Nature::edge, "Edge" },
            { Nature::face, "Face" },
            { Nature::volume, "Volume" },
            // undefined intentionally left out!
        } };

        return ret;
    }


} // namespace


namespace MoReFEM
{


    namespace InterfaceNS
    {


        Nature GetNature(const std::string& interface_name)
        {
            const auto& interface_name_map = InterfaceNameMap();

            for (const auto& pair : interface_name_map)
            {
                if (pair.second == interface_name)
                    return pair.first;
            }

            assert(false && "Was called for an interface_name that didn't match an actual interface!");
            return Nature::undefined;
        }


        std::ostream& operator<<(std::ostream& stream, Nature nature)
        {
            switch (nature)
            {
            case MoReFEM::InterfaceNS::Nature::vertex:
            case MoReFEM::InterfaceNS::Nature::edge:
            case MoReFEM::InterfaceNS::Nature::face:
            case MoReFEM::InterfaceNS::Nature::volume:
            {
                const auto& interface_name_map = InterfaceNameMap();

                auto it = interface_name_map.find(nature);
                assert(it != interface_name_map.cend());
                stream << it->second;
                break;
            }
            case MoReFEM::InterfaceNS::Nature::none:
            case MoReFEM::InterfaceNS::Nature::undefined:
                assert(false);
                break;
            }

            return stream;
        }

    } // namespace InterfaceNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
