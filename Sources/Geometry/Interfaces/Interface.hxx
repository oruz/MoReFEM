/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 27 Mar 2014 12:36:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Interface.hpp"

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Interface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class LocalContentT>
    Interface::Interface(const LocalContentT& local_content, const Coords::vector_shared_ptr& elt_coords_list)
    {
        Coords::vector_shared_ptr content;

        for (auto vertex_index : local_content)
        {
            assert(vertex_index < elt_coords_list.size());
            content.push_back(elt_coords_list[vertex_index]);
        }

        // Set the coordinates and shuffle the points.
        SetVertexCoordsList(content);
    }


    template<allow_reset_index AllowResetIndexT>
    void Interface::SetProgramWiseIndex(InterfaceNS::program_wise_index_type id) noexcept
    {
        if constexpr (AllowResetIndexT == allow_reset_index::no)
            assert("Should be assigned only once!"
                   && program_wise_index_ == NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>());

        program_wise_index_ = id;
    }


    inline InterfaceNS::program_wise_index_type Interface::GetProgramWiseIndex() const noexcept
    {
        assert(NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>() != program_wise_index_);
        return program_wise_index_;
    }


    inline const Coords::vector_shared_ptr& Interface::GetVertexCoordsList() const noexcept
    {
#ifndef NDEBUG
        if (GetNature() == InterfaceNS::Nature::volume)
            assert(vertex_coords_list_.empty() && "A specialization should be called for Volume!");
#endif // NDEBUG

        return vertex_coords_list_;
    }


    inline bool operator==(const Interface& lhs, const Interface& rhs) noexcept
    {
        if (lhs.GetNature() != rhs.GetNature())
            return false;

        return lhs.GetProgramWiseIndex() == rhs.GetProgramWiseIndex();
    }


    inline bool operator<(const Interface& lhs, const Interface& rhs) noexcept
    {
        const auto lhs_nature = lhs.GetNature();
        const auto rhs_nature = rhs.GetNature();

        if (lhs_nature != rhs_nature)
            return lhs_nature < rhs_nature;

        return lhs.GetProgramWiseIndex() < rhs.GetProgramWiseIndex();
    }


    inline bool operator!=(const Interface& lhs, const Interface& rhs) noexcept
    {
        return !(operator==(lhs, rhs));
    }


    inline const LocalVector& Interface::GetPseudoNormal() const noexcept
    {
        assert(!(!pseudo_normal_));
        return *pseudo_normal_;
    }

    inline LocalVector& Interface::GetNonCstPseudoNormal() noexcept
    {
        return const_cast<LocalVector&>(GetPseudoNormal());
    }


    inline const std::unique_ptr<LocalVector>& Interface::GetPseudoNormalPtr() const noexcept
    {
        return pseudo_normal_;
    }


    template<CoordsNS::index_enum TypeT>
    std::vector<CoordsNS::index_type<TypeT>> Interface::ComputeVertexCoordsIndexList() const
    {
        using index_type = CoordsNS::index_type<TypeT>;
        decltype(auto) vertex_coords_list = GetVertexCoordsList();
        std::vector<index_type> ret(vertex_coords_list.size());

        std::transform(vertex_coords_list.cbegin(),
                       vertex_coords_list.cend(),
                       ret.begin(),
                       [](const auto& coords_ptr)
                       {
                           assert(!(!coords_ptr));
                           return coords_ptr->template GetIndex<TypeT>();
                       });

        return ret;
    }


    inline Coords::vector_shared_ptr& Interface::GetNonCstVertexCoordsList() noexcept
    {
        return const_cast<Coords::vector_shared_ptr&>(GetVertexCoordsList());
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_
