/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Jun 2013 14:18:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // for string
#include <memory>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /*!
     * \brief Class in charge of the Vertex interface.
     *
     */
    class Vertex final : public Internal::InterfaceNS::TInterface<Vertex, InterfaceNS::Nature::vertex>
    {

      public:
        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<Vertex>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;


        /*!
         * \brief Alias useful when the vertices are built.
         *
         * When a new \a Vertex is built, index is not yet associated to it; one must determine first whether
         * it has already been built by another \a GeometricElt. The type below is used to list all those
         * already built.
         */
        using InterfaceMap = Utilities::PointerComparison::
            Map<Vertex::shared_ptr, std::vector<const GeometricElt*>, InterfaceNS::LessByCoords>;

        //! Friendship.
        friend Mesh;

        //! Name of the class.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] vertex_coords Coords object associated to the Vertex interface.
         */
        explicit Vertex(const Coords::shared_ptr& vertex_coords);

        //! Destructor.
        ~Vertex() override;

        //! \copydoc doxygen_hide_copy_constructor
        Vertex(const Vertex& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Vertex(Vertex&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Vertex& operator=(const Vertex& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Vertex& operator=(Vertex&& rhs) = delete;


        ///@}

      private:
        /*!
         * \brief Compute the pseudo-normal of a vertex.
         *
         * \param[in] geom_elt_list List of the geometric elements that share this vertex.
         *
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list);

      private:
        /*!
         * \brief Number of vertices.
         *
         * \internal <b><tt>[internal]</tt></b> This is a purely internal attribute used in vertices creation; should
         * not be used outside of Vertex.cpp!
         * \endinternal
         */
        static std::size_t Nvertice_;
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Instances/Vertex.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HPP_
