/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Jun 2013 14:18:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <memory>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp" // IWYU pragma: export


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Class in charge of the face interface.
     *
     */
    class Volume final : public Internal::InterfaceNS::TInterface<Volume, InterfaceNS::Nature::volume>
    {

      public:
        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<Volume>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;


        /*!
         * \brief Typedef useful when the vertices are built.
         *
         * Contrary to the other interfaces, comparison below use up the natural ordering of volumes: the index
         * of a Volume may be assigned during construction (it is the same as the GeometricElt's). We could
         * have done the same as in other interfaces, bt it would have been uselessly memory consuming: we would
         * have to create many pointers for each Volume, whereas the same information can simply be stored
         * within a single index.
         */
        using InterfaceMap = Utilities::PointerComparison::Map<Volume::shared_ptr, std::vector<const GeometricElt*>>;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Volume are apart: when they are built the index is already known as we know for sure each Volume is unique
         * for each \a GeometricElt. Due to this close relationship, no \a Coords are stored within a Volume, as it
         * would be a waste of space given the Coords can be retrieved through the GeometricElt object that shares the
         * same index.
         *
         * \param[in] geometric_elt The \a GeometricElt upom which the Volume interface is built.
         *
         */
        explicit Volume(const std::shared_ptr<GeometricElt>& geometric_elt);

        //! Destructor.
        ~Volume() override;

        //! \copydoc doxygen_hide_copy_constructor
        Volume(const Volume& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Volume(Volume&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Volume& operator=(const Volume& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Volume& operator=(Volume&& rhs) = delete;


        ///@}

        //! Get the list of coords that belongs to the interface.
        const Coords::vector_shared_ptr& GetVertexCoordsList() const noexcept override;


      private:
        /*!
         * \brief Number of volumes built so far for each mesh.
         *
         * \internal <b><tt>[internal]</tt></b> This is a purely internal attribute used in volume creation; should not
         * be used outside of Volume.cpp!
         * \endinternal
         *
         * Key is the identifier of the mesh.
         * Value if the number of volumes built so far for each mesh.
         *
         * \return Number of \a Volume interfaces built for each mesh (represented here by their unique identifiers).
         */
        static std::map<std::size_t, std::size_t>& NvolumePerMesh();

        /*!
         * \brief Pointer to the geometric element in which the Volume is built.
         *
         * \internal <b><tt>[internal]</tt></b> For volumes, it would be needlessly costly to store all the Coords as
         * it is done for the other types of interfaces, hence the pointer below that allows to avoid entirely the
         * duplication of the information. Volume class shouldn't try to delete this pointer; it is handled elsewhere.
         * \endinternal
         */
        std::weak_ptr<GeometricElt> geometric_elt_;
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Instances/Volume.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HPP_
