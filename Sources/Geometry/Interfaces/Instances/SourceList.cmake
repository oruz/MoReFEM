### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Edge.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Face.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedEdge.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedFace.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Volume.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Edge.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Edge.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Face.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Face.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedEdge.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedEdge.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedFace.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/OrientedFace.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Volume.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Volume.hxx"
)

