/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 16:11:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/Edge.hpp"              // IWYU pragma: export
#include "Geometry/Interfaces/Internal/ComputeOrientation.hpp" // IWYU pragma: keep
#include "Geometry/Interfaces/Internal/Orientation.hpp"        // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Class used to store an edge inside a \a GeometricElt.
     *
     * An \a Edge may be shared among several \a GeometricElt, but their orientation might differ inside each
     * of the \a GeometricElt. The purpose of the current object is therefore to store two informations:
     * - A shared pointer to an \a Edge.
     * - An integer that gives the orientation.
     */
    class OrientedEdge final : public Crtp::Orientation<OrientedEdge, Edge>
    {

      public:
        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<OrientedEdge>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] edge Edge before any orientation is applied.
         * \param[in] local_edge_index Index of the edge in the local (topology) element; between 0 and Nedge - 1.
         * \param[in] coords_list_in_geom_elt List of \a Coords relared to the considered \a GeometricElt.
         * \param[in] topology_token A token there for a C++ trick: OrientedEdge constructor requires a template
         * parameter for the topology, but I do not want OrientedEdge to be a template class. So the template
         * type must be inferred from the arguments; that's the role of \a topology_token. The user must provide:
         * \code
         * Utilities::TypeToType<TopologyT>()
         * \endcode
         * as argument. To put in a nutshell, Type2Type here allows to use a ver shallow object with almost no
         * construction cost.
         */
        template<class Type2TypeTopologyT>
        explicit OrientedEdge(const Edge::shared_ptr& edge,
                              const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                              std::size_t local_edge_index,
                              Type2TypeTopologyT topology_token);

        //! Destructor.
        ~OrientedEdge() = default;

        //! \copydoc doxygen_hide_copy_constructor
        OrientedEdge(const OrientedEdge& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OrientedEdge(OrientedEdge&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OrientedEdge& operator=(const OrientedEdge& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OrientedEdge& operator=(OrientedEdge&& rhs) = delete;


        ///@}
    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Instances/OrientedEdge.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HPP_
