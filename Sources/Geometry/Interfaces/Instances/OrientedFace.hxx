/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 9 Oct 2014 16:22:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Instances/OrientedFace.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"
#include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"


namespace MoReFEM
{


    template<class Type2TypeTopologyT>
    OrientedFace::OrientedFace(const Face::shared_ptr& face_ptr,
                               const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                               std::size_t local_face_index,
                               Type2TypeTopologyT topology_token)
    : Crtp::Orientation<OrientedFace, Face>(
        face_ptr,
        Internal::InterfaceNS::ComputeFaceOrientation<typename Type2TypeTopologyT::type>(coords_list_in_geom_elt,
                                                                                         local_face_index))
    {
        static_cast<void>(topology_token);
        assert(GetOrientation() < 2u * Type2TypeTopologyT::type::FaceTopology::Nvertex);
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_FACE_HXX_
