/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>
#include <utility>

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Volume.hpp" // IWYU pragma: associated


namespace MoReFEM
{


    const std::string& Volume::ClassName()
    {
        static std::string ret("Volume");
        return ret;
    }


    namespace // anonymous
    {


        /*!
         * \brief Add a new volume to a mesh, and increment accordingly Nvolume_per_mesh.
         *
         * \param[in] mesh_identifier Identifier of the mesh to which the Volume is added.
         * \param[in,out] Nvolume_per_mesh Number of volumes per mesh (represented by its identifier).
         *
         * \return Index of the newly added volume. This index is 0 for first volume, then 1, and so forth...
         */
        ::MoReFEM::InterfaceNS::program_wise_index_type AddVolume(std::size_t mesh_identifier,
                                                                  std::map<std::size_t, std::size_t>& Nvolume_per_mesh);


    } // namespace


    std::map<std::size_t, std::size_t>& Volume::NvolumePerMesh()
    {
        static std::map<std::size_t, std::size_t> ret;
        return ret;
    }


    Volume::Volume(const GeometricElt::shared_ptr& geometric_elt)
    : Internal::InterfaceNS::TInterface<Volume, InterfaceNS::Nature::volume>(), geometric_elt_(geometric_elt)
    {
        assert(!(!geometric_elt));

        decltype(auto) mesh_id = geometric_elt->GetMeshIdentifier();
        SetProgramWiseIndex(AddVolume(mesh_id, NvolumePerMesh()));
    }


    Volume::~Volume() = default;


    const Coords::vector_shared_ptr& Volume::GetVertexCoordsList() const noexcept
    {
        assert(!geometric_elt_.expired()
               && "By design the geometric element should be born by the same processor "
                  "as the volume interface!");
        GeometricElt::shared_ptr shared_ptr = geometric_elt_.lock();

        return shared_ptr->GetCoordsList();
    }


    namespace // anonymous
    {


        ::MoReFEM::InterfaceNS::program_wise_index_type AddVolume(std::size_t mesh_identifier,
                                                                  std::map<std::size_t, std::size_t>& Nvolume_per_mesh)
        {
            auto it = Nvolume_per_mesh.find(mesh_identifier);

            if (it == Nvolume_per_mesh.cend())
            {
                Nvolume_per_mesh.insert({ mesh_identifier, 1u });
                return ::MoReFEM::InterfaceNS::program_wise_index_type{ 0ul };
            } else
            {
                ::MoReFEM::InterfaceNS::program_wise_index_type index{ it->second };
                ++it->second;
                return index;
            }
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
