/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"


namespace MoReFEM
{


    const std::string& OrientedEdge::ClassName()
    {
        static std::string ret("OrientedEdge");
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
