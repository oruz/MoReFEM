/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <array>
#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <string>
#include <type_traits>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Edge.hpp" // IWYU pragma: associated


namespace MoReFEM
{


    const std::string& Edge::ClassName()
    {
        static std::string ret("Edge");
        return ret;
    }


    Edge::~Edge() = default;


    void Edge::ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elts)
    {
        if (pseudo_normal_ == nullptr)
        {
            const std::size_t geom_elts_size = geom_elts.size();

            pseudo_normal_ = std::make_unique<LocalVector>();
            pseudo_normal_->resize({ 3 });

            auto& pseudo_normal = GetNonCstPseudoNormal();

            pseudo_normal.fill(0.);

            std::size_t counter_faces = 0;

            for (std::size_t i = 0; i < geom_elts_size; ++i)
            {
                const auto& geom_elt = *geom_elts[i];

                if (geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3)
                {
                    const auto& face_list = geom_elt.GetOrientedFaceList();
                    const std::size_t face_list_size = face_list.size();

                    if (face_list_size > 1)
                        throw Exception("A triangle has more than one face. This should not happen during "
                                        "pseudo-normals computation.",
                                        __FILE__,
                                        __LINE__);

                    assert(face_list.size() == 1);
                    const auto& face = *face_list[0];

                    const auto& pseudo_normal_face_ptr = face.GetUnorientedInterface().GetPseudoNormalPtr();

                    if (pseudo_normal_face_ptr != nullptr)
                    {
                        pseudo_normal += *pseudo_normal_face_ptr;
                        ++counter_faces;
                    }
                }
            }

            bool check_face =
                false; // #877 Here the mesh Radek gave me does not respect that, ie the surface mesh is not correct
            // but I need to check with him about it, see in matlab the routine to extract the part of the mesh.

            if (counter_faces > 2 && check_face == true)
            {
                for (std::size_t i = 0; i < geom_elts_size; ++i)
                {
                    const auto& geom_elt = *geom_elts[i];

                    std::cout << "Label " << geom_elt.GetMeshLabelPtr()->GetIndex() << std::endl;

                    std::cout << "Index " << geom_elt.GetIndex() << std::endl;

                    const auto& coords_list = geom_elt.GetCoordsList();

                    const std::size_t coords_list_size = coords_list.size();

                    for (std::size_t j = 0; j < coords_list_size; ++j)
                    {
                        const auto& coord = *coords_list[j];

                        coord.ExtendedPrint(std::cout);
                        std::cout << std::endl;
                    }
                }
                throw Exception("An edge is shared by more than two faces. This should not happen during "
                                "pseudo-normals computation.\n"
                                "Number of faces "
                                    + std::to_string(counter_faces) + ".",
                                __FILE__,
                                __LINE__);
            }

            const double norm = std::sqrt(pseudo_normal(0) * pseudo_normal(0) + pseudo_normal(1) * pseudo_normal(1)
                                          + pseudo_normal(2) * pseudo_normal(2));

            if (NumericNS::IsZero(norm))
                throw Exception(
                    "The norm of a pseudo normal on an edge is zero. This should not happen if the mesh is correct.",
                    __FILE__,
                    __LINE__);

            const double one_over_norm = 1. / norm;

            pseudo_normal *= one_over_norm;
        }
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
