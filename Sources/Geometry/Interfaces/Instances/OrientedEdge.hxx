/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 16:11:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Instances/OrientedEdge.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
#include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"


namespace MoReFEM
{


    template<class Type2TypeTopologyT>
    OrientedEdge::OrientedEdge(const Edge::shared_ptr& edge_ptr,
                               const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                               std::size_t local_edge_index,
                               Type2TypeTopologyT topology_token)
    : Crtp::Orientation<OrientedEdge, Edge>(
        edge_ptr,
        Internal::InterfaceNS::ComputeEdgeOrientation<typename Type2TypeTopologyT::type>(coords_list_in_geom_elt,
                                                                                         local_edge_index))
    {
        static_cast<void>(topology_token);
        assert(GetOrientation() < 2u && "2 possible values for edge orientation!");
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_ORIENTED_EDGE_HXX_
