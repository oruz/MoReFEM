/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Sep 2015 10:47:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>
#include <vector>

#include "Geometry/Interfaces/EnumInterface.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            template<class VertexIndexListTypeT>
            LocalInterface::LocalInterface(const VertexIndexListTypeT& vertex_index_list,
                                           InterfaceNS::Nature nature,
                                           const IsInterior is_interior)
            : nature_(nature), is_interior_(is_interior)
            {
                static_assert(!std::is_same<VertexIndexListTypeT, std::false_type>(),
                              "Should be handled by a dedicated constructor.");

#ifndef NDEBUG
                switch (GetNature())
                {
                case InterfaceNS::Nature::vertex:
                case InterfaceNS::Nature::edge:
                case InterfaceNS::Nature::face:
                case InterfaceNS::Nature::volume:
                    break;
                case InterfaceNS::Nature::none:
                case InterfaceNS::Nature::undefined:
                    assert(false
                           && "Invalid nature for a local interface (these values are used for straight interfaces");
                }
#endif // NDEBUG

                // assert(!vertex_index_list.empty()); No: might be empty in case interior interface is being defined.
                vertex_index_list_.resize(vertex_index_list.size());

                std::copy(vertex_index_list.cbegin(), vertex_index_list.cend(), vertex_index_list_.begin());
            }


            inline const std::vector<std::size_t>& LocalInterface::GetVertexIndexList() const noexcept
            {
#ifndef NDEBUG
                switch (GetNature())
                {
                case InterfaceNS::Nature::vertex:
                case InterfaceNS::Nature::edge:
                case InterfaceNS::Nature::face:
                    break;
                case InterfaceNS::Nature::none:
                case InterfaceNS::Nature::undefined:
                case InterfaceNS::Nature::volume:
                    assert(false && "Should not be called for either of these natures!");
                }
#endif // NDEBUG

                assert(!GetIsInterior()
                       && "Should not be called for an interior interface. For such a case, simply take "
                          "all vertices of the geometric element...");

                return vertex_index_list_;
            }


            inline const InterfaceNS::Nature& LocalInterface::GetNature() const noexcept
            {
                return nature_;
            }


            inline IsInterior LocalInterface::GetIsInterior() const noexcept
            {
                return is_interior_;
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HXX_
