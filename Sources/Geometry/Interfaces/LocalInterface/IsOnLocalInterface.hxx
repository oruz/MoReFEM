/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 Oct 2014 14:16:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_IS_ON_LOCAL_INTERFACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_IS_ON_LOCAL_INTERFACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/LocalInterface/IsOnLocalInterface.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            namespace TopologyNS
            {


                template<class TopologyT>
                inline bool IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
                {
                    return ::MoReFEM::RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetVertexCoord(vertex_index)
                           == coords;
                }


                template<class TopologyT>
                bool IsOnEdge_Spectral(std::size_t edge_index, const LocalCoords& coords)
                {
                    using local_data_type = ::MoReFEM::RefGeomEltNS::TopologyNS::LocalData<TopologyT>;

                    // Extract the vertices that delimits the edge.
                    const auto& vertex_on_edge = local_data_type::GetEdge(edge_index);
                    assert(vertex_on_edge.size() == 2ul);

                    // Then their coordinates.
                    const LocalCoords& coords_vertex_1 = local_data_type::GetVertexCoord(vertex_on_edge[0]);
                    const LocalCoords& coords_vertex_2 = local_data_type::GetVertexCoord(vertex_on_edge[1]);

                    // See which is the component that vary between both vertices.
                    const auto mismatched_component = ExtractMismatchedComponentIndex(coords_vertex_1, coords_vertex_2);

                    constexpr auto Ncomponent = Advanced::ComponentNS::index_type{ TopologyT::dimension };
                    assert(Ncomponent.Get() == coords.GetDimension());
                    assert(Ncomponent.Get() == coords_vertex_1.GetDimension());

                    // The other components must remains equal.
                    for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
                    {
                        if (i == mismatched_component)
                        {
                            const double min = std::min(coords_vertex_1[i.Get()], coords_vertex_2[i.Get()]);
                            const double max = std::max(coords_vertex_1[i.Get()], coords_vertex_2[i.Get()]);

                            // Check whether the coordinates in between vertices.
                            if (coords[i.Get()] < min || coords[i.Get()] > max)
                                return false;

                            continue;
                        }

                        if (!NumericNS::AreEqual(coords[i.Get()], coords_vertex_1[i.Get()]))
                            return false;
                    }

                    return true;
                }


            } // namespace TopologyNS


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_IS_ON_LOCAL_INTERFACE_HXX_
