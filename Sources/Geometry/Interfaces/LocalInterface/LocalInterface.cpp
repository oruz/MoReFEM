/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Sep 2015 10:47:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Interfaces/EnumInterface.hpp"
#include <cassert>
#include <cstdlib>
#include <type_traits>

#include "Geometry/Interfaces/LocalInterface/LocalInterface.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            [[noreturn]] LocalInterface::LocalInterface(std::false_type,
                                                        InterfaceNS::Nature nature,
                                                        IsInterior interior)
            : nature_(nature), is_interior_(interior)
            {
                assert(false && "Here only for compile requirements; should never be actually called!");
                exit(EXIT_FAILURE);
            }


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
