/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 9 Oct 2014 17:00:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief This Crtp is used for both Edge and Face interface.
         *
         * \tparam DerivedT Either 'OrientedEdge' or 'OrientedFace'.
         * \tparam UnorientedInterfaceT Respectively 'Edge' and Face' for 'OrientedEdge' and 'OrientedFace'.
         */
        template<class DerivedT, class UnorientedInterfaceT>
        class Orientation
        {

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] interface The underlying interface without any orientation consideration.
             * \param[in] orientation Integer that tag which orientation is actually used. It is numbered from 0:
             * for instance an OrientedEdge might get 0 or 1 (for faces it depends on the nature of the underlying
             * \a GeometricElt).
             */
            explicit Orientation(const typename UnorientedInterfaceT::shared_ptr& interface, std::size_t orientation);


          protected:
            //! Destructor.
            ~Orientation() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Orientation(const Orientation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Orientation(Orientation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Orientation& operator=(const Orientation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Orientation& operator=(Orientation&& rhs) = delete;


            ///@}


          public:
            //! Return the underlying Interface object (without orientation).
            const UnorientedInterfaceT& GetUnorientedInterface() const noexcept;

            //! Return the underlying Interface object (without orientation).
            UnorientedInterfaceT& GetNonCstUnorientedInterface() noexcept;

            //! Return the underlying Interface object as a smart pointer (without orientation).
            typename UnorientedInterfaceT::shared_ptr GetUnorientedInterfacePtr() const noexcept;

            /*!
             * \brief Return the list of \a Coords that delimit the interface.
             *
             * \internal <b><tt>[internal]</tt></b> Orientation unused here; accessible for metaprogramming purposes.
             * \endinternal
             *
             * \return List of \a Coords that delimit the interface.
             */
            const Coords::vector_shared_ptr& GetVertexCoordsList() const noexcept;


            //! Return the orientation.
            std::size_t GetOrientation() const noexcept;

            //! Get the identifier associated to the unoriented interface.
            InterfaceNS::program_wise_index_type GetProgramWiseIndex() const noexcept;

            //! Nature of the Interface as a static method.
            constexpr static InterfaceNS::Nature StaticNature() noexcept;

            /*!
             * \brief Print the underlying coords list and the orientation.
             *
             * \copydoc doxygen_hide_stream_inout
             */
            void Print(std::ostream& stream) const;


          private:
            //! Underlying unoriented object.
            typename UnorientedInterfaceT::shared_ptr unoriented_interface_;

            //! Orientation.
            const std::size_t orientation_;
        };


        /*!
         * \copydoc doxygen_hide_operator_less
         *
         * The convention is that the ordering is the same as the one of underlying Interface; in case of equality
         * orientation is also considered.
         */
        template<class DerivedT, class UnorientedInterfaceT>
        bool operator<(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                       const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept;


        /*!
         * \copydoc doxygen_hide_operator_equal
         *
         * Two Orientation<DerivedT, UnorientedInterfaceT> objects are equal if they share the same underlying Interface
         * and the same orientation.
         */
        template<class DerivedT, class UnorientedInterfaceT>
        bool operator==(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                        const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept;


    } //  namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Internal/Orientation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HPP_
