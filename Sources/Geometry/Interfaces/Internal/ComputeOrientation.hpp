/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Oct 2014 11:35:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Interfaces/Instances/Edge.hpp"
#include "Geometry/Interfaces/Instances/Face.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            /*!
             * \class doxygen_hide_coords_list_in_geom_elt_arg
             *
             * \param[in] coords_list_in_geom_elt List of all \a Coords of a given \a GeometricElt.
             */


            /*!
             * \brief Compute the orientation of a given edge.
             *
             * \tparam TopologyT Topology considered.
             *
             * \copydoc doxygen_hide_coords_list_in_geom_elt_arg
             * \param[in] local_edge_index Index of the local edge for which orientation is sought (must be
             * in [0, TopologyT::Nedge[).
             *
             * \return Index that tag the orientation (0 or 1 for an edge).
             */
            template<class TopologyT>
            std::size_t ComputeEdgeOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                               std::size_t local_edge_index);


            /*!
             * \brief Compute the orientation of a given face.
             *
             * \tparam TopologyT Topology considered.
             *
             * \copydoc doxygen_hide_coords_list_in_geom_elt_arg
             * \param[in] local_face_index Index of the local face for which orientation is sought (must be
             * in [0, TopologyT::Nface[).
             *
             * \return Index that tag the orientation.
             */
            template<class TopologyT>
            std::size_t ComputeFaceOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                               std::size_t local_face_index);


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Internal/ComputeOrientation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HPP_
