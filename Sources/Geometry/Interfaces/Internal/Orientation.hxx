/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 9 Oct 2014 17:00:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Internal/Orientation.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT, class UnorientedInterfaceT>
        Orientation<DerivedT, UnorientedInterfaceT>::Orientation(
            const typename UnorientedInterfaceT::shared_ptr& interface,
            std::size_t orientation)
        : unoriented_interface_(interface), orientation_(orientation)
        { }


        template<class DerivedT, class UnorientedInterfaceT>
        inline const UnorientedInterfaceT&
        Orientation<DerivedT, UnorientedInterfaceT>::GetUnorientedInterface() const noexcept
        {
            assert(!(!unoriented_interface_));
            return *unoriented_interface_;
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline UnorientedInterfaceT&
        Orientation<DerivedT, UnorientedInterfaceT>::GetNonCstUnorientedInterface() noexcept
        {
            return const_cast<UnorientedInterfaceT&>(GetUnorientedInterface());
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline typename UnorientedInterfaceT::shared_ptr
        Orientation<DerivedT, UnorientedInterfaceT>::GetUnorientedInterfacePtr() const noexcept
        {
            assert(!(!unoriented_interface_));
            return unoriented_interface_;
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline const Coords::vector_shared_ptr&
        Orientation<DerivedT, UnorientedInterfaceT>::GetVertexCoordsList() const noexcept
        {
            return GetUnorientedInterface().GetVertexCoordsList();
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline std::size_t Orientation<DerivedT, UnorientedInterfaceT>::GetOrientation() const noexcept
        {
            return orientation_;
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline InterfaceNS::program_wise_index_type
        Orientation<DerivedT, UnorientedInterfaceT>::GetProgramWiseIndex() const noexcept
        {
            return GetUnorientedInterface().GetProgramWiseIndex();
        }


        template<class DerivedT, class UnorientedInterfaceT>
        inline constexpr InterfaceNS::Nature Orientation<DerivedT, UnorientedInterfaceT>::StaticNature() noexcept
        {
            return UnorientedInterfaceT::StaticNature();
        }


        template<class DerivedT, class UnorientedInterfaceT>
        void Orientation<DerivedT, UnorientedInterfaceT>::Print(std::ostream& stream) const
        {
            GetUnorientedInterface().Print(stream);
            stream << ", orientation = " << GetOrientation();
        }


        template<class DerivedT, class UnorientedInterfaceT>
        bool operator<(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                       const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept
        {
            const auto& lhs_unoriented_interface = lhs.GetUnorientedInterface();
            const auto& rhs_unoriented_interface = rhs.GetUnorientedInterface();

            if (lhs_unoriented_interface != rhs_unoriented_interface)
                return lhs_unoriented_interface < rhs_unoriented_interface;

            return lhs.GetOrientation() < rhs.GetOrientation();
        }


        template<class DerivedT, class UnorientedInterfaceT>
        bool operator==(const Orientation<DerivedT, UnorientedInterfaceT>& lhs,
                        const Orientation<DerivedT, UnorientedInterfaceT>& rhs) noexcept
        {
            return (lhs.GetOrientation() == rhs.GetOrientation()
                    && lhs.GetUnorientedInterface() == rhs.GetUnorientedInterface());
        }


    } //  namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_HXX_
