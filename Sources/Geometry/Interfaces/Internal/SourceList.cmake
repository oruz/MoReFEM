### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceListHelper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceListHelper.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeOrientation.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeOrientation.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/OrderCoordsList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/OrderCoordsList.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Orientation.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Orientation.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/TInterface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TInterface.hxx"
)

