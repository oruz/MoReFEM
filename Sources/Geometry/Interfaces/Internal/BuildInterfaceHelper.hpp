/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class GeometricElt; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            /*!
             * \brief Returns the list of interfaces for the current GeometricElt.
             *
             * \copydetails doxygen_hide_oriented_interface_tparam
             * Volume interface is clearly not relevant here.
             *
             * \internal <b><tt>[internal]</tt></b> GeometricElt is in charge of this information, but I need such a
             * templated free function for metaprogramming purposes.
             * There are no generic instantiation on purpose: what matters is the specialization for each type of
             * interface.
             * \endinternal
             *
             * There are no specialization for Volume on purpose: volume is not stored as a vector of shared_pointer
             * (because there is at most one value), so providing a const ref would be tricky (as a matter of
             * fact the function that needs this function is specialized and implemented differently for Volume).
             *
             * \param[in] geometric_element \a GeometricElt for which list of interfaces is requested.
             *
             * \return List of interfaces (with their orientation) for the current \a GeometricElt.
             */
            template<class OrientedInterfaceT>
            const typename OrientedInterfaceT::vector_shared_ptr&
            GetInterfaceOfGeometricElt(const GeometricElt& geometric_element);


            namespace Impl
            {


                /*!
                 * \brief Helper class to create a new Vertex, Edge or Face interface...
                 *
                 * ... given the list of coordinates in the geometric element and the index of the
                 * chosen interface in the local topologic object.
                 *
                 * \tparam InterfaceT An Interface object, which derives from the namesake base class. Not
                 * OrientedEdge or OrientedFace: orientation is relevant only at GeometricElt scope, whereas
                 * here we create Interfaces at mesh scope.
                 *
                 * For instance,
                 * \code
                 * CreateNewInterface<Edge, TopologyNS::Triangle>::Perform(coords_in_geom_elt, 2)
                 * \endcode
                 * will create the third edge of the geometric element which coordinates are given as first argument.
                 */
                template<class InterfaceT, class TopologyT>
                struct CreateNewInterface;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN


                template<class TopologyT>
                struct CreateNewInterface<Vertex, TopologyT>
                {


                    static Vertex::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                      std::size_t local_vertex_index);
                };


                template<class TopologyT>
                struct CreateNewInterface<Edge, TopologyT>
                {


                    static Edge::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                    std::size_t local_edge_index);
                };


                template<class TopologyT>
                struct CreateNewInterface<Face, TopologyT>
                {


                    static Face::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                    std::size_t local_face_index);
                };
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            } // namespace Impl


            /*!
             * \brief Helper class to build or retrieve interfaces related to a given GeometricElt.
             *
             *
             * If the interface is irrelevant to the topology (for instance Face for a Segment) static function
             * Perform() simply returns an empty vector.
             *
             * If not, for each \a InterfaceT of the local topology:
             * - Its counterpart on the GeometricElt is generated (only coords of GeometricElt appear).
             * - If it already exists, add to the vector the existing one.
             * - If not, add the newly created interface to the vector.
             *
             *
             * \tparam InterfaceT Vertex, Edge, Face or Volume.
             * \tparam TopologyT Topology considered (one of the class defined within TopologyNS namespace).
             */
            template<class InterfaceT, class TopologyT>
            class Build
            {
              private:
                //! Convenient alias.
                using vector_shared_ptr = typename InterfaceT::vector_shared_ptr;

                //! Convenient alias.
                using InterfaceMap = typename InterfaceT::InterfaceMap;


              public:
                /*!
                 * \brief Static function that actually does the job (see class-wise explanation).
                 *
                 * \param[in] coords_in_geometric_elt Coords objects related to the geometric element for which a
                 * new interface is to be built or retrieved.
                 * \param[in,out] already_existing_interface_list List of \a InterfaceT already built. If a new
                 * interface is built during the course of the method, it will be added in this list.
                 * The nature of the comparison performed in \a InterfaceMap depends upon the nature of the
                 * interface (Volume differs from the three others here).
                 * \param[in] geom_elt_ptr \a GeometricElt upon which interface is built.
                 *
                 * \return List of interfaces of \a InterfaceT if relevant, or an empty vector if not.
                 *
                 */
                static vector_shared_ptr Perform(const GeometricElt* geom_elt_ptr,
                                                 const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                 InterfaceMap& already_existing_interface_list);

              private:
                //! Returns the number of \a InterfaceT expected in a \a TopologyT object.
                static constexpr std::size_t Ninterface();
            };


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HPP_
