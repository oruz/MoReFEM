/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            template<>
            const Vertex::vector_shared_ptr& GetInterfaceOfGeometricElt<Vertex>(const GeometricElt& geometric_element)
            {
                return geometric_element.GetVertexList();
            }


            template<>
            const OrientedEdge::vector_shared_ptr&
            GetInterfaceOfGeometricElt<OrientedEdge>(const GeometricElt& geometric_element)
            {
                return geometric_element.GetOrientedEdgeList();
            }


            template<>
            const OrientedFace::vector_shared_ptr&
            GetInterfaceOfGeometricElt<OrientedFace>(const GeometricElt& geometric_element)
            {
                return geometric_element.GetOrientedFaceList();
            }


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
