/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    template<class InterfaceT, class TopologyT>
    typename InterfaceT::vector_shared_ptr
    Build<InterfaceT, TopologyT>::Perform(const GeometricElt* geom_elt_ptr,
                                          const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                          typename InterfaceT::InterfaceMap& already_existing_interface_list)
    {
        if constexpr (Ninterface() == 0)
            return typename InterfaceT::vector_shared_ptr{};

        typename InterfaceT::vector_shared_ptr ret;
        constexpr std::size_t Ninterface = Build<InterfaceT, TopologyT>::Ninterface();

        for (std::size_t i = 0ul; i < Ninterface; ++i)
        {
            auto item_ptr = Impl::CreateNewInterface<InterfaceT, TopologyT>::Perform(coords_in_geometric_elt, i);

            // Look whether the interface already existed; the criterion is to match or not the coord list
            // (if 2 interfaces get the same list of coord then they are actually the same).
            auto it = already_existing_interface_list.find(item_ptr);

            ::MoReFEM::InterfaceNS::program_wise_index_type new_index{ already_existing_interface_list.size() };

            if (it == already_existing_interface_list.cend())
            {
                // Give a unique index to the new interface: the number inside the container so far.
                item_ptr->SetProgramWiseIndex(new_index++);

                ret.push_back(item_ptr);
                std::vector<const GeometricElt*> vector_raw;
                vector_raw.push_back(geom_elt_ptr);
                already_existing_interface_list.insert({ item_ptr, vector_raw });
            } else
            {
                auto& pair = *it;
                ret.push_back(pair.first); // put in the container the already existing interface.
                pair.second.push_back(geom_elt_ptr);
            }
        }

        return ret;
    }


    template<class InterfaceT, class TopologyT>
    constexpr std::size_t Build<InterfaceT, TopologyT>::Ninterface()
    {
        return MoReFEM::InterfaceNS::Ninterface<TopologyT, InterfaceT::StaticNature()>::Value();
    }


    namespace Impl
    {


        template<class TopologyT>
        Vertex::shared_ptr
        CreateNewInterface<Vertex, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                       std::size_t local_vertex_index)
        {
            const std::size_t index = local_vertex_index;
            assert(index < coords_in_geometric_elt.size());

            return std::make_shared<Vertex>(coords_in_geometric_elt[index]);
        }


        template<class TopologyT>
        Edge::shared_ptr
        CreateNewInterface<Edge, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                     std::size_t local_edge_index)
        {
            const auto& local_edge = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetEdge(local_edge_index);

            return std::make_shared<Edge>(local_edge, coords_in_geometric_elt);
        }


        template<class TopologyT>
        Face::shared_ptr
        CreateNewInterface<Face, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                     std::size_t local_face_index)
        {
            const auto& local_face = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);

            return std::make_shared<Face>(local_face, coords_in_geometric_elt);
        }


    } // namespace Impl


} // namespace MoReFEM::Internal::InterfaceNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_
