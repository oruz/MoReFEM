/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Oct 2014 11:35:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            template<class TopologyT>
            std::size_t ComputeEdgeOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                               const std::size_t local_edge_index)
            {
                static_assert(!std::is_same<typename TopologyT::EdgeContent, std::false_type>::value,
                              "This free function shouldn't be called when EdgeContent is irrelevant!");

                const auto& topology_edge =
                    ::MoReFEM::RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetEdge(local_edge_index);

                assert(topology_edge.size() == 2ul);

                assert(topology_edge[0] < coords_list_in_geom_elt.size());
                assert(topology_edge[1] < coords_list_in_geom_elt.size());

                const auto& vertex_0 = coords_list_in_geom_elt[topology_edge[0]];
                const auto& vertex_1 = coords_list_in_geom_elt[topology_edge[1]];

                assert(!(!vertex_0));
                assert(!(!vertex_1));

                if (*vertex_1 < *vertex_0)
                    return 1u;

                return 0ul;
            }


            template<class TopologyT>
            std::size_t ComputeFaceOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                               const std::size_t local_face_index)
            {
                static_assert(!std::is_same<typename TopologyT::FaceContent, std::false_type>::value,
                              "This free function shouldn't be called when FaceContent is irrelevant!");

// Following algorithm is a direct adaptation of Ondomatic's one, with a generalization to avoid
// duplicating it for each topology.
#ifndef NDEBUG
                const std::size_t Nvertex = TopologyT::Nvertex;
#endif // NDEBUG

                assert(coords_list_in_geom_elt.size() == Nvertex);

                const std::size_t Npoint_on_face = TopologyT::FaceTopology::Nvertex;

                const auto& topology_face =
                    ::MoReFEM::RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);
                assert(topology_face.size() == Npoint_on_face);

                std::size_t ret = 0ul;

                for (std::size_t i = 1u; i < Npoint_on_face; ++i)
                {
                    assert(topology_face[i] < Nvertex);
                    assert(topology_face[ret] < Nvertex);
                    assert(!(!coords_list_in_geom_elt[topology_face[i]]));
                    assert(!(!coords_list_in_geom_elt[topology_face[ret]]));

                    if (*coords_list_in_geom_elt[topology_face[i]] < *coords_list_in_geom_elt[topology_face[ret]])
                        ret = i;
                }


                {
                    const std::size_t first_index = (ret + Npoint_on_face - 1u) % Npoint_on_face;
                    const std::size_t second_index = (ret + 1u) % Npoint_on_face;

                    assert(first_index < Npoint_on_face);
                    assert(second_index < Npoint_on_face);

                    const std::size_t topology_first_index = topology_face[first_index];
                    const std::size_t topology_second_index = topology_face[second_index];

                    assert(topology_first_index < Nvertex);
                    assert(topology_second_index < Nvertex);
                    assert(!(!coords_list_in_geom_elt[topology_first_index]));
                    assert(!(!coords_list_in_geom_elt[topology_second_index]));

                    if (*coords_list_in_geom_elt[topology_first_index]
                        < *coords_list_in_geom_elt[topology_second_index])
                        ret += Npoint_on_face;
                }

                return ret;
            }


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_COMPUTE_ORIENTATION_HXX_
