/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Jun 2013 12:09:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Internal/TInterface.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {


            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            template<class LocalContentT>
            TInterface<DerivedT, NatureT>::TInterface(const LocalContentT& local_content,
                                                      const Coords::vector_shared_ptr& elt_coords_list)
            : Interface(local_content, elt_coords_list)
            { }


            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            TInterface<DerivedT, NatureT>::TInterface(const Coords::shared_ptr& vertex_coords)
            : Interface(vertex_coords)
            { }


            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            void TInterface<DerivedT, NatureT>::Print(std::ostream& out) const
            {
                std::ostringstream oconv;
                oconv << "Coord in the ";
                oconv << NatureT;
                oconv << " -> [";

                Utilities::PrintContainer<Utilities::PrintPolicyNS::Pointer>::Do(
                    GetVertexCoordsList(),
                    out,
                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                    ::MoReFEM::PrintNS::Delimiter::opener(oconv.str()),
                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

                assert(GetProgramWiseIndex() != NumericNS::UninitializedIndex<decltype(GetProgramWiseIndex())>());

                out << " and id = " << GetProgramWiseIndex();
            }


            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            ::MoReFEM::InterfaceNS::Nature TInterface<DerivedT, NatureT>::GetNature() const noexcept
            {
                return NatureT;
            }


            template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
            constexpr ::MoReFEM::InterfaceNS::Nature TInterface<DerivedT, NatureT>::StaticNature()
            {
                return NatureT;
            }


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_T_INTERFACE_HXX_
