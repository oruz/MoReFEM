/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <sstream>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"


namespace MoReFEM
{


    Interface::Interface(const Coords::shared_ptr& vertex_coords) : vertex_coords_list_({ vertex_coords })
    {
        assert(!(!vertex_coords));
    }


    Interface::Interface()
    { }


    Interface::~Interface() = default;


    void Interface::SetVertexCoordsList(const Coords::vector_shared_ptr& value)
    {
        vertex_coords_list_ = value;
        Internal::InterfaceNS::OrderCoordsList(vertex_coords_list_);
    }


    std::string ShortHand(const Interface& interface)
    {
        std::ostringstream oconv;

        switch (interface.GetNature())
        {
        case InterfaceNS::Nature::vertex:
            oconv << 'V';
            break;
        case InterfaceNS::Nature::edge:
            oconv << 'E';
            break;
        case InterfaceNS::Nature::face:
            oconv << 'F';
            break;
        case InterfaceNS::Nature::volume:
            oconv << 'G';
            break;
        case InterfaceNS::Nature::none:
        case InterfaceNS::Nature::undefined:
            assert(false && "This method should not be called for such objects.");
            exit(EXIT_FAILURE);
        }

        oconv << interface.GetProgramWiseIndex();

        const auto ret = oconv.str();
        return ret;
    }


    namespace InterfaceNS
    {


        std::size_t Hash::operator()(const Interface* const interface_ptr) const
        {
            assert(!(!interface_ptr));
            const auto& interface = *interface_ptr;

            std::size_t ret = std::hash<std::size_t>()(EnumUnderlyingType(interface.GetNature()));

            Utilities::HashCombine(ret, interface.GetProgramWiseIndex());

            return ret;
        }


        bool LessByCoords::operator()(const Interface::shared_ptr& lhs, const Interface::shared_ptr& rhs) const
        {
            assert(!(!lhs));
            assert(!(!rhs));
            assert(lhs->GetNature() == rhs->GetNature());

            const auto& lhs_coord = lhs->GetVertexCoordsList();
            const auto& rhs_coord = rhs->GetVertexCoordsList();
            const auto lhs_size = lhs_coord.size();
            const auto rhs_size = rhs_coord.size();

            if (lhs_size != rhs_size)
                return lhs_size < rhs_size;

            for (std::size_t i = 0ul; i < lhs_size; ++i)
            {
                auto lhs_item = lhs_coord[i];
                auto rhs_item = rhs_coord[i];

                if (lhs_item != rhs_item)
                    return *lhs_item < *rhs_item;
            }

            // If there they are strictly identical, natural answer is false.
            return false;
        }


    } // namespace InterfaceNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
