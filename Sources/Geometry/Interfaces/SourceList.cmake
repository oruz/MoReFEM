### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LocalInterface/SourceList.cmake)
