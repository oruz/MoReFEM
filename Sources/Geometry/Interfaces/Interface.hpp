/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Numeric/Numeric.hpp"                             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MeshNS { class ComputeInterfaceListInMesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterfaceNS
{

    //! Strong type for displacement vectors.
    // clang-format off
    using program_wise_index_type =
        StrongType
        <
            std::size_t,
            struct program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Hashable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::AsMpiDatatype,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::InterfaceNS


namespace MoReFEM
{


    //! Convenient enum to lift a check in a very specific case (reloading from pre-partitioned data)
    enum class allow_reset_index
    {
        no,
        yes
    };


    /*!
     * \brief Polymorphic base of all interface classes.
     *
     * Very little is defined here; definitions really occur in Internal::InterfaceNS::Interface class.
     *
     * \attention It must be noted that \a OrientedEdge and \a OrientedFace are NOT derived from this class: they are
     * separate objects which are built upon (by composition) respectively \a Edge and \a Face which are \a Interface.
     *
     * \internal <b><tt>[internal]</tt></b> This base class is special because we know for sure the number of final
     * classes expected: Vertex, Edge, Face and Volume. That's why for instance there are some constructors here are
     * already "specialized" for one of these cases.
     * \endinternal
     */

    class Interface
    {
      public:
        //! Alias to shared_ptr.
        using shared_ptr = std::shared_ptr<Interface>;

        //! Friendship to the utility class which reorder properly the \a Coords in the \a Interface after the reduction
        //! occurred.
        friend class Internal::MeshNS::ComputeInterfaceListInMesh;


        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor for vertex.
         *
         * \param[in] vertex_coords Coords object associated to the Vertex interface.
         */
        explicit Interface(const Coords::shared_ptr& vertex_coords);

        /*!
         * \brief Constructor for edges and faces.
         *
         * \tparam LocalContentT Type of the container or structure that represents the interface in the
         * local topology. It is given by TopologyT::EdgeContent for the edges and TopologyT::FaceContent
         * for the faces; where TopologyT is the topology of the GeometricElt for which the Interface is built.
         *
         * \param[in] local_content Integer values that represent the interface within the local topology.
         * Is is one of the element of TopologyT::GetEdgeList() or TopologyT::GetFaceList() (see above for the
         * meaning of TopologyT).
         * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the interface
         * is built.
         */
        template<class LocalContentT>
        explicit Interface(const LocalContentT& local_content,
                           const Coords::vector_shared_ptr& coords_list_in_geom_elt);

        /*!
         * \brief Empty constructor (for volumes).
         *
         * Volume are apart: when they are built the index is already known as we know for sure each Volume is unique
         * for each GeometricElt. Due to this close relationship, no Coords are stored within a Volume, as it would be
         * a waste of space given the Coords can be retrieved through the GeometricElt object that shares the same
         * index. Volume objects include weak pointers to its related GeometricElt so that Coords could
         * be retrieved without duplication of data.
         */
        explicit Interface();

        //! Destructor.
        virtual ~Interface();

        //! \copydoc doxygen_hide_copy_constructor
        Interface(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Interface(Interface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Interface& operator=(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Interface& operator=(Interface&& rhs) = delete;

        ///@}

        //! Nature of the Interface.
        virtual InterfaceNS::Nature GetNature() const noexcept = 0;

        /*!
         * \brief Set the identifier associated to this interface.
         *
         * \param[in] id Identifier to be associated to the interface. Identifier is unique for all
         * interfaces of the same nature: there should be for instance only one edge with id = 124.
         */
        template<allow_reset_index AllowResetIndexT = allow_reset_index::no>
        void SetProgramWiseIndex(InterfaceNS::program_wise_index_type id) noexcept;

        //! Get the identifier associated to this interface.
        InterfaceNS::program_wise_index_type GetProgramWiseIndex() const noexcept;

        //! Get the list of \a Coords that belongs to the interface.
        //! \internal This methods is overriden only for \a Volume, as we do not store directly the \a Coords in this
        //! very specific case to avoid redundancy with the \a GeometricElt which already got them.
        virtual const Coords::vector_shared_ptr& GetVertexCoordsList() const noexcept;

        /*!
         * \brief Compute the list of \a Coords indexes that belongs to the interface.
         *
         * \tparam TypeT The type of \a Coords index requested.
         *
         * \return List of \a Coords indexes that belongs to the interface.
         *
         * \internal This method should not be used that much; that is the reason a return by copy has been adopted
         * here.
         */
        template<CoordsNS::index_enum TypeT>
        std::vector<CoordsNS::index_type<TypeT>> ComputeVertexCoordsIndexList() const;

        //! Constant accessor on the pseudo-normal.
        const LocalVector& GetPseudoNormal() const noexcept;

        //! Constant accessor on the pointer of the pseudo-normal.
        const std::unique_ptr<LocalVector>& GetPseudoNormalPtr() const noexcept;


      private:
        /*!
         * \brief Set coords_list_ and sort it so that the same interface used by another geometric element could
         * match it.
         *
         * The convention is to put first the Coords with the lower index, and then choose the rotation that put
         * the lowest possible index in second.
         *
         * For instance let's consider quadrangle 3 5 1 10.
         *
         * The lower is 1, and there are still two possibilities by rotating it:
         * - 1 10 3 5
         * - 1 5 3 10
         * The latter is chosen because 5 is lower than 10.
         *
         * This method is used only for faces and edges: it is obviously unrequired for vertices, and no Coords
         * is stored for Volumes.
         *
         * \param[in] coords_list List of \a Coords, not yet sort.
         *
         * \internal <b><tt>[internal]</tt></b> It is intended to be called only in Edge and Face constructor.
         * \endinternal
         */
        void SetVertexCoordsList(const Coords::vector_shared_ptr& coords_list);

        /*!
         * \brief Non constant accessor to the underlying list of \a Coords.
         *
         * \return Non constant access to the underlying list of \a Coords.
         */
        Coords::vector_shared_ptr& GetNonCstVertexCoordsList() noexcept;

      protected:
        //! Non constant accessor on the pseudo-normal.
        LocalVector& GetNonCstPseudoNormal() noexcept;

      private:
        /*!
         * \brief Coords that belongs to the edge_or_face.
         *
         * The convention is that the first Coords is the one with the lowest index.
         * The relative ordering is local_coords.
         *
         * For instance, if (89, 42, 15) is read in Init(), (15, 89, 42) will be stored.
         *
         * \internal <b><tt>[internal]</tt></b> This information is really important when the interfaces are built:
         * it is the way we can figure out if a given interface already exist or not.
         * \endinternal
         */
        Coords::vector_shared_ptr vertex_coords_list_;

        //! Facility which handles the indexes related to the \a Interface.
        InterfaceNS::program_wise_index_type program_wise_index_ =
            NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>();

      protected:
        //! Pseudo-normal of the interface.
        std::unique_ptr<LocalVector> pseudo_normal_ = nullptr;
    };


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * The convention is that the ordering is done following the coords list in the manner:
     * - Nature are organized in the order: Vertex, Edge, Face, Volume.
     * - If same nature, indexes are compared.
     *
     */
    bool operator<(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Two Interface objects are equal if their nature and their index are the same.
     *
     * Beware: an overload is provided for OrientedEdge and OrientedFace which also takes into account the orientation.
     */
    bool operator==(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_not_equal
     *
     * Defined from operator==.
     */
    bool operator!=(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \brief Gives a short hand to tag the interface, such as 'V15' or 'E105'.
     *
     * The possible letters are:
     * - V (for Vertex)
     * - E (for Edge)
     * - F (for Face)
     * - G (for Volume - V was always taken and G may stand for GeometricElement).
     *
     * The digits are the program-wise index.
     *
     * Such shorthands are used in the context of pre-partitioning: the file with the prepartitioned data uses them up.
     *
     * \param[in] interface \a Interface considered.
     */
    std::string ShortHand(const Interface& interface);


    namespace InterfaceNS
    {


        /*!
         * \brief Functor to act as hash function in an unordered map.
         *
         * The has index is generated from both the nature and the index of the interface.
         */
        struct Hash
        {


            //! Operator() to provide the hash function.
            //! \param[in] interface \a Interface for which the hash is computed.
            std::size_t operator()(const Interface* const interface) const;
        };


        /*!
         * \brief Functor used to identify already existing interfaces when a new interface is built.
         *
         *
         * As a new interface is not yet properly indexed at this point, comparison is performed upon the coords list
         * (which is less efficient so that's why standard operator< is not implemented this way).
         *
         * Due to its expected usage, nullptr is not expected to be one of the arguments of the functor.
         *
         * Likewise, it is expected to be used to compare two interfaces of the same nature; an assert
         * checks that in debug.
         */
        struct LessByCoords
        {


            //! \copydoc doxygen_hide_operator_equal
            bool operator()(const Interface::shared_ptr& lhs, const Interface::shared_ptr& rhs) const;
        };


    } // namespace InterfaceNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Interface.hxx" // IWYU pragma: export

#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HPP_
