/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>


namespace MoReFEM
{


    namespace InterfaceNS
    {


        /*!
         * \brief Nature of the interface that might be considered.
         */
        enum class Nature : std::size_t
        {
            none = 0,
            vertex,
            edge,
            face,
            volume,
            undefined
        };


        /*!
         * \brief Helper class to return at compile time the number of interfaces of \a NatureT for a given \a
         * TopologyT.
         *
         * \tparam TopologyT Topology for which the number of interfaces is requested.
         * \tparam NatureT Nature of the interface.
         */
        template<class TopologyT, Nature NatureT>
        struct Ninterface
        {

            //! Number of intefaces of \a NatureT for \a TopologyT.
            static constexpr std::size_t Value();
        };


        /*!
         * \brief Returns the nature of an interface from its name.
         *
         * If the name doesn't match any interface, Nature::undefined is returned (and an assert is raised in debug).
         * \param[in] interface_name Name of the interface.
         *
         * \return Nature of the interface.
         */
        Nature GetNature(const std::string& interface_name);


        /*!
         * \copydoc doxygen_hide_std_stream_out_overload
         */
        std::ostream& operator<<(std::ostream& stream, Nature rhs);


    } // namespace InterfaceNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/EnumInterface.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HPP_
