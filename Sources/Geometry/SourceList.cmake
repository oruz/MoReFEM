### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/Coords/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Domain/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/GeometricElt/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Interfaces/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Interpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Mesh/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/RefGeometricElt/SourceList.cmake)
