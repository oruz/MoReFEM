/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Jan 2016 12:15:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HXX_
#define MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HXX_

// IWYU pragma: private, include "Operators/Miscellaneous/FindCoordsOfGlobalVector.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    inline const Coords::vector_shared_ptr& FindCoordsOfGlobalVector::GetCoordsList() const noexcept
    {
        return coords_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HXX_
