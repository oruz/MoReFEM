/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Jan 2016 12:15:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HPP_
#define MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HPP_

#include <memory>

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class GlobalVector;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Returns for a global vector a vector if equal size with the Coords on which each dofs are located.
     *
     * This operation is purely processor-wise.
     *
     * \attention At the time being, it is rather crude: it works only for P1. It could be extended relatively easily to
     * P1b, P2, P0, etc... by computing a Coords object at the barycenter of the interface (not done yet as the
     * model that requires this class is purely P1). Higher order is more difficult, as a decision has to be made
     * on the spatial position given to each node of the node bearer_.
     *
     */
    class FindCoordsOfGlobalVector
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FindCoordsOfGlobalVector;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] felt_space \a FEltSpace used as filter.
        //! \param[in] global_vector \a GlobalVector for which \a Coords are retrieved.
        explicit FindCoordsOfGlobalVector(const FEltSpace& felt_space, const GlobalVector& global_vector);

        //! Destructor.
        ~FindCoordsOfGlobalVector() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FindCoordsOfGlobalVector(const FindCoordsOfGlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FindCoordsOfGlobalVector(FindCoordsOfGlobalVector&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FindCoordsOfGlobalVector& operator=(const FindCoordsOfGlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FindCoordsOfGlobalVector& operator=(FindCoordsOfGlobalVector&& rhs) = delete;

        ///@}

        //! Get the list of coordinates.
        const Coords::vector_shared_ptr& GetCoordsList() const noexcept;

      private:
        //! Coords for each of the dofs covered by the numbering subset in the finite element space.
        Coords::vector_shared_ptr coords_list_;
    };


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/Miscellaneous/FindCoordsOfGlobalVector.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_FIND_COORDS_OF_GLOBAL_VECTOR_HPP_
