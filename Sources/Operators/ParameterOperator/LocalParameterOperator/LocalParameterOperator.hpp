/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

namespace MoReFEM
{


    namespace Advanced
    {


        /*!
         * \brief Base class for any \a LocalParameterOperator instance.
         *
         * Such a class does the computation at a local level related to a \a GlobalParameterOperator. For each
         * \a GlobalParameterOperator, there is a \a LocalParameterOperator for each \a RefGeomElt considered in the
         * \a FEltSpace.
         *
         * \tparam TypeT  Type of the parameter (real, vector, matrix).
         */
        // clang-format off
        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
        >
        // clang-format on
        class LocalParameterOperator
        {

          private:
            //! Alias to self.
            using self = LocalParameterOperator<TypeT, TimeDependencyT>;

          public:
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to vector of unique pointers.
            using vector_unique_ptr = std::vector<unique_ptr>;

            //! Alias to elementary data type.
            using elementary_data_type = ElementaryData<OperatorNS::Nature::linear, std::false_type, LocalVector>;


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            //! \param[in] unknown \a ExtendedUnknown upon which the operator is acting.
            //! \param[in] elementary_data \a ElementaryDataImpl child used to store the elementary data needed
            //! for the computation.
            //! \param[in,out] parameter The \a Parameter that the operator is modifying.
            explicit LocalParameterOperator(const ExtendedUnknown::const_shared_ptr& unknown,
                                            elementary_data_type&& elementary_data,
                                            ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter);

            //! Destructor.
            ~LocalParameterOperator() = default;

            //! \copydoc doxygen_hide_copy_constructor
            LocalParameterOperator(const LocalParameterOperator& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            LocalParameterOperator(LocalParameterOperator&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            LocalParameterOperator& operator=(const LocalParameterOperator& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            LocalParameterOperator& operator=(LocalParameterOperator&& rhs) = delete;

            ///@}

          public:
            /*!
             * \brief Link the LocalParameterOperator to a given local finite element space.
             *
             * \param[in] local_felt_space Local finite element space to which the DerivedT will be associated.
             *
             * The data in elementary_data_ (matrices or vector mostly) are allocated at the beginning of the program
             * but their values keeps changing; what makes them change is current method which is therefore called
             * very often (typical assembling iterate through a wide range of local finite element spaces).
             *
             */
            void SetLocalFEltSpace(const LocalFEltSpace& local_felt_space);

            //! Gain access to the elementary data.
            const elementary_data_type& GetElementaryData() const;

            //! Gain non constant access to the elementary data.
            elementary_data_type& GetNonCstElementaryData();

            //! Constant access to the parameter.
            const ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& GetParameter() const noexcept;

            //! Non constant access to the parameter.
            ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& GetNonCstParameter();

          protected:
            //! Return the Unknown and its associated numbering subset.
            const ExtendedUnknown::const_shared_ptr& GetExtendedUnknownPtr() const;

            //! Return the Unknown and its associated numbering subset.
            const ExtendedUnknown& GetExtendedUnknown() const;

          private:
            //! ElementaryData (matrices and/or vectors used for elementary calculation).
            elementary_data_type elementary_data_;

            //! Parameter on which the operator is defined.
            ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_;

            //! Unknown/numbering subset.
            const ExtendedUnknown::const_shared_ptr extended_unknown_;
        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_x_LOCAL_PARAMETER_OPERATOR_HPP_
