/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 May 2015 10:49:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_ENUM_HPP_
#define MOREFEM_x_OPERATORS_x_ENUM_HPP_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace OperatorNS
        {


            //! Possible types of variational operators.
            enum class Nature
            {
                bilinear,
                linear,
                nonlinear
            };


        } // namespace OperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_ENUM_HPP_
