/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Sep 2015 11:26:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include <cassert>
// IWYU pragma: no_include <__tree>
#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <type_traits>
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"
#include "Operators/ConformInterpolator/Internal/ComputePatternHelper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace // anonymous
            {

                // Forward declaration.
                class LocalRowContent;


                /*!
                 * \brief Take a LocalMatrix and returns an expression of its content more easy to use in
                 * ComputePattern() method.
                 */
                std::vector<LocalRowContent> LocalMatrixContent(const LocalMatrix& matrix);


                /*!
                 * \brief Class that interprets in a more usable format the line of a local matrix.
                 */
                class LocalRowContent
                {

                  public:
                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit LocalRowContent() = default;

                    //! Destructor.
                    ~LocalRowContent() = default;

                    //! Copy constructor.
                    LocalRowContent(const LocalRowContent&) = delete;

                    //! Move constructor.
                    LocalRowContent(LocalRowContent&&) = default;

                    ///@}

                    //! Add a new element in the line.
                    void AddNewElement(std::size_t position, PetscScalar value);

                    //! Get the position list.
                    const std::vector<std::size_t>& GetPositionList() const noexcept;

                    //! Get the values list.
                    const std::vector<PetscScalar>& GetValueList() const noexcept;

                    /*!
                     * \brief Yields the position and values of non zero values in the global matrix for current row.
                     *
                     * Indexes are sort increasingly.
                     *
                     * \param[out] global_position_list Position in the global matrix.
                     * \param[out] values_list Values matching each position of \a global_position_list
                     */
                    void ComputeRowPattern(const std::vector<PetscInt>& source_local_2_global,
                                           std::vector<PetscInt>& global_position_list,
                                           std::vector<PetscScalar>& values_list) const;


                  private:
                    //! Position of the non zero values.
                    std::vector<std::size_t> position_list_;

                    //! Non zero values; position of each is given by \a position_list_.
                    std::vector<PetscScalar> value_list_;
                };


            } // namespace


            void
            ComputePatternFromRefGeomElt(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                         const LocalFEltSpace::per_geom_elt_index& source_local_felt_space_list,
                                         const LocalMatrix& local_projection_matrix,
                                         const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data,
                                         std::map<std::size_t, std::vector<PetscInt>>& map_pattern,
                                         std::unordered_map<std::size_t, std::vector<PetscScalar>>& map_values)
            {
                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();

                const auto& source_unknown_storage = source_data.GetExtendedUnknownList();
                const auto& target_unknown_storage = target_data.GetExtendedUnknownList();

                const auto interpreted_local_projection_matrix = LocalMatrixContent(local_projection_matrix);

                const auto Nlocal_row = local_projection_matrix.shape(0);
                assert(interpreted_local_projection_matrix.size() == Nlocal_row);

                const auto& target_felt_space = target_data.GetFEltSpace();

                const auto& target_local_felt_space_list =
                    target_felt_space.GetLocalFEltSpaceList(ref_felt_space.GetRefGeomElt());

                const auto god_of_dof_ptr = target_felt_space.GetGodOfDofFromWeakPtr();
                const auto Nglobal_row = god_of_dof_ptr->NprocessorWiseDof(target_data.GetNumberingSubset());

                const auto end_source_local_felt_space_list = source_local_felt_space_list.cend();

                for (const auto& target_local_felt_space_pair : target_local_felt_space_list)
                {
                    const auto& target_local_felt_space_ptr = target_local_felt_space_pair.second;
                    assert(!(!target_local_felt_space_ptr));
                    const auto& target_local_felt_space = *target_local_felt_space_ptr;

                    const auto& geometric_elt = target_local_felt_space.GetGeometricElt();

                    // Look if local finite element space is present in source local felt space.
                    const auto it_source_local_felt_space_list =
                        source_local_felt_space_list.find(geometric_elt.GetIndex());

                    // If none found skip it: these rows will be filled with zeros.
                    if (it_source_local_felt_space_list == end_source_local_felt_space_list)
                        continue;

                    const auto& source_local_felt_space_ptr = it_source_local_felt_space_list->second;
                    assert(!(!source_local_felt_space_ptr));
                    const auto& source_local_felt_space = *source_local_felt_space_ptr;
                    assert(source_local_felt_space.GetGeometricElt() == geometric_elt);

                    const auto& source_local2global =
                        source_local_felt_space.template GetLocal2Global<MpiScale::program_wise>(
                            source_unknown_storage);

                    const auto& target_local2global =
                        target_local_felt_space.template GetLocal2Global<MpiScale::processor_wise>(
                            target_unknown_storage);

                    assert(source_local2global.size() == local_projection_matrix.shape(1));
                    assert(target_local2global.size() == Nlocal_row);

                    std::vector<PetscInt> global_position_list;
                    std::vector<PetscScalar> global_value_list;

                    for (std::size_t row = 0ul; row < Nlocal_row; ++row)
                    {
                        const auto& local_row_content = interpreted_local_projection_matrix[row];
                        const auto global_row_index = static_cast<std::size_t>(target_local2global[row]);

                        // Skip the ghosts!
                        if (global_row_index >= Nglobal_row)
                            continue;

                        local_row_content.ComputeRowPattern(
                            source_local2global, global_position_list, global_value_list);

                        auto it = map_pattern.find(global_row_index);

                        if (it == map_pattern.cend())
                        {
                            map_pattern.insert({ global_row_index, global_position_list });
                            map_values.insert({ global_row_index, global_value_list });
                        }
#ifndef NDEBUG
                        else
                        {

                            assert(it->second == global_position_list);
                            auto it_values = map_values.find(global_row_index);
                            assert(it_values != map_values.cend());
                            const auto& stored_value_list = it_values->second;
                            const auto size = stored_value_list.size();

                            for (auto i = 0ul; i < size; ++i)
                                NumericNS::AreEqual(stored_value_list[i], global_value_list[i]);
                        }
#endif // NDEBUG
                    }
                }
            }


            namespace // anonymous
            {


                std::vector<LocalRowContent> LocalMatrixContent(const LocalMatrix& matrix)
                {
                    const auto Nrow = matrix.shape(0);
                    const auto Ncol = matrix.shape(1);

                    assert(!::MoReFEM::Wrappers::Xtensor::IsZeroMatrix(matrix));

                    std::vector<LocalRowContent> ret;
                    ret.reserve(Nrow);

                    for (auto row = 0ul; row < Nrow; ++row)
                    {
                        LocalRowContent line_content;

                        for (auto col = 0ul; col < Ncol; ++col)
                        {
                            const auto value = matrix(row, col);

                            if (!NumericNS::IsZero(value))
                            {
                                line_content.AddNewElement(col, static_cast<PetscScalar>(value));
                            }
                        }

                        ret.emplace_back(std::move(line_content));
                    }

                    assert(ret.size() == Nrow);
                    return ret;
                }


                void LocalRowContent::AddNewElement(std::size_t position, PetscScalar value)
                {
                    position_list_.push_back(position);
                    value_list_.push_back(value);
                }


                const std::vector<std::size_t>& LocalRowContent::GetPositionList() const noexcept
                {
                    assert(position_list_.size() == value_list_.size());
                    return position_list_;
                }


                const std::vector<PetscScalar>& LocalRowContent::GetValueList() const noexcept
                {
                    assert(position_list_.size() == value_list_.size());
                    return value_list_;
                }


                void LocalRowContent::ComputeRowPattern(const std::vector<PetscInt>& source_local_2_global,
                                                        std::vector<PetscInt>& global_position_list,
                                                        std::vector<PetscScalar>& global_value_list) const
                {
                    const auto& local_col_list = GetPositionList();
                    const auto& local_value_list = GetValueList();

                    std::vector<PetscInt> unsort_global_position_list(local_col_list.size());

                    std::transform(local_col_list.cbegin(),
                                   local_col_list.cend(),
                                   unsort_global_position_list.begin(),
                                   [&source_local_2_global](std::size_t local_col)
                                   {
                                       assert(local_col < source_local_2_global.size());
                                       return source_local_2_global[local_col];
                                   });

                    // Now sort the indexes by increasing order; same is done for values so that values are correctly
                    // matched to their position.
                    global_position_list = unsort_global_position_list;
                    std::sort(global_position_list.begin(), global_position_list.end());


                    const auto Nelt = local_value_list.size();
                    assert(global_position_list.size() == Nelt);
                    global_value_list.resize(Nelt);

                    const auto begin = unsort_global_position_list.cbegin();
                    const auto end = unsort_global_position_list.cend();


                    for (auto i = 0ul; i < Nelt; ++i)
                    {
                        const auto row_index = global_position_list[i];

                        // Find the position of the index in the list BEFORE sorting occurred.
                        auto it = std::find(begin, end, row_index);
                        assert(it != end);

                        // Deduce the index of it.
                        const auto pos = static_cast<std::size_t>(it - begin);

                        // Position the correct value accordingly.
                        global_value_list[pos] = local_value_list[i];
                    }
                }


            } // namespace


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
