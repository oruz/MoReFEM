/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Sep 2015 17:07:12 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HPP_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HPP_

#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        //! Convenient alias to pairing.
        using pairing_type = std::vector<std::pair<Unknown::const_shared_ptr, Unknown::const_shared_ptr>>;


        namespace Crtp
        {


            /*!
             * \brief CRTP that defines the minimal interface of a LagrangianInterpolator.
             *
             * All lagrangian interpolators should derive from this CRTP.
             */
            template<class DerivedT>
            class LagrangianInterpolator
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = LagrangianInterpolator;


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * An interpolator wants to obtain dof values in (\a target_felt_space, \a target_numbering_subset)
                 * given the ones in (\a source_felt_space, \a source_numbering_subset).
                 *
                 * \param[in] source_felt_space \a FEltSpace of the source.
                 * \param[in] target_felt_space \a FEltSpace of the target.
                 * \param[in] source_numbering_subset \a NumberingSubset of the source.
                 * \param[in] target_numbering_subset \a NumberingSubset of the target.
                 * \param[in] pairing When we consider finite element spaces with several unknowns, we need to tell
                 * the interpolator which unknown on the target space should be linked to another on the source space.
                 * If there is an unknown for which there is no counterpart (e.g. in a (v, p) -> v interpolation) just
                 * ignore it. This cover as well the case (v) -> (v, p).
                 * It should be noticed that for some operators, such as SubsetOrSuperset, for which interpolation
                 * is performed looking solely at dofs objects, this parameter is left empty.
                 */

                explicit LagrangianInterpolator(const FEltSpace& source_felt_space,
                                                const NumberingSubset& source_numbering_subset,
                                                const FEltSpace& target_felt_space,
                                                const NumberingSubset& target_numbering_subset,
                                                pairing_type&& pairing);

                //! Destructor.
                ~LagrangianInterpolator() = default;

                //! \copydoc doxygen_hide_copy_constructor
                LagrangianInterpolator(const LagrangianInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LagrangianInterpolator(LagrangianInterpolator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LagrangianInterpolator& operator=(const LagrangianInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LagrangianInterpolator& operator=(LagrangianInterpolator&& rhs) = delete;

                ///@}

                /*!
                 * \brief Init() function that must be called to compute the interpolation matrix.
                 *
                 * Construction of an interpolator is not really complete until this method is called.
                 *
                 * \param args Variadic argument should the instantiated interpolator need more arguments than the
                 * one provided through the constructor.
                 */
                template<typename... Args>
                void Init(Args&&... args);


                //! Access to interpolation matrix.
                const GlobalMatrix& GetInterpolationMatrix() const noexcept;


              protected:
                //! Access to interpolation data.
                const Advanced::ConformInterpolatorNS::InterpolationData& GetInterpolationData() const noexcept;

                //! Non constant access to interpolation matrix.
                GlobalMatrix& GetNonCstInterpolationMatrix() const noexcept;

                //! God of dof considered; both finite element spaces must belong to it.
                GodOfDof::const_shared_ptr GetGodOfDofFromWeakPtr() const noexcept;

              private:
                /*!
                 * \brief Allocate interpolation matrix.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method should never be called outside of \a Init() method.
                 * \endinternal
                 */
                void AllocateInterpolationMatrix();


              private:
                //! List of nodes eligible for boundary conditions.
                NodeBearer::vector_shared_ptr node_for_boundary_condition_;

                //! Object which stores most relevant data to actually perform the interpolation.
                Advanced::ConformInterpolatorNS::InterpolationData::unique_ptr interpolation_data_ = nullptr;

                //! Global matrix used for the interpolation.
                GlobalMatrix::unique_ptr interpolation_matrix_ = nullptr;

                //! The list of pair source unknown/target unknown to consider.
                pairing_type pairing_;
            };


        } // namespace Crtp


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/ConformInterpolator/Crtp/LagrangianInterpolator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HPP_
