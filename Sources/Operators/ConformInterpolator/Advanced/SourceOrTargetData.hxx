/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Oct 2016 16:22:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HXX_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HXX_

// IWYU pragma: private, include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            inline std::size_t SourceOrTargetData::NunknownComponent() const noexcept
            {
                return Nunknown_component_;
            }


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HXX_
