/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 10 Sep 2015 11:26:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HXX_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HXX_

// IWYU pragma: private, include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"

#include <cassert>

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            inline const SourceOrTargetData& InterpolationData::GetSourceData() const noexcept
            {
                assert(!(!source_data_));
                return *source_data_;
            }


            inline const SourceOrTargetData& InterpolationData::GetTargetData() const noexcept
            {
                assert(!(!target_data_));
                return *target_data_;
            }


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_INTERPOLATION_DATA_HXX_
