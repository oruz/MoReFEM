/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Sep 2015 14:29:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <string>
#include <type_traits>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            SourceOrTargetData::SourceOrTargetData(const FEltSpace& felt_space,
                                                   const NumberingSubset& numbering_subset,
                                                   const pairing_type& pairing,
                                                   SourceOrTargetDataType type)
            : felt_space_(felt_space), numbering_subset_(numbering_subset)
            {
                Unknown::vector_const_shared_ptr unknown_list(pairing.size());

                switch (type)
                {
                case SourceOrTargetDataType::source:
                {
                    std::transform(pairing.cbegin(),
                                   pairing.cend(),
                                   unknown_list.begin(),
                                   [](const auto& pair)
                                   {
                                       return pair.first;
                                   });
                    break;
                }
                case SourceOrTargetDataType::target:
                {
                    std::transform(pairing.cbegin(),
                                   pairing.cend(),
                                   unknown_list.begin(),
                                   [](const auto& pair)
                                   {
                                       return pair.second;
                                   });
                    break;
                }
                }

                const auto mesh_dimension = felt_space.GetMeshDimension();

                for (const auto& unknown_ptr : unknown_list)
                {
                    assert(unknown_ptr != nullptr);

                    auto extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(*unknown_ptr);

                    extended_unknown_list_.push_back(extended_unknown_ptr);

                    Nunknown_component_ +=
                        unknown_ptr->GetNature() == UnknownNS::Nature::vectorial ? mesh_dimension : 1ul;
                }
            }


            const FEltSpace& SourceOrTargetData::GetFEltSpace() const noexcept
            {
                return felt_space_;
            }


            const NumberingSubset& SourceOrTargetData::GetNumberingSubset() const noexcept
            {
                return numbering_subset_;
            }


            const ExtendedUnknown::vector_const_shared_ptr& SourceOrTargetData ::GetExtendedUnknownList() const noexcept
            {
                return extended_unknown_list_;
            }


            const Internal::RefFEltNS::BasicRefFElt& SourceOrTargetData ::GetCommonBasicRefFElt(
                const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space) const
            {

                const auto& ref_felt_list = ref_felt_space.GetRefFEltList();
                const auto& extended_unknown_list = GetExtendedUnknownList();

                // For this method to make sense, all extended unknown must share same unknown and numbering subset.
                assert(std::none_of(extended_unknown_list.cbegin(),
                                    extended_unknown_list.cend(),
                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

                const auto& any_extended_unknown = *(extended_unknown_list.back());

                assert(std::all_of(extended_unknown_list.cbegin(),
                                   extended_unknown_list.cend(),
                                   [&any_extended_unknown](const auto& current_unknown_ptr)
                                   {
                                       return current_unknown_ptr->GetNumberingSubset()
                                                  == any_extended_unknown.GetNumberingSubset()
                                              && current_unknown_ptr->GetShapeFunctionLabel()
                                                     == any_extended_unknown.GetShapeFunctionLabel();
                                   }));

                // Find any ref felt that is related to \a any_extended_unknown.

                auto it = std::find_if(ref_felt_list.cbegin(),
                                       ref_felt_list.cend(),
                                       [&any_extended_unknown](const auto& ref_felt_ptr)
                                       {
                                           assert(ref_felt_ptr != nullptr);
                                           return ref_felt_ptr->GetExtendedUnknown() == any_extended_unknown;
                                       });

                assert(it != ref_felt_list.cend());
                const auto& ref_felt_ptr = *it;
                assert(!(!ref_felt_ptr));
                return ref_felt_ptr->GetBasicRefFElt();
            }


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
