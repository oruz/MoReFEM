/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 15:50:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HXX_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HXX_

// IWYU pragma: private, include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"

#include "Utilities/MatrixOrVector.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace LagrangianNS
        {


            inline const LocalMatrix& LocalLagrangianInterpolator::GetProjectionMatrix() const noexcept
            {
                return local_projection_matrix_;
            }


            inline LocalMatrix& LocalLagrangianInterpolator::GetNonCstProjectionMatrix() noexcept
            {
                return const_cast<LocalMatrix&>(GetProjectionMatrix());
            }


            inline const RefGeomElt& LocalLagrangianInterpolator::GetRefGeomElt() const noexcept
            {
                return ref_geom_elt_;
            }


        } // namespace LagrangianNS


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LOCAL_LAGRANGIAN_INTERPOLATOR_HXX_
