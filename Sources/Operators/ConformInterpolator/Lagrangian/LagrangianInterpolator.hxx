/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 16:27:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HXX_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HXX_

// IWYU pragma: private, include "Operators/ConformInterpolator/Lagrangian/LagrangianInterpolator.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace LagrangianNS
        {


            template<class DerivedT, class ElementaryInterpolatorT>
            template<typename... Args>
            LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::LagrangianInterpolator(
                const FEltSpace& source_felt_space,
                const NumberingSubset& source_numbering_subset,
                const FEltSpace& target_felt_space,
                const NumberingSubset& target_numbering_subset,
                pairing_type&& pairing,
                Args&&... args)
            : parent(source_felt_space,
                     source_numbering_subset,
                     target_felt_space,
                     target_numbering_subset,
                     std::move(pairing),
                     std::forward<decltype(args)>(args)...)
            {
                const auto& interpolation_data = parent::GetInterpolationData();

                source_felt_space.ComputeLocal2Global(interpolation_data.GetSourceData().GetExtendedUnknownList(),
                                                      DoComputeProcessorWiseLocal2Global::yes);
                target_felt_space.ComputeLocal2Global(interpolation_data.GetTargetData().GetExtendedUnknownList(),
                                                      DoComputeProcessorWiseLocal2Global::yes);
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            template<typename... Args>
            void LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::ComputeInterpolationMatrix(Args&&... args)
            {
                // First create the list of all local operators (one per RefLocalFEltSpace).
                // Read them all to compute the pattern of the matrix.
                CreateLocalOperatorList(std::forward<decltype(args)>(args)...);
                std::unordered_map<std::size_t, std::vector<PetscScalar>> map_values;
                auto&& matrix_pattern = ComputePattern(map_values);

                // Then proceed to the initialization of the Petsc matrix.
                const auto& interpolation_data = parent::GetInterpolationData();
                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();

                const auto& row_numbering_subset = target_data.GetNumberingSubset();
                const auto& col_numbering_subset = source_data.GetNumberingSubset();

                auto& interpolation_matrix = parent::GetNonCstInterpolationMatrix();

                const auto& source_felt_space = source_data.GetFEltSpace();

                const auto& mpi = source_felt_space.GetMpi();
                const auto Nproc = mpi.template Nprocessor<int>();

                const auto god_of_dof_ptr = source_felt_space.GetGodOfDofFromWeakPtr();
                assert(!(!god_of_dof_ptr));
                const auto& god_of_dof = *god_of_dof_ptr;

                const std::size_t Nrow_processor_wise_dof = god_of_dof.NprocessorWiseDof(row_numbering_subset);
                const std::size_t Ncolumn_processor_wise_dof = god_of_dof.NprocessorWiseDof(col_numbering_subset);

                if (Nproc == 1)
                {
                    interpolation_matrix.InitSequentialMatrix(
                        Nrow_processor_wise_dof, Ncolumn_processor_wise_dof, matrix_pattern, mpi, __FILE__, __LINE__);
                } else
                {
                    const std::size_t Nrow_program_wise_dof = god_of_dof.NprogramWiseDof(row_numbering_subset);
                    const std::size_t Ncolumn_program_wise_dof = god_of_dof.NprogramWiseDof(col_numbering_subset);

                    interpolation_matrix.InitParallelMatrix(Nrow_processor_wise_dof,
                                                            Ncolumn_processor_wise_dof,
                                                            Nrow_program_wise_dof,
                                                            Ncolumn_program_wise_dof,
                                                            matrix_pattern,
                                                            mpi,
                                                            __FILE__,
                                                            __LINE__);
                }


                // Now report inside the correct values (so far only the pattern has been reported).
                // It is a bit lengthy as dof were marked with their processor-wise indexes, and I need here the
                // program-wise one to be able to use SetValuesRow, which conveniently known where are the non-zero
                // values.
                const auto& dof_list = god_of_dof.GetProcessorWiseDofList();

                const auto end_values = map_values.cend();

                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;

                    if (!dof.IsInNumberingSubset(row_numbering_subset))
                        continue;

                    const auto processor_wise_index = dof.GetProcessorWiseOrGhostIndex(row_numbering_subset);

                    auto it = map_values.find(processor_wise_index);

                    if (it != end_values)
                    {
                        const auto program_wise_index =
                            static_cast<PetscInt>(dof.GetProgramWiseIndex(row_numbering_subset));

                        const auto& row_content = it->second;

                        if (!row_content.empty()) // because empty case induces a Petsc error.
                            interpolation_matrix.SetValuesRow(
                                program_wise_index, row_content.data(), __FILE__, __LINE__);
                    }
                }

                interpolation_matrix.Assembly(__FILE__, __LINE__);
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            Wrappers::Petsc::MatrixPattern LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::ComputePattern(
                std::unordered_map<std::size_t, std::vector<PetscScalar>>& map_values)
            {
                const auto& interpolation_data = parent::GetInterpolationData();
                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();

                const auto& source_felt_storage =
                    source_data.GetFEltSpace()
                        .template GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

                const auto& target_felt_space = target_data.GetFEltSpace();

                std::map<std::size_t, std::vector<PetscInt>> map_pattern;
                assert(map_values.empty());
                map_values.max_load_factor(Utilities::DefaultMaxLoadFactor());

                for (const auto& pair : source_felt_storage)
                {
                    auto& ref_felt_space_ptr = pair.first;
                    assert(!(!ref_felt_space_ptr));
                    const auto& ref_felt_space = *ref_felt_space_ptr;

                    const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();

                    if (!DoConsider(ref_geom_elt))
                        continue;

                    const auto& local_projection_matrix = GetLocalOperator(ref_geom_elt).GetProjectionMatrix();

                    Internal::ConformInterpolatorNS::ComputePatternFromRefGeomElt(ref_felt_space,
                                                                                  pair.second,
                                                                                  local_projection_matrix,
                                                                                  interpolation_data,
                                                                                  map_pattern,
                                                                                  map_values);
                }

                const auto god_of_dof_ptr = target_felt_space.GetGodOfDofFromWeakPtr();
                const auto Nrow = god_of_dof_ptr->NprocessorWiseDof(target_data.GetNumberingSubset());

                std::vector<std::vector<PetscInt>> matrix_pattern(Nrow);

                for (auto& item : map_pattern)
                {
                    assert(item.first < Nrow);

                    assert(matrix_pattern[item.first].empty() && "A given row should be assigned only once!");
                    matrix_pattern[item.first] = std::move(item.second);
                }

                return Wrappers::Petsc::MatrixPattern(matrix_pattern);
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            auto
            LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::GetIterator(const RefGeomElt& ref_geom_elt) const
            {
                return std::find_if(local_operator_list_.cbegin(),
                                    local_operator_list_.cend(),
                                    [&ref_geom_elt](const auto& local_operator_ptr)
                                    {
                                        return local_operator_ptr->GetRefGeomElt() == ref_geom_elt;
                                    });
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            ElementaryInterpolatorT& LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::GetNonCstLocalOperator(
                const RefGeomElt& ref_geom_elt)
            {
                return const_cast<ElementaryInterpolatorT&>(GetLocalOperator(ref_geom_elt));
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            const ElementaryInterpolatorT& LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::GetLocalOperator(
                const RefGeomElt& ref_geom_elt) const
            {
                auto it = GetIterator(ref_geom_elt);
                assert(it != local_operator_list_.cend());
                assert(!(!*it));
                return *(*it);
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            bool
            LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::DoConsider(const RefGeomElt& ref_geom_elt) const
            {
                auto it = GetIterator(ref_geom_elt);
                return it != local_operator_list_.cend();
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            template<typename... Args>
            void LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::CreateLocalOperatorList(Args&&... args)
            {
                const auto& interpolation_data = parent::GetInterpolationData();

                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();

                const auto& target_felt_space = target_data.GetFEltSpace();
                const auto& target_felt_storage =
                    target_felt_space.template GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

                if (target_felt_storage.empty())
                    std::cout << "[WARNING] Finite element space related to operator " << DerivedT::ClassName()
                              << " is empty! "
                                 "It might be due for instance to a non existing label for the dimension considered "
                                 "here (namely "
                              << target_felt_space.GetMeshDimension() << ")." << std::endl;

                const auto& source_felt_space = source_data.GetFEltSpace();

                auto& local_operator_list = GetNonCstLocalOperatorList();


                for (const auto& pair : target_felt_storage)
                {
                    const auto& ref_felt_space_ptr = pair.first;
                    assert(!(!ref_felt_space_ptr));
                    const auto& ref_felt_space = *ref_felt_space_ptr;

                    auto&& local_operator_ptr = std::make_unique<ElementaryInterpolatorT>(
                        source_felt_space, ref_felt_space, interpolation_data, std::forward<decltype(args)>(args)...);

                    local_operator_list.emplace_back(std::move(local_operator_ptr));
                }
            }


            template<class DerivedT, class ElementaryInterpolatorT>
            typename ElementaryInterpolatorT::vector_unique_ptr&
            LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>::GetNonCstLocalOperatorList() noexcept
            {
                return local_operator_list_;
            }


        } // namespace LagrangianNS


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HXX_
