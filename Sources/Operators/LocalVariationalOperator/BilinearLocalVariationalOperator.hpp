/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 14:36:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_

#include "Utilities/MatrixOrVector.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hpp"     // IWYU pragma: export
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief The base class for all bilinear form of local variational operators.
             *
             * \tparam MatrixTypeT Type of the elementary matrix.
             */
            template<class MatrixTypeT>
            class BilinearLocalVariationalOperator
            : public Internal::LocalVariationalOperatorNS::
                  LocalVariationalOperator<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>,
              public Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<
                  BilinearLocalVariationalOperator<MatrixTypeT>>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = BilinearLocalVariationalOperator<MatrixTypeT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Alias to parent.
                using parent = Internal::LocalVariationalOperatorNS::
                    LocalVariationalOperator<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>;

                //! Convenient alias to parent.
                using sub_matrix_parent = Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<self>;

                //! Alias to matrix type.
                using matrix_type = MatrixTypeT;

                //! Alias to elementary data type.
                using elementary_data_type = typename parent::elementary_data_type;


              public:
                /// \name Special members.
                ///@{

                //! \copydoc doxygen_hide_local_var_op_constructor
                explicit BilinearLocalVariationalOperator(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data);

                //! Destructor.
                virtual ~BilinearLocalVariationalOperator() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                BilinearLocalVariationalOperator(const BilinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                BilinearLocalVariationalOperator(BilinearLocalVariationalOperator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                BilinearLocalVariationalOperator& operator=(const BilinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                BilinearLocalVariationalOperator& operator=(BilinearLocalVariationalOperator&& rhs) = delete;

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
