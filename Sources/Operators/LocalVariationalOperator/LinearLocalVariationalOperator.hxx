/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 May 2015 09:04:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            template<class VectorTypeT>
            LinearLocalVariationalOperator<VectorTypeT>::LinearLocalVariationalOperator(
                const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                typename parent::elementary_data_type&& elementary_data)
            : parent(unknown_list, test_unknown_list, std::move(elementary_data))
            { }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_
