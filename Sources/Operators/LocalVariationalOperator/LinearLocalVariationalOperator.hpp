/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 14:36:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief The base class for all linear form of local variational operators.
             *
             * \tparam VectorTypeT Type of the elementary vector.
             */
            template<class VectorTypeT>
            class LinearLocalVariationalOperator
            : public Internal::LocalVariationalOperatorNS::
                  LocalVariationalOperator<OperatorNS::Nature::linear, std::false_type, VectorTypeT>
            {

              public:
                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<LinearLocalVariationalOperator>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Alias to parent.
                using parent = Internal::LocalVariationalOperatorNS::
                    LocalVariationalOperator<OperatorNS::Nature::linear, std::false_type, VectorTypeT>;

                //! Alias to matrix type.
                using matrix_type = VectorTypeT;

                //! Alias to elementary data type.
                using elementary_data_type = typename parent::elementary_data_type;


              public:
                /// \name Special members.
                ///@{

                //! \copydoc doxygen_hide_local_var_op_constructor
                explicit LinearLocalVariationalOperator(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data);

                //! Destructor.
                virtual ~LinearLocalVariationalOperator() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                LinearLocalVariationalOperator(const LinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LinearLocalVariationalOperator(LinearLocalVariationalOperator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LinearLocalVariationalOperator& operator=(const LinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LinearLocalVariationalOperator& operator=(LinearLocalVariationalOperator&& rhs) = delete;

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_LINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
