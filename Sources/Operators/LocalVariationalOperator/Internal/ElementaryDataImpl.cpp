/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:56:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"

namespace MoReFEM
{
    class LocalFEltSpace;
}


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            ElementaryDataImpl::ElementaryDataImpl(const Internal::RefFEltNS::RefLocalFEltSpace& a_ref_felt_space,
                                                   const QuadratureRule& quadrature_rule,
                                                   const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                                   std::size_t felt_space_dimension,
                                                   std::size_t mesh_dimension,
                                                   AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            : ref_felt_space_(a_ref_felt_space), quadrature_rule_(quadrature_rule), Nnode_row_(0ul), Nnode_col_(0ul),
              Ndof_row_(0ul), Ndof_col_(0ul), felt_space_dimension_(felt_space_dimension),
              mesh_dimension_(mesh_dimension)
            {
                assert(felt_space_dimension <= mesh_dimension);

                const auto& ref_felt_space = GetRefLocalFEltSpace();

                const std::size_t Nquadrature_point = quadrature_rule.NquadraturePoint();
                const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();

                InitAllReferenceFiniteElements(unknown_storage, test_unknown_storage);

                infos_at_quad_pt_list_.reserve(Nquadrature_point);

                SetGeomEltDimension();

                Ncoords_in_geom_ref_elt_ = ref_geom_elt.Ncoords();

                point_.resize({ Ncoords_in_geom_ref_elt_, GetGeomEltDimension() });

                // Initialize correctly the informations at each quadrature point.
                for (std::size_t quadrature_point_index = 0; quadrature_point_index < Nquadrature_point;
                     ++quadrature_point_index)
                {
                    const auto& quadrature_point = quadrature_rule.Point(quadrature_point_index);

                    decltype(auto) test_ref_list = GetTestRefFEltList();

                    Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint item(
                        quadrature_point,
                        ref_geom_elt,
                        GetRefFEltList(),
                        test_ref_list,
                        mesh_dimension_,
                        do_allocate_gradient_felt_phi);

                    infos_at_quad_pt_list_.emplace_back(std::move(item));
                }

                assert(infos_at_quad_pt_list_.size() == Nquadrature_point);
            }


            void ElementaryDataImpl::UpdateCoordinates(const GeometricElt& geometric_element)
            {
                const std::size_t Ncomponent = this->GetGeomEltDimension();
                const std::size_t Nnode_in_geometric_element = this->NnodeInRefGeomElt();

                const auto& coords_list = geometric_element.GetCoordsList();
                assert(coords_list.size() == Nnode_in_geometric_element);

                for (std::size_t i = 0; i < Nnode_in_geometric_element; ++i)
                {
                    const auto& current_point_ptr = coords_list[i];
                    assert(!(!current_point_ptr));

                    const auto& current_point = *current_point_ptr;

                    for (std::size_t icoor = 0; icoor < Ncomponent; ++icoor)
                        point_(i, icoor) = current_point[icoor];
                }
            }


            void ElementaryDataImpl::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
            {
                auto& infos_at_quad_pt_list = GetNonCstInformationsAtQuadraturePointList();

                SetCurrentLocalFEltSpace(&local_felt_space);

                for (auto& infos_at_quad_pt : infos_at_quad_pt_list)
                    infos_at_quad_pt.ComputeLocalFEltSpaceData(local_felt_space);
            }


            void ElementaryDataImpl::SetGeomEltDimension()
            {
                geom_elt_dimension_ = GetRefLocalFEltSpace().GetRefGeomElt().GetDimension();
            }


            namespace // anonymous
            {


                void InitRefFEltList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                     const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                     std::size_t& Nnode,
                                     std::size_t& Ndof,
                                     Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ret)
                {
                    assert(Nnode == 0ul);
                    assert(Ndof == 0ul);

                    for (const auto& extended_unknown_ptr : unknown_storage)
                    {
                        const auto& ref_felt = ref_felt_space.GetRefFElt(*extended_unknown_ptr);

                        // No std::make_unique here as friendship is not seen through it.
                        auto ref_felt_in_local_numbering_ptr =
                            new Advanced::RefFEltInLocalOperator(ref_felt, Nnode, Ndof);

                        Nnode += ref_felt_in_local_numbering_ptr->Nnode();
                        Ndof += ref_felt_in_local_numbering_ptr->Ndof();

                        ret.emplace_back(std::move(ref_felt_in_local_numbering_ptr));
                    }
                }


            } // namespace


            void ElementaryDataImpl ::InitAllReferenceFiniteElements(
                const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage)

            {
                const auto& ref_felt_space = GetRefLocalFEltSpace();

                assert(ref_felt_list_.empty());

                assert(Nnode_col_ == 0ul);
                assert(Ndof_col_ == 0ul);
                assert(Nnode_row_ == 0ul);
                assert(Ndof_row_ == 0ul);

                const bool are_test_and_unknown_equal = (unknown_storage == test_unknown_storage);

                InitRefFEltList(ref_felt_space, unknown_storage, Nnode_col_, Ndof_col_, ref_felt_list_);

                if (are_test_and_unknown_equal)
                {
                    Nnode_row_ = Nnode_col_;
                    Ndof_row_ = Ndof_col_;

                    test_ref_felt_list_ = {}; // let optional empty, and accessors will fetch ref_felt_list_ which
                                              // holds the proper content.
                } else
                {
                    test_ref_felt_list_ =
                        std::make_optional(Advanced::RefFEltInLocalOperator::vector_const_unique_ptr());

                    InitRefFEltList(
                        ref_felt_space, test_unknown_storage, Nnode_row_, Ndof_row_, test_ref_felt_list_.value());
                }
            }


            namespace // anonymous
            {


                const Advanced::RefFEltInLocalOperator&
                GetRefFEltImpl(const ExtendedUnknown& unknown,
                               const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list);


            } // namespace


            const Advanced::RefFEltInLocalOperator& ElementaryDataImpl::GetRefFElt(const ExtendedUnknown& unknown) const
            {
                const auto& ref_felt_list = GetRefFEltList();
                return GetRefFEltImpl(unknown, ref_felt_list);
            }


            const Advanced::RefFEltInLocalOperator&
            ElementaryDataImpl::GetTestRefFElt(const ExtendedUnknown& unknown) const
            {
                const auto& test_ref_felt_list = GetTestRefFEltList();
                return GetRefFEltImpl(unknown, test_ref_felt_list);
            }


            namespace // anonymous
            {


                const Advanced::RefFEltInLocalOperator&
                GetRefFEltImpl(const ExtendedUnknown& unknown,
                               const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
                {
                    auto it = std::find_if(ref_felt_list.cbegin(),
                                           ref_felt_list.cend(),
                                           [&unknown](const auto& current_ref_felt)
                                           {
                                               assert(!(!current_ref_felt));
                                               return current_ref_felt->GetExtendedUnknown() == unknown;
                                           });
                    assert(it != ref_felt_list.cend());
                    assert(!(!(*it)));
                    return *(*it);
                }


            } // namespace


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
