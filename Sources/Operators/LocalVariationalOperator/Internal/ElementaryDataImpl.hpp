/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:56:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_

#include <cstddef> // IWYU pragma: keep
#include <optional>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class QuadratureRule; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Defines all of ElementaryData that does not depend upon the kind of object considered (matrix or
             * vector).
             */
            class ElementaryDataImpl
            {


              public:
                /// \name Special members.

                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
                 * list of unknowns involved for the operator related to this ElementaryData will provide all the data
                 * required to build RefFEltInLocalOperator objects that allow easy navigation through the
                 * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the
                 * required data). \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
                 * \param[in] unknown_storage List of unknowns involved in the current local operator. This must be a
                 * subset of the ones in the enclosing FEltSpace; however there is no need to follow the same ordering
                 * (ordering is specified at the LocalVariationalOperator level).
                 * \param[in] test_unknown_storage List  of all the pair test_unknown/numbering subset considered by the operator.
                 * \param[in] felt_space_dimension Dimension considered in the finite element space.
                 * \param[in] mesh_dimension Dimension in the mesh. Must be equal or higher than felt_space_dimension.
                 * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
                 */
                explicit ElementaryDataImpl(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                            const QuadratureRule& quadrature_rule,
                                            const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                            const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                            std::size_t felt_space_dimension,
                                            std::size_t mesh_dimension,
                                            AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

              protected:
                //! Destructor.
                ~ElementaryDataImpl() = default;

              public:
                //! \copydoc doxygen_hide_copy_constructor
                ElementaryDataImpl(const ElementaryDataImpl& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ElementaryDataImpl(ElementaryDataImpl&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                ElementaryDataImpl& operator=(const ElementaryDataImpl& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ElementaryDataImpl& operator=(ElementaryDataImpl&& rhs) = delete;
                ///@}

              public:
                //! Computes internal quantities related to finite element. Must absolutely be called before each finite
                //! element is used; will disappear shortly as it is way too dangerous.
                //! \param[in] local_felt_space \a LocalFEltSpace for which dinternal data are computed.
                void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of nodes.
                 */
                std::size_t NnodeRow() const noexcept;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of nodes.
                 */
                std::size_t NnodeCol() const noexcept;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of dofs.
                 */
                std::size_t NdofRow() const noexcept;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 *
                 * \return Number of dofs.
                 */
                std::size_t NdofCol() const noexcept;

                //! Dimension of the \a GeometricElt.
                std::size_t GetGeomEltDimension() const noexcept;

                //! Number of quadrature points.
                std::size_t NquadraturePoint() const noexcept;

                //! Access the properties shared by all the FElts related to the current local variational operator.
                //! \param[in] unknown \a Unknown sed as filter.
                const Advanced::RefFEltInLocalOperator& GetRefFElt(const ExtendedUnknown& unknown) const;

                //! Access the properties shared by all the FElts related to the current local variational operator.
                //! \param[in] unknown \a Unknown sed as filter.
                const Advanced::RefFEltInLocalOperator& GetTestRefFElt(const ExtendedUnknown& unknown) const;

                //! Number of \a Unknown.
                std::size_t Nunknown() const noexcept;

                //! Number of \a Unknown.
                std::size_t NtestUnknown() const noexcept;

                //! Get the dimension of the finite element space.
                std::size_t GetFEltSpaceDimension() const noexcept;

                //! Get the dimension of the mesh.
                std::size_t GetMeshDimension() const noexcept;

                //! Accessor to informations at each quadrature point (vector index stands for a quadrature point
                //! index).
                const std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>&
                GetInformationsAtQuadraturePointList() const noexcept;

                //! Accessor to a specific element of infos_at_quad_pt_list.
                //! \param[in] quadrature_pt_index Position of the requested quadratuure point in the local storage.
                const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint&
                GetInformationsAtQuadraturePoint(std::size_t quadrature_pt_index) const noexcept;

                //! Access to the current \a LocalFEltSpace considered.
                const LocalFEltSpace& GetCurrentLocalFEltSpace() const noexcept;

                //! Access to the current geometric element considered.
                const GeometricElt& GetCurrentGeomElt() const noexcept;

                //! Returns the quadrature rule to use.
                const QuadratureRule& GetQuadratureRule() const noexcept;


              protected:
                //! \name Access to cached geometric data.
                ///@{

                //! Number of \a LocalNode in current \a RefGeomElt.
                std::size_t NnodeInRefGeomElt() const noexcept;

                ///@}

              private:
                /*!
                 * \brief Get the underlying \a RefGeomElt.
                 */
                const RefGeomElt& GetRefGeomElt() const noexcept;

                /*!
                 * \brief Access the \a RefLocalFEltSpace.
                 *
                 * \internal <b><tt>[internal]</tt></b> This accessor is public but should only be used by
                 * LocalVariationalOperators.
                 * \endinternal
                 *
                 * \return Underlying \a RefLocalFEltSpace.
                 */
                const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace() const noexcept;


              private:
                //! Fill the point_ matrix with the coordinates of the points of the geometric element in the mesh.
                //! \param[in] geometric_element \a GeometricElt for which new coordinates are computed.
                void UpdateCoordinates(const GeometricElt& geometric_element);

                //! Set the number of components and the related sequence.
                void SetGeomEltDimension();

              private:
                //! Non constant accessor to informations at each quadrature point (vector index stands for a
                //! quadrature point index).
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>&
                GetNonCstInformationsAtQuadraturePointList();


                /*!
                 * \brief Init the reference finite elements.
                 *
                 * Numbering subset are completely dropped in this method: they are irrelevant at a local level,
                 * which is where ElementaryData is used.
                 *
                 * \param[in] extended_unknown_list List of \a ExtendedUnknown considered in the current object.
                 * \param[in] test_extended_unknown_list List of \a ExtendedUnknown considered in the current object for
                 * test unknowns.
                 */

                void InitAllReferenceFiniteElements(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list);


                //! Access the list of reference finite elements.
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& GetRefFEltList() const noexcept;

                //! Access the list of reference finite elements.
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& GetTestRefFEltList() const noexcept;

                //! Set the pointer to \a LocalFEltSpace under consideration.
                //! \param[in] local_felt_space Pointer to the \a LocalFEltSpace to consider.
                void SetCurrentLocalFEltSpace(const LocalFEltSpace* local_felt_space);


              private:
                //! Finite element type for which the elementary data are produced.
                const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space_;

                //! Quadrature rule to use.
                const QuadratureRule& quadrature_rule_;

                //! Coordinates of the points defining the element: point_(ipoint,jcoor)  (in the current FE)
                LocalMatrix point_;

                /*!
                 * \brief Stores the informations at quadrature point level.
                 *
                 * The index of the vector stands for a quadrature point.
                 */
                std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint> infos_at_quad_pt_list_;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                std::size_t Nnode_row_;

                /*!
                 * \brief Total number of \a Node in the ElementaryData object.
                 *
                 * Might be less than the number of nodes in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                std::size_t Nnode_col_;


                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                std::size_t Ndof_row_;

                /*!
                 * \brief Total number of \a Dof in the ElementaryData object.
                 *
                 * Might be less than the number of dofs in the RefLocalFEltSpace, as some unknowns of the latter might
                 * have been filtered out.
                 */
                std::size_t Ndof_col_;


                //! Dimension considered in the finite element space.
                const std::size_t felt_space_dimension_;

                //! Dimension in the mesh. Must be equal or higher than felt_space_dimension_.
                const std::size_t mesh_dimension_;

                /*!
                 * \brief LocalFEltSpace under consideration.
                 *
                 * \warning Do not delete this pointer!
                 *
                 */
                const LocalFEltSpace* current_local_felt_space_ = nullptr;


              private:
                //! \name Cached geometric data.

                ///@{

                //! Dimension of the \a GeometricElt.
                std::size_t geom_elt_dimension_ = NumericNS::UninitializedIndex<std::size_t>();


                /*!
                 * \brief Number of Coords object required to describe fully a GeometricElt of this type.
                 *
                 * For instance 27 for an Hexahedron 27 or 1 for a Point.
                 */
                std::size_t Ncoords_in_geom_ref_elt_ = NumericNS::UninitializedIndex<std::size_t>();

                ///@}


              private:
                //! List of reference finite elements.
                Advanced::RefFEltInLocalOperator::vector_const_unique_ptr ref_felt_list_;

                //! List of reference finite elements for test unknowns.
                std::optional<Advanced::RefFEltInLocalOperator::vector_const_unique_ptr> test_ref_felt_list_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HPP_
