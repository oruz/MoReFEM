/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hpp"

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            inline LocalMatrix& SubMatrixForNumberingSubsetPair::GetSubMatrix()
            {
                return local_matrix_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HXX_
