/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 May 2016 14:07:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Crtp that stores one or two \a ExtendedUnknown objects and provide constant accessors to them.
             *
             * \attention This Crtp is rather different from the namesake in GlobalVariationalOperatorNS (even if
             * originally one was used for both).
             */
            template<class DerivedT>
            class ExtendedUnknownAndTestUnknownList
            {

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                //! \param[in] extended_unknown_list List of extended unknowns to use, sort in the same order as in the
                //! \a GlobalVariationalOperator.
                //! \copydoc doxygen_hide_test_extended_unknown_list_param
                explicit ExtendedUnknownAndTestUnknownList(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list);

                //! Destructor.
                ~ExtendedUnknownAndTestUnknownList() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ExtendedUnknownAndTestUnknownList(const ExtendedUnknownAndTestUnknownList& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ExtendedUnknownAndTestUnknownList(ExtendedUnknownAndTestUnknownList&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ExtendedUnknownAndTestUnknownList& operator=(const ExtendedUnknownAndTestUnknownList& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ExtendedUnknownAndTestUnknownList& operator=(ExtendedUnknownAndTestUnknownList&& rhs) = delete;

                ///@}

                //! Return the list of Unknowns and their associated numbering subset.
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList() const noexcept;

                //! Return the list of test Unknowns and their associated numbering subset.
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedTestUnknownList() const noexcept;

              protected:
                //! \copydoc doxygen_hide_crtp_Nunknown_method
                const ExtendedUnknown& GetNthUnknown(std::size_t index = 0) const noexcept;

                //! \copydoc doxygen_hide_crtp_Nunknown_method
                const ExtendedUnknown& GetNthTestUnknown(std::size_t index = 0) const noexcept;

              private:
                //! Unknown/numbering subset list.
                const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list_;

                //! Unknown/numbering subset list.
                const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_
