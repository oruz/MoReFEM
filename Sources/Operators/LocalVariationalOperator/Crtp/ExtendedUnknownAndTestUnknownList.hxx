/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 May 2016 14:07:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            template<class DerivedT>
            ExtendedUnknownAndTestUnknownList<DerivedT>::ExtendedUnknownAndTestUnknownList(
                const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list)
            : extended_unknown_list_(extended_unknown_list), test_extended_unknown_list_(test_extended_unknown_list)
            {
                assert(!extended_unknown_list.empty());
#ifndef NDEBUG
                {
                    const auto& list = GetExtendedTestUnknownList();
                    assert(!list.empty());
                    assert(std::none_of(
                        list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
                }
#endif // NDEBUG
            }


            template<class DerivedT>
            inline const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList() const noexcept
            {
                return test_extended_unknown_list_;
            }


            template<class DerivedT>
            const ExtendedUnknown&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthTestUnknown(std::size_t index) const noexcept
            {
                const auto& list = GetExtendedTestUnknownList();
                assert(index < list.size());
                assert(!(!list[index]));
                return *list[index];
            }


            template<class DerivedT>
            inline const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList() const noexcept
            {
                return extended_unknown_list_;
            }


            template<class DerivedT>
            const ExtendedUnknown&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthUnknown(std::size_t index) const noexcept
            {
                const auto& list = GetExtendedUnknownList();
                assert(index < list.size());
                assert(!(!list[index]));
                return *list[index];
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_
