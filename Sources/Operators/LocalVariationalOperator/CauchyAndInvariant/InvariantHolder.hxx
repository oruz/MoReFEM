/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


//  Invariants.hxx
//
//  Created by Sebastien Gilles on 3/20/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace // anonymous
    {


        template<std::size_t NinvariantsT, std::size_t DimensionT, FiberNS::AtNodeOrAtQuadPt PolicyT>
        [[maybe_unused]] void UpdateHelper(const LocalVector& cauchy_green_tensor,
                                           const QuadraturePoint& quad_pt,
                                           const GeometricElt& geom_elt,
                                           InvariantHolderNS::Content content,
                                           std::array<double, NinvariantsT>& invariants,
                                           std::array<LocalVector, NinvariantsT>& invariants_first_derivative,
                                           std::array<LocalMatrix, 2>& invariants_second_derivative,
                                           bool do_i4_activate,
                                           const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI4,
                                           bool do_i6_activate,
                                           const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI6);


    } // namespace


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    InvariantHolder<NinvariantsT, PolicyT>::InvariantHolder(std::size_t mesh_dimension,
                                                            InvariantHolderNS::Content content)
    : content_(content), mesh_dimension_(mesh_dimension)
    {
        std::size_t size = 0;

        assert((mesh_dimension >= 1 && mesh_dimension <= 3)
               && "Invariants can only be created in dimension 1, 2 or 3.");

        switch (mesh_dimension)
        {
        case 1:
            size = 1ul;
            break;
        case 2:
            size = 3ul;
            break;
        case 3:
            size = 6ul;
            break;
        }

        switch (content)
        {
        case InvariantHolderNS::Content::invariants_and_first_and_second_deriv:
            static_assert(std::tuple_size<decltype(invariants_second_derivative_)>::value == 2ul);
            for (std::size_t i = 0ul; i < 2u; ++i)
            {
                invariants_second_derivative_[i].resize({ size, size });
            }
            [[fallthrough]];
        case InvariantHolderNS::Content::invariants_and_first_deriv:
            static_assert(std::tuple_size<decltype(invariants_first_derivative_)>::value == NinvariantsT);
            for (std::size_t i = 0ul; i < NinvariantsT; ++i)
            {
                invariants_first_derivative_[i].resize({ size });
            }
            [[fallthrough]];
        case InvariantHolderNS::Content::invariants:
            break;
        }
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    void InvariantHolder<NinvariantsT, PolicyT>::Update(const LocalVector& cauchy_green_tensor,
                                                        const QuadraturePoint& quad_pt,
                                                        const GeometricElt& geom_elt)
    {
        const auto mesh_dimension = GetMeshDimension();
        const auto content = GetContent();
        auto& invariants = GetNonCstInvariant();
        auto& invariants_first_derivative = GetNonCstFirstDerivativeWrtCauchyGreen();
        auto& invariants_second_derivative = GetNonCstSecondDerivativeWrtCauchyGreen();

        switch (mesh_dimension)
        {
        case 1u:
            UpdateHelper<NinvariantsT, 1u, PolicyT>(cauchy_green_tensor,
                                                    quad_pt,
                                                    geom_elt,
                                                    content,
                                                    invariants,
                                                    invariants_first_derivative,
                                                    invariants_second_derivative,
                                                    DoI4Activate(),
                                                    GetFibersI4(),
                                                    DoI6Activate(),
                                                    GetFibersI6());
            break;
        case 2u:
            UpdateHelper<NinvariantsT, 2u, PolicyT>(cauchy_green_tensor,
                                                    quad_pt,
                                                    geom_elt,
                                                    content,
                                                    invariants,
                                                    invariants_first_derivative,
                                                    invariants_second_derivative,
                                                    DoI4Activate(),
                                                    GetFibersI4(),
                                                    DoI6Activate(),
                                                    GetFibersI6());
            break;
        case 3u:
            UpdateHelper<NinvariantsT, 3u, PolicyT>(cauchy_green_tensor,
                                                    quad_pt,
                                                    geom_elt,
                                                    content,
                                                    invariants,
                                                    invariants_first_derivative,
                                                    invariants_second_derivative,
                                                    DoI4Activate(),
                                                    GetFibersI4(),
                                                    DoI6Activate(),
                                                    GetFibersI6());
            break;
        default:
        {
            assert(false && "Invariants computation is available only for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }
        }

#ifndef NDEBUG
        are_invariant_set_ = true;
#endif // NDEBUG
    }


    namespace // anonymous
    {


        template<std::size_t NinvariantsT, std::size_t DimensionT, FiberNS::AtNodeOrAtQuadPt PolicyT>
        void UpdateHelper(const LocalVector& cauchy_green_tensor,
                          const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          InvariantHolderNS::Content content,
                          std::array<double, NinvariantsT>& invariants,
                          std::array<LocalVector, NinvariantsT>& invariants_first_derivative,
                          std::array<LocalMatrix, 2>& invariants_second_derivative,
                          bool do_i4_activate,
                          const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI4,
                          bool do_i6_activate,
                          const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI6)
        {
            using namespace InvariantHolderNS;

            switch (content)
            {
            case Content::invariants_and_first_and_second_deriv:
            {
                const auto d2I2dCdC = EnumUnderlyingType(
                    InvariantHolder<NinvariantsT, PolicyT>::invariants_second_derivative_index::d2I2dCdC);

                const auto d2I3dCdC = EnumUnderlyingType(
                    InvariantHolder<NinvariantsT, PolicyT>::invariants_second_derivative_index::d2I3dCdC);

                assert(d2I2dCdC < invariants_second_derivative.size());
                assert(d2I3dCdC < invariants_second_derivative.size());

                SecondDerivativeInvariant2CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                  invariants_second_derivative[d2I2dCdC]);
                SecondDerivativeInvariant3CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                  invariants_second_derivative[d2I3dCdC]);
            }
                [[fallthrough]];
            case Content::invariants_and_first_deriv:
            {
                const auto dI1dC = EnumUnderlyingType(
                    InvariantHolder<NinvariantsT, PolicyT>::invariants_first_derivative_index::dI1dC);

                const auto dI2dC = EnumUnderlyingType(
                    InvariantHolder<NinvariantsT, PolicyT>::invariants_first_derivative_index::dI2dC);

                const auto dI3dC = EnumUnderlyingType(
                    InvariantHolder<NinvariantsT, PolicyT>::invariants_first_derivative_index::dI3dC);

                assert(dI1dC < invariants_first_derivative.size());
                assert(dI2dC < invariants_first_derivative.size());
                assert(dI3dC < invariants_first_derivative.size());

                FirstDerivativeInvariant1CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                 invariants_first_derivative[dI1dC]);
                FirstDerivativeInvariant2CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                 invariants_first_derivative[dI2dC]);
                FirstDerivativeInvariant3CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                 invariants_first_derivative[dI3dC]);

                if (do_i4_activate)
                {
                    const auto& tau_interpolate_at_quad_pointI4 = fibersI4->GetValue(quad_pt, geom_elt);

                    const auto dI4dC = EnumUnderlyingType(
                        InvariantHolder<NinvariantsT, PolicyT>::invariants_first_derivative_index::dI4dC);

                    assert(dI4dC < invariants_first_derivative.size());

                    FirstDerivativeInvariant4CauchyGreen<DimensionT>(
                        cauchy_green_tensor, tau_interpolate_at_quad_pointI4, invariants_first_derivative[dI4dC]);
                }

                if (do_i6_activate)
                {
                    const auto& tau_interpolate_at_quad_pointI6 = fibersI6->GetValue(quad_pt, geom_elt);

                    const auto dI6dC = EnumUnderlyingType(
                        InvariantHolder<NinvariantsT, PolicyT>::invariants_first_derivative_index::dI6dC);

                    FirstDerivativeInvariant6CauchyGreen<DimensionT>(
                        cauchy_green_tensor, tau_interpolate_at_quad_pointI6, invariants_first_derivative[dI6dC]);
                }
            }
                [[fallthrough]];
            case Content::invariants:
            {
                const auto I1 = EnumUnderlyingType(InvariantHolder<NinvariantsT, PolicyT>::invariants_index::I1);
                const auto I2 = EnumUnderlyingType(InvariantHolder<NinvariantsT, PolicyT>::invariants_index::I2);
                const auto I3 = EnumUnderlyingType(InvariantHolder<NinvariantsT, PolicyT>::invariants_index::I3);

                assert(I1 < invariants.size());
                assert(I2 < invariants.size());
                assert(I3 < invariants.size());

                invariants[I1] = Invariant1<DimensionT>(cauchy_green_tensor);
                invariants[I2] = Invariant2<DimensionT>(cauchy_green_tensor);
                invariants[I3] = Invariant3<DimensionT>(cauchy_green_tensor);

                if (do_i4_activate)
                {
                    const auto& tau_interpolate_at_quad_pointI4 = fibersI4->GetValue(quad_pt, geom_elt);

                    const auto I4 = EnumUnderlyingType(InvariantHolder<NinvariantsT, PolicyT>::invariants_index::I4);
                    assert(I4 < invariants.size());

                    invariants[I4] = Invariant4<DimensionT>(cauchy_green_tensor, tau_interpolate_at_quad_pointI4);
                }

                if (do_i6_activate)
                {
                    const auto& tau_interpolate_at_quad_pointI6 = fibersI6->GetValue(quad_pt, geom_elt);

                    const auto I6 = EnumUnderlyingType(InvariantHolder<NinvariantsT, PolicyT>::invariants_index::I6);
                    assert(I6 < invariants.size());

                    invariants[I6] = Invariant6<DimensionT>(cauchy_green_tensor, tau_interpolate_at_quad_pointI6);
                }
                break;
            }
            } // switch
        }
    } // namespace


#ifndef NDEBUG
    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    inline void InvariantHolder<NinvariantsT, PolicyT>::Reset()
    {
        are_invariant_set_ = false;
    }
#endif // NDEBUG


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    double InvariantHolder<NinvariantsT, PolicyT>::GetInvariant(invariants_index a_invariants_index) const noexcept
    {
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_index) < NinvariantsT);
        return invariants_[EnumUnderlyingType(a_invariants_index)];
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    const LocalVector& InvariantHolder<NinvariantsT, PolicyT>::GetFirstDerivativeWrtCauchyGreen(
        invariants_first_derivative_index a_invariants_first_derivative_index) const noexcept
    {
        assert(GetContent() >= InvariantHolderNS::Content::invariants_and_first_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
                  "constructor argument if you really need first derivate!");
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_first_derivative_index) < NinvariantsT);

        return invariants_first_derivative_[EnumUnderlyingType(a_invariants_first_derivative_index)];
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    const LocalMatrix& InvariantHolder<NinvariantsT, PolicyT>::GetSecondDerivativeWrtCauchyGreen(
        invariants_second_derivative_index a_invariants_second_derivative_index) const noexcept
    {
        assert(GetContent() == InvariantHolderNS::Content::invariants_and_first_and_second_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
                  "constructor argument if you really need first derivate!");


        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_second_derivative_index) < 2);

        return invariants_second_derivative_[EnumUnderlyingType(a_invariants_second_derivative_index)];
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    inline std::size_t InvariantHolder<NinvariantsT, PolicyT>::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    inline InvariantHolderNS::Content InvariantHolder<NinvariantsT, PolicyT>::GetContent() const noexcept
    {
        return content_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    std::array<double, NinvariantsT>& InvariantHolder<NinvariantsT, PolicyT>::GetNonCstInvariant() noexcept
    {
        return invariants_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    std::array<LocalVector, NinvariantsT>&
    InvariantHolder<NinvariantsT, PolicyT>::GetNonCstFirstDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_first_derivative_;
    }

    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    std::array<LocalMatrix, 2>&
    InvariantHolder<NinvariantsT, PolicyT>::GetNonCstSecondDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_second_derivative_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    void
    InvariantHolder<NinvariantsT, PolicyT>::SetFibersI4(const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI4)
    {
        fibers_I4_ = fibersI4;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    void InvariantHolder<NinvariantsT, PolicyT>::SetI4(bool do_i4_activate)
    {
        do_i4_activate_ = do_i4_activate;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    bool InvariantHolder<NinvariantsT, PolicyT>::DoI4Activate() const noexcept
    {
        return do_i4_activate_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    const FiberList<PolicyT, ParameterNS::Type::vector>*
    InvariantHolder<NinvariantsT, PolicyT>::GetFibersI4() const noexcept
    {
        return fibers_I4_;
    }

    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    void
    InvariantHolder<NinvariantsT, PolicyT>::SetFibersI6(const FiberList<PolicyT, ParameterNS::Type::vector>* fibersI6)
    {
        fibers_I6_ = fibersI6;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    void InvariantHolder<NinvariantsT, PolicyT>::SetI6(bool do_i6_activate)
    {
        do_i6_activate_ = do_i6_activate;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    bool InvariantHolder<NinvariantsT, PolicyT>::DoI6Activate() const noexcept
    {
        return do_i6_activate_;
    }


    template<std::size_t NinvariantsT, FiberNS::AtNodeOrAtQuadPt PolicyT>
    const FiberList<PolicyT, ParameterNS::Type::vector>*
    InvariantHolder<NinvariantsT, PolicyT>::GetFibersI6() const noexcept
    {
        return fibers_I6_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
