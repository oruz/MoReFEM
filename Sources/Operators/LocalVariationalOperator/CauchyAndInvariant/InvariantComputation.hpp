/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 May 2016 09:25:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    /*!
     * \brief Returns first invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the first invariant.
     */
    template<std::size_t DimensionT>
    double Invariant1(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns second invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the second invariant.
     */
    template<std::size_t DimensionT>
    double Invariant2(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns third invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT>
    double Invariant3(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns fourth invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT>
    double Invariant4(const LocalVector& cauchy_green_tensor,
                      const LocalVector& tau_interpolate_at_quad_point) noexcept;

    /*!
     * \brief Returns fourth invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT>
    double Invariant6(const LocalVector& cauchy_green_tensor,
                      const LocalVector& tau_interpolate_at_quad_point) noexcept;


    /*!
     * \class doxygen_hide_invariant_first_deriv_arg
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */


    /*!
     * \class doxygen_hide_invariant_second_deriv_arg
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[out] out Matrix which stores the second derivative of the invariant. Must be already properly allocated.
     */


    /*!
     * \brief Returns first derivative of first invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT>
    void FirstDerivativeInvariant1CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT>
    void FirstDerivativeInvariant2CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT>
    void FirstDerivativeInvariant3CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of fourth invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     */
    template<std::size_t DimensionT>
    void FirstDerivativeInvariant4CauchyGreen(const LocalVector& cauchy_green_tensor,
                                              const LocalVector& tau_interpolate_at_quad_point,
                                              LocalVector& out);

    /*!
     * \brief Returns first derivative of fourth invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     */
    template<std::size_t DimensionT>
    void FirstDerivativeInvariant6CauchyGreen(const LocalVector& cauchy_green_tensor,
                                              const LocalVector& tau_interpolate_at_quad_point,
                                              LocalVector& out);


    /*!
     * \brief Returns second derivative of first invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT>
    void SecondDerivativeInvariant1CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);

    /*!
     * \brief Returns second derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT>
    void SecondDerivativeInvariant2CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);

    /*!
     * \brief Returns second derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT>
    void SecondDerivativeInvariant3CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_
