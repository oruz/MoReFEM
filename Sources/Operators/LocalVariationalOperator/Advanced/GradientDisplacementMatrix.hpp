/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 May 2016 14:12:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_

#include <vector>

#include "Utilities/MatrixOrVector.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace OperatorNS
        {


            /*!
             * \brief Compute the gradient matrix related to a given local displacement.
             *
             * \copydoc doxygen_hide_quad_pt_unknown_data_arg
             * \attention This method clearly expects to work within an operator which acts only upon a solid
             * displacement unknown. If your operator actually consider others unknown, you should rather create another
             * operator (typically a \a GlobalParameterOperator) which acts only on solid displacement and then call the
             * resulting value in your own operator (see \a UpdateCauchyGreenTensor use for an illustration). \param[in]
             * ref_felt Reference finite element. \param[in] local_displacement Displacement at the dofs of the finite
             * element under consideration. \param[out] gradient_matrix A square matrix which dimension is dimension of
             * the mesh.
             */
            void ComputeGradientDisplacementMatrix(
                const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                const Advanced::RefFEltInLocalOperator& ref_felt,
                const std::vector<double>& local_displacement,
                LocalMatrix& gradient_matrix);


        } // namespace OperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HPP_
