/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:20:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            void ExtractGradientBasedBlock(const LocalMatrix& full_matrix,
                                           ComponentNS::index_type row_component,
                                           ComponentNS::index_type col_component,
                                           LocalMatrix& matrix)
            {
                const auto Ncomponent = matrix.shape(0);
                assert(Ncomponent == matrix.shape(1));
                //                assert(static_cast<int>(row_component) < Ncomponent);
                //                assert(static_cast<int>(col_component) < Ncomponent);
                // >= and not > in the following assert because of the 1D case where this extraction is just a copy.
                // Indeed the two matrices in this case are in fact scalars.
                assert(full_matrix.shape(0) >= Ncomponent);
                assert(full_matrix.shape(1) >= Ncomponent);

                const auto first_row_index_in_full_matrix = row_component.Get() * Ncomponent;
                const auto first_col_index_in_full_matrix = col_component.Get() * Ncomponent;

                for (auto m = 0ul; m < Ncomponent; ++m)
                {
                    for (auto n = 0ul; n < Ncomponent; ++n)
                    {
                        assert(first_row_index_in_full_matrix + m < full_matrix.shape(0));
                        assert(first_col_index_in_full_matrix + n < full_matrix.shape(1));

                        matrix(m, n) =
                            full_matrix(first_row_index_in_full_matrix + m, first_col_index_in_full_matrix + n);
                    }
                }
            }


            void ExtractGradientBasedBlockColumnMatrix(const LocalVector& full_column_matrix,
                                                       ComponentNS::index_type row_component,
                                                       LocalMatrix& column_matrix)
            {
                const auto Ncomponent = column_matrix.shape(0);
                assert(row_component.Get() < Ncomponent);
                assert(static_cast<size_t>(column_matrix.shape(1)) == 1ul);
                // >= and not > in the following assert because of the 1D case where this extraction is just a copy.
                // Indeed the two matrices in this case are in fact scalars.
                assert(full_column_matrix.shape(0) >= Ncomponent);

                const auto first_row_index_in_full_column_matrix = row_component.Get() * Ncomponent;

                for (auto m = 0ul; m < Ncomponent; ++m)
                {
                    assert(first_row_index_in_full_column_matrix + m < full_column_matrix.shape(0));

                    column_matrix(m, 0) = full_column_matrix(first_row_index_in_full_column_matrix + m);
                }
            }


            void ExtractGradientBasedBlockRowMatrix(const LocalVector& full_row_matrix,
                                                    ComponentNS::index_type col_component,
                                                    LocalMatrix& row_matrix)
            {
                const auto Ncomponent = row_matrix.shape(1);
                assert(col_component.Get() < Ncomponent);
                assert(row_matrix.shape(0) == 1);
                // >= and not > in the following assert because of the 1D case where this extraction is just a copy.
                // Indeed the two matrices in this case are in fact scalars.
                assert(full_row_matrix.shape(0) >= Ncomponent);

                const auto first_col_index_in_full_row_matrix = col_component.Get() * Ncomponent;

                for (auto m = 0ul; m < Ncomponent; ++m)
                {
                    assert(first_col_index_in_full_row_matrix + m < full_row_matrix.shape(0));

                    row_matrix(0, m) = full_row_matrix(first_col_index_in_full_row_matrix + m);
                }
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
