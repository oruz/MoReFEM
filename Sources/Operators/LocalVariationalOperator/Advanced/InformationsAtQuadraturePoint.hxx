/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const QuadraturePoint& InformationsAtQuadraturePoint::GetQuadraturePoint() const noexcept
            {
                return quadrature_point_;
            }


            inline std::size_t InformationsAtQuadraturePoint::GetMeshDimension() const noexcept
            {
                return mesh_dimension_;
            }


            inline const InfosAtQuadPointNS::ForUnknownList&
            InformationsAtQuadraturePoint::GetUnknownData() const noexcept
            {
                assert(!(!for_unknown_));
                return *for_unknown_;
            }


            inline const InfosAtQuadPointNS::ForUnknownList&
            InformationsAtQuadraturePoint::GetTestUnknownData() const noexcept
            {
                if (for_test_unknown_) // case in which test unknown is not the same as unknown
                {
                    assert(!(!for_test_unknown_.value()));
                    return *(for_test_unknown_.value());
                }

                return GetUnknownData();
            }


            inline InfosAtQuadPointNS::ForUnknownList& InformationsAtQuadraturePoint::GetNonCstUnknownData() noexcept
            {
                return const_cast<InfosAtQuadPointNS::ForUnknownList&>(GetUnknownData());
            }


            inline InfosAtQuadPointNS::ForUnknownList&
            InformationsAtQuadraturePoint::GetNonCstTestUnknownData() noexcept
            {
                assert(for_test_unknown_
                       && "If not, this private method should not be called as it should mean "
                          "test unknown is the same as unknown.");
                assert(!!for_test_unknown_.value());
                return *(for_test_unknown_.value());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_
