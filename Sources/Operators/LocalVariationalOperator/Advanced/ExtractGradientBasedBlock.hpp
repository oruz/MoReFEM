/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:20:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Extract from \a full matrix a block delimited by \a row_component for the rows and \a
             * col_component for the columns.
             *
             * This is used in elastic and hyperelastic operator.
             *
             * \param[in] full_matrix Matrix from which the block is extracted.
             * \param[in] row_component Index of the component we want to extract from rows.
             * \param[in] col_component Index of the component we want to extract from columns.
             * \param[out] matrix The extracted matrix. It is expected to be already properly allocated; only the
             * content is set by current function.
             *
             */
            void ExtractGradientBasedBlock(const LocalMatrix& full_matrix,
                                           ComponentNS::index_type row_component,
                                           ComponentNS::index_type col_component,
                                           LocalMatrix& matrix);


            /*!
             * \brief Extract from \a full column matrix a block delimited by \a row_component for the rows.
             *
             * This is used in mixed solid incompressibility operator.
             *
             * \param[in] full_column_matrix Column matrix from which the block is extracted.
             * \param[in] row_component Index of the component we want to extract from rows.
             * \param[out] column_matrix The extracted column matrix. It is expected to be already properly allocated;
             * only the content is set by current function.
             *
             */
            void ExtractGradientBasedBlockColumnMatrix(const LocalVector& full_column_matrix,
                                                       ComponentNS::index_type row_component,
                                                       LocalMatrix& column_matrix);

            /*!
             * \brief Extract from \a full row matrix a block delimited by \a col_component for the rows.
             *
             * This is used in mixed solid incompressibility operator.
             *
             * \param[in] full_row_matrix Row matrix from which the block is extracted.
             * \param[in] col_component Index of the component we want to extract from cols.
             * \param[out] row_matrix The extracted row matrix. It is expected to be already properly allocated; only
             * the content is set by current function.
             *
             */
            void ExtractGradientBasedBlockRowMatrix(const LocalVector& full_row_matrix,
                                                    ComponentNS::index_type col_component,
                                                    LocalMatrix& row_matrix);


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HPP_
