/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class RefGeomElt;
    class LocalFEltSpace;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {

            /*!
             * \class doxygen_hide_infos_at_quad_pt_arg
             *
             * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature point, such as the
             * geometric and finite element shape function values.
             */


            /*!
             * \brief Stores data related to a given quadrature point, such as the geometric and finite element shape
             * function values.
             *
             * All unknowns (or test unknowns) are referenced in this object; it's up to the variational operator
             * developer to know how unknowns and tests unknowns are structured.
             *
             */
            class InformationsAtQuadraturePoint final
            {

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
                 * \param[in] quadrature_point \a QuadraturePoint for which the helper data are computed.
                 * \param[in] ref_geom_elt \a RefGeomElt considered.
                 * \param[in] ref_felt_list List of reference finite elements (The brand adapted for usage in
                 * \a LocalVariationalOperator).
                 * \param[in] test_ref_felt_list List of reference finite elements for test functions. It might be the
                 * same as \a ref_felt_list if test unknowns all match unknowns. \param[in] mesh_dimension Dimension of
                 * the mesh (so might be higher than dimension of \a ref_geom_elt.
                 */
                explicit InformationsAtQuadraturePoint(
                    const QuadraturePoint& quadrature_point,
                    const RefGeomElt& ref_geom_elt,
                    const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                    const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
                    std::size_t mesh_dimension,
                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

                //! Destructor.
                ~InformationsAtQuadraturePoint() = default;

                //! \copydoc doxygen_hide_copy_constructor
                InformationsAtQuadraturePoint(const InformationsAtQuadraturePoint& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                InformationsAtQuadraturePoint(InformationsAtQuadraturePoint&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                InformationsAtQuadraturePoint& operator=(const InformationsAtQuadraturePoint& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                InformationsAtQuadraturePoint& operator=(InformationsAtQuadraturePoint&& rhs) = delete;

                ///@}


              public:
                //! Get quadrature point informations.
                const QuadraturePoint& GetQuadraturePoint() const noexcept;

                //! Returns the dimension of the mesh.
                std::size_t GetMeshDimension() const noexcept;


                /*!
                 * \brief Compute the attributes that depends on the \a local_felt_space.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace for which data are computed.
                 */
                void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);

                //! Accessor to the data regarding unknown.
                const InfosAtQuadPointNS::ForUnknownList& GetUnknownData() const noexcept;

                /*!
                 * \brief Accessor to the data regarding test unknown.
                 *
                 * \internal Contrary to what is usually done, this one is not implemented on top of
                 * GetNonCstTestUnknownData(): it might be called and gives away the correct data whether the test
                 * unknown is the same as the unknown or not (whereas the non constant and PRIVATE accessor is to
                 * be called only when test unknown differ from unknown).
                 * \endinternal
                 *
                 * \return Reference to the data regarding test unknown.
                 */
                const InfosAtQuadPointNS::ForUnknownList& GetTestUnknownData() const noexcept;

              private:
                //! Non constant accessor to the data regarding unknown.
                InfosAtQuadPointNS::ForUnknownList& GetNonCstUnknownData() noexcept;

                /*!
                 * \brief Non constant accessor to the data regarding test unknown.
                 *
                 * \internal This accessor should be called only when for_test_unknown_ actually handles a value.
                 * If not, it means test unknown is same as unknown and the for_unknown_ object should be used.
                 * \endinternal
                 *
                 * \return Reference to the data regarding test unknown.
                 */
                InfosAtQuadPointNS::ForUnknownList& GetNonCstTestUnknownData() noexcept;

              private:
                /// \name Reference quantities
                ///@{

                //! Quadrature point considered in the class.
                const QuadraturePoint& quadrature_point_;

                //! Dimension of the mesh (might be higher than the one of finite elements here).
                const std::size_t mesh_dimension_;


                ///@}

                /*!
                 * \brief Specific data related to the unknowns (by opposition to test unknowns).
                 *
                 * If test unknown is the same, these data are also used for it.
                 */
                InfosAtQuadPointNS::ForUnknownList::unique_ptr for_unknown_ = nullptr;

                /*!
                 * \brief Specific data related to the test unknowns.
                 *
                 * Optional is not filled if test unknowns are the same as unknowns.
                 *
                 * There might be a different number of unknowns and test unknowns, and no ordering relationship
                 * is assumed: all these considerations are up to the developer of the variational operator
                 * (in the order in which he/she puts the unknowns and test unknowns in the constructor).
                 */
                std::optional<InfosAtQuadPointNS::ForUnknownList::unique_ptr> for_test_unknown_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_
