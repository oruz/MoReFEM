/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 11 Jun 2015 13:32:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_GRADIENT_BASED_BLOCK_HXX_
