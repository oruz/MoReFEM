/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 11:42:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            GreenLagrangeTensor::GreenLagrangeTensor(std::size_t mesh_dimension) : mesh_dimension_(mesh_dimension)
            {
                auto& vector = GetNonCstVector();

                switch (mesh_dimension)
                {
                case 1u:
                    vector.resize({ 1 });
                    break;
                case 2u:
                    vector.resize({ 3 });
                    break;
                case 3u:
                    vector.resize({ 6 });
                    break;
                default:
                    assert(false && "Only implemented for dimensions 2 and 3.");
                    exit(EXIT_FAILURE);
                }
            }


            namespace // anonymous
            {

                void Update1D(const LocalVector& cauchy_green_tensor_value, LocalVector& result);

                void Update2D(const LocalVector& cauchy_green_tensor_value, LocalVector& result);

                void Update3D(const LocalVector& cauchy_green_tensor_value, LocalVector& result);


            } // namespace


            const LocalVector& GreenLagrangeTensor::Update(const LocalVector& cauchy_green_tensor_value)
            {
                const auto mesh_dimension = GetMeshDimension();

                auto& ret = GetNonCstVector();

                switch (mesh_dimension)
                {
                case 1u:
                    Update1D(cauchy_green_tensor_value, ret);
                    return ret;
                case 2u:
                    Update2D(cauchy_green_tensor_value, ret);
                    return ret;
                case 3u:
                    Update3D(cauchy_green_tensor_value, ret);
                    return ret;
                default:
                    assert(false && "Only implemented for dimensions 2 and 3.");
                    exit(EXIT_FAILURE);
                }
            }


            namespace // anonymous
            {


                void Update1D(const LocalVector& cauchy_green_tensor_value, LocalVector& result)
                {
                    assert(result.shape(0) == 1);
                    assert(cauchy_green_tensor_value.size() == 1);
                    result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
                }


                void Update2D(const LocalVector& cauchy_green_tensor_value, LocalVector& result)
                {
                    assert(result.shape(0) == 3);
                    assert(cauchy_green_tensor_value.size() == 3);
                    result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
                    result(1) = 0.5 * (cauchy_green_tensor_value(1) - 1.);
                    result(2) = cauchy_green_tensor_value(2);
                }


                void Update3D(const LocalVector& cauchy_green_tensor_value, LocalVector& result)
                {
                    assert(result.shape(0) == 6);
                    assert(cauchy_green_tensor_value.size() == 6);

                    result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
                    result(1) = 0.5 * (cauchy_green_tensor_value(1) - 1.);
                    result(2) = 0.5 * (cauchy_green_tensor_value(2) - 1.);
                    result(3) = cauchy_green_tensor_value(3);
                    result(4) = cauchy_green_tensor_value(4);
                    result(5) = cauchy_green_tensor_value(5);
                }

            } // namespace


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
