/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 May 2016 14:12:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace OperatorNS
        {


            namespace // anonymous
            {


                // \todo #1491 Evaluate if Xtensor can't do that much better...
                /*!
                 * \brief Perform matrix-vector product on a given subset of the input data.
                 *
                 * \param[in] local_displacement Displacement computed in previous iteration for the current finite
                 * element. \param[in] transposed_dphi Transposed of the gradient shape function matrix. \param[in]
                 * Nnode Number of nodes in the finite element. \param[in] row Index of the row considered. \param[in]
                 * component Component of displacement considered.
                 */
                double RowProductMatVect(const std::vector<double>& local_displacement,
                                         const LocalMatrix& transposed_dphi,
                                         std::size_t Nnode,
                                         std::size_t row,
                                         std::size_t component);


            } // namespace


            void ComputeGradientDisplacementMatrix(
                const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                const Advanced::RefFEltInLocalOperator& ref_felt,
                const std::vector<double>& local_displacement,
                LocalMatrix& gradient_matrix)
            {
                const auto mesh_dimension = gradient_matrix.shape(0);
                assert(gradient_matrix.shape(1) == mesh_dimension);

                const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();

                const auto& dphi_of_unknown = ExtractSubMatrix(dphi, ref_felt);

                const auto Nnode = dphi_of_unknown.shape(0);
                assert(Nnode == ref_felt.Nnode());

                for (auto derivation_component = 0ul; derivation_component < mesh_dimension; ++derivation_component)
                {
                    for (auto component = 0ul; component < mesh_dimension; ++component)
                    {
                        gradient_matrix(derivation_component, component) = RowProductMatVect(
                            local_displacement, dphi_of_unknown, Nnode, component, derivation_component);
                    }
                }
            }


            namespace // anonymous
            {


                double RowProductMatVect(const std::vector<double>& local_displacement,
                                         const LocalMatrix& dphi,
                                         std::size_t Nnode,
                                         std::size_t col,
                                         std::size_t component)
                {
                    double sum = 0.;

                    for (auto i = 0ul; i < Nnode; ++i)
                    {
                        const auto index = i + component * Nnode;
                        assert(index < local_displacement.size());

                        sum += dphi(i, col) * local_displacement[index];
                    }
                    return sum;
                };


            } // namespace


        } // namespace OperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
