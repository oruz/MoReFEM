/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM
{
    class QuadraturePoint;
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            InformationsAtQuadraturePoint ::InformationsAtQuadraturePoint(
                const QuadraturePoint& quadrature_point,
                const RefGeomElt& ref_geom_elt,
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
                const std::size_t mesh_dimension,
                AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            : quadrature_point_(quadrature_point), mesh_dimension_(mesh_dimension)
            {
                for_unknown_ = std::make_unique<InfosAtQuadPointNS::ForUnknownList>(
                    quadrature_point, ref_geom_elt, ref_felt_list, mesh_dimension, do_allocate_gradient_felt_phi);

                if (ref_felt_list != test_ref_felt_list)
                {
                    for_test_unknown_ = std::make_optional(
                        std::make_unique<InfosAtQuadPointNS::ForUnknownList>(quadrature_point,
                                                                             ref_geom_elt,
                                                                             test_ref_felt_list,
                                                                             mesh_dimension,
                                                                             do_allocate_gradient_felt_phi));
                }
            }


            void InformationsAtQuadraturePoint::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
            {
                GetNonCstUnknownData().ComputeLocalFEltSpaceData(local_felt_space);

                if (for_test_unknown_)
                    GetNonCstTestUnknownData().ComputeLocalFEltSpaceData(local_felt_space);
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
