/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 11:42:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{

    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Class in charge of computing Green-Lagrange tensor.
             */
            class GreenLagrangeTensor final
            {
              public:
                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<GreenLagrangeTensor>;

              public:
                //! Constructor.
                //! \param[in] mesh_dimension Dimension of the mesh.
                explicit GreenLagrangeTensor(std::size_t mesh_dimension);

                //! Destructor.
                ~GreenLagrangeTensor() = default;

                //! \copydoc doxygen_hide_copy_constructor
                GreenLagrangeTensor(const GreenLagrangeTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GreenLagrangeTensor(GreenLagrangeTensor&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                GreenLagrangeTensor& operator=(const GreenLagrangeTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GreenLagrangeTensor& operator=(GreenLagrangeTensor&& rhs) = delete;

                //! Update the values.
                //! \param[in] cauchy_green_tensor_value Value of the Cauchy-Green tensor.
                //! \return The updated GreenLagrangeTensor vector.
                const LocalVector& Update(const LocalVector& cauchy_green_tensor_value);

              private:
                //! Dimension of the mesh considered.
                std::size_t GetMeshDimension() const noexcept;

                /*!
                 * \brief Access to the vector.
                 *
                 * \internal This one is at the moment private because all uses are currently covered by Update().
                 * \endinternal
                 *
                 * \return Vector.
                 */
                const LocalVector& GetVector() const noexcept;

                //! Non constant access to the vector.
                LocalVector& GetNonCstVector() noexcept;

              private:
                //! Mesh dimension.
                const std::size_t mesh_dimension_;

                //! The matrix.
                LocalVector vector_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HPP_
