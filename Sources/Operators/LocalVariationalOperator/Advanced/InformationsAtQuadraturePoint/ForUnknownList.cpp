/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <array>
#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <numeric>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"
#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS::InfosAtQuadPointNS
        {


            namespace // anonymous
            {


                void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                         const QuadraturePoint& quadrature_point,
                                         LocalVector& phi,
                                         LocalMatrix& dphi);


            } // namespace


            ForUnknownList ::ForUnknownList(
                const QuadraturePoint& quadrature_point,
                const RefGeomElt& ref_geom_elt,
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                const std::size_t mesh_dimension,
                AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            : quadrature_point_(quadrature_point), mesh_dimension_(mesh_dimension)
            {
                compute_jacobian_helper_ = std::make_unique<Advanced::GeomEltNS::ComputeJacobian>(mesh_dimension);

                InitRefGeometricShapeFunction(ref_geom_elt);
                InitRefFEltShapeFunction(ref_geom_elt, ref_felt_list);

                const auto& deriv_ref_phi_felt = GetGradientRefFEltPhi();

                if (do_allocate_gradient_felt_phi == AllocateGradientFEltPhi::yes)
                {
                    ResizeGradientFEltPhi(deriv_ref_phi_felt.shape(0), deriv_ref_phi_felt.shape(1));
                }

                Ncomponent_sequence_.resize(ref_geom_elt.GetDimension());
                std::iota(Ncomponent_sequence_.begin(), Ncomponent_sequence_.end(), 0);

                GetNonCstInverseJacobian().resize({ mesh_dimension, mesh_dimension });

                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_deriv_phi_felt)>().resize(
                    { deriv_ref_phi_felt.shape(1), deriv_ref_phi_felt.shape(0) });

                {
                    const auto ref_geom_elt_dimension = ref_geom_elt.GetDimension();

                    if (ref_geom_elt_dimension == 2 && ref_geom_elt_dimension != GetMeshDimension())
                    {
                        auto& col1 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column1)>();
                        auto& col2 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column2)>();
                        auto& cross_product = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::cross_product)>();

                        col1.resize({ 3 });
                        col2.resize({ 3 });
                        cross_product.resize({ 3 });
                    }
                }
            }


            void ForUnknownList::ResizeGradientFEltPhi(std::size_t Nrow, std::size_t Ncol)
            {
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>().resize({ Nrow, Ncol });
            }


            void ForUnknownList::InitRefGeometricShapeFunction(const RefGeomElt& ref_geom_elt)
            {
                const auto dimension = ComponentNS::index_type{ ref_geom_elt.GetDimension() };
                const auto& quad_pt = GetQuadraturePoint();

                auto& phi_ref_geo = GetNonCstRefGeometricPhi();
                auto& deriv_phi_ref_geo = GetNonCstGradientRefGeometricPhi();

                const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(
                    ref_geom_elt.Ncoords()) };

                phi_ref_geo.resize({ Nnode.Get() });
                deriv_phi_ref_geo.resize({ Nnode.Get(), dimension.Get() });

                for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode; ++local_node_index)
                {
                    phi_ref_geo(local_node_index.Get()) = ref_geom_elt.ShapeFunction(local_node_index, quad_pt);

                    for (ComponentNS::index_type component{ 0ul }; component < dimension; ++component)
                    {
                        deriv_phi_ref_geo(local_node_index.Get(), component.Get()) =
                            ref_geom_elt.FirstDerivateShapeFunction(local_node_index, component, quad_pt);
                    }
                }
            }


            void ForUnknownList ::InitRefFEltShapeFunction(
                const RefGeomElt& ref_geom_elt,
                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
            {
                const std::size_t Nnode = std::accumulate(
                    ref_felt_list.cbegin(),
                    ref_felt_list.cend(),
                    0ul,
                    [](std::size_t sum, const Advanced::RefFEltInLocalOperator::const_unique_ptr& ref_felt_ptr)
                    {
                        if (!(!ref_felt_ptr))
                            return sum + ref_felt_ptr->Nnode();
                        else
                            return sum;
                    });

                assert(Nnode > 0);

                const std::size_t dimension = ref_geom_elt.GetDimension();

                auto& phi_ref_felt = GetNonCstRefFEltPhi();
                auto& deriv_phi_ref_felt = GetNonCstGradientRefFEltPhi();

                phi_ref_felt.resize({ Nnode });
                deriv_phi_ref_felt.resize({ Nnode, dimension });

                const auto& quad_pt = GetQuadraturePoint();

                for (const auto& ref_felt_ptr : ref_felt_list)
                {
                    assert(!(!ref_felt_ptr));
                    FillPhiAndDerivates(*ref_felt_ptr, quad_pt, phi_ref_felt, deriv_phi_ref_felt);
                }
            }


            const LocalMatrix& ForUnknownList ::GetTransposedGradientFEltPhi() const
            {
                decltype(auto) ret = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_deriv_phi_felt)>();
                xt::noalias(ret) = xt::transpose(GetGradientFEltPhi());
                return ret;
            }


            void ForUnknownList::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
            {
                const auto& geom_elt = local_felt_space.GetGeometricElt();

                const auto& quad_pt = GetQuadraturePoint();

                auto& jacobian = GetNonCstComputeJacobianHelper().Compute(geom_elt, quad_pt);

                const std::size_t mesh_dimension = GetMeshDimension();

                if (mesh_dimension == geom_elt.GetDimension())
                {
                    if (DoAllocateGradientFEltPhi())
                    {
                        // Compute inverse of the jacobian.
                        auto& inverse_jacobian = GetNonCstInverseJacobian();

                        Wrappers::Xtensor::ComputeInverseSquareMatrix(
                            jacobian, inverse_jacobian, GetNonCstDeterminant());

                        xt::noalias(GetNonCstGradientFEltPhi()) =
                            xt::linalg::dot(GetGradientRefFEltPhi(), inverse_jacobian);
                    } else
                        GetNonCstDeterminant() = xt::linalg::det(jacobian);
                } else // Case in which a geometric with lower dimension than the mesh is considered.
                {
                    const std::size_t geom_elt_dimension = geom_elt.GetDimension();
                    assert(mesh_dimension > geom_elt_dimension);

                    switch (geom_elt_dimension)
                    {
                    case 2u:
                    {
                        auto& col1 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column1)>();
                        auto& col2 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column2)>();
                        auto& cross_product = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::cross_product)>();

                        col1 = xt::view(jacobian, xt::all(), 0);
                        col2 = xt::view(jacobian, xt::all(), 1);

                        Wrappers::Xtensor::CrossProduct(col1, col2, cross_product);

                        GetNonCstDeterminant() =
                            std::sqrt(NumericNS::Square(cross_product(0)) + NumericNS::Square(cross_product(1))
                                      + NumericNS::Square(cross_product(2)));

                        break;
                    }
                    case 1u:
                    {
                        GetNonCstDeterminant() =
                            std::sqrt(NumericNS::Square(jacobian(0, 0)) + NumericNS::Square(jacobian(1, 0)));
                        break;
                    }
                    case 0ul:
                    {
                        GetNonCstDeterminant() = std::fabs(jacobian(0, 0));
                        break;
                    }
                    default:
                    {
                        assert(false && "Should never happen!");
                        break;
                    }
                    } // switch
                }
            }


            namespace // anonymous
            {


                void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                         const QuadraturePoint& quadrature_point,
                                         LocalVector& phi,
                                         LocalMatrix& dphi)
                {
                    const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

                    const auto Ncomponent = ComponentNS::index_type{ basic_ref_felt.GetTopologyDimension() };
                    const auto Nnode = LocalNodeNS::index_type{ ref_felt.Nnode() };

                    // It's this line that make the difference between the two reference finite elements (if there
                    // are 2 of course...)
                    decltype(auto) node_position_list_in_matrix = ref_felt.GetLocalNodeIndexList();

                    assert(node_position_list_in_matrix.size() > 0);
                    assert(node_position_list_in_matrix.size() == Nnode.Get());

                    for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode; ++local_node_index)
                    {
                        const auto node_position_in_matrix = node_position_list_in_matrix[local_node_index.Get()];

                        phi(node_position_in_matrix) = basic_ref_felt.ShapeFunction(local_node_index, quadrature_point);

                        for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                        {
                            dphi(node_position_in_matrix, component.Get()) = basic_ref_felt.FirstDerivateShapeFunction(
                                local_node_index, component, quadrature_point);
                        }
                    }
                }


            } // namespace


        } // namespace LocalVariationalOperatorNS::InfosAtQuadPointNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
