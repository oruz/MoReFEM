/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Feb 2016 11:04:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DERIVATIVE_GREEN_LAGRANGE_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DERIVATIVE_GREEN_LAGRANGE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            //! Switch between the derivatives of eta or green lagrange.
            enum class GreenLagrangeOrEta
            {
                green_lagrange,
                eta
            };


            /*!
             * \brief Helper class for Green-Lagrange or a Eta local matrix and its transposed.
             *
             * The key of this class is its Update() method: when it is called the relevant matrix is computed
             * given the gradient displacement matrix.
             *
             * \internal <b><tt>[internal]</tt></b> Same template class is used for both Green-Lagrange and eta as
             * these matrices are extremely similar: Green-Lagrange is just Eta matrix with some terms incremented by 1.
             * \endinternal
             *
             */
            template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
            class DerivativeGreenLagrange
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = DerivativeGreenLagrange;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                //! \param[in] mesh_dimension Dimension of the mesh.
                explicit DerivativeGreenLagrange(std::size_t mesh_dimension);

                //! Destructor.
                ~DerivativeGreenLagrange() = default;

                //! \copydoc doxygen_hide_copy_constructor
                DerivativeGreenLagrange(const DerivativeGreenLagrange& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                DerivativeGreenLagrange(DerivativeGreenLagrange&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                DerivativeGreenLagrange& operator=(const DerivativeGreenLagrange& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                DerivativeGreenLagrange& operator=(DerivativeGreenLagrange&& rhs) = delete;

                ///@}

                //! Compute the matrix from a given gradient displacement matrix.
                //! \param[in] gradient_displacement_matrix Matri used to compute the new derivative GreenLagrange
                //! matrix.
                const LocalMatrix& Update(const LocalMatrix& gradient_displacement_matrix);

                /*!
                 * \brief Get transposed matrix.
                 *
                 * \attention This method uses up the current value of the derivative Green-Lagrange; make sure
                 * the latter is up-to-date!
                 *
                 * \return Transposed matrix.
                 */
                const LocalMatrix& GetTransposed();

              private:
                /*!
                 * \brief Access to the matrix.
                 *
                 * \internal <b><tt>[internal]</tt></b> This one is at the moment private because all uses are currently
                 * covered by Update().
                 * \endinternal
                 *
                 * \return Underlying matrix.
                 */
                const LocalMatrix& GetMatrix() const noexcept;

                //! Non constant access to the matrix.
                LocalMatrix& GetNonCstMatrix() noexcept;

              private:
                //! Mesh dimension.
                const std::size_t mesh_dimension_;

                //! The matrix.
                LocalMatrix matrix_;

                //! Transpose of the matrix.
                LocalMatrix transposed_matrix_;
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_DERIVATIVE_GREEN_LAGRANGE_HPP_
