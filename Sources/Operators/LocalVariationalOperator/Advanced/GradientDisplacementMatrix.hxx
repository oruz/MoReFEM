/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 May 2016 14:12:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace OperatorNS
        {


        } // namespace OperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GRADIENT_DISPLACEMENT_MATRIX_HXX_
