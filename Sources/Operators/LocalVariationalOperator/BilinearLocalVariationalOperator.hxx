/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 May 2015 09:04:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            template<class MatrixTypeT>
            BilinearLocalVariationalOperator<MatrixTypeT>::BilinearLocalVariationalOperator(
                const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                elementary_data_type&& elementary_data)
            : parent(unknown_list, test_unknown_list, std::move(elementary_data))
            {
                sub_matrix_parent::AllocateSubMatrices(this->GetElementaryData());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_BILINEAR_LOCAL_VARIATIONAL_OPERATOR_HXX_
