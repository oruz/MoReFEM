/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 14:36:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_NONLINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_NONLINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hpp"
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief The base class for all nonlinear form of local variational operators.
             *
             * \tparam MatrixTypeT Type of the elementary matrix.
             * \tparam VectorTypeT Type of the elementary vector.
             */
            template<class MatrixTypeT, class VectorTypeT>
            class NonlinearLocalVariationalOperator
            : public Internal::LocalVariationalOperatorNS::
                  LocalVariationalOperator<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
              public Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<
                  NonlinearLocalVariationalOperator<MatrixTypeT, VectorTypeT>>
            {

              public:
                //! Convenient alias.
                using self = NonlinearLocalVariationalOperator<MatrixTypeT, VectorTypeT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Alias to parent.
                using parent = Internal::LocalVariationalOperatorNS::
                    LocalVariationalOperator<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>;

                //! Convenient alias to parent.
                using sub_matrix_parent = Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<self>;

                //! Alias to matrix type.
                using matrix_type = MatrixTypeT;

                //! Alias to vector type.
                using vector_type = VectorTypeT;

                //! Alias to elementary data type.
                using elementary_data_type = typename parent::elementary_data_type;


              public:
                /// \name Special members.
                ///@{

                //! \copydoc doxygen_hide_local_var_op_constructor
                explicit NonlinearLocalVariationalOperator(
                    const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                    elementary_data_type&& elementary_data);

                //! Destructor.
                virtual ~NonlinearLocalVariationalOperator() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                NonlinearLocalVariationalOperator(const NonlinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                NonlinearLocalVariationalOperator(NonlinearLocalVariationalOperator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                NonlinearLocalVariationalOperator& operator=(const NonlinearLocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                NonlinearLocalVariationalOperator& operator=(NonlinearLocalVariationalOperator&& rhs) = delete;

                ///@}
            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_NONLINEAR_LOCAL_VARIATIONAL_OPERATOR_HPP_
