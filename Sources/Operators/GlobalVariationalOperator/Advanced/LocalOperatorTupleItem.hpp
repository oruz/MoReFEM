/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:02:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HPP_

#include <cassert>
#include <memory>
#include <vector>

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

#include "Operators/Enum.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace GlobalVariationalOperatorNS
        {


            /*!
             * \brief Class which includes the \a LocalVariationalOperator to use for a given \a RefGeomElt.
             *
             * It is intended to be used when a \a GlobalVariationalOperator uses up a different \a
             * LocalVariationalOperator implementation for different geometries; for most operators implementation is
             * the same whatever the geometry is and in this case you do not have to handle this kind of object
             * directly.
             *
             * \see GlobalVariationalOperatorNS::DependsOnRefGeomElt.
             *
             * \tparam RefGeomEltTypeT Enum which signs which \a RefGeomElt is considered.
             * \tparam LocalVariationalOperatorPtrT Type of the unique_ptr which encloses a \a LocalVariationalOperator.
             * e.g. std::unique_ptr<LocalOperatorNS::Mass>.
             */
            template<::MoReFEM::Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
            class LocalOperatorTupleItem
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = LocalOperatorTupleItem;

                //! Returns the \a RefGeomElt considered.
                static constexpr ::MoReFEM::Advanced::GeometricEltEnum RefGeomEltType();

                //! Alias to the type of the \a LocalVariationalOperator.
                using local_operator_type = typename LocalVariationalOperatorPtrT::element_type;

                //! Alias to \a LocalVariationalOperatorPtrT
                using local_operator_pointer_type = LocalVariationalOperatorPtrT;

                /*!
                 * \brief Whether the operator is bilinear, linear or non linear (in the latter case, it might be
                 * assembled into a matrix and a vector).
                 *
                 * \return Bilinear, linear or non linear.
                 */
                static constexpr ::MoReFEM::Advanced::OperatorNS::Nature OperatorNature();


              public:
                /// \name Special members.
                ///@{

                //! Constructor.
                explicit LocalOperatorTupleItem() = default;

                //! Destructor.
                ~LocalOperatorTupleItem() = default;

                //! \copydoc doxygen_hide_copy_constructor
                LocalOperatorTupleItem(const LocalOperatorTupleItem& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LocalOperatorTupleItem(LocalOperatorTupleItem&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LocalOperatorTupleItem& operator=(const LocalOperatorTupleItem& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LocalOperatorTupleItem& operator=(LocalOperatorTupleItem&& rhs) = delete;

                ///@}

                /*!
                 * \brief Mutator for the underlying std::unique_ptr.
                 *
                 * Used only when the list of \a LocalVariationalOperator is actually generated.
                 *
                 * \param[in] ptr Value set.
                 */
                void SetLocalOperator(LocalVariationalOperatorPtrT&& ptr) noexcept;

                /*!
                 * \brief Whether for the \a GlobalVariationalOperator the current \a RefGeomElt is relevant.
                 *
                 * If not (e.g. for instance if in the implementation no \a GeometricElt of the chosen type are
                 * present in the relevant finite element), the \a LocalVariationalOperator unique_ptr remains set
                 * to a nullptr value.
                 *
                 * \return True if there is an actual \a LocalVariationalOperator related to the current \a RefGeomElt.
                 */
                bool IsRelevant() const noexcept;

                /*!
                 * \brief Non constant accessor to the \a LocalVariationalOperator.
                 *
                 * \attention We assume when this method is called that IsRelevant() returns true!
                 *
                 * \return Reference to the \a LocalVariationalOperator.
                 */
                local_operator_type& GetNonCstLocalOperator() const noexcept;


              private:
                /*!
                 * \brief std::unique_ptr to the \a LocalVariationalOperator, if one needs to be defined for the
                 * \a GlobalVariationalOperator.
                 */
                LocalVariationalOperatorPtrT local_operator_ = nullptr;
            };


            /*!
             * \brief Specialization for the namesake class when the given type for \a LocalVariationalOperator
             * pointer is std::nullptr_t.
             *
             * This is intended if for a given operator a \a RefGeomElt is utterly irrelevant (e.g. Point1 or Segment2
             * for a GradOnGradientBasedElasticTensor).
             */
            template<::MoReFEM::Advanced::GeometricEltEnum RefGeomEltTypeT>
            class LocalOperatorTupleItem<RefGeomEltTypeT, std::nullptr_t>
            {

              public:
                //! Returns the \a RefGeomElt considered.
                static constexpr Advanced::GeometricEltEnum RefGeomEltType();

                //! Alias to \a LocalVariationalOperatorPtrT
                using local_operator_pointer_type = std::nullptr_t;
            };


        } // namespace GlobalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorTupleItem.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HPP_
