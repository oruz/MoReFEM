/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Apr 2017 18:45:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ENUM_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ENUM_HPP_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        //! Convenient alias to perform only part of the elementary computation.
        enum class elementary_mode
        {
            full = 0,
            just_init = 1
        };


        /*!
         * \class doxygen_hide_gvo_elementary_mode_tparam
         *
         * \tparam ModeT What is actually done in an assemble call:
         * - If 'full', normal assembling.
         * - If 'just_init', local_operator.InitLocalComputation() is called and there is no actual assembling operation
         * toward the linear algebra.
         */


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ENUM_HPP_
