/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 17:34:46 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_GLOBAL_VARIATIONAL_OPERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_GLOBAL_VARIATIONAL_OPERATOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/Containers/Tuple.hpp" // IWYU pragma: export
#include "Utilities/MatrixOrVector.hpp"   // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"  // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp" // IWYU pragma: export

#include "Geometry/Domain/Domain.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Mesh.hpp"     // IWYU pragma: export

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"              // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"              // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"               // IWYU pragma: export
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp" // IWYU pragma: export

#include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp"     // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"    // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/Enum.hpp"                                      // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/Internal/Helper.hpp"                           // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/Internal/LocalVariationalOperatorIterator.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            //! Convenient alias.
            using elementary_mode = ::MoReFEM::GlobalVariationalOperatorNS::elementary_mode;


            /// \addtogroup OperatorsGroup
            ///@{


            /*!
             * \brief Parent class of all GlobalVariationalOperator.
             *
             * This kind of operator is closely related to a (bi)linear form and its matricial or vectorial
             * representation; MoReFEM works mostly by 'iterating' (by recursion...) over a tuple of such beasts and
             * calling functions upon them.
             *
             * \attention Each variational operator specify in its constructor the local->global he needs automatically;
             * the only input the develop has is to decide when designing a new one whether he will require the
             * processor-wise local2global or not. The cost of it is that all variational operators must be defined
             * before the end of the initialization phase, after which the information to built them is imply no longer
             * available.
             *
             * \tparam LocalVariationalOperatorT Class that defines the expected elementary behaviour; it must be
             * derived from LocalVariationalOperator. \copydoc doxygen_hide_global_operator_local_operator_tuple_type
             */
            // clang-format off
            template
            <
                class DerivedT,
                Advanced::OperatorNS::Nature NatureT,
                class LocalOperatorTupleT
            >
            class GlobalVariationalOperator
            : public Internal::GlobalVariationalOperatorNS::ExtendedUnknownAndTestUnknownList
            <
                GlobalVariationalOperator<DerivedT, NatureT, LocalOperatorTupleT>
            >
            // clang-format on
            {

              private:
                //! Alias over ExtendedUnknownAndTestUnknownList parent.
                using extended_unknown_and_test_unknown_list_parent =
                    Internal::GlobalVariationalOperatorNS::ExtendedUnknownAndTestUnknownList<
                        GlobalVariationalOperator<DerivedT, NatureT, LocalOperatorTupleT>>;

              public:
                //! Alias to one of the class template argument.
                using local_operator_storage_type = LocalOperatorTupleT;

                static_assert(
                    std::tuple_size<local_operator_storage_type>::value
                        == EnumUnderlyingType(Advanced::GeometricEltEnum::Nref_geom_elt),
                    "The tuple structure is highly constrained and should encompass all RefGeomElt handled by "
                    "MoReFEM.");


//! Convenient macro to make the list of static assert checks more readable.
#define MOREFEM_GVO_CHECK_ORDER(GeometricElt)                                                                          \
    static_assert(std::tuple_element<EnumUnderlyingType(GeometricElt), LocalOperatorTupleT>::type::RefGeomEltType()    \
                      == GeometricElt,                                                                                 \
                  "The tuple must feature the local variational operator in a very specific order (the same as "       \
                  "the one in Advanced::GeometricEltEnum).");


                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Point1)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Segment2)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Segment3)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Triangle3)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Triangle6)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Quadrangle4)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Quadrangle8)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Quadrangle9)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Tetrahedron4)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Tetrahedron10)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Hexahedron8)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Hexahedron20)
                MOREFEM_GVO_CHECK_ORDER(Advanced::GeometricEltEnum::Hexahedron27)


                /*!
                 * \brief Whether the operator can be assembled in vector, in matrix or in both.
                 *
                 * \return Whether the operator can be assembled in vector, in matrix or in both.
                 *
                 * \internal <b><tt>[internal]</tt></b> It can be used inside <> brackets due to its constexpr nature
                 * (for metaprogramming purposes). \endinternal
                 */
                constexpr static Advanced::OperatorNS::Nature GetOperatorNature();

                //! Friendship to an internal helper structure.
                template<class LocalOperatorTupleT2,
                         std::size_t I,
                         std::size_t TupleSizeT,
                         Advanced::OperatorNS::Nature NatureT2>
                friend struct Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;


                /// \name Special members.
                ///@{

              private:
                /*!
                 * \brief Constructor.
                 *
                 * \attention You shouldn't use this constructor at all: the versions for 1, 2 and 3 unknowns are the
                 * ones intended to be built upon when devising a new instance of a GlobalVariationalOperator (you may
                 * ask us to write more if you really need it).
                 *
                 * \copydoc doxygen_hide_gvo_constructor_1
                 * \param[in] unknown_storage List of all the pair unknown/numbering subset considered by the operator.
                 * Their relative ordering fix the structure of the local matrix (but it should be
                 * completely transparent for the user anyway: the library takes care of fetching the appropriate
                 * elements through RefFEltInLocalOperator class). \internal It's not a reference (lvalue or rvalue) on
                 * purpose. \endinternal
                 *
                 * \param[in] test_unknown_storage List of all the pair test_unknown/numbering subset considered by the
                 * operator. Their relative ordering fix the structure of the local matrix (but it should be completely
                 * transparent for the user anyway: the library takes care of fetching the appropriate elements through
                 * RefFEltInLocalOperator class). \internal It's not a reference (lvalue or rvalue) on purpose.
                 * \endinternal
                 *
                 * \param[in] args Variadic arguments that will be perfect-forwarded to the constructor of each
                 * LocalVariationalOperator. For instance Force needs the definition of the force applied, so its
                 * prototype will be:
                 *
                 * \code
                 * Force(const InputDataT& input_data,
                 *       const FEltSpace& felt_space,
                 *       Unknown::vector_const_shared_ptr&& unknown_list,
                 *       std::size_t mesh_dimension,
                 *       AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                 *       const SpatialFunction
                 *       const GeometricElt& geom_elt)& force)
                 * \endcode
                 *
                 * \copydetails doxygen_hide_do_compute_processor_wise_local_2_global_arg
                 */
                template<typename... Args>
                explicit GlobalVariationalOperator(
                    const FEltSpace& felt_space,
                    const ExtendedUnknown::vector_const_shared_ptr unknown_storage,
                    const ExtendedUnknown::vector_const_shared_ptr test_unknown_storage,
                    const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                    DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
                    Args&&... args);

              protected:
                /*!
                 * \brief  Constructor when multiple unknowns and test unknowns are involved.
                 *
                 * \copydoc doxygen_hide_gvo_constructor_1
                 * \copydetails doxygen_hide_do_compute_processor_wise_local_2_global_arg
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 * \param[in] unknown_list Array of the unknowns considered \copydoc doxygen_hide_gvo_unknown_arg
                 * \copydoc doxygen_hide_test_unknown_list_param
                 */
                template<std::size_t NunknownsT, std::size_t NtestUnknownsT, typename... Args>
                explicit GlobalVariationalOperator(
                    const FEltSpace& felt_space,
                    const std::array<Unknown::const_shared_ptr, NunknownsT> unknown_list,
                    const std::array<Unknown::const_shared_ptr, NtestUnknownsT> test_unknown_list,
                    const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                    DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
                    Args&&... args);

                /*!
                 * \brief  Constructor when one unknown and  one test unknown are involved.
                 *
                 * \copydoc doxygen_hide_gvo_constructor_1
                 * \copydetails doxygen_hide_do_compute_processor_wise_local_2_global_arg
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 * \param[in] unknown_ptr Unknown considered \copydoc doxygen_hide_gvo_unknown_arg
                 * \copydoc doxygen_hide_test_unknown_ptr_param
                 */
                template<typename... Args>
                explicit GlobalVariationalOperator(
                    const FEltSpace& felt_space,
                    const Unknown::const_shared_ptr unknown_ptr,
                    const Unknown::const_shared_ptr test_unknown_ptr,
                    const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                    DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
                    Args&&... args);

                //! Protected destructor: no direct instance of this class should occur!
                ~GlobalVariationalOperator() = default;

                //! \copydoc doxygen_hide_copy_constructor
                GlobalVariationalOperator(const GlobalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GlobalVariationalOperator(GlobalVariationalOperator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GlobalVariationalOperator& operator=(const GlobalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GlobalVariationalOperator& operator=(GlobalVariationalOperator&& rhs) = delete;


                ///@}

              protected:
                /*!
                 * \brief This method is in charge of computing the elementary linear algebra and assembling it.
                 *
                 * This method uses the new C++ 11 feature of variadic template; so that these methods can
                 * handle generically all the operators, whatever argument they might require. The drawback is that
                 * it isn't clear which arguments are expected for a specific global operator; that's the reason the
                 * following method is NOT called Assemble() and is protected rather than public. When a developer
                 * introduces a new operator, he must therefore define a public \a Assemble() that calls the present
                 * one.
                 *
                 * For instance for TransientSource operator, which requires an additional \a time argument:
                 *
                 * \code
                 * template<class LinearAlgebraTupleT>
                 * inline void TransientSource::Assemble(LinearAlgebraTupleT&& global_vector_with_coeff_tuple,
                 *                                       double time,
                 *                                       const Domain& domain) const
                 * {
                 *      return parent::template AssembleImpl<>(std::move(global_vector_with_coeff_tuple),
                 *                                  domain,
                 *                                  time); // notice the inversion: time must come last.
                 * }
                 * \endcode
                 *
                 * \copydoc doxygen_hide_linear_algebra_tuple_arg
                 * \copydoc doxygen_hide_gvo_assemble_domain_arg
                 * \param[in] args Variadic template arguments, specific to the operator being implemented. These
                 * arguments might be global: they are to be given to DerivedT::SetComputeEltArrayArguments() which will
                 * produce the local ones. \copydoc doxygen_hide_gvo_elementary_mode_tparam
                 *
                 * Note that the variadic argument \a args can contain strong types if you wish to have different
                 * overloads for specific operators that would otherwise share the same signature. See for instance the
                 * SecondPiolaKirchhoff operator where is it used to have a safer interface and which enables different
                 * function signatures that would otherwise clash with the ones used in the QuasiIncompressible version.
                 *
                 */
                template<class LinearAlgebraTupleT, typename... Args, elementary_mode ModeT = elementary_mode::full>
                void
                AssembleImpl(LinearAlgebraTupleT&& linear_algebra_tuple, const Domain& domain, Args&&... args) const;

              public:
                //! FEltSpace.
                const FEltSpace& GetFEltSpace() const noexcept;

              protected:
                /*!
                 * \brief Access to the container that contains the match between a reference geometric element and a
                 * local variational operator.
                 */
                const local_operator_storage_type& GetLocalOperatorPerRefGeomElt() const noexcept;


                /*!
                 * \brief Whether there is a local variational operator related to a given \a ref_geom_elt.
                 *
                 * \param[in] ref_geom_elt Enum that signs a reference geometric element which pertinence is evaluated.
                 *
                 * \return True if a local variational operator was found.
                 *
                 * It might not if there is a restriction of the domain of definition (for instance elastic stiffness
                 * operator does not act upon geometric objects of dimension 1).
                 *
                 * This method should be called prior to GetNonCstLocalOperator(): the latter will assert if the
                 * \a ref_felt is invalid.
                 */
                bool DoConsider(Advanced::GeometricEltEnum ref_geom_elt) const;

                //! Return the quadrature rule that match a given reference geometric element.
                //! \param[in] ref_geom_elt \a RefGeomElt used as filter.
                const QuadratureRule& GetQuadratureRule(const RefGeomElt& ref_geom_elt) const;


                //! Returns the list of quadrature rules to use for each topology.
                const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;


              private:
                /*!
                 * \brief Overload when there are variadic arguments to handle.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is called in AssembleImpl() and has no business
                 * being called elsewhere. \endinternal
                 *
                 * \param[in] local_felt_space Local finite element space considered.
                 * \param[in,out] local_operator Local operator in charge of the elementary computation.
                 * \param[in] add_arg_as_tuple Variadic template arguments given to Assemble() method stored within
                 * a std::tuple.
                 *
                 */
                template<elementary_mode ModeT, class LocalVariationalOperatorT, class AdditionalArgTupleT>
                void PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                                  LocalVariationalOperatorT& local_operator,
                                                  const AdditionalArgTupleT& add_arg_as_tuple) const;


                /*!
                 * \brief Create a LocalVariationalOperator for each RefFEltInFEltSpace and store it into the class.
                 *
                 * \param[in] mesh_dimension Dimension of the mesh considered.
                 * \param[in] args List of variadic arguments given to the instance of the \a DerivedT
                 * constructor.
                 *
                 */
                template<typename... Args>
                void CreateLocalOperatorList(std::size_t mesh_dimension, Args&&... args);

                //! Whether the gradient of finite element is required or not.
                AllocateGradientFEltPhi DoAllocateGradientFEltPhi() const noexcept;


              private:
                //! List of nodes eligible for boundary conditions.
                NodeBearer::vector_shared_ptr node_for_boundary_condition_;

                //! Tuple which includes local variational operators data for each \a RefGeomElt.
                local_operator_storage_type local_operator_per_ref_geom_elt_;

                //! Finite element space.
                const FEltSpace& felt_space_;

                //! \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi
                const AllocateGradientFEltPhi do_allocate_gradient_felt_phi_;

                /*!
                 * \brief List of quadrature rules for each type of reference geometric element considered.
                 *
                 * If left empty (nullptr) default one from \a FEltSpace is used.
                 *
                 * Do not try to deallocate this pointer: it is assumed the caller takes care of that (usually the list
                 * of rules should be stored in a \a VariationalFormulation or a \a Model).
                 *
                 * \internal <b><tt>[internal]</tt></b> This list is given in the constructor; it is up to the caller of
                 * the operator to make sure all topologies are covered. If not an exception is thrown. \endinternal
                 */
                const QuadratureRulePerTopology* const quadrature_rule_per_topology_ = nullptr;
            };


            ///@}


            /*!
             * \brief An helper class which inherits from std::true_type when \a T is a valid member of assembling
             tuple.
             *
             * First argument of Assemble() call should be something like:
             *
             * \code
             std::make_tuple(std::ref(GlobalMatrixWithCoefficient), ..., std::ref(GlobalVectorWithCoefficient), ..._
             * \endcode
             *
             * Current struct is an helper that checks a given member \a T is either GlobalMatrixWithCoefficient& or
             * GlobalVectorWithCoefficient&; this is to be used in conjunction with is_valid_assemble_tuple.
             */
            template<class T>
            struct is_valid_assemble_tuple_arg
            : std::integral_constant<bool,
                                     std::is_same<T, GlobalMatrixWithCoefficient&>()
                                         || std::is_same<T, GlobalVectorWithCoefficient&>()>
            { };


            /*!
             * \brief Check whether the \a TupleT given as first argument of Assemble() call is valid.
             *
             * Said tuple should be a melt of GlobalMatrixWithCoefficient& and GlobalVectorWithCoefficient& arguments.
             *
             */
            template<class TupleT>
            struct is_valid_assemble_tuple;


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/GlobalVariationalOperator/Internal/GlobalVariationalOperator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_GLOBAL_VARIATIONAL_OPERATOR_HPP_
