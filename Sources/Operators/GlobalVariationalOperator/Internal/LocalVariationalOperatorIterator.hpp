/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:12:49 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <set>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Operators/GlobalVariationalOperator/Enum.hpp"                             // IWYU pragma: export
#include "Operators/LocalVariationalOperator/ElementaryData.hpp"                    // IWYU pragma: export
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            //! Convenient alias.
            using elementary_mode = ::MoReFEM::GlobalVariationalOperatorNS::elementary_mode;


            /*!
             * \brief Metaprogramming helper class used to iterate over the tuple that contains the local variational
             * operators for all \a RefGeomElt.
             *
             * \copydoc doxygen_hide_global_operator_local_operator_tuple_type
             * \tparam I Index of the current position in the tuple.
             * \tparam TupleSizeT Must be equal to std::tuple_size<TupleSizeT>::value; is used for the stop condition
             * in the struct partial specialization.
             * \tparam doxygen_hide_global_operator_nature
             */
            template<class LocalOperatorTupleT,
                     std::size_t I,
                     std::size_t TupleSizeT,
                     Advanced::OperatorNS::Nature NatureT>
            struct LocalVariationalOperatorIterator
            {

                static_assert(TupleSizeT == std::tuple_size<LocalOperatorTupleT>::value, "");

                //! Alias to the type of the \a I -th element of the tuple.
                using current_item_type = typename std::tuple_element<I, LocalOperatorTupleT>::type;

                //! Whether the type of the local variational operator of \a current_item_type is nullptr_t.
                static constexpr bool IsNullptr();


                /*!
                 * \brief Fill the \a local_operator_tuple with the appropriate \a LocalVariationalOperator for each
                 * \a RefGeomElt.
                 *
                 * \tparam GlobalOperatorT Type of the \a GlobalVariationalOperator for which the
                 * \a LocalVariationalOperator are generated.
                 *
                 * \param[in] mesh_dimension Dimension of the mesh considered.
                 * \param[in,out] global_operator \a GlobalVariationalOperator for which the operation is done (which
                 * is the one calling current static method).
                 * \copydoc doxygen_hide_global_operator_local_operator_tuple_inout
                 * \param[in] args List of variadic arguments given to the instance of the \a GlobalOperatorT
                 * constructor.
                 */
                template<class GlobalOperatorT, typename... Args>
                static void FillLocalVariationalOperatorList(std::size_t mesh_dimension,
                                                             GlobalOperatorT& global_operator,
                                                             LocalOperatorTupleT& local_operator_tuple,
                                                             Args&&... args);


                /*!
                 * \brief Call the assembling recursively for each relevant \a RefGeomElt.
                 *
                 * \tparam GlobalOperatorT Type of the \a GlobalVariationalOperator for which the
                 * \a LocalVariationalOperator are generated.
                 *
                 * \param[in,out] global_operator \a GlobalVariationalOperator for which the operation is done (which
                 * is the one calling current static method).
                 * \copydoc doxygen_hide_global_operator_local_operator_tuple_inout
                 * \param[in] additional_args_as_tuple List of variadic arguments given to the
                 * GlobalOperatorT::Assemble() operator grouped together within a tuple.
                 * \copydoc doxygen_hide_gvo_assemble_domain_arg
                 * \copydoc doxygen_hide_linear_algebra_tuple_arg
                 * \copydoc doxygen_hide_gvo_elementary_mode_tparam
                 * \param[in] do_assemble_into_matrix Whether the \a
                 * GlobalVariationalOperator Assemble() call targets at least one matrix.
                 * \param[in] do_assemble_into_vector Whether the \a GlobalVariationalOperator Assemble() call targets at least one
                 * vector.
                 */
                template<elementary_mode ModeT,
                         class GlobalOperatorT,
                         class LinearAlgebraTupleT,
                         class AdditionalArgsTupleT>
                static void Assemble(const GlobalOperatorT& global_operator,
                                     const LocalOperatorTupleT& local_operator_tuple,
                                     const LinearAlgebraTupleT& linear_algebra_tuple,
                                     const Domain& domain,
                                     assemble_into_matrix do_assemble_into_matrix,
                                     assemble_into_vector do_assemble_into_vector,
                                     const AdditionalArgsTupleT& additional_args_as_tuple);
            };


            /*!
             * \brief Issue a warning that an operator is used upon an empty \a FEltSpace - but only if it wasn't
             * already told for the current rank.
             *
             * \param[in,out] stream The stream upon which put the warning.
             * \param[in] global_operator Operator for which the warning appears.
             */
            template<class GlobalOperatorT>
            void EmptyFEltSpaceWarning(const GlobalOperatorT& global_operator, std::ostream& stream);


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Stopping condition for the recursive call.
            // ============================


            template<class TupleT, std::size_t TupleSizeT, Advanced::OperatorNS::Nature NatureT>
            struct LocalVariationalOperatorIterator<TupleT, TupleSizeT, TupleSizeT, NatureT>
            {


                template<class GlobalOperatorT, typename... Args>
                static void
                FillLocalVariationalOperatorList(std::size_t, GlobalOperatorT& global_operator, TupleT&, Args&&...);


                template<elementary_mode ModeT,
                         class GlobalOperatorT,
                         class LinearAlgebraTupleT,
                         class AdditionalArgsTupleT>
                static void Assemble(const GlobalOperatorT&,
                                     const TupleT&,
                                     const LinearAlgebraTupleT&,
                                     const Domain&,
                                     assemble_into_matrix,
                                     assemble_into_vector,
                                     const AdditionalArgsTupleT& additional_args_as_tuple);
            };


            // ============================
            // Stopping condition for the recursive call.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/GlobalVariationalOperator/Internal/LocalVariationalOperatorIterator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_ITERATOR_HPP_
