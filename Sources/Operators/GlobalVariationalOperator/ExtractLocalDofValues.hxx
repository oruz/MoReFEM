/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Mar 2016 17:27:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HXX_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HXX_

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HXX_
