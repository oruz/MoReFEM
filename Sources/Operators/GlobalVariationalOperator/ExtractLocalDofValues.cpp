/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Mar 2016 17:27:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        void ExtractLocalDofValues(const LocalFEltSpace& local_felt_space,
                                   const ExtendedUnknown& extended_unknown,
                                   const GlobalVector& vector,
                                   std::vector<double>& result)
        {
            assert(extended_unknown.GetNumberingSubset() == vector.GetNumberingSubset());

            const auto& local_2_global = local_felt_space.GetLocal2Global<MpiScale::processor_wise>(extended_unknown);

            assert(local_2_global.size() == result.size());

            Wrappers::Petsc::AccessGhostContent ghost_vector(vector, __FILE__, __LINE__);

            const auto& vector_with_ghost = ghost_vector.GetVectorWithGhost();

            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> ghost_vector_content(
                vector_with_ghost, __FILE__, __LINE__);

            auto counter = 0ul;

            for (auto global_index : local_2_global)
                result[counter++] = ghost_vector_content.GetValue(static_cast<std::size_t>(global_index));
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
