### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVariationalOperator.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
