/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Mar 2016 17:27:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HPP_

#include <vector>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class LocalFEltSpace;
    class ExtendedUnknown;
    class GlobalVector;

    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Extract from a global vector the values required at a local level.
         *
         * \param[in] local_felt_space The pendant of GeometricElt for finite element analysis, which includes all
         * finite elements related to the given geometric element.
         * \param[in] unknown Unknown to consider, that must belong to the \a local_felt_space. Processor-wise local
         * to global array must have been computed for this unknown.
         * \param[in] vector Global vector from which we seek to extract relevant local values.
         * \param[out] result Vector into which result of the function is written. This vector is assumed to be properly
         * allocated before the call to this function. This vector follows the ordering convention of local vectors
         * (i.e. dofs are grouped by component: vx1 vx2 vx3 vy1 vy2 vy3...)
         */
        void ExtractLocalDofValues(const LocalFEltSpace& local_felt_space,
                                   const ExtendedUnknown& unknown,
                                   const GlobalVector& vector,
                                   std::vector<double>& result);


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_EXTRACT_LOCAL_DOF_VALUES_HPP_
