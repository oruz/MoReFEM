/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 30 Nov 2017 15:55:46 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            template<class DerivedT>
            ExtendedUnknownAndTestUnknownList<DerivedT>::ExtendedUnknownAndTestUnknownList(
                const ExtendedUnknown::vector_const_shared_ptr operator_extended_unknown_list,
                const ExtendedUnknown::vector_const_shared_ptr operator_test_extended_unknown_list)
            : extended_unknown_list_(operator_extended_unknown_list),
              test_extended_unknown_list_(operator_test_extended_unknown_list)
            {
                {
                    const auto& list = GetExtendedUnknownList();
#ifndef NDEBUG
                    assert(!list.empty());
                    assert(std::none_of(
                        list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
#endif // NDEBUG

                    for (const auto& ptr : list)
                        numbering_subset_list_.push_back(ptr->GetNumberingSubsetPtr());

                    Utilities::EliminateDuplicate(
                        numbering_subset_list_,
                        Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                        Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

                    for (const auto& numbering_subset_ptr : numbering_subset_list_)
                    {
                        const auto& numbering_subset = *numbering_subset_ptr;

                        ExtendedUnknown::vector_const_shared_ptr unknown_per_numbering_subset;

                        std::copy_if(list.cbegin(),
                                     list.cend(),
                                     std::back_inserter(unknown_per_numbering_subset),
                                     [&numbering_subset](const auto& extended_unknown_ptr)
                                     {
                                         assert(!(!extended_unknown_ptr));
                                         return (extended_unknown_ptr->GetNumberingSubset() == numbering_subset);
                                     });

                        auto&& pair =
                            std::make_pair(numbering_subset.GetUniqueId(), std::move(unknown_per_numbering_subset));

                        const auto check = extended_unknown_list_per_numbering_subset_.insert(std::move(pair));
                        assert(check.second && "A numbering subset should be present only once!");
                        static_cast<void>(check);
                    }
                }

                {
                    const auto& list = GetExtendedTestUnknownList();

#ifndef NDEBUG
                    assert(!list.empty());
                    assert(std::none_of(
                        list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
#endif // NDEBUG

                    for (const auto& ptr : list)
                        test_numbering_subset_list_.push_back(ptr->GetNumberingSubsetPtr());

                    Utilities::EliminateDuplicate(
                        test_numbering_subset_list_,
                        Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                        Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

                    for (const auto& numbering_subset_ptr : test_numbering_subset_list_)
                    {
                        const auto& numbering_subset = *numbering_subset_ptr;

                        ExtendedUnknown::vector_const_shared_ptr test_unknown_per_numbering_subset;

                        std::copy_if(list.cbegin(),
                                     list.cend(),
                                     std::back_inserter(test_unknown_per_numbering_subset),
                                     [&numbering_subset](const auto& extended_test_unknown_ptr)
                                     {
                                         assert(!(!extended_test_unknown_ptr));
                                         return (extended_test_unknown_ptr->GetNumberingSubset() == numbering_subset);
                                     });

                        auto&& pair = std::make_pair(numbering_subset.GetUniqueId(),
                                                     std::move(test_unknown_per_numbering_subset));

                        const auto check = test_extended_unknown_list_per_numbering_subset_.insert(std::move(pair));
                        assert(check.second && "A numbering subset should be present only once!");
                        static_cast<void>(check);
                    }
                }
            }


            template<class DerivedT>
            inline const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList() const
            {
                return extended_unknown_list_;
            }


            template<class DerivedT>
            inline const NumberingSubset::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetNumberingSubsetList() const
            {
                assert(!numbering_subset_list_.empty());
                return numbering_subset_list_;
            }


            template<class DerivedT>
            const ExtendedUnknown& ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthUnknown(std::size_t index) const
            {
                const auto& list = GetExtendedUnknownList();
                assert(index < list.size());
                assert(!(!list[index]));
                return *list[index];
            }


            template<class DerivedT>
            const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList(
                const NumberingSubset& numbering_subset) const
            {
                auto it = extended_unknown_list_per_numbering_subset_.find(numbering_subset.GetUniqueId());
                assert(it != extended_unknown_list_per_numbering_subset_.cend()
                       && "It's likely you tried to assemble into a linear algebra object not defined in the expected "
                          "numbering subset.");
                return it->second;
            }


            template<class DerivedT>
            inline const std::map<std::size_t, ExtendedUnknown::vector_const_shared_ptr>&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownListPerNumberingSubset() const noexcept
            {
                return extended_unknown_list_per_numbering_subset_;
            }


            template<class DerivedT>
            inline const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList() const
            {
                return test_extended_unknown_list_;
            }


            template<class DerivedT>
            inline const NumberingSubset::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetTestNumberingSubsetList() const
            {
                assert(!test_numbering_subset_list_.empty());
                return test_numbering_subset_list_;
            }


            template<class DerivedT>
            const ExtendedUnknown&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthTestUnknown(std::size_t index) const
            {
                const auto& list = GetExtendedTestUnknownList();
                assert(index < list.size());
                assert(!(!list[index]));
                return *list[index];
            }


            template<class DerivedT>
            const ExtendedUnknown::vector_const_shared_ptr&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList(
                const NumberingSubset& numbering_subset) const
            {
                auto it = test_extended_unknown_list_per_numbering_subset_.find(numbering_subset.GetUniqueId());
                assert(it != test_extended_unknown_list_per_numbering_subset_.cend()
                       && "It's likely you tried to assemble into a linear algebra object not defined in the expected "
                          "numbering subset.");
                return it->second;
            }


            template<class DerivedT>
            inline const std::map<std::size_t, ExtendedUnknown::vector_const_shared_ptr>&
            ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownListPerNumberingSubset() const noexcept
            {
                return test_extended_unknown_list_per_numbering_subset_;
            }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HXX_
