/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 15:29:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_

// IWYU pragma: private, include "ParameterInstances/Fiber/FiberList.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& FiberList<PolicyT, TypeT>::ClassName()
    {
        static std::string ret = std::string("FiberList<") + ::MoReFEM::ParameterNS::Name<TypeT>() + ">";
        return ret;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    FiberList<PolicyT, TypeT>::FiberList(const std::size_t unique_id,
                                         const std::string& fiber_file,
                                         const Domain& domain,
                                         const FEltSpace& felt_space,
                                         const TimeManager& time_manager,
                                         const Unknown& unknown)
    : unique_id_parent(unique_id), parent("Fiber manager", domain), felt_space_(felt_space),
      time_manager_(time_manager), unknown_(unknown), fiber_file_(fiber_file)
    {
        assert(felt_space.GetDomain().GetUniqueId() == domain.GetUniqueId()
               && "Domain to create the parameter and domain used to define the finite element space should be "
                  "the same.");

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        const auto& numbering_subset = felt_space.GetNumberingSubset(unknown);

        global_vector_ = std::make_unique<GlobalVector>(numbering_subset);
        AllocateGlobalVector(god_of_dof, *global_vector_);

        assert(god_of_dof.GetMesh().GetUniqueId() == domain.GetMesh().GetUniqueId());

        typename Internal::FiberNS::Traits<PolicyT, TypeT>::value_list_per_coord_index_type value_list_per_coord_index;

        decltype(auto) mesh = god_of_dof.GetMesh();

        if constexpr (PolicyT == FiberNS::AtNodeOrAtQuadPt::at_node)
        {
            Internal::FiberNS::ReadFiberFile<PolicyT, TypeT>(
                god_of_dof.GetMpi(), fiber_file, mesh, domain, value_list_per_coord_index);

            std::cout << god_of_dof.GetMpi().GetRankPreffix() << ' ' << value_list_per_coord_index.size()
                      << " values read in the fiber file." << std::endl;

            auto& global_vector = GetNonCstGlobalVector();

            // Values of fiber are actually given at nodes; so build first a \a ParameterAtDof.
            Internal::FiberNS::FillGlobalVector<PolicyT, TypeT>(value_list_per_coord_index,
                                                                god_of_dof.GetProcessorWiseNodeBearerList(),
                                                                unknown,
                                                                numbering_subset,
                                                                global_vector);
        }

        if constexpr (PolicyT == FiberNS::AtNodeOrAtQuadPt::at_quad_pt)
        {
            const auto& mpi = god_of_dof.GetMpi();

            const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                decltype(auto) iterator_range =
                    mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

                Nprocessor_wise_geom_elt =
                    static_cast<std::size_t>(std::distance(iterator_range.first, iterator_range.second));

                Nprogram_wise_geom_elt = static_cast<std::size_t>(
                    mpi.AllReduce(Nprocessor_wise_geom_elt, MoReFEM::Wrappers::MpiNS::Op::Sum));

                assert(Nprogram_wise_geom_elt > 0 && "Domain containing the parameter is empty!");

                const auto& first_geom_elt = *iterator_range.first;
                const auto index = first_geom_elt->GetIndex();

                first_index_program_wise_ = mpi.AllReduce(index, MoReFEM::Wrappers::MpiNS::Op::Min);
            }
        }
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    void FiberList<PolicyT, TypeT>::Initialize(const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    {
        assert(underlying_parameter_ == nullptr);
        const auto& domain = this->GetDomain();
        const auto& felt_space = GetFeltSpace();
        const auto& time_manager = GetTimeManager();
        const auto& unknown = GetUnknown();

        if constexpr (PolicyT == FiberNS::AtNodeOrAtQuadPt::at_node)
        {
            // Construct the parameter holding temporarly fibers at the dof level.
            ParameterAtDof<TypeT, ParameterNS::TimeDependencyNS::None> param_at_dof(
                "Temporary", domain, felt_space, unknown, GetNonCstGlobalVector());

            // Then convert them into the actual attribute.
            using converter_type = ParameterNS::FromParameterAtDof<TypeT, ParameterNS::TimeDependencyNS::None, 1u>;

            underlying_parameter_ = converter_type::Perform("Fiber manager",
                                                            domain,
                                                            quadrature_rule_per_topology == nullptr
                                                                ? felt_space.GetQuadratureRulePerTopologyRawPtr()
                                                                : quadrature_rule_per_topology,
                                                            time_manager,
                                                            param_at_dof);
        }

        if constexpr (PolicyT == FiberNS::AtNodeOrAtQuadPt::at_quad_pt)
        {
            const auto& mesh = domain.GetMesh();

            typename Internal::FiberNS::Traits<PolicyT, TypeT>::value_list_per_quad_pt_index_type
                value_list_per_quad_pt_index;

            const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                assert(!(!quadrature_rule_per_topology));
                const auto& quadrature_rule =
                    quadrature_rule_per_topology->GetRule(ref_geom_elt.GetTopologyIdentifier());

                const auto Nquadrature_points = quadrature_rule.NquadraturePoint();

                assert(Nquadrature_points > 0 && "Empty quadrature point list!");

                // Fill the unordered map 'value_list_per_coord_index' with the values read in the fiber file for each
                // processor.
                Internal::FiberNS::ReadFiberFileAtQuadPt<TypeT>(fiber_file_,
                                                                mesh,
                                                                domain,
                                                                Nquadrature_points,
                                                                first_index_program_wise_,
                                                                Nprogram_wise_geom_elt,
                                                                Nprocessor_wise_geom_elt,
                                                                value_list_per_quad_pt_index);
            }

            // Construct the parameter with default initialisation.
            if constexpr (TypeT == ParameterNS::Type::vector)
            {
                LocalVector init_vector;
                constexpr std::size_t spatial_point_size = 3;
                init_vector.resize({ spatial_point_size });
                init_vector.fill(0.);

                assert(!(!quadrature_rule_per_topology));
                underlying_parameter_ = std::make_unique<ParameterAtQuadraturePoint<TypeT>>(
                    "Fiber vector at quad points", domain, *quadrature_rule_per_topology, init_vector, time_manager);
            }

            if constexpr (TypeT == ParameterNS::Type::scalar)
            {
                const double init_value = 0.;

                assert(!(!quadrature_rule_per_topology));
                underlying_parameter_ = std::make_unique<ParameterAtQuadraturePoint<TypeT>>(
                    "Fiber scalar at quad points", domain, *quadrature_rule_per_topology, init_value, time_manager);
            }

            // Update the parameter with the coords read in the fiber file.
            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                const auto& quadrature_rule =
                    quadrature_rule_per_topology->GetRule(ref_geom_elt.GetTopologyIdentifier());

                const auto Nquadrature_points = quadrature_rule.NquadraturePoint();

                const auto iterator_range =
                    mesh.template GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

                // Iterate over all geometric elements that share the ref_geom_elt.
                for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
                {
                    const auto& geom_elt_ptr = *it_geom_elt;
                    assert(!(!geom_elt_ptr));
                    const auto geom_elt_index = geom_elt_ptr->GetIndex();
                    const auto& geom_elt = *geom_elt_ptr;

                    if (!domain.IsGeometricEltInside(geom_elt))
                        continue;

                    const auto target_index = (geom_elt_index - first_index_program_wise_) * Nquadrature_points;

                    for (auto i = 0ul; i < Nquadrature_points; ++i)
                    {
                        const auto& quad_pt = quadrature_rule.Point(i);

                        if constexpr (TypeT == ParameterNS::Type::vector)
                        {
                            auto functor = [&target_index, &i, &value_list_per_quad_pt_index](LocalVector& fiber_vector)
                            {
                                assert(fiber_vector.size() == 3);

                                fiber_vector(0) = value_list_per_quad_pt_index[target_index + i][0];
                                fiber_vector(1) = value_list_per_quad_pt_index[target_index + i][1];
                                fiber_vector(2) = value_list_per_quad_pt_index[target_index + i][2];
                            };
                            underlying_parameter_->UpdateValue(quad_pt, geom_elt, functor);
                        }

                        if constexpr (TypeT == ParameterNS::Type::scalar)
                        {
                            auto functor = [&target_index, &i, &value_list_per_quad_pt_index](double& fiber_scalar)
                            {
                                fiber_scalar = value_list_per_quad_pt_index[target_index + i][0];
                            };
                            underlying_parameter_->UpdateValue(quad_pt, geom_elt, functor);
                        }
                    }
                }
            }
        }
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline bool FiberList<PolicyT, TypeT>::IsConstant() const noexcept
    {
        return GetUnderlyingParameter().IsConstant();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline typename FiberList<PolicyT, TypeT>::return_type FiberList<PolicyT, TypeT>::SupplGetConstantValue() const
    {
        return GetUnderlyingParameter().SupplGetConstantValue();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline typename FiberList<PolicyT, TypeT>::return_type FiberList<PolicyT, TypeT>::SupplGetAnyValue() const
    {
        return GetUnderlyingParameter().SupplGetAnyValue();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline typename FiberList<PolicyT, TypeT>::return_type
    FiberList<PolicyT, TypeT>::SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const
    {
        return GetUnderlyingParameter().SupplGetValue(local_coords, geom_elt);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline void FiberList<PolicyT, TypeT>::SupplWrite(std::ostream& out) const
    {
        return GetUnderlyingParameter().SupplWrite(out);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline GlobalVector& FiberList<PolicyT, TypeT>::GetNonCstGlobalVector() noexcept
    {
        return const_cast<GlobalVector&>(GetGlobalVector());
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline const GlobalVector& FiberList<PolicyT, TypeT>::GetGlobalVector() const noexcept
    {
        assert(!(!global_vector_));
        return *global_vector_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline const typename FiberList<PolicyT, TypeT>::parameter_type&
    FiberList<PolicyT, TypeT>::GetUnderlyingParameter() const noexcept
    {
        assert(!(!underlying_parameter_) && "You should call Initialize() in your model.");
        return *underlying_parameter_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    void FiberList<PolicyT, TypeT>::SupplTimeUpdate()
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    void FiberList<PolicyT, TypeT>::SupplTimeUpdate(double time)
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
        static_cast<void>(time);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline void FiberList<PolicyT, TypeT>::SetConstantValue(double value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    inline const FEltSpace& FiberList<PolicyT, TypeT>::GetFeltSpace() const noexcept
    {
        return felt_space_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    const TimeManager& FiberList<PolicyT, TypeT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }

    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt PolicyT,
        ParameterNS::Type TypeT
    >
    // clang-format on
    const Unknown& FiberList<PolicyT, TypeT>::GetUnknown() const noexcept
    {
        return unknown_;
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_
