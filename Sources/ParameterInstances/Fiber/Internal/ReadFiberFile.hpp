/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:00:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HPP_

#include <string>
#include <unordered_map>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "Core/Parameter/FiberEnum.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Mesh;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FiberNS
        {


            /*!
             * \brief Read and interpret the Ensight fiber file.
             *
             * This function is an helper function of FiberList constructor.
             *
             * \copydetails doxygen_hide_mpi_param
             * \param[in] fiber_file File at the Ensight format which will be interpreted. Its format is specified by
             * Ensight specifications;  typically scalar files get a 'scl' extension and their first line is 'Scalar per
             * node' whereas vectorial get a 'vct' extension and first line is 'Vector per node'. \param[in] mesh Mesh
             * considered for the parameters; in peculiar vertices to consider will be taken from there. \param[in]
             * domain \a Domain considered for the parameters; in peculiar vertices to consider will be taken from
             * there. \param[out] out Values per each Coord managed by the current processor. Index used is the one read
             * from the file.
             *
             */
            template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT>
            void ReadFiberFile(const ::MoReFEM::Wrappers::Mpi& mpi,
                               const std::string& fiber_file,
                               const Mesh& mesh,
                               const Domain& domain,
                               typename Traits<PolicyT, TypeT>::value_list_per_coord_index_type& out);


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/Fiber/Internal/ReadFiberFile.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HPP_
