/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:06:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/

#include <string>

#include "Utilities/Exceptions/Exception.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            template<>
            void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
                const std::string& fiber_file,
                const std::string& line)
            {
                if (line != "Scalar per node")
                    throw Exception("Error in fiber file '" + fiber_file
                                        + "': expected format was Ensight one for a scalar "
                                          "quantity (the one with scl extension)",
                                    __FILE__,
                                    __LINE__);
            }


            template<>
            void
            Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
                const std::string& fiber_file,
                const std::string& line)
            {
                if (line != "Scalar per quad_pt")
                    throw Exception("Error in fiber file '" + fiber_file
                                        + "': expected format was an Ensight one for a scalar "
                                          "quantity defined at the quadrature points",
                                    __FILE__,
                                    __LINE__);
            }


            template<>
            void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::CheckFirstLineOfFile(
                const std::string& fiber_file,
                const std::string& line)
            {
                if (line != "Vector per node")
                    throw Exception("Error in fiber file '" + fiber_file
                                        + "': expected format was Ensight one for a vectorial "
                                          "quantity (the one with vct extension)",
                                    __FILE__,
                                    __LINE__);
            }


            template<>
            void
            Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::vector>::CheckFirstLineOfFile(
                const std::string& fiber_file,
                const std::string& line)
            {
                if (line != "Vector per quad_pt")
                    throw Exception("Error in fiber file '" + fiber_file
                                        + "': expected format was an Ensight one for a vectorial "
                                          "quantity defined at the quadrature points",
                                    __FILE__,
                                    __LINE__);
            }


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
