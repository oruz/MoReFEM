/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:00:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HXX_

// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/FillGlobalVector.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::FiberNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT>
    void
    FillGlobalVector(const typename Traits<PolicyT, TypeT>::value_list_per_coord_index_type& value_list_per_coord_index,
                     const NodeBearer::vector_shared_ptr& node_bearer_list,
                     const Unknown& unknown,
                     const NumberingSubset& numbering_subset,
                     GlobalVector& vector)
    {
#ifndef NDEBUG
        if (TypeT == ParameterNS::Type::scalar)
            assert(unknown.GetNature() == UnknownNS::Nature::scalar);
        if (TypeT == ParameterNS::Type::vector)
            assert(unknown.GetNature() == UnknownNS::Nature::vectorial);
#endif // NDEBUG

        {
            ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_and_write> vector_content(
                vector, __FILE__, __LINE__);

#ifndef NDEBUG
            const auto vector_proc_size = static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__));
#endif // NDEBUG

            std::size_t number_of_dofs_filled = 0;

            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                if (node_bearer.GetNature() != ::MoReFEM::InterfaceNS::Nature::vertex)
                    continue;

                const auto& vertex_coords_list = node_bearer.GetInterface().GetVertexCoordsList();
                assert(vertex_coords_list.size() == 1ul && "Interface is a vertex...");
                const auto& vertex_coords_ptr = vertex_coords_list.back();
                assert(!(!vertex_coords_ptr));

                const auto geometric_vertex_index = vertex_coords_ptr->GetIndexFromMeshFile();

                auto it = value_list_per_coord_index.find(geometric_vertex_index);

                if (it != value_list_per_coord_index.cend())
                {
                    const auto& value_list_for_current_coord = it->second;

                    const auto& node_list = node_bearer.GetNodeList();

                    // Find the only node related to \a Unknown. There is no more direct accessor in
                    // \a NodeBearer s it would be error-prone: one should not be encouraged to select
                    // solely on \a Unknown regardless of shape function.
                    const auto& find_node_with_unknown = [&unknown](const auto& node_ptr)
                    {
                        assert(!(!node_ptr));
                        return node_ptr->GetUnknown() == unknown;
                    };

                    assert(std::count_if(node_list.cbegin(), node_list.cend(), find_node_with_unknown) == 1ul);

                    const auto it_node = std::find_if(node_list.cbegin(), node_list.cend(), find_node_with_unknown);

                    decltype(auto) node_ptr = *it_node;
                    assert(!(!node_ptr));

                    const auto& node = *node_ptr;

                    assert(node.IsInNumberingSubset(numbering_subset));

                    const auto& dof_list = node.GetDofList();

                    const std::size_t Ndof_in_node = dof_list.size();
                    assert(Ndof_in_node <= value_list_for_current_coord.size());

                    for (std::size_t i = 0ul; i < Ndof_in_node; ++i)
                    {
                        const auto& dof_ptr = dof_list[i];
                        assert(!(!dof_ptr));

                        vector_content[dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset)] =
                            value_list_for_current_coord[i];

                        ++number_of_dofs_filled;
                    }
                }
            }
            assert(vector_proc_size == number_of_dofs_filled && "All the dofs have not been filled.");
        }

        vector.UpdateGhosts(__FILE__, __LINE__);
    }


} // namespace MoReFEM::Internal::FiberNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HXX_
