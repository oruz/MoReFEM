/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:06:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_

// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/Traits.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {

            template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT>
            inline constexpr std::size_t Traits<PolicyT, ParameterNS::Type::scalar>::NvertexPerLine()
            {
                return 6u;
            }

            template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT>
            inline constexpr std::size_t Traits<PolicyT, ParameterNS::Type::vector>::NvertexPerLine()
            {
                return 2u;
            }

            template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT>
            inline constexpr std::size_t Traits<PolicyT, ParameterNS::Type::scalar>::NvaluePerVertex()
            {
                return 1u;
            }

            template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT>
            inline constexpr std::size_t Traits<PolicyT, ParameterNS::Type::vector>::NvaluePerVertex()
            {
                return 3u;
            }


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
