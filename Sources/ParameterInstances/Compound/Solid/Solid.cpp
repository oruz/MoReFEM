/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>
#include <type_traits>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        /*!
         * \brief Compare a parameter as given directly in the input file to the same recomputed from other parameters.
         *
         * For instance compare Young modulus to the one recomputed from Lame parameters.
         *
         * There are actually two checks done: first is to compare directly the numerical values and if they
         * do not match two outcomes are possible:
         * - If the relative difference between both is small, a simple warning is issued.
         * - If not, an exception that should terminate the program is thrown.
         *
         * If they match nothing is done.
         *
         */
        class CompareSameParameterFromDifferentComputation
        {
          public:
            using unique_ptr = std::unique_ptr<CompareSameParameterFromDifferentComputation>;

            /*!
             * \brief Constructor.
             *
             * \param[in] tolerance Relative tolerance below which s a slight discrepancy may be given a pass.
             */
            CompareSameParameterFromDifferentComputation(std::string&& original_name,
                                                         std::string&& recomputed_from,
                                                         double tolerance);

            //! The method that does the acual check onto the two provided values.
            void Perform(double original, double recomputed);

          private:
            //! Name of the parameter begin compared.
            std::string original_name_;

            //! Names of the parameters from which the recomputation was done.
            std::string recomputed_from_;

            //! Relative tolerance under which discrepancies may pass.
            const double tolerance_;

            //! Whether there was already a warning printed for very small discrepancy.
            bool was_warning_already_printed_ = false;
        };


    } // namespace


    void Solid::CheckConsistency(const double relative_tolerance) const
    {

        // ======================================
        // First check the obvious: material parameters go by pair...
        // ======================================

        const bool are_lame = IsLameLambda();

        if (are_lame != IsLameMu())
            throw Exception("Inconsistency in Solid: Lame coefficients should be either both defined or "
                            "both ignored.",
                            __FILE__,
                            __LINE__);

        const bool are_young_poisson = IsYoungModulus();

        if (are_young_poisson != IsPoissonRatio())
            throw Exception("Inconsistency in Solid: Young modulus and Poisson ratio should be either both "
                            "defined or both ignored.",
                            __FILE__,
                            __LINE__);

        const bool are_kappa_coeff = IsKappa1();

        if (are_kappa_coeff != IsKappa2())
            throw Exception("Inconsistency in Solid: kappa parameters should be either both "
                            "defined or both ignored.",
                            __FILE__,
                            __LINE__);

        if (are_kappa_coeff && !IsHyperelasticBulk())
            throw Exception("Inconsistency in Solid: if kappa coefficients are defined hyperelastic bulk "
                            "should be as well.",
                            __FILE__,
                            __LINE__);


        // ======================================
        // Now check numeric values at quadrature points are also consistent.
        // ======================================
        decltype(auto) domain = GetDomain();
        const auto& mesh = domain.GetMesh();

        decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

        const double one_third = 1. / 3.;

        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_lambda_and_young_poisson = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_mu_and_young_poisson = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_lambda_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_mu_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_young_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_poisson_and_kappa = nullptr;

        if (are_lame && are_young_poisson)
        {
            compare_lame_lambda_and_young_poisson = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame lambda", "Young modulus and Poisson ratio", relative_tolerance);
            compare_lame_mu_and_young_poisson = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame mu", "Young modulus and Poisson ratio", relative_tolerance);
        }

        if (are_lame && are_kappa_coeff)
        {
            compare_lame_lambda_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame lambda", "Kappa parameters and hyperelastic bulk", relative_tolerance);

            compare_lame_mu_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame mu", "Kappa parameters", relative_tolerance);
        }

        if (are_young_poisson && are_kappa_coeff)
        {
            compare_young_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Young modulus", "Kappa parameters and hyperelastic bulk", relative_tolerance);

            compare_poisson_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Poisson ratio", "Kappa parameters and hyperelastic bulk", relative_tolerance);
        }

        const auto& quadrature_rule_per_topology = GetQuadratureRulePerTopology();

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;
            const auto topology_type = ref_geom_elt.GetTopologyIdentifier();

            decltype(auto) quadrature_rule = quadrature_rule_per_topology.GetRule(topology_type);
            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            decltype(auto) geom_elt_range =
                mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(*ref_geom_elt_ptr);

            for (auto it = geom_elt_range.first; it != geom_elt_range.second; ++it)
            {
                const auto& geom_elt_ptr = *it;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                if (!domain.IsGeometricEltInside(geom_elt))
                    continue;

                for (const auto& quad_pt_ptr : quad_pt_list)
                {
                    assert(!(!quad_pt_ptr));
                    const auto& quad_pt = *quad_pt_ptr;

                    if (are_lame && are_young_poisson)
                    {
                        const double young = GetYoungModulus().GetValue(quad_pt, geom_elt);
                        const double poisson = GetPoissonRatio().GetValue(quad_pt, geom_elt);
                        const double lame_lambda = GetLameLambda().GetValue(quad_pt, geom_elt);
                        const double lame_mu = GetLameMu().GetValue(quad_pt, geom_elt);

                        const double lame_lbd_from_yp = young * poisson / ((1. + poisson) * (1. - 2. * poisson));
                        const double lame_mu_from_yp = young * 0.5 / (1. + poisson);

                        assert(!(!compare_lame_lambda_and_young_poisson));
                        compare_lame_lambda_and_young_poisson->Perform(lame_lambda, lame_lbd_from_yp);

                        assert(!(!compare_lame_mu_and_young_poisson));
                        compare_lame_mu_and_young_poisson->Perform(lame_mu, lame_mu_from_yp);
                    }

                    if (are_lame && are_kappa_coeff)
                    {
                        const double lame_lambda = GetLameLambda().GetValue(quad_pt, geom_elt);
                        const double lame_mu = GetLameMu().GetValue(quad_pt, geom_elt);

                        const double kappa_1 = GetKappa1().GetValue(quad_pt, geom_elt);
                        const double kappa_2 = GetKappa2().GetValue(quad_pt, geom_elt);

                        const double bulk = GetHyperelasticBulk().GetValue(quad_pt, geom_elt);

                        const double lame_mu_from_kappa = 2. * (kappa_1 + kappa_2);
                        const double lame_lbd_from_kappa_and_bulk = -4. * one_third * (kappa_1 + kappa_2) + bulk;

                        assert(!(!compare_lame_lambda_and_kappa));
                        compare_lame_lambda_and_kappa->Perform(lame_lambda, lame_lbd_from_kappa_and_bulk);

                        assert(!(!compare_lame_mu_and_kappa));
                        compare_lame_mu_and_kappa->Perform(lame_mu, lame_mu_from_kappa);
                    }


                    if (are_young_poisson && are_kappa_coeff && !are_lame) // the latter because if all are defined
                                                                           // tests above are already sufficient.
                    {
                        const double kappa_sum =
                            GetKappa1().GetValue(quad_pt, geom_elt) + GetKappa2().GetValue(quad_pt, geom_elt);
                        const double bulk = GetHyperelasticBulk().GetValue(quad_pt, geom_elt);

                        const double young = GetYoungModulus().GetValue(quad_pt, geom_elt);
                        const double poisson = GetPoissonRatio().GetValue(quad_pt, geom_elt);

                        const double young_from_kappa_and_bulk =
                            6. * bulk * kappa_sum / (bulk + 2. * one_third * kappa_sum);

                        const double poisson_from_kappa_and_bulk =
                            .5 * (-4. * one_third * kappa_sum + bulk) / (bulk + 2. * one_third * kappa_sum);

                        assert(!(!compare_young_and_kappa));
                        compare_young_and_kappa->Perform(young, young_from_kappa_and_bulk);

                        assert(!(!compare_poisson_and_kappa));
                        compare_poisson_and_kappa->Perform(poisson, poisson_from_kappa_and_bulk);
                    }
                }
            }
        }
    }


    void Solid::Print(std::ostream& out) const
    {
        out << "Values of parameters (assumed constant to be able to call the current method) are:" << std::endl;

        out << "\t- Volumic mass = " << GetVolumicMass().GetConstantValue() << std::endl;

        if (IsYoungModulus())
        {
            out << "\t- Young modulus = " << GetYoungModulus().GetConstantValue() << std::endl;
            out << "\t- Poisson ratio = " << GetPoissonRatio().GetConstantValue() << std::endl;
        }

        if (IsLameLambda())
        {
            out << "\t- Lame lambda = " << GetLameLambda().GetConstantValue() << std::endl;
            out << "\t- Lame mu = " << GetLameMu().GetConstantValue() << std::endl;
        }

        if (IsKappa1())
        {
            out << "\t- Kappa1 = " << GetKappa1().GetConstantValue() << std::endl;
            out << "\t- Kappa2 = " << GetKappa2().GetConstantValue() << std::endl;
        }

        if (IsHyperelasticBulk())
            out << "\t- Hyperelastic bulk = " << GetHyperelasticBulk().GetConstantValue() << std::endl;

        if (IsMu1())
        {
            out << "\t- Mu1 = " << GetMu1().GetConstantValue() << std::endl;
            out << "\t- Mu2 = " << GetMu2().GetConstantValue() << std::endl;
            out << "\t- C0 = " << GetC0().GetConstantValue() << std::endl;
            out << "\t- C1 = " << GetC1().GetConstantValue() << std::endl;
            out << "\t- C2 = " << GetC2().GetConstantValue() << std::endl;
            out << "\t- C3 = " << GetC3().GetConstantValue() << std::endl;
            out << "\t- C4 = " << GetC4().GetConstantValue() << std::endl;
            out << "\t- C5 = " << GetC5().GetConstantValue() << std::endl;
        }
    }


    namespace // anonymous
    {


        CompareSameParameterFromDifferentComputation ::CompareSameParameterFromDifferentComputation(
            std::string&& original_name,
            std::string&& recomputed_from,
            double tolerance)
        : original_name_(std::move(original_name)), recomputed_from_(std::move(recomputed_from)), tolerance_(tolerance)
        { }


        void CompareSameParameterFromDifferentComputation ::Perform(double original, double recomputed)
        {
            if (NumericNS::AreEqual(original, recomputed))
                return;

            std::ostringstream oconv;
            oconv << "Inconsistency in Solid: parameters aren't self consistant:\n\n";

            // If unequal, it might be due to numerical precision. So if relative error is small enough, just print
            // a warning on screen.
            if (!NumericNS::IsZero(original))
            {
                const double relative_difference = std::fabs(original - recomputed) / original;

                if (relative_difference > tolerance_)
                {
                    oconv << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                          << recomputed_from_ << " we obtain " << recomputed << " (difference is "
                          << recomputed - original << "; relative difference is " << relative_difference * 100.
                          << "%).\n";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                if (!was_warning_already_printed_)
                {
                    std::cout << "===========================================\n";
                    std::cout << "[WARNING] A slight inconsistency was found in Solid parameters:\n\n";

                    std::cout << "\t- " << original_name_ << " read in file is " << original
                              << " but if recomputed from " << recomputed_from_ << " we obtain " << recomputed
                              << " (difference is " << recomputed - original << "; relative difference is "
                              << relative_difference * 100. << "%).\n";
                    std::cout << "\t  Difference is deemed small enough that the computation may proceed." << std::endl;
                    std::cout
                        << "\t  This warning won't be issued again if another small discrepancy for the same set "
                           "of parameters is found; a huge one would of course trigger the abortion of the program.\n";
                    std::cout << "===========================================" << std::endl;

                    was_warning_already_printed_ = true;
                }
            } else
            {
                const double difference = std::fabs(original - recomputed);

                if (difference > tolerance_)
                {
                    oconv << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                          << recomputed_from_ << " we obtain " << recomputed << " (difference is "
                          << recomputed - original << ").\n";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                if (!was_warning_already_printed_)
                {
                    std::cout << "===========================================\n";
                    std::cout << "[WARNING] A slight inconsistency was found in Solid parameters:\n\n";

                    std::cout << "\t- " << original_name_ << " read in file is " << original
                              << " but if recomputed from " << recomputed_from_ << " we obtain " << recomputed
                              << " (difference is " << recomputed - original << ").\n";
                    std::cout << "\t  Difference is deemed small enough that the computation may proceed." << std::endl;
                    std::cout
                        << "\t  This warning won't be issued again if another small discrepancy for the same set "
                           "of parameters is found; a huge one would of course trigger the abortion of the program.\n";
                    std::cout << "===========================================" << std::endl;

                    was_warning_already_printed_ = true;
                }
            }
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
