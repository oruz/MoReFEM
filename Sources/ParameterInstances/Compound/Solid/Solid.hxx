/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/Solid/Solid.hpp"


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace SolidNS
        {


            template<class InputDataT, class T>
            constexpr bool IsDefined()
            {
                if (Utilities::Tuple::IndexOf<T, typename InputDataT::Tuple>::value
                    != NumericNS::UninitializedIndex<std::size_t>())
                    return true;

                // If InputDataNS::Solid is defined, ALL parameters are defined and true should be returned!
                return (Utilities::Tuple::IndexOf<::MoReFEM::InputDataNS::Solid, typename InputDataT::Tuple>::value
                        != NumericNS::UninitializedIndex<std::size_t>());
            }


        } // namespace SolidNS


    } // namespace Internal


    template<class InputDataT>
    Solid::Solid(const InputDataT& input_data,
                 const Domain& domain,
                 const QuadratureRulePerTopology& quadrature_rule_per_topology,
                 const double relative_tolerance)
    : domain_(domain), quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using SolidIP = InputDataNS::Solid;

        volumic_mass_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass", domain, input_data);

        if constexpr ((Internal::SolidNS::IsDefined<InputDataT, SolidIP::LameLambda>())
                      || (Internal::SolidNS::IsDefined<InputDataT, SolidIP::LameMu>()))
        {
            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::LameLambda>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::LameMu>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameLambda>("Lame lambda", domain, input_data);
            std::get<1>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameMu>("Lame mu", domain, input_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<InputDataT, SolidIP::YoungModulus>())
                      || (Internal::SolidNS::IsDefined<InputDataT, SolidIP::PoissonRatio>()))
        {
            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::PoissonRatio>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::YoungModulus>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(young_poisson_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>("Young modulus", domain, input_data);


            std::get<1>(young_poisson_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>("Poisson ratio", domain, input_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<InputDataT, SolidIP::Kappa1>())
                      || (Internal::SolidNS::IsDefined<InputDataT, SolidIP::Kappa2>()))
        {
            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::Kappa1>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::Kappa2>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa1>("Kappa_1", domain, input_data);

            std::get<1>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa2>("Kappa_2", domain, input_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<InputDataT, SolidIP::HyperelasticBulk>())
        {
            hyperelastic_bulk_ = InitScalarParameterFromInputData<InputDataNS::Solid::HyperelasticBulk>(
                "Hyperelastic bulk", domain, input_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<InputDataT, SolidIP::Viscosity>())
        {
            viscosity_ =
                InitScalarParameterFromInputData<InputDataNS::Solid::Viscosity>("Viscosity", domain, input_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu1>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::C0>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::C1>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::C2>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::C3>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu2>())
        {
            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<0>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu1>("Mu1", domain, input_data);
            std::get<1>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu2>("Mu2", domain, input_data);
            std::get<2>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C0>("C0", domain, input_data);

            std::get<3>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C1>("C1", domain, input_data);

            std::get<4>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C2>("C2", domain, input_data);

            std::get<5>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C3>("C3", domain, input_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<InputDataT, SolidIP::C4>()
                      || Internal::SolidNS::IsDefined<InputDataT, SolidIP::C5>())
        {
            static_assert(Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C4>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::C5>()
                              && Internal::SolidNS::IsDefined<InputDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<6>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C4>("C4", domain, input_data);

            std::get<7>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C5>("C5", domain, input_data);
        } // if constexpr C4 / C5

        if (relative_tolerance >= 0.)
            CheckConsistency(relative_tolerance);
    }


    inline const Domain& Solid::GetDomain() const noexcept
    {
        return domain_;
    }


    inline const QuadratureRulePerTopology& Solid::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    inline const Solid::scalar_parameter& Solid::GetVolumicMass() const
    {
        if (!volumic_mass_)
            throw SolidNS::UndefinedData("Volumic mass", __FILE__, __LINE__);

        return *volumic_mass_;
    }


    inline const Solid::scalar_parameter& Solid::GetHyperelasticBulk() const
    {
        if (!IsHyperelasticBulk())
            throw SolidNS::UndefinedData("Hyperelastic bulk", __FILE__, __LINE__);

        assert(!(!hyperelastic_bulk_));
        return *hyperelastic_bulk_;
    }

    inline const Solid::scalar_parameter& Solid::GetKappa1() const
    {
        if (!IsKappa1())
            throw SolidNS::UndefinedData("Kappa1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetKappa2() const
    {
        if (!IsKappa2())
            throw SolidNS::UndefinedData("Kappa2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetYoungModulus() const
    {
        if (!IsYoungModulus())
            throw SolidNS::UndefinedData("YoungModulus", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetPoissonRatio() const
    {
        if (!IsPoissonRatio())
            throw SolidNS::UndefinedData("PoissonRatio", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameLambda() const
    {
        if (!IsLameLambda())
            throw SolidNS::UndefinedData("Lame lambda", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameMu() const
    {
        if (!IsLameMu())
            throw SolidNS::UndefinedData("Lame mu", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetMu1() const
    {
        if (!IsMu1())
            throw SolidNS::UndefinedData("Mu1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetMu2() const
    {
        if (!IsMu2())
            throw SolidNS::UndefinedData("Mu2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC0() const
    {
        if (!IsC0())
            throw SolidNS::UndefinedData("C0", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<2>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC1() const
    {
        if (!IsC1())
            throw SolidNS::UndefinedData("C1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<3>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC2() const
    {
        if (!IsC2())
            throw SolidNS::UndefinedData("C2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<4>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC3() const
    {
        if (!IsC3())
            throw SolidNS::UndefinedData("C3", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<5>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC4() const
    {
        if (!IsC4())
            throw SolidNS::UndefinedData("C4", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<6>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC5() const
    {
        if (!IsC5())
            throw SolidNS::UndefinedData("C5", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<7>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetViscosity() const
    {
        if (!IsViscosity())
            throw SolidNS::UndefinedData("Viscosity", __FILE__, __LINE__);

        assert(!(!viscosity_));
        return *viscosity_;
    }


    inline bool Solid::IsHyperelasticBulk() const noexcept
    {
        return hyperelastic_bulk_ != nullptr;
    }


    inline bool Solid::IsKappa1() const noexcept
    {
        return std::get<0>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsKappa2() const noexcept
    {
        return std::get<1>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsYoungModulus() const noexcept
    {
        return std::get<0>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsPoissonRatio() const noexcept
    {
        return std::get<1>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsLameLambda() const noexcept
    {
        return std::get<0>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsLameMu() const noexcept
    {
        return std::get<1>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsMu1() const noexcept
    {
        return std::get<0>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsMu2() const noexcept
    {
        return std::get<1>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC0() const noexcept
    {
        return std::get<2>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC1() const noexcept
    {
        return std::get<3>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC2() const noexcept
    {
        return std::get<4>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC3() const noexcept
    {
        return std::get<5>(mu_i_C_i_) != nullptr;
    }

    inline bool Solid::IsC4() const noexcept
    {
        return std::get<6>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC5() const noexcept
    {
        return std::get<7>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsViscosity() const noexcept
    {
        return viscosity_ != nullptr;
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
