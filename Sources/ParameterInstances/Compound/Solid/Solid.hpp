/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp" // IWYU pragma: export

#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Contains all the \a Parameter that are related to the properties of a solid.
     *
     * Some of them might be deactivated if not used, with a properly placed "ignore" in the input data file given
     * as constructor argument.
     */
    class Solid
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Solid;

        //! Alias to unique pointer to const object.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique pointer to const object.
        template<std::size_t N>
        using array_const_unique_ptr = std::array<const_unique_ptr, N>;

        //! Alias to scalar parameter.
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] relative_tolerance Relative tolerance acceptable between a parameter and its
         * recomputation (for instance Young modulus read agains Young modulus recomputed from Lame parameters).
         * If they are not deemed exactly equal but close enough, a warning is printed
         * rather than an exception thrown. If a negative value is provided, skip entirely those checks.
         * \copydoc doxygen_hide_input_data_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         * \param[in] domain Mesh upon which the solid \a Parameter are defined.
         */
        template<class InputDataT>
        explicit Solid(const InputDataT& input_data,
                       const Domain& domain,
                       const QuadratureRulePerTopology& quadrature_rule_per_topology,
                       double relative_tolerance = 1.e-5);

        //! Destructor.
        ~Solid() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Solid(const Solid& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Solid(Solid&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Solid& operator=(const Solid& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Solid& operator=(Solid&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Print the content of \a Solid assuming all parameters are spatially constant.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const;

        //! Constant accessor to the volumic mass.
        const scalar_parameter& GetVolumicMass() const;

        /*!
         * \class doxygen_hide_solid_optional_for_accessor
         *
         * \attention This method assumes the parameter is relevant for your model (i.e. it was addressed in the
         * input data file). If not, it should not be called at all!
         *
         * You may check existence priori to the call with IsXXX() methods, for
         * instance:
         * \code
         if (solid.IsKappa1())
         {
            decltype(auto) kappa1 = solid.GetKappa1();
            ...
         }
         * \endcode
         *
         * \return The \a Parameter object.
         */

        /*!
         * \brief Constant accessor to the hyperelastic bulk.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetHyperelasticBulk() const;

        /*!
         * \brief Constant accessor to Kappa1.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetKappa1() const;

        /*!
         * \brief Constant accessor to Kappa2.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetKappa2() const;

        /*!
         * \brief Constant accessor to the Young modulus.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetYoungModulus() const;

        /*!
         * \brief Constant accessor to the Poisson ratio.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetPoissonRatio() const;

        /*!
         * \brief Constant accessor to Lame lambda.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetLameLambda() const;

        /*!
         * \brief Constant accessor to Lame mu.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetLameMu() const;

        /*!
         * \brief Constant accessor to mu1.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetMu1() const;

        /*!
         * \brief Constant accessor to mu2.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetMu2() const;

        /*!
         * \brief Constant accessor to C0.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC0() const;

        /*!
         * \brief Constant accessor to C1.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC1() const;

        /*!
         * \brief Constant accessor to C2.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC2() const;

        /*!
         * \brief Constant accessor to C3.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC3() const;

        /*!
         * \brief Constant accessor to C4.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC4() const;

        /*!
         * \brief Constant accessor to C5.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetC5() const;

        /*!
         * \brief Constant accessor to the viscosity.
         *
         * \copydoc doxygen_hide_solid_optional_for_accessor
         */
        const scalar_parameter& GetViscosity() const;

      public:
        /*!
         * \class doxygen_hide_solid_is_method
         *
         * \brief Whether the parameter is relevant for the \a Model considered.
         *
         * It might be irrelevant in two ways:
         * - Either it's truly pointless for the \a Model, and it's not even present in the \a InputData.
         * - Or it's present but the user chose the value 'ignore' as the nature of the parameter.
         *
         * \return True if the related accessor may be used safely.
         */

        //! \copydoc doxygen_hide_solid_is_method
        bool IsHyperelasticBulk() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsKappa1() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsKappa2() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsYoungModulus() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsPoissonRatio() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsLameLambda() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsLameMu() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsMu1() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsMu2() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC0() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC1() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC2() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC3() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC4() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsC5() const noexcept;

        //! \copydoc doxygen_hide_solid_is_method
        bool IsViscosity() const noexcept;

      private:
        /*!
         * \brief Check the material parameters are auto-consistent.
         *
         * \param[in] relative_tolerance Relative tolerance acceptable between a parameter and its
         * recomputation. If they are not deemed exactly equal but close enough, a warning is printed
         * rather than an exception thrown.
         */
        void CheckConsistency(double relative_tolerance) const;

        //! Domain upon which the solid is described.
        const Domain& GetDomain() const noexcept;

        //! Quadrature rule to use for each type of topology.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

      private:
        //! Domain upon which the solid is described.
        const Domain& domain_;

        //! Quadrature rule to use for each type of topology.
        const QuadratureRulePerTopology& quadrature_rule_per_topology_;

        //! Volumic mass.
        scalar_parameter::unique_ptr volumic_mass_ = nullptr;

        /*!
         * \class doxygen_hide_solid_optional_singular
         *
         * Might stay nullptr if this solid parameter is not relevant for the current \a Model (i.e. it is not
         * an entry in the input data file).
         */

        /*!
         * \class doxygen_hide_solid_optional_plural
         *
         * Might stay nullptr if these solid parameters are not relevant for the current \a Model (i.e. it is not
         * entries in the input data file).
         */

        /*!
         * \brief Hyperelastic bulk.
         *
         * \copydoc doxygen_hide_solid_optional_singular
         */
        scalar_parameter::unique_ptr hyperelastic_bulk_ = nullptr;

        /*!
         * \brief Kappa1 and kappa2.
         *
         * \copydoc doxygen_hide_solid_optional_plural
         */
        scalar_parameter::array_unique_ptr<2> kappa_list_{ { nullptr, nullptr } };

        /*!
         * \brief Young modulus and poisson ratio.
         *
         * \copydoc doxygen_hide_solid_optional_plural
         */
        scalar_parameter::array_unique_ptr<2> young_poisson_{ { nullptr, nullptr } };

        /*!
         * \brief Lame coefficients (lambda first then mu).
         *
         * \copydoc doxygen_hide_solid_optional_plural
         */
        scalar_parameter::array_unique_ptr<2> lame_coeff_{ { nullptr, nullptr } };

        /*!
         * \brief Mu1, Mu2, C0 to C5.
         *
         * \copydoc doxygen_hide_solid_optional_plural
         */
        scalar_parameter::array_unique_ptr<8> mu_i_C_i_{
            { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
        };

        /*!
         * \brief Viscosity.
         *
         * \copydoc doxygen_hide_solid_optional_singular
         */
        scalar_parameter::unique_ptr viscosity_ = nullptr;
    };


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/Compound/Solid/Solid.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HPP_
