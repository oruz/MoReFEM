/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ParameterInstancesNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace InternalVariablePolicyNS
                {


                    template<class InputDataT>
                    InputAnalyticalPrestress::InputAnalyticalPrestress(const InputDataT& input_data,
                                                                       const Domain& domain)
                    : initial_value_internal_variable_(
                        Utilities::InputDataNS::
                            Extract<::MoReFEM::InputDataNS::AnalyticalPrestress::InitialCondition::ActiveStress>::Value(
                                input_data))
                    {
                        using type = ::MoReFEM::InputDataNS::AnalyticalPrestress::Contractility;

                        contractility_ = InitScalarParameterFromInputData<type>("Contractility", domain, input_data);
                    };


                    inline const ScalarParameter<>& InputAnalyticalPrestress::GetContractility() const noexcept
                    {
                        assert(!(!contractility_));
                        return *contractility_;
                    }


                    inline double InputAnalyticalPrestress::GetInitialValueInternalVariable() const noexcept
                    {
                        return initial_value_internal_variable_;
                    }


                } // namespace InternalVariablePolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace ParameterInstancesNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HXX_
