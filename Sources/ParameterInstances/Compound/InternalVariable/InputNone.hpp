/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 14:28:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_INPUT_NONE_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_INPUT_NONE_HPP_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ParameterInstancesNS
        {


            namespace InternalVariablePolicyNS
            {


                struct InputNone
                { };


            } // namespace InternalVariablePolicyNS


        } // namespace ParameterInstancesNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_INPUT_NONE_HPP_
