/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 27 May 2019 17:35:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Domain;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Contains all the \a Parameter that are related to the properties of a InputMicrosphere.
     *
     * Some of them might be deactivated if not used, with a properly placed "ignore" in the input data file given
     * as constructor argument.
     */
    class InputMicrosphere
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = InputMicrosphere;

        //! Alias to unique pointer to const object.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to array of unique pointer to const object.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to scalar parameter.
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         * \param[in] domain Mesh upon which the InputMicrosphere \a Parameter are defined.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         *
         */
        template<class InputDataT>
        explicit InputMicrosphere(const InputDataT& input_data,
                                  const Domain& domain,
                                  const QuadratureRulePerTopology& quadrature_rule_per_topology);

        //! Destructor.
        ~InputMicrosphere() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InputMicrosphere(const InputMicrosphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InputMicrosphere(InputMicrosphere&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InputMicrosphere& operator=(const InputMicrosphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InputMicrosphere& operator=(InputMicrosphere&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Print the content of \a InputMicrosphere assuming all parameters are spatially constant.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const;

        /*!
         * \class doxygen_hide_InputMicrosphere_optional_for_accessor
         *
         * \attention This method assumes the parameter is relevant for your model (i.e. it was addressed in the
         * input data file). If not, it should not be called at all!
         *
         * You may check existence priori to the call with IsXXX() methods, for
         * instance:
         * \code
         if (InputMicrosphere.IsInPlaneFiberDispersionI4())
         {
            decltype(auto) fiber_planedispersion_I4 = InputMicrosphere.GetInPlaneFiberDispersionI4();
            ...
         }
         * \endcode
         *
         * \return The \a Parameter object.
         */
        /*!


         * \brief Constant accessor to the in-plane fiber dispersion associated to the 4th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetInPlaneFiberDispersionI4() const;


        /*!
         * \brief Constant accessor to the out-of-plane fiber dispersion associated to the 4th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetOutOfPlaneFiberDispersionI4() const;

        /*!
         * \brief Constant accessor to stiffness of the fibers associated with the 4th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetFiberStiffnessDensityI4() const;

        /*!
         * \brief Constant accessor to the in-plane fiber dispersion associated to the 6th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetInPlaneFiberDispersionI6() const;

        /*!
         * \brief Constant accessor to the out-of-plane fiber dispersion associated to the 6th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetOutOfPlaneFiberDispersionI6() const;

        /*!
         * \brief Constant accessor to stiffness of the fibers associated with the 6th invariant.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_for_accessor
         */
        const scalar_parameter& GetFiberStiffnessDensityI6() const;

      public:
        /*!
         * \class doxygen_hide_InputMicrosphere_is_method
         *
         * \brief Whether the parameter is relevant for the \a Model considered.
         *
         * It might be irrelevant in two ways:
         * - Either it's truly pointless for the \a Model, and it's not even present in the \a InputData.
         * - Or it's present but the user chose the value 'ignore' as the nature of the parameter.
         *
         * \return True if the related accessor may be used safely.
         */

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsInPlaneFiberDispersionI4() const noexcept;

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsOutOfPlaneFiberDispersionI4() const noexcept;

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsFiberStiffnessDensityI4() const noexcept;

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsInPlaneFiberDispersionI6() const noexcept;

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsOutOfPlaneFiberDispersionI6() const noexcept;

        //! \copydoc doxygen_hide_InputMicrosphere_is_method
        bool IsFiberStiffnessDensityI6() const noexcept;

      private:
        //! Domain upon which the InputMicrosphere is described.
        const Domain& GetDomain() const noexcept;

        //! Quadrature rule to use for each type of topology.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

      private:
        //! Domain upon which the InputMicrosphere is described.
        const Domain& domain_;

        //! Quadrature rule to use for each type of topology.
        const QuadratureRulePerTopology& quadrature_rule_per_topology_;

        /*!
         * \class doxygen_hide_InputMicrosphere_optional_plural
         *
         * Might stay nullptr if these InputMicrosphere parameters are not relevant for the current \a Model (i.e. it is
         * not entries in the input data file).
         */

        /*!
         * \brief All six parameters to define the fiber dispersions (in and out of plane) and the stiffness related to
         * I4 and I6.
         *
         * \copydoc doxygen_hide_InputMicrosphere_optional_plural
         */
        scalar_parameter::array_unique_ptr<6> input_microsphere_{
            { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
        };
    };


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HPP_
