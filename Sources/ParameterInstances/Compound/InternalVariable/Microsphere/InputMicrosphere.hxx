/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 27 May 2019 17:35:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Exceptions/InputMicrosphere.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputMicrosphereNS
        {


            template<class InputDataT, class T>
            constexpr bool IsDefined()
            {
                if (Utilities::Tuple::IndexOf<T, typename InputDataT::Tuple>::value
                    != NumericNS::UninitializedIndex<std::size_t>())
                    return true;

                // If InputDataNS::InputMicrosphere is defined, ALL parameters are defined and true should be returned!
                return (
                    Utilities::Tuple::IndexOf<::MoReFEM::InputDataNS::Microsphere, typename InputDataT::Tuple>::value
                    != NumericNS::UninitializedIndex<std::size_t>());
            }


        } // namespace InputMicrosphereNS


    } // namespace Internal


    template<class InputDataT>
    InputMicrosphere::InputMicrosphere(const InputDataT& input_data,
                                       const Domain& domain,
                                       const QuadratureRulePerTopology& quadrature_rule_per_topology)
    : domain_(domain), quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using MicrosphereIP = InputDataNS::Microsphere;

        if constexpr (Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
                      || Internal::InputMicrosphereNS::IsDefined<InputDataT,
                                                                 MicrosphereIP::OutOfPlaneFiberDispersionI4>()
                      || Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::FiberStiffnessDensityI4>()
                      || Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
                      || Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
                      || Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::FiberStiffnessDensityI6>())
        {
            static_assert(
                Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
                    && Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
                    && Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::FiberStiffnessDensityI4>()
                    && Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
                    && Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::OutOfPlaneFiberDispersionI6>()
                    && Internal::InputMicrosphereNS::IsDefined<InputDataT, MicrosphereIP::FiberStiffnessDensityI6>(),
                "It makes no sense to define one and not the others");

            std::get<0>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::InPlaneFiberDispersionI4>(
                "InPlaneFiberDispersionI4", domain, input_data);

            std::get<1>(input_microsphere_) =
                InitScalarParameterFromInputData<MicrosphereIP::OutOfPlaneFiberDispersionI4>(
                    "OutOfPlaneFiberDispersionI4", domain, input_data);

            std::get<2>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::FiberStiffnessDensityI4>(
                "FiberStiffnessDensityI4", domain, input_data);

            std::get<3>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::InPlaneFiberDispersionI6>(
                "InPlaneFiberDispersionI6", domain, input_data);

            std::get<4>(input_microsphere_) =
                InitScalarParameterFromInputData<MicrosphereIP::OutOfPlaneFiberDispersionI6>(
                    "OutOfPlaneFiberDispersionI6", domain, input_data);

            std::get<5>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::FiberStiffnessDensityI6>(
                "FiberStiffnessDensityI6", domain, input_data);
        }
    }


    inline const Domain& InputMicrosphere::GetDomain() const noexcept
    {
        return domain_;
    }


    inline const QuadratureRulePerTopology& InputMicrosphere::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetInPlaneFiberDispersionI4() const
    {
        if (!IsInPlaneFiberDispersionI4())
            throw InputMicrosphereNS::UndefinedData("InPlaneFiberDispersionI4", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetOutOfPlaneFiberDispersionI4() const
    {
        if (!IsOutOfPlaneFiberDispersionI4())
            throw InputMicrosphereNS::UndefinedData("OutOfPlaneFiberDispersionI4", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetFiberStiffnessDensityI4() const
    {
        if (!IsFiberStiffnessDensityI4())
            throw InputMicrosphereNS::UndefinedData("FiberStiffnessDensityI4", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<2>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetInPlaneFiberDispersionI6() const
    {
        if (!IsInPlaneFiberDispersionI6())
            throw InputMicrosphereNS::UndefinedData("InPlaneFiberDispersionI6", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<3>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetOutOfPlaneFiberDispersionI6() const
    {
        if (!IsOutOfPlaneFiberDispersionI6())
            throw InputMicrosphereNS::UndefinedData("OutOfPlaneFiberDispersionI6", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<4>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const InputMicrosphere::scalar_parameter& InputMicrosphere::GetFiberStiffnessDensityI6() const
    {
        if (!IsFiberStiffnessDensityI6())
            throw InputMicrosphereNS::UndefinedData("FiberStiffnessDensityI6", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<5>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    inline bool InputMicrosphere::IsInPlaneFiberDispersionI4() const noexcept
    {
        return std::get<0>(input_microsphere_) != nullptr;
    }


    inline bool InputMicrosphere::IsOutOfPlaneFiberDispersionI4() const noexcept
    {
        return std::get<1>(input_microsphere_) != nullptr;
    }


    inline bool InputMicrosphere::IsFiberStiffnessDensityI4() const noexcept
    {
        return std::get<2>(input_microsphere_) != nullptr;
    }


    inline bool InputMicrosphere::IsInPlaneFiberDispersionI6() const noexcept
    {
        return std::get<3>(input_microsphere_) != nullptr;
    }


    inline bool InputMicrosphere::IsOutOfPlaneFiberDispersionI6() const noexcept
    {
        return std::get<4>(input_microsphere_) != nullptr;
    }


    inline bool InputMicrosphere::IsFiberStiffnessDensityI6() const noexcept
    {
        return std::get<5>(input_microsphere_) != nullptr;
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INPUT_MICROSPHERE_HXX_
