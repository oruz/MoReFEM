/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 27 May 2019 17:35:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include <sstream>

#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"


namespace MoReFEM
{


    void InputMicrosphere::Print(std::ostream& out) const
    {
        out << "Values of parameters (assumed constant to be able to call the current method) are:" << std::endl;


        if (IsInPlaneFiberDispersionI4())
        {
            out << "\t- InPlaneFiberDispersionI4 = " << GetInPlaneFiberDispersionI4().GetConstantValue() << std::endl;
            out << "\t- OutOfPlaneFiberDispersionI4 = " << GetOutOfPlaneFiberDispersionI4().GetConstantValue()
                << std::endl;
            out << "\t- FiberStiffnessDensityI4 = " << GetFiberStiffnessDensityI4().GetConstantValue() << std::endl;
            out << "\t- InPlaneFiberDispersionI6 = " << GetInPlaneFiberDispersionI6().GetConstantValue() << std::endl;
            out << "\t- OutOfPlaneFiberDispersionI6 = " << GetOutOfPlaneFiberDispersionI6().GetConstantValue()
                << std::endl;
            out << "\t- FiberStiffnessDensityI6 = " << GetFiberStiffnessDensityI6().GetConstantValue() << std::endl;
        }
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
