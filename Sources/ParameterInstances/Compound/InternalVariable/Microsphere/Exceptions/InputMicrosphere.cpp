/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 27 May 2019 17:35:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include <sstream>
#include <string> // IWYU pragma: keep

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Exceptions/InputMicrosphere.hpp"


namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name);


} // namespace


namespace MoReFEM
{


    namespace InputMicrosphereNS
    {


        UndefinedData::UndefinedData(const std::string& data, const char* invoking_file, int invoking_line)
        : ::MoReFEM::Exception(UndefinedDataMsg(data), invoking_file, invoking_line)
        { }


        UndefinedData::~UndefinedData() = default;


    } // namespace InputMicrosphereNS


} // namespace MoReFEM


namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name)
    {
        std::ostringstream oconv;
        oconv << "Data '" << data_name
              << "' is undefined: in all likelihood either the model is ill-formed and the "
                 "data is not in the InputData, or it is but in the Lua file the chosen nature was 'ignore'.";
        std::string ret = oconv.str();
        return ret;
    }


} // namespace


// @} // addtogroup ParameterInstancesGroup
