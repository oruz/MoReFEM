/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 11:24:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_

// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalParameterOperatorNS
        {


            template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT, std::size_t NfeltSpace>
            const std::string& FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::ClassName()
            {
                static std::string ret = std::string("GlobalParameterOperatorNS::FromAtDofToAtQuadPt<")
                                         + ::MoReFEM::ParameterNS::Name<TypeT>() + ">";
                return ret;
            }


            template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT, std::size_t NfeltSpace>
            FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::FromAtDofToAtQuadPt(
                const FEltSpace& felt_space,
                const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& param_at_dof,
                const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_to_set)
            : parent(felt_space,
                     param_at_dof.GetUnknown(),
                     quadrature_rule_per_topology,
                     AllocateGradientFEltPhi::no,
                     parameter_to_set,
                     param_at_dof)
            { }


            template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT, std::size_t NfeltSpace>
            inline void FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>::Update() const
            {
                return parent::UpdateImpl();
            }


        } // namespace GlobalParameterOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HXX_
