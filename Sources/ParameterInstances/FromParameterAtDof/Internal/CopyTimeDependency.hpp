/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 17:48:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HPP_

// No include here: this include should be used at only one place in which all includes are already in place.

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Static dispatcher for time dependency.
             *
             * \copydetails doxygen_hide_at_dof_policy_tparam
             * \tparam TimeDependencyT Policy in charge of time dependency.
             *
             * TimeDependency is copied except when the time dependency is TimeDependencyNS::None.
             */
            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     std::size_t NfeltSpaceT>
            struct CopyTimeDependency
            {

                //! Alias for the \a ParameterAtDof, which acts as the source one.
                using at_dof_type = ParameterAtDof<TypeT, TimeDependencyT, NfeltSpaceT>;

                //! Alias for the \a ParameterAtQuadraturePoint, which acts as the target one.
                using at_quad_pt_type = ParameterAtQuadraturePoint<TypeT, TimeDependencyT>;


                /*!
                 * \brief Static method that performs the actual work.
                 *
                 * \param[in] param_at_dof Source parameter, from which the time dependency is taken.
                 * \param[in,out] new_param Target parameter, which will get a copy of the time dependency after the
                 * functions does its bidding.
                 */
                static void Perform(const at_dof_type& param_at_dof, at_quad_pt_type& new_param);
            };


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            template<ParameterNS::Type TypeT, std::size_t NfeltSpaceT>
            struct CopyTimeDependency<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>
            {


                using at_dof_type = ParameterAtDof<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>;

                using at_quad_pt_type =
                    ParameterAtQuadraturePoint<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>;


                static void Perform(const at_dof_type& param_at_dof, at_quad_pt_type& new_param);
            };
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HPP_
