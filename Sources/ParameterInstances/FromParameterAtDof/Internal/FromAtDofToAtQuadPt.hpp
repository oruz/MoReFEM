/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 11:24:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtDof.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "ParameterInstances/FromParameterAtDof/Internal/Local/FromAtDofToAtQuadPt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalParameterOperatorNS
        {


            /*!
             * \brief Child of \a GlobalParameterOperator which set the values at \a QuadraturePoint of
             * a \a ParameterAtQuadraturePoint from those of a \a ParameterAtDof.
             *
             * An instance of this class does the job for only one dimension; if several \a FEltSpace are covered
             * by the \a ParameterAtDof there will be one such object per \a FEltSpace.
             *
             * \copydetails doxygen_hide_at_dof_policy_tparam
             * \tparam TimeDependencyT Policy in charge of time dependency.
             */
            template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT, std::size_t NfeltSpace>
            class FromAtDofToAtQuadPt final
            : public GlobalParameterOperator<
                  FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>,
                  LocalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>,
                  TypeT>
            {

              public:
                //! Alias to self.
                using self = FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Alias to pendant local operator.
                using local_operator_type =
                    LocalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>;

                //! Convenient alias to pinpoint the GlobalParameterOperator parent.
                using parent = GlobalParameterOperator<self, local_operator_type, TypeT>;

                //! Friendship to the parent class so that the CRTP can reach private methods defined below.
                friend parent;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \class doxygen_hide_from_at_dof_to_quad_pt_param_args
                 *
                 * \param[in] param_at_dof \a ParameterAtDof from which values at \a QuadraturePoint are copied.
                 * \param[in,out] parameter_to_set \a ParameterAtQuadraturePoint into which values at \a QuadraturePoint
                 */

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] felt_space \a FEltSpace which values are copied.
                 * \copydoc doxygen_hide_from_at_dof_to_quad_pt_param_args
                 * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
                 */
                explicit FromAtDofToAtQuadPt(const FEltSpace& felt_space,
                                             const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& param_at_dof,
                                             const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                             ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_to_set);

                //! Destructor.
                ~FromAtDofToAtQuadPt() = default;

                //! \copydoc doxygen_hide_copy_constructor
                FromAtDofToAtQuadPt(const FromAtDofToAtQuadPt& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                FromAtDofToAtQuadPt(FromAtDofToAtQuadPt&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                FromAtDofToAtQuadPt& operator=(const FromAtDofToAtQuadPt& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                FromAtDofToAtQuadPt& operator=(FromAtDofToAtQuadPt&& rhs) = delete;

                ///@}

                /*!
                 * \brief Update the \a ParameterAtQuadraturePoint (which is the whole point of currrent class!)
                 */
                void Update() const;
            };


        } // namespace GlobalParameterOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_
