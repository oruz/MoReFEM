/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 11:24:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Parameter.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalParameterOperatorNS
        {


            /*!
             * \brief Local counterpart of \a GlobalParameterOperatorNS::FromAtDofToAtQuadPt.
             *
             */

            template<ParameterNS::Type TypeT, template<ParameterNS::Type> class TimeDependencyT, std::size_t NfeltSpace>
            class FromAtDofToAtQuadPt final : public Advanced::LocalParameterOperator<TypeT>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpace>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to parent.
                using parent = Advanced::LocalParameterOperator<TypeT>;

                //! Alias to 'inherit' from parent: elementary_data_type.
                using elementary_data_type = typename parent::elementary_data_type;

                //! Alias to the return type of the parameter.
                using return_type = typename ParameterNS::Traits<TypeT>::return_type;

                //! Alias to non constant reference to the parameter value.
                using non_constant_reference = typename ParameterNS::Traits<TypeT>::non_constant_reference;

                //! Returns the name of the operator.
                static const std::string& ClassName();


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] unknown List of unknowns considered by the operators. Its type (vector_shared_ptr)
                 * is due to constraints from genericity; for current operator it is expected to hold exactly
                 * two unknowns (the first one vectorial and the second one scalar).
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \copydoc doxygen_hide_from_at_dof_to_quad_pt_param_args
                 *
                 *
                 * \internal This constructor must not be called manually: it is involved only in
                 * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
                 * \endinternal
                 */
                explicit FromAtDofToAtQuadPt(const ExtendedUnknown::const_shared_ptr& unknown,
                                             elementary_data_type&& elementary_data,
                                             ParameterAtQuadraturePoint<TypeT, TimeDependencyT>& parameter_to_set,
                                             const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& param_at_dof);

                //! Destructor.
                ~FromAtDofToAtQuadPt() = default;

                //! \copydoc doxygen_hide_copy_constructor
                FromAtDofToAtQuadPt(const FromAtDofToAtQuadPt& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                FromAtDofToAtQuadPt(FromAtDofToAtQuadPt&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                FromAtDofToAtQuadPt& operator=(const FromAtDofToAtQuadPt& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                FromAtDofToAtQuadPt& operator=(FromAtDofToAtQuadPt&& rhs) = delete;

                ///@}


                //! Compute the elementary vector.
                void ComputeEltArray();


              private:
                //! Initial \a ParameterAtDof, from which we want to build a \a ParameterFromParameterAtDof.
                const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& GetParamAtDof() const noexcept;


              private:
                //! Initial \a ParameterAtDof, from which we want to build a \a ParameterFromParameterAtDof.
                const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpace>& param_at_dof_;
            };


        } // namespace LocalParameterOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/FromParameterAtDof/Internal/Local/FromAtDofToAtQuadPt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_LOCAL_x_FROM_AT_DOF_TO_AT_QUAD_PT_HPP_
