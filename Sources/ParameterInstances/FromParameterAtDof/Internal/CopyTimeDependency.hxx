/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 17:48:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HXX_

// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     std::size_t NfeltSpaceT>
            void CopyTimeDependency<TypeT, TimeDependencyT, NfeltSpaceT>::Perform(const at_dof_type& param_at_dof,
                                                                                  at_quad_pt_type& new_param)
            {
                auto&& time_dependency = std::make_unique<TimeDependencyT<TypeT>>(param_at_dof.GetTimeDependency());
                new_param.SetTimeDependency(std::move(time_dependency));
            }


            template<ParameterNS::Type TypeT, std::size_t NfeltSpaceT>
            void CopyTimeDependency<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>::Perform(
                const at_dof_type&,
                at_quad_pt_type&)
            {
                // Do nothing.
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_INTERNAL_x_COPY_TIME_DEPENDENCY_HXX_
