/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 16:10:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/

#include <cassert>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>::
                ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            {
                assert(!NumericNS::AreEqual(poisson_ratio, -1.));
                assert(!NumericNS::AreEqual(poisson_ratio, 0.5));

                const double inv = 1. / (1. - poisson_ratio);

                auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

                const double factor =
                    young_modulus * (1. - poisson_ratio) / ((1. + poisson_ratio) * (1. - 2 * poisson_ratio));

                engineering_elasticity_tensor(0, 0) = factor;
                engineering_elasticity_tensor(1, 1) = factor;
                engineering_elasticity_tensor(2, 2) = 0.5 * inv * factor * (1. - 2. * poisson_ratio);
                engineering_elasticity_tensor(0, 1) = factor * poisson_ratio * inv;
                engineering_elasticity_tensor(1, 0) = engineering_elasticity_tensor(0, 1);

                return engineering_elasticity_tensor;
            }


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>::
                ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            {
                assert(!NumericNS::AreEqual(poisson_ratio, 1.));
                assert(!NumericNS::IsZero(poisson_ratio));

                const double factor = young_modulus / (1. - NumericNS::Square(poisson_ratio));
                auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

                engineering_elasticity_tensor(0, 0) = factor;
                engineering_elasticity_tensor(1, 1) = factor;
                engineering_elasticity_tensor(2, 2) = 0.5 * factor * (1. - poisson_ratio);
                engineering_elasticity_tensor(0, 1) = factor * poisson_ratio;
                engineering_elasticity_tensor(1, 0) = engineering_elasticity_tensor(0, 1);

                return engineering_elasticity_tensor;
            }


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>::
                ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            {
                assert(!NumericNS::AreEqual(poisson_ratio, -1.));
                assert(!NumericNS::AreEqual(poisson_ratio, 0.5));

                const double factor =
                    young_modulus * (1. - poisson_ratio) / ((1. + poisson_ratio) * (1. - 2. * poisson_ratio));

                auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

                engineering_elasticity_tensor(0, 0) = factor;
                engineering_elasticity_tensor(1, 1) = factor;
                engineering_elasticity_tensor(2, 2) = factor;

                const double inv = factor / (1. - poisson_ratio);

                engineering_elasticity_tensor(3, 3) = 0.5 * (1. - 2. * poisson_ratio) * inv;
                engineering_elasticity_tensor(4, 4) = engineering_elasticity_tensor(3, 3);
                engineering_elasticity_tensor(5, 5) = engineering_elasticity_tensor(3, 3);

                {
                    const double non_diag_value = poisson_ratio * inv;

                    for (auto i = 0ul; i < 3; ++i)
                    {
                        for (auto j = i + 1; j < 3; ++j)
                            engineering_elasticity_tensor(i, j) = engineering_elasticity_tensor(j, i) = non_diag_value;
                    }
                }

                return engineering_elasticity_tensor;
            }


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>::
                ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            {
                static_cast<void>(poisson_ratio);
                assert(NumericNS::AreEqual(poisson_ratio, 0.));

                auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

                engineering_elasticity_tensor(0, 0) = young_modulus;

                return engineering_elasticity_tensor;
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
