/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 16:10:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_

// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            ComputeGradientBasedElasticityTensor<ConfigurationT>::ComputeGradientBasedElasticityTensor()
            {
                result_.resize(std::vector<std::size_t>{ traits::result_size, traits::result_size });
                engineering_elasticity_tensor_.resize(
                    std::vector<std::size_t>{ traits::engineering_size, traits::engineering_size });
                engineering_elasticity_tensor_.fill(0.);
                intermediate_product_.resize(std::vector<std::size_t>{ traits::engineering_size, traits::result_size });
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstResult()
            {
                return const_cast<LocalMatrix&>(GetResult());
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetResult() const noexcept
            {
                assert(result_.shape(0) == traits::result_size);
                assert(result_.shape(1) == traits::result_size);

                return result_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix&
            ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstEngineeringElasticityTensor()
            {
                assert(engineering_elasticity_tensor_.shape(0) == traits::engineering_size);
                assert(engineering_elasticity_tensor_.shape(1) == traits::engineering_size);

                return engineering_elasticity_tensor_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstIntermediateProduct()
            {
                assert(intermediate_product_.shape(0) == traits::engineering_size);
                assert(intermediate_product_.shape(1) == traits::result_size);

                return intermediate_product_;
            }


            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::Compute(const double young_modulus,
                                                                                             const double poisson_ratio)
            {
                const auto& engineering_elasticity_tensor =
                    ComputeEngineeringElasticityTensor(young_modulus, poisson_ratio);

                const auto& gradient_2_strain = Gradient2Strain<traits::dimension>();
                auto& intermediate_product = GetNonCstIntermediateProduct();

                xt::noalias(intermediate_product) = xt::linalg::dot(engineering_elasticity_tensor, gradient_2_strain);
                auto& result = GetNonCstResult();
                xt::noalias(result) =
                    xt::linalg::dot(xt::transpose(Gradient2Strain<traits::dimension>()), intermediate_product);

                return result_;
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
