/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 15:35:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HPP_

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Matrix defined so that strain = Gradient2Strain x gradient(displacement)
             *
             * No definition provided on purpose: this function is intended to be specialized.
             *
             * \tparam DimensionT Dimension of the mesh in which Parameter is defined.
             *
             * \return Matrix defined so that strain = Gradient2Strain x gradient(displacement)
             */
            template<int DimensionT>
            const LocalMatrix& Gradient2Strain();


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================

            template<>
            const LocalMatrix& Gradient2Strain<1>();

            template<>
            const LocalMatrix& Gradient2Strain<2>();

            template<>
            const LocalMatrix& Gradient2Strain<3>();

            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HPP_
