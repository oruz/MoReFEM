/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 14:28:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_CONFIGURATION_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_CONFIGURATION_HPP_

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace TraitsNS
            {


                /*!
                 * \brief Purpose of this traits struct is to provide few enum values depending of the configuration.
                 *
                 * \tparam Configuration Configuration to consider in the GradientBasedElasticityTensor (plane stress,
                 * plain strain or 3d).
                 */
                template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
                struct Configuration;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                template<>
                struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>
                {
                    enum
                    {
                        dimension = 1,
                        engineering_size = 1,
                        result_size = 1
                    };
                };


                template<>
                struct Configuration<
                    ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>
                {
                    enum
                    {
                        dimension = 2,
                        engineering_size = 3,
                        result_size = 4
                    };
                };


                template<>
                struct Configuration<
                    ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>
                {
                    enum
                    {
                        dimension = 2,
                        engineering_size = 3,
                        result_size = 4
                    };
                };


                template<>
                struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>
                {
                    enum
                    {
                        dimension = 3,
                        engineering_size = 6,
                        result_size = 9
                    };
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace TraitsNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_CONFIGURATION_HPP_
