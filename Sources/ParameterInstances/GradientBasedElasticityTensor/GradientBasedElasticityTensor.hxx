/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 15:35:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_

// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        GradientBasedElasticityTensor<ConfigurationT>::GradientBasedElasticityTensor(
            const scalar_parameter& young_modulus,
            const scalar_parameter& poisson_ratio)
        : parent("Gradient-based elasticity tensor.", young_modulus.GetDomain()), young_modulus_(young_modulus),
          poisson_ratio_(poisson_ratio)
        {
            assert(GetDomain() == poisson_ratio.GetDomain());

            // #926
            if (IsConstant())
                ComputeValue(young_modulus.GetConstantValue(), poisson_ratio.GetConstantValue());
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        typename GradientBasedElasticityTensor<ConfigurationT>::return_type
        GradientBasedElasticityTensor<ConfigurationT>::SupplGetValue(const local_coords_type& local_coords,
                                                                     const GeometricElt& geom_elt) const
        {
            const double young_modulus = GetYoungModulus().GetValue(local_coords, geom_elt);
            const double poisson_ratio = GetPoissonRatio().GetValue(local_coords, geom_elt);

            return ComputeValue(young_modulus, poisson_ratio);
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        const typename GradientBasedElasticityTensor<ConfigurationT>::scalar_parameter&
        GradientBasedElasticityTensor<ConfigurationT>::GetYoungModulus() const
        {
            return young_modulus_;
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        const typename GradientBasedElasticityTensor<ConfigurationT>::scalar_parameter&
        GradientBasedElasticityTensor<ConfigurationT>::GetPoissonRatio() const
        {
            return poisson_ratio_;
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        bool GradientBasedElasticityTensor<ConfigurationT>::IsConstant() const
        {
            return GetYoungModulus().IsConstant() && GetPoissonRatio().IsConstant();
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        inline typename GradientBasedElasticityTensor<ConfigurationT>::return_type
        GradientBasedElasticityTensor<ConfigurationT>::ComputeValue(const double young_modulus,
                                                                    const double poisson_ratio) const
        {
            return helper_.Compute(young_modulus, poisson_ratio);
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        typename GradientBasedElasticityTensor<ConfigurationT>::return_type
        GradientBasedElasticityTensor<ConfigurationT>::SupplGetConstantValue() const
        {
            assert(IsConstant());

            return helper_.GetResult();
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        void GradientBasedElasticityTensor<ConfigurationT>::SupplWrite(std::ostream& out) const
        {
            out << "# This matrix parameter is defined from Young modulus and Poisson ratio, which values are defined "
                   "the "
                   "following way:"
                << std::endl;
            out << std::endl;
            GetYoungModulus().Write(out);
            out << std::endl;
            GetPoissonRatio().Write(out);
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        void GradientBasedElasticityTensor<ConfigurationT>::SupplTimeUpdate()
        {
            // #926
            assert(!GetYoungModulus().IsTimeDependent());
            assert(!GetPoissonRatio().IsTimeDependent());
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        void GradientBasedElasticityTensor<ConfigurationT>::SupplTimeUpdate(double time)
        {
            // #926
            assert(!GetYoungModulus().IsTimeDependent());
            assert(!GetPoissonRatio().IsTimeDependent());
            static_cast<void>(time);
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        typename GradientBasedElasticityTensor<ConfigurationT>::return_type
        GradientBasedElasticityTensor<ConfigurationT>::SupplGetAnyValue() const
        {
            return helper_.GetResult();
        }


        template<GradientBasedElasticityTensorConfiguration ConfigurationT>
        inline void GradientBasedElasticityTensor<ConfigurationT>::SetConstantValue(double value)
        {
            static_cast<void>(value);
            assert(false);
            exit(EXIT_FAILURE);
        }

    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_GRADIENT_BASED_ELASTICITY_TENSOR_HXX_
