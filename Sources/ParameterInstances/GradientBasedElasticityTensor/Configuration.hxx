/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 14:28:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HXX_

// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace ParameterNS
    {


        template<class InputDataT>
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration
        ReadGradientBasedElasticityTensorConfigurationFromFile(const std::size_t dimension,
                                                               const InputDataT& input_data)
        {
            // First case: dimension is 3.
            if (dimension == 3)
            {
                return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3;
            } else if (dimension == 2) // If dimension is 2, read the kinematric parameter.
            {
                namespace IPL = Utilities::InputDataNS;
                using Solid = InputDataNS::Solid;
                decltype(auto) str_kinematic_parameter = IPL::Extract<Solid::PlaneStressStrain>::Value(input_data);

                if (str_kinematic_parameter == "plane_strain")
                    return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain;

                if (str_kinematic_parameter == "plane_stress")
                    return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress;
            } else
            {
                return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1;
            }

            assert(false && "LuaOptionFile should have filtered out any other value!");
            exit(EXIT_FAILURE);
        }


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_CONFIGURATION_HXX_
