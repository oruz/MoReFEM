/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HXX_

// IWYU pragma: private, include "ParameterInstances/ScalarParameterFromFile/ScalarParameterFromFile.hpp"


#include <cassert>
#include <iosfwd>
#include <memory>
#include <type_traits>

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class TimeManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class T>
    ScalarParameterFromFile::ScalarParameterFromFile(T&& name,
                                                     const Domain& domain,
                                                     const TimeManager& time_manager,
                                                     const std::string& file)
    {
        using parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::scalar,
                                                                        ParameterNS::Policy::Constant,
                                                                        ParameterNS::TimeDependencyFromFile>;

        internal_parameter_ = std::make_unique<parameter_type>(std::forward<T>(name), domain, 1.);

        auto time_dep =
            std::make_unique<ParameterNS::TimeDependencyFromFile<ParameterNS::Type::scalar>>(time_manager, file);

        GetNonCstInternalParameter().SetTimeDependency(std::move(time_dep));
    }


    inline void ScalarParameterFromFile::TimeUpdate()
    {
        return GetNonCstInternalParameter().TimeUpdate();
    }


    inline void ScalarParameterFromFile::TimeUpdate(double time)
    {
        return GetNonCstInternalParameter().TimeUpdate(time);
    }


    inline ScalarParameterFromFile::return_type ScalarParameterFromFile::GetConstantValue() const
    {
        return GetInternalParameter().GetConstantValue();
    }


    inline bool ScalarParameterFromFile::IsConstant() const
    {
        return GetInternalParameter().IsConstant();
    }


    inline void ScalarParameterFromFile::Write(std::ostream& out) const
    {
        GetInternalParameter().Write(out);
    }

    inline void ScalarParameterFromFile::Write(const std::string& filename) const
    {
        GetInternalParameter().Write(filename);
    }

    inline const Domain& ScalarParameterFromFile::GetDomain() const noexcept
    {
        return GetInternalParameter().GetDomain();
    }

    inline bool ScalarParameterFromFile::IsTimeDependent() const noexcept
    {
        return GetInternalParameter().IsTimeDependent();
    }


    inline const ParameterNS::TimeDependencyFromFile<ParameterNS::Type::scalar>&
    ScalarParameterFromFile::GetTimeDependency() const noexcept
    {
        return GetInternalParameter().GetTimeDependency();
    }


    inline const ScalarParameterFromFile::internal_param_type&
    ScalarParameterFromFile ::GetInternalParameter() const noexcept
    {
        assert(!(!internal_parameter_));
        return *internal_parameter_;
    }


    inline ScalarParameterFromFile::internal_param_type& ScalarParameterFromFile::GetNonCstInternalParameter() noexcept
    {
        return const_cast<internal_param_type&>(GetInternalParameter());
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HXX_
