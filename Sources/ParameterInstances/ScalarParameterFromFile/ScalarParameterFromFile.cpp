/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include "ParameterInstances/ScalarParameterFromFile/ScalarParameterFromFile.hpp"


namespace MoReFEM
{


    ScalarParameterFromFile::~ScalarParameterFromFile() = default;


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


/// @} // addtogroup ParametersGroup
