/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 May 2015 14:24:14 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_MU_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_MU_HXX_

// IWYU pragma: private, include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameMu.hpp"

#include <cassert>
#include <cstdlib>
#include <limits>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "Parameters/Parameter.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace ParameterNS
    {


        inline LameMu::return_type LameMu::SupplGetValue(const LocalCoords& local_coords,
                                                         const GeometricElt& geom_elt) const
        {
            const double young_modulus = GetYoungModulus().GetValue(local_coords, geom_elt);
            const double poisson_ratio = GetPoissonRatio().GetValue(local_coords, geom_elt);

            return ComputeValue(young_modulus, poisson_ratio);
        }


        inline const LameMu::scalar_parameter& LameMu::GetYoungModulus() const
        {
            return young_modulus_;
        }


        inline const LameMu::scalar_parameter& LameMu::GetPoissonRatio() const
        {
            return poisson_ratio_;
        }


        inline bool LameMu::IsConstant() const
        {
            return GetYoungModulus().IsConstant() && GetPoissonRatio().IsConstant();
        }


        inline typename LameMu::return_type LameMu::ComputeValue(const double young_modulus,
                                                                 const double poisson_ratio) const
        {
            assert(!NumericNS::AreEqual(poisson_ratio, -1.));
            return young_modulus * 0.5 / (1. + poisson_ratio);
        }


        inline typename LameMu::return_type LameMu::SupplGetConstantValue() const
        {
            assert(IsConstant());
            assert(!NumericNS::AreEqual(constant_value_, std::numeric_limits<double>::lowest()));
            return constant_value_;
        }


        inline void LameMu::SetConstantValue(double value)
        {
            static_cast<void>(value);
            assert(false && "SetConstantValue() meaningless for current Parameter.");
            exit(EXIT_FAILURE);
        }


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_LAME_COEFFICIENTS_FROM_YOUNG_AND_POISSON_x_LAME_MU_HXX_
