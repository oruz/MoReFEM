/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_HXX_
#define MOREFEM_x_PARAMETERS_x_PARAMETER_HXX_

// IWYU pragma: private, include "Parameters/Parameter.hpp"


namespace MoReFEM
{


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    template<class T>
    Parameter<TypeT, LocalCoordsT, TimeDependencyT>::Parameter(T&& name, const Domain& domain)
    : name_(name), domain_(domain)
    {
        if (!domain.template IsConstraintOn<DomainNS::Criterion::mesh>())
            throw Exception(std::string("Parameter ") + name
                                + " is inconsistently defined: domain should be "
                                  "circumscribed to a mesh.",
                            __FILE__,
                            __LINE__);

        // If there is a time dependency, it must be set up through SetTimeDependency() method.
        // If not, I need an empty object for conveniency; let's define it here.
        if constexpr (std::is_same<TimeDependencyT<TypeT>, ParameterNS::TimeDependencyNS::None<TypeT>>())
            time_dependency_ = std::make_unique<ParameterNS::TimeDependencyNS::None<TypeT>>();
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    void Parameter<TypeT, LocalCoordsT, TimeDependencyT>::SetTimeDependency(
        typename TimeDependencyT<TypeT>::unique_ptr&& time_dependency)
    {
        static_assert(!std::is_same<TimeDependencyT<TypeT>, ParameterNS::TimeDependencyNS::None<TypeT>>::value,
                      "Should not be called for Parameters without time dependency!");

        assert(time_dependency_ == nullptr && "Should be assigned only once!");

        assert(!(!time_dependency));
        time_dependency_ = std::move(time_dependency);

        time_dependency_->Init(SupplGetAnyValue());
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline typename Parameter<TypeT, LocalCoordsT, TimeDependencyT>::return_type
    Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetConstantValue() const
    {
        assert(IsConstant() && "This method is relevant only for spatially constant parameters.");

        if constexpr (std::is_same<TimeDependencyT<TypeT>, ParameterNS::TimeDependencyNS::None<TypeT>>())
            return SupplGetConstantValue();
        else
            return GetTimeDependency().ApplyTimeFactor(SupplGetConstantValue());
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline typename Parameter<TypeT, LocalCoordsT, TimeDependencyT>::return_type
    Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetValue(const LocalCoordsT& local_coords,
                                                              const GeometricElt& geom_elt) const
    {
#ifndef NDEBUG
        {
            decltype(auto) domain = GetDomain();
            assert(domain.IsGeometricEltInside(geom_elt)
                   && "Attempt to use a Parameter outside of its domain "
                      "definition.");
        }
#endif // NDEBUG

        if (IsConstant())
            return GetConstantValue(); // Potentiel time dependency already applied within GetConstantValue().

        if constexpr (std::is_same<TimeDependencyT<TypeT>, ParameterNS::TimeDependencyNS::None<TypeT>>())
            return SupplGetValue(local_coords, geom_elt);
        else
            return GetTimeDependency().ApplyTimeFactor(SupplGetValue(local_coords, geom_elt));
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline void Parameter<TypeT, LocalCoordsT, TimeDependencyT>::Write(const std::string& filename) const
    {
        std::ofstream out;
        FilesystemNS::File::Create(out, filename, __FILE__, __LINE__);
        this->Write(out);
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline void Parameter<TypeT, LocalCoordsT, TimeDependencyT>::Write(std::ostream& out) const
    {
        out << "# Name = " << GetName() << std::endl;
        this->SupplWrite(out);
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline const std::string& Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetName() const
    {
        return name_;
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline const TimeDependencyT<TypeT>&
    Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetTimeDependency() const noexcept
    {
        assert(!(!time_dependency_)
               && "Make sure Parameter::SetTimeDependency() has been correctly called after "
                  "construction.");
        return *time_dependency_;
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline TimeDependencyT<TypeT>& Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetNonCstTimeDependency() noexcept
    {
        return const_cast<TimeDependencyT<TypeT>&>(GetTimeDependency());
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline void Parameter<TypeT, LocalCoordsT, TimeDependencyT>::TimeUpdate()
    {
        GetNonCstTimeDependency().Update();
        SupplTimeUpdate();
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline void Parameter<TypeT, LocalCoordsT, TimeDependencyT>::TimeUpdate(double time)
    {
        GetNonCstTimeDependency().Update(time);
        SupplTimeUpdate(time);
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    constexpr bool Parameter<TypeT, LocalCoordsT, TimeDependencyT>::IsTimeDependent() const noexcept
    {
        return !std::is_same<TimeDependencyT<TypeT>, ParameterNS::TimeDependencyNS::None<TypeT>>();
    }


    template<ParameterNS::Type TypeT, class LocalCoordsT, template<ParameterNS::Type> class TimeDependencyT>
    inline const Domain& Parameter<TypeT, LocalCoordsT, TimeDependencyT>::GetDomain() const noexcept
    {
        return domain_;
    }


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_HXX_
