/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <limits>
#include <string>

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Parameters/TimeDependency/Policy/FromFile.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            namespace PolicyNS
            {


                FromFile::FromFile(const std::string& file)
                {
                    std::ifstream file_in;

                    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

                    FilesystemNS::File::Read(file_in, environment.SubstituteValues(file), __FILE__, __LINE__);

                    // Skip the first line
                    std::streamsize max_stream_size = std::numeric_limits<std::streamsize>::max();

                    file_in.ignore(max_stream_size, '\n');

                    auto& storage = GetNonCstStorage();

                    while (file_in)
                    {
                        std::array<double, 2ul> buf_values;

                        for (std::size_t i = 0ul; i < 2; ++i)
                        {
                            file_in >> buf_values[i];
                        }

                        storage.push_back(std::make_pair(buf_values[0], buf_values[1]));
                    }

                    if (!std::is_sorted(storage.cbegin(),
                                        storage.cend(),
                                        [](const auto& lhs, const auto& rhs)
                                        {
                                            return lhs.first < rhs.first;
                                        }))
                    {
                        throw Exception("Times in " + file + " are not sorted correctly!", __FILE__, __LINE__);
                    }

                    if (!file_in.eof())
                        throw Exception("Error while reading " + file, __FILE__, __LINE__);
                }


                namespace // anonymous
                {


                    struct Comp
                    {
                        bool operator()(const std::pair<double, double>& pair, const double time)
                        {
                            return pair.first < time;
                        }

                        bool operator()(const double time, const std::pair<double, double>& pair)
                        {
                            return time < pair.first;
                        }
                    };


                } // namespace


                double FromFile::GetCurrentTimeFactor(double time) const
                {
                    constexpr auto epsilon = 1.e-12;
                    const auto& storage = GetStorage();
                    const auto begin = storage.cbegin();
                    const auto end = storage.cend();

                    const auto it = std::find_if(begin,
                                                 end,
                                                 [time, epsilon](const auto& pair)
                                                 {
                                                     return NumericNS::AreEqual(pair.first, time, epsilon);
                                                 });

                    if (it != end)
                    {
                        return it->second;
                    } else
                    {
                        auto it_pair = std::equal_range(begin, end, time, Comp());

                        auto& it_lower = it_pair.first;
                        const auto& it_upper = it_pair.second;

                        if (it_lower == begin && it_upper != end)
                        {
                            return it_upper->second;
                        } else if (it_lower != begin && it_upper == end)
                        {
                            --it_lower;
                            return it_lower->second;
                        } else if (it_lower != begin && it_upper != end)
                        {
                            --it_lower;
                            const double t_inf = it_lower->first;
                            const double t_sup = it_upper->first;
                            const double value_inf = it_lower->second;
                            const double value_sup = it_upper->second;
                            assert(!NumericNS::IsZero(t_sup - t_inf));
                            const double alpha = (t_sup - time) / (t_sup - t_inf);

                            return alpha * value_inf + (1. - alpha) * value_sup;
                        } else
                        {
                            throw Exception("No value found for the time step for Time Dependency Policy From File. "
                                            "It should not happen so verify the time step that you gave is in the "
                                            "range of time of the simulation.",
                                            __FILE__,
                                            __LINE__);
                        }
                    }
                }


            } // namespace PolicyNS


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup
