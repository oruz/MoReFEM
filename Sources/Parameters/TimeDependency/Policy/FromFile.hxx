/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HXX_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HXX_

// IWYU pragma: private, include "Parameters/TimeDependency/Policy/FromFile.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            namespace PolicyNS
            {

                inline const FromFile::storage_type& FromFile::GetStorage() const noexcept
                {
                    return storage_;
                }


                inline FromFile::storage_type& FromFile::GetNonCstStorage() noexcept
                {
                    return const_cast<storage_type&>(GetStorage());
                }


            } // namespace PolicyNS


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HXX_
