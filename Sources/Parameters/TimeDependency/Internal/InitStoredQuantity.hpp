/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Mar 2016 15:36:13 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_INIT_STORED_QUANTITY_HPP_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_INIT_STORED_QUANTITY_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Parameters/Internal/Alias.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace TimeDependencyNS
            {


                /*!
                 * \brief Helper class to mimic static if.
                 *
                 * The point is to properly allocate the object that stores the full value of a Parameter at a \a
                 * QuadraturePoint, once time factor has been taken into account.
                 *
                 */
                template<Type TypeT>
                struct InitStoredQuantity;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                template<>
                struct InitStoredQuantity<Type::scalar>
                {

                    static void Perform(double any_value, double& stored_value);
                };


                template<>
                struct InitStoredQuantity<Type::vector>
                {

                    static void Perform(const LocalVector& any_value, LocalVector& stored_value);
                };


                template<>
                struct InitStoredQuantity<Type::matrix>
                {

                    static void Perform(const LocalMatrix& any_value, LocalMatrix& stored_value);
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace TimeDependencyNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_INIT_STORED_QUANTITY_HPP_
