/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Mar 2016 10:46:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HXX_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HXX_

// IWYU pragma: private, include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace TimeDependencyNS
            {


                inline void ApplyTimeFactor(double value_without_time_factor,
                                            double time_factor,
                                            double& value_with_time_factor) noexcept
                {
                    value_with_time_factor = value_without_time_factor * time_factor;
                }


                inline void ApplyTimeFactor(const LocalMatrix& value_without_time_factor,
                                            double time_factor,
                                            LocalMatrix& value_with_time_factor)
                {
                    xt::noalias(value_with_time_factor) = time_factor * value_without_time_factor;
                }


                inline void ApplyTimeFactor(const LocalVector& value_without_time_factor,
                                            double time_factor,
                                            LocalVector& value_with_time_factor)
                {
                    xt::noalias(value_with_time_factor) = time_factor * value_without_time_factor;
                }


            } // namespace TimeDependencyNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HXX_
