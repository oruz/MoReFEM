/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Mar 2016 10:46:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HPP_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HPP_

#include <memory>
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace TimeDependencyNS
            {


                /*!
                 * \class doxygen_hide_apply_time_factor
                 *
                 * \brief Update the \a value_with_time_factor obtained after multiplying the time factor by the
                 * purely spatial component.
                 *
                 * Or in short, compute f(x) * g(t) given both parts.
                 *
                 * \copydoc doxygen_hide_param_time_dependancy
                 *
                 * The difficulty is that f(x) might be a scalar, a vector or a matrix; the different overloads here
                 * take care of each of this case.
                 *
                 * \param[in] value_without_time_factor Value which was obtained before applying time dependency.
                 * \param[in] time_factor Time factor at the considered local position (it's the job of TimeDependency
                 * class to provide the right value; we're at a very low-level here).
                 * \param[out] value_with_time_factor Result of \a value_without_time_factor * \a time_factor.
                 */

                /*!
                 * \copydoc doxygen_hide_apply_time_factor
                 *
                 * <b>Overload for TypeT == Type::scalar.</b>
                 */
                void ApplyTimeFactor(double value_without_time_factor,
                                     double time_factor,
                                     double& value_with_time_factor) noexcept;


                /*!
                 * \copydoc doxygen_hide_apply_time_factor
                 *
                 * <b>Overload for TypeT == Type::vector.</b>
                 */
                void ApplyTimeFactor(const LocalVector& value_without_time_factor,
                                     double time_factor,
                                     LocalVector& value_with_time_factor);


                /*!
                 * \copydoc doxygen_hide_apply_time_factor
                 *
                 * <b>Overload for TypeT == Type::matrix.</b>
                 */
                void ApplyTimeFactor(const LocalMatrix& value_without_time_factor,
                                     double time_factor,
                                     LocalMatrix& value_with_time_factor);


            } // namespace TimeDependencyNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_APPLY_TIME_FACTOR_HPP_
