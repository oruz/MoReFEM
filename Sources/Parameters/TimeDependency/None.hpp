/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Mar 2016 10:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_

#include <memory>


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            /*!
             * \brief Policy to use when there are no time dependency for the \a Parameter.
             */
            template<Type TypeT>
            struct None
            {


                //! Convenient alias.
                using unique_ptr = std::unique_ptr<None>;

                //! Defined for conveniency; do nothing.
                void Update()
                { }

                //! Defined for conveniency; do nothing.
                void Update(double)
                { }
            };


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_NONE_HPP_
