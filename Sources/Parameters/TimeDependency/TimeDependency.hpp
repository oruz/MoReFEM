/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Mar 2016 10:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HPP_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Parameters/Internal/Traits.hpp"
#include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"
#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"
#include "Parameters/TimeDependency/Policy/FromFile.hpp"
#include "Parameters/TimeDependency/Policy/Functor.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            /*!
             * \brief Base class when there is a decoupled time dependency (look at TimeDependencyNS::None if there
             * aren't).
             *
             * \copydoc doxygen_hide_param_time_dependancy
             *
             * \attention This very class is what is expected as second argument of Parameter template; you need
             * however a call to Parameter::SetTimeDependency() to really set the time dependency. In practice:
             * \code
             * using time_dependency_type =
             *      TimeDependencyNS::Base<TypeT, TimeDependencyNS::PolicyNS::Functor<std::function<double(double)>>>;
             *
             * Parameter
             * <
             *      TypeT,
             *      time_dependency_type
             * > my_parameter; // init it properly, either with dedicated constructor or from input parameter data.
             *
             *
             * {
             *      auto linear = [](double time) { return time; }
             *      auto time_dep =
             *          std::make_unique<time_dependency_type>(time_manager, std::move(linear));
             *      my_parameter.SetTimeDependency(std::move(time_dep));
             * }
             *
             * ...
             * ... call my_parameter.TimeUpdate() at each time modification, typically in Model::InitializeStep() or
             * Model::FinalizeStep()...
             * \endcode
             *
             * \note Some aliases for most frequent time dependency types are defined in this very file (including
             * the type used in the example).
             *
             * \tparam TimeDependencyPolicyT A policy for time dependency. Currently 'Functor' and 'FromFile'.
             * \tparam TypeT Whether the parameter at a quadrature point yields a scalar, a vector or a matrix.
             *
             */
            template<ParameterNS::Type TypeT, class TimeDependencyPolicyT>
            class Base : public TimeDependencyPolicyT
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = Base<TypeT, TimeDependencyPolicyT>;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to traits class related to TypeT.
                using traits = Internal::ParameterNS::Traits<TypeT>;

                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::decay_t<return_type>;


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] time_manager Object that keeps track of the time within the Model.
                 * \param[in] args Variadic argument to the constructor, for the policy to expand on it if needed.
                 */
                template<typename... Args>
                explicit Base(const TimeManager& time_manager, Args&&... args);

                //! Destructor.
                ~Base() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Base(const Base& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Base(Base&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Base& operator=(const Base& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Base& operator=(Base&& rhs) = delete;

                ///@}

                /*!
                 * \brief Init the class before first use.
                 *
                 * It is here for instance that the local vectors or matrices (depending on \a TypeT) in which time
                 * factor have been correctly applied.
                 *
                 * \param[in] any_value A random value that might be returned when Parameter value is requested; the
                 * whole point here is to extract sizes if TypeT is a vector or a matrix.
                 */
                void Init(return_type any_value);


                /*!
                 * \brief Update the value of the time dependent factor.
                 */
                void Update();

                /*!
                 * \brief Update the value of the time dependent factor.
                 *
                 * \param[in] time Time for the update.
                 * One should prefer to use the default one if one wants to use the current time.
                 * Extra security to verify the synchro of the parameter to the current time is done in he default one.
                 * This method is for particular cases only when the user knows exactly what is he doing.
                 */
                void Update(double time);

                //! Constant accessor to the value of time dependent factor.
                double GetCurrentTimeFactor() const noexcept;

                //! Constant accessor to the object that keeps track of the time within the Model.
                const TimeManager& GetTimeManager() const noexcept;

                /*!
                 * \brief Return the full value of the Parameter, once the time factor has been taken into account.
                 *
                 * \param[in] value_without_time_factor Value computed at the local position when only the spatial
                 * contribution has been taken into account (f(x) in the class explanation).
                 * \return Value once the time contribution has been taken into account (f(x) * g(t) in the class
                 * explanation).
                 */
                return_type ApplyTimeFactor(return_type value_without_time_factor) const;

              private:
                /// \name Accessors
                ///@{

                //! Constant accessor to the value of \a TimeManager::NtimesModified() at the last \a Update() call.
                std::size_t GetNtimesModifiedAtLastUpdate() const noexcept;

                ///@}

                /*!
                 * \brief Update \a Ntimes_modified_at_last_update_.
                 *
                 * New value is given by GetTimeManager().NtimesModified().
                 */
                void UpdateNtimesModifiedAtLastUpdate();


                /*!
                 * \brief Non constant accessor to the mutable object used to contain the value with the time factor
                 * currently applied.
                 *
                 * This is handy for local matrices and vectors not to reallocate each time a brand
                 * new object.
                 *
                 * The method is marked const as it acts solely upon a mutable data attribute.
                 *
                 * \return Reference to the object in which full value of the parameter at a given local position is
                 * stored.
                 */
                storage_type& GetNonCstValueWithTimeFactor() const noexcept;


              private:
                /*!
                 * \brief Value of \a TimeManager::NtimesModified() at the last \a Update() call.
                 */
                std::size_t Ntimes_modified_at_last_update_ = NumericNS::UninitializedIndex<std::size_t>();

                //! Value computed for the current time.
                double current_time_factor_;

                //! Object that keeps track of the time within the Model.
                const TimeManager& time_manager_;


              private:
                /*!
                 * \brief Mutable object used to contain the value with the time factor currently applied. This is
                 * handy for local matrices and vectors not to reallocate each time a brand new object.
                 */
                mutable storage_type value_with_time_factor_;
            };


        } // namespace TimeDependencyNS


        /*!
         * \brief An instantiation of \a TimeDependencyNS::Base where the function is given by a mere std::function.
         *
         * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
         */
        template<ParameterNS::Type TypeT>
        using TimeDependencyFunctor =
            TimeDependencyNS::Base<TypeT, TimeDependencyNS::PolicyNS::Functor<std::function<double(double)>>>;

        /*!
         * \brief An instantiation of \a TimeDependencyNS::Base where the time dependency policy is FromFile.
         *
         * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
         */
        template<ParameterNS::Type TypeT>
        using TimeDependencyFromFile = TimeDependencyNS::Base<TypeT, TimeDependencyNS::PolicyNS::FromFile>;


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/TimeDependency/TimeDependency.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_TIME_DEPENDENCY_HPP_
