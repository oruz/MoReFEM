/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 17:51:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_AT_DOF_HPP_
#define MOREFEM_x_PARAMETERS_x_PARAMETER_AT_DOF_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Parameters/Internal/ParameterAtDof.hpp"
#include "Parameters/Policy/AtDof/AtDof.hpp"


namespace MoReFEM
{


    /*!
     *
     * \brief Parameter class which provides values at \a LocalCoords of quantities defined on dofs.
     *
     * \copydetails doxygen_hide_param_at_dof_and_unknown
     *
     * \copydoc doxygen_hide_parameter_at_dof_template_args
     *
     * Currently Doxygen doesn't seem able to display the interface of the underlying class, despite the
     * TYPEDEF_HIDES_STRUCT = YES in Doxygen file. You may see the full interface of the class (on complete Doxygen
     * documentation) at
     * Internal::ParameterNS::ParameterInstance except the constructor (due to variadic arguments); its prototype is:
     *
     * \code
     * ParameterAtDof(const Mesh& mesh,
     *                const FEltSpace& felt_space, // one such line for each \a NfeltSpace
     *                const Unknown& unknown,
     *                const GlobalVector& global_vector);
     * \endcode
     *
     * where:
     * \copydetails doxygen_hide_at_dof_impl_constructor_args
     * \param[in] felt_space \a FEltSpace upon which the Parameter may be evaluated.
     *
     * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
     *
     */
    template<ParameterNS::Type TypeT,
             template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
             std::size_t NfeltSpace = 1>
    using ParameterAtDof = typename Internal::ParameterNS::ParameterAtDofImpl<TypeT, TimeDependencyT, NfeltSpace>::type;


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_AT_DOF_HPP_
