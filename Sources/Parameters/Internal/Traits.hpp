/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 12:00:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HPP_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    /// \addtogroup ParametersGroup
    ///@{


    //! Convenient alias.
    using Type = ::MoReFEM::ParameterNS::Type;

    /*!
     * \brief Traits class that yields relevant C++ types to use for each \a TypeT.
     *
     * One alias is currently expected:
     * - return_type, which is the type to be returned by Parameter::GetValue(). Typically it is expected
     * to be a const reference to lvalue for non POD types (vector, matrix) or the type itself for POD types.
     */
    template<Type T>
    struct Traits;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    template<>
    struct Traits<Type::scalar> final
    {
        using return_type = double;

        using non_constant_reference = double&;

        static double AllocateDefaultValue(std::size_t, std::size_t) noexcept;
    };


    template<>
    struct Traits<Type::vector> final
    {
        using return_type = const LocalVector&;

        using non_constant_reference = LocalVector&;

        static return_type AllocateDefaultValue(std::size_t Nrow, std::size_t) noexcept;
    };


    template<>
    struct Traits<Type::matrix> final
    {
        using return_type = const LocalMatrix&;

        using non_constant_reference = LocalMatrix&;

        static return_type AllocateDefaultValue(std::size_t Nrow, std::size_t Ncol) noexcept;
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    ///@} // \addtogroup Parameters


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup


#include "Parameters/Internal/Traits.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HPP_
