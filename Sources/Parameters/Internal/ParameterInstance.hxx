/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 14:09:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_

// IWYU pragma: private, include "Parameters/Internal/ParameterInstance.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            template<class T, typename... ConstructorArgs>
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::ParameterInstance(
                T&& name,
                const Domain& domain,
                ConstructorArgs&&... arguments)
            : parent(std::forward<T>(name), domain),
              nature_policy(parent::GetName(), domain, std::forward<ConstructorArgs>(arguments)...)
            { }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetValue(
                const local_coords_type& local_coords,
                const GeometricElt& geom_elt) const
            {
                return nature_policy::GetValueFromPolicy(local_coords, geom_elt);
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetConstantValue() const
            {
                assert(nature_policy::IsConstant());
                return nature_policy::GetConstantValueFromPolicy();
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetAnyValue() const
            {
                return nature_policy::GetAnyValueFromPolicy();
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline bool ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::IsConstant() const
            {
                return nature_policy::IsConstant();
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline void
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplWrite(std::ostream& out) const
            {
                return nature_policy::WriteFromPolicy(out);
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplTimeUpdate()
            {
                // Do nothing in general case.
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplTimeUpdate(double time)
            {
                // Do nothing in general case.
                static_cast<void>(time);
            }


            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type, typename... Args>
                     class NaturePolicyT,
                     template<ParameterNS::Type>
                     class TimeDependencyT,
                     typename... Args>
            inline void
            ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SetConstantValue(double value)
            {
                nature_policy::SetConstantValue(value);
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_
