/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 11 Oct 2016 17:49:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_AT_DOF_HPP_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_AT_DOF_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Parameters/Internal/Alias.hpp"
#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtDof/AtDof.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Intermediate traits class that provide the type for a parameter defined at dof.
             *
             * \copydoc doxygen_hide_parameter_at_dof_template_args
             *
             * You shouldn't have to deal with this class directly; it's just a bridge to deal with the fact
             * ParameterNS::Policy::ParameterAtDof gets an additional template parameter (namely the number of
             * \a FEltSpace).
             */
            template<ParameterNS::Type TypeT,
                     template<ParameterNS::Type> class TimeDependencyT = ::MoReFEM::ParameterNS::TimeDependencyNS::None,
                     std::size_t NfeltSpace = 1>
            struct ParameterAtDofImpl
            {
              private:
                static_assert(TypeT != ParameterNS::Type::matrix, "Doesn't make sense for a matrix parameter.");

                /*!
                 * \brief Alias which the proper prototype for \a ParameterInstance.
                 *
                 * \a ParameterNS::Policy::AtDof does not work directly, as there are 2 template arguments whereas
                 * template template argument of \a ParameterInstance expects only one.
                 */
                template<ParameterNS::Type TypeTT>
                using intermediate_policy_type = ::MoReFEM::ParameterNS::Policy::AtDof<TypeTT, NfeltSpace>;

              public:
                //! Alias which provides the parameter type.
                using type = Internal::ParameterNS::ParameterInstance<TypeT, intermediate_policy_type, TimeDependencyT>;
            };


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_AT_DOF_HPP_
