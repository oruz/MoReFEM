/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 14:56:14 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HXX_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HXX_

// IWYU pragma: private, include "Parameters/Internal/Traits.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::ParameterNS
{


    inline double Traits<Type::scalar>::AllocateDefaultValue(std::size_t, std::size_t) noexcept
    {
        return 0.;
    }


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_TRAITS_HXX_
