/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Parameters/Internal/Traits.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    namespace // anonymous
    {


        LocalVector InitLocalVector(std::size_t Nrow, std::size_t);

        LocalMatrix InitLocalMatrix(std::size_t Nrow, std::size_t Ncol);


    } // namespace


    Traits<Type::vector>::return_type Traits<Type::vector>::AllocateDefaultValue(std::size_t Nrow, std::size_t) noexcept
    {
        static auto ret = InitLocalVector(Nrow, 0ul);
        return ret;
    }


    Traits<Type::matrix>::return_type Traits<Type::matrix>::AllocateDefaultValue(std::size_t Nrow,
                                                                                 std::size_t Ncol) noexcept
    {
        static auto ret = InitLocalMatrix(Nrow, Ncol);
        return ret;
    }


    namespace // anonymous
    {


        LocalVector InitLocalVector(std::size_t Nrow, std::size_t)
        {
            LocalVector ret;
            ret.resize({ Nrow });
            ret.fill(0.);
            return ret;
        }


        LocalMatrix InitLocalMatrix(std::size_t Nrow, std::size_t Ncol)
        {
            LocalMatrix ret({ Nrow, Ncol });
            ret.fill(0.);
            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup
