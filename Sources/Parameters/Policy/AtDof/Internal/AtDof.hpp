/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 13:59:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Miscellaneous.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/Nodes_and_dofs/Node.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class FEltSpace; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace Policy
            {


                namespace AtDofNS
                {


                    /*!
                     * \brief Init local value in the case of a scalar AtDof parameter: just put it to zero.
                     *
                     * \param[in] mesh_dimension Unused (it is there because the vector overload needs it).
                     * \param[out] value Value that is set to 0.
                     */
                    void InitLocalValue(std::size_t mesh_dimension, double& value);

                    /*!
                     * \brief Init local value in the case of a vectorial AtDof parameter
                     *
                     * \param[in] mesh_dimension Dimension of the mesh.
                     * \param[out] value Vector being initialized: it is allocated (with a size of \a mesh_dimension)
                     * and filled with zeroes.
                     */
                    void InitLocalValue(std::size_t mesh_dimension, LocalVector& value);


                    /*!
                     * \class doxygen_hide_at_dof_compute_local_value_arg
                     *
                     * \param[in] basic_ref_felt \a BasicRefElt into which computation occurs, which provides the values
                     * of shape functions.
                     * \param[in] local_coords \a LocalCoords for which value is computed.
                     * \param[in] node_list List of \a Node in the current \a FElt.
                     * \param[in] numbering_subset \a NumberingSubset of the \a GlobalVector considered in the
                     * \a ParameterAtDof policy.
                     * \param[in] ghost_content Processor-wise and ghost content of the \a GlobalVector underneath
                     * the \a ParameterAtDof.
                     * \param[out] value Result of the computation.
                     */


                    /*!
                     * \brief Compute local value in case of a scalar parameter.
                     *
                     * \copydoc doxygen_hide_at_dof_compute_local_value_arg
                     */
                    void ComputeLocalValue(
                        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                        const LocalCoords& local_coords,
                        const Node::vector_shared_ptr& node_list,
                        const NumberingSubset& numbering_subset,
                        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>&
                            ghost_content,
                        double& value);


                    /*!
                     * \brief Compute local value in case of a vectorial parameter.
                     *
                     * \copydoc doxygen_hide_at_dof_compute_local_value_arg
                     */
                    void ComputeLocalValue(
                        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                        const LocalCoords& local_coords,
                        const Node::vector_shared_ptr& node_list,
                        const NumberingSubset& numbering_subset,
                        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>&
                            ghost_content,
                        LocalVector& value);


                    /*!
                     * \brief An helper structure which purpose is to store references and provide access to \a
                     * FEltSpace.
                     *
                     * \tparam Ndim Number of dimensions stored in the class.
                     */
                    template<std::size_t Ndim>
                    struct FEltSpaceStorage;


                    /*!
                     * \brief Specialization when only one underlying \a FEltSpace.
                     *
                     * In this case, the \a FEltSpace might be any dimension, regardless of the mesh dimension.
                     */
                    template<>
                    struct FEltSpaceStorage<1>
                    {

                        /*!
                         * \brief Constructor
                         *
                         * \param[in] felt_space \a FEltSpace considered.
                         */
                        FEltSpaceStorage(const FEltSpace& felt_space);


                        /*!
                         * \brief Return the \a FEltSpace adequate for the \a dimension.
                         *
                         * \param[in] dimension Dimension for which \a FEltSpace is requested.
                         *
                         * \return \a FEltSpace.
                         */
                        const FEltSpace& GetFEltSpace(std::size_t dimension) const;

                        /*!
                         * \brief Return the \a FEltSpace.
                         *
                         * \param[in] geom_elt Unused for current class.
                         *
                         * \return \a FEltSpace.
                         */
                        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

                      private:
                        //! Underlying \a FEltSpace.
                        const FEltSpace& felt_space_;

                        //! Dimension of the mesh.
                        const std::size_t mesh_dimension_;
                    };


                    /*!
                     * \brief Specialization when there are two underlying \a FEltSpace.
                     *
                     * In this case,
                     * - First one is expected to cover the dimension of the mesh.
                     * - Second one is expected to cover the dimension of the mesh minus one.
                     */
                    template<>
                    struct FEltSpaceStorage<2>
                    {

                        /*!
                         * \brief Constructor
                         *
                         * \param[in] felt_space_dim \a FEltSpace that covers the same dimension as the mesh.
                         * \param[in] felt_space_dim_minus_1 \a FEltSpace that covers the dimension as the mesh minus 1.
                         */
                        FEltSpaceStorage(const FEltSpace& felt_space_dim, const FEltSpace& felt_space_dim_minus_1);


                        /*!
                         * \brief Return the \a FEltSpace adequate for the \a dimension.
                         *
                         * \param[in] dimension Dimension for which \a FEltSpace is requested.
                         *
                         * \return \a FEltSpace.
                         */
                        const FEltSpace& GetFEltSpace(std::size_t dimension) const;

                        /*!
                         * \brief Return the adequate \a FEltSpace for the given \a geom_elt.
                         *
                         * \param[in] geom_elt \a GeometricElt being considered.
                         *
                         * \return Relevant \a FEltSpace for \a geom_elt.
                         */
                        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

                      private:
                        //! \a FEltSpace that covers the same dimension as the mesh.
                        const FEltSpace& felt_space_dim_;

                        //! \a FEltSpace that covers the dimension as the mesh minus 1.
                        const FEltSpace& felt_space_dim_minus_1_;

                        //! Dimension of the mesh.
                        const std::size_t mesh_dimension_;
                    };


                    /*!
                     * \brief Specialization when there are three underlying \a FEltSpace.
                     *
                     * In this case,
                     * - First one is expected to cover the dimension of the mesh.
                     * - Second one is expected to cover the dimension of the mesh minus one.
                     * - Third one is expected to cover the dimension of the mesh minus two.
                     */
                    template<>
                    struct FEltSpaceStorage<3>
                    {


                        /*!
                         * \brief Constructor
                         *
                         * \param[in] felt_space_dim \a FEltSpace that covers the same dimension as the mesh.
                         * \param[in] felt_space_dim_minus_1 \a FEltSpace that covers the dimension as the mesh minus 1.
                         * \param[in] felt_space_dim_minus_2 \a FEltSpace that covers the dimension as the mesh minus 2.
                         */
                        FEltSpaceStorage(const FEltSpace& felt_space_dim,
                                         const FEltSpace& felt_space_dim_minus_1,
                                         const FEltSpace& felt_space_dim_minus_2);


                        /*!
                         * \brief Return the \a FEltSpace adequate for the \a dimension.
                         *
                         * \param[in] dimension Dimension for which \a FEltSpace is requested.
                         *
                         * \return \a FEltSpace.
                         */
                        const FEltSpace& GetFEltSpace(std::size_t dimension) const;

                        /*!
                         * \brief Return the adequate \a FEltSpace for the given \a geom_elt.
                         *
                         * \param[in] geom_elt \a GeometricElt being considered.
                         *
                         * \return Relevant \a FEltSpace for \a geom_elt.
                         */
                        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

                      private:
                        //! \a FEltSpace that covers the same dimension as the mesh.
                        const FEltSpace& felt_space_dim_;

                        //! \a FEltSpace that covers the dimension as the mesh minus 1.
                        const FEltSpace& felt_space_dim_minus_1_;

                        //! \a FEltSpace that covers the dimension as the mesh minus 2.
                        const FEltSpace& felt_space_dim_minus_2_;

                        //! Dimension of the mesh.
                        const std::size_t mesh_dimension_;
                    };


                } // namespace AtDofNS


            } // namespace Policy


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/Policy/AtDof/Internal/AtDof.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HPP_
