/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 13:59:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <type_traits>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Parameters/Policy/AtDof/Internal/AtDof.hpp"

namespace MoReFEM
{
    class NumberingSubset;
}


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace Policy
            {


                namespace AtDofNS
                {


                    void InitLocalValue(std::size_t dimension, double& value)
                    {
                        static_cast<void>(dimension);
                        value = 0.;
                    }


                    void InitLocalValue(std::size_t dimension, LocalVector& value)
                    {
                        value.resize({ dimension });
                        value.fill(0.);
                    }


                    void ComputeLocalValue(
                        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                        const LocalCoords& local_coords,
                        const Node::vector_shared_ptr& node_list,
                        const NumberingSubset& numbering_subset,
                        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>&
                            vector_with_ghost_content,
                        double& value)
                    {
                        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(
                            node_list.size()) };

                        value = 0.;

                        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode;
                             ++local_node_index)
                        {
                            assert(local_node_index.Get() < node_list.size());
                            const auto& node_ptr = node_list[local_node_index.Get()];
                            assert(!(!node_ptr));
                            const auto& node = *node_ptr;
                            assert(node.IsInNumberingSubset(numbering_subset));

                            const auto& dof_list = node.GetDofList();

                            assert(dof_list.size() == 1ul && "Scalar case!");

                            const auto& dof_ptr = dof_list.back();
                            assert(!(!dof_ptr));
                            const auto processor_wise_index = dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);

                            value += basic_ref_felt.ShapeFunction(local_node_index, local_coords)
                                     * vector_with_ghost_content.GetValue(processor_wise_index);
                        }
                    }


                    void ComputeLocalValue(
                        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                        const LocalCoords& local_coords,
                        const Node::vector_shared_ptr& node_list,
                        const NumberingSubset& numbering_subset,
                        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>&
                            vector_with_ghost_content,
                        LocalVector& value)
                    {
                        value.fill(0.);

                        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(
                            node_list.size()) };

                        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode;
                             ++local_node_index)
                        {
                            assert(local_node_index.Get() < node_list.size());
                            const auto& node_ptr = node_list[local_node_index.Get()];
                            assert(!(!node_ptr));
                            const auto& node = *node_ptr;
                            assert(node.IsInNumberingSubset(numbering_subset));

                            const auto& dof_list = node.GetDofList();

                            const auto Ndof = dof_list.size();

                            for (auto dof_index = 0ul; dof_index < Ndof; ++dof_index)
                            {
                                const auto& dof_ptr = dof_list[dof_index];
                                assert(!(!dof_ptr));
                                const auto processor_wise_index =
                                    dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);
                                assert(dof_index < value.size());

                                value(dof_index) += basic_ref_felt.ShapeFunction(local_node_index, local_coords)
                                                    * vector_with_ghost_content.GetValue(processor_wise_index);
                            }
                        }
                    }


                    FEltSpaceStorage<1>::FEltSpaceStorage(const FEltSpace& felt_space)
                    : felt_space_(felt_space), mesh_dimension_(felt_space.GetMeshDimension())
                    { }


                    FEltSpaceStorage<2>::FEltSpaceStorage(const FEltSpace& felt_space_dim,
                                                          const FEltSpace& felt_space_dim_minus_1)
                    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
                      mesh_dimension_(felt_space_dim.GetMeshDimension())
                    {
                        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
                    }


                    FEltSpaceStorage<3>::FEltSpaceStorage(const FEltSpace& felt_space_dim,
                                                          const FEltSpace& felt_space_dim_minus_1,
                                                          const FEltSpace& felt_space_dim_minus_2)
                    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
                      felt_space_dim_minus_2_(felt_space_dim_minus_2),
                      mesh_dimension_(felt_space_dim.GetMeshDimension())
                    {
                        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
                        assert(mesh_dimension_ == felt_space_dim_minus_2.GetMeshDimension());
                    }


                    const FEltSpace& FEltSpaceStorage<2>::GetFEltSpace(std::size_t geom_elt_dimension) const
                    {
                        assert(geom_elt_dimension <= mesh_dimension_);
                        const auto diff = mesh_dimension_ - geom_elt_dimension;
                        assert(diff <= 2); // 2 as the template parameter!

                        switch (diff)
                        {
                        case 0:
                            return felt_space_dim_;
                        case 1:
                            return felt_space_dim_minus_1_;
                        }

                        assert(false);
                        exit(EXIT_FAILURE);
                    }


                    const FEltSpace& FEltSpaceStorage<3>::GetFEltSpace(std::size_t geom_elt_dimension) const
                    {
                        assert(geom_elt_dimension <= mesh_dimension_);
                        const auto diff = mesh_dimension_ - geom_elt_dimension;
                        assert(diff <= 3); // 3 as the template parameter!

                        switch (diff)
                        {
                        case 0:
                            return felt_space_dim_;
                        case 1:
                            return felt_space_dim_minus_1_;
                        case 2:
                            return felt_space_dim_minus_2_;
                        }

                        assert(false);
                        exit(EXIT_FAILURE);
                    }


                } // namespace AtDofNS


            } // namespace Policy


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup
