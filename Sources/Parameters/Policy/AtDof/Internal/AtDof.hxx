/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Oct 2016 11:46:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HXX_

// IWYU pragma: private, include "Parameters/Policy/AtDof/Internal/AtDof.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/GeometricElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace Policy
            {


                namespace AtDofNS
                {


                    inline const FEltSpace& FEltSpaceStorage<1>::GetFEltSpace(std::size_t) const
                    {
                        return felt_space_;
                    }


                    inline const FEltSpace& FEltSpaceStorage<1>::GetFEltSpace(const GeometricElt&) const
                    {
                        return felt_space_;
                    }


                    inline const FEltSpace& FEltSpaceStorage<2>::GetFEltSpace(const GeometricElt& geom_elt) const
                    {
                        return GetFEltSpace(geom_elt.GetDimension());
                    }


                    inline const FEltSpace& FEltSpaceStorage<3>::GetFEltSpace(const GeometricElt& geom_elt) const
                    {
                        return GetFEltSpace(geom_elt.GetDimension());
                    }


                } // namespace AtDofNS


            } // namespace Policy


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_INTERNAL_x_AT_DOF_HXX_
