/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HPP_

#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \brief Parameter policy when the parameter gets the same value everywhere.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             */
            template<ParameterNS::Type TypeT>
            class Constant
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = Constant<TypeT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

              private:
                //! Alias to traits class related to TypeT.
                using traits = Internal::ParameterNS::Traits<TypeT>;

              public:
                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::decay_t<return_type>;


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_parameter_name_and_domain_arg
                 * \param[in] value Numerical value of the constant Parameter.
                 */
                explicit Constant(const std::string& name, const Domain& domain, storage_type value);

                //! Destructor.
                ~Constant() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Constant(const Constant& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Constant(Constant&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Constant& operator=(const Constant& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Constant& operator=(Constant&& rhs) = delete;

                ///@}

              public:
                /*!
                 * \brief Enables to modify the constant value of a parameter.
                 *
                 * \param[in] value Value for the \a Parameter.
                 */
                void SetConstantValue(double value) noexcept;

              protected:
                //! Get the value.
                return_type GetConstantValueFromPolicy() const;

                //! Provided here to make the code compile, but should never be called.
                [[noreturn]] return_type GetValueFromPolicy(const LocalCoords&, const GeometricElt&) const;

                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;

              protected:
                //! Whether the parameter varies spatially or not.
                constexpr bool IsConstant() const noexcept;


                //! Write the content of the parameter for which policy is used in a stream.
                //! \copydoc doxygen_hide_stream_inout
                void WriteFromPolicy(std::ostream& stream) const;

              private:
                //! Get the value of the parameter.
                storage_type value_;
            };


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/Policy/Constant/Constant.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HPP_
