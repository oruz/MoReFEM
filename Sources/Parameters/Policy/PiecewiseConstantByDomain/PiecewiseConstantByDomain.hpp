/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp"    // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \brief Parameter policy when the parameter is piecewise constant by domain.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             *
             */
            template<ParameterNS::Type TypeT>
            class PiecewiseConstantByDomain
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = PiecewiseConstantByDomain<TypeT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

              private:
                //! Alias to traits class related to TypeT.
                using traits = Internal::ParameterNS::Traits<TypeT>;


              public:
                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::map<std::size_t, std::decay_t<return_type>>;


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor for the paramter policy PiecewiseConstantByDomain.
                 *
                 * \copydoc doxygen_hide_parameter_name_and_domain_arg
                 * \param[in] value An associative container which key is the index of the domain and the value
                 * the scalar value the parameter takes in the matching domain.
                 *
                 * It is expected that the \a Domain defined here are actually subsets of the \a Domain upon which
                 * the \a Parameter is defined.
                 */
                explicit PiecewiseConstantByDomain(const std::string& name,
                                                   const Domain& domain,
                                                   const storage_type& value);

                //! Destructor.
                ~PiecewiseConstantByDomain() = default;

                //! \copydoc doxygen_hide_copy_constructor
                PiecewiseConstantByDomain(const PiecewiseConstantByDomain& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                PiecewiseConstantByDomain(PiecewiseConstantByDomain&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                PiecewiseConstantByDomain& operator=(const PiecewiseConstantByDomain& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                PiecewiseConstantByDomain& operator=(PiecewiseConstantByDomain&& rhs) = delete;

                ///@}

              public:
                /*!
                 * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
                 */
                void SetConstantValue(double);

              protected:
                //! Provided here to make the code compile, but should never be called.
                [[noreturn]] return_type GetConstantValueFromPolicy() const noexcept;

                //! \copydoc doxygen_hide_parameter_get_value
                return_type GetValueFromPolicy(const local_coords_type& local_coords,
                                               const GeometricElt& geom_elt) const;

                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;


              protected:
                //! Whether the parameter varies spatially or not (so always false here).
                constexpr bool IsConstant() const noexcept;

                //! Write the content of the parameter for which policy is used in a stream.
                //! \copydoc doxygen_hide_stream_inout
                void WriteFromPolicy(std::ostream& stream) const;

              private:
                //! Constant accessor to the attribut value_.
                const storage_type& GetStoredValuesPerDomain() const noexcept;

                //! Access to the map that links a geometric element to a domain (through their unique ids).
                const std::unordered_map<std::size_t, std::size_t>& GetMapGeomEltDomain() const noexcept;

                //! Non constant access to the map that links a geometric element to a domain (through their unique
                //! ids).
                std::unordered_map<std::size_t, std::size_t>& GetNonCstMapGeomEltDomain() noexcept;


              private:
                //! Value of the parameter for each domain (represented by their unique ids).
                const storage_type& stored_values_per_domain_;

                /*!
                 * \brief Map of geom_elem associated to its domain (must be unique!).
                 *
                 * Key is the unique id of the geometric element.
                 * Value is the unique id of the domain.
                 */
                std::unordered_map<std::size_t, std::size_t> mapping_geom_elt_domain_;
            };


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
