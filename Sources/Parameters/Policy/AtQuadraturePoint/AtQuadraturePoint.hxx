/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 15 Jun 2015 11:59:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_AT_QUADRATURE_POINT_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_AT_QUADRATURE_POINT_HXX_

// IWYU pragma: private, include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp" // for Sonarqube sake only - is already included in the hpp file
                                         // which includes present file...


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            namespace // anonymous
            {


                // Pragma here because clang issues a warning about unused template - even if this template is clearly
                // used!
                PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"

                template<class StorageValueTypeT, class StorageTypeT>
                void InsertValuesInStorage(const TimeManager& time_manager,
                                           Mesh::subset_range&& range,
                                           StorageValueTypeT initial_value,
                                           const std::size_t Nquadrature_point,
                                           StorageTypeT& storage);

                PRAGMA_DIAGNOSTIC(pop)


            } // namespace


            template<ParameterNS::Type TypeT>
            AtQuadraturePoint<TypeT>::AtQuadraturePoint(const std::string& name,
                                                        const Domain& domain,
                                                        const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                                        storage_value_type initial_value,
                                                        const TimeManager& time_manager)
            : parameter_name_(name), domain_(domain), initial_value_(initial_value),
              quadrature_rule_per_topology_(quadrature_rule_per_topology), time_manager_(time_manager)
            {
                auto& storage = GetNonCstStorage();

                storage.max_load_factor(Utilities::DefaultMaxLoadFactor());

                assert(domain.template IsConstraintOn<DomainNS::Criterion::mesh>()
                       && "It is an assert because this should have been checked earlier in ParameterInstance "
                          "constructor. At that place it is an exception, as a mere  user might provide an invalid "
                          "Domain.");

                const auto& mesh = domain.GetMesh();

                storage.reserve(
                    mesh.NgeometricElt<RoleOnProcessor::processor_wise>(+mesh.NgeometricElt<RoleOnProcessor::ghost>()));

                decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::both>();

                for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
                {
                    assert(!(!ref_geom_elt_ptr));
                    const auto& ref_geom_elt = *ref_geom_elt_ptr;

                    const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt.GetTopologyIdentifier());
                    const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

                    // Iterate over all geometric elements that share the ref_geom_elt.
                    InsertValuesInStorage<storage_value_type>(
                        time_manager,
                        mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt),
                        initial_value,
                        Nquadrature_point,
                        storage);

                    InsertValuesInStorage<storage_value_type>(
                        time_manager,
                        mesh.GetSubsetGeometricEltList<RoleOnProcessor::ghost>(ref_geom_elt),
                        initial_value,
                        Nquadrature_point,
                        storage);
                }
            }


            template<ParameterNS::Type TypeT>
            inline typename AtQuadraturePoint<TypeT>::return_type
            AtQuadraturePoint<TypeT>::GetValueFromPolicy(const QuadraturePoint& quad_pt,
                                                         const GeometricElt& geom_elt) const
            {
                return FindValue(quad_pt, geom_elt).value;
            }


            template<ParameterNS::Type TypeT>
            inline typename AtQuadraturePoint<TypeT>::return_type
            AtQuadraturePoint<TypeT>::GetAnyValueFromPolicy() const
            {
                decltype(auto) storage = GetStorage();
                assert(!storage.empty());

                const auto& geom_elt_content = storage.cbegin()->second; // don't use decltype(auto) here

                assert(!geom_elt_content.empty());
                return geom_elt_content.back().value;
            }


            template<ParameterNS::Type TypeT>
            const typename AtQuadraturePoint<TypeT>::value_holder_type&
            AtQuadraturePoint<TypeT>::FindValue(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt) const
            {
                auto it = storage_.find(geom_elt.GetIndex());
                assert(it != storage_.cend());

                const auto& values_in_geom_elt = it->second;

                const auto quad_pt_index = quad_pt.GetIndex();
                assert(quad_pt_index < values_in_geom_elt.size());

                return values_in_geom_elt[quad_pt_index];
            }


            template<ParameterNS::Type TypeT>
            inline typename AtQuadraturePoint<TypeT>::value_holder_type&
            AtQuadraturePoint<TypeT>::FindNonCstValue(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt)
            {
                return const_cast<typename AtQuadraturePoint<TypeT>::value_holder_type&>(FindValue(quad_pt, geom_elt));
            }


            template<ParameterNS::Type TypeT>
            template<class UpdateFunctorT>
            typename AtQuadraturePoint<TypeT>::return_type
            AtQuadraturePoint<TypeT>::UpdateAndGetValue(const QuadraturePoint& quad_pt,
                                                        const GeometricElt& geom_elt,
                                                        const UpdateFunctorT& update_functor)
            {
                auto& value_holder = FindNonCstValue(quad_pt, geom_elt);

                value_holder.last_update_index = GetTimeManager().NtimeModified();
                update_functor(value_holder.value);
                return value_holder.value;
            }

            template<ParameterNS::Type TypeT>
            template<class UpdateFunctorT>
            void AtQuadraturePoint<TypeT>::UpdateValue(const QuadraturePoint& quad_pt,
                                                       const GeometricElt& geom_elt,
                                                       const UpdateFunctorT& update_functor)
            {
                auto& value_holder = FindNonCstValue(quad_pt, geom_elt);

                value_holder.last_update_index = GetTimeManager().NtimeModified();
                update_functor(value_holder.value);
            }


            template<ParameterNS::Type TypeT>
            void AtQuadraturePoint<TypeT>::Copy(const AtQuadraturePoint& parameter_at_quad_point)
            {
                GetNonCstStorage() = parameter_at_quad_point.GetStorage();
            }


            template<ParameterNS::Type TypeT>
            [[noreturn]] typename AtQuadraturePoint<TypeT>::return_type
            AtQuadraturePoint<TypeT>::GetConstantValueFromPolicy() const
            {
                assert(false && "Should yield IsConstant() = false!");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT>
            bool AtQuadraturePoint<TypeT>::IsConstant() const
            {
                return false;
            }


            template<ParameterNS::Type TypeT>
            void AtQuadraturePoint<TypeT>::WriteFromPolicy(std::ostream& out) const
            {
                decltype(auto) domain = this->GetDomain();
                const auto& mesh = domain.GetMesh();
                const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

                const auto& storage = GetStorage();

#ifndef NDEBUG
                const auto end_storage = storage.cend();
#endif // NDEBUG
                out << "# Geometric element index; quadrature point; TimeManager::NtimesModified() at last update; "
                       "value"
                    << std::endl;

                for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
                {
                    assert(!(!ref_geom_elt_ptr));
                    const auto& ref_geom_elt = *ref_geom_elt_ptr;

                    if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                        continue;

                    const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt.GetTopologyIdentifier());

                    decltype(auto) iterator_range =
                        mesh.template GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

                    const auto quadrature_rule_name = quadrature_rule.GetName();

                    const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

                    for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
                    {
                        const auto& geom_elt_ptr = *it_geom_elt;
                        assert(!(!geom_elt_ptr));
                        const auto& geom_elt = *geom_elt_ptr;

                        if (!domain.IsGeometricEltInside(geom_elt))
                            continue;

                        const auto geom_elt_index = geom_elt.GetIndex();

                        auto it_in_storage = storage.find(geom_elt_index);
                        assert(it_in_storage != end_storage);
                        const auto& value_per_quad_pt = it_in_storage->second;

                        for (std::size_t i = 0ul; i < Nquadrature_point; ++i)
                        {
                            out << geom_elt_index << ';' << quadrature_rule_name << '_' << i << ';'
                                << value_per_quad_pt[i].last_update_index << ';';

                            if constexpr (TypeT == ParameterNS::Type::scalar)
                                out << std::setprecision(16) << value_per_quad_pt[i].value;

                            // To get rid of the braces "{ }" of the xtensor output and have more control over the
                            // precision.
                            if constexpr (TypeT == ParameterNS::Type::vector)
                            {
                                out.precision(16);
                                Utilities::PrintContainer<>::Do(value_per_quad_pt[i].value,
                                                                out,
                                                                PrintNS::Delimiter::separator(" "),
                                                                PrintNS::Delimiter::opener(""),
                                                                PrintNS::Delimiter::closer(""));
                            }

                            out << std::endl;
                        }
                    }
                }
            }


            template<ParameterNS::Type TypeT>
            inline const Domain& AtQuadraturePoint<TypeT>::GetDomain() const noexcept
            {
                return domain_;
            }


            template<ParameterNS::Type TypeT>
            inline const typename AtQuadraturePoint<TypeT>::storage_type&
            AtQuadraturePoint<TypeT>::GetStorage() const noexcept
            {
                return storage_;
            }


            template<ParameterNS::Type TypeT>
            inline typename AtQuadraturePoint<TypeT>::storage_type&
            AtQuadraturePoint<TypeT>::GetNonCstStorage() noexcept
            {
                return storage_;
            }


            template<ParameterNS::Type TypeT>
            inline const QuadratureRule&
            AtQuadraturePoint<TypeT>::GetQuadratureRule(RefGeomEltNS::TopologyNS::Type topology_id) const
            {
                return GetQuadratureRulePerTopology().GetRule(topology_id);
            }


            template<ParameterNS::Type TypeT>
            inline const TimeManager& AtQuadraturePoint<TypeT>::GetTimeManager() const noexcept
            {
                return time_manager_;
            }


            template<ParameterNS::Type TypeT>
            inline const QuadratureRulePerTopology&
            AtQuadraturePoint<TypeT>::GetQuadratureRulePerTopology() const noexcept
            {
                return quadrature_rule_per_topology_;
            }

            template<ParameterNS::Type TypeT>
            inline void AtQuadraturePoint<TypeT>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false && "SetConstantValue() meaningless for current Parameter.");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT>
            const std::string& AtQuadraturePoint<TypeT>::GetParameterName() const noexcept
            {
                return parameter_name_;
            }


            namespace // anonymous
            {


                PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"

                template<class StorageValueTypeT, class StorageTypeT>
                void InsertValuesInStorage(const TimeManager& time_manager,
                                           Mesh::subset_range&& range,
                                           StorageValueTypeT initial_value,
                                           const std::size_t Nquadrature_point,
                                           StorageTypeT& storage)
                {

                    for (auto it_geom_elt = range.first; it_geom_elt != range.second; ++it_geom_elt)
                    {
                        const auto& geom_elt_ptr = *it_geom_elt;
                        assert(!(!geom_elt_ptr));
                        const auto geom_elt_index = geom_elt_ptr->GetIndex();

                        using value_type = Internal::ParameterNS::AtQuadraturePointNS::ValueHolder<StorageValueTypeT>;

                        value_type default_value(initial_value, time_manager.NtimeModified());

                        auto check = storage.insert(
                            { geom_elt_index, std::vector<value_type>(Nquadrature_point, default_value) });
                        assert(check.second && "A geometric element should be entered only once.");
                        static_cast<void>(check);
                    }
                }

                PRAGMA_DIAGNOSTIC(pop)


            } // namespace


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_AT_QUADRATURE_POINT_HXX_
