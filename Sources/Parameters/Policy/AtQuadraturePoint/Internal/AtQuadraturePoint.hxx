/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Jun 2015 15:48:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HXX_

// IWYU pragma: private, include "Parameters/Policy/AtQuadraturePoint/Internal/AtQuadraturePoint.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace AtQuadraturePointNS
            {


                template<class StorageValueTypeT>
                ValueHolder<StorageValueTypeT>::ValueHolder(StorageValueTypeT a_value,
                                                            std::size_t time_manager_Ntime_modified)
                : value(a_value), last_update_index(time_manager_Ntime_modified)
                { }


            } // namespace AtQuadraturePointNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HXX_
