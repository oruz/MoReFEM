/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 16:38:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HXX_

// IWYU pragma: private, include "Parameters/Policy/LuaFunction/LuaFunction.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            LuaFunction<TypeT, SettingsSpatialFunctionT>::LuaFunction(const std::string&,
                                                                      const Domain&,
                                                                      storage_type lua_function)
            : lua_function_(lua_function)
            { }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            typename LuaFunction<TypeT, SettingsSpatialFunctionT>::return_type
            LuaFunction<TypeT, SettingsSpatialFunctionT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                             const GeometricElt& geom_elt) const
            {
                const auto coords_type = SettingsSpatialFunctionT::GetCoordsType();

                switch (coords_type)
                {
                case CoordsType::local:
                {
                    return lua_function_(
                        local_coords.GetValueOrZero(0), local_coords.GetValueOrZero(1), local_coords.GetValueOrZero(2));
                }
                case CoordsType::global:
                {
                    auto& coords = GetNonCstWorkCoords();
                    Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, coords);
                    return lua_function_(coords.x(), coords.y(), coords.z());
                }
                }

                assert(false
                       && "Should never happen as all possible values of the enum class are addressed in the "
                          "switch.");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            typename LuaFunction<TypeT, SettingsSpatialFunctionT>::return_type
            LuaFunction<TypeT, SettingsSpatialFunctionT>::GetAnyValueFromPolicy() const
            {
                return lua_function_(0., 0., 0.);
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            [[noreturn]] typename LuaFunction<TypeT, SettingsSpatialFunctionT>::return_type
            LuaFunction<TypeT, SettingsSpatialFunctionT>::GetConstantValueFromPolicy() const
            {
                assert(false && "A Lua function should yield IsConstant() = false!");
                exit(-1);
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            bool LuaFunction<TypeT, SettingsSpatialFunctionT>::IsConstant() const
            {
                return false;
            }

            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            void LuaFunction<TypeT, SettingsSpatialFunctionT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Given by the Lua function given in the input data file." << std::endl;
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            inline SpatialPoint& LuaFunction<TypeT, SettingsSpatialFunctionT>::GetNonCstWorkCoords() const noexcept
            {
                return work_coords_;
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            inline void LuaFunction<TypeT, SettingsSpatialFunctionT>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false);
                exit(EXIT_FAILURE);
            }


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HXX_
