/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 13:41:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_

#include "Core/SpatialLuaFunction.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Parameter.hpp"
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/Policy/LuaFunction/LuaFunction.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"

#include "Parameters/Internal/Alias.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Initialize properly a scalar Parameter from the informations given in the input file.
             *
             * \attention This function is in the Internal namespace and should not be called directly unless you know
             * exactly what you're doing! Usually the namesake function in MoReFEM namespace is what you're looking for.
             *
             * Current function in the Internal namespace has been introduced as there are currently two different cases
             * in which we want to instantiate a scalar parameter:
             * . When it is actually a scalar parameter we want to build.
             *
             * Typically, such a parameter is defined like:
             * \code
             * YoungModulus = {
             * nature = "lua_function",
             * scalar_value = 10.,
             * lua_function = function (x, y, z)
             * return x + y * 10 + z * 100
             *  end
             * }
             * \endcode
             * . When we want to build a vectorial parameter. Under the hood each component is built as a scalar
             * parameter; but nature and scalar_value are given within vectors in \a LuaOptionFile file. Lua function
             * can't be given in a vector, hence the asymmetry between some parameters given as template arguments and
             * other as plain arguments.
             *
             *
             * \param[in] name Name of the parameter (for outputs only).
             * \copydoc doxygen_hide_parameter_domain_arg
             * \param[in] nature Nature of the scalar parameter as read in the input file ('constant', 'lua_function',
             * etc..) \a LuaOptionFile is supposed to check the given values are legit. \param[in] value The
             * std::variant object read from the input data file.
             *
             * \return Parameter considered.
             */
            template<template<ParameterNS::Type> class TimeDependencyT, class StringT, class VariantT>
            typename ScalarParameter<TimeDependencyT>::unique_ptr
            InitScalarParameterFromInputData(StringT&& name,
                                             const Domain& domain,
                                             const std::string& nature,
                                             const VariantT& value);


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/InitParameterFromInputData/Internal/InitParameterFromInputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
