/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 13:41:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_

// IWYU pragma: private, include "Parameters/InitParameterFromInputData/Internal/InitParameterFromInputData.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<template<ParameterNS::Type> class TimeDependencyT, class T, class VariantT>
            typename ScalarParameter<TimeDependencyT>::unique_ptr
            InitScalarParameterFromInputData(T&& name,
                                             const Domain& domain,
                                             const std::string& nature,
                                             const VariantT& value)
            {
                namespace IPL = Utilities::InputDataNS;

                if (nature == "ignore")
                    return nullptr;

                static_assert(Utilities::IsSpecializationOf<std::variant, VariantT>());

                if (nature == "constant")
                {

                    using parameter_type =
                        Internal::ParameterNS::ParameterInstance<ParameterNS::Type::scalar,
                                                                 ::MoReFEM::ParameterNS::Policy::Constant,
                                                                 TimeDependencyT>;

                    return std::make_unique<parameter_type>(std::forward<T>(name), domain, std::get<double>(value));
                } else if (nature == "lua_function")
                {
                    using parameter_type = Internal::ParameterNS::ParameterInstance<
                        ParameterNS::Type::scalar,
                        ::MoReFEM::ParameterNS::Policy::LuaFunction3DGlobalCoords,
                        TimeDependencyT>;

                    using value_type = spatial_lua_function;

                    return std::make_unique<parameter_type>(std::forward<T>(name), domain, std::get<value_type>(value));
                } else if (nature == "piecewise_constant_by_domain")
                {
                    using parameter_type = Internal::ParameterNS::ParameterInstance<
                        ParameterNS::Type::scalar,
                        ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                        TimeDependencyT>;

                    using value_type = std::map<std::size_t, double>;

                    return std::make_unique<parameter_type>(std::forward<T>(name), domain, std::get<value_type>(value));
                } else
                {
                    assert(false
                           && "Should not happen: all the possible choices are assumed to be checked by "
                              "LuaOptionFile Constraints.");
                    exit(EXIT_FAILURE);
                }

                return nullptr;
            }


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
