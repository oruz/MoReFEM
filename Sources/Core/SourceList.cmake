### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InitTimeKeepLog.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/InitTimeKeepLog.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialLuaFunction.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LinearAlgebra/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/MoReFEMData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/NumberingSubset/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Parameter/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solver/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeManager/SourceList.cmake)
