/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Jan 2015 14:19:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <iosfwd>
#include <type_traits>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "Core/InitTimeKeepLog.hpp"


namespace MoReFEM
{


    void InitTimeKeepLog(const FilesystemNS::Directory& result_directory)
    {
        std::string time_log_filename = result_directory.AddFile("time_log.hhdata");

        std::ofstream out;
        FilesystemNS::File::Create(out, time_log_filename, __FILE__, __LINE__);

        TimeKeep::CreateOrGetInstance(__FILE__, __LINE__, std::move(out));
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
