/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HXX_
#define MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HXX_

// IWYU pragma: private, include "Core/Crtp/NumberingSubsetForMatrix.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT>
        NumberingSubsetForMatrix<DerivedT>::NumberingSubsetForMatrix(const NumberingSubset& row_numbering_subset,
                                                                     const NumberingSubset& col_numbering_subset)
        : row_numbering_subset_(row_numbering_subset), col_numbering_subset_(col_numbering_subset)
        { }


        template<class DerivedT>
        NumberingSubsetForMatrix<DerivedT>::NumberingSubsetForMatrix(const NumberingSubsetForMatrix<DerivedT>& rhs)
        : row_numbering_subset_(rhs.row_numbering_subset_), col_numbering_subset_(rhs.col_numbering_subset_)
        { }


        template<class DerivedT>
        inline const NumberingSubset& NumberingSubsetForMatrix<DerivedT>::GetRowNumberingSubset() const
        {
            return row_numbering_subset_;
        }


        template<class DerivedT>
        inline const NumberingSubset& NumberingSubsetForMatrix<DerivedT>::GetColNumberingSubset() const
        {
            return col_numbering_subset_;
        }


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_CRTP_x_NUMBERING_SUBSET_FOR_MATRIX_HXX_
