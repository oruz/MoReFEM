/*!
//
// \file
//
//
// Created by Sebastien Gilles <srpgilles@gmail.com> on the Fri, 15 Feb 2013 12:45:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HPP_

#include <map>
#include <string>
#include <type_traits>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/InputData/Base.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{

    /*!
     * \brief This class read the input parameters and then is in charge of holding the values read.
     *
     * It hold the charge of checking that mandatory parameters are correctly filled in the data file as soon
     * as possible: we want for instance to avoid a program failure at the end because a path for an output
     * file is invalid.
     *
     * \copydoc doxygen_hide_tparam_allow_invalid_lua_file
     *
     */
    template<class TupleT,
             Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT =
                 Utilities::InputDataNS::allow_invalid_lua_file::no>
    class InputData
    : public Utilities::InputDataNS::Base<InputData<TupleT, AllowInvalidLuaFileT>, TupleT, AllowInvalidLuaFileT>
    {

      public:
        //! Alias to self.
        using self = InputData<TupleT, AllowInvalidLuaFileT>;

        //! Alias to underlying tuple.
        using tuple = TupleT;

        //! Alias to parent.
        using parent =
            Utilities::InputDataNS::Base<InputData<TupleT, AllowInvalidLuaFileT>, TupleT, AllowInvalidLuaFileT>;

        //! Alias to unique smart pointers.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Special members.
        //@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Name of the input parameter list to be read. This file is expected to be formatted
         * in \a LuaOptionFile format.
         * \param[in] do_track_unused_fields Whether a field found in the file but not referenced in the tuple
         * yields an exception or not. Should be 'yes' most of the time; I have introduced the 'no' option for
         * cases in which we need only a handful of parameters shared by all models for post-processing
         * purposes (namely mesh-related ones).
         * \copydoc doxygen_hide_mpi_param
         */
        explicit InputData(const std::string& filename,
                           const Wrappers::Mpi& mpi,
                           Utilities::InputDataNS::DoTrackUnusedFields do_track_unused_fields =
                               Utilities::InputDataNS::DoTrackUnusedFields::yes);

        //! Destructor
        ~InputData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InputData(const InputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InputData(InputData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InputData& operator=(const InputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InputData& operator=(InputData&& rhs) = delete;


        //@}
    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/InputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HPP_
