/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT>
        constexpr std::size_t LightweightDomainList<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template<std::size_t IndexT>
        const std::string& LightweightDomainList<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("LightweightDomainList", IndexT);
            return ret;
        };


        template<std::size_t IndexT>
        typename LightweightDomainList<IndexT>::section_content_type&
        LightweightDomainList<IndexT>::GetNonCstSectionContent() noexcept
        {
            return section_content_;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HXX_
