/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_MESH_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_MESH_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Impl/Mesh.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputDataNS::Mesh should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct Mesh
            { };


        } // namespace BaseNS


        /*!
         * \brief Holds information related to the input parameter Mesh::inputMesh.
         *
         * \tparam IndexT Several meshes can be present in the input data file, provided they each
         * use a different value for this template parameter. For instance:
         *
         * \code
         * Mesh1 = {
         * mesh = "/Volumes/Data/sebastien/Freefem/Elasticity/Data/Medit/elasticity_Nx50_Ny20_force_label.mesh",
         * ...
         * }
         *
         * Mesh2 = {
         * mesh = "/Volumes/Data/sebastien/Freefem/Elasticity/Data/Medit/elasticity_Nx50_Ny20.mesh",
         * ...
         * }
         * \endcode
         */
        template<std::size_t IndexT>
        struct Mesh : public Crtp::Section<Mesh<IndexT>, NoEnclosingSection>, public BaseNS::Mesh
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'Mesh1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr std::size_t GetUniqueId() noexcept;


            //! Convenient alias.
            using self = Mesh<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Path of the mesh file.
             */
            struct Path : public Crtp::InputData<Path, self, std::string>, public Impl::MeshNS::PathImpl
            { };


            /*!
             * \brief Format of the mesh file.
             */
            struct Format : public Crtp::InputData<Format, self, std::string>, public Impl::MeshNS::FormatImpl
            { };


            //! Holds information related to the format of the mesh.
            struct Dimension : public Crtp::InputData<Dimension, self, std::size_t>, public Impl::MeshNS::DimensionImpl
            { };

            //! Holds information related to the format of the mesh.
            struct SpaceUnit : public Crtp::InputData<SpaceUnit, self, double>, public Impl::MeshNS::SpaceUnitImpl
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                Path,
                Format,
                Dimension,
                SpaceUnit
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct Mesh


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Geometry/Mesh.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_MESH_HPP_
