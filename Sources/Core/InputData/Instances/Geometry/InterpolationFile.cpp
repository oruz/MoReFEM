/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Dec 2015 16:45:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Geometry/InterpolationFile.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& InterpolationFile::NameInFile()
        {
            static std::string ret("interpolation_file");
            return ret;
        }


        const std::string& InterpolationFile::Description()
        {
            static std::string ret("File that gives for each vertex on the first mesh on the interface the index of "
                                   "the equivalent vertex in the second mesh.");
            return ret;
        }


        const std::string& InterpolationFile::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& InterpolationFile::DefaultValue()
        {
            return Utilities::EmptyString();
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
