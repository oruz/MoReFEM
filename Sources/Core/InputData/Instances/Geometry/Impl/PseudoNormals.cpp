/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 13 Jun 2016 17:23:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Geometry/Impl/PseudoNormals.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


            namespace PseudoNormalsNS
            {


                const std::string& MeshIndexImpl::NameInFile()
                {
                    static std::string ret("mesh_index");
                    return ret;
                }


                const std::string& MeshIndexImpl::Description()
                {
                    static std::string ret("Index of the geometric mesh upon which to compute pseudo-normals."
                                           "At most one value is expected here.");
                    return ret;
                }


                const std::string& MeshIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& MeshIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& DomainIndexListImpl::NameInFile()
                {
                    static std::string ret("domain_index_list");
                    return ret;
                }

                const std::string& DomainIndexListImpl::Description()
                {
                    static std::string ret("Index of the domain of the mesh upon which to compute pseudo-normals."
                                           "Might be left empty if the computation is not limited to one domain.");
                    return ret;
                }


                const std::string& DomainIndexListImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& DomainIndexListImpl::DefaultValue()
                {
                    static std::string ret("{}");
                    return ret;
                }


            } // namespace PseudoNormalsNS


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
