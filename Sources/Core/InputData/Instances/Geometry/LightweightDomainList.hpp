/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 15:14:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Impl/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Impl/LightweightDomainList.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputDataNS::LightweightDomainList should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            class LightweightDomainList
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        class LightweightDomainList : public Crtp::Section<LightweightDomainList<IndexT>, NoEnclosingSection>,
                                      public BaseNS::LightweightDomainList
        {
          public:
            //! Return the unique id (i.e. 'IndexT').
            static constexpr std::size_t GetUniqueId() noexcept;

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'Domain1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = LightweightDomainList<IndexT>;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! Indicates the mesh upon which the domain is defined (if any).
            struct MeshIndex : public Crtp::InputData<MeshIndex, self, std::size_t>,
                               public Impl::LightweightDomainListNS::MeshIndex
            { };


            //! Indicates the list of mesh labels considered in the variable domains. Separators then will split
            //! them into different domains.
            struct MeshLabelList : public Crtp::InputData<MeshLabelList, self, std::vector<std::size_t>>,
                                   public Impl::DomainNS::MeshLabelList
            { };


            struct DomainIndexList : public Crtp::InputData<DomainIndexList, self, std::vector<std::size_t>>,
                                     public Impl::LightweightDomainListNS::DomainIndexList
            { };


            struct NumberInDomainList : public Crtp::InputData<NumberInDomainList, self, std::vector<std::size_t>>,
                                        public Impl::LightweightDomainListNS::NumberInDomainList
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                MeshIndex,
                MeshLabelList,
                DomainIndexList,
                NumberInDomainList
            >;
            // clang-format on

            //! Non constant accessor to the section content.
            section_content_type& GetNonCstSectionContent() noexcept;

          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // class LightweightDomainList


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Geometry/LightweightDomainList.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
