/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_

#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace ReactionNS
        {


            //! \copydoc doxygen_hide_core_input_data_section
            struct MitchellSchaeffer : public Crtp::Section<MitchellSchaeffer, NoEnclosingSection>
            {


                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                //! Convenient alias.
                using self = MitchellSchaeffer;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                //! Coefficient Tau_in in MitchellSchaeffer.
                struct TauIn : public Crtp::InputData<TauIn, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient Tau_out in MitchellSchaeffer.
                struct TauOut : public Crtp::InputData<TauOut, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient Tau_open in MitchellSchaeffer.
                struct TauOpen : public Crtp::InputData<TauOpen, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! \copydoc doxygen_hide_core_input_data_section
                struct TauClose : public Crtp::Section<TauClose, MitchellSchaeffer>
                {


                    //! Convenient alias.
                    using self = TauClose;

                    //! Friendship to section parent.
                    using parent = Crtp::Section<self, MitchellSchaeffer>;

                    //! \cond IGNORE_BLOCK_IN_DOXYGEN
                    friend parent;
                    //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                    //! Return the name of the section in the input parameter.
                    static const std::string& GetName();


                    /*!
                     * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                     */
                    struct Nature
                    : public Crtp::
                          InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                      public Advanced::InputDataNS::ParamNS::Nature<>
                    { };

                    //! Convenient alias to define \a Value.
                    using param_value_type =
                        Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                    //! \copydoc doxygen_hide_param_value_struct
                    struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                                   public param_value_type
                    { };


                    //! Alias to the tuple of structs.
                    // clang-format off
                    using section_content_type = std::tuple
                    <
                        Nature,
                        Value
                    >;
                    // clang-format on


                  private:
                    //! Content of the section. // \todo #635 Should be private with accessor.
                    section_content_type section_content_;

                }; // struct TauClose


                //! Coefficient U_gate in MitchellSchaeffer.
                struct PotentialGate : public Crtp::InputData<PotentialGate, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient U_min in MitchellSchaeffer.
                struct PotentialMin : public Crtp::InputData<PotentialMin, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient U_max in MitchellSchaeffer.
                struct PotentialMax : public Crtp::InputData<PotentialMax, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Reaction Coefficient in FitzHughNagumo.
                struct ReactionCoefficient : public Crtp::InputData<ReactionCoefficient, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    TauIn,
                    TauOut,
                    TauOpen,
                    TauClose,
                    PotentialGate,
                    PotentialMin,
                    PotentialMax
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct MitchellSchaeffer


        } // namespace ReactionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
