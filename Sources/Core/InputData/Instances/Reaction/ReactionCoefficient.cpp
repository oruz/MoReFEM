/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace ReactionNS
        {


            const std::string& ReactionCoefficient::NameInFile()
            {
                static std::string ret("ReactionCoefficient");
                return ret;
            }


            const std::string& ReactionCoefficient::Description()
            {
                static std::string ret("Value of R.");
                return ret;
            }

            const std::string& ReactionCoefficient::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& ReactionCoefficient::DefaultValue()
            {
                static std::string ret("1.");
                return ret;
            }


        } // namespace ReactionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
