/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_

#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace ReactionNS
        {


            //! \copydoc doxygen_hide_core_input_data_section
            struct FitzHughNagumo : public Crtp::Section<FitzHughNagumo, NoEnclosingSection>
            {


                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                //! Convenient alias.
                using self = FitzHughNagumo;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                //! Coefficient a in FitzHughNagumo.
                struct ACoefficient : public Crtp::InputData<ACoefficient, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient b in FitzHughNagumo.
                struct BCoefficient : public Crtp::InputData<BCoefficient, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient c in FitzHughNagumo.
                struct CCoefficient : public Crtp::InputData<CCoefficient, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    ACoefficient,
                    BCoefficient,
                    CCoefficient
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct FitzHughNagumo


        } // namespace ReactionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_
