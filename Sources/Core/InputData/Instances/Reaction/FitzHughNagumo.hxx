/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 1 Jul 2015 11:51:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Reaction
        {


        } // namespace Reaction


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HXX_
