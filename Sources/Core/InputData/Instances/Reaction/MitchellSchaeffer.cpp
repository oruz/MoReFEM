/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 1 Jul 2015 11:51:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace ReactionNS
        {


            const std::string& MitchellSchaeffer::GetName()
            {
                static std::string ret("ReactionMitchellSchaeffer");
                return ret;
            };


            const std::string& MitchellSchaeffer::TauIn::NameInFile()
            {
                static std::string ret("tau_in");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauIn::Description()
            {
                static std::string ret("Value of Tau_in in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::TauIn::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::TauIn::DefaultValue()
            {
                static std::string ret("0.2");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauOut::NameInFile()
            {
                static std::string ret("tau_out");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauOut::Description()
            {
                static std::string ret("Value of Tau_out in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::TauOut::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::TauOut::DefaultValue()
            {
                static std::string ret("6");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauOpen::NameInFile()
            {
                static std::string ret("tau_open");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauOpen::Description()
            {
                static std::string ret("Value of Tau_open in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::TauOpen::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::TauOpen::DefaultValue()
            {
                static std::string ret("120");
                return ret;
            }


            const std::string& MitchellSchaeffer::TauClose::GetName()
            {
                static std::string ret("tau_close");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialGate::NameInFile()
            {
                static std::string ret("u_gate");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialGate::Description()
            {
                static std::string ret("Value of U_gate in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::PotentialGate::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::PotentialGate::DefaultValue()
            {
                static std::string ret("0.13");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialMin::NameInFile()
            {
                static std::string ret("u_min");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialMin::Description()
            {
                static std::string ret("Value of U_min in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::PotentialMin::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::PotentialMin::DefaultValue()
            {
                static std::string ret("0.");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialMax::NameInFile()
            {
                static std::string ret("u_max");
                return ret;
            }


            const std::string& MitchellSchaeffer::PotentialMax::Description()
            {
                static std::string ret("Value of U_max in MitchellSchaeffer.");
                return ret;
            }

            const std::string& MitchellSchaeffer::PotentialMax::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& MitchellSchaeffer::PotentialMax::DefaultValue()
            {
                static std::string ret("1.");
                return ret;
            }


        } // namespace ReactionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
