/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& TimeManager::GetName()
        {
            static std::string ret("transient");
            return ret;
        }


        const std::string& TimeManager::TimeStep::NameInFile()
        {
            static std::string ret("timeStep");
            return ret;
        }


        const std::string& TimeManager::TimeStep::Description()
        {
            static std::string ret("Time step between two iterations, in seconds.");
            return ret;
        }

        const std::string& TimeManager::TimeStep::Constraint()
        {
            static std::string ret("v > 0.");
            return ret;
        }


        const std::string& TimeManager::TimeStep::DefaultValue()
        {
            static std::string ret("0.1");
            return ret;
        }

        const std::string& TimeManager::MinimumTimeStep::NameInFile()
        {
            static std::string ret("minimum_time_step");
            return ret;
        }


        const std::string& TimeManager::MinimumTimeStep::Description()
        {
            static std::string ret("Minimum time step between two iterations, in seconds.");
            return ret;
        }

        const std::string& TimeManager::MinimumTimeStep::Constraint()
        {
            static std::string ret("v > 0.");
            return ret;
        }


        const std::string& TimeManager::MinimumTimeStep::DefaultValue()
        {
            static std::string ret("1.e-6");
            return ret;
        }

        const std::string& TimeManager::TimeInit::NameInFile()
        {
            static std::string ret("init_time");
            return ret;
        }


        const std::string& TimeManager::TimeInit::Description()
        {
            static std::string ret("Time at the beginning of the code (in seconds).");
            return ret;
        }

        const std::string& TimeManager::TimeInit::Constraint()
        {
            static std::string ret("v >= 0.");
            return ret;
        }


        const std::string& TimeManager::TimeInit::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }


        const std::string& TimeManager::TimeMax::NameInFile()
        {
            static std::string ret("timeMax");
            return ret;
        }


        const std::string& TimeManager::TimeMax::Description()
        {
            static std::string ret("Maximum time, if set to zero run a static case.");
            return ret;
        }


        const std::string& TimeManager::TimeMax::Constraint()
        {
            static std::string ret("v >= 0.");
            return ret;
        }


        const std::string& TimeManager::TimeMax::DefaultValue()
        {
            return Utilities::EmptyString();
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
