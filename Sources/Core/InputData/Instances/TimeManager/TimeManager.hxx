/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Aug 2015 13:56:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_TIME_MANAGER_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
