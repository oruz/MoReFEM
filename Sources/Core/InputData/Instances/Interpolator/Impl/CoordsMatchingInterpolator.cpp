/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 16:19:57 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Interpolator/Impl/CoordsMatchingInterpolator.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


            namespace CoordsMatchingInterpolatorNS
            {


                const std::string& SourceFEltSpaceIndexImpl::NameInFile()
                {
                    static std::string ret("source_finite_element_space");
                    return ret;
                }


                const std::string& SourceFEltSpaceIndexImpl::Description()
                {
                    static std::string ret(
                        "Source finite element space for which the dofs index will be associated to each Coords.");
                    return ret;
                }


                const std::string& SourceFEltSpaceIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& SourceFEltSpaceIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& SourceNumberingSubsetIndexImpl::NameInFile()
                {
                    static std::string ret("source_numbering_subset");
                    return ret;
                }


                const std::string& SourceNumberingSubsetIndexImpl::Description()
                {
                    static std::string ret(
                        "Source numbering subset for which the dofs index will be associated to each Coords.");
                    return ret;
                }


                const std::string& SourceNumberingSubsetIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& SourceNumberingSubsetIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& TargetFEltSpaceIndexImpl::NameInFile()
                {
                    static std::string ret("target_finite_element_space");
                    return ret;
                }


                const std::string& TargetFEltSpaceIndexImpl::Description()
                {
                    static std::string ret(
                        "Target finite element space for which the dofs index will be associated to each Coords.");
                    return ret;
                }


                const std::string& TargetFEltSpaceIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& TargetFEltSpaceIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& TargetNumberingSubsetIndexImpl::NameInFile()
                {
                    static std::string ret("target_numbering_subset");
                    return ret;
                }


                const std::string& TargetNumberingSubsetIndexImpl::Description()
                {
                    static std::string ret(
                        "Target numbering subset for which the dofs index will be associated to each Coords.");
                    return ret;
                }


                const std::string& TargetNumberingSubsetIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& TargetNumberingSubsetIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


            } // namespace CoordsMatchingInterpolatorNS


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
