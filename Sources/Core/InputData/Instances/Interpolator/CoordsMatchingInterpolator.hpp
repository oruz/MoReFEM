/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_

#include <memory>
#include <vector>


#include <string>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Interpolator/Impl/CoordsMatchingInterpolator.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {

            /*!
             * \brief Common base class from which all InputDataNS::CoordsMatchingInterpolator should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct CoordsMatchingInterpolator
            { };


        } // namespace BaseNS


        /*!
         * \brief Holds information related to the input parameter CoordsMatchingInterpolator.
         *
         */
        template<unsigned int IndexT>
        struct CoordsMatchingInterpolator
        : public Crtp::Section<CoordsMatchingInterpolator<IndexT>, NoEnclosingSection>,
          public BaseNS::CoordsMatchingInterpolator
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'CoordsMatchingInterpolator1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = CoordsMatchingInterpolator<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Source finite element space.
             */
            struct SourceFEltSpaceIndex : public Crtp::InputData<SourceFEltSpaceIndex, self, unsigned int>,
                                          public Impl::CoordsMatchingInterpolatorNS::SourceFEltSpaceIndexImpl
            { };


            /*!
             * \brief \a NumberingSubset for the source.
             */
            struct SourceNumberingSubsetIndex
            : public Crtp::InputData<SourceNumberingSubsetIndex, self, unsigned int>,
              public Impl::CoordsMatchingInterpolatorNS::SourceNumberingSubsetIndexImpl
            { };


            /*!
             * \brief Target finite element space.
             */
            struct TargetFEltSpaceIndex : public Crtp::InputData<TargetFEltSpaceIndex, self, unsigned int>,
                                          public Impl::CoordsMatchingInterpolatorNS::TargetFEltSpaceIndexImpl
            { };


            /*!
             * \brief \a NumberingSubset for the target.
             */
            struct TargetNumberingSubsetIndex
            : public Crtp::InputData<TargetNumberingSubsetIndex, self, unsigned int>,
              public Impl::CoordsMatchingInterpolatorNS::TargetNumberingSubsetIndexImpl
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                SourceFEltSpaceIndex,
                SourceNumberingSubsetIndex,
                TargetFEltSpaceIndex,
                TargetNumberingSubsetIndex
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct Mesh


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Interpolator/CoordsMatchingInterpolator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_
