/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 14:08:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Crtp/Section.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HXX_
