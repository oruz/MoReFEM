/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 14:08:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Utilities/InputData/Crtp/Data.hpp"    // IWYU pragma: export
#include "Utilities/InputData/Crtp/Section.hpp" // IWYU pragma: export
#include "Utilities/InputData/Enum.hpp"         // IWYU pragma: export


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! Convenient alias when a parameter or a section is not enclosed in a section.
        using NoEnclosingSection = Utilities::InputDataNS::NoEnclosingSection;


        namespace Crtp
        {


            /*!
             * \brief Convenient alias for a Section.
             *
             * \tparam DerivedT Placeholder for the class upon which Crtp is applied.
             * \tparam EnclosingSectionT Type of the section that enclose current section. Might be NoEnclosingSection.
             */
            template<class DerivedT, class EnclosingSectionT>
            using Section = Utilities::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;


            /*!
             * \brief Convenient alias for an input parameter..
             *
             * \tparam DerivedT Placeholder for the class upon which Crtp is applied.
             * \tparam EnclosingSectionT Type of the section that enclose current section. Might be NoEnclosingSection.
             * \tparam ReturnTypeT Return type of the \a InputData.
             */
            template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
            using InputData = Utilities::InputDataNS::Crtp::InputData<DerivedT, EnclosingSectionT, ReturnTypeT>;


        } // namespace Crtp


        namespace Impl
        {


            /*!
             * \brief Generates the name of the templatized section.
             *
             * \param[in] name Name of the type of section looked at. For instance 'Mesh', 'Domain'.
             * \param[in] index Index related to the section. For instance index = 1 will yield 'Foo1' if \a name is
             * 'Foo'.
             *
             * \return Generated name.
             */
            std::string GenerateSectionName(std::string&& name, std::size_t index);


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Crtp/Section.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_CRTP_x_SECTION_HPP_
