/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 14:08:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>
#include <type_traits>

#include "Core/InputData/Instances/Crtp/Section.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


            std::string GenerateSectionName(std::string&& name, std::size_t index)
            {
                auto ret = std::move(name) + std::to_string(index);
                return ret;
            }


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
