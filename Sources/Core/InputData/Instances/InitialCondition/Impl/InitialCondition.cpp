/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/InitialCondition/Impl/InitialCondition.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace InitialConditionNS
        {


            const std::string& Nature::NameInFile()
            {
                static std::string ret("nature");
                return ret;
            }


            const std::string& Nature::Description()
            {
                static std::string ret(
                    "How is given the initial condition value (as a constant or as a Lua function.)");
                return ret;
            }

            const std::string& Nature::Constraint()
            {
                static std::string ret("value_in(v, {'ignore', 'constant', 'lua_function'})");
                return ret;
            }


            const std::string& Nature::DefaultValue()
            {
                return Utilities::EmptyString();
            }


            const std::string& Scalar::NameInFile()
            {
                static std::string ret("scalar_value");
                return ret;
            }


            const std::string& Scalar::Description()
            {
                static std::string ret("Value of the initial condition in the case nature is 'constant.");
                return ret;
            }

            const std::string& Scalar::Constraint()
            {
                return Utilities::EmptyString();
            }


            const std::string& Scalar::DefaultValue()
            {
                return Utilities::EmptyString();
            }


            namespace Impl
            {


                const std::string& LuaFunction::Description()
                {
                    static std::string ret =
                        std::string("Value of the initial condition in the case nature is 'lua_function'.")
                        + DescriptionCoordsType();
                    return ret;
                }


                const std::string& LuaFunction::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& LuaFunction::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


            } // namespace Impl


            const std::string& LuaFunctionX::NameInFile()
            {
                static std::string ret("lua_function_x");
                return ret;
            }


            const std::string& LuaFunctionY::NameInFile()
            {
                static std::string ret("lua_function_y");
                return ret;
            }


            const std::string& LuaFunctionZ::NameInFile()
            {
                static std::string ret("lua_function_z");
                return ret;
            }


        } // namespace InitialConditionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
