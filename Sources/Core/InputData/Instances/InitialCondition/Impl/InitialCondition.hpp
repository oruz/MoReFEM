/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_HPP_

#include <iosfwd>
#include <vector>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/SpatialLuaFunction.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace InitialConditionNS
        {


            /*!
             * \brief Choose how is described the transient source (through a scalar, a function, etc...)
             *
             * Might be 'ignore' if the force is not to be considered.
             */
            struct Nature
            {


                //! Convenient alias.
                using storage_type = std::vector<std::string>;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text
                 * from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value
                 * has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar
            {


                //! Convenient alias.
                using storage_type = std::vector<double>;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text
                 * from \a LuaOptionFile example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                 * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value
                 * has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * LuaOptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            namespace Impl
            {


                /*!
                 * \brief Function that determines the transient source value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public SpatialFunction<CoordsType::global, spatial_lua_function>
                {


                    //! Convenient alias.
                    using storage_type = spatial_lua_function;


                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


            } // namespace Impl


            //! Parameter which describe first component of a vectorial \a Parameter  through a Lua function.
            struct LuaFunctionX : public Impl::LuaFunction
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();
            };


            //! Parameter which describe second component of a vectorial \a Parameter  through a Lua function.
            struct LuaFunctionY : public Impl::LuaFunction
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();
            };


            //! Parameter which describe third component of a vectorial \a Parameter through a Lua function.
            struct LuaFunctionZ : public Impl::LuaFunction
            {


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();
            };

        } // namespace InitialConditionNS


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_IMPL_x_INITIAL_CONDITION_HPP_
