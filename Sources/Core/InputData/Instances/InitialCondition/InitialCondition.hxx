/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 12 Feb 2016 14:27:57 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT>
        const std::string& InitialCondition<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("InitialCondition", IndexT);
            return ret;
        };


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HXX_
