/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& AnalyticalPrestress::GetName()
        {
            static std::string ret("AnalyticalPrestress");
            return ret;
        }


        const std::string& AnalyticalPrestress::Contractility::GetName()
        {
            static std::string ret("Contractility");
            return ret;
        }


        const std::string& AnalyticalPrestress::InitialCondition::GetName()
        {
            static std::string ret("InitialCondition");
            return ret;
        }


        const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::NameInFile()
        {
            static std::string ret("ActiveStress");
            return ret;
        }


        const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::Description()
        {
            static std::string ret("Active Stress initial condition.");
            return ret;
        }

        const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
