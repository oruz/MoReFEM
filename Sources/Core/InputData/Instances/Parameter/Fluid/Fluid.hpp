/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_

#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"
#include "Core/InputData/Instances/Parameter/MaterialProperty/VolumicMass.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! \copydoc doxygen_hide_core_input_data_section
        struct Fluid : public Crtp::Section<Fluid, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = Fluid;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Convenient alias.
            using VolumicMass = MaterialProperty::VolumicMass<self>;

            //! \copydoc doxygen_hide_core_input_data_section_with_index
            struct Viscosity : public Crtp::Section<Viscosity, Fluid>
            {


                //! Convenient alias.
                using self = Viscosity;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Fluid>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };

                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct Viscosity


            //! \copydoc doxygen_hide_core_input_data_section
            struct Density : public Crtp::Section<Density, Fluid>
            {


                //! Convenient alias.
                using self = Density;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Fluid>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };

                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section. // \todo  Should be private with accessor.
                section_content_type section_content_;


            }; // struct Density


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                VolumicMass,
                Density,
                Viscosity
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct Fluid


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_
