/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 17:05:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_

#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! \copydoc doxygen_hide_core_input_data_section
        struct Microsphere : public Crtp::Section<Microsphere, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter ('ActiveStress' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = Microsphere;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! \copydoc doxygen_hide_core_input_data_section
            struct InPlaneFiberDispersionI4 : public Crtp::Section<InPlaneFiberDispersionI4, Microsphere>
            {


                //! Convenient alias.
                using self = InPlaneFiberDispersionI4;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };

                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct InPlaneFiberDispersionI4


            //! \copydoc doxygen_hide_core_input_data_section
            struct OutOfPlaneFiberDispersionI4 : public Crtp::Section<OutOfPlaneFiberDispersionI4, Microsphere>
            {


                //! Convenient alias.
                using self = OutOfPlaneFiberDispersionI4;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct OutOfPlaneFiberDispersionI4


            //! \copydoc doxygen_hide_core_input_data_section
            struct FiberStiffnessDensityI4 : public Crtp::Section<FiberStiffnessDensityI4, Microsphere>
            {


                //! Convenient alias.
                using self = FiberStiffnessDensityI4;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple<Nature, Value>;


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct FiberStiffnessDensityI4


            //! \copydoc doxygen_hide_core_input_data_section
            struct InPlaneFiberDispersionI6 : public Crtp::Section<InPlaneFiberDispersionI6, Microsphere>
            {


                //! Convenient alias.
                using self = InPlaneFiberDispersionI6;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct InPlaneFiberDispersionI6


            //! \copydoc doxygen_hide_core_input_data_section
            struct OutOfPlaneFiberDispersionI6 : public Crtp::Section<OutOfPlaneFiberDispersionI6, Microsphere>
            {


                //! Convenient alias.
                using self = OutOfPlaneFiberDispersionI6;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct OutOfPlaneFiberDispersionI6


            //! \copydoc doxygen_hide_core_input_data_section
            struct FiberStiffnessDensityI6 : public Crtp::Section<FiberStiffnessDensityI6, Microsphere>
            {


                //! Convenient alias.
                using self = FiberStiffnessDensityI6;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, Microsphere>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                /*!
                 * \brief Choose how is described the parameter (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple<Nature, Value>;


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct FiberStiffnessDensityI6


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
               InPlaneFiberDispersionI4,
               OutOfPlaneFiberDispersionI4,
               FiberStiffnessDensityI4,
               InPlaneFiberDispersionI6,
               OutOfPlaneFiberDispersionI6,
               FiberStiffnessDensityI6
            >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;

        }; // struct Microsphere


    } // namespace InputDataNS

} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hxx" // IWYU pragma: export

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_
