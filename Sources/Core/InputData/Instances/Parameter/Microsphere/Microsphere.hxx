/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 17:05:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HXX_
