/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 17:05:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& Microsphere::GetName()
        {
            static std::string ret("Microsphere");
            return ret;
        }


        const std::string& Microsphere::InPlaneFiberDispersionI4::GetName()
        {
            static std::string ret("InPlaneFiberDispersionI4");
            return ret;
        }


        const std::string& Microsphere::OutOfPlaneFiberDispersionI4::GetName()
        {
            static std::string ret("OutOfPlaneFiberDispersionI4");
            return ret;
        }


        const std::string& Microsphere::FiberStiffnessDensityI4::GetName()
        {
            static std::string ret("FiberStiffnessDensityI4");
            return ret;
        }


        const std::string& Microsphere::InPlaneFiberDispersionI6::GetName()
        {
            static std::string ret("InPlaneFiberDispersionI6");
            return ret;
        }


        const std::string& Microsphere::OutOfPlaneFiberDispersionI6::GetName()
        {
            static std::string ret("OutOfPlaneFiberDispersionI6");
            return ret;
        }


        const std::string& Microsphere::FiberStiffnessDensityI6::GetName()
        {
            static std::string ret("FiberStiffnessDensityI6");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
