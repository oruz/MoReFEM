/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/Heart/Heart.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& Heart::GetName()
        {
            static std::string ret("Heart");
            return ret;
        }


        const std::string& Heart::Time::GetName()
        {
            static std::string ret("Time");
            return ret;
        }


        const std::string& Heart::Time::HeartBeatDuration::NameInFile()
        {
            static std::string ret("HeartBeatDuration");
            return ret;
        }


        const std::string& Heart::Time::HeartBeatDuration::Description()
        {
            static std::string ret("Heart beat duration.");
            return ret;
        }

        const std::string& Heart::Time::HeartBeatDuration::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::Time::HeartBeatDuration::DefaultValue()
        {
            static std::string ret("0.8");
            return ret;
        }


        const std::string& Heart::Geometry::GetName()
        {
            static std::string ret("Geometry");
            return ret;
        }


        const std::string& Heart::Geometry::Velocity::NameInFile()
        {
            static std::string ret("Velocity");
            return ret;
        }


        const std::string& Heart::Geometry::Velocity::Description()
        {
            static std::string ret("Velocity of electrical activation.");
            return ret;
        }

        const std::string& Heart::Geometry::Velocity::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::Geometry::Velocity::DefaultValue()
        {
            static std::string ret("0.5");
            return ret;
        }

        const std::string& Heart::Geometry::VelocitySlow::NameInFile()
        {
            static std::string ret("VelocitySlow");
            return ret;
        }


        const std::string& Heart::Geometry::VelocitySlow::Description()
        {
            static std::string ret("Slow velocity of electrical activation.");
            return ret;
        }

        const std::string& Heart::Geometry::VelocitySlow::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::Geometry::VelocitySlow::DefaultValue()
        {
            static std::string ret("60.");
            return ret;
        }


        const std::string& Heart::LongAxis::GetName()
        {
            static std::string ret("LongAxis");
            return ret;
        }

        const std::string& Heart::LongAxis::X::NameInFile()
        {
            static std::string ret("x");
            return ret;
        }


        const std::string& Heart::LongAxis::X::Description()
        {
            static std::string ret("x coordinate of the long axis.");
            return ret;
        }

        const std::string& Heart::LongAxis::X::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::LongAxis::X::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }

        const std::string& Heart::LongAxis::Y::NameInFile()
        {
            static std::string ret("y");
            return ret;
        }


        const std::string& Heart::LongAxis::Y::Description()
        {
            static std::string ret("y coordinate of the long axis.");
            return ret;
        }

        const std::string& Heart::LongAxis::Y::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::LongAxis::Y::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }

        const std::string& Heart::LongAxis::Z::NameInFile()
        {
            static std::string ret("z");
            return ret;
        }


        const std::string& Heart::LongAxis::Z::Description()
        {
            static std::string ret("z coordinate of the long axis.");
            return ret;
        }

        const std::string& Heart::LongAxis::Z::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Heart::LongAxis::Z::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
