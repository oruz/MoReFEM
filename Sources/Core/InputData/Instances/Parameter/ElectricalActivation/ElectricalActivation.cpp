/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/ElectricalActivation/ElectricalActivation.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& ElectricalActivation::GetName()
        {
            static std::string ret("ElectricalActivation");
            return ret;
        }


        const std::string& ElectricalActivation::Delay::NameInFile()
        {
            static std::string ret("Delay");
            return ret;
        }


        const std::string& ElectricalActivation::Delay::Description()
        {
            static std::string ret("Delay of the ElectricalActivation. Irrelevant if analytic.");
            return ret;
        }

        const std::string& ElectricalActivation::Delay::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::Delay::DefaultValue()
        {
            static std::string ret("-0.02.");
            return ret;
        }


        const std::string& ElectricalActivation::AmplitudeMax::NameInFile()
        {
            static std::string ret("AmplitudeMax");
            return ret;
        }


        const std::string& ElectricalActivation::AmplitudeMax::Description()
        {
            static std::string ret("Amplitude Max of the ElectricalActivation.");
            return ret;
        }

        const std::string& ElectricalActivation::AmplitudeMax::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::AmplitudeMax::DefaultValue()
        {
            static std::string ret("15.");
            return ret;
        }


        const std::string& ElectricalActivation::AmplitudeMin::NameInFile()
        {
            static std::string ret("AmplitudeMin");
            return ret;
        }


        const std::string& ElectricalActivation::AmplitudeMin::Description()
        {
            static std::string ret("Amplitude Min of the ElectricalActivation.");
            return ret;
        }

        const std::string& ElectricalActivation::AmplitudeMin::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::AmplitudeMin::DefaultValue()
        {
            static std::string ret("-5.");
            return ret;
        }


        const std::string& ElectricalActivation::DepolarizationDuration::NameInFile()
        {
            static std::string ret("DepolarizationDuration");
            return ret;
        }


        const std::string& ElectricalActivation::DepolarizationDuration::Description()
        {
            static std::string ret("Depolarization Duration of the ElectricalActivation.");
            return ret;
        }

        const std::string& ElectricalActivation::DepolarizationDuration::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::DepolarizationDuration::DefaultValue()
        {
            static std::string ret("0.04");
            return ret;
        }


        const std::string& ElectricalActivation::PlateauDuration::NameInFile()
        {
            static std::string ret("PlateauDuration");
            return ret;
        }


        const std::string& ElectricalActivation::PlateauDuration::Description()
        {
            static std::string ret("Plateau Duration of the ElectricalActivation. Irrelevant if analytic.");
            return ret;
        }

        const std::string& ElectricalActivation::PlateauDuration::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::PlateauDuration::DefaultValue()
        {
            static std::string ret("0.25");
            return ret;
        }


        const std::string& ElectricalActivation::RepolarizationDuration::NameInFile()
        {
            static std::string ret("RepolarizationDuration");
            return ret;
        }


        const std::string& ElectricalActivation::RepolarizationDuration::Description()
        {
            static std::string ret("Repolarization Duration of the ElectricalActivation.");
            return ret;
        }

        const std::string& ElectricalActivation::RepolarizationDuration::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ElectricalActivation::RepolarizationDuration::DefaultValue()
        {
            static std::string ret("0.2");
            return ret;
        }


        const std::string& ElectricalActivation::Type::NameInFile()
        {
            static std::string ret("Type");
            return ret;
        }


        const std::string& ElectricalActivation::Type::Description()
        {
            static std::string ret(
                "Type of the ElectricalActivation. With prescribed need two fibers for delay and plateau_duration.");
            return ret;
        }

        const std::string& ElectricalActivation::Type::Constraint()
        {
            static std::string ret("value_in(v, {'analytic', 'prescribed'})");
            return ret;
        }


        const std::string& ElectricalActivation::Type::DefaultValue()
        {
            static std::string ret("\"analytic\"");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
