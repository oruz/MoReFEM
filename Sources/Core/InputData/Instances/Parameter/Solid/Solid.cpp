/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 15:31:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& Solid::GetName()
        {
            static std::string ret("Solid");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
