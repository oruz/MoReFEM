//
//  PlaneStressStrain.cpp
//  All targets
//
//  Created by Jerôme Diaz on 10/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {

        const std::string& Solid::PlaneStressStrain::NameInFile()
        {
            static std::string ret("PlaneStressStrain");
            return ret;
        }


        const std::string& Solid::PlaneStressStrain::Description()
        {
            static std::string ret("For 2D operators, which approximation to use.");
            return ret;
        }

        const std::string& Solid::PlaneStressStrain::Constraint()
        {
            static std::string ret("value_in(v, {'irrelevant', 'plane_strain', 'plane_stress'})");
            return ret;
        }


        const std::string& Solid::PlaneStressStrain::DefaultValue()
        {
            static std::string ret("\"plane_strain\"");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
