//
//  CheckInvertedElements.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 17/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {

        const std::string& Solid::CheckInvertedElements::NameInFile()
        {
            static std::string ret("CheckInvertedElements");
            return ret;
        }


        const std::string& Solid::CheckInvertedElements::Description()
        {
            static std::string ret("If the displacement induced by the applied loading is strong enough"
                                   "it can lead to inverted elements: some vertices of the element are moved"
                                   "such that the volume of the finite element becomes negative. This means"
                                   "that the resulting deformed mesh is no longer valid. This parameter enables a"
                                   "computationally expensive test on all quadrature points to check that the volume"
                                   "of all finite elements remains positive throughout the computation.");

            return ret;
        }

        const std::string& Solid::CheckInvertedElements::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& Solid::CheckInvertedElements::DefaultValue()
        {
            static std::string ret("false");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
