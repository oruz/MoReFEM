/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& Solid::Viscosity::GetName()
        {
            static std::string ret("Viscosity");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
