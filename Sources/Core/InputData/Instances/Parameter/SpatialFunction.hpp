/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:08:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_

#include <cassert> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>


namespace MoReFEM
{


    //! Whether \a Coords or \a LocalCoords are considered.
    enum class CoordsType
    {
        local,
        global
    };


    namespace InputDataNS
    {

        /*!
         * \brief Input parameter which wraps a Lua function with three arguments for spatial coordinates.
         *
         * \tparam CoordsTypeT Whether we deal with local or global coordinates for the function.
         * \tparam LuaFunctionTypeT Type of the Lua function, e.g. \a spatial_lua_function
         * for a function which gets three double parameters and returns a double.
         */
        template<CoordsType CoordsTypeT, class LuaFunctionTypeT>
        struct SpatialFunction
        {

            //! Returns the type of Coords.
            static CoordsType GetCoordsType();

            //! Part of the description related to the format of the spatial function.
            static const std::string& DescriptionCoordsType();

            //! Lua function type.
            using lua_function_type = LuaFunctionTypeT;
        };


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/SpatialFunction.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HPP_
