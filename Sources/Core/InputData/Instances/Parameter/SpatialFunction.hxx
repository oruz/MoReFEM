/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:29:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"

#include <cassert>
#include <cstdlib>
#include <iosfwd>
#include <string>


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<CoordsType CoordsTypeT, class LuaFunctionTypeT>
        inline CoordsType SpatialFunction<CoordsTypeT, LuaFunctionTypeT>::GetCoordsType()
        {
            return CoordsTypeT;
        }


        template<CoordsType CoordsTypeT, class LuaFunctionTypeT>
        const std::string& SpatialFunction<CoordsTypeT, LuaFunctionTypeT>::DescriptionCoordsType()
        {
            if constexpr (CoordsTypeT == CoordsType::local)
            {
                static std::string ret(
                    "\n Function expects as arguments local coordinates (e.g. coords of a quadrature point).");
                return ret;
            } else if constexpr (CoordsTypeT == CoordsType::global)
            {
                static std::string ret(
                    "\n Function expects as arguments global coordinates (coordinates in the mesh).");
                return ret;
            } else
            {
                assert(false && "All possible values of CoordsType should be handled explicitly here.");
                exit(EXIT_FAILURE);
            }
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_
