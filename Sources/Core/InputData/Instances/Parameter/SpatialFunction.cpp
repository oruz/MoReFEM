/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:29:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
