/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 12:00:59 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_FIBER_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_FIBER_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT, ::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT>
        constexpr std::size_t Fiber<IndexT, PolicyT, TypeT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template<std::size_t IndexT, ::MoReFEM::FiberNS::AtNodeOrAtQuadPt PolicyT, ParameterNS::Type TypeT>
        const std::string& Fiber<IndexT, PolicyT, TypeT>::GetName()
        {
            static std::string ret =
                Impl::GenerateSectionName(std::string("Fiber_") + ::MoReFEM::ParameterNS::Name<TypeT>() + "_", IndexT);
            return ret;
        };


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_FIBER_HXX_
