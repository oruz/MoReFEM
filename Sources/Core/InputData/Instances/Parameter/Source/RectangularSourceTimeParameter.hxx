/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Sep 2015 15:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Source/RectangularSourceTimeParameter.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {

        template<std::size_t IndexT>
        constexpr std::size_t RectangularSourceTimeParameter<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template<std::size_t IndexT>
        const std::string& RectangularSourceTimeParameter<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("RectangularSourceTimeParameter", IndexT);
            return ret;
        }


        template<std::size_t IndexT>
        typename RectangularSourceTimeParameter<IndexT>::section_content_type&
        RectangularSourceTimeParameter<IndexT>::GetNonCstSectionContent() noexcept
        {
            return section_content_;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_
