/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        struct ScalarTransientSource : public Crtp::Section<ScalarTransientSource<IndexT>, NoEnclosingSection>
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'TransientSource1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = ScalarTransientSource<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
             */
            struct Nature
            : public Utilities::InputDataNS::Crtp::InputData<
                  Nature,
                  self,
                  typename Advanced::InputDataNS::ParamNS::Nature<
                      Advanced::InputDataNS::ParamNS::IsVectorial::no>::storage_type>,
              public Advanced::InputDataNS::ParamNS::Nature<Advanced::InputDataNS::ParamNS::IsVectorial::no>
            { };

            //! Convenient alias to define \a Value.
            using param_value_type =
                Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

            //! \copydoc doxygen_hide_param_value_struct
            struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                           public param_value_type
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                Nature,
                Value
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct TransientSource


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_
