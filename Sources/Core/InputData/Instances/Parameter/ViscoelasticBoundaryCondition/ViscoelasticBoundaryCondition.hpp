/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Aug 2013 11:26:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_

#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! \copydoc doxygen_hide_core_input_data_section
        struct ViscoelasticBoundaryCondition : public Crtp::Section<ViscoelasticBoundaryCondition, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter ('ViscoelasticBoundaryCondition' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = ViscoelasticBoundaryCondition;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct Damping : public Crtp::Section<Damping, ViscoelasticBoundaryCondition>
            {


                //! Convenient alias.
                using self = Damping;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, ViscoelasticBoundaryCondition>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };

                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct Damping


            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct Stiffness : public Crtp::Section<Stiffness, ViscoelasticBoundaryCondition>
            {


                //! Convenient alias.
                using self = Stiffness;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, ViscoelasticBoundaryCondition>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };

                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct Stiffness


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                Damping,
                Stiffness
            >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct ViscoelasticBoundaryCondition


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/ViscoelasticBoundaryCondition/ViscoelasticBoundaryCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_
