### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Diffusion/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ElectricalActivation/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fiber/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fluid/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Heart/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/MaterialProperty/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Microsphere/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solid/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Source/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ViscoelasticBoundaryCondition/SourceList.cmake)
