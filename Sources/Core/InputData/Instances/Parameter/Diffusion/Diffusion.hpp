/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <tuple>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        //! \copydoc doxygen_hide_core_input_data_section
        struct Diffusion : public Crtp::Section<Diffusion, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = Diffusion;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! \copydoc doxygen_hide_core_input_data_section_with_index
            template<std::size_t IndexT>
            struct Tensor : public Crtp::Section<Tensor<IndexT>, Diffusion>
            {


                //! Convenient alias.
                using self = Tensor<IndexT>;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                 */
                struct Nature
                : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                  public Advanced::InputDataNS::ParamNS::Nature<>
                { };

                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;

            }; // struct Tensor


            //! \copydoc doxygen_hide_core_input_data_section
            struct Density : public Crtp::Section<Density, Diffusion>
            {


                //! Convenient alias.
                using self = Density;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the diffusion density (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputData<Nature,
                                                       Density,
                                                       typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                                public Advanced::InputDataNS::ParamNS::Nature<>
                { };

                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct Density


            //! \copydoc doxygen_hide_core_input_data_section
            struct TransfertCoefficient : public Crtp::Section<TransfertCoefficient, Diffusion>
            {

                //! Convenient alias.
                using self = TransfertCoefficient;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the transfert coefficient (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputData<Nature,
                                                       TransfertCoefficient,
                                                       typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
                                public Advanced::InputDataNS::ParamNS::Nature<>
                { };


                //! Convenient alias to define \a Value.
                using param_value_type =
                    Advanced::InputDataNS::ParamNS::Value<Nature, Advanced::InputDataNS::ParamNS::IsVectorial::no>;

                //! \copydoc doxygen_hide_param_value_struct
                struct Value : public Crtp::InputData<Value, self, typename param_value_type::storage_type>,
                               public param_value_type
                { };


                //! Alias to the tuple of structs.
                // clang-format off
                using section_content_type = std::tuple
                <
                    Nature,
                    Value
                >;
                // clang-format on


              private:
                //! Content of the section.
                section_content_type section_content_;


            }; // struct TransfertCoefficient


        }; // struct Diffusion


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
