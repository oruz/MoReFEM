/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 15:31:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        const std::string& Diffusion::GetName()
        {
            static std::string ret("Diffusion");
            return ret;
        };


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
