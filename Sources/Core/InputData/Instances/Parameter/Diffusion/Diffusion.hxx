/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"

#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT>
        const std::string& Diffusion::Tensor<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("Tensor", IndexT);
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_
