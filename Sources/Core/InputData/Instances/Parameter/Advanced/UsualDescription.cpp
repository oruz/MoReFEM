/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{


    namespace Advanced::InputDataNS::ParamNS
    {


        const std::string& LuaFunction::NameInFile()
        {
            static std::string ret("lua_function");
            return ret;
        }


        const std::string& LuaFunction::Description()
        {
            static std::string ret(
                "Value of the Parameter in the case nature is 'lua_function'"
                "(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).");
            return ret;
        }

        const std::string& LuaFunction::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& LuaFunction::DefaultValue()
        {
            return Utilities::EmptyString();
        }


    } // namespace Advanced::InputDataNS::ParamNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
