/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HPP_

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits>
#include <variant>
#include <vector>

#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/SpatialLuaFunction.hpp"


namespace MoReFEM
{


    namespace Advanced::InputDataNS::ParamNS
    {


        //! Convenient alias to indicate whether a \a Parameter is scalar or vectorial.
        enum class IsVectorial
        {
            no,
            yes
        };


        /*!
         * \brief Choose how is described the \a Parameter (through a scalar, a function, etc...)
         *
         * \tparam IsVectorialT Whether the parameter is scalar or vectorial
         */
        template<IsVectorial IsVectorialT = IsVectorial::no>
        struct Nature
        {


            //! Convenient alias.
            // clang-format off
            using storage_type =
                std::conditional_t
                <
                    IsVectorialT == IsVectorial::yes,
                    std::vector<std::string>,
                    std::string
                >;
            // clang-format off

            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has been given
             * in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        /*!
         * \brief Function that determines \a Parameter value. Irrelevant if nature is not lua_function.
         */
        struct LuaFunction : public ::MoReFEM::InputDataNS::SpatialFunction<CoordsType::global, spatial_lua_function>
        {

            //! Convenient alias.
            using storage_type = spatial_lua_function;

            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has been given
             * in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a LuaOptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };



        /*!
         * \brief The value of the parameter.
         *
         * A std::variant is used here: the value might be a scalar, an associative container or a LuaFunction
         * depending on what the user chose as 'NatureT'.
         *
         * A std::variant might be read with a visitor (see std::visit) or std::get<> calls; anyway these calls
         * aren't supposed to be exposed for user interface.
         *
         * \tparam NatureT Another field of the input data file, which should provide the type of expected data.
         * The associated string read should be "constant", "piecewise_constant_by_domain" or "lua_function".
         *
         * Typically it should be a sibling field in the same section, e.g.:
         *
         \verbatim
         Kappa1 =
         {
            nature = "constant",
            value = 5.
         }
         \endverbatim
         * where nature is the field interpreted by NatureT and value the field interpreted by current struct.
         *
         * \tparam ComponentT 'none' if the parameter is scalar, 'X', 'Y'or 'Z' otherwise.
         */
        template<class NatureT, IsVectorial IsVectorialT>
        struct Value
        {


            //! Alias to the std::variant which describes all the types into which Parameter may be expressed.
            using variant_type = std::variant
            <
                double,
                spatial_lua_function,
                std::map<std::size_t, double>,
                std::nullptr_t // is nature is 'ignore'
            >;

            //! The type used to store the value; depends upon whether we consider a vectorial or scalar \a Parameter.
            // clang-format off
            using storage_type =
            std::conditional_t
            <
                IsVectorialT == IsVectorial::yes,
                std::vector<variant_type>,
                variant_type
            >;
            // clang-format on


            /*!
             * \brief Selector which role is to set the std::variant with the proper type pointed by \a NatureT.
             *
             * The std::variant is filled with default value: the point is not the actual value put there, but
             * the fact that the type is known (e.g. assigning a double if \a NatureT points to 'constant'.
             *
             * \tparam InputDataT Type of \a input_data.
             *
             * \copydoc doxygen_hide_input_data_arg
             *
             * \return The std::variant properly filled with a non important value of the correct type (e.g. double
             * if NatureT gives away "constant", std::map>std::size_t, double> if NatureT is
             * "piecewise_constant_by_domain".
             */
            template<class InputDataT>
            static storage_type Selector(const InputDataT* input_data);


            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from
             * \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has
             * been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * LuaOptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        namespace Impl
        {


            /*!
             * \brief Returns the description of the Selector class.
             *
             * \tparam IsVectorialT Whether the parameter is scalar or vectorial
             *
             * \return The description as a new std::string.
             */
            template<IsVectorial IsVectorialT>
            std::string SelectorDescription();


        } // namespace Impl


    } // namespace Advanced::InputDataNS::ParamNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HPP_
