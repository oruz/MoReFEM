/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 15:17:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Solver/Impl/Petsc.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputDataNS::DirichletBoundaryCondition should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct Petsc
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        struct Petsc : public BaseNS::Petsc, public Crtp::Section<Petsc<IndexT>, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'DirichletBoundaryCondition' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr std::size_t GetUniqueId() noexcept;


            //! Convenient alias.
            using self = Petsc<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! Holds information related to the input parameter Petsc::Solver.
            struct Solver : public Crtp::InputData<Solver, self, std::string>, public Impl::PetscNS::Solver
            { };


            //! Holds information related to the input parameter Petsc::gmresRestart.
            struct GmresRestart : public Crtp::InputData<GmresRestart, self, std::size_t>,
                                  public Impl::PetscNS::GmresRestart
            { };


            //! Holds information related to the input parameter Petsc::preconditioner.
            struct Preconditioner : public Crtp::InputData<Preconditioner, self, std::string>,
                                    public Impl::PetscNS::Preconditioner
            { };


            //! Holds information related to the input parameter Petsc::relativeTolerance.
            struct RelativeTolerance : public Crtp::InputData<RelativeTolerance, self, double>,
                                       public Impl::PetscNS::RelativeTolerance
            { };


            //! Holds information related to the input parameter Petsc::absoluteTolerance.
            struct AbsoluteTolerance : public Crtp::InputData<AbsoluteTolerance, self, double>,
                                       public Impl::PetscNS::AbsoluteTolerance
            { };


            //! Holds information related to the input parameter Petsc::stepSizeTolerance.
            struct StepSizeTolerance : public Crtp::InputData<StepSizeTolerance, self, double>,
                                       public Impl::PetscNS::StepSizeTolerance
            { };


            //! Holds information related to the input parameter Petsc::maxIteration.
            struct MaxIteration : public Crtp::InputData<MaxIteration, self, std::size_t>,
                                  public Impl::PetscNS::MaxIteration
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                AbsoluteTolerance,
                GmresRestart,
                MaxIteration,
                Preconditioner,
                RelativeTolerance,
                StepSizeTolerance,
                Solver
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct Petsc


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Solver/Petsc.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_
