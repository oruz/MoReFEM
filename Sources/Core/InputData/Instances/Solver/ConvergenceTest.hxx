/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Apr 2016 16:36:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Solver/ConvergenceTest.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HXX_
