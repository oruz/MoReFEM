/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Apr 2016 16:36:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Solver/ConvergenceTest.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {

        const std::string& ConvergenceTest::GetName()
        {
            static std::string ret("ConvergenceTest");
            return ret;
        }


        const std::string& ConvergenceTest::ResidualNormMax::NameInFile()
        {
            static std::string ret("ResidualNormMax");
            return ret;
        }


        const std::string& ConvergenceTest::ResidualNormMax::Description()
        {
            static std::string ret("Maximum residual norm evaluation in newton loop.");
            return ret;
        }

        const std::string& ConvergenceTest::ResidualNormMax::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& ConvergenceTest::ResidualNormMax::DefaultValue()
        {
            static std::string ret("1.e5");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
