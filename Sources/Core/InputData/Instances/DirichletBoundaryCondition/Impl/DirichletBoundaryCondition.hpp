/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_IMPL_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_IMPL_x_DIRICHLET_BOUNDARY_CONDITION_HPP_

#include <iosfwd>


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


            namespace DirichletBoundaryConditionNS
            {


                //! Holds information related to the input parameter BoundaryCondition::variable.
                struct Name
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Holds information related to the input parameter BoundaryCondition::variable.
                struct UnknownName
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Holds information related to the input parameter BoundaryCondition::component.
                struct Component
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Lit of all domains upon which a boudnary condition occur.
                struct DomainIndex
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Holds information related to the input parameter BoundaryCondition::value.
                struct Values
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! \copydoc doxygen_hide_core_input_data_parameter
                struct IsMutable
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some
                     * text from \a LuaOptionFile example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
                     * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();


                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no
                     * value has been given in the input data file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be \a
                     * LuaOptionFile-formatted.
                     */
                    static const std::string& DefaultValue();
                };


            } // namespace DirichletBoundaryConditionNS


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_IMPL_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
