/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Sep 2015 11:23:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT>
        const std::string& DirichletBoundaryCondition<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("EssentialBoundaryCondition", IndexT);
            return ret;
        };


        template<std::size_t IndexT>
        constexpr std::size_t DirichletBoundaryCondition<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
