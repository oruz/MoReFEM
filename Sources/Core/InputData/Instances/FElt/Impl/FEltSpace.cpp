/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 11:47:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/FElt/Impl/FEltSpace.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace Impl
        {


            namespace FEltSpaceNS
            {


                const std::string& GodOfDofIndex::NameInFile()
                {
                    static std::string ret("god_of_dof_index");
                    return ret;
                }


                const std::string& GodOfDofIndex::Description()
                {
                    static std::string ret("Index of the god of dof into which the finite element space is defined.");
                    return ret;
                }


                const std::string& GodOfDofIndex::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& GodOfDofIndex::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& DomainIndex::NameInFile()
                {
                    static std::string ret("domain_index");
                    return ret;
                }


                const std::string& DomainIndex::Description()
                {
                    static std::string ret("Index of the domain onto which the finite element space is defined. "
                                           "This domain must be unidimensional.");

                    return ret;
                }


                const std::string& DomainIndex::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& DomainIndex::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& UnknownList::NameInFile()
                {
                    static std::string ret("unknown_list");
                    return ret;
                }


                const std::string& UnknownList::Description()
                {
                    static std::string ret("List of all unknowns defined in the finite element space. "
                                           "Unknowns here must be defined in this file as an 'Unknown' block; "
                                           "expected name/identifier is the name given there.");

                    return ret;
                }


                const std::string& UnknownList::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& UnknownList::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& ShapeFunctionList::NameInFile()
                {
                    static std::string ret("shape_function_list");
                    return ret;
                }


                const std::string& ShapeFunctionList::Description()
                {
                    static std::string ret("List of the shape function to use for each unknown;");
                    return ret;
                }


                const std::string& ShapeFunctionList::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& ShapeFunctionList::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


                const std::string& NumberingSubsetList::NameInFile()
                {
                    static std::string ret("numbering_subset_list");
                    return ret;
                }


                const std::string& NumberingSubsetList::Description()
                {
                    static std::string ret("List of the numbering subset to use for each unknown;");
                    return ret;
                }


                const std::string& NumberingSubsetList::Constraint()
                {
                    return Utilities::EmptyString();
                }


                const std::string& NumberingSubsetList::DefaultValue()
                {
                    return Utilities::EmptyString();
                }


            } // namespace FEltSpaceNS


        } // namespace Impl


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
