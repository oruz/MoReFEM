/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 11:47:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/FElt/Impl/FEltSpace.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputDataNS::FEltSpace should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct FEltSpace
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        struct FEltSpace : public BaseNS::FEltSpace, public Crtp::Section<FEltSpace<IndexT>, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'FEltSpace' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr std::size_t GetUniqueId() noexcept;


            //! Convenient alias.
            using self = FEltSpace<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Indicates the god of dof that possess the finite element space.
             *
             * God of dof index is the same as mesh one.
             */
            struct GodOfDofIndex : public Crtp::InputData<GodOfDofIndex, self, std::size_t>,
                                   public Impl::FEltSpaceNS::GodOfDofIndex
            { };


            //! Indicates the domain upon which the finite element space is defined. This domain must be
            //! uni-dimensional.
            struct DomainIndex : public Crtp::InputData<DomainIndex, self, std::size_t>,
                                 public Impl::FEltSpaceNS::DomainIndex
            { };


            //! Indicates which unknowns are defined on the finite element space.
            struct UnknownList : public Crtp::InputData<UnknownList, self, std::vector<std::string>>,
                                 public Impl::FEltSpaceNS::UnknownList
            { };


            //! Indicates for each unknowns the shape function to use.
            struct ShapeFunctionList : public Crtp::InputData<ShapeFunctionList, self, std::vector<std::string>>,
                                       public Impl::FEltSpaceNS::ShapeFunctionList
            { };


            //! Indicates the numbering subset to use for each unknown.
            struct NumberingSubsetList : public Crtp::InputData<NumberingSubsetList, self, std::vector<std::size_t>>,
                                         public Impl::FEltSpaceNS::NumberingSubsetList
            { };


            //! Alias to the tuple of structs.
            // clang-format off
            using section_content_type = std::tuple
            <
                GodOfDofIndex,
                DomainIndex,
                UnknownList,
                ShapeFunctionList,
                NumberingSubsetList
            >;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct FEltSpace


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/FElt/FEltSpace.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_
