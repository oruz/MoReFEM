/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 11 Apr 2017 11:30:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/OutputDeformedMesh.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("OutputDeformedMesh", IndexT);
            return ret;
        };


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputName::NameInFile()
        {
            static std::string ret("output_name");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputName::Description()
        {
            static std::string ret("Name of the ouput deformed mesh.");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputName::Constraint()
        {
            return Utilities::EmptyString();
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputName::DefaultValue()
        {
            static std::string ret("\"output_mesh\"");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFormat::NameInFile()
        {
            static std::string ret("output_format");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFormat::Description()
        {
            static std::string ret("Format of the ouput deformed mesh.");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFormat::Constraint()
        {
            static std::string ret("value_in(v, {'Ensight', 'Medit'})");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFormat::DefaultValue()
        {
            static std::string ret("\"Medit\"");
            return ret;
        }

        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::NameInFile()
        {
            static std::string ret("output_space_unit");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::Description()
        {
            static std::string ret("Space unit of the output mesh relative to the real unit of the input mesh, "
                                   "(output_space_unit / mesh_space_unit).");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::Constraint()
        {
            return Utilities::EmptyString();
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::DefaultValue()
        {
            static std::string ret("1.");
            return ret;
        }

        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputOffset::NameInFile()
        {
            static std::string ret("output_offset");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputOffset::Description()
        {
            static std::string ret("Index of the iteration you want to start the output.");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputOffset::Constraint()
        {
            return Utilities::EmptyString();
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputOffset::DefaultValue()
        {
            static std::string ret("1");
            return ret;
        }

        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::NameInFile()
        {
            static std::string ret("output_frequence");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::Description()
        {
            static std::string ret("Frequence of the iterations you want the output.");
            return ret;
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::Constraint()
        {
            return Utilities::EmptyString();
        }


        template<std::size_t IndexT>
        const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::DefaultValue()
        {
            static std::string ret("1");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HXX_
