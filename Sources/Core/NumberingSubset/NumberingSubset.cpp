/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
#include <vector>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    const std::string& NumberingSubset::ClassName()
    {
        static std::string ret("NumberingSubset");
        return ret;
    }


    NumberingSubset::NumberingSubset(std::size_t id, bool do_move_mesh)
    : unique_id_parent(id), do_move_mesh_(do_move_mesh)
    { }


#ifndef NDEBUG
    bool IsConsistentOverRanks(const Wrappers::Mpi& mpi,
                               const NumberingSubset::vector_const_shared_ptr& numbering_subset_list)
    {
        std::vector<std::size_t> numbering_subset_index_list(numbering_subset_list.size());

        std::transform(numbering_subset_list.cbegin(),
                       numbering_subset_list.cend(),
                       numbering_subset_index_list.begin(),
                       [](const auto& numbering_subset_ptr)
                       {
                           assert(!(!numbering_subset_ptr));
                           return numbering_subset_ptr->GetUniqueId();
                       });

        return IsSameForAllRanks(mpi, numbering_subset_index_list);
    }
#endif // NDEBUG


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
