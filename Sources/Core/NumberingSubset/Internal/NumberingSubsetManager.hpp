/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HPP_
#define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            /*!
             * \brief Object that is aware of all existing \a NumberingSubset.
             *
             * \internal <b><tt>[internal]</tt></b> Contrary to other managers, this one is really meant to be
             * hidden to users and developers: NumberingSubset should be queried against \a GodOfDof objects.
             * \endinternal
             */
            class NumberingSubsetManager : public Utilities::Singleton<NumberingSubsetManager>
            {

              public:
                /*!
                 * \brief Returns the name of the class (required for some Singleton-related errors).
                 *
                 * \return Name of the class.
                 */
                static const std::string& ClassName();

                /*!
                 * \brief Base type of NumberingSubse as input parameter (requested to identify numbering subsets in
                 * the input parameter data).
                 */
                using input_data_type = ::MoReFEM::InputDataNS::BaseNS::NumberingSubset;


              public:
                /*!
                 * \brief Create a new Unknown object from the data of the input data file.
                 *
                 * \param[in] section Section of the input data file dedicated to the Unknown to build.
                 */
                template<class UnknownSectionT>
                void Create(const UnknownSectionT& section);

                /*!
                 * \class doxygen_hide_numbering_subset_manager_unique_id_arg
                 *
                 * \param[in] unique_id Unique identifier of the NumberingSubset, e.g. 5 for what is written
                 * in input data file NumberingSubset5.
                 */


                /*!
                 * \brief Fetch the \a NumberingSubset object associated with \a unique_id unique identifier.
                 *
                 * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
                 * \return Reference to the numbering subset.
                 */
                const NumberingSubset& GetNumberingSubset(std::size_t unique_id) const;

                /*!
                 * \brief Fetch the NumberingSubset object associated with \a unique_id unique identifier.
                 *
                 * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
                 *
                 * \return Shared pointer to the NumberingSubset.
                 */
                NumberingSubset::const_shared_ptr GetNumberingSubsetPtr(std::size_t unique_id) const;

                /*!
                 * \brief Whether numbering subset \a unique_id is handled by the NumberingSubsetManager.
                 *
                 * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
                 *
                 * \return True if a \a NumberingSubset with \a unique_id do exist.
                 */
                bool DoExist(std::size_t unique_id) const;


              public:
                /*!
                 * \brief Get access to the list of existing numbering subset.
                 *
                 * \internal This method is public solely because of its occasional usefulness in debug; you shouldn't
                 * have to use it while writing a \a Model.
                 * \endinternal
                 *
                 * \return List of pointers to the \a NumberingSubset avaliable throughout the program.
                 */
                const NumberingSubset::vector_const_shared_ptr& GetList() const;


              private:
                //! \name Singleton requirements.
                ///@{

                //! Constructor.
                NumberingSubsetManager() = default;

                //! Destructor.
                virtual ~NumberingSubsetManager() override;

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<NumberingSubsetManager>;
                ///@}


              private:
                /*!
                 * \brief Create a new NumberingSubset object.
                 *
                 * \param[in] unique_id Unique identifier of the NumberingSubset, which is also what tags the
                 * NumberingSubset in the input data file (e.g. 5 in 'NumberingSubset5').
                 * \param[in] do_move_mesh Whether the numbering subset covers a displacement unknown that might be
                 * used to make the mesh move. Should be false in most cases; see \a MovemeshHelper for more details.
                 */
                void Create(std::size_t unique_id, bool do_move_mesh);

                //! Get non constant access to the list of existing numbering subset.
                NumberingSubset::vector_const_shared_ptr& GetNonCstList();

                //! Get the iterator to the element matching \a unique_id.
                //! \param[in] unique_id The unique identifier which iterator is sought.
                NumberingSubset::vector_const_shared_ptr::const_iterator GetIterator(std::size_t unique_id) const;


              private:
                //! Store the NumberingSubset objects by their unique identifier.
                NumberingSubset::vector_const_shared_ptr list_;
            };


        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HPP_
