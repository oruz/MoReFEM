/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 12:17:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FIND_FUNCTOR_HXX_
#define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FIND_FUNCTOR_HXX_

// IWYU pragma: private, include "Core/NumberingSubset/Internal/FindFunctor.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            template<class T>
            FindIfCondition<T>::FindIfCondition(const NumberingSubset& numbering_subset)
            : numbering_subset_(numbering_subset)
            { }


            template<class T>
            bool FindIfCondition<T>::operator()(const T& rhs) const noexcept
            {
                return FetchNumberingSubset(rhs) == numbering_subset_;
            }


            template<class T>
            FindIfConditionForPair<T>::FindIfConditionForPair(const NumberingSubset& row_numbering_subset,
                                                              const NumberingSubset& col_numbering_subset)
            : numbering_subset_pair_(std::make_pair(std::cref(row_numbering_subset), std::cref(col_numbering_subset)))
            { }


            template<class T>
            bool FindIfConditionForPair<T>::operator()(const T& rhs) const noexcept
            {
                assert(!(!rhs));
                return FetchNumberingSubsetPair(rhs) == numbering_subset_pair_;
            }


        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FIND_FUNCTOR_HXX_
