/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 10:26:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HPP_
#define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HPP_

#include <memory>
#include <vector>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            /*!
             * \brief Returns the requested numbering subset from \a object when T is a pointer or smart pointer.
             *
             * \param[in] pointer Pointer or pointer-like to an object that must define a method with prototype:
             *
             * \code
             * const NumberingSubset& GetNumberingSubset() const;
             * \endcode
             *
             * \return Reference returned by pointer->GetNumberingSubset().
             */
            template<class T>
            std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                             const NumberingSubset&>
            FetchNumberingSubset(const T& pointer) noexcept;


            /*!
             * \brief Returns the requested numbering subset from \a object when T is <b>not</b> a pointer or smart
             * pointer.
             *
             * \param[in] object Object that must define a method with prototype:
             *
             * \code
             * const NumberingSubset& GetNumberingSubset() const;
             * \endcode
             *
             * \return Object returned by object.GetNumberingSubset().
             */
            template<class T>
            std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                             const NumberingSubset&>
            FetchNumberingSubset(const T& object) noexcept;


            /*!
             * \brief Returns the requested numbering subset pair from \a object when T is a pointer or smart pointer.
             *
             * \param[in] pointer Pointer or pointer-like to an object that must define methods with following
             * prototypes:
             *
             * \code
             * const NumberingSubset& GetRowNumberingSubset() const;
             * const NumberingSubset& GetColNumberingSubset() const;
             * \endcode
             *
             * \return A tuple with two references to respectively NumberingSubset to use for rows and columns.
             */
            template<class T>
            std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                             std::pair<const NumberingSubset&, const NumberingSubset&>>
            FetchNumberingSubsetPair(const T& pointer) noexcept;


            /*!
             * \brief Returns the requested numbering subset pair from \a object when T is <b>not</b> a pointer or smart
             * pointer.
             *
             * \param[in] object Object that must define methods with following prototypes:
             *
             * \code
             * const NumberingSubset& GetRowNumberingSubset() const;
             * const NumberingSubset& GetColNumberingSubset() const;
             * \endcode
             *
             * \return A tuple with two references to respectively NumberingSubset to use for rows and columns.
             */

            template<class T>
            std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                             std::pair<const NumberingSubset&, const NumberingSubset&>>
            FetchNumberingSubsetPair(const T& object) noexcept;


        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/NumberingSubset/Internal/FetchFunction.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HPP_
