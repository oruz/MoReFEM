/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

// IWYU pragma: no_include <iosfwd>
#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string> // IWYU pragma: keep
#include <type_traits>
#include <vector>

#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            NumberingSubsetManager::~NumberingSubsetManager() = default;


            const std::string& NumberingSubsetManager::ClassName()
            {
                static std::string ret("NumberingSubsetManager");
                return ret;
            }


            NumberingSubset::vector_const_shared_ptr::const_iterator
            NumberingSubsetManager ::GetIterator(std::size_t unique_id) const
            {
                const auto& list = GetList();

                return std::find_if(list.cbegin(),
                                    list.cend(),
                                    [unique_id](const auto& numbering_subset_ptr)
                                    {
                                        assert(!(!numbering_subset_ptr));
                                        return unique_id == numbering_subset_ptr->GetUniqueId();
                                    });
            }


            void NumberingSubsetManager::Create(std::size_t unique_id, bool do_move_mesh)
            {
                auto raw_ptr = new NumberingSubset(unique_id, do_move_mesh);

                auto ptr = NumberingSubset::const_shared_ptr(raw_ptr);

                assert(ptr->GetUniqueId() == unique_id);

                auto it = GetIterator(unique_id);

                auto& list = GetNonCstList();

                if (it != list.cend())
                    throw Exception("Two numbering subset objects can't share the same unique identifier! (namely "
                                        + std::to_string(unique_id) + ").",
                                    __FILE__,
                                    __LINE__);

                list.push_back(ptr);
            }


            bool NumberingSubsetManager::DoExist(std::size_t unique_id) const
            {
                auto it = GetIterator(unique_id);
                return (it != GetList().cend());
            }


        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
