/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    TimeManager::~TimeManager() = default;


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
