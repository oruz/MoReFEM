/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/TimeManager/TimeManager.hpp"
#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace TimeManagerNS
    {


        namespace Policy
        {


            /*!
             * \brief TimeManager policy when time step is constant.
             *
             * So in input file time step and maximum time are enough to describe the whole time evolution.
             */
            class ConstantTimeStep
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = ConstantTimeStep;

              protected:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_input_data_arg
                 */
                template<class InputDataT>
                explicit ConstantTimeStep(const InputDataT& input_data);

                //! Destructor.
                ~ConstantTimeStep() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ConstantTimeStep(const ConstantTimeStep& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ConstantTimeStep(ConstantTimeStep&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ConstantTimeStep& operator=(const ConstantTimeStep& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ConstantTimeStep& operator=(ConstantTimeStep&& rhs) = delete;

                ///@}

                /*!
                 * \brief Increment the time.
                 *
                 * \param[in,out] time Time is incremented with current time step. Tn+1 = Tn + dt.
                 * Current time step is stored in the class.
                 *
                 */
                void IncrementTime(double& time);

                /*!
                 * \brief Decrement the time.
                 *
                 * \param[in,out] time Time is decremented with current time step. Tn+1 = Tn - dt.
                 * Current time step is stored in the class.
                 *
                 */
                void DecrementTime(double& time);

                //! Get the size of a time step.
                double GetTimeStep() const noexcept;

                //! Get the size of a time step.
                double& GetNonCstTimeStep();

                //! Returns true if current time is equal or beyond maximum time.
                //! \param[in] time Current time.
                bool HasFinished(double time) const;

                //! Returns whether the time step is constant (obviously true for this class!).
                static constexpr bool IsTimeStepConstant() noexcept;

                //! Get the maximum time (in seconds).
                double GetMaximumTime() const noexcept;

                /*!
                 * \copydoc doxygen_hide_time_manager_adapt_time_step
                 * \param[in] time Current time.
                 */
                [[noreturn]] void AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                                const double time);

                //! Set a new time step.
                //! \param[in] time_step New value for time step.
                void SetTimeStep(double time_step);

                /*!
                 * \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
                 * \param[in,out] time Current time.
                 */
                void ResetTimeManagerAtInitialTime(double& time);

              private:
                //! Get the number of iterations done from the start.
                std::size_t& NiterationsFromStart() noexcept;

                //! Get the initial time.
                double GetInitialTime() const noexcept;

              private:
                //! Size of a time step.
                double time_step_ = std::numeric_limits<double>::lowest();

                //! Maximum time.
                double maximum_time_ = std::numeric_limits<double>::lowest();

                //! Number of iterations done from the start.
                std::size_t Niterations_from_start_ = 0ul;

                //! Initial time.
                double initial_time_ = std::numeric_limits<double>::lowest();
            };


        } // namespace Policy


    } // namespace TimeManagerNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/Policy/ConstantTimeStep.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_
