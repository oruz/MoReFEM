/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:37:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_

// IWYU pragma: private, include "Core/TimeManager/TimeManager.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    template<class MoReFEMDataT>
    TimeManager::TimeManager(const MoReFEMDataT& morefem_data)
    {
        namespace IPL = Utilities::InputDataNS;
        using TimeManager = InputDataNS::TimeManager;

        time_iteration_file_ = morefem_data.GetResultDirectory().AddFile("time_iteration.hhdata");

        decltype(auto) mpi = morefem_data.GetMpi();

        if (mpi.IsRootProcessor())
        {
            if (FilesystemNS::File::DoExist(time_iteration_file_))
            {
                std::cerr << "[WARNING] A file named " << time_iteration_file_
                          << " already existed; it has been removed "
                             "and recreated from scratch. Please consider using ${MOREFEM_START_TIME} in your result "
                             "directory path "
                             "to guarantee each run gets its outputs written in an empty directory."
                          << std::endl;
                FilesystemNS::File::Remove(time_iteration_file_, __FILE__, __LINE__);
            }

            std::ofstream stream;
            FilesystemNS::File::Create(stream, time_iteration_file_, __FILE__, __LINE__);
            stream << "# Time iteration; time; numbering subset id; filename" << std::endl;
        }

        time_ = IPL::Extract<TimeManager::TimeInit>::Value(morefem_data.GetInputData());
    }


    inline std::size_t TimeManager::NtimeModified() const
    {
        return Ntime_modified_;
    }


    inline double TimeManager::GetTime() const
    {
        return time_;
    }


    inline void TimeManager::SetStaticOrDynamic(StaticOrDynamic value)
    {
        static_or_dynamic_ = value;
    }


    inline StaticOrDynamic TimeManager::GetStaticOrDynamic() const
    {
        return static_or_dynamic_;
    }


    inline double& TimeManager::GetNonCstTime()
    {
        return time_;
    }


    inline void TimeManager::SetTime(double time)
    {
        time_ = time;
    }


    inline void TimeManager::IncrementNtimeModified()
    {
        SetStaticOrDynamic(StaticOrDynamic::dynamic_);
        ++Ntime_modified_;
    }


    inline double TimeManager::GetInverseTimeStep() const noexcept
    {
        const double time_step = GetTimeStep();
        assert(time_step > 0.);
        return 1. / time_step;
    }


    inline const std::string& TimeManager::GetTimeIterationFile() const noexcept
    {
        assert(!time_iteration_file_.empty());
        assert(FilesystemNS::File::DoExist(time_iteration_file_));
        return time_iteration_file_;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
