/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_SPATIAL_LUA_FUNCTION_HPP_
#define MOREFEM_x_CORE_x_SPATIAL_LUA_FUNCTION_HPP_

#include "Utilities/InputData/LuaFunction.hpp"


namespace MoReFEM
{


    //! Convenient alias to a \a LuaFunction which takes three parameters standing for spatial coordinates and returns
    //! a double.
    using spatial_lua_function = Utilities::InputDataNS::LuaFunction<double(double, double, double)>;


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_SPATIAL_LUA_FUNCTION_HPP_
