//! \file
//
//
//  TypeEnum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HPP_
#define MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HPP_

#include <array>
#include <string>


namespace MoReFEM::ParameterNS
{


    /*!
     * \class doxygen_hide_tparam_parameter_type
     *
     * \tparam TypeT Type of the parameter (real, vector, matrix).
     */


    //! Type of the parameter to build.
    enum class Type
    {
        scalar,
        vector,
        matrix
    };


    /*!
     * \brief Returns a name as a string that describes the type considered.
     *
     * \note This function is used to populate the field of the Traits class in Parameter library.
     *
     * \tparam TypeT \a Type for which we want a string moniker.
     *
     * \return String moniker associated to \a TypeT.
     */
    template<Type TypeT>
    const std::string& Name();


} // namespace MoReFEM::ParameterNS


#include "Core/Parameter/TypeEnum.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HPP_
