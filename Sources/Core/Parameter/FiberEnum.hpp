//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_
#define MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_


namespace MoReFEM::FiberNS
{


    //! Type of the input data read in the fiber file to instantiate the fiber parameter.
    enum class AtNodeOrAtQuadPt
    {
        at_node,
        at_quad_pt,
        irrelevant
    };


} // namespace MoReFEM::FiberNS

#endif // MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_
