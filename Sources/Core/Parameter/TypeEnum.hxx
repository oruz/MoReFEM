//! \file
//
//
//  TypeEnum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_
#define MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_

// IWYU pragma: private, include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::ParameterNS
{


    template<Type TypeT>
    const std::string& Name()
    {
        static std::array<std::string, 3ul> name_list{ { "scalar", "vector", "matrix" } };

        switch (TypeT)
        {
        case Type::scalar:
            return name_list[0];
        case Type::vector:
            return name_list[1];
        case Type::matrix:
            return name_list[2];
        }
    }


} // namespace MoReFEM::ParameterNS


#endif // MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_
