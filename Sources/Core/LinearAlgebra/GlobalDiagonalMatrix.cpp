/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Jan 2016 14:47:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hpp"


namespace MoReFEM
{
    class NumberingSubset;
}


namespace MoReFEM
{


    GlobalDiagonalMatrix::~GlobalDiagonalMatrix() = default;


    GlobalDiagonalMatrix::GlobalDiagonalMatrix(const NumberingSubset& numbering_subset)
    : parent(numbering_subset, numbering_subset)
    { }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
