/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 27 Apr 2015 09:33:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef NDEBUG
#include <iostream>
#endif // NDEBUG

#include <cassert>
#include <string_view>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    GlobalMatrix::~GlobalMatrix() = default;


    GlobalMatrix::GlobalMatrix(const NumberingSubset& row_numbering_subset, const NumberingSubset& col_numbering_subset)
    : Crtp::NumberingSubsetForMatrix<GlobalMatrix>(row_numbering_subset, col_numbering_subset)
    { }


    GlobalMatrix::GlobalMatrix(const GlobalMatrix& rhs) : petsc_parent(rhs), numbering_subset_parent(rhs)
    { }


    void Swap(GlobalMatrix& A, GlobalMatrix& B)
    {
        // We swap the content of two matrices that share the same numbering subsets.
        assert(A.GetColNumberingSubset() == B.GetColNumberingSubset());
        assert(A.GetRowNumberingSubset() == B.GetRowNumberingSubset());

        using parent = GlobalMatrix::petsc_parent;

        Swap(static_cast<parent&>(A), static_cast<parent&>(B));
    }


#ifndef NDEBUG
    void AssertSameNumberingSubset(const GlobalMatrix& matrix1, const GlobalMatrix& matrix2)
    {
        assert(matrix1.GetRowNumberingSubset() == matrix2.GetRowNumberingSubset());
        assert(matrix1.GetColNumberingSubset() == matrix2.GetColNumberingSubset());
    }


    void PrintNumberingSubset(std::string_view matrix_name, const GlobalMatrix& matrix)
    {
        std::cout << "Numbering subsets for matrix '" << matrix_name << "': row -> "
                  << matrix.GetRowNumberingSubset().GetUniqueId() << " and col -> "
                  << matrix.GetColNumberingSubset().GetUniqueId() << std::endl;
    }
#endif // NDEBUG


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
