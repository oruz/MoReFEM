/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Jan 2016 14:47:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_
#define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_

#include <memory>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class NumberingSubset; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Specific case of GlobalMatrix which is square and diagonal.
     *
     * \internal <b><tt>[internal]</tt></b> Actually there is not much here that signes the diagonal structure of the
     * matrix, save the fact only one numbering subset is used for both rows and columns. However,
     * AllocateGlobalMatrix() provides an overload for this type that allocates only values on the diagonal.
     * \endinternal
     */
    class GlobalDiagonalMatrix final : public GlobalMatrix
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalDiagonalMatrix;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to direct parent.
        using parent = GlobalMatrix;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] numbering_subset \a NumberingSubset to use to define the numbering of rows and columns of the
        //! diagonal matrix.
        explicit GlobalDiagonalMatrix(const NumberingSubset& numbering_subset);

        //! Destructor.
        ~GlobalDiagonalMatrix() override;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalDiagonalMatrix(const GlobalDiagonalMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalDiagonalMatrix(GlobalDiagonalMatrix&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalDiagonalMatrix& operator=(const GlobalDiagonalMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalDiagonalMatrix& operator=(GlobalDiagonalMatrix&& rhs) = delete;

        ///@}
    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HPP_
