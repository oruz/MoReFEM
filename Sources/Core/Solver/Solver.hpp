/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
#define MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_

#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: export

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    //! Whether a non linear solver might be used in the variational formulation
    enum class enable_non_linear_solver
    {
        no,
        yes
    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
