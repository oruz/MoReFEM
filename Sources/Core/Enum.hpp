/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Jan 2013 11:55:30 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_ENUM_HPP_
#define MOREFEM_x_CORE_x_ENUM_HPP_

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    //! Very high value to use in penalization schemes.
    constexpr double penalisation_very_high_value = 1.e20;


    /*!
     * \brief Whether models should compute the very first processor-wise local2global array for each
     * LocalFEltSpace/numbering subset combination found in FEltSpace.
     *
     */
    enum class DoConsiderProcessorWiseLocal2Global
    {
        no,
        yes
    };


    /*!
     * \brief If DoConsiderProcessorWiseLocal2Global is yes, each operator might decide whether it actually requires
     * the processor-wise local2global array or not.
     */
    enum class DoComputeProcessorWiseLocal2Global
    {
        no,
        yes
    };

    //! \todo #900 Temporary trick to choose default quadrature rules.
    constexpr const std::size_t DEFAULT_DEGREE_OF_EXACTNESS = 5;

    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_ENUM_HPP_
