/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    // clang-format on
    constexpr bool MoReFEMData<InputDataT, ProgramTypeT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>::
        HasParallelismField()
    {
        return InputDataT::template Find<InputDataNS::Parallelism>();
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::MoReFEMData(int argc, char** argv)
    // clang-format on
    {
        Internal::PetscNS::RAII::CreateOrGetInstance(__FILE__, __LINE__, argc, argv);

        const auto ptr = std::is_same<AdditionalCommandLineArgumentsPolicyT, std::nullptr_t>() ? nullptr : this;

        try
        {

            Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory;

            std::string input_data_file =
                Internal::MoReFEMDataNS::ParseCommandLine<ProgramTypeT, AdditionalCommandLineArgumentsPolicyT>(
                    argc, argv, do_overwrite_directory, ptr);

            decltype(auto) mpi = GetMpi();

            Internal::MoReFEMDataNS::DefineEnvironmentVariable(mpi);

            if (mpi.IsRootProcessor())
                // Stops the program after creating it for root processor.
                Internal::MoReFEMDataNS::CreateIfNotExisting<typename InputDataT::Tuple>(input_data_file);

            Internal::MoReFEMDataNS::CheckExistingForAllRank(mpi, input_data_file);

            // We can be here only if the file exists...
            input_data_ = std::make_unique<InputDataT>(input_data_file, mpi, DoTrackUnusedFieldsT);

            namespace ipl = Utilities::InputDataNS;

            {
                FilesystemNS::behaviour directory_behaviour{ FilesystemNS::behaviour::ignore };

                if constexpr (ProgramTypeT == program_type::model)
                    directory_behaviour = do_overwrite_directory == Internal::MoReFEMDataNS::overwrite_directory::yes
                                              ? FilesystemNS::behaviour::overwrite
                                              : FilesystemNS::behaviour::ask;
                else if constexpr (ProgramTypeT == program_type::test)
                    directory_behaviour = FilesystemNS::behaviour::overwrite;
                else if constexpr (ProgramTypeT == program_type::post_processing)
                    directory_behaviour = FilesystemNS::behaviour::read;

                static_cast<void>(directory_behaviour);

                if constexpr (InputDataT::template Find<InputDataNS::TimeManager>())
                {
                    static_assert(InputDataT::template Find<InputDataNS::Result::OutputDirectory>(),
                                  "TimeManager requires the existence of the output directory.");
                }


                if constexpr (ProgramTypeT != program_type::update_lua_file)
                {
                    if constexpr (InputDataT::template Find<InputDataNS::Result::OutputDirectory>())
                    {
                        using Result = InputDataNS::Result;
                        std::string path = ipl::Extract<Result::OutputDirectory>::Path(*input_data_);

                        result_directory_ = std::make_unique<FilesystemNS::Directory>(
                            mpi, path, directory_behaviour, __FILE__, __LINE__);

                        InitTimeKeepLog(GetResultDirectory());
                    }
                }

                if constexpr (InputDataT::template Find<InputDataNS::Result::BinaryOutput>())
                {
                    const auto& binary_output = ipl::Extract<InputDataNS::Result::BinaryOutput>::Value(*input_data_);
                    Utilities::OutputFormat::CreateOrGetInstance(__FILE__, __LINE__, binary_output);
                }

                {
                    // Parallelism is an optional field: it might not be present in the Lua file (for tests for instance
                    // it is not meaningful).
                    if constexpr (HasParallelismField())
                        parallelism_ = std::make_unique<Internal::Parallelism>(mpi, *input_data_, directory_behaviour);
                }
            } // namespace Utilities::InputDataNS;

            mpi.Barrier();

            if constexpr (InputDataT::template Find<InputDataNS::Solid::CheckInvertedElements>())
            {
                const auto& do_check_inverted_elements =
                    ipl::Extract<InputDataNS::Solid::CheckInvertedElements>::Value(*input_data_);

                MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements ::CreateOrGetInstance(
                    __FILE__, __LINE__, do_check_inverted_elements);
            } else
            {
                // Create it nonetheless - but will trigger an exception if the value is called.
                MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements::CreateOrGetInstance(__FILE__, __LINE__);
            }
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::~MoReFEMData()
    // clang-format on
    {

        // Flush all the standard outputs. Catch all exceptions: exceptions in destructor are naughty!
        const auto& mpi = GetMpi();

        try
        {
            Wrappers::Petsc::SynchronizedFlush(mpi, stdout, __FILE__, __LINE__);
            Wrappers::Petsc::SynchronizedFlush(mpi, stderr, __FILE__, __LINE__);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Untimely exception caught in ~MoReFEMData(): " << e.what() << std::endl;
            assert(false && "No exception in destructors!");
        }
        catch (...)
        {
            assert(false && "No exception in destructors!");
        }

        input_data_ = nullptr;

        mpi.Barrier(); // to better handle the possible MpiAbort() on one of the branch...
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    inline const Wrappers::Mpi&
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetMpi() const noexcept
    // clang-format on
    {
        decltype(auto) raii = Internal::PetscNS::RAII::GetInstance(__FILE__, __LINE__);
        return raii.GetMpi();
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const InputDataT&
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetInputData() const noexcept
    // clang-format on
    {
        assert(!(!input_data_));
        return *input_data_;
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const FilesystemNS::Directory&
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetResultDirectory() const noexcept
    // clang-format on
    {
        static_assert(ProgramTypeT != program_type::update_lua_file,
                      "The result directory is not used in this executable and thus its status is unknown, so it's "
                      "bbest to forbid it to be queried at all.");

        static_assert(
            InputDataT::template Find<InputDataNS::Result::OutputDirectory>(),
            "The call to this method assumes a field Result::OutputDirectory was present in InputData tuple.");

        assert(!(!result_directory_));
        return *result_directory_;
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism*
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelismPtr() const noexcept
    // clang-format on
    {
        if (!parallelism_)
            return nullptr;

        return parallelism_.get();
    }


    // clang-format off
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism&
    MoReFEMData
    <
        InputDataT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelism() const noexcept
    // clang-format on
    {
        assert(!(!parallelism_));
        return *parallelism_;
    }


    template<class MoReFEMDataT>
    void PrecomputeExit(const MoReFEMDataT& morefem_data)
    {
        const auto parallelism_ptr = morefem_data.GetParallelismPtr();

        if (!(!parallelism_ptr))
        {
            switch (parallelism_ptr->GetParallelismStrategy())
            {
            case Advanced::parallelism_strategy::precompute:
            {
                if (morefem_data.GetMpi().IsRootProcessor())
                    std::cout << "The parallelism elements have been precomputed; the program will now end."
                              << std::endl;

                throw ExceptionNS::GracefulExit(__FILE__, __LINE__);
            }
            case Advanced::parallelism_strategy::run_from_preprocessed:
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::parallel_no_write:
                break;
            case Advanced::parallelism_strategy::none:
            {
                assert(false && "Should not be possible (if so parallelism_ptr should have been nullptr).");
                exit(EXIT_FAILURE);
            }
            }
        }
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
