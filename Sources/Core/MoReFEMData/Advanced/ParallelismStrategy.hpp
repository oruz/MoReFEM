//! \file
//
//
//  ParallelismStrategy.hpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_


namespace MoReFEM::Advanced
{


    //! Enum class which specifies the possible behaviour related to parallelism.
    enum class parallelism_strategy
    {
        none,
        precompute,
        parallel_no_write,
        parallel,
        run_from_preprocessed
    };


} // namespace MoReFEM::Advanced

#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_
