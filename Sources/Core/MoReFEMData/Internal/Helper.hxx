//! \file
//
//
//  Helper.hxx
//  MoReFEM
//
//  Created by sebastien on 30/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Internal/Helper.hpp"

#include <cassert>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Base.hpp"

#include "Core/MoReFEMData/Enum.hpp"

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    template<program_type ProgramTypeT, class AdditionalCommandLineArgumentsPolicyT>
    std::string ParseCommandLine(int argc,
                                 char** argv,
                                 overwrite_directory& do_overwrite_directory,
                                 AdditionalCommandLineArgumentsPolicyT* additional)
    {
        std::string ret;

        try
        {
            TCLAP::CmdLine cmd("Command description message");
            cmd.setExceptionHandling(false);

            TCLAP::ValueArg<std::string> input_data_file_arg(
                "i", "input_data", "Input data file (Lua)", true, "", "string", cmd);

            TCLAP::MultiArg<::MoReFEM::Wrappers::Tclap::StringPair> env_arg(
                "e", "env", "environment_variable", false, "string=string", cmd);

            std::unique_ptr<TCLAP::SwitchArg> overwrite_directory_arg = nullptr;

            if constexpr (ProgramTypeT == program_type::model)
            {
                overwrite_directory_arg =
                    std::make_unique<TCLAP::SwitchArg>("",
                                                       "overwrite_directory",
                                                       "If this flag is set, the output directory will be removed "
                                                       "silently "
                                                       "if it already exists.",
                                                       cmd,
                                                       false);
            }

            if constexpr (!std::is_same<AdditionalCommandLineArgumentsPolicyT, std::false_type>())
            {
                assert(!(!additional));
                additional->Add(cmd);
            } else
                static_cast<void>(additional);

            cmd.parse(argc, argv);

            if constexpr (ProgramTypeT == program_type::model)
            {
                assert(!(!overwrite_directory_arg));
                do_overwrite_directory =
                    overwrite_directory_arg->getValue() ? overwrite_directory::yes : overwrite_directory::no;
            }

            decltype(auto) env_var_list = env_arg.getValue();

            auto& environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

            for (const auto& pair : env_var_list)
                environment.SetEnvironmentVariable(pair.GetValue(), __FILE__, __LINE__);

            ret = input_data_file_arg.getValue();

            return ret;
        }
        catch (TCLAP::ArgException& e) // catch any exceptions
        {
            std::ostringstream oconv;
            oconv << "Caught command line exception " << e.error() << " for argument " << e.argId() << std::endl;
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


    template<class TupleT>
    void CreateIfNotExisting(const std::string& input_data_file)
    {
        if (!::MoReFEM::FilesystemNS::File::DoExist(input_data_file))
        {
            ::MoReFEM::Utilities::InputDataNS::CreateDefaultInputFile<TupleT>(input_data_file);

            std::cout << '\n'
                      << input_data_file
                      << " wasn't existing and has just been created on root processor; "
                         "please edit it and then copy it onto all machines intended to run the code in parallel."
                      << std::endl;

            throw ExceptionNS::GracefulExit(__FILE__, __LINE__);
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_
