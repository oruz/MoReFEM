//! \file
//
//
//  Parallelism.hxx
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Internal/Parallelism.hpp"

#include <cassert>
#include <iosfwd>
#include <memory>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"


namespace MoReFEM::Internal
{


    template<class InputDataT>
    Parallelism::Parallelism(const ::MoReFEM::Wrappers::Mpi& mpi,
                             const InputDataT& input_data,
                             ::MoReFEM::FilesystemNS::behaviour behaviour)
    {
        namespace ipl = Utilities::InputDataNS;

        using Parallelism = ::MoReFEM::InputDataNS::Parallelism;

        SetParallelismStrategy(ipl::Extract<Parallelism::Policy>::Value(input_data));

        switch (parallelism_strategy_)
        {
        case ::MoReFEM::Advanced::parallelism_strategy::precompute:
        case ::MoReFEM::Advanced::parallelism_strategy::parallel:
        {
            using directory = ::MoReFEM::InputDataNS::Parallelism::Directory;
            std::string path = ipl::Extract<directory>::Path(input_data);

            directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(mpi, path, behaviour, __FILE__, __LINE__);

            break;
        }
        case ::MoReFEM::Advanced::parallelism_strategy::parallel_no_write:
            break;
        case ::MoReFEM::Advanced::parallelism_strategy::run_from_preprocessed:
        {
            using directory = ::MoReFEM::InputDataNS::Parallelism::Directory;
            std::string path = ipl::Extract<directory>::Path(input_data);

            directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                mpi, path, ::MoReFEM::FilesystemNS::behaviour::read, __FILE__, __LINE__);

            break;
        }
        case ::MoReFEM::Advanced::parallelism_strategy::none:
            break;
        }
    }


    inline const ::MoReFEM::FilesystemNS::Directory& Parallelism::GetDirectory() const noexcept
    {
        assert(!(!directory_));
        return *directory_;
    }


    inline ::MoReFEM::Advanced::parallelism_strategy Parallelism::GetParallelismStrategy() const noexcept
    {
        assert(parallelism_strategy_ != ::MoReFEM::Advanced::parallelism_strategy::none && "Must be initialized!");
        return parallelism_strategy_;
    }


} // namespace MoReFEM::Internal


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HXX_
