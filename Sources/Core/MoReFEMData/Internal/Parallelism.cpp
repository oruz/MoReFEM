//! \file
//
//
//  Parallelism.cpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
#include <cstdlib>
#include <string>
#include <string_view>

#include "Core/MoReFEMData/Internal/Parallelism.hpp"


namespace MoReFEM::Internal
{


    void Parallelism::SetParallelismStrategy(std::string_view policy)
    {
        if (policy == "Precompute")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::precompute;
        else if (policy == "ParallelNoWrite")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::parallel_no_write;
        else if (policy == "Parallel")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::parallel;
        else if (policy == "RunFromPreprocessed")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::run_from_preprocessed;
        else
        {
            assert(false && "Constraint in Lua file should have ruled out this case sooner!");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal
