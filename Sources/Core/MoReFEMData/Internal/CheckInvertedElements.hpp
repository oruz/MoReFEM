//
//  CheckInvertedElements.hpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 10/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HPP_


#include <iosfwd>
#include <memory>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp"  // IWYU pragma: export

#include "Core/MoReFEMData/Internal/EnumInvertedElements.hpp"


/// \addtogroup FiniteElementGroup
///@{


namespace MoReFEM::Internal::MoReFEMDataNS
{


    /*!
     * \brief Enables a check for the validity of finite elements during computations. It ensures that
     * the Jacobian of the mapping from the reference configuration to the deformed one remains positive.
     * If it is negative at some point, an exception is raised.
     *
     * This singleton acts as a global variable in order to store the boolean value related to the check
     * for inverted finite elements. It's only purpose is to propagate this information throughout the whole code.
     * \tparam  inverted_elements_check_policy_ is set to no_check by default, which makes for faster runs.
     * If is set to do_check (through Solid::CheckInvertedElements) then the test for the positveness of the Jacobian is
     * done at each quadrature point. One should note that this test is only a necessary condition for the validity of
     * elements but it is not sufficient for elements of order higher or equal than 2.
     *
     */
    class CheckInvertedElements final : public Utilities::Singleton<CheckInvertedElements>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = CheckInvertedElements;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] do_check_inverted_elements True to enable the check for inverted elements.
         */
        explicit CheckInvertedElements(bool do_check_inverted_elements);

        /*!
         * \brief Constructor when no Solid field in input data file.
         *
         * This case indicates no field was specified at all in the input data file; this probably means the model is
         * ill-formed.
         */
        explicit CheckInvertedElements() = default;

        //! Destructor.
        virtual ~CheckInvertedElements() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CheckInvertedElements>;

        //! Name of the class.
        static const std::string& ClassName();


        ///@}


      public:
        //! Whether we do check for inverted elements during computation.
        check_inverted_elements_policy DoCheckInvertedElements() const;

      private:
        //! Policy to define whether the test on the inverted elements is done or not.
        check_inverted_elements_policy check_inverted_elements_policy_ = check_inverted_elements_policy::unspecified;
    };


} // namespace MoReFEM::Internal::MoReFEMDataNS


/// @} // addtogroup FiniteElementGroup


#include "Core/MoReFEMData/Internal/CheckInvertedElements.hxx" // IWYU pragma: export
#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HPP_
