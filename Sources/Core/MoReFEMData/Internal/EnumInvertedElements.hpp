//
//  EnumInvertedElements.h
//  MoReFEM
//
//  Created by Jerôme Diaz on 10/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ENUM_INVERTED_ELEMENTS_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ENUM_INVERTED_ELEMENTS_HPP_


namespace MoReFEM
{


    //! Enum class to choose between doing a (relatively) expensive check on the Jacobian of the mapping from reference
    //! to deformed configuration
    enum class check_inverted_elements_policy
    {
        from_input_data, // takes the value specified in the input data file.
        do_check,
        no_check,
        unspecified // if there was no Solid field in the input data file.
    };


} // namespace MoReFEM


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ENUM_INVERTED_ELEMENTS_HPP_
