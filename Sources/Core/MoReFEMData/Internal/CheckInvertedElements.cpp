//
//  CheckInvertedElements.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 10/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"
#include "Core/MoReFEMData/Internal/EnumInvertedElements.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    CheckInvertedElements::~CheckInvertedElements() = default;


    CheckInvertedElements::CheckInvertedElements(bool do_check_inverted_elements)
    : check_inverted_elements_policy_(do_check_inverted_elements ? check_inverted_elements_policy::do_check
                                                                 : check_inverted_elements_policy::no_check)
    { }


    const std::string& CheckInvertedElements::ClassName()
    {
        static std::string ret("CheckInvertedElements");
        return ret;
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS
