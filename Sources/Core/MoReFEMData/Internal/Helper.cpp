//! \file
//
//
//  Helper.cpp
//  MoReFEM
//
//  Created by sebastien on 30/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/Internal/Helper.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    void DefineEnvironmentVariable(const ::MoReFEM::Wrappers::Mpi& mpi)
    {
        const char* const result_dir = "MOREFEM_RESULT_DIR";

        auto& environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        if (!environment.DoExist(result_dir))
        {
            const char* const default_value("/Volumes/Data/${USER}/MoReFEM/Results");

            environment.SetEnvironmentVariable(std::make_pair("MOREFEM_RESULT_DIR", default_value), __FILE__, __LINE__);

            std::cout << "[WARNING] Environment variable '" << result_dir << "' was not defined; default value '"
                      << default_value
                      << "' has therefore be provided. This environment variable may appear in mesh "
                         "directory defined in input data file; if not it is in fact unused."
                      << std::endl;
        }

        const char* const morefem_dir = "MOREFEM_ROOT";

        if (!environment.DoExist(morefem_dir))
        {
            const char* const default_value("${HOME}/Codes/MoReFEM/CoreLibrary");

            environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", default_value), __FILE__, __LINE__);

            std::cout << "[WARNING] Environment variable '" << morefem_dir << "' was not defined; default value '"
                      << default_value
                      << "' has therefore be provided. This environment variable may appear in output "
                         "directory defined in input data file; if not it is in fact unused."
                      << std::endl;
        }

        std::string start_time = "MOREFEM_START_TIME";

        if (!environment.DoExist(start_time))
        {
            environment.SetEnvironmentVariable(
                std::make_pair(start_time, ::MoReFEM::Utilities::Now(mpi)), __FILE__, __LINE__);
            mpi.Barrier();
        }
    }


    void CheckExistingForAllRank(const ::MoReFEM::Wrappers::Mpi& mpi, const std::string& input_data_file)
    {
        // Check the input data file can be found on each processor.
        const bool do_file_exist_for_rank = FilesystemNS::File::DoExist(input_data_file);
        std::vector<bool> sent_data{ do_file_exist_for_rank };
        std::vector<bool> gathered_data;

        mpi.Gather(sent_data, gathered_data);

        // Root processor sent true if all files exist for all ranks, false otherwise.
        // Beware (#461): it doesn't check here the file is consistent!
        bool do_file_exist_for_all_ranks{ true };
        std::vector<std::size_t> rank_without_file;

        if (mpi.IsRootProcessor())
        {
            for (std::size_t i = 0ul, Nproc = gathered_data.size(); i < Nproc; ++i)
            {
                if (!gathered_data[i])
                    rank_without_file.push_back(i);
            }

            if (!rank_without_file.empty())
                do_file_exist_for_all_ranks = false;
        }

        mpi.Broadcast(do_file_exist_for_all_ranks);

        if (mpi.IsRootProcessor())
        {

            if (!rank_without_file.empty())
            {
                std::ostringstream oconv;
                oconv << "The following ranks couldn't find the input data file: ";
                Utilities::PrintContainer<>::Do(rank_without_file,
                                                oconv,
                                                ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                ::MoReFEM::PrintNS::Delimiter::opener(""),
                                                ::MoReFEM::PrintNS::Delimiter::closer(".\n"));

                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        } else
        {
            if (do_file_exist_for_all_ranks == false)
                throw Exception("Input data file not found for one or more rank; see root processor "
                                "exception for more details!",
                                __FILE__,
                                __LINE__);
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS
