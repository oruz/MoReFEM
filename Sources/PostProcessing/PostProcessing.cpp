/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <cassert>
#include <cstdlib>
#include <set>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Internal/GetRankDirectory.hpp"
#include "Utilities/OutputFormat/OutputFormat.hpp"
#include "Utilities/OutputFormat/ReadBinaryFile.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/Exceptions/Exception.hpp"
#include "PostProcessing/PostProcessing.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        PostProcessing::PostProcessing(const FilesystemNS::Directory& data_directory,
                                       const std::vector<std::size_t>& numbering_subset_id_list,
                                       const Mesh& mesh)
        : data_directory_(data_directory), mesh_(mesh), Nprocessor_(NumericNS::UninitializedIndex<std::size_t>()),
          interface_file_(nullptr), unknown_file_(nullptr), time_iteration_file_(nullptr),
          numbering_subset_id_list_(numbering_subset_id_list)
        {
            data_directory_.SetBehaviour(FilesystemNS::behaviour::read);

            //! Read the unknown file and store its content into the class.
            ReadUnknownFile();

            //! Read the time iteration file and store its content into the class.
            ReadTimeIterationFile();

            //! Read how many processors were involved in the simulation.
            ReadNprocessor();

            // Read the different files.
            ReadInterfaceFiles();

            //! Read the dof files (one per processor) and store its content into the class.
            ReadDofFiles();
        }


        std::vector<double> LoadVector(const std::string& file)
        {
            std::vector<double> ret;

            const auto binary_or_ascii_choice =
                Utilities::OutputFormat::GetInstance(__FILE__, __LINE__).IsBinaryOutput();

            switch (binary_or_ascii_choice)
            {
            case binary_or_ascii::binary:
            {
                return Advanced::ReadSimpleBinaryFile(file, __FILE__, __LINE__);
            }
            case binary_or_ascii::ascii:
            {
                std::ifstream stream;
                FilesystemNS::File::Read(stream, file, __FILE__, __LINE__);

                std::string line;

                while (getline(stream, line))
                    ret.push_back(std::stod(line));
                break;
            }
            case binary_or_ascii::from_input_data:
            {
                assert(false && "Should not happen!");
                exit(EXIT_FAILURE);
            }
            }

            return ret;
        }


        void PostProcessing::ReadInterfaceFiles()
        {
            assert(!interface_file_);

            assert(GetDataDirectory().GetBehaviour() == FilesystemNS::behaviour::read);
            FilesystemNS::Directory root_dir(
                GetDataDirectory(), "..", __FILE__, __LINE__, FilesystemNS::behaviour::read);

            // Assumes number of processors has been retrieved earlier; constructor is built to ensure this is the
            // case.
            const auto Nprocessor = this->Nprocessor();

            const std::string mesh_id = std::to_string(GetMesh().GetUniqueId());

            InterfaceFile::vector_unique_ptr interface_file_per_rank;

            for (auto rank = 0ul; rank < Nprocessor; ++rank)
            {
                FilesystemNS::Directory mesh_dir(root_dir, "Rank_" + std::to_string(rank), __FILE__, __LINE__);

                mesh_dir.AddSubdirectory("Mesh_" + mesh_id, __FILE__, __LINE__);

                const std::string input_file = mesh_dir.AddFile("interfaces.hhdata");

                interface_file_per_rank.emplace_back(std::make_unique<InterfaceFile>(input_file));
            }

            interface_file_ = std::make_unique<InterfaceFile>(std::move(interface_file_per_rank));
        }


        void PostProcessing::ReadUnknownFile()
        {
            std::string input_file(GetDataDirectory());
            input_file += "/unknowns.hhdata";

            assert(!unknown_file_);
            unknown_file_ = std::make_unique<UnknownInformationFile>(input_file);
        }


        void PostProcessing::ReadTimeIterationFile()
        {
            decltype(auto) data_dir = GetDataDirectory();

            // First check whether a 'filtered_time_iteration.hhdata' is present (it would have been generated through
            // the PostProcessing/FilterTimeIteration script). If found, use it.
            std::string input_file = data_dir.AddFile("filtered_time_iteration.hhdata");

            assert(!time_iteration_file_);

            if (FilesystemNS::File::DoExist(input_file))
                std::cout << "[Time iterations] Using the filtered_time_iteration.hhdata file." << std::endl;
            else
                input_file = data_dir.AddFile("time_iteration.hhdata");

            time_iteration_file_ = std::make_unique<TimeIterationFile>(input_file);
        }


        void PostProcessing::ReadNprocessor()
        {
            assert(Nprocessor_ == NumericNS::UninitializedIndex<std::size_t>());
            std::ifstream in;

            FilesystemNS::File::Read(in, GetDataDirectory().AddFile("mpi.hhdata"), __FILE__, __LINE__);

            std::string line;
            getline(in, line);

            const std::string sequence("Nprocessor:");

            if (!Utilities::String::StartsWith(line, sequence))
                throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

            line.erase(0, sequence.size());

            Nprocessor_ = std::stoul(line);
        }


        void PostProcessing::ReadDofFiles()
        {
            DofInformationFile::vector_unique_ptr dof_information_file_list_for_numbering_subset;

            const std::size_t Nproc = Nprocessor();
            dof_information_file_list_for_numbering_subset.reserve(Nproc);

            assert(Ndof_ == 0ul);

            const auto& interface_file = GetInterfaceData();

            decltype(auto) numbering_subset_id_list = GetNumberingSubsetIdList();

            auto set_numbering_subset_id_list =
                std::set<std::size_t>(numbering_subset_id_list.cbegin(), numbering_subset_id_list.cend());

            for (const auto& numbering_subset_id : set_numbering_subset_id_list)
            {
                std::ostringstream oconv;

                for (std::size_t rank = 0; rank < Nproc; ++rank)
                {
                    decltype(auto) numbering_subset_directory =
                        GetNumberingSubsetDirectory(static_cast<std::size_t>(rank), numbering_subset_id);

                    const auto dof_infos_data_file = numbering_subset_directory.AddFile("dof_information.hhdata");

                    auto&& dof_list_ptr =
                        std::make_unique<DofInformationFile>(rank, dof_infos_data_file, interface_file);
                    Ndof_ += dof_list_ptr->Ndof();

                    dof_information_file_list_for_numbering_subset.emplace_back(std::move(dof_list_ptr));
                }

                assert(dof_information_file_list_for_numbering_subset.size() == Nproc);

                using pair_type = std::pair<std::size_t, DofInformationFile::vector_unique_ptr>;

                pair_type&& pair = { numbering_subset_id, std::move(dof_information_file_list_for_numbering_subset) };

                dof_file_per_processor_list_per_numbering_subset_.insert(std::move(pair));
            }
        }


        const DofInformationFile& PostProcessing::GetDofFile(const std::size_t numbering_subset_id,
                                                             const std::size_t processor) const
        {
            assert(processor < Nprocessor());

            auto it = dof_file_per_processor_list_per_numbering_subset_.find(numbering_subset_id);
            assert(it != dof_file_per_processor_list_per_numbering_subset_.cend());

            const auto& dof_file_per_processor_list = it->second;

            assert(Nprocessor() == dof_file_per_processor_list.size());
            return *dof_file_per_processor_list[processor];
        }


        FilesystemNS::Directory PostProcessing::GetNumberingSubsetDirectory(const std::size_t rank,
                                                                            const std::size_t numbering_subset_id) const
        {
            decltype(auto) data_directory = GetDataDirectory();

            FilesystemNS::Directory rank_data_directory =
                Internal::FilesystemNS::GetRankDirectory(data_directory, rank, __FILE__, __LINE__);

            FilesystemNS::Directory mesh_dir(
                rank_data_directory, "Mesh_" + std::to_string(GetMesh().GetUniqueId()), __FILE__, __LINE__);

            FilesystemNS::Directory ret(
                mesh_dir, "NumberingSubset_" + std::to_string(numbering_subset_id), __FILE__, __LINE__);

            return ret;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
