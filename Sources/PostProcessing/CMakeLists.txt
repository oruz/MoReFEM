
target_sources(${MOREFEM_POST_PROCESSING}
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.cpp" / 

	#PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/ConvertLinearAlgebra/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/File/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/OutputFormat/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Data/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/RefineMeshQuadranglesSpectral/CMakeLists.txt)
#include(${CMAKE_CURRENT_LIST_DIR}/OutputDeformedMesh/CMakeLists.txt)

#add_executable(MoReFEMRefineMeshOutput ${CMAKE_CURRENT_LIST_DIR}/main_refined_mesh_output.cpp)
#target_link_libraries(MoReFEMRefineMeshOutput
#                      ${MOREFEM_POST_PROCESSING})


#add_executable(MoReFEMConvertMatrixToFreefem ${CMAKE_CURRENT_LIST_DIR}/main_convert_matrix_to_freefem.cpp)
#target_link_libraries(MoReFEMConvertMatrixToFreefem
#                      ${MOREFEM_POST_PROCESSING})


#morefem_install(MoReFEMRefineMeshOutput MoReFEMConvertMatrixToFreefem)
