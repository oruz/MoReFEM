/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HXX_
#define MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HXX_

// IWYU pragma: private, include "PostProcessing/File/DofInformationFile.hpp"

#include <cstddef> // IWYU pragma: keep

#include "PostProcessing/Data/DofInformation.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        inline std::size_t DofInformationFile::Ndof() const noexcept
        {
            return dof_information_list_.size();
        }


        inline const Data::DofInformation::vector_const_shared_ptr& DofInformationFile::GetDofList() const noexcept
        {
            return dof_information_list_;
        }


        inline std::size_t DofInformationFile::GetProcessor() const noexcept
        {
            return processor_;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HXX_
