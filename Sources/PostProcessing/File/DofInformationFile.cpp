/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/Exceptions/Exception.hpp"
#include "PostProcessing/File/DofInformationFile.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace // anonymous
        {


            using difference_type = std::string::iterator::difference_type;


            std::size_t ExtractNdof(std::string line)
            {
                const std::string first_line_key = "Ndof (processor_wise) = ";

                if (!Utilities::String::StartsWith(line, first_line_key))
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                line.erase(line.begin(), line.begin() + static_cast<difference_type>(first_line_key.size()));

                return (std::stoul(line));
            }


        } // namespace


        DofInformationFile::DofInformationFile(std::size_t processor,
                                               const std::string& input_file,
                                               const InterfaceFile& interface_file)
        : processor_(processor)
        {

            std::ifstream interface_stream;

            FilesystemNS::File::Read(interface_stream, input_file, __FILE__, __LINE__);

            std::string line;

            getline(interface_stream, line);
            const std::size_t Ndof = ExtractNdof(line);

            dof_information_list_.reserve(Ndof);

            while (getline(interface_stream, line))
            {
                // Ignore comment lines.
                if (Utilities::String::StartsWith(line, "#"))
                    continue;

                dof_information_list_.emplace_back(std::make_shared<Data::DofInformation>(line, interface_file));
            }

            if (Ndof != dof_information_list_.size())
                throw ExceptionNS::InvalidFormatInFile(input_file,
                                                       "the number of announced dofs is not the number "
                                                       "of dofs effectively read.",
                                                       __FILE__,
                                                       __LINE__);

            assert(Ndof == this->Ndof());
        }


        Data::DofInformation::vector_const_shared_ptr DofInformationFile ::GetDof(InterfaceNS::Nature nature,
                                                                                  const std::size_t interface_index,
                                                                                  const std::string& unknown) const
        {
            Data::DofInformation::vector_const_shared_ptr ret;

            auto match_condition =
                [nature, interface_index, &unknown](const Data::DofInformation::const_shared_ptr& dof_info_ptr)
            {
                assert(!(!dof_info_ptr));
                const auto& dof_info = *dof_info_ptr;


                return dof_info.GetInterfaceNature() == nature && dof_info.GetInterfaceIndex() == interface_index
                       && dof_info.GetUnknown() == unknown;
            };

            auto end = dof_information_list_.cend();

            auto it = std::find_if(dof_information_list_.cbegin(), end, match_condition);

            assert(it != end);

            ret.push_back(*it);

            // We make use of the fact components are stored together in the output format.
            while (++it != end && match_condition(*it))
                ret.push_back(*it);

            return ret;
        }


        const Data::DofInformation& DofInformationFile ::GetDof(InterfaceNS::Nature nature,
                                                                const std::size_t interface_index,
                                                                const std::string& unknown,
                                                                const Advanced::ComponentNS::index_type component) const
        {
            Data::DofInformation::vector_const_shared_ptr ret;

            auto match_condition = [nature, interface_index, &unknown, component](
                                       const Data::DofInformation::const_shared_ptr& dof_info_ptr)
            {
                assert(!(!dof_info_ptr));
                const auto& dof_info = *dof_info_ptr;

                return dof_info.GetInterfaceNature() == nature && dof_info.GetInterfaceIndex() == interface_index
                       && dof_info.GetUnknown() == unknown && dof_info.GetUnknownComponent() == component.Get();
            };

            auto end = dof_information_list_.cend();

            auto it = std::find_if(dof_information_list_.cbegin(), end, match_condition);
            assert(it != end);

            decltype(auto) dof_info_ptr = *it;
            assert(dof_info_ptr != nullptr);

            return *dof_info_ptr;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
