/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_
#define MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_

// IWYU pragma: private, include "PostProcessing/File/InterfaceFile.hpp"

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <utility>
#include <vector>

#include "Geometry/Interfaces/EnumInterface.hpp"

#include "PostProcessing/Data/Interface.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        template<InterfaceNS::Nature NatureT>
        std::size_t InterfaceFile::Ninterface() const
        {
            auto it = content_.find(NatureT);

            if (it == content_.cend())
                return 0ul;

            return it->second.size();
        }


        inline std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr>&
        InterfaceFile ::GetNonCstContent() noexcept
        {
            return content_;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_
