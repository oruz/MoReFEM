/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "PostProcessing/File/InterfaceFile.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        InterfaceFile::InterfaceFile(const std::string& input_file)
        {
            std::ifstream interface_stream;

            FilesystemNS::File::Read(interface_stream, input_file, __FILE__, __LINE__);

            std::string line;

            // The file is written so that vertices come first, then edges, faces and volume.
            InterfaceNS::Nature current_nature(InterfaceNS::Nature::vertex);

            Data::Interface::vector_unique_ptr current_interface_content;

            using difference_type = std::string::iterator::difference_type;

            while (getline(interface_stream, line))
            {
                // Ignore comment lines.
                if (Utilities::String::StartsWith(line, "#"))
                    continue;

                auto begin = line.cbegin();

                // The format of each line should be: *Interface* *n*;[*list of vertices defining the interface*]

                // Identify the first space. The word before must match an interface.
                {
                    const auto space_pos = line.find(' ');

                    if (space_pos == std::string::npos)
                    {
                        std::ostringstream oconv;
                        oconv << "Invalid format for file '" << input_file << "' in line " << line
                              << ": no space found!";
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }

                    std::string interface_name(line, 0, space_pos);
                    InterfaceNS::Nature interface_nature = InterfaceNS::GetNature(interface_name);

                    if (interface_nature != current_nature)
                    {
                        assert(interface_nature > current_nature);
                        content_.emplace(std::make_pair(current_nature, std::move(current_interface_content)));
                        current_nature = interface_nature;
                        current_interface_content.clear();
                    }

                    const auto semicolon_pos = line.find(';', space_pos + 1);

                    if (semicolon_pos == std::string::npos)
                    {
                        std::ostringstream oconv;
                        oconv << "Invalid format for file '" << input_file << "' in line " << line
                              << ": no semicolon found!";
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }

                    std::string str_interface_index(begin + static_cast<difference_type>(space_pos + 1ul),
                                                    begin + static_cast<difference_type>(semicolon_pos));

                    const std::size_t interface_index = static_cast<std::size_t>(std::stoi(str_interface_index));

                    const auto left_bracket_pos = line.find('[', semicolon_pos + 1);

                    if (left_bracket_pos == std::string::npos)
                    {
                        std::ostringstream oconv;
                        oconv << "Invalid format for file '" << input_file << "' in line " << line << ": no [ found!";
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }

                    const auto right_bracket_pos = line.find(']', left_bracket_pos + 1);

                    if (right_bracket_pos == std::string::npos)
                    {
                        std::ostringstream oconv;
                        oconv << "Invalid format for file '" << input_file << "' in line " << line << ": no ] found!";
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }

                    std::string str_coord_index(begin + static_cast<difference_type>(left_bracket_pos + 1ul),
                                                begin + static_cast<difference_type>(right_bracket_pos));

                    std::vector<CoordsNS::processor_wise_position> coords_on_vertex_in_interface;
                    {
                        std::istringstream iconv(str_coord_index);
                        std::size_t buf;

                        while (!iconv.eof())
                        {
                            iconv >> buf;

                            coords_on_vertex_in_interface.push_back(CoordsNS::processor_wise_position(buf));

                            if (iconv.fail())
                            {
                                std::ostringstream oconv;
                                oconv << "Invalid format for file '" << input_file << "' in line " << line
                                      << ": Coords were not correctly read.";
                                throw Exception(oconv.str(), __FILE__, __LINE__);
                            }

                            iconv.ignore(); // ignore the separating ',' or the final '/'.
                        }
                    }

                    auto interface = std::make_unique<Data::Interface>(
                        current_nature, interface_index, std::move(coords_on_vertex_in_interface));
                    current_interface_content.emplace_back(std::move(interface));
                }
            }


            if (!current_interface_content.empty())
                content_.emplace(std::make_pair(current_nature, std::move(current_interface_content)));
        }


        InterfaceFile::InterfaceFile(InterfaceFile::vector_unique_ptr&& rank_interface_file_list)
        {
            for (auto& interface_file_for_rank : rank_interface_file_list)
            {
                assert(!(!interface_file_for_rank));
                auto& content_for_rank = interface_file_for_rank->GetNonCstContent();

                for (auto& pair : content_for_rank)
                {
                    const auto nature = pair.first;

                    auto& aggregated_interface_list = content_[nature]; // not a mistake: we want to create the pair if
                                                                        // not yet existing.

                    auto& interface_list_for_rank = pair.second;

                    std::move(interface_list_for_rank.begin(),
                              interface_list_for_rank.end(),
                              std::back_inserter(aggregated_interface_list));
                }
            }

            // Now make sure they are properly sort by increasing index
            {
                auto& content = GetNonCstContent();

                auto comp_index = [](const auto& lhs_ptr, const auto& rhs_ptr)
                {
                    assert(!(!lhs_ptr));
                    assert(!(!rhs_ptr));

                    return lhs_ptr->GetIndex() < rhs_ptr->GetIndex();
                };

                for (auto& pair : content)
                {
                    auto& interface_list = pair.second;

                    std::sort(begin(interface_list), end(interface_list), comp_index);
                }
            }
        }


        const Data::Interface::vector_unique_ptr&
        InterfaceFile ::GetInterfaceList(InterfaceNS::Nature interface_nature) const
        {
            auto it = content_.find(interface_nature);
            assert(it != content_.cend());
            return it->second;
        }


        const Data::Interface& InterfaceFile::GetInterface(InterfaceNS::Nature interface_nature,
                                                           std::size_t interface_index) const
        {

            decltype(auto) interface_list = GetInterfaceList(interface_nature);

            auto lambda = [interface_index](const auto& interface_ptr)
            {
                assert(!(!interface_ptr));
                return interface_index == interface_ptr->GetIndex();
            };

            auto guess_min = interface_list.cbegin() + static_cast<std::ptrdiff_t>(interface_index - 3);

            if (guess_min < interface_list.cbegin())
                guess_min = interface_list.cbegin();

            auto guess_max = guess_min + 6;

            if (guess_max > interface_list.cend())
                guess_max = interface_list.cend();

            auto it_interface = std::find_if(guess_min, guess_max, lambda);

            if (it_interface == guess_max)
            {
                it_interface = std::find_if(interface_list.cbegin(), interface_list.cend(), lambda);
            }

            assert(it_interface != interface_list.cend());
            return *(*it_interface);
        }


        const Data::Interface&
        InterfaceFile::GetVertexInterfaceFromCoordIndex(CoordsNS::processor_wise_position coords_index) const
        {
            decltype(auto) interface_list = GetInterfaceList(InterfaceNS::Nature::vertex);
            const auto end_interface_list = interface_list.cend();

            const auto it_interface = std::find_if(interface_list.cbegin(),
                                                   end_interface_list,
                                                   [coords_index](const auto& interface_ptr)
                                                   {
                                                       assert(!(!interface_ptr));
                                                       decltype(auto) coords =
                                                           interface_ptr->GetVertexCoordsIndexList();
                                                       assert(coords.size() == 1);
                                                       const auto current_coords_index = coords.back();
                                                       return current_coords_index == coords_index;
                                                   });

            assert(it_interface != end_interface_list);
            return *(*it_interface);
        }


        const Data::Interface&
        InterfaceFile ::GetFaceInterfaceFromCoordsList(const Coords::vector_shared_ptr& coords_list) const
        {
            decltype(auto) interface_list = GetInterfaceList(InterfaceNS::Nature::face);
            const auto end_interface_list = interface_list.cend();

            std::vector<CoordsNS::processor_wise_position> coords_index_list(coords_list.size());

            std::transform(coords_list.cbegin(),
                           coords_list.cend(),
                           coords_index_list.begin(),
                           [](const auto& coords_ptr)
                           {
                               assert(!(!coords_ptr));
                               return coords_ptr->GetProcessorWisePosition();
                           });

            std::sort(coords_index_list.begin(), coords_index_list.end());

            const auto it = std::find_if(interface_list.cbegin(),
                                         end_interface_list,
                                         [coords_index_list](const auto& interface_ptr)
                                         {
                                             assert(!(!interface_ptr));

                                             auto coords = interface_ptr->GetVertexCoordsIndexList();
                                             // < Copy incurred on purpose (see sort below).

                                             assert(coords.size() == coords_index_list.size());

                                             std::sort(coords.begin(), coords.end());
                                             return coords == coords_index_list;
                                         });

            assert(it != end_interface_list);

            return *(*it);
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
