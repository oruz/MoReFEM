/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
#define MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "PostProcessing/Data/TimeIteration.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from time_iteration.hhdata output file.
         *
         * There is one such file per mesh.
         *
         * Example:
         * \verbatim
         # Time iteration; time; numbering subset id; filename
         1;0.001;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
         /Mesh_1/NumberingSubset_10/fluid_velocity_time_00001.hhdata
         2;0.002;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
         /Mesh_1/NumberingSubset_10/fluid_velocity_time_00002.hhdata
         ...
         \endverbatim
         *
         * (no space after Rank_* - but I had to add it to avoid compiler mistaking it for an end of a C comment).
         *
         */
        class TimeIterationFile final
        {
          public:
            //! Alias to most relevant smart pointer.
            using const_unique_ptr = std::unique_ptr<const TimeIterationFile>;


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            //! \param[in] input_file Result file of MoReFEM program which gives the time related informations;
            //! it is named 'time_iteration.hhdata'.
            explicit TimeIterationFile(const std::string& input_file);

            //! Destructor.
            ~TimeIterationFile() = default;

            //! \copydoc doxygen_hide_copy_constructor
            TimeIterationFile(const TimeIterationFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            TimeIterationFile(TimeIterationFile&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            TimeIterationFile& operator=(const TimeIterationFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            TimeIterationFile& operator=(TimeIterationFile&& rhs) = delete;

            ///@}

          public:
            //! Accessor to the list of time iterations.
            const Data::TimeIteration::vector_const_unique_ptr& GetTimeIterationList() const;

            //! Number of steps.
            std::size_t Nstep() const;

          private:
            //! List of all time iterations considered.
            Data::TimeIteration::vector_const_unique_ptr time_iteration_list_;
        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#include "PostProcessing/File/TimeIterationFile.hxx" // IWYU pragma: export


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
