/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HPP_
#define MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HPP_

#include <iosfwd>
#include <memory>

#include "PostProcessing/Data/UnknownInformation.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from one line of unknowns.hhdata output file.
         *
         * This file gives for each unknown the nature ('scalar' or 'vectorial').
         */
        class UnknownInformationFile final
        {
          public:
            //! Alias to unique_ptr.
            using const_unique_ptr = std::unique_ptr<const UnknownInformationFile>;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] input_file Result file of MoReFEM program which gives the unknowns related informations;
             * it is named 'unknowns.hhdata'.
             */
            explicit UnknownInformationFile(const std::string& input_file);

            //! Destructor.
            ~UnknownInformationFile() = default;

            //! \copydoc doxygen_hide_copy_constructor
            UnknownInformationFile(const UnknownInformationFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UnknownInformationFile(UnknownInformationFile&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            UnknownInformationFile& operator=(const UnknownInformationFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UnknownInformationFile& operator=(UnknownInformationFile&& rhs) = delete;


            ///@}

          public:
            //! Get the list of unknowns.
            const Data::UnknownInformation::vector_const_shared_ptr& GetExtendedUnknownList() const;


          private:
            //! List of unknowns.
            Data::UnknownInformation::vector_const_shared_ptr unknown_list_;
        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#include "PostProcessing/File/UnknownInformationFile.hxx" // IWYU pragma: export


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HPP_
