/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <fstream>

#include "Utilities/Filesystem/File.hpp"

#include "PostProcessing/File/UnknownInformationFile.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        UnknownInformationFile::UnknownInformationFile(const std::string& input_file)
        {
            std::ifstream stream;

            FilesystemNS::File::Read(stream, input_file, __FILE__, __LINE__);

            std::string line;

            while (getline(stream, line))
                unknown_list_.emplace_back(std::make_unique<Data::UnknownInformation>(line));
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
