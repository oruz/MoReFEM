/*!
//
// \file
//
//
// Created by Federica Caforio <federica.caforio@inria.fr> on the Mon, 6 Jun 2016 16:19:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_
#define MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace RefineMeshNS
    {


        /*!
         * \brief Create a new refined mesh, whose vertices are the coordinates of the DOFs.
         *
         *  Useful for higher order shape functions. Remark: the
         *  numbering is specific for spectral elements and the algorithm is only valid for quadrangles.
         * \param[in] felt_space finite element space instance,
         * \param[in] mesh initial mesh to be refined,
         * \param[in] output_directory output directory in which the refined mesh will be saved.
         */
        void RefineMeshSpectral(const FEltSpace& felt_space, const Mesh& mesh, const std::string& output_directory);


    } // namespace RefineMeshNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_
