/*!
//
// \file
//
//
// Created by Federica Caforio <federica.caforio@inria.fr> on the Thu, 12 May 2016 16:34:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"
#include "Geometry/GeometricElt/Instances/Quadrangle/Quadrangle4.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "PostProcessing/RefineMeshQuadranglesSpectral/RefineMesh.hpp"


namespace MoReFEM
{


    namespace RefineMeshNS
    {


        void RefineMeshSpectral(const FEltSpace& felt_space, const Mesh& mesh, const std::string& output_directory)
        {

            const auto& unknown_list = felt_space.GetExtendedUnknownList();
            assert(unknown_list.size() == 1);
            const auto& unknown_ptr = unknown_list.back();
            assert(!(!unknown_ptr));
            const auto& unknown = *unknown_ptr;

            const auto Ncomponent = unknown.GetNature() == UnknownNS::Nature::scalar ? 1u : mesh.GetDimension();

            const auto& local_felt_space_storage =
                felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();
            assert(!local_felt_space_storage.empty());


            const auto n_dof = felt_space.GetProcessorWiseDofList().size();
            const auto n_nodes = n_dof / Ncomponent;

            Coords::vector_shared_ptr coords_list;
            coords_list.reserve(n_nodes);

            for (auto node = 0ul; node < n_nodes; ++node)
            {
                auto coords_ptr = Internal::CoordsNS::Factory::Origin();
                coords_ptr->SetIndexFromMeshFile(::MoReFEM::CoordsNS::index_from_mesh_file(node));
                // < \todo Very suspect! (Index is format-dependant, and Medit starts as 1 for instance...)
                coords_list.emplace_back(std::move(coords_ptr));
            }

            assert(coords_list.size() == n_nodes);

            // Get instance for new refined mesh.
            auto& mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);

            // Generate a unique id for the new mesh.
            const auto unique_id_new_mesh = mesh_manager.GenerateUniqueId();
            std::size_t Nelement_created = 0ul;
            for (const auto& local_felt_space_pair : local_felt_space_storage)
            {
                const auto& ref_local_felt_space_ptr = local_felt_space_pair.first;
                assert(!(!ref_local_felt_space_ptr));
                const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                if (ref_local_felt_space.GetRefGeomElt().GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                    continue;

                const auto& local_felt_space_list_per_geom_elt = local_felt_space_pair.second;

                // Get the position of the nodes on the reference element.
                const auto& ref_felt_list = ref_local_felt_space.GetRefFEltList();
                assert(!ref_felt_list.empty());
                assert(ref_felt_list.size() == 1ul);

                const auto& spectral_ref_felt = ref_felt_list.back()->GetBasicRefFElt();

                const std::size_t order = spectral_ref_felt.GetOrder();

                std::vector<std::shared_ptr<GeometricElt>> new_quadrangle_vector;

                const auto n_elem = local_felt_space_list_per_geom_elt.size();
                std::cout << "n_elem = " << n_elem << std::endl;

                new_quadrangle_vector.reserve(order * order * n_elem);

                // Get the local2global array.
                for (const auto& local_felt_space_for_geom_elt : local_felt_space_list_per_geom_elt)
                {

                    const auto& original_geom_elt = mesh.GetGeometricEltFromIndex<RoleOnProcessor::processor_wise>(
                        local_felt_space_for_geom_elt.first);
                    const auto& mesh_label_ptr = original_geom_elt.GetMeshLabelPtr();

                    const auto& local_felt_space_ptr = local_felt_space_for_geom_elt.second;
                    assert(!(!local_felt_space_ptr));
                    const auto& local_felt_space = *local_felt_space_ptr;

                    const auto& loc_2_glob = local_felt_space.GetLocal2Global<MpiScale::processor_wise>(unknown_list);

                    const auto Nlocal_node = spectral_ref_felt.NlocalNode();

                    assert(Nlocal_node * Ncomponent == static_cast<std::size_t>(loc_2_glob.size()));

                    for (std::size_t local_node_index = 0ul; local_node_index < Nlocal_node; ++local_node_index)
                    {
                        const auto& local_coord = spectral_ref_felt.GetLocalNode(local_node_index).GetLocalCoords();

                        const auto global_index = static_cast<std::size_t>(loc_2_glob[local_node_index]);
                        assert(global_index % Ncomponent == 0ul);

                        const auto coords_index = global_index / Ncomponent;

                        assert(coords_index < coords_list.size());
                        auto& coord_in_mesh = *coords_list[coords_index];

                        Advanced::GeomEltNS::Local2Global(
                            local_felt_space.GetGeometricElt(), local_coord, coord_in_mesh);
                    }

                    // Create new refined quadrangles.
                    for (std::size_t r = 0; r < order; ++r)
                    {
                        for (std::size_t s = 0; s < order; ++s)
                        {
                            std::size_t R = (order + 1) * s + r;

                            std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> vertices(4);

                            assert(R + order + 2 < loc_2_glob.size());

                            vertices[0] =
                                ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(loc_2_glob[R]));
                            vertices[1] =
                                ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(loc_2_glob[R + 1]));
                            vertices[2] = ::MoReFEM::CoordsNS::index_from_mesh_file(
                                static_cast<std::size_t>(loc_2_glob[R + order + 2]));
                            vertices[3] = ::MoReFEM::CoordsNS::index_from_mesh_file(
                                static_cast<std::size_t>(loc_2_glob[R + order + 1]));


                            for (auto& vertex : vertices)
                            {
                                assert(vertex.Get() % Ncomponent == 0ul);
                                vertex.Get() /= Ncomponent;
                            }


                            assert(std::all_of(vertices.cbegin(),
                                               vertices.cend(),
                                               [n_nodes](const auto& vertex)
                                               {
                                                   return vertex.Get() < n_nodes;
                                               }));

                            // Create a quadrangle and add it in the list.
                            auto new_quadrangle_ptr =
                                std::make_shared<Quadrangle4>(unique_id_new_mesh, coords_list, std::move(vertices));
                            new_quadrangle_ptr->SetIndex(Nelement_created++);
                            new_quadrangle_ptr->SetMeshLabel(mesh_label_ptr);
                            new_quadrangle_vector.push_back(new_quadrangle_ptr);
                        }
                    }
                }

                assert(new_quadrangle_vector.size() == order * order * n_elem);

                const auto mesh_dimension = felt_space.GetDimension();

                const double space_unit = 1.; // Mesh::GetSpaceUnit();

                MeshLabel::vector_const_shared_ptr new_label_list(mesh.GetLabelList());

                Mesh::BuildEdge do_build_edge = Mesh::BuildEdge::no;
                Mesh::BuildFace do_build_face = Mesh::BuildFace::no;
                Mesh::BuildVolume do_build_volume = Mesh::BuildVolume::no;

                // Construction of the mesh.
                mesh_manager.Create(mesh_dimension,
                                    space_unit,
                                    std::move(new_quadrangle_vector),
                                    std::vector<std::shared_ptr<GeometricElt>>(),
                                    std::move(coords_list),
                                    std::move(new_label_list),
                                    do_build_edge,
                                    do_build_face,
                                    do_build_volume,
                                    Mesh::BuildPseudoNormals::no);

                const auto& new_mesh = mesh_manager.GetMesh(unique_id_new_mesh);

                new_mesh.Write<MeshNS::Format::Ensight>(output_directory + "/refined_mesh.geo");
                // new_mesh.Write<MeshNS::Format::Medit>(output_directory + "/refined_mesh.mesh"); // Version 2
                // implicitly defined.
            }
        }

    } // namespace RefineMeshNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
