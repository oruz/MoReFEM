/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 16:13:29 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENSIGHT6_HPP_
#define MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENSIGHT6_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <vector>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Enum class that specifies the nature of the mesh using in post-processing.
         *
         * No if the mesh is the one that was present during the run of the model.
         * Yes if it is rather a finer mesh upon which the solution is displayed as if it was Q1.
         *
         * Currently 'no' assumes the model was P1 or Q1, and 'yes' that it was run in sequential.
         */
        enum class RefinedMesh
        {
            no,
            yes
        };


        namespace OutputFormat
        {


            /*!
             * \brief Generates the output in Ensight 6 format.
             *
             */
            class Ensight6 final
            {


              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] data_directory The directory into which outputs of a model were written (this is
                 * the field Result/output_directory in the Lua file).
                 * \param[in] numbering_subset_id_list List of \a NumberingSubset unique identifiers to consider.
                 * \param[in] mesh Mesh upon which the results are to be displayed. Most of the time
                 * it is the mesh given in the Lua file; it might also be a refined mesh if you used
                 * RefineMeshSpectral(). \param[in] is_mesh_refined \see RefinedMesh for more details. \param[in]
                 * unknown_list Define here the list of unknowns for which Ensight output is sought. If empty, all the
                 * unknowns found in the output file are taken; if not only the selected subset is taken. The name
                 * specified here MUST match some of the unknowns in the unknown file. \param[in] ensight_directory
                 * Directory into which the Ensight outputs will be written. If nullptr, a subdirectory named "Ensight6"
                 * will be created in the Mesh directory (this should be the default behaviour but I need the other one
                 * for some tests). Call site is responsible of the object destruction (should be stored in a smart
                 * pointer in the first place).
                 */
                explicit Ensight6(const FilesystemNS::Directory& data_directory,
                                  const std::vector<std::string>& unknown_list,
                                  const std::vector<std::size_t>& numbering_subset_id_list,
                                  const Mesh& mesh,
                                  RefinedMesh is_mesh_refined = RefinedMesh::no,
                                  const FilesystemNS::Directory* ensight_directory = nullptr);

                //! Destructor.
                ~Ensight6() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Ensight6(const Ensight6& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Ensight6(Ensight6&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Ensight6& operator=(const Ensight6& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Ensight6& operator=(Ensight6&& rhs) = delete;

                ///@}
            };


        } // namespace OutputFormat


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#include "PostProcessing/OutputFormat/Ensight6.hxx" // IWYU pragma: export


#endif // MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENSIGHT6_HPP_
