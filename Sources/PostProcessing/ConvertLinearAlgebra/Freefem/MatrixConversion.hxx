/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 May 2016 11:23:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_
#define MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_

// IWYU pragma: private, include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>
#include <vector>

#include "PostProcessing/PostProcessing.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::Wrappers::Petsc { class Matrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace FreefemNS
        {


            inline const Wrappers::Petsc::Matrix& MatrixConversion::GetOriginalMatrix() const noexcept
            {
                return original_matrix_;
            }


            inline const PostProcessing& MatrixConversion::GetPostProcessingData() const noexcept
            {
                assert(!(!post_processing_data_));
                return *post_processing_data_;
            }


            inline const std::vector<std::vector<std::size_t>>&
            MatrixConversion ::GetFreefemDofNumberingPerGeomElt() const noexcept
            {
                return freefem_dof_numbering_per_geom_elt_;
            }


            inline const std::vector<std::size_t>&
            MatrixConversion ::GetFreefemDofList(const std::size_t highest_dim_geom_elt_index) const noexcept
            {
                decltype(auto) freefem_dof_numbering_per_geom_elt = GetFreefemDofNumberingPerGeomElt();

                assert(highest_dim_geom_elt_index < freefem_dof_numbering_per_geom_elt.size());

                return freefem_dof_numbering_per_geom_elt[highest_dim_geom_elt_index];
            }


            inline const std::vector<std::size_t>& MatrixConversion::GetDofMapping() const noexcept
            {
                return dof_mapping_;
            }


            inline void MatrixConversion::SetDofMapping(std::vector<std::size_t>&& value)
            {
                assert(dof_mapping_.empty());
                dof_mapping_ = std::move(value);
            }


            inline const FilesystemNS::Directory& MatrixConversion::GetOutputDirectory() const noexcept
            {
                return output_directory_;
            }


        } // namespace FreefemNS


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_
