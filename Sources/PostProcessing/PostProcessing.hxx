/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_
#define MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_

// IWYU pragma: private, include "PostProcessing/PostProcessing.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib> // IWYU pragma: keep
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/Data/TimeIteration.hpp"
#include "PostProcessing/Data/UnknownInformation.hpp"
#include "PostProcessing/File/DofInformationFile.hpp"
#include "PostProcessing/File/InterfaceFile.hpp"
#include "PostProcessing/File/TimeIterationFile.hpp"
#include "PostProcessing/File/UnknownInformationFile.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        inline const Data::UnknownInformation::vector_const_shared_ptr& PostProcessing ::GetExtendedUnknownList() const
        {
            return GetUnknownFile().GetExtendedUnknownList();
        }


        inline const Data::TimeIteration::vector_const_unique_ptr& PostProcessing::GetTimeIterationList() const
        {
            return GetTimeIterationFile().GetTimeIterationList();
        }


        inline const Data::DofInformation::vector_const_shared_ptr&
        PostProcessing ::GetDofInformationList(const std::size_t numbering_subset_id, const std::size_t processor) const
        {
            return GetDofFile(numbering_subset_id, processor).GetDofList();
        }


        inline const FilesystemNS::Directory& PostProcessing::GetDataDirectory() const noexcept
        {
            return data_directory_;
        }


        inline const Mesh& PostProcessing::GetMesh() const noexcept
        {
            return mesh_;
        }


        inline std::size_t PostProcessing::Nprocessor() const noexcept
        {
            assert(Nprocessor_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nprocessor_;
        }


        inline const InterfaceFile& PostProcessing::GetInterfaceData() const noexcept
        {
            assert(!(!interface_file_));
            return *interface_file_;
        }


        inline const UnknownInformationFile& PostProcessing::GetUnknownFile() const noexcept
        {
            assert(!(!unknown_file_));
            return *unknown_file_;
        }


        inline const TimeIterationFile& PostProcessing::GetTimeIterationFile() const noexcept
        {
            assert(!(!time_iteration_file_));
            return *time_iteration_file_;
        }


        inline std::size_t PostProcessing::Ndof() const noexcept
        {
            return Ndof_;
        }


        inline const std::vector<std::size_t>& PostProcessing::GetNumberingSubsetIdList() const noexcept
        {
            return numbering_subset_id_list_;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_
