/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 Jun 2016 11:13:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HXX_
#define MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HXX_

// IWYU pragma: private, include "PostProcessing/Data/Interface.hpp"

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"

namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            inline InterfaceNS::Nature Interface::GetNature() const noexcept
            {
                return nature_;
            }


            inline std::size_t Interface::GetIndex() const noexcept
            {
                return index_;
            }


            inline const std::vector<CoordsNS::processor_wise_position>&
            Interface::GetVertexCoordsIndexList() const noexcept
            {
                return coords_list_;
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HXX_
