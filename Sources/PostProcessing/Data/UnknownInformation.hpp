/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:38:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HPP_
#define MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HPP_

#include <iosfwd>
#include <memory>
#include <vector>

#include "FiniteElement/Unknown/EnumUnknown.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            //! Convenient alias.
            using UnknownNature = UnknownNS::Nature;


            /*!
             * \brief Class which holds the informations obtained from one line of unknowns.hhdata output file.
             *
             * This file gives for each unknown the nature ('scalar' or 'vectorial').
             */
            class UnknownInformation final
            {

              public:
                //! Alias to shared pointer.
                using const_shared_ptr = std::shared_ptr<const UnknownInformation>;

                //! Alias to vector of shared pointers.
                using vector_const_shared_ptr = std::vector<const_shared_ptr>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] line Line as read in unknowns.hhdata output file. Format is:
                 * unknown_name : unknown_nature.
                 *
                 * For instance: 'displacement : vectorial'.
                 */
                explicit UnknownInformation(const std::string& line);

                //! Destructor.
                ~UnknownInformation() = default;

                //! \copydoc doxygen_hide_copy_constructor
                UnknownInformation(const UnknownInformation& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                UnknownInformation(UnknownInformation&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                UnknownInformation& operator=(const UnknownInformation& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                UnknownInformation& operator=(UnknownInformation&& rhs) = delete;

                ///@}

              public:
                //! Name.
                const std::string& GetName() const;

                //! Nature.
                UnknownNature GetNature() const;


              private:
                //! Name.
                std::string name_;

                //! Nature (scalar or vectorial).
                UnknownNature nature_;
            };


        } //  namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#include "PostProcessing/Data/UnknownInformation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HPP_
