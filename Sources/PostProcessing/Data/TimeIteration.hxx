/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:50:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HXX_
#define MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HXX_

// IWYU pragma: private, include "PostProcessing/Data/TimeIteration.hpp"
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            inline std::size_t TimeIteration::GetIteration() const noexcept
            {
                return time_iteration_;
            }


            inline double TimeIteration::GetTime() const noexcept
            {
                return time_;
            }


            inline const std::string& TimeIteration::GetSolutionFilename() const noexcept
            {
                return solution_filename_;
            }


            inline std::size_t TimeIteration::GetNumberingSubsetId() const noexcept
            {
                return numbering_subset_id_;
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HXX_
