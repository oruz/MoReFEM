/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:53:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <sstream>

#include "Geometry/Interfaces/EnumInterface.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/Exceptions/Exception.hpp"
#include "PostProcessing/File/InterfaceFile.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            DofInformation::DofInformation(const std::string& line, const InterfaceFile& interface_file)
            {
                std::istringstream iconv(line);

                {
                    iconv >> program_wise_index_;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }

                iconv.ignore(); // the ';'

                {

                    iconv >> processor_wise_index_;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }

                iconv.ignore(); // the ';'

                InterfaceNS::Nature interface_nature;

                {
                    std::string interface_name;
                    iconv >> interface_name;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                    interface_nature = InterfaceNS::GetNature(interface_name);
                }

                std::size_t interface_index;

                {
                    iconv >> interface_index;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }

                iconv.ignore(); // the ';'

                {
                    iconv >> unknown_name_;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }

                {
                    iconv >> unknown_component_;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }

                iconv.ignore(); // the ';'

                {
                    iconv >> shape_function_label_;
                    if (iconv.fail())
                        throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                }


                interface_ = &(interface_file.GetInterface(interface_nature, interface_index));
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
