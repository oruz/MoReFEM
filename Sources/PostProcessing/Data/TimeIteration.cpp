/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:38:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <sstream>

#include "Utilities/Environment/Environment.hpp"

#include "PostProcessing/Data/TimeIteration.hpp"
#include "PostProcessing/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            TimeIteration::TimeIteration(const std::string& line)
            {
                std::istringstream iconv(line);

                iconv >> time_iteration_;

                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                iconv.ignore(); // for ';'

                iconv >> time_;

                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                iconv.ignore(); // for ';'

                iconv >> numbering_subset_id_;

                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                iconv.ignore(); // for ';'

                iconv >> solution_filename_;

                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);

                decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
                solution_filename_ = environment.SubstituteValues(solution_filename_);
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
