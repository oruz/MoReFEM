/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 Jun 2016 11:13:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HPP_
#define MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            /*!
             * \brief Class which holds the informations obtained from one line of interfaces.hhdata output file.
             *
             * There is one such file per mesh.
             *
             * This file gives for each interface (vertex, edge, face or volume)  the list of \a Coords that delimit it.
             */
            class Interface
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = Interface;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] nature Nature of the interface.
                 * \param[in] index Index of the interface. This index is unique within interfaces of the same nature
                 * (e.g. there is only one 'Edge 1' but there might be a 'Vertex 1' or a 'Face 1').
                 * \param[in] coords_list  Indexes of the Coords that describe the interface. We assume P1 geometry here
                 * (i.e. all Coords are vertex).
                 */
                explicit Interface(InterfaceNS::Nature nature,
                                   std::size_t index,
                                   std::vector<CoordsNS::processor_wise_position>&& coords_list);

                //! Destructor.
                ~Interface() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Interface(const Interface& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Interface(Interface&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Interface& operator=(const Interface& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Interface& operator=(Interface&& rhs) = delete;

                ///@}

              public:
                //! Accessor to the nature of the interface.
                InterfaceNS::Nature GetNature() const noexcept;

                //! Constant accessor to the index of the interface.
                std::size_t GetIndex() const noexcept;

                //! Indexes of the Coords that describe the interface.
                // #248 Only P1 Geometry at this point, so assumption Coords are vertices...
                const std::vector<CoordsNS::processor_wise_position>& GetVertexCoordsIndexList() const noexcept;

              private:
                //! Nature of the interface.
                const InterfaceNS::Nature nature_;

                /*!
                 * \brief Index of the interface.
                 *
                 * This index is unique within interfaces of the same nature (e.g. there is only one 'Edge 1' but there
                 * might be a 'Vertex 1' or a 'Face 1').
                 */
                const std::size_t index_;

                /*!
                 * \brief Indexes of the Coords that describe the interface.
                 */
                const std::vector<CoordsNS::processor_wise_position> coords_list_;
            };


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#include "PostProcessing/Data/Interface.hxx" // IWYU pragma: export


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_INTERFACE_HPP_
