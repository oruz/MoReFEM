/*!
// \file
//
//
// Created by Federica Caforio <federica.caforio@inria.fr> on the Fri, 13 May 2016 15:12:32 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"


#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hpp"

#include "Utilities/UniqueId/UniqueId.hpp"

//#include "ModelInstances/main_refined_mesh_output/Model.hpp"
//#include "ModelInstances/main_refined_mesh_output/InputData.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{

    using namespace MoReFEM;


    try
    {


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple<InputDataNS::TimeManager,

                                          InputDataNS::Mesh<1>,

                                          InputDataNS::FEltSpace<1>,

                                          InputDataNS::Domain<1>,

                                          InputDataNS::Unknown<1>,

                                          InputDataNS::NumberingSubset<1>,

                                          InputDataNS::Result>;

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        using morefem_data_type =
            MoReFEMData<InputData, program_type::post_processing, Utilities::InputDataNS::DoTrackUnusedFields::no>;

        morefem_data_type morefem_data(argc, argv);

        const auto& mpi = morefem_data.GetMpi();

        try
        {
            RefineMeshNS::Model<morefem_data_type> model(morefem_data);
            model.Run();
            std::cout << "End of Post-Processing. Refined mesh created." << std::endl;
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch (const ExceptionNS::GracefulExit&)
    {
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
