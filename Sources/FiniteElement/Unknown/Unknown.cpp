/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    const std::string& Unknown::ClassName()
    {
        static std::string ret("Unknown");
        return ret;
    }


    Unknown::Unknown(const std::string& name, const std::size_t unique_id, const UnknownNS::Nature nature)
    : unique_id_parent(unique_id), name_(name), nature_(nature)
    { }


    bool operator<(const Unknown& unknown1, const Unknown& unknown2)
    {
        return unknown1.GetUniqueId() < unknown2.GetUniqueId();
    }


    bool operator==(const Unknown& unknown1, const Unknown& unknown2)
    {
        return unknown1.GetUniqueId() == unknown2.GetUniqueId();
    }


    std::size_t Ncomponent(const Unknown& unknown, const std::size_t mesh_dimension)
    {

        switch (unknown.GetNature())
        {
        case UnknownNS::Nature::scalar:
            return 1u;
        case UnknownNS::Nature::vectorial:
            return mesh_dimension;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
