/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/EnumUnknown.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Very generic informations about a given unknown.
     *
     *
     */
    class Unknown : public Crtp::UniqueId<Unknown, UniqueIdNS::AssignationMode::manual>
    {

      public:
        //! Name of the class.
        static const std::string& ClassName();

        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const Unknown>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Alias to the unique id parent.
        using unique_id_parent = Crtp::UniqueId<Unknown, UniqueIdNS::AssignationMode::manual>;

        //! Friendship.
        friend class UnknownManager;


        /// \name Special members.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * Private as unknown should be created by UnknownManager.
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        explicit Unknown(const std::string& name, std::size_t unique_id, const UnknownNS::Nature nature);

      public:
        //! Destructor.
        ~Unknown() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Unknown(const Unknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Unknown(Unknown&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Unknown& operator=(const Unknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Unknown& operator=(Unknown&& rhs) = delete;


        ///@}


      public:
        //! Nature of the unknown.
        UnknownNS::Nature GetNature() const noexcept;

        //! Get the name of the variable.
        const std::string& GetName() const noexcept;

      private:
        //! Name of the variable.
        const std::string name_;

        //! Nature of the unknown.
        const UnknownNS::Nature nature_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criterion used here is the underlying unique id.
    bool operator<(const Unknown& lhs, const Unknown& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criterion used here is the underlying unique id.
    bool operator==(const Unknown& lhs, const Unknown& rhs);

    //! \copydoc doxygen_hide_operator_not_equal
    //! Criterion used here is the underlying unique id.
    bool operator!=(const Unknown& lhs, const Unknown& rhs);

    //! Number of components to consider for the unknown.
    //! \param[in] unknown \a Unknown for which the information is requested.
    //! \param[in] mesh_dimension Dimension of the mesh.
    std::size_t Ncomponent(const Unknown& unknown, const std::size_t mesh_dimension);


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/Unknown/Unknown.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HPP_
