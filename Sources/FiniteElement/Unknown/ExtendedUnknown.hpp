/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Apr 2015 10:40:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HPP_

#include <iosfwd>
#include <memory>
#include <vector>

#include "Core/NumberingSubset/NumberingSubset.hpp" // IWYU pragma: export
#include "FiniteElement/Unknown/Unknown.hpp"        // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief This class encapsulates an unknown in a given finite element space.
     *
     * In one finite element space unknown is related to exactly one numbering subset and exactly shape function label.
     */
    class ExtendedUnknown final
    {

      public:
        //! Alias to shared pointer to const object.
        using const_shared_ptr = std::shared_ptr<const ExtendedUnknown>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown Pointer to the \a Unknown for which \a ExtendedUnknown is created.
         * \param[in] numbering_subset Pointer to the \a NumberingSubset for which \a ExtendedUnknown is created.
         * \param[in] shape_function_label String that acts as shape_function_label for which \a ExtendedUnknown is
         * created.
         */
        explicit ExtendedUnknown(Unknown::const_shared_ptr unknown,
                                 NumberingSubset::const_shared_ptr numbering_subset,
                                 const std::string& shape_function_label);

        //! Destructor.
        ~ExtendedUnknown() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ExtendedUnknown(const ExtendedUnknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ExtendedUnknown(ExtendedUnknown&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ExtendedUnknown& operator=(const ExtendedUnknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ExtendedUnknown& operator=(ExtendedUnknown&& rhs) = delete;

        ///@}

        //! Get the underlying unknown.
        const Unknown& GetUnknown() const noexcept;

        //! Get the underlying unknown.
        const Unknown::const_shared_ptr& GetUnknownPtr() const noexcept;

        //! Get the underlying numbering subset.
        const NumberingSubset& GetNumberingSubset() const noexcept;

        //! Get the underlying numbering subset as a smart pointer.
        const NumberingSubset::const_shared_ptr& GetNumberingSubsetPtr() const noexcept;

        //! Return the shape function label ('P1', 'Q3', etc...)
        const std::string& GetShapeFunctionLabel() const noexcept;

        //! Nature of the unknown.
        UnknownNS::Nature GetNature() const noexcept;

      private:
        //! Unknown considered.
        Unknown::const_shared_ptr unknown_;

        //! Numbering subset considered.
        NumberingSubset::const_shared_ptr numbering_subset_;

        //! Shape function label.
        const std::string& shape_function_label_;
    };


    //! \copydoc doxygen_hide_operator_equal
    //! Criteria: Two such objects are equal if both unknown and numbering subset are the same.
    bool operator==(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs);


    //! \copydoc doxygen_hide_operator_less
    //! Criteria: first unknown are compared, then numbering subset and finally shape function label.
    bool operator<(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs);


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/Unknown/ExtendedUnknown.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HPP_
