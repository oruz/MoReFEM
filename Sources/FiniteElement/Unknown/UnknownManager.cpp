/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 09:05:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "Utilities/Filesystem/File.hpp"

#include "FiniteElement/Nodes_and_dofs/Exceptions/Dof.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{


    UnknownManager::~UnknownManager() = default;


    const std::string& UnknownManager::ClassName()
    {
        static std::string ret("UnknownManager");
        return ret;
    }


    void UnknownManager::RegisterUnknown(const Unknown::const_shared_ptr& unknown_ptr)
    {
        assert(!(!unknown_ptr));

        const auto& unknown_list = GetList();
        const auto& name = unknown_ptr->GetName();

        if (std::find_if(unknown_list.cbegin(),
                         unknown_list.cend(),
                         [&name](const auto& unknown_in_list_ptr)
                         {
                             assert(!(!unknown_in_list_ptr));
                             return unknown_in_list_ptr->GetName() == name;
                         })
            != unknown_list.cend())
            throw ExceptionNS::Dof::DuplicatedUnknownInInputFile(name, __FILE__, __LINE__);

        const auto& unique_id = unknown_ptr->GetUniqueId();

        if (std::find_if(unknown_list.cbegin(),
                         unknown_list.cend(),
                         [unique_id](const auto& unknown_in_list_ptr)
                         {
                             assert(!(!unknown_in_list_ptr));
                             return unknown_in_list_ptr->GetUniqueId() == unique_id;
                         })
            != unknown_list.cend())
            throw Exception("Two different unknowns can't share the same unique id (namely " + std::to_string(unique_id)
                                + ")",
                            __FILE__,
                            __LINE__);

        unknown_list_.push_back(unknown_ptr);
    }


    Unknown::const_shared_ptr UnknownManager::GetUnknownPtr(std::size_t unique_id) const
    {
        const auto& unknown_list = GetList();

        auto it = std::find_if(unknown_list.cbegin(),
                               unknown_list.cend(),
                               [unique_id](const Unknown::const_shared_ptr& unknown_ptr)
                               {
                                   assert(!(!unknown_ptr));
                                   return unknown_ptr->GetUniqueId() == unique_id;
                               });

        assert(it != unknown_list.cend());
        return *it;
    }


    Unknown::const_shared_ptr UnknownManager::GetUnknownPtr(const std::string& unknown_name) const
    {
        const auto& unknown_list = GetList();

        auto it = std::find_if(unknown_list.cbegin(),
                               unknown_list.cend(),
                               [&unknown_name](const Unknown::const_shared_ptr& unknown_ptr)
                               {
                                   assert(!(!unknown_ptr));
                                   return unknown_ptr->GetName() == unknown_name;
                               });

#ifndef NDEBUG
        {
            if (it == unknown_list.cend())
            {
                std::ostringstream oconv;
                oconv << "Unknown '" << unknown_name
                      << "' was sought but couldn't be found in the UnknownManager."
                         " Registration is to be performed by your Model; make sure the unknown is correctly there in "
                         "its "
                         "UnknownTuple. The unknowns currently known are: "
                      << std::endl;

                for (const auto& unknown_ptr : unknown_list)
                {
                    assert(!(!unknown_ptr));
                    oconv << "\t- " << unknown_ptr->GetName() << std::endl;
                }
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        }
#endif // NDEBUG

        assert(!(!(*it)));
        return *it;
    }


    void UnknownManager::Create(std::size_t unique_id, const std::string& name, const std::string& str_nature)
    {
        UnknownNS::Nature nature;

        if (str_nature == "scalar")
            nature = UnknownNS::Nature::scalar;
        else if (str_nature == "vectorial")
            nature = UnknownNS::Nature::vectorial;
        else
        {
            assert(false);
            exit(EXIT_FAILURE);
        }

        auto raw_ptr = new Unknown(name, unique_id, nature);

        auto unknown_ptr = Unknown::const_shared_ptr(raw_ptr);

        RegisterUnknown(unknown_ptr);
    }


    void WriteUnknownList(const std::string& output_directory)
    {
        std::ostringstream oconv;
        oconv << output_directory << "/unknowns.hhdata";
        std::ofstream out;
        FilesystemNS::File::Create(out, oconv.str(), __FILE__, __LINE__);

        const auto& storage = UnknownManager::GetInstance(__FILE__, __LINE__).GetList();

        for (const auto& unknown_ptr : storage)
        {
            assert(!(!unknown_ptr));
            const auto& unknown = *unknown_ptr;

            out << unknown.GetName() << " : " << unknown.GetNature() << std::endl;
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
