/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_

// IWYU pragma: private, include "FiniteElement/Unknown/Unknown.hpp"

#include <iosfwd>

#include "FiniteElement/Unknown/EnumUnknown.hpp"
// IWYU pragma: no_forward_declare MoReFEM::Unknown


namespace MoReFEM
{


    inline const std::string& Unknown::GetName() const noexcept
    {
        return name_;
    }


    inline bool operator!=(const Unknown& unknown1, const Unknown& unknown2)
    {
        return !(operator==(unknown1, unknown2));
    }


    inline UnknownNS::Nature Unknown::GetNature() const noexcept
    {
        return nature_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_
