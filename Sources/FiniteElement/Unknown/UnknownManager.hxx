/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 08:44:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/Unknown/UnknownManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    template<class UnknownSectionT>
    void UnknownManager::Create(const UnknownSectionT& section)
    {
        namespace ipl = Internal::InputDataNS;

        decltype(auto) name = ipl::ExtractLeaf<typename UnknownSectionT::Name>(section);
        decltype(auto) str_nature = ipl::ExtractLeaf<typename UnknownSectionT::Nature>(section);

        Create(section.GetUniqueId(), name, str_nature);
    }


    inline std::size_t UnknownManager::Nunknown() const noexcept
    {
        return unknown_list_.size();
    }


    inline const Unknown& UnknownManager::GetUnknown(std::size_t i) const noexcept
    {
        return *GetUnknownPtr(i);
    }


    inline const Unknown& UnknownManager::GetUnknown(const std::string& unknown_name) const
    {
        return *GetUnknownPtr(unknown_name);
    }


    inline const Unknown::vector_const_shared_ptr& UnknownManager::GetList() const noexcept
    {
        return unknown_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
