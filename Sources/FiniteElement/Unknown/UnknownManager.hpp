/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 08:44:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp"                        // IWYU pragma: export

#include "Core/InputData/Instances/FElt/Unknown.hpp"

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    /*!
     * \brief Singleton that is aware of all considered unknown, regardless of GodOfDof.
     *
     * Each of the unknowns must be defined in the input data file.
     */
    class UnknownManager final : public Utilities::Singleton<UnknownManager>
    {

      public:
        //! Return the name of the class.
        static const std::string& ClassName();

        //! Base type of Domain as input parameter (requested to identify domains in the input parameter data).
        using input_data_type = InputDataNS::BaseNS::Unknown;


      public:
        /// \name Special members.
        ///@{


        //! \copydoc doxygen_hide_copy_constructor
        UnknownManager(const UnknownManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UnknownManager(UnknownManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UnknownManager& operator=(const UnknownManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UnknownManager& operator=(UnknownManager&& rhs) = delete;

        ///@}


        /*!
         * \brief Create a new \a Unknown object from the data of the input data file.
         *
         * \param[in] section The section related to the \a Unknown to create in the input data file.
         */
        template<class UnknownSectionT>
        void Create(const UnknownSectionT& section);


        //! Returns the number of unknowns.
        std::size_t Nunknown() const noexcept;

        //! Get the unknown associated with \a unique_id.
        //! \unique_id_param_in_accessor{Unknown}
        const Unknown& GetUnknown(std::size_t unique_id) const noexcept;

        //! Get the unknown associated with \a unique_id as a smart pointer.
        //! \unique_id_param_in_accessor{Unknown}
        Unknown::const_shared_ptr GetUnknownPtr(std::size_t unique_id) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         *
         * \name_param_in_accessor{Unknown}
         */
        const Unknown& GetUnknown(const std::string& name) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         *
         * \name_param_in_accessor{Unknown}
         */
        Unknown::const_shared_ptr GetUnknownPtr(const std::string& name) const;

        //! Access to the list of unknowns.
        const Unknown::vector_const_shared_ptr& GetList() const noexcept;


      private:
        /*!
         * \brief Create a new Unknown, which is a unique id, a name and a nature (scalar or vectorial).
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        void Create(std::size_t unique_id, const std::string& name, const std::string& nature);

      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        UnknownManager() = default;

        //! Destructor.
        virtual ~UnknownManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<UnknownManager>;
        ///@}


      private:
        //! Register properly unknown in use.
        //! \param[in] unknown New \a Unknown to register.
        void RegisterUnknown(const Unknown::const_shared_ptr& unknown);


      private:
        //! List of unknowns.
        Unknown::vector_const_shared_ptr unknown_list_;
    };


    /*!
     * \brief Write in output directory a file that lists all the unknowns.
     *
     * Should be called only on root processor.
     *
     * \param[in] output_directory Output directory in which a file named 'unknowns.hhdata'
     * will be written.
     */
    void WriteUnknownList(const std::string& output_directory);


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/Unknown/UnknownManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_
