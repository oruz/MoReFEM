/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 13:36:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <set>
#include <type_traits>
#include <utility>
#include <vector>

#ifndef NDEBUG
#include "Utilities/Containers/Vector.hpp"
#endif // NDEBUG

#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"
#include "FiniteElement/Nodes_and_dofs/Node.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltNS
        {


            namespace // anonymous
            {

#ifndef NDEBUG
                void AssertNotCleared(bool cleared)
                {
                    assert(!cleared
                           && "Should not be called once Clear() has been called! Clear() is usually called at the "
                              "very end of Model::Initialize()");
                }
#endif // NDEBUG

            } // namespace


            Local2GlobalStorage::Local2GlobalStorage(const FElt::vector_shared_ptr& felt_list, MpiScale mpi_scale)
            : felt_list_(felt_list)
            {
                assert(std::none_of(felt_list.cbegin(), felt_list.cend(), Utilities::IsNullptr<FElt::shared_ptr>));

                const auto& numbering_subset = felt_list.back()->GetExtendedUnknown().GetNumberingSubset();

                assert(std::all_of(felt_list.cbegin(),
                                   felt_list.cend(),
                                   [&numbering_subset](const auto& felt_ptr)
                                   {
                                       return felt_ptr->GetExtendedUnknown().GetNumberingSubset() == numbering_subset;
                                   })
                       && "All finite elements here should belong to the same numbering subset!");


                auto& local_2_global_array = GetNonCstLocal2Global();
                auto& unknown_id_list = GetNonCstUnknownIdList();

                for (const auto& felt_ptr : felt_list)
                {
                    assert(!(!felt_ptr));

                    const auto& felt = *felt_ptr;

                    unknown_id_list.push_back(felt.GetExtendedUnknown().GetUnknown().GetUniqueId());

                    const auto& node_list = felt.GetNodeList();

                    assert(!node_list.empty());
                    assert(std::none_of(node_list.cbegin(), node_list.cend(), Utilities::IsNullptr<Node::shared_ptr>));

                    const auto Ncomponent = node_list.back()->Ndof();

                    for (std::size_t i = 0ul; i < Ncomponent; ++i)
                    {
                        for (const auto& node_ptr : node_list)
                        {
                            const auto& dof = node_ptr->GetDof(i);

                            switch (mpi_scale)
                            {
                            case MpiScale::program_wise:
                                local_2_global_array.push_back(
                                    static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset)));
                                break;

                            case MpiScale::processor_wise:
                                local_2_global_array.push_back(
                                    static_cast<PetscInt>(dof.GetProcessorWiseOrGhostIndex(numbering_subset)));
                                break;
                            }
                        }
                    }
                }

#ifndef NDEBUG
                {
                    auto copy = unknown_id_list;
                    Utilities::EliminateDuplicate(copy);
                    assert(copy.size() == unknown_id_list.size()
                           && "All finite elements should have been related to a "
                              "different unknown!");
                }
#endif // NDEBUG
            }


            const std::vector<PetscInt>&
            Local2GlobalStorage ::GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const
            {
                const auto& local_2_global_per_unknown_id_list = GetLocal2GlobalPerUnknownIdList();

                const auto Nunknown = unknown_list.size();

                const auto end = local_2_global_per_unknown_id_list.cend();

                auto it =
                    std::find_if(local_2_global_per_unknown_id_list.cbegin(),
                                 end,
                                 [&unknown_list, Nunknown](const auto& pair)
                                 {
                                     const auto& current_unknown_list = pair.first;

                                     if (current_unknown_list.size() != Nunknown)
                                         return false;

                                     for (std::size_t i = 0ul; i < Nunknown; ++i)
                                     {
                                         if (current_unknown_list[i] != unknown_list[i]->GetUnknown().GetUniqueId())
                                             return false;
                                     }

                                     return true;
                                 });

                assert(it != end
                       && "GetLocal2Global() can only reach values previously computed by ComputeLocal2Global(); "
                          "if the issue is related to processor-wise local2global it is likely the "
                          "DoComputeLocal2GLobalProcessorWise "
                          "argument should be set to yes. It might also be that the list of unknown is not used by any "
                          "operator, in which case it has not been pre-computed.");

                return it->second;
            }


            const std::vector<PetscInt>&
            Local2GlobalStorage ::GetLocal2Global(const ExtendedUnknown& extended_unknown) const
            {
                const auto& local_2_global_per_unknown_id_list = GetLocal2GlobalPerUnknownIdList();

                const auto end = local_2_global_per_unknown_id_list.cend();
                const auto unknown_id = extended_unknown.GetUnknown().GetUniqueId();

                auto it = std::find_if(local_2_global_per_unknown_id_list.cbegin(),
                                       end,
                                       [unknown_id](const auto& pair)
                                       {
                                           const auto& current_unknown_list = pair.first;

                                           if (current_unknown_list.size() != 1ul)
                                           {
                                               return false;
                                           }

                                           return current_unknown_list.back() == unknown_id;
                                       });

                assert(it != end
                       && "GetLocal2Global() can only reach values previously computed by ComputeLocal2Global(); "
                          "if the issue is related to processor-wise local2global it is likely the "
                          "DoComputeLocal2GLobalProcessorWise "
                          "argument should be set to yes. It might also be that the list of unknown is not used by any "
                          "operator, in which case it has not been pre-computed. You may ask somewhere (in a "
                          "GlobalVariationalOperator constructor for instance) to build it with the line:"
                          "felt_space.ComputeLocal2Global(Advanced::GlobalVariationalOperatorNS::"
                          "DetermineExtendedUnknownList(felt_space, *unknown name*),"
                          "                               DoComputeProcessorWiseLocal2Global::yes);");

                return it->second;
            }


            void
            Local2GlobalStorage::ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const
            {
#ifndef NDEBUG
                {
                    AssertNotCleared(cleared_);

                    // Check all unknowns in the list are present in the storage.
                    const auto& storage_unknown_list = GetUnknownIdList();

                    std::set<std::size_t> check;

                    for (const auto& unknown_ptr : unknown_list)
                    {
                        assert(!(!unknown_ptr));
                        const auto id = unknown_ptr->GetUnknown().GetUniqueId();

                        const auto end = storage_unknown_list.cend();

                        assert(std::find(storage_unknown_list.cbegin(), end, id) != end);

                        check.insert(id);
                    }

                    assert(check.size() == unknown_list.size() && "There should be no duplicate in unknown_list!");
                }
#endif // NDEBUG

                // Determine the vector of unknown ids.
                std::vector<std::size_t> unknown_id_list(unknown_list.size());

                std::transform(unknown_list.cbegin(),
                               unknown_list.cend(),
                               unknown_id_list.begin(),
                               [](const auto& unknown_ptr)
                               {
                                   return unknown_ptr->GetUnknown().GetUniqueId();
                               });


                auto& local_2_global_per_unknown_id_list = GetNonCstLocal2GlobalPerUnknownIdList();

                // First determine if the sequence is already stored. If so, do nothing.
                {
                    auto end = local_2_global_per_unknown_id_list.cend();
                    auto it = std::find_if(local_2_global_per_unknown_id_list.cbegin(),
                                           end,
                                           [&unknown_id_list](const auto& pair)
                                           {
                                               return pair.first == unknown_id_list;
                                           });

                    if (it != end)
                        return;
                }


                // If not, compute the local2global for this list of unknowns.
                const auto& felt_list = GetFEltList();
                const auto begin_felt_list = felt_list.cbegin();
                const auto end_felt_list = felt_list.cend();

                std::vector<PetscInt> local2global_array;

                const auto& local2global = GetLocal2Global();

#ifndef NDEBUG
                const auto end_local2global = local2global.cend();
#endif // NDEBUG

                for (const auto& unknown_ptr : unknown_list)
                {
                    const auto& current_unknown_id = unknown_ptr->GetUnknown().GetUniqueId();

                    bool found = false;

                    std::size_t current_dof_count = 0ul;

                    // Find in the finite element list the finite element matching the current unknown.
                    // There should be one by construct; an assert checks this in debug mode.
                    for (auto it = begin_felt_list; !found && it != end_felt_list; ++it)
                    {
                        const auto& felt_ptr = *it;
                        assert(!(!felt_ptr));

                        const auto& felt = *felt_ptr;

                        const auto& ref_felt_elt = felt.GetRefFElt();
                        const auto Ndof = ref_felt_elt.Ndof();

                        const auto felt_unknown_id = felt.GetExtendedUnknown().GetUnknown().GetUniqueId();

                        if (current_unknown_id == felt_unknown_id)
                        {
                            found = true;
                            auto it_begin = local2global.cbegin() + static_cast<std::ptrdiff_t>(current_dof_count);
                            auto it_end = it_begin + static_cast<std::ptrdiff_t>(Ndof);
                            assert(it_end <= end_local2global);
                            std::copy(it_begin, it_end, std::back_inserter(local2global_array));
                        }

                        current_dof_count += Ndof;
                    }

                    assert(found
                           && "unknown_list given in argument should encompass only unknowns known in the "
                              "LocalFEltSpace");
                }

                // Complete the storage with new local2global array.
                local_2_global_per_unknown_id_list.push_back(
                    std::make_pair(std::move(unknown_id_list), std::move(local2global_array)));
            }


            void Local2GlobalStorage::Clear() noexcept
            {
                assert(!cleared_ && "Clear() should be called at most once!");
                local_2_global_.clear();
                unknown_id_list_.clear();
#ifndef NDEBUG
                cleared_ = true;
#endif // NDEBUG
            }


            const std::vector<PetscInt>& Local2GlobalStorage::GetLocal2Global() const noexcept
            {
#ifndef NDEBUG
                AssertNotCleared(cleared_);
#endif // NDEBUG
                return local_2_global_;
            }


            std::vector<PetscInt>& Local2GlobalStorage::GetNonCstLocal2Global() noexcept
            {
#ifndef NDEBUG
                AssertNotCleared(cleared_);
#endif // NDEBUG
                return const_cast<std::vector<PetscInt>&>(GetLocal2Global());
            }


            const std::vector<std::size_t>& Local2GlobalStorage::GetUnknownIdList() const noexcept
            {
#ifndef NDEBUG
                AssertNotCleared(cleared_);
#endif // NDEBUG

                return unknown_id_list_;
            }


        } // namespace FEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
