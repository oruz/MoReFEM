/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 13:36:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/FiniteElement/Internal/FElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltNS
        {


            inline std::vector<std::size_t>& Local2GlobalStorage::GetNonCstUnknownIdList() noexcept
            {
                return const_cast<std::vector<std::size_t>&>(GetUnknownIdList());
            }


            inline const FElt::vector_shared_ptr& Local2GlobalStorage::GetFEltList() const noexcept
            {
                assert(!felt_list_.empty());
                return felt_list_;
            }


            inline const std::vector<std::pair<std::vector<std::size_t>, std::vector<PetscInt>>>&
            Local2GlobalStorage::GetLocal2GlobalPerUnknownIdList() const noexcept
            {
                return local_2global_per_unknown_id_list_;
            }


            inline std::vector<std::pair<std::vector<std::size_t>, std::vector<PetscInt>>>&
            Local2GlobalStorage::GetNonCstLocal2GlobalPerUnknownIdList() const noexcept
            {
                return const_cast<std::vector<std::pair<std::vector<std::size_t>, std::vector<PetscInt>>>&>(
                    GetLocal2GlobalPerUnknownIdList());
            }


        } // namespace FEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HXX_
