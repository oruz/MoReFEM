/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_F_ELT_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_F_ELT_HPP_

#include <memory>
#include <vector>

#include "FiniteElement/Nodes_and_dofs/Node.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM::Internal::FEltNS { class Local2GlobalStorage; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    /// \addtogroup FiniteElementGroup
    ///@{

    /*!
     * \brief Finite element class.
     *
     * In MoReFEM, what we call a FElt is the pendant of a \a GeometricElt for a given \a Unknown and
     * \a NumberingSubset (and implicity a shape function label, as FElt are defined in \a FEltSpace in which
     * a given unknown is associated to exactly one shape function label).
     *
     * As a result, this is a fairly internal object; most of the time (e.g. in operator definition) the
     * object of choice is the \a LocalFEltSpace, which groups all the finite elements related to a given
     * \a GeometricElt together. This is probably the object you sought before landing here: it is noticeably it
     * that is aware of the local -> global arrays.
     */
    class FElt final
    {

      public:
        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<FElt>;

        //! Alias to vector of shared pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to the only class entitled to build a new FElt.
        friend class ::MoReFEM::LocalFEltSpace;

        //! Friendship to Local2GlobalStorage; which needs to access to reference element.
        friend class FEltNS::Local2GlobalStorage;


      private:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] ref_felt Reference finite element used as the basis for the new finite element.
        explicit FElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt);

      public:
        //! Destructor.
        ~FElt() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FElt(const FElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FElt(FElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FElt& operator=(const FElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FElt& operator=(FElt&& rhs) = delete;


        ///@}

        //! Returns the related unknown and numbering subset.
        const ExtendedUnknown& GetExtendedUnknown() const noexcept;


      public:
        //! Add a new Node.
        //! \param[in] node_ptr \a Node to be added in the \a node_list_.
        //! WARNING: Despite being public, this method should never be called directly by an end-user.
        void AddNode(const Node::shared_ptr& node_ptr);

        /*!
         * \brief Return the list of Node.
         *
         * WARNING: this node list is used only upon constructor, and is erased once it is no longer used.
         * Current method should not be called past the initialization phase; in debug mode an assert makes sure
         * there are no inconsistent calls.
         */
        const Node::vector_shared_ptr& GetNodeList() const noexcept;


      private:
        //! Access to the reference felt space.
        const Internal::RefFEltNS::RefFEltInFEltSpace& GetRefFElt() const noexcept;

      private:
        //! Reference finite element.
        const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt_;

        /*!
         * \brief Store the node in the correct order.
         *
         * Local2Global can be built from here; the list is destroyed once said build has occurred.
         */
        Node::vector_shared_ptr node_list_;
    };


    ///@} // \addtogroup


} // namespace MoReFEM::Internal


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElement/Internal/FElt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_F_ELT_HPP_
