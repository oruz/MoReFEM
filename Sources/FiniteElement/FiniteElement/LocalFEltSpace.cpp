/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"
#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    LocalFEltSpace::LocalFEltSpace(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                   const GeometricElt::shared_ptr& geometric_element)
    : ref_felt_space_(ref_felt_space), geometric_elt_(geometric_element)
#ifndef NDEBUG
      ,
      do_consider_processor_wise_local_2_global_(DoComputeProcessorWiseLocal2Global::no)
#endif // NDEBUG

    {
        assert(!(!geometric_element));
    }


    LocalFEltSpace::~LocalFEltSpace() = default;


    void LocalFEltSpace::SetNodeBearerList(NodeBearer::vector_shared_ptr&& node_bearer_list)
    {
        assert(node_bearer_list_.empty() && "Should be only called once!");
        node_bearer_list_ = std::move(node_bearer_list);
    }


    Internal::FElt& LocalFEltSpace::AddFElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt)
    {
        auto felt_ptr = new Internal::FElt(ref_felt); // new rather than std::make_unique because of friendship.

        auto& felt_list = GetNonCstFEltList();

#ifndef NDEBUG
        {

            const auto& extended_unknown = ref_felt.GetExtendedUnknown();

            assert(std::find_if(felt_list.cbegin(),
                                felt_list.cend(),
                                [&extended_unknown](const auto& current_felt_ptr)
                                {
                                    return current_felt_ptr->GetExtendedUnknown() == extended_unknown;
                                })
                   == felt_list.cend());

            const auto& unknown = extended_unknown.GetUnknown();

            assert("LocalFEltSpace are supposed to be defined within a FEltSpace, in which an unknown might be present "
                   "only once (can't be associated to two different numbering subsets at this level)."
                   && std::find_if(felt_list.cbegin(),
                                   felt_list.cend(),
                                   [&unknown](const auto& current_felt_ptr)
                                   {
                                       return current_felt_ptr->GetExtendedUnknown().GetUnknown() == unknown;
                                   })
                          == felt_list.cend());
        }
#endif // NDEBUG

        felt_list.push_back(Internal::FElt::shared_ptr(felt_ptr));

        return *(felt_list.back());
    }


    bool IsLocalFEltSpaceInDomain(const LocalFEltSpace& local_felt_space, const Domain& domain) noexcept
    {
        return domain.IsGeometricEltInside(local_felt_space.GetGeometricElt());
    }


    void
    LocalFEltSpace ::InitLocal2Global(const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                      DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global)
    {
        const auto& felt_list = GetFEltList();

        const auto begin = felt_list.cbegin();
        const auto end = felt_list.cend();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            Internal::FElt::vector_shared_ptr reduced_felt_list;

            {
                std::copy_if(begin,
                             end,
                             std::back_inserter(reduced_felt_list),
                             [&numbering_subset](const auto& felt_ptr)
                             {
                                 return felt_ptr->GetExtendedUnknown().GetNumberingSubset() == numbering_subset;
                             });

                assert(!reduced_felt_list.empty());
            }

            const auto numbering_subset_id = numbering_subset.GetUniqueId();

            {
                assert(std::none_of(local_2_global_program_wise_per_numbering_subset_.cbegin(),
                                    local_2_global_program_wise_per_numbering_subset_.cend(),
                                    [numbering_subset_id](const auto& pair)
                                    {
                                        return pair.first == numbering_subset_id;
                                    })
                       && "Each numbering subset should be present as key only once.");


                auto&& storage =
                    std::make_unique<Internal::FEltNS::Local2GlobalStorage>(reduced_felt_list, MpiScale::program_wise);

                local_2_global_program_wise_per_numbering_subset_.emplace_back(
                    std::make_pair(numbering_subset_id, std::move(storage)));
            }

            if (do_consider_processor_wise_local_2_global == DoConsiderProcessorWiseLocal2Global::yes)
            {
                assert(std::none_of(local_2_global_processor_wise_per_numbering_subset_.cbegin(),
                                    local_2_global_processor_wise_per_numbering_subset_.cend(),
                                    [numbering_subset_id](const auto& pair)
                                    {
                                        return pair.first == numbering_subset_id;
                                    })
                       && "Each numbering subset should be present as key only once.");

                auto&& storage = std::make_unique<Internal::FEltNS::Local2GlobalStorage>(reduced_felt_list,
                                                                                         MpiScale::processor_wise);

                local_2_global_processor_wise_per_numbering_subset_.emplace_back(
                    std::make_pair(numbering_subset_id, std::move(storage)));
            }
        }
    }


    void
    LocalFEltSpace::ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                        DoComputeProcessorWiseLocal2Global do_consider_processor_wise_local_2_global)
    {
        assert(!unknown_list.empty());
        assert(std::none_of(
            unknown_list.cbegin(), unknown_list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

        const auto& numbering_subset = unknown_list.back()->GetNumberingSubset();

        assert(std::all_of(unknown_list.cbegin(),
                           unknown_list.cend(),
                           [&numbering_subset](const auto& unknown_ptr)
                           {
                               return unknown_ptr->GetNumberingSubset() == numbering_subset;
                           })
               && "This method should be called for one and only one numbering subset!");

        {
            auto& storage = GetNonCstLocal2GlobalStorageForNumberingSubset<MpiScale::program_wise>(numbering_subset);
            storage.ComputeLocal2Global(unknown_list);
        }

        if (do_consider_processor_wise_local_2_global == DoComputeProcessorWiseLocal2Global::yes)
        {
            auto& storage = GetNonCstLocal2GlobalStorageForNumberingSubset<MpiScale::processor_wise>(numbering_subset);
            storage.ComputeLocal2Global(unknown_list);
        }
    }


    namespace // anonymous
    {


        void ClearHelper(const std::vector<std::pair<std::size_t, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>&
                             container_to_clear)
        {
            for (const auto& pair : container_to_clear)
            {
                assert(!(!pair.second));
                pair.second->Clear();
            }
        }


    } // namespace


    void LocalFEltSpace::ClearTemporaryData() const noexcept
    {
        ClearHelper(GetLocal2GlobalStorage<MpiScale::program_wise>());
        ClearHelper(GetLocal2GlobalStorage<MpiScale::processor_wise>());
    }


    bool operator<(const LocalFEltSpace& lhs, const LocalFEltSpace& rhs) noexcept
    {
        return lhs.GetGeometricElt() < rhs.GetGeometricElt();
    }


    bool operator==(const LocalFEltSpace& lhs, const LocalFEltSpace& rhs) noexcept
    {
        return lhs.GetGeometricElt() == rhs.GetGeometricElt();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
