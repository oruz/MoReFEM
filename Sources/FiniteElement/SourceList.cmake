### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/BoundaryConditions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FiniteElement/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FiniteElementSpace/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Nodes_and_dofs/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/QuadratureRules/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/RefFiniteElement/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Unknown/SourceList.cmake)
