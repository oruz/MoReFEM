/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 11:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <memory>

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    /*!
     * \brief This class list the quadrature rule to use fo each topology.
     *
     * It is intended to be used either in FEltSpace level or in GlobalVariationalOperator level (the latter supersedes
     * the former if specified; if not the operator takes the set of rules defines within its FEltSpace).
     */
    class QuadratureRulePerTopology
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = QuadratureRulePerTopology;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Convenient alias.
        using storage_type = std::map<RefGeomEltNS::TopologyNS::Type, QuadratureRule::const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] quadrature_rule_per_topology Quadrature rule to use for each topology.
         */
        explicit QuadratureRulePerTopology(storage_type&& quadrature_rule_per_topology);


        /*!
         * \brief Constructor to produce default choices for each topology.
         *
         * \param[in] degree_of_exactness Parameter used to determine rule to use for topologies that are built upon
         * PerDegreeOfExactness process.
         * \param[in] shape_function_order Parameter used to determine rule to use for topologies that are built upon
         * PerShapeFunctionOrder process.
         */
        explicit QuadratureRulePerTopology(std::size_t degree_of_exactness, std::size_t shape_function_order);


        //! Destructor.
        ~QuadratureRulePerTopology() = default;

        //! \copydoc doxygen_hide_copy_constructor
        QuadratureRulePerTopology(const QuadratureRulePerTopology& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        QuadratureRulePerTopology(QuadratureRulePerTopology&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        QuadratureRulePerTopology& operator=(const QuadratureRulePerTopology& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        QuadratureRulePerTopology& operator=(QuadratureRulePerTopology&& rhs) = delete;

        ///@}


        /*!
         * \brief Print the list of quadrature rules in storage.
         *
         * This method is mostly there for dev purposes.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const noexcept;


        //! Access to the list of quadrature rule to use for each topology.
        const storage_type& GetRulePerTopology() const noexcept;

        //! Return the rule to use with a given topology.
        //! \param[in] topology Topology for which the \a QuadratureRule is sought.
        const QuadratureRule& GetRule(RefGeomEltNS::TopologyNS::Type topology) const;


      private:
        /*!
         * \brief Quadrature rule to use for each topology.
         */
        const storage_type quadrature_rule_per_topology_;
    };


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_
