/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 10:40:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace GaussQuadratureNS
        {


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Ignored as I do not have a clue about most of the function arguments...
            // ============================

            /*!
             * \brief Intermediary function used for the computation of Gauss points
             *
             *     computes pn(theta) and its derivative dpb(theta) with
             *     respect to theta
             *
             *     computes the fourier coefficients of the legendre
             *     polynomial p_n0 and its derivative.
             *     n is the degree and n/2 or (n+1)/2
             *     coefficients are returned in cp depending on whether
             *     n is even or odd. The same number of coefficients
             *     are returned in dcp. For n even the constant
             *     coefficient is returned in cz.
             *
             * \param[out] cp This container is already allocated in input; itss size is the number of quadrature
             * points. \param[out] dcp This container is already allocated in input; itss size is the number of
             * quadrature points. \param[out] ddcp This container is already allocated in input; itss size is the number
             * of quadrature points.
             *
             * n in the text above is the size of either of the container involved.
             */
            void ComputeFourierCoef(double& cz,
                                    std::vector<double>& cp,
                                    std::vector<double>& dcp,
                                    std::vector<double>& ddcp);


            /*!
             * \brief Intermediary function used for the computation of Gauss points
             *
             *  Computes pn(theta) and its derivative dpb(theta) with respect to theta.
             */
            void ComputeLegendrePolAndDerivative(std::size_t n,
                                                 double theta,
                                                 double cz,
                                                 const std::vector<double>& cp,
                                                 const std::vector<double>& dcp,
                                                 const std::vector<double>& ddcp,
                                                 double& pb,
                                                 double& dpb,
                                                 double& ddpb);


            /*!
             * \brief Compute exact formula.
             *
             * \tparam QuadratureFormulaT Whether Gauss or Gauss-Lobatto is considered/
             * \warning Only Gauss-Lobatto one has been checked; other one was taken from Ondomatic
             * but not used at all.
             * \tparam I Used to determine the number of quadrature points to use.
             * In Gauss-Lobatto, I + 1 7quadrature points are used.
             * In Gauss, I quadrature points are used.
             *
             */
            template<QuadratureNS::GaussFormula QuadratureFormulaT, std::size_t I>
            struct ComputeExactFormula;


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss_Lobatto, 1u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss_Lobatto, 2u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss_Lobatto, 3u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 1u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 2u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            template<>
            struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 3u>
            {

                static void Perform(std::vector<double>& points, std::vector<double>& weights);
            };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace GaussQuadratureNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INTERNAL_x_GAUSS_QUADRATURE_FORMULA_HPP_
