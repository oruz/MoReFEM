/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Apr 2014 14:51:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_POINT_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_POINT_HXX_

// IWYU pragma: private, include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

namespace MoReFEM
{


    inline double QuadraturePoint::GetWeight() const noexcept
    {
        return weight_;
    }


    inline const std::string& QuadraturePoint::GetRuleName() const
    {
        return rule_name_;
    }


    inline std::size_t QuadraturePoint::GetIndex() const noexcept
    {
        return index_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_POINT_HXX_
