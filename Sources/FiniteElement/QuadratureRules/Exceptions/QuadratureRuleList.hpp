/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_EXCEPTIONS_x_QUADRATURE_RULE_LIST_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_EXCEPTIONS_x_QUADRATURE_RULE_LIST_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace QuadratureRuleListNS
        {


            //! Generic GetQuadratureRule exception.
            class Exception : public MoReFEM::Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! \copydoc doxygen_hide_copy_constructor
                Exception(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Exception(Exception&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Exception& operator=(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Exception& operator=(Exception&& rhs) = default;
            };


            /*!
             * \brief When no quadrature rule with the exact degree of exactness specified is available.
             */
            class InvalidDegree final : public Exception
            {
              public:
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] degree Degree of exactness required by the user.
                 * \param[in] max_degree Maximum degree of exactness required by the user.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit InvalidDegree(std::size_t degree,
                                       std::size_t max_degree,
                                       const char* invoking_file,
                                       int invoking_line);

                //! Destructor
                virtual ~InvalidDegree() override;

                //! \copydoc doxygen_hide_copy_constructor
                InvalidDegree(const InvalidDegree& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                InvalidDegree(InvalidDegree&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                InvalidDegree& operator=(const InvalidDegree& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                InvalidDegree& operator=(InvalidDegree&& rhs) = default;
            };


        } // namespace QuadratureRuleListNS


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_EXCEPTIONS_x_QUADRATURE_RULE_LIST_HPP_
