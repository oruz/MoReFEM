//! \file
//
//
//  FwdForCpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 24/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_

#include <array>       // IWYU pragma: export
#include <cassert>     // IWYU pragma: export
#include <cmath>       // IWYU pragma: export
#include <iosfwd>      // IWYU pragma: export
#include <memory>      // IWYU pragma: export
#include <string>      // IWYU pragma: export
#include <type_traits> // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp" // IWYU pragma: export
#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"     // IWYU pragma: export
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"             // IWYU pragma: export

#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_
