/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 13:42:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep
#include "FiniteElement/QuadratureRules/Instantiation/Segment.hpp"


namespace MoReFEM
{


    namespace QuadratureNS
    {


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness();


        } // namespace


        const std::array<QuadratureRule::const_shared_ptr, 3>& Segment::GetPerDegreeOfExactnessList()
        {
            static const std::array<QuadratureRule::const_shared_ptr, 3> ret =
                CreateQuadratureRuleListPerDegreeOfExactness();

            return ret;
        }


        QuadratureRule::const_shared_ptr Segment::GetShapeFunctionOrder(std::size_t order)
        {
            auto ret = std::make_shared<QuadratureRule>(std::string("segment_") + std::to_string(order) + "_points",
                                                        RefGeomEltNS::TopologyNS::Type::segment);

            {
                auto& n_points = *ret;

                std::vector<double> points;
                std::vector<double> weights;

                const std::size_t Ngauss_points = order + 1u;
                QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss>(Ngauss_points, points, weights);

                const auto Npoints = points.size();

                for (auto i = 0ul; i < Npoints; ++i)
                    n_points.AddQuadraturePoint(LocalCoords({ points[i] }), weights[i]);
            }

            return ret;
        }


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness()
            {

                auto one_point_ptr =
                    std::make_shared<QuadratureRule>("segment_1_point", RefGeomEltNS::TopologyNS::Type::segment, 1);

                one_point_ptr->AddQuadraturePoint(LocalCoords({ 0. }), 2.);


                auto two_points_ptr =
                    std::make_shared<QuadratureRule>("segment_2_points", RefGeomEltNS::TopologyNS::Type::segment, 3);

                {
                    auto& two_points = *two_points_ptr;
                    const double alpha = std::sqrt(1. / 3.);
                    two_points.AddQuadraturePoint(LocalCoords({ -alpha }), 1.);
                    two_points.AddQuadraturePoint(LocalCoords({ alpha }), 1.);
                }


                auto three_points_ptr =
                    std::make_shared<QuadratureRule>("segment_3_points", RefGeomEltNS::TopologyNS::Type::segment, 4);


                {
                    auto& three_points = *three_points_ptr;

                    const double alpha = std::sqrt(3. / 5.);
                    const double one_ninth = 1. / 9.;
                    const double five_ninth = 5. * one_ninth;

                    three_points.AddQuadraturePoint(LocalCoords({ 0. }), 8. * one_ninth);
                    three_points.AddQuadraturePoint(LocalCoords({ -alpha }), five_ninth);
                    three_points.AddQuadraturePoint(LocalCoords({ alpha }), five_ninth);
                }


                assert(one_point_ptr->NquadraturePoint() == 1u);
                assert(two_points_ptr->NquadraturePoint() == 2u);
                assert(three_points_ptr->NquadraturePoint() == 3u);

                return { { one_point_ptr, two_points_ptr, three_points_ptr } };
            }


        } // namespace


    } //  namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
