/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 May 2013 17:33:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep
#include "FiniteElement/QuadratureRules/Instantiation/Quadrangle.hpp"


namespace MoReFEM
{


    namespace QuadratureNS
    {


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness();


        } // namespace


        const std::array<QuadratureRule::const_shared_ptr, 3>& Quadrangle::GetPerDegreeOfExactnessList()
        {
            static const std::array<QuadratureRule::const_shared_ptr, 3> ret =
                CreateQuadratureRuleListPerDegreeOfExactness();

            return ret;
        }


        QuadratureRule::const_shared_ptr Quadrangle::GetShapeFunctionOrder(std::size_t order)
        {
            auto ret = std::make_shared<QuadratureRule>(std::string("quadrangle_") + std::to_string(order) + "_points",
                                                        RefGeomEltNS::TopologyNS::Type::quadrangle);

            {
                auto& n_points = *ret;

                std::vector<double> points;
                std::vector<double> weights;

                const std::size_t Ngauss_points = order + 1u;
                QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss>(Ngauss_points, points, weights);

                const auto Npoints = points.size();

                for (auto i = 0ul; i < Npoints; ++i)
                {
                    for (auto j = 0ul; j < Npoints; ++j)
                        n_points.AddQuadraturePoint(LocalCoords({ points[j], points[i] }), weights[i] * weights[j]);
                }
            }

            return ret;
        }


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness()
            {

                auto one_point_ptr = std::make_shared<QuadratureRule>(
                    "quadrangle_1_point", RefGeomEltNS::TopologyNS::Type::quadrangle, 1);
                {
                    one_point_ptr->AddQuadraturePoint(LocalCoords({ 0., 0. }), .5);
                }


                auto four_points_ptr = std::make_shared<QuadratureRule>(
                    "quadrangle_4_points", RefGeomEltNS::TopologyNS::Type::quadrangle, 3);

                {
                    auto& four_points = *four_points_ptr;

                    const double q2ptx1 = -std::sqrt(1. / 3.);
                    const double q2ptx2 = -q2ptx1;

                    four_points.AddQuadraturePoint(LocalCoords({ q2ptx1, q2ptx1 }), 1.);
                    four_points.AddQuadraturePoint(LocalCoords({ q2ptx2, q2ptx1 }), 1.);
                    four_points.AddQuadraturePoint(LocalCoords({ q2ptx2, q2ptx2 }), 1.);
                    four_points.AddQuadraturePoint(LocalCoords({ q2ptx1, q2ptx2 }), 1.);
                }


                auto nine_points_ptr = std::make_shared<QuadratureRule>(
                    "quadrangle_9_points", RefGeomEltNS::TopologyNS::Type::quadrangle, 5);

                {
                    auto& nine_points = *nine_points_ptr;

                    const double q3ptx1 = 0., q3ptx2 = -std::sqrt(3. / 5.), q3ptx3 = -q3ptx2;
                    const double one_ninth = 1. / 9.;

                    const double q3ptw1 = 8 * one_ninth;
                    const double q3ptw2 = 5 * one_ninth;
                    const double q3ptw3 = 5 * one_ninth;

                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx1 }), q3ptw1 * q3ptw1);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx1 }), q3ptw2 * q3ptw1);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx1 }), q3ptw3 * q3ptw1);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx2 }), q3ptw1 * q3ptw2);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx2 }), q3ptw2 * q3ptw2);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx2 }), q3ptw3 * q3ptw2);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx3 }), q3ptw1 * q3ptw3);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx3 }), q3ptw2 * q3ptw3);
                    nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx3 }), q3ptw3 * q3ptw3);
                }

                assert(one_point_ptr->NquadraturePoint() == 1u);
                assert(four_points_ptr->NquadraturePoint() == 4u);
                assert(nine_points_ptr->NquadraturePoint() == 9u);

                return { { one_point_ptr, four_points_ptr, nine_points_ptr } };
            }


        } // namespace


    } //  namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
