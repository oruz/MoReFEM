/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 12:32:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "FiniteElement/QuadratureRules/Instantiation/Point.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace QuadratureNS
    {


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 1> CreateQuadratureRuleListPerDegreeOfExactness()
            {
                auto one_point_ptr =
                    std::make_shared<QuadratureRule>("point", RefGeomEltNS::TopologyNS::Type::point, 1);
                one_point_ptr->AddQuadraturePoint(LocalCoords({}), 1.);


                return { { one_point_ptr } };
            }


        } // namespace


        const std::array<QuadratureRule::const_shared_ptr, 1>& Point::GetPerDegreeOfExactnessList()
        {
            static const std::array<QuadratureRule::const_shared_ptr, 1> ret =
                CreateQuadratureRuleListPerDegreeOfExactness();

            return ret;
        }


    } //  namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
