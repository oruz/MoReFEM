/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 12:04:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include "FiniteElement/QuadratureRules/Instantiation/Tetrahedron.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace QuadratureNS
    {


        namespace // anonymous
        {


            const std::array<QuadratureRule::const_shared_ptr, 5> CreateQuadratureRuleListPerDegreeOfExactness()
            {
                auto one_point_ptr = std::make_shared<QuadratureRule>(
                    "tetrahedron_1_point", RefGeomEltNS::TopologyNS::Type::tetrahedron, 1);

                {
                    one_point_ptr->AddQuadraturePoint(LocalCoords({ .25, .25, .25 }), 1. / 6.);
                }


                auto four_points_ptr = std::make_shared<QuadratureRule>(
                    "tetrahedron_4_points", RefGeomEltNS::TopologyNS::Type::tetrahedron, 2);

                {
                    auto& four_points = *four_points_ptr;

                    const double alpha = std::sqrt(5.) / 20.;
                    const double beta = 0.25 - alpha;
                    const double gamma = 0.25 + 3. * alpha;
                    const double weight = 1. / 24.;

                    four_points.AddQuadraturePoint(LocalCoords({ beta, beta, beta }), weight);
                    four_points.AddQuadraturePoint(LocalCoords({ beta, beta, gamma }), weight);
                    four_points.AddQuadraturePoint(LocalCoords({ beta, gamma, beta }), weight);
                    four_points.AddQuadraturePoint(LocalCoords({ gamma, beta, beta }), weight);
                }


                // (Comincioli page 236)
                auto five_points_ptr = std::make_shared<QuadratureRule>(
                    "tetrahedron_5_points", RefGeomEltNS::TopologyNS::Type::tetrahedron, 3);

                {
                    auto& five_points = *five_points_ptr;

                    const double one_sixth = 1. / 6.;
                    const double alpha = 1. / 120.;
                    const double weight_1 = 9. * alpha;

                    five_points.AddQuadraturePoint(LocalCoords({ one_sixth, one_sixth, one_sixth }), weight_1);
                    five_points.AddQuadraturePoint(LocalCoords({ one_sixth, one_sixth, .5 }), weight_1);
                    five_points.AddQuadraturePoint(LocalCoords({ one_sixth, .5, one_sixth }), weight_1);
                    five_points.AddQuadraturePoint(LocalCoords({ .5, one_sixth, one_sixth }), weight_1);
                    five_points.AddQuadraturePoint(LocalCoords({ .25, .25, .25 }), -16. * alpha);
                }


                // (Stroud, T3:5-1 page 315)
                //            QuadratureRule FifteenPoints()
                auto fifteen_points_ptr = std::make_shared<QuadratureRule>(
                    "tetrahedron_15_points", RefGeomEltNS::TopologyNS::Type::tetrahedron, 5);

                {
                    auto& fifteen_points = *fifteen_points_ptr;

                    const double r5 = 0.25;
                    const std::array<double, 2> s5{ { 0.09197107805272303, 0.3197936278296299 } };

                    const std::array<double, 2> t5{ { 0.7240867658418310, 0.04061911651111023 } };

                    const double u5 = 0.05635083268962915;
                    const double v5 = 0.4436491673103708;
                    const double A5 = 0.01975308641975309;

                    const std::array<double, 2> B5{ { 0.01198951396316977, 0.01151136787104540 } };

                    const double C5 = 0.008818342151675485;

                    fifteen_points.AddQuadraturePoint(LocalCoords({ r5, r5, r5 }), A5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[0], s5[0], s5[0] }), B5[0]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ t5[0], s5[0], s5[0] }), B5[0]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[0], t5[0], s5[0] }), B5[0]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[0], s5[0], t5[0] }), B5[0]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[1], s5[1], s5[1] }), B5[1]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ t5[1], s5[1], s5[1] }), B5[1]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[1], t5[1], s5[1] }), B5[1]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ s5[1], s5[1], t5[1] }), B5[1]);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ u5, u5, v5 }), C5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ u5, v5, u5 }), C5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ v5, u5, u5 }), C5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ v5, v5, u5 }), C5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ v5, u5, v5 }), C5);
                    fifteen_points.AddQuadraturePoint(LocalCoords({ u5, v5, v5 }), C5);
                }


                // (Stroud, T3:7-1 page 315)
                //            QuadratureRule SixtyFourPoints()
                auto sixty_four_points_ptr = std::make_shared<QuadratureRule>(
                    "tetrahedron_64_points", RefGeomEltNS::TopologyNS::Type::tetrahedron, 7);


                {
                    auto& sixty_four_points = *sixty_four_points_ptr;

                    const std::array<double, 4> t{ { 0.0485005494, 0.2386007376, 0.5170472951, 0.7958514179 } };

                    const std::array<double, 4> s{ { 0.0571041961, 0.2768430136, 0.5835904324, 0.8602401357 } };

                    const std::array<double, 4> r{ { 0.0694318422, 0.3300094782, 0.6699905218, 0.9305681558 } };

                    const std::array<double, 4> A{ { 0.1739274226, 0.3260725774, 0.3260725774, 0.1739274226 } };

                    const std::array<double, 4> B{ { 0.1355069134, 0.2034645680, 0.1298475476, 0.0311809709 } };

                    const std::array<double, 4> C{ { 0.1108884156, 0.1434587898, 0.0686338872, 0.0103522407 } };


                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[0] * (1 - t[0]), r[0] * (1 - s[0]) * (1 - t[0]) }), A[0] * B[0] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[0] * (1 - t[1]), r[0] * (1 - s[0]) * (1 - t[1]) }), A[0] * B[0] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[0] * (1 - t[2]), r[0] * (1 - s[0]) * (1 - t[2]) }), A[0] * B[0] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[0] * (1 - t[3]), r[0] * (1 - s[0]) * (1 - t[3]) }), A[0] * B[0] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[1] * (1 - t[0]), r[0] * (1 - s[1]) * (1 - t[0]) }), A[0] * B[1] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[1] * (1 - t[1]), r[0] * (1 - s[1]) * (1 - t[1]) }), A[0] * B[1] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[1] * (1 - t[2]), r[0] * (1 - s[1]) * (1 - t[2]) }), A[0] * B[1] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[1] * (1 - t[3]), r[0] * (1 - s[1]) * (1 - t[3]) }), A[0] * B[1] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[2] * (1 - t[0]), r[0] * (1 - s[2]) * (1 - t[0]) }), A[0] * B[2] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[2] * (1 - t[1]), r[0] * (1 - s[2]) * (1 - t[1]) }), A[0] * B[2] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[2] * (1 - t[2]), r[0] * (1 - s[2]) * (1 - t[2]) }), A[0] * B[2] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[2] * (1 - t[3]), r[0] * (1 - s[2]) * (1 - t[3]) }), A[0] * B[2] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[3] * (1 - t[0]), r[0] * (1 - s[3]) * (1 - t[0]) }), A[0] * B[3] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[3] * (1 - t[1]), r[0] * (1 - s[3]) * (1 - t[1]) }), A[0] * B[3] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[3] * (1 - t[2]), r[0] * (1 - s[3]) * (1 - t[2]) }), A[0] * B[3] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[3] * (1 - t[3]), r[0] * (1 - s[3]) * (1 - t[3]) }), A[0] * B[3] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[0] * (1 - t[0]), r[1] * (1 - s[0]) * (1 - t[0]) }), A[1] * B[0] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[0] * (1 - t[1]), r[1] * (1 - s[0]) * (1 - t[1]) }), A[1] * B[0] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[0] * (1 - t[2]), r[1] * (1 - s[0]) * (1 - t[2]) }), A[1] * B[0] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[0] * (1 - t[3]), r[1] * (1 - s[0]) * (1 - t[3]) }), A[1] * B[0] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[1] * (1 - t[0]), r[1] * (1 - s[1]) * (1 - t[0]) }), A[1] * B[1] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[1] * (1 - t[1]), r[1] * (1 - s[1]) * (1 - t[1]) }), A[1] * B[1] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[1] * (1 - t[2]), r[1] * (1 - s[1]) * (1 - t[2]) }), A[1] * B[1] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[1] * (1 - t[3]), r[1] * (1 - s[1]) * (1 - t[3]) }), A[1] * B[1] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[2] * (1 - t[0]), r[1] * (1 - s[2]) * (1 - t[0]) }), A[1] * B[2] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[2] * (1 - t[1]), r[1] * (1 - s[2]) * (1 - t[1]) }), A[1] * B[2] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[2] * (1 - t[2]), r[1] * (1 - s[2]) * (1 - t[2]) }), A[1] * B[2] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[2] * (1 - t[3]), r[1] * (1 - s[2]) * (1 - t[3]) }), A[1] * B[2] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[3] * (1 - t[0]), r[1] * (1 - s[3]) * (1 - t[0]) }), A[1] * B[3] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[3] * (1 - t[1]), r[1] * (1 - s[3]) * (1 - t[1]) }), A[1] * B[3] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[3] * (1 - t[2]), r[1] * (1 - s[3]) * (1 - t[2]) }), A[1] * B[3] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[3] * (1 - t[3]), r[1] * (1 - s[3]) * (1 - t[3]) }), A[1] * B[3] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[0] * (1 - t[0]), r[2] * (1 - s[0]) * (1 - t[0]) }), A[2] * B[0] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[0] * (1 - t[1]), r[2] * (1 - s[0]) * (1 - t[1]) }), A[2] * B[0] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[0] * (1 - t[2]), r[2] * (1 - s[0]) * (1 - t[2]) }), A[2] * B[0] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[0] * (1 - t[3]), r[2] * (1 - s[0]) * (1 - t[3]) }), A[2] * B[0] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[1] * (1 - t[0]), r[2] * (1 - s[1]) * (1 - t[0]) }), A[2] * B[1] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[1] * (1 - t[1]), r[2] * (1 - s[1]) * (1 - t[1]) }), A[2] * B[1] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[1] * (1 - t[2]), r[2] * (1 - s[1]) * (1 - t[2]) }), A[2] * B[1] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[1] * (1 - t[3]), r[2] * (1 - s[1]) * (1 - t[3]) }), A[2] * B[1] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[2] * (1 - t[0]), r[2] * (1 - s[2]) * (1 - t[0]) }), A[2] * B[2] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[2] * (1 - t[1]), r[2] * (1 - s[2]) * (1 - t[1]) }), A[2] * B[2] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[2] * (1 - t[2]), r[2] * (1 - s[2]) * (1 - t[2]) }), A[2] * B[2] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[2] * (1 - t[3]), r[2] * (1 - s[2]) * (1 - t[3]) }), A[2] * B[2] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[3] * (1 - t[0]), r[2] * (1 - s[3]) * (1 - t[0]) }), A[2] * B[3] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[3] * (1 - t[1]), r[2] * (1 - s[3]) * (1 - t[1]) }), A[2] * B[3] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[3] * (1 - t[2]), r[2] * (1 - s[3]) * (1 - t[2]) }), A[2] * B[3] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[3] * (1 - t[3]), r[2] * (1 - s[3]) * (1 - t[3]) }), A[2] * B[3] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[0] * (1 - t[0]), r[3] * (1 - s[0]) * (1 - t[0]) }), A[3] * B[0] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[0] * (1 - t[1]), r[3] * (1 - s[0]) * (1 - t[1]) }), A[3] * B[0] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[0] * (1 - t[2]), r[3] * (1 - s[0]) * (1 - t[2]) }), A[3] * B[0] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[0] * (1 - t[3]), r[3] * (1 - s[0]) * (1 - t[3]) }), A[3] * B[0] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[1] * (1 - t[0]), r[3] * (1 - s[1]) * (1 - t[0]) }), A[3] * B[1] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[1] * (1 - t[1]), r[3] * (1 - s[1]) * (1 - t[1]) }), A[3] * B[1] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[1] * (1 - t[2]), r[3] * (1 - s[1]) * (1 - t[2]) }), A[3] * B[1] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[1] * (1 - t[3]), r[3] * (1 - s[1]) * (1 - t[3]) }), A[3] * B[1] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[2] * (1 - t[0]), r[3] * (1 - s[2]) * (1 - t[0]) }), A[3] * B[2] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[2] * (1 - t[1]), r[3] * (1 - s[2]) * (1 - t[1]) }), A[3] * B[2] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[2] * (1 - t[2]), r[3] * (1 - s[2]) * (1 - t[2]) }), A[3] * B[2] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[2] * (1 - t[3]), r[3] * (1 - s[2]) * (1 - t[3]) }), A[3] * B[2] * C[3]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[0], s[3] * (1 - t[0]), r[3] * (1 - s[3]) * (1 - t[0]) }), A[3] * B[3] * C[0]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[1], s[3] * (1 - t[1]), r[3] * (1 - s[3]) * (1 - t[1]) }), A[3] * B[3] * C[1]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[2], s[3] * (1 - t[2]), r[3] * (1 - s[3]) * (1 - t[2]) }), A[3] * B[3] * C[2]);
                    sixty_four_points.AddQuadraturePoint(
                        LocalCoords({ t[3], s[3] * (1 - t[3]), r[3] * (1 - s[3]) * (1 - t[3]) }), A[3] * B[3] * C[3]);
                }


                assert(one_point_ptr->NquadraturePoint() == 1u);
                assert(four_points_ptr->NquadraturePoint() == 4u);
                assert(five_points_ptr->NquadraturePoint() == 5u);
                assert(fifteen_points_ptr->NquadraturePoint() == 15u);
                assert(sixty_four_points_ptr->NquadraturePoint() == 64u);

                return {
                    { one_point_ptr, four_points_ptr, five_points_ptr, fifteen_points_ptr, sixty_four_points_ptr }
                };
            }


        } // namespace


        const std::array<QuadratureRule::const_shared_ptr, 5>& Tetrahedron::GetPerDegreeOfExactnessList()
        {
            static const std::array<QuadratureRule::const_shared_ptr, 5> ret =
                CreateQuadratureRuleListPerDegreeOfExactness();

            return ret;
        }


    } //  namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
