/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 May 2013 17:33:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_QUADRANGLE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_QUADRANGLE_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerShapeFunctionOrder.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    namespace QuadratureNS
    {


        //! CRTP-defined class for the quadrature rules that concern Quadrangles.
        struct Quadrangle : public Advanced::QuadratureRuleNS::PerShapeFunctionOrder<Quadrangle> //,
        // public Advanced::QuadratureRuleNS::PerDegreeOfExactness<Quadrangle>
        {

            //! \copydoc doxygen_hide_quadrature_rule_degree_of_exactness_list_method
            static const std::array<QuadratureRule::const_shared_ptr, 3>& GetPerDegreeOfExactnessList();


            //! \copydoc doxygen_hide_quadrature_rule_shape_function_order_method
            static QuadratureRule::const_shared_ptr GetShapeFunctionOrder(std::size_t order);
        };


    } // namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_QUADRANGLE_HPP_
