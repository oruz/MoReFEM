/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 12:04:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_HEXAHEDRON_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_HEXAHEDRON_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hpp"
#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerShapeFunctionOrder.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    namespace QuadratureNS
    {


        //! CRTP-defined class for the quadrature rules that concern Hexahedrons.
        struct Hexahedron : public Advanced::QuadratureRuleNS::PerShapeFunctionOrder<Hexahedron>,
                            public Advanced::QuadratureRuleNS::PerDegreeOfExactness<Hexahedron>
        {

            //! \copydoc doxygen_hide_quadrature_rule_degree_of_exactness_list_method
            static const std::array<QuadratureRule::const_shared_ptr, 3>& GetPerDegreeOfExactnessList();

            //! \copydoc doxygen_hide_quadrature_rule_shape_function_order_method
            static QuadratureRule::const_shared_ptr GetShapeFunctionOrder(std::size_t order);
        };


    } // namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_INSTANTIATION_x_HEXAHEDRON_HPP_
