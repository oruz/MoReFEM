/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 10:38:07 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <type_traits>

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    QuadratureRule::QuadratureRule(std::string&& name,
                                   QuadraturePoint::vector_const_shared_ptr&& point_list,
                                   RefGeomEltNS::TopologyNS::Type topology_id,
                                   std::size_t degree_of_exactness)
    : name_(std::move(name)), point_list_(std::move(point_list)), topology_id_(topology_id),
      degree_of_exactness_(degree_of_exactness)
    { }


    QuadratureRule::QuadratureRule(std::string&& name,
                                   RefGeomEltNS::TopologyNS::Type topology_id,
                                   std::size_t degree_of_exactness)
    : name_(std::move(name)), topology_id_(topology_id), degree_of_exactness_(degree_of_exactness)
    { }


    QuadratureRule::QuadratureRule(RefGeomEltNS::TopologyNS::Type topology_id, std::size_t degree_of_exactness)
    : topology_id_(topology_id), degree_of_exactness_(degree_of_exactness)
    { }


    void QuadratureRule::AddQuadraturePoint(LocalCoords&& local_coords, double weight)
    {
        auto&& quad_pt =
            std::make_shared<const QuadraturePoint>(std::move(local_coords), weight, GetName(), point_list_.size());

        point_list_.emplace_back(std::move(quad_pt));
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
