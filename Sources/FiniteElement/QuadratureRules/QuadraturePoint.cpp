/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 May 2013 16:15:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <type_traits>

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"


namespace MoReFEM
{


    QuadraturePoint::QuadraturePoint(LocalCoords&& local_coords,
                                     double weight,
                                     const std::string& rule_name,
                                     const std::size_t index)
    : LocalCoords(std::move(local_coords)), weight_(weight), rule_name_(rule_name), index_(index)
    { }


    QuadraturePoint::~QuadraturePoint() = default;


    void QuadraturePoint::Print(std::ostream& out) const
    {
        LocalCoords::Print(out);
        out << " (weight = " << GetWeight() << ')';
    }


    std::ostream& operator<<(std::ostream& stream, const QuadraturePoint& point)
    {
        point.Print(stream);
        return stream;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
