/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 10:25:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HPP_

#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <numeric>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/Internal/GaussQuadratureFormula.hpp"


namespace MoReFEM
{


    namespace QuadratureNS
    {


        /// \addtogroup FiniteElementGroup
        ///@{


        /*!
         * \brief Computation of Gauss quadrature formula.
         *
         * \tparam QuadratureFormulaT Variant of Gauss quadrature formula to use.
         *
         * \param[in] Npoints_gauss Number of Gauss points to consider.
         * \param[out] points Values for each of the quadrature point.
         * \param[out] weights Weight for each of the quadrature point.
         *
         * For details about that function, take a look at
         *    P. N. swarztrauber, Computing the points and weights for
         *    Gauss-Legendre quadrature, SIAM J. Sci. Comput.,
         *    24(2002) pp. 945-954.
         *
         * This code is adapted from Ondomatic, which itself was taken from elsewhere.
         */
        template<GaussFormula QuadratureFormulaT>
        void ComputeGaussFormulas(std::size_t Npoints_gauss, std::vector<double>& points, std::vector<double>& weights);


        ///@} // \addtogroup


    } // namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HPP_
