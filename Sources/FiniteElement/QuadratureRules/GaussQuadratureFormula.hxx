/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 10:25:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HXX_

// IWYU pragma: private, include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace QuadratureNS
    {


        template<GaussFormula QuadratureFormulaT>
        void
        ComputeGaussFormulas(const std::size_t Ngauss_points, std::vector<double>& points, std::vector<double>& weights)
        {
            // For details about that function, take a look at
            //
            //     P. N. swarztrauber, Computing the points and weights for
            //     Gauss-Legendre quadrature, SIAM J. Sci. Comput.,
            //     24(2002) pp. 945-954.
            //

            assert(Ngauss_points > 0ul && "Added in MoReFEM; case 0 caused crashes...");

            const double eps = std::numeric_limits<double>::epsilon();

            const double pi = static_cast<double>(M_PI);
            const double half_pi = static_cast<double>(M_PI_2);

            // For Gauss-Lobatto, we compute internal nodes.
            assert(!(QuadratureFormulaT == GaussFormula::Gauss_Lobatto && Ngauss_points < 2u));

            const std::size_t Nquadrature_point =
                (QuadratureFormulaT == GaussFormula::Gauss_Lobatto ? Ngauss_points - 1u : Ngauss_points);

            // points(i) will be equal to cos(theta(i))
            std::vector<double> theta(Ngauss_points);
            points.resize(Ngauss_points);
            weights.resize(Ngauss_points);

            // coefficient of Fourier transform of Legendre polynom and derivative
            // we need the second derivative for Gauss-Lobatto points
            std::vector<double> coef(Nquadrature_point, 0.);
            std::vector<double> deriv_coef(Nquadrature_point, 0.);
            std::vector<double> second_deriv_coef(Nquadrature_point, 0.);

            // exact expressions are used for order 1, 2, 3
            switch (Nquadrature_point)
            {
            case 1:
                Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 1u>::Perform(points, weights);
                return;
            case 2:
                Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 2u>::Perform(points, weights);
                return;
            case 3:
                Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 3u>::Perform(points, weights);
                return;
            default:
                break;
            }

            // for order greater than 3, numerical computation
            const std::size_t Nparity_points = Nquadrature_point % 2u;
            const std::size_t Nhalf_points = Ngauss_points / 2u;
            const std::size_t Nhalf = (Nquadrature_point + 1u) / 2u;

            double zero = 0.0;
            double cz = zero;

            Internal::GaussQuadratureNS::ComputeFourierCoef(cz, coef, deriv_coef, second_deriv_coef);

            double dtheta = zero;
            if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto && (Nparity_points == 1))
                dtheta = half_pi / static_cast<double>(Nhalf - 1);
            else
                dtheta = half_pi / static_cast<double>(Nhalf);

            const double dthalf = 0.5 * dtheta;
            const double cmax = 0.2 * dtheta;

            // the zeros of Legendre polynom
            double zprev = zero, zlast = zero;
            double pb = zero, dpb = zero, ddpb = zero, dcor = zero;
            std::size_t nix;

            //
            //     estimate first point next to theta = pi/2
            //
            if (Nparity_points != 0)
            {
                // Nquadrature_point = 2 Nhalf-1
                // if odd the first zero is at the middle pi/2
                // and the following pi/2-pi/(2*Nhalf)
                if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto)
                {
                    zero = half_pi - dthalf;
                    zprev = half_pi + dthalf;
                    nix = Nhalf - 1;
                } else
                {
                    zero = half_pi - dtheta;
                    zprev = half_pi;
                    nix = Nhalf - 1; // index of the point
                }
            } else
            {
                // if even, no zero on the middle, the first zero is on pi/2-pi/(4*Nhalf)
                if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto)
                {
                    zero = half_pi - dtheta;
                    zprev = half_pi;
                    nix = Nhalf - 1; // index of the point
                } else
                {
                    zero = half_pi - dthalf;
                    zprev = half_pi + dthalf;
                    nix = Nhalf;
                }
            }

            bool each_point_not_computed = true;

            while (each_point_not_computed)
            {
                int Niteration = 0;
                bool test_convergence_newton = true;
                double residu(1.0);

                while (test_convergence_newton && Niteration < 100)
                {
                    ++Niteration;
                    zlast = zero;

                    //
                    //     newton iterations
                    //
                    Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                        Nquadrature_point, zero, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

                    if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto)
                        dcor = dpb / ddpb;
                    else
                        dcor = pb / dpb;

                    const double sgnd = NumericNS::IsZero(dcor) ? 1.0 : dcor / std::fabs(dcor);

                    // we don't move the point further than 0.2*delta_theta
                    double tmp = std::fabs(dcor);
                    dcor = sgnd * std::min(tmp, cmax);
                    zero = zero - dcor;
                    double residu_prec = residu;
                    residu = std::fabs(zero - zlast);
                    // we check if the stopping criteria are reached
                    if ((std::fabs(zero - zlast) <= eps * std::fabs(zero))
                        || ((Niteration > 5) && (residu_prec < residu)))
                        test_convergence_newton = false;
                }

                assert(nix - 1 < theta.size());
                theta[nix - 1] = zero;
                double zhold = zero;
                //      weights(nix) = (Nquadrature_point+Nquadrature_point+1)/(dpb*dpb)
                //
                //     yakimiw's formula permits using old pb and dpb
                //
                if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto)
                {
                    double tmp = NumericNS::Square(pb - dcor * dpb + 0.5 * dcor * dcor * ddpb);
                    assert(!NumericNS::IsZero(tmp));
                    assert(nix - 1 < weights.size());
                    weights[nix - 1] = 1.0 / tmp;
                } else
                {
                    assert(!NumericNS::IsZero(std::sin(zlast)));
                    double tmp = NumericNS::Square(dpb + pb * std::cos(zlast) / std::sin(zlast));
                    assert(!NumericNS::IsZero(tmp));
                    assert(nix - 1 < weights.size());
                    weights[nix - 1] = static_cast<double>(2u * Nquadrature_point + 1u) / tmp;
                }

                --nix;

                if (nix == 0)
                    each_point_not_computed = false;
                else if (nix <= (Nhalf - 1))
                    zero += zero - zprev;

                zprev = zhold;
            }

            //
            //     extend points and weights via symmetries
            //
            if ((QuadratureFormulaT == GaussFormula::Gauss) && (Nparity_points != 0))
            {
                assert(Nhalf - 1 < theta.size());
                theta[Nhalf - 1] = half_pi;
                Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                    Nquadrature_point, half_pi, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

                assert(!NumericNS::IsZero(dpb));
                assert(Nhalf - 1 < weights.size());
                weights[Nhalf - 1] = static_cast<double>(2u * Nquadrature_point + 1u) / NumericNS::Square(dpb);
            }

            if ((QuadratureFormulaT == GaussFormula::Gauss_Lobatto) && (Nparity_points == 0))
            {
                assert(Nhalf - 1 < theta.size());
                theta[Nhalf - 1] = half_pi;
                Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                    Nquadrature_point, half_pi, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);
                assert(!NumericNS::IsZero(pb));
                assert(Nhalf - 1 < weights.size());
                weights[Nhalf - 1] = 1. / NumericNS::Square(pb);
            }

            // DISP(Nhalf); DISP(theta);DISP(weights);
            if (QuadratureFormulaT == GaussFormula::Gauss_Lobatto)
            {
                assert(theta.size() >= Nhalf);
                assert(weights.size() >= Nhalf);

                for (auto i = static_cast<int>(Nhalf) - 1; i >= 0; --i)
                {
                    theta[static_cast<std::size_t>(i + 1)] = theta[static_cast<std::size_t>(i)];
                    weights[static_cast<std::size_t>(i + 1)] = weights[static_cast<std::size_t>(i)];
                }


                theta[0] = 0.0;
                Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                    Nquadrature_point, theta[0], cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

                // DISP(cz); DISP(pb);
                weights[0] = 1.0 / (NumericNS::Square(pb));
            }

            // DISP(theta);
            for (std::size_t i = 0; i < Nhalf_points; i++)
            {
                assert(Ngauss_points - i - 1 < weights.size());
                assert(Ngauss_points - i - 1 < theta.size());
                weights[Ngauss_points - i - 1] = weights[i];
                theta[Ngauss_points - i - 1] = pi - theta[i];
            }

            // DISP(theta); DISP(weights);
            double sum = 0.;

            assert(Ngauss_points <= weights.size());
            assert(Ngauss_points <= theta.size());
            for (std::size_t i = 0; i < Ngauss_points; i++)
                sum += weights[i];

            assert(!NumericNS::IsZero(sum));

            for (std::size_t i = 0; i < Ngauss_points; i++)
            {
                weights[i] = 2. * weights[i] / sum;
                points[i] = std::cos(theta[i]);
            }

            // reverse the arrays
            const auto Nweight = weights.size();

            for (auto i = 0ul; i < Nweight / 2; i++)
            {
                double tmp = weights[i];
                assert(weights.size() >= i + 1);
                std::size_t index = weights.size() - i - 1;

                weights[i] = weights[index];
                weights[index] = tmp;
            }

            const auto Npoints = points.size();

            for (auto i = 0ul; i < Npoints / 2; i++)
            {
                double tmp = points[i];
                assert(points.size() >= i + 1);
                std::size_t index = Nweight - i - 1;

                points[i] = points[index];
                points[index] = tmp;
            }
        }


    } // namespace QuadratureNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_GAUSS_QUADRATURE_FORMULA_HXX_
