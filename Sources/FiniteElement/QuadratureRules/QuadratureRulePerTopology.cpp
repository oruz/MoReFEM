/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Apr 2016 14:46:23 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <sstream>
#include <type_traits>
#include <utility>

#include "FiniteElement/QuadratureRules/Instantiation/Hexahedron.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Point.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Quadrangle.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Segment.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Tetrahedron.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Triangle.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        QuadratureRulePerTopology::storage_type DetermineDefaultQuadratureRule(std::size_t degree_of_exactness,
                                                                               std::size_t max_shape_function_order);


    } // namespace


    // clang-format off
    QuadratureRulePerTopology::QuadratureRulePerTopology(storage_type&& quadrature_rule_per_topology)
    : quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {
#ifndef NDEBUG
            decltype(auto) list = GetRulePerTopology();
            for (const auto& pair : list)
                assert(pair.second != nullptr);
#endif // NDEBUG
    }
    // clang-format on


    QuadratureRulePerTopology::QuadratureRulePerTopology(std::size_t degree_of_exactness,
                                                         std::size_t shape_function_order)
    : self(DetermineDefaultQuadratureRule(degree_of_exactness, shape_function_order))
    { }


    void QuadratureRulePerTopology::Print(std::ostream& out) const noexcept
    {
        const auto& quadrature_rule_per_topology = GetRulePerTopology();

        out << "List of registered quadrature rules:" << std::endl;

        for (const auto& topology_content : quadrature_rule_per_topology)
        {
            const auto& quadrature_rule_ptr = topology_content.second;
            assert(!(!quadrature_rule_ptr));

            out << "\t - " << quadrature_rule_ptr->GetName() << std::endl;
        }
    }


    const QuadratureRule& QuadratureRulePerTopology::GetRule(RefGeomEltNS::TopologyNS::Type topology) const
    {
        decltype(auto) rule_list = GetRulePerTopology();
        const auto it = rule_list.find(topology);

        if (it == rule_list.cend())
        {
            std::ostringstream oconv;

            oconv << "You require a quadrature rule for topology " << topology << "; no rule was given for it.";
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }

        assert(!(!it->second));
        return *(it->second);
    }


    namespace // anonymous
    {


        QuadratureRulePerTopology::storage_type DetermineDefaultQuadratureRule(std::size_t degree_of_exactness,
                                                                               std::size_t max_shape_function_order)
        {
            return { { { RefGeomEltNS::TopologyNS::Type::point,
                         QuadratureNS::Point::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { RefGeomEltNS::TopologyNS::Type::segment,
                         QuadratureNS::Segment::GetRuleFromShapeFunctionOrder(max_shape_function_order) },
                       { RefGeomEltNS::TopologyNS::Type::triangle,
                         QuadratureNS::Triangle::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { RefGeomEltNS::TopologyNS::Type::quadrangle,
                         QuadratureNS::Quadrangle::GetRuleFromShapeFunctionOrder(max_shape_function_order) },
                       { RefGeomEltNS::TopologyNS::Type::tetrahedron,
                         QuadratureNS::Tetrahedron::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { RefGeomEltNS::TopologyNS::Type::hexahedron,
                         QuadratureNS::Hexahedron::GetRuleFromShapeFunctionOrder(max_shape_function_order) } } };
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
