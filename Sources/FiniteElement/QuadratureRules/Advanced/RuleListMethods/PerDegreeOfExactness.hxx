/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 13:06:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HXX_

// IWYU pragma: private, include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Advanced
    {


        namespace QuadratureRuleNS
        {


            template<class DerivedT>
            std::size_t PerDegreeOfExactness<DerivedT>::Nrule()
            {
                return DerivedT::GetPerDegreeOfExactnessList().size();
            }


            template<class DerivedT>
            std::size_t PerDegreeOfExactness<DerivedT>::MaximumDegreeOfExactness()
            {
                // As the rules are assumed to be sort in increasing order, this is the degree of the last element.
                const auto& list = DerivedT::GetPerDegreeOfExactnessList();
                assert(!list.empty());

                assert(std::is_sorted(list.cbegin(),
                                      list.cend(),
                                      [](const QuadratureRule& rule1, const QuadratureRule& rule2)
                                      {
                                          return rule1.DegreeOfExactness() < rule2.DegreeOfExactness();
                                      }));

                return list.back().DegreeOfExactness();
            }


            template<class DerivedT>
            const QuadratureRule::const_shared_ptr&
            PerDegreeOfExactness<DerivedT>::GetRuleFromDegreeOfExactness(std::size_t degree)
            {
                const auto& list = DerivedT::GetPerDegreeOfExactnessList();
                assert(!list.empty());

                auto sorting_rule =
                    [](const QuadratureRule::const_shared_ptr& lhs, const QuadratureRule::const_shared_ptr& rhs)
                {
                    assert(!(!lhs));
                    assert(!(!rhs));
                    return lhs->DegreeOfExactness() < rhs->DegreeOfExactness();
                };

                // There is a strong assumption in the class that the rules are sort in increasing order; this is
                // checked here.
                assert(std::is_sorted(list.cbegin(), list.cend(), sorting_rule));

                auto dummy = std::make_shared<QuadratureRule>((*list.cbegin())->GetTopologyIdentifier(), degree);

                auto it = std::lower_bound(list.cbegin(), list.cend(), dummy, sorting_rule);

                // If the requested degree of exactness is too high, take the biggest available.
                if (it == list.cend())
                    return *(list.crbegin());

                // throw ExceptionNS::QuadratureRuleListNS::InvalidDegree(degree, MaximumDegreeOfExactness(), __FILE__,
                // __LINE__);

                return *it;
            }


        } // namespace QuadratureRuleNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HXX_
