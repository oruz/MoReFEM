/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 11:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HPP_


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace QuadratureRuleNS
        {


            /*!
             * \brief Intended to be used as a CRTP for quadrature rules for which selection is done by giving a degree
             * of exactness.
             *
             * The rule with the least points that fulfilled the required exactness is chosen.
             */
            template<class DerivedT>
            struct PerDegreeOfExactness
            {

                //! Number of rules.
                static std::size_t Nrule();

                //! Maximum degree of exactness.
                static std::size_t MaximumDegreeOfExactness();

                //! Return the cheapest quadrature rule ensuring that polynoms of degree \c degree are computed exactly.
                //! \param[in] degree Degree pf exactness used to choose the \a QuadratureRule.
                static const QuadratureRule::const_shared_ptr& GetRuleFromDegreeOfExactness(std::size_t degree);
            };


        } // namespace QuadratureRuleNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_DEGREE_OF_EXACTNESS_HPP_
