/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Apr 2016 16:11:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace QuadratureRuleNS
        {


            /*!
             * \brief Intended to be used as a CRTP for quadrature rules for which selection is done by giving the
             * order of  the shape function.
             *
             * This is intended to be used for Spectral finite elements.
             */
            template<class DerivedT>
            struct PerShapeFunctionOrder
            {

                //! Constructor.
                PerShapeFunctionOrder();

                //! Return the quadrature rule adapted for the \a order given, e.g. 3 for Q3.
                //! \param[in] order Order used as filter.
                static QuadratureRule::const_shared_ptr GetRuleFromShapeFunctionOrder(std::size_t order);


              private:
                /*!
                 * \brief List of quadrature rules stored so far.
                 *
                 * When a new rule is queried, there is first a check: if it already exists the existing law is
                 * returned, otherwise the new one is generated, stored and then returned.
                 *
                 * \return Key is the order considered, value the related quadrature rule.
                 */
                static std::unordered_map<std::size_t, QuadratureRule::const_shared_ptr>&
                GetNonCstQuadratureRulePerOrderList();
            };


        } // namespace QuadratureRuleNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods//PerShapeFunctionOrder.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HPP_
