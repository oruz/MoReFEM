/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp" // IWYU pragma: associated

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    DirichletBoundaryConditionManager::~DirichletBoundaryConditionManager() = default;


    const std::string& DirichletBoundaryConditionManager::ClassName()
    {
        static std::string ret("DirichletBoundaryConditionManager");
        return ret;
    }


    void DirichletBoundaryConditionManager ::RegisterDirichletBoundaryCondition(
        const DirichletBoundaryCondition::shared_ptr& boundary_condition_ptr)
    {
        assert(!(!boundary_condition_ptr));

        const auto& boundary_condition_list = GetList();

        const auto& name = boundary_condition_ptr->GetName();

        if (std::find_if(boundary_condition_list.cbegin(),
                         boundary_condition_list.cend(),
                         [&name](const auto& bc_in_list_ptr)
                         {
                             assert(!(!bc_in_list_ptr));
                             return bc_in_list_ptr->GetName() == name;
                         })
            != boundary_condition_list.cend())
            throw Exception("Two different boundary_conditions can't share the same name (namely " + name + ")",
                            __FILE__,
                            __LINE__);


        const auto& unique_id = boundary_condition_ptr->GetUniqueId();

        if (std::find_if(boundary_condition_list.cbegin(),
                         boundary_condition_list.cend(),
                         [unique_id](const auto& boundary_condition_in_list_ptr)
                         {
                             assert(!(!boundary_condition_in_list_ptr));
                             return boundary_condition_in_list_ptr->GetUniqueId() == unique_id;
                         })
            != boundary_condition_list.cend())
            throw Exception("Two different boundary_conditions can't share the same unique id (namely "
                                + std::to_string(unique_id) + ")",
                            __FILE__,
                            __LINE__);

        boundary_condition_list_.push_back(boundary_condition_ptr);
    }


    DirichletBoundaryCondition::shared_ptr
    DirichletBoundaryConditionManager ::GetDirichletBoundaryConditionPtr(std::size_t unique_id) const
    {
        const auto& boundary_condition_list = GetList();

        auto it = std::find_if(boundary_condition_list.cbegin(),
                               boundary_condition_list.cend(),
                               [unique_id](const DirichletBoundaryCondition::shared_ptr& boundary_condition_ptr)
                               {
                                   assert(!(!boundary_condition_ptr));
                                   return boundary_condition_ptr->GetUniqueId() == unique_id;
                               });

        assert(it != boundary_condition_list.cend());
        return *it;
    }


    DirichletBoundaryCondition::shared_ptr
    DirichletBoundaryConditionManager ::GetDirichletBoundaryConditionPtr(const std::string& name) const
    {
        const auto& boundary_condition_list = GetList();

        auto it = std::find_if(boundary_condition_list.cbegin(),
                               boundary_condition_list.cend(),
                               [name](const DirichletBoundaryCondition::shared_ptr& boundary_condition_ptr)
                               {
                                   assert(!(!boundary_condition_ptr));
                                   return boundary_condition_ptr->GetName() == name;
                               });

        assert(it != boundary_condition_list.cend());
        return *it;
    }


    void DirichletBoundaryConditionManager::Create(std::size_t unique_id,
                                                   const std::string& name,
                                                   const Domain& domain,
                                                   const Unknown& unknown,
                                                   const std::vector<double>& value_per_component,
                                                   const std::string& component,
                                                   const bool is_mutable)
    {
        auto&& boundary_condition_ptr = new DirichletBoundaryCondition(
            unique_id, name, domain, unknown, value_per_component, component, is_mutable);

        RegisterDirichletBoundaryCondition(std::move(DirichletBoundaryCondition::shared_ptr(boundary_condition_ptr)));
    }


    void ClearAllBoundaryConditionInitialValueList()
    {
        const auto& manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

        const auto& list = manager.GetList();

        for (const auto& boundary_condition_ptr : list)
        {
            assert(!(!boundary_condition_ptr));
            boundary_condition_ptr->ClearInitialDofValueList();
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
