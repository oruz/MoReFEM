/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 23 Jul 2015 14:21:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            inline const std::vector<PetscInt>& DofStorage::GetProgramWiseDofIndexList() const noexcept
            {
                return program_wise_dof_index_list_;
            }


            inline const std::vector<std::size_t>& DofStorage::GetProcessorWiseDofIndexList() const noexcept
            {
                return processor_wise_dof_index_list_;
            }


            inline const std::vector<PetscScalar>& DofStorage::GetDofValueList() const noexcept
            {
                assert(GetProgramWiseDofIndexList().size() == dof_value_list_.size());
                return dof_value_list_;
            }


            inline const Dof::vector_shared_ptr& DofStorage::GetDofList() const noexcept
            {
                assert(GetProgramWiseDofIndexList().size() == dof_list_.size());
                return dof_list_;
            }


            inline std::vector<PetscScalar>& DofStorage::GetNonCstDofValueList() noexcept
            {
                return const_cast<std::vector<PetscScalar>&>(GetDofValueList());
            }


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_DOF_STORAGE_HXX_
