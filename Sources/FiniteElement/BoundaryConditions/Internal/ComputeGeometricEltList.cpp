//! \file
//
//
//  ComputeGeometricEltList.cpp
//  MoReFEM
//
//  Created by sebastien on 10/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#include <cassert>
#include <memory>
#include <tuple>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    GeometricElt::vector_shared_ptr ComputeGeometricEltList(const Mesh& mesh,
                                                            const DirichletBoundaryCondition& boundary_condition)
    {
        const auto& boundary_condition_domain = boundary_condition.GetDomain();

        using iterator_geometric_element = Mesh::iterator_geometric_element;
        iterator_geometric_element begin, end;

        GeometricElt::vector_shared_ptr ret;

        // We know that mathematically dimension lesser than dimension - 1 may not be properly defined, but it's
        // still useful in some contexts, providing model writer knows what he's doing. See #1262 discussion
        // for more details.
        for (auto dimension = mesh.GetDimension() - 1u; dimension < 3u; --dimension)
        {
            // As this function is called before reduction; processor-wise encompass in fact all prgram-wise data...
            const auto& bag_of_boundary_domain = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>(dimension);

            for (const auto& geometric_elt_type_ptr : bag_of_boundary_domain)
            {
                const auto& geom_elt_type = *geometric_elt_type_ptr;

                if (!boundary_condition_domain.DoRefGeomEltMatchCriteria(geom_elt_type))
                    continue;

                std::tie(begin, end) = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(geom_elt_type);

                for (auto it = begin; it != end; ++it)
                {
                    const auto& geometric_elt_ptr = *it;
                    assert(!(!geometric_elt_ptr));
                    const auto& geom_elt = *geometric_elt_ptr;
                    assert(geom_elt.GetIdentifier() == geom_elt_type.GetIdentifier()
                           && "If not, GetSubsetGeometricEltList() above is buggy!");

                    if (!boundary_condition_domain.IsGeometricEltInside(geom_elt))
                        continue;

                    ret.push_back(geometric_elt_ptr);
                }
            }
        }

        return ret;
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS
