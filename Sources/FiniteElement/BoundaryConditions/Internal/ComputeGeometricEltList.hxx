//! \file
//
//
//  ComputeGeometricEltList.hxx
//  MoReFEM
//
//  Created by sebastien on 10/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HXX_

// IWYU pragma: private, include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


} // namespace MoReFEM::Internal::BoundaryConditionNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HXX_
