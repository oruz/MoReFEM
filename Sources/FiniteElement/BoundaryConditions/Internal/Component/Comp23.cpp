/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:36:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

// IWYU pragma: no_include <iosfwd>
#include "FiniteElement/BoundaryConditions/Internal/Component/FwdForCpp.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/Comp23.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            namespace // anonymous
            {


                ComponentManager::const_shared_ptr Create()
                {
                    return std::make_unique<Comp23>();
                }


                // Register the geometric element in the 'ComponentFactory' singleton.
                // The return value is mandatory: we can't simply call a void function outside function boundaries
                // See "Modern C++ Design", Chapter 8, P205.
                __attribute__((unused)) const bool registered =
                    Internal::BoundaryConditionNS::ComponentFactory::CreateOrGetInstance(__FILE__, __LINE__)
                        .Register<Comp23>(Create);


            } // anonymous namespace


            Comp23::Comp23() = default;


            const std::string& Comp23::Name()
            {
                static std::string ret("Comp23");
                return ret;
            }


            const std::bitset<3>& Comp23::IsActivated()
            {
                static std::bitset<3> ret("110");
                return ret;
            }


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
