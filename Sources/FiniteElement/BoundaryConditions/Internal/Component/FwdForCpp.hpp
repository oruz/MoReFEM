//! \file
//
//
//  FwdForCpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 25/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_FWD_FOR_CPP_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_FWD_FOR_CPP_HPP_

#include <bitset>     // IWYU pragma: export
#include <functional> // IWYU pragma: export
#include <memory>     // IWYU pragma: export
#include <string>     // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"  // IWYU pragma: export
#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_FWD_FOR_CPP_HPP_
