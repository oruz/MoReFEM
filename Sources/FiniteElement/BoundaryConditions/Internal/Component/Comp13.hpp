/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMP13_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMP13_HPP_

#include <bitset>
#include <iosfwd>

#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            /*!
             * \brief The class in charge fo managing Comp13 behaviour.
             *
             * \attention Make sure not to define any data attribute in the class!
             * Mother class doesn't get a virtual destructor, and is used polymorphically, so we must make sure
             * not to add any data attribute that would induce memory leaks (it shouldn't be required anyway:
             * the derived classes are designed to be very lightweight and add no additional functionalities...)
             *
             */
            struct Comp13 final : public TComponentManager<Comp13>
            {

                /*!
                 * \brief Constructor.
                 */
                explicit Comp13();


                /*!
                 * \copydoc doxygen_hide_component_static_name_method
                 */
                static const std::string& Name();


                /*!
                 * \copydoc doxygen_hide_component_static_is_activated_method
                 */
                static const std::bitset<3>& IsActivated();
            };


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMP13_HPP_
