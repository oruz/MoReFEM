/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:36:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

// IWYU pragma: no_include <iosfwd>
#include "FiniteElement/BoundaryConditions/Internal/Component/FwdForCpp.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/CompNA.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            namespace // anonymous
            {


                ComponentManager::const_shared_ptr Create()
                {
                    return std::make_unique<CompNA>();
                }


                // Register the geometric element in the 'ComponentFactory' singleton.
                // The return value is mandatory: we can't simply call a void function outside function boundaries
                // See "Modern C++ Design", Chapter 8, P205.
                __attribute__((unused)) const bool registered =
                    Internal::BoundaryConditionNS::ComponentFactory::CreateOrGetInstance(__FILE__, __LINE__)
                        .Register<CompNA>(Create);


            } // anonymous namespace


            CompNA::CompNA() = default;


            const std::string& CompNA::Name()
            {
                static std::string ret("CompNA");
                return ret;
            }


            const std::bitset<3>& CompNA::IsActivated()
            {
                static std::bitset<3> ret("000");
                return ret;
            }


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
