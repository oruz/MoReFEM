### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGeometricEltList.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGeometricEltList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGeometricEltList.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Component/SourceList.cmake)
