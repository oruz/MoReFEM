/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HXX_

// IWYU pragma: private, include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <type_traits>
#include <utility>
#include <vector>

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::Internal::BoundaryConditionNS { class DofStorage; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const Unknown& DirichletBoundaryCondition::GetUnknown() const noexcept
    {
        return unknown_;
    }


    inline const Internal::BoundaryConditionNS::ComponentManager&
    DirichletBoundaryCondition ::GetComponentManager() const noexcept
    {
        assert(!(!component_manager_));
        return *component_manager_;
    }


    inline const std::vector<std::pair<Dof::shared_ptr, double>>&
    DirichletBoundaryCondition ::GetInitialDofValueList() const noexcept
    {
        assert(!is_cleared_
               && "This should be used only once; it is cleared past this usage. "
                  "If you get this assert, you probably tried to get access past its intended lifetime.");

        return initial_dof_value_list_;
    }


    inline std::vector<std::pair<Dof::shared_ptr, double>>&
    DirichletBoundaryCondition ::GetNonCstInitialDofValueList() noexcept
    {
        return const_cast<std::vector<std::pair<Dof::shared_ptr, double>>&>(GetInitialDofValueList());
    }


    inline const Domain& DirichletBoundaryCondition::GetDomain() const noexcept
    {
        return domain_;
    }


    inline const Dof::vector_shared_ptr& DirichletBoundaryCondition::GetDofList() const noexcept
    {
        assert(is_dof_list_computed_);

        // No check for emptiness: dof list might easily be empty for a Dirichlet condition in parallel on a given
        // processor (it's very likely all dofs of the domain are stored on a unique processor).
        return dof_list_;
    }


    inline Dof::vector_shared_ptr& DirichletBoundaryCondition::GetNonCstDofList() noexcept
    {
        return const_cast<Dof::vector_shared_ptr&>(GetDofList());
    }


    inline const std::vector<std::pair<std::size_t, Internal::BoundaryConditionNS::DofStorage>>&
    DirichletBoundaryCondition ::GetDofStorage() const noexcept
    {
        return dof_storage_per_numbering_subset_;
    }


    inline std::vector<std::pair<std::size_t, Internal::BoundaryConditionNS::DofStorage>>&
    DirichletBoundaryCondition ::GetNonCstDofStorage() noexcept
    {
        return const_cast<std::vector<std::pair<std::size_t, Internal::BoundaryConditionNS::DofStorage>>&>(
            GetDofStorage());
    }


    inline Internal::BoundaryConditionNS::DofStorage&
    DirichletBoundaryCondition ::GetNonCstDofStorage(const NumberingSubset& numbering_subset) noexcept
    {
        return const_cast<Internal::BoundaryConditionNS::DofStorage&>(GetDofStorage(numbering_subset));
    }


    inline const std::string& DirichletBoundaryCondition::GetName() const noexcept
    {
        return name_;
    }


    inline const NodeBearer::vector_shared_ptr& DirichletBoundaryCondition ::GetNodeBearerList() const noexcept
    {
        return node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& DirichletBoundaryCondition ::GetNonCstNodeBearerList() noexcept
    {
        return const_cast<NodeBearer::vector_shared_ptr&>(GetNodeBearerList());
    }


    inline const NodeBearer::vector_shared_ptr&
    DirichletBoundaryCondition ::SetNodeBearerList(NodeBearer::vector_shared_ptr&& node_bearer_list)
    {
        assert(node_bearer_list_.empty());
        node_bearer_list_ = std::move(node_bearer_list);
        return node_bearer_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
