/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Sep 2015 14:23:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <vector>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/InputData.hpp" // IWYU pragma: keep
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"

#include "Geometry/Domain/DomainManager.hpp" // IWYU pragma: keep

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp" // IWYU pragma: export
#include "FiniteElement/Unknown/UnknownManager.hpp"                        // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Singleton class which is aware of all essential boundary conditions that might be considered within
     * a model.
     *
     * These conditions are defined in the input data file.
     */
    class DirichletBoundaryConditionManager final : public Utilities::Singleton<DirichletBoundaryConditionManager>
    {

      public:
        //! Return the name of the class.
        static const std::string& ClassName();

        //! Base type of DirichletBoundaryCondition as input parameter (requested to identify domains in the input
        //! parameter data).
        using input_data_type = InputDataNS::BaseNS::DirichletBoundaryCondition;


      private:
        /// \name Special members
        ///@{

        //! Destructor.
        virtual ~DirichletBoundaryConditionManager() override;

        //! \copydoc doxygen_hide_copy_constructor
        DirichletBoundaryConditionManager(const DirichletBoundaryConditionManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DirichletBoundaryConditionManager(DirichletBoundaryConditionManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DirichletBoundaryConditionManager& operator=(const DirichletBoundaryConditionManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DirichletBoundaryConditionManager& operator=(DirichletBoundaryConditionManager&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Create a new DirichletBoundaryCondition object from the data of the input data file.
         *
         * \param[in] section Section dedicated to a BoundaryCondition in the
         * input data file.
         *
         */
        template<class DirichletBoundaryConditionSectionT>
        void Create(const DirichletBoundaryConditionSectionT& section);

        //! Returns the number of boundary conditions.
        std::size_t NboundaryCondition() const noexcept;

        //! Get the boundary condition associated with \a unique_id.
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        const DirichletBoundaryCondition& GetDirichletBoundaryCondition(std::size_t unique_id) const noexcept;

        //! Get the boundary condition associated with \a unique_id.
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        DirichletBoundaryCondition& GetNonCstDirichletBoundaryCondition(std::size_t unique_id) noexcept;

        //! Get the boundary condition associated with \a unique_id as a smart pointer.
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        DirichletBoundaryCondition::shared_ptr GetDirichletBoundaryConditionPtr(std::size_t unique_id) const;

        //! Get the boundary condition associated with \a name.
        //! \name_param_in_accessor{DirichletBoundaryCondition}
        const DirichletBoundaryCondition& GetDirichletBoundaryCondition(const std::string& name) const noexcept;

        //! Get the boundary condition associated with \a name.
        //! \name_param_in_accessor{DirichletBoundaryCondition}
        DirichletBoundaryCondition& GetNonCstDirichletBoundaryCondition(const std::string& name) noexcept;

        //! Get the boundary condition associated with \a name as a smart pointer.
        //! \name_param_in_accessor{DirichletBoundaryCondition}
        DirichletBoundaryCondition::shared_ptr GetDirichletBoundaryConditionPtr(const std::string& name) const;

        //! Access to the list of boundary conditions.
        const DirichletBoundaryCondition::vector_shared_ptr& GetList() const noexcept;


      private:
        /*!
         *
         * \copydetails doxygen_hide_boundary_condition_constructor_args
         */
        void Create(std::size_t unique_id,
                    const std::string& name,
                    const Domain& domain,
                    const Unknown& unknown,
                    const std::vector<double>& value_per_component,
                    const std::string& component,
                    const bool is_mutable);

      private:
        /// \name Singleton requirements.
        ///@{

        //! Constructor.
        DirichletBoundaryConditionManager() = default;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<DirichletBoundaryConditionManager>;
        ///@}


      private:
        //! Register properly boundary condition in use.
        //! \param[in] ptr Shared pointernton the new boundary condition to register.
        void RegisterDirichletBoundaryCondition(const DirichletBoundaryCondition::shared_ptr& ptr);


      private:
        //! List of boundary conditions.
        DirichletBoundaryCondition::vector_shared_ptr boundary_condition_list_;
    };


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HPP_
