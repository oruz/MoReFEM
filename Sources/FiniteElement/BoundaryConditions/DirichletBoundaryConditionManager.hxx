/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{


    template<class DirichletBoundaryConditionSectionT>
    void DirichletBoundaryConditionManager::Create(const DirichletBoundaryConditionSectionT& section)
    {
        namespace ipl = Internal::InputDataNS;

        decltype(auto) name = ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::Name>(section);
        decltype(auto) domain_id = ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::DomainIndex>(section);
        decltype(auto) unknown_name =
            ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::UnknownName>(section);
        decltype(auto) component = ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::Component>(section);
        decltype(auto) value_per_component =
            ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::Values>(section);
        decltype(auto) is_mutable = ipl::ExtractLeaf<typename DirichletBoundaryConditionSectionT::IsMutable>(section);

        const auto& domain = DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(domain_id, __FILE__, __LINE__);

        const auto& unknown = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknown(unknown_name);

        Create(section.GetUniqueId(), name, domain, unknown, value_per_component, component, is_mutable);
    }


    inline std::size_t DirichletBoundaryConditionManager::NboundaryCondition() const noexcept
    {
        return boundary_condition_list_.size();
    }


    inline const DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetDirichletBoundaryCondition(std::size_t i) const noexcept
    {
        return *GetDirichletBoundaryConditionPtr(i);
    }


    inline const DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetDirichletBoundaryCondition(const std::string& name) const noexcept
    {
        return *GetDirichletBoundaryConditionPtr(name);
    }


    inline DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetNonCstDirichletBoundaryCondition(std::size_t i) noexcept
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(i));
    }


    inline DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetNonCstDirichletBoundaryCondition(const std::string& name) noexcept
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(name));
    }


    inline const DirichletBoundaryCondition::vector_shared_ptr&
    DirichletBoundaryConditionManager ::GetList() const noexcept
    {
        return boundary_condition_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
