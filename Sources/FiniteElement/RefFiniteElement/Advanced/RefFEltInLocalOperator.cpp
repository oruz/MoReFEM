/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 14:47:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <numeric>

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        RefFEltInLocalOperator::RefFEltInLocalOperator(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                                       const std::size_t index_first_node_in_elementary_data,
                                                       const std::size_t index_first_dof_in_elementary_data)
        : ref_felt_(ref_felt), index_first_dof_(index_first_dof_in_elementary_data),
          Ndof_per_component_(ref_felt.Ndof() / ref_felt.Ncomponent().Get())
        {
            const auto Ndof = ref_felt.Ndof();
            const auto Ncomponent = ref_felt.Ncomponent();

            assert(Ndof % Ncomponent.Get() == 0ul);
            const auto Ndof_per_component = NdofPerComponent();

            const auto Nnode = ref_felt.Nnode();

            local_node_indexes_.resize(Nnode);
            std::iota(local_node_indexes_.begin(), local_node_indexes_.end(), index_first_node_in_elementary_data);

            local_dof_indexes_.resize(Ndof);
            std::iota(local_dof_indexes_.begin(), local_dof_indexes_.end(), index_first_dof_in_elementary_data);

            std::vector<std::size_t> buf(Ndof_per_component);

            for (Advanced::ComponentNS::index_type i{ 0ul }; i < Ncomponent; ++i)
            {
                std::iota(buf.begin(), buf.end(), index_first_dof_in_elementary_data + i.Get() * Ndof_per_component);
                local_dof_indexes_per_component_.push_back(buf);
            }

            const auto& ref_felt_space_dim = ref_felt.GetFEltSpaceDimension();

            InitLocalMatrixStorage({ { { Nnode, ref_felt_space_dim } } });

            InitLocalVectorStorage({ { Nnode } });
        }


        // \todo #1491 Probably might be done with native Xtensor functions
        const LocalMatrix& ExtractSubMatrix(const LocalMatrix& full_matrix, const RefFEltInLocalOperator& ref_felt)
        {
            auto& ret = ref_felt.GetLocalMatrix<0>(); // mutable...

            assert(full_matrix.shape(0) >= ret.shape(0));
            assert(full_matrix.shape(1) >= ret.shape(1));

            const auto& row_range = ref_felt.GetLocalNodeIndexList();

            const auto ref_felt_space_dim = ref_felt.GetFEltSpaceDimension();

#ifndef NDEBUG
// \todo #1292 Wrappers::Xtensor::AssertBlockRowValidity(full_matrix, row_range);
#endif // NDEBUG

            assert(ref_felt.Nnode() == row_range.size());
            assert(ref_felt_space_dim == ret.shape(1));

            const auto Nrow = row_range.size();

            for (auto i = 0ul; i < Nrow; ++i)
            {
                auto row_index = row_range[i];

                for (auto component = 0ul; component < ref_felt_space_dim; ++component)
                    ret(i, component) = full_matrix(row_index, component);
            }

            return ret;
        }


        const LocalVector& ExtractSubVector(const LocalVector& full_vector, const RefFEltInLocalOperator& ref_felt)
        {
            auto& ret = ref_felt.GetLocalVector<0>(); // mutable...

            assert(full_vector.shape(0) >= ret.shape(0));

            const auto& row_range = ref_felt.GetLocalNodeIndexList();

#ifndef NDEBUG
// \todo #1292 Wrappers::Xtensor::AssertBlockValidity(full_vector, row_range);
#endif // NDEBUG

            const auto Nrow = row_range.size();

            for (auto i = 0ul; i < Nrow; ++i)
            {
                auto row_index = row_range[i];
                ret(i) = full_vector(row_index);
            }

            return ret;
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
