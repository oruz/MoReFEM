/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 14:47:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Advanced
    {


        inline std::size_t RefFEltInLocalOperator::Nnode() const noexcept
        {
            return GetUnderlyingRefFElt().Nnode();
        }


        inline std::size_t RefFEltInLocalOperator::Ndof() const noexcept
        {
            return GetUnderlyingRefFElt().Ndof();
        }


        inline const Internal::RefFEltNS::BasicRefFElt& RefFEltInLocalOperator::GetBasicRefFElt() const noexcept
        {
            return GetUnderlyingRefFElt().GetBasicRefFElt();
        }


        inline const std::vector<std::size_t>& RefFEltInLocalOperator::GetLocalNodeIndexList() const noexcept
        {
            return local_node_indexes_;
        }


        inline const std::vector<std::size_t>& RefFEltInLocalOperator::GetLocalDofIndexList() const noexcept
        {
            return local_dof_indexes_;
        }


        inline const std::vector<std::size_t>&
        RefFEltInLocalOperator::GetLocalDofIndexList(ComponentNS::index_type component_index) const noexcept
        {
            assert(component_index.Get() < local_dof_indexes_per_component_.size());
            return local_dof_indexes_per_component_[component_index.Get()];
        }


        inline const Internal::RefFEltNS::RefFEltInFEltSpace&
        RefFEltInLocalOperator::GetUnderlyingRefFElt() const noexcept
        {
            return ref_felt_;
        }


        inline const ExtendedUnknown& RefFEltInLocalOperator::GetExtendedUnknown() const noexcept
        {
            return GetUnderlyingRefFElt().GetExtendedUnknown();
        }


        inline ComponentNS::index_type RefFEltInLocalOperator::Ncomponent() const noexcept
        {
            return GetUnderlyingRefFElt().Ncomponent();
        }


        inline std::size_t RefFEltInLocalOperator::GetIndexFirstDofInElementaryData() const noexcept
        {
            assert(index_first_dof_ == GetLocalDofIndexList()[0]);
            return index_first_dof_;
        }


        inline std::size_t
        RefFEltInLocalOperator::GetIndexFirstDofInElementaryData(ComponentNS::index_type component) const noexcept
        {
            assert(component < GetUnderlyingRefFElt().Ncomponent());
            return GetIndexFirstDofInElementaryData() + NdofPerComponent() * component.Get();
        }


        inline std::size_t RefFEltInLocalOperator::NdofPerComponent() const noexcept
        {
            return Ndof_per_component_;
        }


        inline std::size_t RefFEltInLocalOperator::GetMeshDimension() const noexcept
        {
            return GetUnderlyingRefFElt().GetMeshDimension();
        }


        inline std::size_t RefFEltInLocalOperator::GetFEltSpaceDimension() const noexcept
        {
            return GetUnderlyingRefFElt().GetFEltSpaceDimension();
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_
