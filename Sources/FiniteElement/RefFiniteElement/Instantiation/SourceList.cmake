### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ0.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ1.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ2c.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Point.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ0.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ1.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ2c.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP0.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP1.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP2.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Spectral.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP0.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1Bubble.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP2.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP0.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1Bubble.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP2.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FwdForCpp.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ0.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ1.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/HexahedronQ2c.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Point.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ0.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ1.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadrangleQ2c.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP0.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP1.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SegmentP2.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Spectral.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Spectral.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP0.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1Bubble.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP2.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP0.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1Bubble.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP2.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
