/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q2C_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q2C_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron20.hpp"
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for HexahedronQ2c.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Hexahedron class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in RefGeomEltNS::TopologyNS::Hexahedron
         * traits class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class HexahedronQ2c
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Hexahedron,
            RefGeomEltNS::ShapeFunctionNS::Hexahedron20,
            InterfaceNS::Nature::edge,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit HexahedronQ2c() = default;

            //! Destructor.
            virtual ~HexahedronQ2c() override;

            //! \copydoc doxygen_hide_copy_constructor
            HexahedronQ2c(const HexahedronQ2c& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            HexahedronQ2c(HexahedronQ2c&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            HexahedronQ2c& operator=(const HexahedronQ2c& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            HexahedronQ2c& operator=(HexahedronQ2c&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q2C_HPP_
