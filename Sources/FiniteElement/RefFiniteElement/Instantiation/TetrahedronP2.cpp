/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "FiniteElement/RefFiniteElement/Instantiation/TetrahedronP2.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        namespace // anonymous
        {


            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<TetrahedronP2>();


        } // namespace


        const std::string& TetrahedronP2::ShapeFunctionLabel()
        {
            static std::string ret("P2");
            return ret;
        }


        TetrahedronP2::~TetrahedronP2() = default;


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
