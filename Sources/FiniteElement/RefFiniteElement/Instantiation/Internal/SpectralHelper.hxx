/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 12:13:46 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <sstream>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyCommon.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::RefGeomEltNS::TopologyNS { class Segment; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            template<std::size_t NI, std::size_t NJ, std::size_t NK>
            std::array<LocalNodeNS::index_type, 3u> ComputeIntegerCoordinates(LocalNodeNS::index_type local_node_index)
            {
                assert(local_node_index.Get() < (NI + 1u) * (NJ + 1u) * (NK + 1u));
                const auto m = local_node_index % LocalNodeNS::index_type{ (NI + 1u) * (NJ + 1u) };

                // clang-format off
                std::array<LocalNodeNS::index_type, 3u> ret
                {
                    {
                        m % LocalNodeNS::index_type { NI + 1u },
                        m / LocalNodeNS::index_type { NJ + 1u },
                        local_node_index / LocalNodeNS::index_type { (NI + 1u) * (NJ + 1u) }
                    }
                };
                // clang-format on

                return ret;
            }


            template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
            std::string GenerateShapeFunctionLabel()
            {
                std::ostringstream oconv;
                const char letter = std::is_same<TopologyT, ::MoReFEM::RefGeomEltNS::TopologyNS::Segment>() ? 'P' : 'Q';

                constexpr std::size_t dimension = TopologyT::dimension;

                oconv << letter << NI;

                switch (dimension)
                {
                case 1u:
                    // Nothing to add.
                    break;
                case 2u:
                    // Check for anisotropy.
                    if (NI != NJ)
                        oconv << '-' << letter << NJ;
                    break;
                case 3u:
                    // Check for anisotropy.
                    if (NI != NJ || NI != NK)
                        oconv << '-' << letter << NJ << '-' << letter << NK;
                    break;
                default:
                    assert(false && "Should not happen!");
                    exit(EXIT_FAILURE);
                }

                const std::string ret(oconv.str());
                return ret;
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SPECTRAL_HELPER_HXX_
