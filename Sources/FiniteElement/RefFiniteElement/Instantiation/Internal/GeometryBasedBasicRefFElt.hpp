/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 14:25:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            /*!
             * \brief Base class used to define P1, P2, Q2c reference finite elements (except for segments).
             *
             * . Q1, Q2 are handled by Spectral class.
             * . Segment P1/P2 is also handled by Spectral class.
             *
             * \internal <b><tt>[internal]</tt></b> In fact, Q1 and Q2 could be also handled by this class; I have
             * arbitrarily chosen Spectral for it (the reason for this choice is mostly test purposes: Spectral is hence
             * tested in low-orders as well, so any bug not related to orientation might be seen there. Also, it hence
             * keeps a symmetry with definitions used for higher orders.). \endinternal
             *
             * \tparam TopologyT Topology considered (one of the class defined within RefGeomEltNS::TopologyNS
             * namespace). \tparam ShapeFunctionT Shape function traits class considered (one of the class defined
             * within RefGeomEltNS::ShapeFunctionNS namespace). \tparam HigherInterfaceConnectedT The 'maximum' kind of
             * interface that is numbered. For instance if 'face' is chosen, vertices, edges and faces will be numbered
             * but not volume. This parameter is really important: it is there that the difference between a TriangleP1
             * or a TriangleP2 is drawn. \tparam NdiscontinuousLocalNodeT Number of local nodes to place on the interior
             * of the element (for instance on the face for a triangle, which is not connected to any of its triangle
             * neighbours).
             *
             * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
             * the one on edges (if relevant - see \a HigherInterfaceConnectedT), then on faces (same remark) and
             * finally on volume. Local nodes on vertices are numbered exactly as they were on the \a TopologyT traits
             * class. If relevant, nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in \a TopologyT
             * traits class is now 'Nvertex + i'. And keep going with face and volume if relevant.
             *
             * \attention This class is by design restricted to the cases in which there is at most one node per node
             * bearer. The reason for this is that we limit ourselves to geometric  order equal or lower than 2 (i.e. P2
             * or Q2) for which this is true.
             */
            template<class TopologyT,
                     class ShapeFunctionT,
                     InterfaceNS::Nature HigherInterfaceConnectedT,
                     std::size_t NdiscontinuousLocalNodeT>
            class GeometryBasedBasicRefFElt : public BasicRefFElt
            {

              public:
                //! Alias over Topology Traits class.
                using topology = TopologyT;

                //! Alias to parent.
                using parent = BasicRefFElt;

              public:
                /*!
                 * \brief Constructor.
                 */
                explicit GeometryBasedBasicRefFElt();

                //! Destructor.
                virtual ~GeometryBasedBasicRefFElt() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                GeometryBasedBasicRefFElt(const GeometryBasedBasicRefFElt& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                GeometryBasedBasicRefFElt(GeometryBasedBasicRefFElt&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                GeometryBasedBasicRefFElt& operator=(const GeometryBasedBasicRefFElt& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                GeometryBasedBasicRefFElt& operator=(GeometryBasedBasicRefFElt&& rhs) = default;


                /*!
                 * \brief Compute the local nodes.
                 *
                 * This method should not be called outside of Init() method.
                 *
                 * \return List of local nodes.
                 */
                virtual Advanced::LocalNode::vector_const_shared_ptr ComputeLocalNodeList() override final;

                /*!
                 *
                 * \copydoc doxygen_hide_shape_function
                 */
                virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                             const LocalCoords& local_coords) const override final;

                /*!
                 *
                 * \copydoc doxygen_hide_first_derivate_shape_function
                 */
                virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                          Advanced::ComponentNS::index_type component,
                                                          const LocalCoords& local_coords) const override final;

                //! \copydoc doxygen_hide_shape_function_order_method
                virtual std::size_t GetOrder() const noexcept override final;
            };


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_GEOMETRY_BASED_BASIC_REF_F_ELT_HPP_
