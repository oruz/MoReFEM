/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Dec 2015 14:30:24 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TRIANGLE_P1_BUBBLE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TRIANGLE_P1_BUBBLE_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ShapeFunctionNS
        {


        } // namespace ShapeFunctionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TRIANGLE_P1_BUBBLE_HXX_
