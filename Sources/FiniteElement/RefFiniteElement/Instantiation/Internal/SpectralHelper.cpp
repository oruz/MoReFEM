/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 12:13:46 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cassert>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            double Interpolation(const std::vector<double>& pos, double p, LocalNodeNS::index_type phi)
            {
                const LocalNodeNS::index_type n{ static_cast<LocalNodeNS::index_type::underlying_type>(pos.size()) };
                double s = 1.;

                assert(phi.Get() < pos.size());
                const double pos_phi = pos[phi.Get()];

                for (LocalNodeNS::index_type r{ 0ul }; r < n; ++r)
                {
                    if (r != phi)
                    {
                        const auto r_value = r.Get();
                        s *= (p - pos[r_value]) / (pos_phi - pos[r_value]);
                    }
                }

                return s;
            }


            double DerivativeInterpolation(const std::vector<double>& pos, double p, LocalNodeNS::index_type phi)
            {
                double c;
                double s = 0.;
                double d = 1.;
                const LocalNodeNS::index_type n{ static_cast<LocalNodeNS::index_type::underlying_type>(pos.size()) };

                for (LocalNodeNS::index_type r{ 0ul }; r < n; ++r)
                {
                    if (r != phi)
                        d *= (pos[phi.Get()] - pos[r.Get()]);
                }

                for (LocalNodeNS::index_type r1{ 0ul }; r1 < n; ++r1)
                {
                    if (r1 != phi)
                    {
                        c = 1.;

                        for (LocalNodeNS::index_type r2{ 0ul }; r2 < n; ++r2)
                        {
                            if (r2 != r1 && r2 != phi)
                                c *= (p - pos[r2.Get()]);
                        }

                        s += c;
                    }
                }
                return s / d;
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
