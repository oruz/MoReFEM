/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle6.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TriangleP1.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Triangle class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in RefGeomEltNS::TopologyNS::Triangle
         * traits class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class TriangleP2 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Triangle,
            RefGeomEltNS::ShapeFunctionNS::Triangle6,
            InterfaceNS::Nature::edge,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TriangleP2() = default;

            //! Destructor.
            virtual ~TriangleP2() override;

            //! \copydoc doxygen_hide_copy_constructor
            TriangleP2(const TriangleP2& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TriangleP2(TriangleP2&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TriangleP2& operator=(const TriangleP2& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TriangleP2& operator=(TriangleP2&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_
