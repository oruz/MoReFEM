/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P2_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P2_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron10.hpp"
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TetrahedronP2.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Tetrahedron class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in
         * RefGeomEltNS::TopologyNS::Tetrahedron traits class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TetrahedronP2
        // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Tetrahedron,
            RefGeomEltNS::ShapeFunctionNS::Tetrahedron10,
            InterfaceNS::Nature::edge,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TetrahedronP2() = default;

            //! Destructor.
            ~TetrahedronP2() override;

            //! \copydoc doxygen_hide_copy_constructor
            TetrahedronP2(const TetrahedronP2& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TetrahedronP2(TetrahedronP2&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TetrahedronP2& operator=(const TetrahedronP2& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TetrahedronP2& operator=(TetrahedronP2&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P2_HPP_
