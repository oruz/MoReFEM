/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Sep 2014 14:25:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Geometry/RefGeometricElt/Instances/TopologyCommon.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"


#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        const std::string& Spectral<TopologyT, NI, NJ, NK>::ShapeFunctionLabel()
        {
            static std::string ret = Internal::RefFEltNS::GenerateShapeFunctionLabel<TopologyT, NI, NJ, NK>();
            return ret;
        }


        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        Spectral<TopologyT, NI, NJ, NK>::Spectral()
        {
            this->template Init<TopologyT>();
        }


        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        double Spectral<TopologyT, NI, NJ, NK>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                              const LocalCoords& local_coords) const
        {
            const auto shape_function_index_list =
                Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

            const auto Ncomponent = local_coords.GetDimension();

            double ret = 1.;

            for (auto index = 0ul; index < Ncomponent; ++index)
                ret *= Internal::RefFEltNS::Interpolation(
                    points_per_ijk_[index], local_coords[index], shape_function_index_list[index]);


            return ret;
        }


        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        double Spectral<TopologyT, NI, NJ, NK>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                           Advanced::ComponentNS::index_type icoor,
                                                                           const LocalCoords& local_coords) const
        {
            const auto shape_function_index_list =
                Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

            double ret = 1.;

            const auto Ncomponent = Advanced::ComponentNS::index_type{ local_coords.GetDimension() };

            for (Advanced::ComponentNS::index_type index{ 0ul }; index < Ncomponent; ++index)
            {
                if (icoor == index)
                    ret *= Internal::RefFEltNS::DerivativeInterpolation(points_per_ijk_[index.Get()],
                                                                        local_coords[index.Get()],
                                                                        shape_function_index_list[index.Get()]);
                else
                    ret *= Internal::RefFEltNS::Interpolation(points_per_ijk_[index.Get()],
                                                              local_coords[index.Get()],
                                                              shape_function_index_list[index.Get()]);
            }

            return ret;
        }


        /////////////////////
        // PRIVATE METHODS //
        /////////////////////

        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        Advanced::LocalNode::vector_const_shared_ptr Spectral<TopologyT, NI, NJ, NK>::ComputeLocalNodeList()
        {
            //            static_assert(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Hexahedron>() ||
            //                          std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Quadrangle>() ||
            //                          std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Segment>(),
            //                          "Spectral finite element doesn't make sense for other topologies.");
            //
            //            static_assert(!(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Segment>() && (NJ != 0ul ||
            //            NK != 0ul)),
            //                          "Spectral upon segment can't be defined with NJ or NK > 0!");
            //
            //            static_assert(!(std::is_same<TopologyT, RefGeomEltNS::TopologyNS::Quadrangle>() && NK != 0ul),
            //                          "Spectral upon quadrangle can't be defined with NK > 0!");
            //

            Advanced::LocalNode::vector_const_shared_ptr local_node_list;

            std::size_t Ncomponent = 0;

            assert(std::all_of(points_per_ijk_.cbegin(),
                               points_per_ijk_.cend(),
                               [](const std::vector<double>& vector)
                               {
                                   return vector.empty();
                               })
                   && "This container is initialized in the present method which should be called only once!");

            // The local coords of the local dofs are computed with the Gauss-Lobatto formula.
            std::array<std::size_t, 3> Ngauss{ { NI, NJ, NK } };
            std::array<std::vector<double>, 3> weights; // I don't care about weight in present function;
            // only there for Gauss-Lobatto function prototype.


            for (std::size_t i = 0ul; i < 3ul; ++i)
            {
                if (Ngauss[i] > 0)
                {
                    QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss_Lobatto>(
                        Ngauss[i] + 1, points_per_ijk_[i], weights[i]);
                    Ncomponent = i + 1u;
                } else
                    points_per_ijk_[i] = { 0. };
            }


            // Create here all the local nodes.
            for (std::size_t k = 0ul; k <= NK; ++k)
            {
                const std::size_t is_k_border = (k == 0ul || k == NK ? 1u : 0ul);

                for (std::size_t j = 0ul; j <= NJ; ++j)
                {
                    const std::size_t is_j_border = (j == 0ul || j == NJ ? 1u : 0ul);

                    for (std::size_t i = 0ul; i <= NI; ++i)
                    {
                        const LocalNodeNS::index_type index{ k * (NJ + 1u) * (NI + 1u) + j * (NI + 1u) + i };
                        const std::size_t Nborder = is_k_border + is_j_border + (i == 0ul || i == NI ? 1u : 0ul);

                        InterfaceNS::Nature nature = InterfaceNS::Nature::none;

                        LocalCoords::unique_ptr local_coords_ptr(nullptr);

                        switch (Ncomponent)
                        {
                        case 1u:
                        {
                            std::vector<double> buf{ points_per_ijk_[0][i] };
                            local_coords_ptr = std::make_unique<LocalCoords>(buf);
                            break;
                        }
                        case 2u:
                        {
                            std::vector<double> buf{ points_per_ijk_[0][i], points_per_ijk_[1][j] };
                            local_coords_ptr = std::make_unique<LocalCoords>(buf);
                            break;
                        }
                        case 3u:
                        {
                            std::vector<double> buf{ points_per_ijk_[0][i],
                                                     points_per_ijk_[1][j],
                                                     points_per_ijk_[2][k] };
                            local_coords_ptr = std::make_unique<LocalCoords>(buf);
                            break;
                        }
                        default:
                        {
                            assert(false);
                            break;
                        }
                        }

                        assert(!(!local_coords_ptr));
                        const auto& local_coords = *local_coords_ptr;


                        switch (Nborder)
                        {
                        case 0ul:
                            nature = InterfaceNS::Nature::volume;
                            break;
                        case 1u:
                            nature = InterfaceNS::Nature::face;
                            break;
                        case 2u:
                            nature = InterfaceNS::Nature::edge;
                            break;
                        case 3u:
                            nature = InterfaceNS::Nature::vertex;
                            break;
                        default:
                            assert(false);
                        }

                        assert(nature != InterfaceNS::Nature::none);

                        auto&& local_interface =
                            RefGeomEltNS::TopologyNS::LocalData<TopologyT>::FindLocalInterface(nature, local_coords);


                        auto local_node_ptr = std::make_shared<const Advanced::LocalNode>(
                            std::move(local_interface), index, local_coords);

                        local_node_list.push_back(local_node_ptr);
                    }
                }
            }

            assert(local_node_list.size() == (NI + 1u) * (NJ + 1u) * (NK + 1u));

            return local_node_list;
        }


        template<class TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
        std::size_t Spectral<TopologyT, NI, NJ, NK>::GetOrder() const noexcept
        {
            return std::max({ NI, NJ, NK });
        }


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SPECTRAL_HXX_
