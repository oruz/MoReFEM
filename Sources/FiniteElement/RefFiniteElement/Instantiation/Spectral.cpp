/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Sep 2014 14:25:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <string>

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        namespace // anonymous
        {


            /////////////////////////////////////////////////////////////////////////////////////////////////
            // REGISTER THE MOST USUAL SPECTRAL ELEMENTS
            //
            // If for a problem you need a Spectral instantiation not registered here, you only need
            // to provide a similar line for it in any of your problem cpp file
            //
            // For instance if in your main cpp file you add:
            //
            // \code
            // namespace
            // {
            // using SegmentP12 =  RefFEltNS::Spectral<RefGeomEltNS::TopologyNS::Segment, 12, 0, 0>;
            //
            //    const bool register = Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
            //    __LINE__).Register<SegmentP12>();
            // }
            // \endcode
            //
            // you can safely use a SegmentP12 in your problem. Anisotropic spectral elements are for instance
            // not registered here.
            //
            /////////////////////////////////////////////////////////////////////////////////////////////////


            using SegmentP1 = Spectral<RefGeomEltNS::TopologyNS::Segment, 1, 0, 0>;

            //   __attribute__((unused)) const bool registered_segP1 =
            //       Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
            //       __LINE__).Register<SegmentP1>();

            using SegmentP2 = Spectral<RefGeomEltNS::TopologyNS::Segment, 2, 0, 0>;

            //   __attribute__((unused)) const bool registered_segP2 =
            //       Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
            //       __LINE__).Register<SegmentP2>();

            using SegmentP3 = Spectral<RefGeomEltNS::TopologyNS::Segment, 3, 0, 0>;

            __attribute__((unused)) const bool registered_segP3 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP3>();

            using SegmentP4 = Spectral<RefGeomEltNS::TopologyNS::Segment, 4, 0, 0>;

            __attribute__((unused)) const bool registered_segP4 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP4>();

            using SegmentP5 = Spectral<RefGeomEltNS::TopologyNS::Segment, 5, 0, 0>;

            __attribute__((unused)) const bool registered_segP5 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP5>();

            using SegmentP6 = Spectral<RefGeomEltNS::TopologyNS::Segment, 6, 0, 0>;

            __attribute__((unused)) const bool registered_segP6 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP6>();

            using SegmentP7 = Spectral<RefGeomEltNS::TopologyNS::Segment, 7, 0, 0>;

            __attribute__((unused)) const bool registered_segP7 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP7>();

            using SegmentP8 = Spectral<RefGeomEltNS::TopologyNS::Segment, 8, 0, 0>;

            __attribute__((unused)) const bool registered_segP8 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP8>();

            using SegmentP9 = Spectral<RefGeomEltNS::TopologyNS::Segment, 9, 0, 0>;

            __attribute__((unused)) const bool registered_segP9 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP9>();

            using SegmentP10 = Spectral<RefGeomEltNS::TopologyNS::Segment, 10, 0, 0>;

            __attribute__((unused)) const bool registered_segP10 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<SegmentP10>();

            using QuadrangleQ1 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 1, 1, 0>;

            __attribute__((unused)) const bool registered_quadQ1 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ1>();

            using QuadrangleQ2 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 2, 2, 0>;

            __attribute__((unused)) const bool registered_quadQ2 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ2>();

            using QuadrangleQ3 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 3, 3, 0>;

            __attribute__((unused)) const bool registered_quadQ3 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ3>();

            using QuadrangleQ4 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 4, 4, 0>;

            __attribute__((unused)) const bool registered_quadQ4 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ4>();

            using QuadrangleQ5 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 5, 5, 0>;

            __attribute__((unused)) const bool registered_quadQ5 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ5>();

            using QuadrangleQ6 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 6, 6, 0>;

            __attribute__((unused)) const bool registered_quadQ6 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ6>();

            using QuadrangleQ7 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 7, 7, 0>;

            __attribute__((unused)) const bool registered_quadQ7 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ7>();

            using QuadrangleQ8 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 8, 8, 0>;

            __attribute__((unused)) const bool registered_quadQ8 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ8>();

            using QuadrangleQ9 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 9, 9, 0>;

            __attribute__((unused)) const bool registered_quadQ9 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ9>();

            using QuadrangleQ10 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 10, 10, 0>;

            __attribute__((unused)) const bool registered_quadQ10 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ10>();

            using QuadrangleQ20 = Spectral<RefGeomEltNS::TopologyNS::Quadrangle, 20, 20, 0>;

            __attribute__((unused)) const bool registered_quadQ20 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<QuadrangleQ20>();

            using HexahedronQ1 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 1, 1, 1>;

            __attribute__((unused)) const bool registered_hexaQ1 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ1>();

            using HexahedronQ2 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 2, 2, 2>;

            __attribute__((unused)) const bool registered_hexaQ2 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ2>();

            using HexahedronQ3 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 3, 3, 3>;

            __attribute__((unused)) const bool registered_hexaQ3 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ3>();

            using HexahedronQ4 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 4, 4, 4>;

            __attribute__((unused)) const bool registered_hexaQ4 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ4>();

            using HexahedronQ5 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 5, 5, 5>;

            __attribute__((unused)) const bool registered_hexaQ5 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ5>();

            using HexahedronQ6 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 6, 6, 6>;

            __attribute__((unused)) const bool registered_hexaQ6 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ6>();

            using HexahedronQ7 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 7, 7, 7>;

            __attribute__((unused)) const bool registered_hexaQ7 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ7>();

            using HexahedronQ8 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 8, 8, 8>;

            __attribute__((unused)) const bool registered_hexaQ8 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ8>();

            using HexahedronQ9 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 9, 9, 9>;

            __attribute__((unused)) const bool registered_hexaQ9 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ9>();

            using HexahedronQ10 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 10, 10, 10>;

            __attribute__((unused)) const bool registered_hexaQ10 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ10>();

            using HexahedronQ20 = Spectral<RefGeomEltNS::TopologyNS::Hexahedron, 20, 20, 20>;

            __attribute__((unused)) const bool registered_hexaQ20 =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ20>();


        } // namespace


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
