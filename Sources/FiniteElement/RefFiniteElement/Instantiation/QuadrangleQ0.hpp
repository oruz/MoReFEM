/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 11 Oct 2020 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q0_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q0_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for QuadrangleQ0.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Quadrangle class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class QuadrangleQ0 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt<RefGeomEltNS::TopologyNS::Quadrangle,
                                                                                   Internal::ShapeFunctionNS::Order0,
                                                                                   InterfaceNS::Nature::none,
                                                                                   1u>
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit QuadrangleQ0() = default;

            //! Destructor.
            ~QuadrangleQ0() override;

            //! \copydoc doxygen_hide_copy_constructor
            QuadrangleQ0(const QuadrangleQ0& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            QuadrangleQ0(QuadrangleQ0&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            QuadrangleQ0& operator=(const QuadrangleQ0& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            QuadrangleQ0& operator=(QuadrangleQ0&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q0_HPP_
