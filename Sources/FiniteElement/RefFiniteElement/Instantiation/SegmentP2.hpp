/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P2_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P2_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment3.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for SegmentP2.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Segment class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class SegmentP2 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Segment,
            RefGeomEltNS::ShapeFunctionNS::Segment3,
            InterfaceNS::Nature::vertex,
            1u
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit SegmentP2() = default;

            //! Destructor.
            ~SegmentP2() override;

            //! \copydoc doxygen_hide_copy_constructor
            SegmentP2(const SegmentP2& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            SegmentP2(SegmentP2&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            SegmentP2& operator=(const SegmentP2& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            SegmentP2& operator=(SegmentP2&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P2_HPP_
