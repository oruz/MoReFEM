/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q1_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q1_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron8.hpp"
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for HexahedronQ1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Segment class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class HexahedronQ1
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Hexahedron,
            RefGeomEltNS::ShapeFunctionNS::Hexahedron8,
            InterfaceNS::Nature::vertex,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit HexahedronQ1() = default;

            //! Destructor.
            ~HexahedronQ1() override;

            //! \copydoc doxygen_hide_copy_constructor
            HexahedronQ1(const HexahedronQ1& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            HexahedronQ1(HexahedronQ1&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            HexahedronQ1& operator=(const HexahedronQ1& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            HexahedronQ1& operator=(HexahedronQ1&& rhs) = default;


            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q1_HPP_
