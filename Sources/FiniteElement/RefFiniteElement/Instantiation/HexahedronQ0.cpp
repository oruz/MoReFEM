/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 11 Oct 2020 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

// IWYU pragma: no_include <iosfwd>
#include <string> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Instantiation/HexahedronQ0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        namespace // anonymous
        {


            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__)
                    .Register<HexahedronQ0>();


        } // namespace


        const std::string& HexahedronQ0::ShapeFunctionLabel()
        {
            static std::string ret("Q0");
            return ret;
        }


        HexahedronQ0::~HexahedronQ0() = default;


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
