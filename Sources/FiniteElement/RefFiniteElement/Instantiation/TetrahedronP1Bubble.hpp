/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_BUBBLE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_BUBBLE_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TetrahedronP1Bubble.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TetrahedronP1 with a bubble.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Tetrahedron class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in
         * RefGeomEltNS::TopologyNS::Tetrahedron traits class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */

        class TetrahedronP1Bubble
        // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Tetrahedron,
            Internal::ShapeFunctionNS::TetrahedronP1Bubble,
            InterfaceNS::Nature::vertex,
            1u
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = TetrahedronP1Bubble;


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TetrahedronP1Bubble() = default;

            //! Destructor.
            virtual ~TetrahedronP1Bubble() override;

            //! \copydoc doxygen_hide_copy_constructor
            TetrahedronP1Bubble(const TetrahedronP1Bubble& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TetrahedronP1Bubble(TetrahedronP1Bubble&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TetrahedronP1Bubble& operator=(const TetrahedronP1Bubble& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TetrahedronP1Bubble& operator=(TetrahedronP1Bubble&& rhs) = default;

            ///@}

          private:
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_BUBBLE_HPP_
