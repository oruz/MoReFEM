/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment2.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for SegmentP1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Segment class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class SegmentP1 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Segment,
            RefGeomEltNS::ShapeFunctionNS::Segment2,
            InterfaceNS::Nature::vertex,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit SegmentP1() = default;

            //! Destructor.
            ~SegmentP1() override;

            //! \copydoc doxygen_hide_copy_constructor
            SegmentP1(const SegmentP1& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            SegmentP1(SegmentP1&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            SegmentP1& operator=(const SegmentP1& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            SegmentP1& operator=(SegmentP1&& rhs) = default;


            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_SEGMENT_P1_HPP_
