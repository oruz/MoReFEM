/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q1_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q1_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle4.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for QuadrangleQ1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Segment class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class QuadrangleQ1
        // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Quadrangle,
            RefGeomEltNS::ShapeFunctionNS::Quadrangle4,
            InterfaceNS::Nature::vertex,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit QuadrangleQ1() = default;

            //! Destructor.
            ~QuadrangleQ1() override;

            //! \copydoc doxygen_hide_copy_constructor
            QuadrangleQ1(const QuadrangleQ1& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            QuadrangleQ1(QuadrangleQ1&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            QuadrangleQ1& operator=(const QuadrangleQ1& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            QuadrangleQ1& operator=(QuadrangleQ1&& rhs) = default;


            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q1_HPP_
