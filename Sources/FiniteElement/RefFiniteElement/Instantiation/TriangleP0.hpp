/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P0_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P0_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TriangleP0.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Triangle class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TriangleP0
        // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Triangle,
            Internal::ShapeFunctionNS::Order0,
            InterfaceNS::Nature::none,
            1u
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TriangleP0() = default;

            //! Destructor.
            ~TriangleP0() override;

            //! \copydoc doxygen_hide_copy_constructor
            TriangleP0(const TriangleP0& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TriangleP0(TriangleP0&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TriangleP0& operator=(const TriangleP0& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TriangleP0& operator=(TriangleP0&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P0_HPP_
