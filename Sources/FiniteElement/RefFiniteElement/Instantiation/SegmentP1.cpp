/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "FiniteElement/RefFiniteElement/Instantiation/SegmentP1.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        namespace // anonymous
        {


            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP1>();


        } // namespace


        const std::string& SegmentP1::ShapeFunctionLabel()
        {
            static std::string ret("P1");
            return ret;
        }


        SegmentP1::~SegmentP1() = default;


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
