/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron4.hpp"
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TetrahedronP1.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Tetrahedron class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TetrahedronP1
        // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Tetrahedron,
            RefGeomEltNS::ShapeFunctionNS::Tetrahedron4,
            InterfaceNS::Nature::vertex,
            0ul
        >
        // clang-format on
        {

          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TetrahedronP1() = default;

            //! Destructor.
            ~TetrahedronP1() override;

            //! \copydoc doxygen_hide_copy_constructor
            TetrahedronP1(const TetrahedronP1& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TetrahedronP1(TetrahedronP1&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TetrahedronP1& operator=(const TetrahedronP1& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TetrahedronP1& operator=(TetrahedronP1&& rhs) = default;

            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P1_HPP_
