/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q2C_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q2C_HPP_

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle8.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for QuadrangleQ2c.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Quadrangle class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in RefGeomEltNS::TopologyNS::Quadrangle
         * traits class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        // clang-format off
        class QuadrangleQ2c
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Quadrangle,
            RefGeomEltNS::ShapeFunctionNS::Quadrangle8,
            InterfaceNS::Nature::edge,
            0ul
        >
        // clang-format on
        {


          public:
            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit QuadrangleQ2c() = default;

            //! Destructor.
            virtual ~QuadrangleQ2c() override;

            //! \copydoc doxygen_hide_copy_constructor
            QuadrangleQ2c(const QuadrangleQ2c& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            QuadrangleQ2c(QuadrangleQ2c&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            QuadrangleQ2c& operator=(const QuadrangleQ2c& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            QuadrangleQ2c& operator=(QuadrangleQ2c&& rhs) = default;


            ///@}
        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_QUADRANGLE_Q2C_HPP_
