/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "FiniteElement/RefFiniteElement/Instantiation/QuadrangleQ1.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        namespace // anonymous
        {

            // #896 obsolete with the correction of the spectral elements.
            //__attribute__((unused)) const bool registered =
            //    Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
            //    __LINE__).Register<QuadrangleQ1>();


        } // namespace


        const std::string& QuadrangleQ1::ShapeFunctionLabel()
        {
            static std::string ret("Q1");
            return ret;
        }


        QuadrangleQ1::~QuadrangleQ1() = default;


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
