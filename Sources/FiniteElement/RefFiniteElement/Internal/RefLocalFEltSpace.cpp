/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:12:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>
#include <vector>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            RefLocalFEltSpace::RefLocalFEltSpace(const RefGeomElt::shared_ptr& ref_geom_elt,
                                                 const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                                 const std::size_t mesh_dimension,
                                                 const std::size_t felt_space_dimension)
            : ref_geom_elt_(ref_geom_elt), Nnode_(0ul), Ndof_(0ul)
            {
                assert(!(!ref_geom_elt));
                assert(felt_space_dimension <= mesh_dimension && "To check order has not been inverted!");

                const auto& factory = Internal::RefFEltNS::BasicRefFEltFactory::GetInstance(__FILE__, __LINE__);

                for (const auto& extended_unknown_ptr : unknown_list)
                {
                    assert(!(!extended_unknown_ptr));
                    const auto& extended_unknown = *extended_unknown_ptr;

                    const auto& basic_ref_felt = factory.FetchBasicRefFElt(ref_geom_elt->GetTopologyName(),
                                                                           extended_unknown.GetShapeFunctionLabel());

                    auto&& fe_type_unknown = std::make_shared<RefFEltInFEltSpace>(
                        basic_ref_felt, extended_unknown, mesh_dimension, felt_space_dimension);

                    Ndof_ += fe_type_unknown->Ndof();
                    Nnode_ += fe_type_unknown->Nnode();

                    ref_felt_list_.emplace_back(std::move(fe_type_unknown));
                }
            }


            const RefFEltInFEltSpace& RefLocalFEltSpace::GetRefFElt(const FEltSpace& felt_space,
                                                                    const Unknown& unknown) const
            {
                const auto& extended_unknown = felt_space.GetExtendedUnknown(unknown);
                return GetRefFElt(extended_unknown);
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
