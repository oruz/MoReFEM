/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Nov 2014 16:03:44 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_FACTORY_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_FACTORY_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"

#include <memory>

#include "Utilities/Exceptions/Factory.hpp"

#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            template<class BasicRefFEltT>
            bool BasicRefFEltFactory::Register()
            {
                using Topology = typename BasicRefFEltT::topology;

                auto&& key = GenerateKey(Topology::ClassName(), BasicRefFEltT::ShapeFunctionLabel());

                auto holder_ptr = std::make_unique<Impl::BasicRefFEltHolder<BasicRefFEltT>>();

                if (!basic_ref_felt_storage_.insert(make_pair(key, std::move(holder_ptr))).second)
                {
                    ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(
                        key, "topology name and shape function label", __FILE__, __LINE__));
                }

                known_pair_.push_back({ Topology::ClassName(), BasicRefFEltT::ShapeFunctionLabel() });

                return true;
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_FACTORY_HXX_
