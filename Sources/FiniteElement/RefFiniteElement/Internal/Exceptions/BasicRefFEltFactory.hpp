/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Nov 2014 16:03:44 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_EXCEPTIONS_x_BASIC_REF_F_ELT_FACTORY_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_EXCEPTIONS_x_BASIC_REF_F_ELT_FACTORY_HPP_

#include <iosfwd>
#include <utility>
#include <vector>

#include "Utilities/Exceptions/Factory.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            namespace BasicRefFEltFactoryNS
            {


                namespace ExceptionNS
                {


                    //! Generic exception for factory.
                    class Exception : public ::MoReFEM::ExceptionNS::Factory::Exception
                    {
                      public:
                        /*!
                         * \brief Constructor with simple message.
                         *
                         * \param[in] msg Message.
                         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                         */
                        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                        //! Destructor
                        virtual ~Exception() override;

                        //! \copydoc doxygen_hide_copy_constructor
                        Exception(const Exception& rhs) = default;

                        //! \copydoc doxygen_hide_move_constructor
                        Exception(Exception&& rhs) = default;

                        //! \copydoc doxygen_hide_copy_affectation
                        Exception& operator=(const Exception& rhs) = default;

                        //! \copydoc doxygen_hide_move_affectation
                        Exception& operator=(Exception&& rhs) = default;
                    };


                    //! Thrown when trying to create an object which key is not registered.
                    class UnregisteredObject final : public Exception
                    {
                      public:
                        /*!
                         * \brief Thrown when trying to create an object which keu is not registered.
                         *
                         * \param[in] topology_name Name of the topology for which the problem arose.
                         * For instance 'triangle'.
                         * \param[in] shape_function_label Shape function label involved.
                         * \param[in] known_pair_list List of all combination known by the factory.
                         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                         */
                        explicit UnregisteredObject(
                            const std::string& topology_name,
                            const std::string& shape_function_label,
                            const std::vector<std::pair<std::string, std::string>>& known_pair_list,
                            const char* invoking_file,
                            int invoking_line);

                        //! Destructor
                        virtual ~UnregisteredObject() override;

                        //! \copydoc doxygen_hide_copy_constructor
                        UnregisteredObject(const UnregisteredObject& rhs) = default;

                        //! \copydoc doxygen_hide_move_constructor
                        UnregisteredObject(UnregisteredObject&& rhs) = default;

                        //! \copydoc doxygen_hide_copy_affectation
                        UnregisteredObject& operator=(const UnregisteredObject& rhs) = default;

                        //! \copydoc doxygen_hide_move_affectation
                        UnregisteredObject& operator=(UnregisteredObject&& rhs) = default;
                    };


                } // namespace ExceptionNS


            } // namespace BasicRefFEltFactoryNS


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_EXCEPTIONS_x_BASIC_REF_F_ELT_FACTORY_HPP_
