/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 17:15:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>

#include "Utilities/Containers/Print.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            BasicRefFElt::BasicRefFElt()
            : topology_dimension_(NumericNS::UninitializedIndex<decltype(topology_dimension_)>())
            { }


            BasicRefFElt::~BasicRefFElt() = default;


            void BasicRefFElt::Print(std::ostream& out) const
            {
                const std::size_t Nvertex = this->NnodeOnVertex();
                const std::size_t Nedge = this->NnodeOnEdge();
                const std::size_t Nface = this->NnodeOnFace();


                for (std::size_t i = 0ul; i < Nvertex; ++i)
                {
                    auto vertex_dof_ptr = GetLocalNodeOnVertexPtr(i);

                    if (!vertex_dof_ptr)
                        continue;

                    out << "Vertex " << i << ": (" << vertex_dof_ptr->GetIndex() << ')' << std::endl;
                }


                if (AreNodesOnEdges())
                {
                    for (std::size_t i = 0ul; i < Nedge; ++i)
                    {
                        for (std::size_t orientation = 0ul; orientation < 2u; ++orientation)
                        {
                            auto dof_on_current_edge_list = GetLocalNodeOnEdgeList(i, orientation);

                            if (dof_on_current_edge_list.empty())
                                continue;

                            std::vector<LocalNodeNS::index_type> indexes;

                            for (const auto& local_node_ptr : dof_on_current_edge_list)
                            {
                                assert(!(!local_node_ptr));
                                indexes.push_back(local_node_ptr->GetIndex());
                            }

                            out << "Edge " << i << ", Orientation " << orientation << ": ";
                            Utilities::PrintContainer<>::Do(indexes,
                                                            out,
                                                            ::MoReFEM::PrintNS::Delimiter::separator(","),
                                                            ::MoReFEM::PrintNS::Delimiter::opener("("),
                                                            ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
                        }
                    }
                }


                if (AreNodesOnFaces())
                {
                    for (std::size_t i = 0ul; i < Nface; ++i)
                    {
                        for (std::size_t orientation = 0ul; orientation < 8u; ++orientation)
                        {
                            auto dof_on_current_face_list = GetLocalNodeOnFaceList(i, orientation);

                            if (dof_on_current_face_list.empty())
                                continue;

                            std::vector<LocalNodeNS::index_type> indexes;

                            for (auto local_node_ptr : dof_on_current_face_list)
                            {
                                assert(!(!local_node_ptr));
                                indexes.push_back(local_node_ptr->GetIndex());
                            }

                            out << "Face " << i << ", Orientation " << orientation << ": ";
                            Utilities::PrintContainer<>::Do(indexes,
                                                            out,
                                                            ::MoReFEM::PrintNS::Delimiter::separator(","),
                                                            ::MoReFEM::PrintNS::Delimiter::opener("("),
                                                            ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
                        }
                    }
                }


                if (AreNodesOnVolume())
                {
                    const auto& local_node_list_on_current_volume = GetLocalNodeOnVolumeList();

                    std::vector<LocalNodeNS::index_type> indexes;

                    for (auto local_node_ptr : local_node_list_on_current_volume)
                    {
                        assert(!(!local_node_ptr));
                        indexes.push_back(local_node_ptr->GetIndex());
                    }

                    out << "Volume: ";
                    Utilities::PrintContainer<>::Do(indexes,
                                                    out,
                                                    ::MoReFEM::PrintNS::Delimiter::separator(","),
                                                    ::MoReFEM::PrintNS::Delimiter::opener("("),
                                                    ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
                }
            }


#ifndef NDEBUG


            //! Local function to check whether the list are well-formed.
            void CheckLocalNodeListConsistency(const Advanced::LocalNode::vector_const_shared_ptr& local_node_list,
                                               InterfaceNS::Nature nature)
            {
                for (const auto& node_ptr : local_node_list)
                {
                    assert(!(!node_ptr));
                    assert(node_ptr->GetLocalInterface().GetNature() == nature);
                }
            }

#endif // NDEBUG


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
