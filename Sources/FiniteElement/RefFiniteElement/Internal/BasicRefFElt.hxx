/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 17:15:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

// IWYU pragma: no_include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/Miscellaneous.hpp"


#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            /////////////////
            // CONSTRUCTOR //
            /////////////////

            template<class TopologyT>
            void BasicRefFElt::Init()
            {
                local_node_list_ = ComputeLocalNodeList();

                topology_dimension_ = TopologyT::dimension;

                Advanced::LocalNode::vector_const_shared_ptr local_node_on_edge_list;
                Advanced::LocalNode::vector_const_shared_ptr local_node_on_face_list;

                {
                    const auto& local_node_list = GetLocalNodeList();

                    for (const auto& local_node_ptr : local_node_list)
                    {
                        assert(!(!local_node_ptr));
                        const auto interface_nature = local_node_ptr->GetLocalInterface().GetNature();

                        switch (interface_nature)
                        {
                        case ::MoReFEM::InterfaceNS::Nature::vertex:
                            local_node_on_vertex_list_.push_back(local_node_ptr);
                            break;
                        case ::MoReFEM::InterfaceNS::Nature::edge:
                            local_node_on_edge_list.push_back(local_node_ptr);
                            break;
                        case ::MoReFEM::InterfaceNS::Nature::face:
                            local_node_on_face_list.push_back(local_node_ptr);
                            break;
                        case ::MoReFEM::InterfaceNS::Nature::volume:
                            local_node_on_volume_list_.push_back(local_node_ptr);
                            break;
                        case ::MoReFEM::InterfaceNS::Nature::none:
                        case ::MoReFEM::InterfaceNS::Nature::undefined:
                            assert(false);
                            exit(EXIT_FAILURE);
                        }
                    }
                }

                if (!local_node_on_vertex_list_.empty())
                    SortLocalNodeOnVertexList<TopologyT>();

                SetEdgeLocalNodeList<TopologyT>(local_node_on_edge_list);
                SetFaceLocalNodeList<TopologyT>(local_node_on_face_list);
            }


            ////////////////////////////////////////////////
            // ACCESSORS TO NODES REGARDLESS OF INTERFACE //
            ////////////////////////////////////////////////

            inline std::size_t BasicRefFElt::NlocalNode() const noexcept
            {
                return local_node_list_.size();
            }


            inline const Advanced::LocalNode::vector_const_shared_ptr& BasicRefFElt::GetLocalNodeList() const noexcept
            {
                return local_node_list_;
            }


            inline const Advanced::LocalNode& BasicRefFElt::GetLocalNode(std::size_t i) const noexcept
            {
                const auto& list = GetLocalNodeList();
                const std::size_t index = i;
                assert(index < list.size());
                assert(!(!(list[index])));
                return *list[index];
            }


            ////////////////////////////////////
            // ACCESSORS TO NODES ON VERTICES //
            ////////////////////////////////////


            inline bool BasicRefFElt::AreNodesOnVertices() const noexcept
            {
                return NnodeOnVertex() > 0ul;
            }


            inline const Advanced::LocalNode::const_shared_ptr&
            BasicRefFElt ::GetLocalNodeOnVertexPtr(std::size_t topologic_vertex_index) const noexcept
            {
                const auto& list = GetLocalNodeOnVertexList();
                const std::size_t index = topologic_vertex_index;
                assert(index < list.size());
                assert(!(!(list[index])));
                assert("Make sure the index given in input is relevant."
                       && list[index]->GetLocalInterface().GetNature() == ::MoReFEM::InterfaceNS::Nature::vertex);
                return list[index];
            }


            inline const Advanced::LocalNode&
            BasicRefFElt::GetLocalNodeOnVertex(std::size_t topologic_vertex_index) const noexcept
            {
                return *GetLocalNodeOnVertexPtr(topologic_vertex_index);
            }


            inline const Advanced::LocalNode::vector_const_shared_ptr&
            BasicRefFElt::GetLocalNodeOnVertexList() const noexcept
            {
#ifndef NDEBUG
                CheckLocalNodeListConsistency(local_node_on_vertex_list_, ::MoReFEM::InterfaceNS::Nature::vertex);
#endif // NDEBUG
                return local_node_on_vertex_list_;
            }


            /////////////////////////////////
            // ACCESSORS TO NODES ON EDGES //
            /////////////////////////////////


            inline bool BasicRefFElt::AreNodesOnEdges() const noexcept
            {
                return NnodeOnEdge() > 0ul;
            }


            inline const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
            BasicRefFElt ::GetLocalNodeOnEdgeList(std::size_t topologic_edge_index) const noexcept
            {
                assert(topologic_edge_index < local_node_on_edge_storage_.size());
                return local_node_on_edge_storage_[topologic_edge_index];
            }


            inline const Advanced::LocalNode::vector_const_shared_ptr&
            BasicRefFElt ::GetLocalNodeOnEdgeList(std::size_t topologic_edge_index,
                                                  std::size_t orientation) const noexcept
            {
                const auto& edge_content = GetLocalNodeOnEdgeList(topologic_edge_index);
                assert(edge_content.size() > orientation);
                assert(!edge_content[orientation].empty());

                return edge_content[orientation];
            }


            /////////////////////////////////
            // ACCESSORS TO NODES ON FACES //
            /////////////////////////////////

            inline bool BasicRefFElt::AreNodesOnFaces() const noexcept
            {
                return NnodeOnFace() > 0ul;
            }


            inline const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
            BasicRefFElt ::GetLocalNodeOnFaceList(std::size_t topologic_face_index) const noexcept
            {
                assert(topologic_face_index < local_node_on_face_storage_.size());
                return local_node_on_face_storage_[topologic_face_index];
            }


            inline const Advanced::LocalNode::vector_const_shared_ptr&
            BasicRefFElt ::GetLocalNodeOnFaceList(std::size_t topologic_face_index,
                                                  std::size_t orientation) const noexcept
            {
                const auto& face_content = GetLocalNodeOnFaceList(topologic_face_index);
                assert(face_content.size() > orientation);
                assert(!face_content[orientation].empty());

                return face_content[orientation];
            }


            //////////////////////////////////
            // ACCESSORS TO NODES ON VOLUME //
            //////////////////////////////////

            inline bool BasicRefFElt::AreNodesOnVolume() const noexcept
            {
                return !local_node_on_volume_list_.empty();
            }


            inline const Advanced::LocalNode::vector_const_shared_ptr&
            BasicRefFElt::GetLocalNodeOnVolumeList() const noexcept
            {
#ifndef NDEBUG
                CheckLocalNodeListConsistency(local_node_on_volume_list_, ::MoReFEM::InterfaceNS::Nature::volume);
#endif // NDEBUG

                return local_node_on_volume_list_;
            }


            inline std::size_t BasicRefFElt::GetTopologyDimension() const noexcept
            {
                return topology_dimension_;
            }


            /////////////////////
            // PRIVATE METHODS //
            /////////////////////


            template<class TopologyT>
            void BasicRefFElt::SortLocalNodeOnVertexList()
            {
                Advanced::LocalNode::vector_const_shared_ptr sorted_node_on_vertex_list(TopologyT::Nvertex, nullptr);

                const auto& unsorted_node_on_vertex_list = GetLocalNodeOnVertexList();

                for (const auto& local_node_ptr : unsorted_node_on_vertex_list)
                {
                    for (std::size_t topologic_vertex_index = 0ul; topologic_vertex_index < TopologyT::Nvertex;
                         ++topologic_vertex_index)
                    {
                        assert(!(!local_node_ptr));
                        if (TopologyT::IsOnVertex(topologic_vertex_index, local_node_ptr->GetLocalCoords()))
                        {
                            assert(sorted_node_on_vertex_list[topologic_vertex_index] == nullptr);
                            sorted_node_on_vertex_list[topologic_vertex_index] = local_node_ptr;
                        }
                    }
                }

                assert(std::none_of(sorted_node_on_vertex_list.cbegin(),
                                    sorted_node_on_vertex_list.cend(),
                                    Utilities::IsNullptr<Advanced::LocalNode::const_shared_ptr>));

                std::swap(sorted_node_on_vertex_list, local_node_on_vertex_list_);
            }


            template<class TopologyT>
            std::enable_if_t<std::is_same<typename TopologyT::EdgeTopology, std::false_type>::value, void>
            BasicRefFElt::SetEdgeLocalNodeList(
                const Advanced::LocalNode::vector_const_shared_ptr& /* local_node_on_edge_list */)
            {
                // Do nothing.
            }


            template<class TopologyT>
            std::enable_if_t<!std::is_same<typename TopologyT::EdgeTopology, std::false_type>::value, void>
            BasicRefFElt::SetEdgeLocalNodeList(
                const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_edge_list)
            {
                // If no local dofs on edge, nothing to do.
                if (local_node_on_edge_list.empty())
                    return;

                local_node_on_edge_storage_.resize(TopologyT::Nedge);

                Advanced::LocalNode::vector_const_shared_ptr ret;

                constexpr std::size_t Nedge_orientation = 2u;

                for (std::size_t topologic_edge_index = 0ul; topologic_edge_index < TopologyT::Nedge;
                     ++topologic_edge_index)
                {
                    auto& edge_content = local_node_on_edge_storage_[topologic_edge_index];
                    edge_content.resize(Nedge_orientation);

                    for (auto orientation = 0ul; orientation < Nedge_orientation; ++orientation)
                    {
                        auto& local_dof_on_current_edge_list = edge_content[orientation];

                        for (const auto& local_node_ptr : local_node_on_edge_list)
                        {
                            assert(!(!local_node_ptr));

                            if (TopologyT::IsOnEdge(topologic_edge_index, local_node_ptr->GetLocalCoords()))
                                local_dof_on_current_edge_list.push_back(local_node_ptr);
                        }

                        assert(!local_dof_on_current_edge_list.empty());

                        if (orientation == 1ul)
                            std::reverse(local_dof_on_current_edge_list.begin(), local_dof_on_current_edge_list.end());
                    }

                    assert(edge_content.size() == Nedge_orientation);
                }
            }


            template<class TopologyT>
            std::enable_if_t<std::is_same<typename TopologyT::FaceTopology, std::false_type>::value, void>
            BasicRefFElt::SetFaceLocalNodeList(
                const Advanced::LocalNode::vector_const_shared_ptr& /* local_node_on_face_list */)
            {
                // Do nothing.
            }


            template<class TopologyT>
            std::enable_if_t<!std::is_same<typename TopologyT::FaceTopology, std::false_type>::value, void>
            BasicRefFElt::SetFaceLocalNodeList(
                const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_face_list)
            {
                // If no local dofs on faces, nothing to do.
                if (local_node_on_face_list.empty())
                    return;

                local_node_on_face_storage_.resize(TopologyT::Nface);


                Advanced::LocalNode::vector_const_shared_ptr ret;

                // \todo #250 This won't work for elements with different type of faces... (pyramids for instance)
                constexpr std::size_t Nface_orientation = 2u * TopologyT::FaceTopology::Nvertex;

                for (std::size_t topologic_face_index = 0ul; topologic_face_index < TopologyT::Nface;
                     ++topologic_face_index)
                {
                    Advanced::LocalNode::vector_const_shared_ptr local_node_on_current_face_without_orientation;

                    for (const auto& local_node_ptr : local_node_on_face_list)
                    {
                        assert(!(!local_node_ptr));

                        if (TopologyT::IsOnFace(topologic_face_index, local_node_ptr->GetLocalCoords()))
                            local_node_on_current_face_without_orientation.push_back(local_node_ptr);
                    }

                    assert(!local_node_on_current_face_without_orientation.empty());

                    assert(topologic_face_index < local_node_on_face_storage_.size());

                    auto& face_content = local_node_on_face_storage_[topologic_face_index];
                    face_content.resize(Nface_orientation);

                    assert(face_content.size() == Nface_orientation);

                    for (auto orientation = 0ul; orientation < Nface_orientation; ++orientation)
                    {
                        auto& local_node_on_current_face = face_content[orientation];

                        for (const auto& local_node_ptr : local_node_on_current_face_without_orientation)
                        {
                            auto&& new_coords = TopologyT::TransformFacePoint(
                                local_node_ptr->GetLocalCoords(), topologic_face_index, orientation);

                            auto it = std::find_if(local_node_on_current_face_without_orientation.cbegin(),
                                                   local_node_on_current_face_without_orientation.cend(),
                                                   [&new_coords](const auto& local_node_in_list_ptr)
                                                   {
                                                       assert(!(!local_node_in_list_ptr));
                                                       return local_node_in_list_ptr->GetLocalCoords() == new_coords;
                                                   });

                            assert(it != local_node_on_current_face_without_orientation.cend());
                            local_node_on_current_face.push_back(*it);
                        }
                        assert(!local_node_on_current_face.empty());
                    }
                }
            }


            inline std::size_t BasicRefFElt::NnodeOnVertex() const noexcept
            {
                return local_node_on_vertex_list_.size();
            }


            inline std::size_t BasicRefFElt::NnodeOnEdge() const noexcept
            {
                return local_node_on_edge_storage_.size();
            }


            inline std::size_t BasicRefFElt::NnodeOnFace() const noexcept
            {
                return local_node_on_face_storage_.size();
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HXX_
