/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Nov 2014 09:54:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            namespace Impl
            {


                AbstractBasicRefFEltHolder::~AbstractBasicRefFEltHolder() = default;


            } // namespace Impl


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
