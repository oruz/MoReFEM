/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:18:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"

#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            inline const BasicRefFElt& RefFEltInFEltSpace::GetBasicRefFElt() const noexcept
            {
                return basic_ref_felt_;
            }


            inline std::size_t RefFEltInFEltSpace::Nnode() const noexcept
            {
                return GetBasicRefFElt().NlocalNode();
            }


            inline const ExtendedUnknown& RefFEltInFEltSpace::GetExtendedUnknown() const noexcept
            {
                return extended_unknown_;
            }


            inline std::size_t RefFEltInFEltSpace::GetMeshDimension() const noexcept
            {
                return mesh_dimension_;
            }


            inline std::size_t RefFEltInFEltSpace::GetFEltSpaceDimension() const noexcept
            {
                return felt_space_dimension_;
            }


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HXX_
