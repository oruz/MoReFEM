/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_

// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/Dof.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Numeric/Numeric.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Dof; }
namespace MoReFEM { class Node; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline std::size_t Dof::GetInternalProcessorWiseOrGhostIndex() const noexcept
    {
        assert(internal_processor_wise_or_ghost_index_ != NumericNS::UninitializedIndex<std::size_t>());
        return internal_processor_wise_or_ghost_index_;
    }


    inline bool operator<(const Dof& lhs, const Dof& rhs)
    {
        return (lhs.GetInternalProcessorWiseOrGhostIndex() < rhs.GetInternalProcessorWiseOrGhostIndex());
    }


    inline bool operator==(const Dof& lhs, const Dof& rhs)
    {
        return (lhs.GetInternalProcessorWiseOrGhostIndex() == rhs.GetInternalProcessorWiseOrGhostIndex());
    }


    inline const Dof::index_per_numbering_subset_type& Dof::GetProcessorWiseOrGhostIndexPerNumberingSubset() const
    {
        return processor_wise_or_ghost_index_per_numbering_subset_;
    }


    inline const Dof::index_per_numbering_subset_type& Dof::GetProgramWiseIndexPerNumberingSubset() const
    {
        return program_wise_index_per_numbering_subset_;
    }


    inline std::shared_ptr<const Node> Dof::GetNodeFromWeakPtr() const
    {
        assert(!node_.expired());
        return node_.lock();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HXX_
