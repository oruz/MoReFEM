/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_

// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    inline ::MoReFEM::NodeBearerNS::program_wise_index_type NodeBearer::GetProgramWiseIndex() const noexcept
    {
        assert(program_wise_index_ != NumericNS::UninitializedIndex<NodeBearerNS::program_wise_index_type>());
        return program_wise_index_;
    }


    inline std::size_t NodeBearer::GetProcessor() const noexcept
    {
        assert(processor_ != NumericNS::UninitializedIndex<std::size_t>());
        return processor_;
    }


    inline bool operator<(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetProgramWiseIndex() < rhs.GetProgramWiseIndex();
    }


    inline bool operator==(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetProgramWiseIndex() == rhs.GetProgramWiseIndex();
    }


    inline InterfaceNS::Nature NodeBearer::GetNature() const noexcept
    {
        assert(!(!interface_));
        return interface_->GetNature();
    }


    inline const Interface& NodeBearer::GetInterface() const noexcept
    {
        assert(!(!interface_));
        return *interface_;
    }


    inline const Node::vector_shared_ptr& NodeBearer::GetNodeList() const noexcept
    {
        return node_list_;
    }


    inline DofNumberingScheme CurrentDofNumberingScheme()
    {
        return DofNumberingScheme::contiguous_per_node;
    }


    inline bool NodeBearer::IsEmpty() const noexcept
    {
        return node_list_.empty();
    }


    inline bool NodeBearer::IsGhost(const Wrappers::Mpi& mpi) const noexcept
    {
        return GetProcessor() != mpi.GetRank<std::size_t>();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_
