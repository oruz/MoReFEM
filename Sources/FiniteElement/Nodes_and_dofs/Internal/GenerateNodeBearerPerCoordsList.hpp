//! \file
//
//
//  GenerateNodeBearerPerCoordsList.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 14/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_GENERATE_NODE_BEARER_PER_COORDS_LIST_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_GENERATE_NODE_BEARER_PER_COORDS_LIST_HPP_


#include <unordered_map>
#include <vector>

#include "Geometry/Coords/StrongType.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Utilities { struct ContainerHash; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::NodeBearerNS
{


    // clang-format off
    //! Convenient alias.
    using node_bearer_per_coords_index_list_type = std::unordered_map
    <
        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>,
        NodeBearer::shared_ptr,
        Utilities::ContainerHash
    >;
    // clang-format on


    /*!
     * \brief iterates over a list of \a NodeBearer and sort them by the \a Coords list related to its underlying \a Interface.
     *
     * \internal This is in Internal namespace for a good reason: it is only required as an helper in some initialization stuff, such as for \a CoordsMatching
     * opetator.
     *
     * \param[in] node_bearer_list The \a NodeBearer list that will populate the return value.
     *
     * \return Key is the list of \a Coords indexes that delimits the \a Interface, value is the \a NodeBearer. The chosen index is the one read from
     * the mesh file - thos choice stems from the use case of \a CoordsMatching.
     */
    node_bearer_per_coords_index_list_type
    GenerateNodeBearerPerCoordsList(const NodeBearer::vector_shared_ptr& node_bearer_list);


} // namespace MoReFEM::Internal::NodeBearerNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_GENERATE_NODE_BEARER_PER_COORDS_LIST_HPP_
