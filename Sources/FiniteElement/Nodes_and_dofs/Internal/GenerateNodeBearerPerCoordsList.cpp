//! \file
//
//
//  GenerateNodeBearerPerCoordsList.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 14/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#include <algorithm>
#include <cassert>
#include <functional>
#include <memory>
#include <type_traits>

#include "Utilities/Containers/UnorderedMap.hpp" // IWYU pragma: keep

#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"


namespace MoReFEM::Internal::NodeBearerNS
{


    node_bearer_per_coords_index_list_type
    GenerateNodeBearerPerCoordsList(const NodeBearer::vector_shared_ptr& node_bearer_list)
    {
        node_bearer_per_coords_index_list_type ret;

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));

            decltype(auto) interface = node_bearer_ptr->GetInterface();

            decltype(auto) coords_list =
                interface.ComputeVertexCoordsIndexList<::MoReFEM::CoordsNS::index_enum::from_mesh_file>();

            Internal::InterfaceNS::OrderCoordsList(coords_list, std::less<::MoReFEM::CoordsNS::index_from_mesh_file>());

            auto [it, is_inserted] = ret.insert({ coords_list, node_bearer_ptr });
            assert(is_inserted);
            static_cast<void>(is_inserted);
        }

        return ret;
    }


} // namespace MoReFEM::Internal::NodeBearerNS
