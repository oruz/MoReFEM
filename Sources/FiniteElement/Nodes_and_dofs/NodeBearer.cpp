/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <iterator>
#include <memory>
#include <set>
#include <string>
#include <type_traits>
#include <vector>
// IWYU pragma: no_include <__tree>

#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    NodeBearer::NodeBearer(const Interface::shared_ptr& interface) : interface_(interface)
    {
        assert(!(!interface));
    }


    Node::vector_shared_ptr NodeBearer ::GetNodeList(const Unknown& unknown,
                                                     const std::string& shape_function_label) const
    {
        const auto& node_list = GetNodeList();

        static_cast<void>(shape_function_label); // #1146 temporary!

        Node::vector_shared_ptr ret;

        std::copy_if(node_list.cbegin(),
                     node_list.cend(),
                     std::back_inserter(ret),
                     [&unknown /*, &shape_function_label*/](const auto& node_ptr)
                     {
                         assert(!(!node_ptr));
                         return node_ptr->GetUnknown() == unknown;
                         // #1146 Temporary deactivated.
                         // && node_ptr->GetShapeFunctionLabel() == shape_function_label;
                     });

        return ret;
    }


    Node::shared_ptr NodeBearer::AddNode(const ExtendedUnknown& extended_unknown, const std::size_t Ndof)
    {
        assert(Ndof > 0ul);

        auto node_ptr = std::make_shared<Node>(shared_from_this(), extended_unknown);
        node_ptr->Init(Ndof);

        node_list_.push_back(node_ptr);

        return node_ptr;
    }


    void NodeBearer::SetProcessor(std::size_t processor)
    {
        processor_ = processor;
    }


    bool NodeBearer::IsUnknown(const Unknown& unknown) const
    {
        const auto& node_list = GetNodeList();

        const auto end = node_list.cend();
        const auto it = std::find_if(node_list.cbegin(),
                                     end,
                                     [&unknown](const auto& node_ptr)
                                     {
                                         assert(!(!node_ptr));
                                         return node_ptr->GetUnknown() == unknown;
                                     });

        return it != end;
    }


    std::size_t NodeBearer::Ndof() const
    {
        const auto& node_list = GetNodeList();

        std::size_t ret = 0ul;

        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));
            ret += node_ptr->Ndof();
        }

        return ret;
    }


    std::size_t NodeBearer::Ndof(const NumberingSubset& numbering_subset) const
    {
        const auto& node_list = GetNodeList();

        std::size_t ret = 0ul;

        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));

            if (node_ptr->IsInNumberingSubset(numbering_subset))
                ret += node_ptr->Ndof();
        }

        return ret;
    }


    void NodeBearer::SetProgramWiseIndex(NodeBearerNS::program_wise_index_type index)
    {
        program_wise_index_ = index;
    }


    std::size_t NodeBearer::Nnode(const Unknown& unknown, const std::string& shape_function_label) const
    {
        const auto& node_list = GetNodeList();

        return static_cast<std::size_t>(std::count_if(node_list.cbegin(),
                                                      node_list.cend(),
                                                      [&unknown, &shape_function_label](const auto& node_ptr)
                                                      {
                                                          assert(!(!node_ptr));
                                                          const auto& node = *node_ptr;
                                                          return node.GetUnknown() == unknown
                                                                 && node.GetShapeFunctionLabel()
                                                                        == shape_function_label;
                                                      }));
    }


    bool NodeBearer::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) node_list = GetNodeList();

        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));

            if (node_ptr->IsInNumberingSubset(numbering_subset))
                return true;
        }

        return false;
    }


    void NodeBearer::SetGhost(std::size_t processor)
    {
        ghost_processor_list_.insert(processor);
    }


    bool NodeBearer::IsGhostOnProcessor(std::size_t processor) const
    {
        return ghost_processor_list_.find(processor) != ghost_processor_list_.cend();
    }


    const std::set<std::size_t>& NodeBearer::GetGhostProcessorList() const noexcept
    {
        return ghost_processor_list_;
    }


    bool NodeBearer::IsGhosted() const noexcept
    {
        return !(GetGhostProcessorList().empty());
    }


    Node::vector_shared_ptr NodeBearer::GetNodeList(const NumberingSubset& numbering_subset) const noexcept
    {
        decltype(auto) full_node_list = GetNodeList();

        Node::vector_shared_ptr ret;
        ret.reserve(full_node_list.size());

        std::copy_if(full_node_list.cbegin(),
                     full_node_list.cend(),
                     std::back_inserter(ret),
                     [&numbering_subset](const auto& node_ptr)
                     {
                         assert(!(!node_ptr));
                         return node_ptr->IsInNumberingSubset(numbering_subset);
                     });

        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
