/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Apr 2014 15:38:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_

// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/Node.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NodeBearer; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const Unknown& Node::GetUnknown() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return unknown_;
    }


    inline const Dof::vector_shared_ptr& Node::GetDofList() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return dof_list_;
    }


    inline const Dof::shared_ptr& Node::GetDofPtr(std::size_t i) const
    {
        assert(init_called_ && "Init() should be called!");
        const auto& dof_list = GetDofList();
        assert(i < dof_list.size());
        assert(!(!dof_list[i]));
        return dof_list[i];
    }


    inline const Dof& Node::GetDof(std::size_t i) const
    {
        assert(init_called_ && "Init() should be called!");
        return *GetDofPtr(i);
    }


    inline const std::string& Node::GetShapeFunctionLabel() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return shape_function_label_;
    }


    inline std::size_t Node::Ndof() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return dof_list_.size();
    }


    inline const NumberingSubset::vector_const_shared_ptr& Node::GetNumberingSubsetList() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return numbering_subset_list_;
    }


    inline std::shared_ptr<const NodeBearer> Node::GetNodeBearerFromWeakPtr() const
    {
        assert(init_called_ && "Init() should be called!");
        assert(!node_bearer_.expired());
        return node_bearer_.lock();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_HXX_
