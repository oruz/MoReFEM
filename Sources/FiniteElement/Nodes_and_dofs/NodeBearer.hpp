/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <set>
#include <type_traits>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp"   // IWYU pragma: export

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/Nodes_and_dofs/Node.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatrixPattern; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::NodeBearerNS
{

    //! Strong type for displacement vectors.
    // clang-format off
    using program_wise_index_type =
        StrongType
        <
            std::size_t,
            struct program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Hashable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::AsMpiDatatype,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::NodeBearerNS


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{

    //! Enum to list all the possible choices available to number the dofs.
    enum class DofNumberingScheme
    {

        contiguous_per_node //! < Means that all the dofs born by a same node share contiguous indexes.
                            //        contiguous_per_component
    };


    /*!
     *
     * \brief Returns the way dofs are numbered.
     *
     * \return the way dofs are numbered.
     *
     * DEV TMP #256 Put that in the input data file!
     */
    DofNumberingScheme CurrentDofNumberingScheme();


    /*!
     * \brief A NodeBearer is created whenever some dofs are located on a given geometric Interface.
     *
     * So for instance if in a mesh there are triangles P1:
     * - If we consider P1 finite elements, dofs are all located on vertices. So NodeBearer objects will be created
     * for each Vertex object that bears a dof.
     * - If we consider P2 finite elements, there are also dofs on edges. Therefore additional NodeBearer objects are
     * created for each Edge object that bears a dof.
     *
     * Of course, the same is true for Face or Volume interfaces.
     *
     * There is only one NodeBearer for a given interface; there is further classification within NodeBearer objects
     * (Dofs are grouped per Node).
     *
     */
    class NodeBearer final : public std::enable_shared_from_this<NodeBearer>
    {
      public:
        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<NodeBearer>;

        //! Vector of shared smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Shared smart pointer to a const value. This is used for instance when a \a Node creates such a pointer from
        //! a weak_ptr.
        using const_shared_ptr = std::shared_ptr<const NodeBearer>;

        //! Vector of shared smart pointers to a const value. This is used for instance when a \a Node creates such a
        //! pointer from a weak_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Friendship to give access to internal class.
        friend class Internal::FEltSpaceNS::MatrixPattern;

        //! Friendship to give access to internal class.
        friend class Internal::FEltSpaceNS::MatchInterfaceNodeBearer;

        //! Friendship to \a GodOfDof.
        friend class GodOfDof;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] interface Interface onto which the node is built.
         *
         * \internal <b><tt>[internal]</tt></b> index is given as a std::size_t as in practice it will be the size of
         * the list that will be passed to this constructor:
         * \code
         * node_list_.push_back(std::make_shared<Node>(interface, node_list_.size()));
         * \endcode
         * \endinternal
         *
         */
        explicit NodeBearer(const Interface::shared_ptr& interface);

        //! Destructor.
        ~NodeBearer() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NodeBearer(const NodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NodeBearer(NodeBearer&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NodeBearer& operator=(const NodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NodeBearer& operator=(NodeBearer&& rhs) = delete;

        ///@}

        //! Get index.
        ::MoReFEM::NodeBearerNS::program_wise_index_type GetProgramWiseIndex() const noexcept;

        /*!
         * \brief Returns the list of \a Node matching the given \a unknown and \a shape_function_label.
         *
         * \internal <b><tt>[internal]</tt></b> This method is expected to be used only in init phase, hence the brutal
         * return by value of a vector.
         * No restriction upon \a NumberingSubset in purpose; that's the reason \a ExtendedUnknown is not used.
         * A same \a Node might be defined upon several \a NumberingSubset.
         * \endinternal
         *
         * \param[in] unknown \a Unknown for which the list of \a Node is sought.
         * \param[in] shape_function_label Shape function label for which the list of \a Node is sought.
         *
         * \return List of \a Node related to \a unknown.
         *
         * \internal This method is only called in the initialization phase, and it should remain that way: it involves
         * allocating and fillind a std::vector on the fly.
         * \endinternal
         */
        Node::vector_shared_ptr GetNodeList(const Unknown& unknown, const std::string& shape_function_label) const;

        //! Set the processor to which the node bearer belongs to.
        //! \param[in] processor Processor that handles the current object.
        void SetProcessor(std::size_t processor);

        /*!
         * \brief Specify the \a NodeBearer is a ghost on \a processor.
         *
         * \param[in] processor The processor on which \a NodeBearer exists as a ghost.
         */
        void SetGhost(std::size_t processor);

        //! Get the processor to which the node bearer belongs to.
        std::size_t GetProcessor() const noexcept;

        //! Returns the nature of the node.
        InterfaceNS::Nature GetNature() const noexcept;

        //! Returns the number of dofs born by the node.
        std::size_t Ndof() const;

        //! Returns the number of dofs born by the node inside the \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset used as filter.
        std::size_t Ndof(const NumberingSubset& numbering_subset) const;

        //! Whether an unknown is present or not.
        //! \param[in] unknown \a Unknown under test.
        bool IsUnknown(const Unknown& unknown) const;

        //! Returns the interface on which the node is located.
        const Interface& GetInterface() const noexcept;

        //! Set the index.
        //! \param[in] index Index of the \a NodeBearer, computed outside the class.
        void SetProgramWiseIndex(NodeBearerNS::program_wise_index_type index);

        //! Number of Nodes for a couple Unknown/Shape function label.
        //! \param[in] unknown \a Unknown used as filter.
        //! \param[in] shape_function_label String representing the shape function used as filter.
        std::size_t Nnode(const Unknown& unknown, const std::string& shape_function_label) const;

        //! Whether there are nodes in the node bearer.
        bool IsEmpty() const noexcept;

        //! Return the list of nodes.
        const Node::vector_shared_ptr& GetNodeList() const noexcept;

        /*!
         * \brief Return the list of nodes that match the given \a NumberingSubset.
         *
         * \param[in] numbering_subset Only consider \a Node that are in this \a NumberingSubset.
         *
         * \return The list of \a Node after filtering.
         *
         * \attention This method is not very efficient as it entails a memory allocation - should be performed only during initialization phase.
         */
        Node::vector_shared_ptr GetNodeList(const NumberingSubset& numbering_subset) const noexcept;

        /*!
         * \brief Whether the \a NodeBearer is a ghost or not.
         *
         * \attention This method is intended to be called ONLY once the partitioning is fully done (but if you're not a
         * developer of the library you shouldn't bother as the partitioning occurs during \a Model::Initialize()  or \a
         * Model::Run() calls which should be one of your very first line of code in the main).
         *
         * This is actually a simple test upon the processor: if processor is same as rank it is not a ghost.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \return True if the \a NodeBearer is a ghost of a \a NodeBearer handled by another mpi rank.
         */
        bool IsGhost(const Wrappers::Mpi& mpi) const noexcept;

        /*!
         * \brief Whether a given \a processor is using the \a NodeBearer as a ghost.
         *
         * Beware: current method returns 'false' if the given \a processor is the current one - the point is just to
         * single out ghost.
         *
         * \param[in] processor Processor under investigation.
         *
         * \return True if current \a NodeBearer is a ghost of indicated \a processor.
         */
        bool IsGhostOnProcessor(std::size_t processor) const;

        /*!
         * \brief Returns whether the \a NodeBearer is in the \a numbering_subset.
         *
         * \param[in] numbering_subset \a NumberingSubset used as filter.
         *
         * \return True if one of its \a Node is in \a NumberingSubset.
         */
        bool IsInNumberingSubset(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Returns the list of processors that store the \a NodeBearer as a ghost.
         *
         * \return The list of processors.
         */
        const std::set<std::size_t>& GetGhostProcessorList() const noexcept;

        /*!
         * \brief Whether there are other processors that ghost the \a NodeBearer.
         *
         * \return True if there is at least one.
         */
        bool IsGhosted() const noexcept;


      private:
        /*!
         * \brief Add a new node.
         *
         * \internal <b><tt>[internal]</tt></b> It is assumed here the node does not exist yet; such liabilities are
         * handled by the class in charge of actually calling such methods (\a MatchInterfaceNodeBearer).
         * \endinternal
         *
         * \param[in] extended_unknown Couple \a Unknown / \a Numbering subset for which the node is/are created.
         * \param[in] Ndof Number of dofs to create on the node.
         *
         * \return Node newly created.
         */
        Node::shared_ptr AddNode(const ExtendedUnknown& extended_unknown, std::size_t Ndof);


      private:
        //! An internal index that is useful during partitioning steps.
        ::MoReFEM::NodeBearerNS::program_wise_index_type program_wise_index_ =
            NumericNS::UninitializedIndex<NodeBearerNS::program_wise_index_type>();

        /*!
         * \brief Processor that holds the node.
         *
         * Once data have been reduced to processor wise, this data might still be useful: ghost nodes
         * keep existing!
         */
        std::size_t processor_ = NumericNS::UninitializedIndex<std::size_t>();

        /*!
         * \brief Processors that include as ghost the current \a NodeBearer.
         *
         */
        std::set<std::size_t> ghost_processor_list_;

        //! List of nodes.
        Node::vector_shared_ptr node_list_;

        //! Interface that bears the node.
        Interface::shared_ptr interface_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criterion: the underlying index (returned by GetProgramWiseIndex()).
    bool operator<(const NodeBearer& lhs, const NodeBearer& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criterion: the underlying index (returned by GetProgramWiseIndex()).
    bool operator==(const NodeBearer& lhs, const NodeBearer& rhs);


    ///@} // \addtogroup


} // namespace MoReFEM


namespace std
{


    //! Provide hash function for NodeBearer::shared_ptr.
    template<>
    struct hash<MoReFEM::NodeBearer::shared_ptr>
    {
      public:
        //! Overload of std::has for NodeBearer::shared_ptr
        //! \param[in] ptr Shared pointer to a \a NodeBearer object.
        std::size_t operator()(const MoReFEM::NodeBearer::shared_ptr& ptr) const
        {
            assert(!(!ptr));
            return std::hash<MoReFEM::NodeBearerNS::program_wise_index_type>()(ptr->GetProgramWiseIndex());
        }
    };


} // namespace std


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/Nodes_and_dofs/NodeBearer.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HPP_
