/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 18 Sep 2013 11:05:16 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HPP_


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"   // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Node; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Class in charge of dof informations.
     *
     * This class mostly sports the different numbering associated to a given dof:
     * - An internal unique id.
     * - The program-wise index for each numbering subset to which the Dof belongs to.
     * - The processor-wise or ghost index for each numbering subset to which the Dof belongs to.
     * - Yet another one processor-wise index, used for internal purposes, regardless of numbering subset.
     *
     * Dof objects are expected to be stored within \a Node objects, but they also appears directly in internals of
     * \a GodOfDof.
     */
    class Dof final : public Crtp::UniqueId<Dof>
    {

      public:
        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<Dof>;

        //! Vector of shared smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] node_ptr Pointer to the \a Node onto which the dof will be created.
         */
        explicit Dof(const std::shared_ptr<const Node>& node_ptr);

        //! Destructor.
        ~Dof();

        //! \copydoc doxygen_hide_copy_constructor
        Dof(const Dof& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Dof(Dof&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Dof& operator=(const Dof& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Dof& operator=(Dof&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Processor-wise or ghost index.
         *
         * This index tells where the dof is stored locally in the vector. The processor-wise indexes comes first;
         * then all the ghost are grouped together. It is FEltSpace class that is aware where the ghost
         * begins; current class is oblivious whether it is a ghost or a processor-wise value.
         *
         * This index should typically be used when an element must be fetched in a parallel Petsc::Vector and
         * a Petsc::AccessGhostContent object is used to do so.
         *
         * Nonetheless, beware: function Petsc::Vector::GetValues() and Petsc::Vector::SetValues() expects a program-
         * wise index rather than a processor-wise one.
         *
         * \param[in] numbering_subset Numbering subset considered.
         * \return Processor-wise or ghost index of the dof in the given \a numbering_subset.
         *
         */
        std::size_t GetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Program-wise index.
         *
         * This index is useful when you deal with a parallel matrix: for instance if you need to zero a row
         * for a boundary condition, Petsc expects a program-wise index to do so.
         *
         * \param[in] numbering_subset Numbering subset considered.
         * \return Program-wise index of the dof in the given \a numbering_subset.
         */
        std::size_t GetProgramWiseIndex(const NumberingSubset& numbering_subset) const;


        /*!
         * \brief Processor- or ghost-wise index independant of the numbering subset.
         *
         * A same dof may be present or not in a given numbering subset, and there is no guaranteed special
         * numbering subset that encompass all the dofs of a given GodOfDof.
         *
         * Hence this index which covers all the dofs on a same processor, regardless of their numbering subset.
         *
         * \return Processor- or ghost-wise index independant of the numbering subset.
         */
        std::size_t GetInternalProcessorWiseOrGhostIndex() const noexcept;


        /*!
         * \brief Whether the current dof is present in \a numbering_subset.
         *
         * \internal <b><tt>[internal]</tt></b> Such a check is made by looking into
         * program_wise_index_per_numbering_subset_: the processor-wise counterpart might have been left empty if not
         * required by the model.
         * \endinternal
         *
         * \param[in] numbering_subset Numbering subset considered.
         * \return True if the dof belong to \a numbering_subset.
         */
        bool IsInNumberingSubset(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Return a pointer to the \a Node to which the current dof belongs to.
         *
         * \internal <b><tt>[internal]</tt></b> No reference on purpose here: node_ is stored as a weak_ptr not
         * to introduce circular dependancy.
         * \endinternal
         *
         * \return Shared pointer to the enclosing \a Node.
         */
        std::shared_ptr<const Node> GetNodeFromWeakPtr() const;


      public: // TMP #1571!
        //! Set the program-wise index for the given \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset for which program-wise index is set.
        //! \param[in] program_wise_index Value of the program-wise index, computed outside the class.
        void SetProgramWiseIndex(const NumberingSubset& numbering_subset, std::size_t program_wise_index);

        //! Set the processor-wise index for the given \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset for which processor-wise or ghost index is set.
        //! \param[in] index Value of the index, computed outside the class.
        void SetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset, std::size_t index);

        //! Set a processor-wise index regardless of the numbering subset.
        //! \param[in] index Index to set, computed outside the class.
        void SetInternalProcessorWiseOrGhostIndex(std::size_t index);


        //! Convenient alias.
        using index_per_numbering_subset_type = std::vector<std::pair<std::size_t, std::size_t>>;

        /*!
         * \brief List of processor-wise or ghost index per numbering subset index.
         */
        const index_per_numbering_subset_type& GetProcessorWiseOrGhostIndexPerNumberingSubset() const;

        /*!
         * \brief List of program-wise index per numbering subset index.
         */
        const index_per_numbering_subset_type& GetProgramWiseIndexPerNumberingSubset() const;


      private:
        /*!
         * \brief Processor- or ghost-wise index independant of the numbering subset.
         *
         * A same dof may be present or not in a given numbering subset, and there is no guaranteed there is a special
         * numbering subset that encompass all the dofs of a given GodOfDof.
         *
         * Hence this index which covers all the dofs on a same processor, regardless of their numbering subset.
         */
        std::size_t internal_processor_wise_or_ghost_index_ = NumericNS::UninitializedIndex<std::size_t>();


        /*!
         * \brief Processor-wise or ghost index.
         *
         * This index tells where the dof is stored locally in the vector. The processor-wise indexes comes first;
         * then all the ghost are grouped together. It is FEltSpace class that is aware where the ghost
         * begins; current class is oblivious whether it is a ghost or a processor-wise value.
         *
         */
        index_per_numbering_subset_type processor_wise_or_ghost_index_per_numbering_subset_;

        /*!
         * \brief Program-wise index.
         *
         * This index is the one that is assigned after the mesh has been partitionned; so it is devised so that
         * all dofs on a same processor are contiguous. We don't care about the original mesh that was assigned to
         * begin with (except in sequential, because in this case they are the same!).
         *
         *
         */
        index_per_numbering_subset_type program_wise_index_per_numbering_subset_;


#ifndef NDEBUG
      public:
        /*!
         * \brief List of the unique ids of all numbering subsets that cover this dof.
         *
         * \internal <b><tt>[internal]</tt></b> It is only intended for dev/debug purposes, hence the NDEBUG and the
         * very unusual public status.
         * \endinternal
         */
        std::vector<std::size_t> numbering_subset_index_list_;
#endif // NDEBUG

        //! Weak pointer to NodeBearer.
        std::weak_ptr<const Node> node_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criterion: the underlying index (returned by GetIndex()).
    bool operator<(const Dof& lhs, const Dof& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criterion: the underlying index (returned by GetIndex()).
    bool operator==(const Dof& lhs, const Dof& rhs);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


namespace std
{


    /*!
     * \brief Provide hash function for Dof::shared_ptr.
     *
     * \attention This hash function assumes the dof lists have already been reduced to processor-wise and that the
     * processor-wise indexes have been properly computed. Both conditions are true one GodOfDof::Init() has properly
     * been called.
     */
    template<>
    struct hash<MoReFEM::Dof::shared_ptr>
    {
      public:
        //! Overload of std::has for Dof::shared_ptr
        //! \param[in] ptr Shared pointer to a \a Dof object.
        std::size_t operator()(const MoReFEM::Dof::shared_ptr& ptr) const
        {
            assert(!(!ptr));
            return std::hash<std::size_t>()(ptr->GetInternalProcessorWiseOrGhostIndex());
        }
    };


} // namespace std


#include "FiniteElement/Nodes_and_dofs/Dof.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_HPP_
