/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Sep 2013 15:14:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cstddef> // IWYU pragma: keep
#include <map>
#include <sstream>
#include <type_traits>
#include <vector>

#include "FiniteElement/Nodes_and_dofs/Exceptions/Dof.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"


namespace // anonymous
{


    std::string InvalidNumberOfUnknownMsg(std::size_t Nin_file, std::size_t Nexpected);

    std::string DuplicatedUnknownInInputFileMsg(const std::string& duplicated_unknown);

    std::string InconsistentUnknownListMsg(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                           const std::vector<std::string>& tuple_unknown_list);


} // namespace


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Dof
        {


            Exception::~Exception() = default;


            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }


            InvalidNumberOfUnknown::~InvalidNumberOfUnknown() = default;


            InvalidNumberOfUnknown::InvalidNumberOfUnknown(std::size_t Nin_file,
                                                           std::size_t Nexpected,
                                                           const char* invoking_file,
                                                           int invoking_line)
            : Exception(InvalidNumberOfUnknownMsg(Nin_file, Nexpected), invoking_file, invoking_line)
            { }


            DuplicatedUnknownInInputFile::~DuplicatedUnknownInInputFile() = default;


            DuplicatedUnknownInInputFile::DuplicatedUnknownInInputFile(const std::string& duplicated_unknown,
                                                                       const char* invoking_file,
                                                                       int invoking_line)
            : Exception(DuplicatedUnknownInInputFileMsg(std::move(duplicated_unknown)), invoking_file, invoking_line)
            { }


            InconsistentUnknownList::~InconsistentUnknownList() = default;


            InconsistentUnknownList::InconsistentUnknownList(
                const std::map<std::string, std::size_t>& input_file_unknown_list,
                const std::vector<std::string>& tuple_unknown_list,
                const char* invoking_file,
                int invoking_line)
            : Exception(InconsistentUnknownListMsg(input_file_unknown_list, tuple_unknown_list),
                        invoking_file,
                        invoking_line)
            { }


        } // namespace Dof


    } // namespace ExceptionNS


} // namespace MoReFEM


namespace // anonymous
{


    std::string InvalidNumberOfUnknownMsg(std::size_t Nin_file, std::size_t Nexpected)
    {
        std::ostringstream oconv;
        oconv << Nexpected << " unknowns were expected in the input parameter list, but " << Nin_file
              << " were actually read in the file.";

        return oconv.str();
    }


    std::string DuplicatedUnknownInInputFileMsg(const std::string& duplicated_unknown)
    {
        std::ostringstream oconv;
        oconv << "Unknown '" << duplicated_unknown << "' was present at least twice in the input data file.";
        return oconv.str();
    }


    std::string InconsistentUnknownListMsg(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                           const std::vector<std::string>& tuple_unknown_list)
    {
        std::ostringstream oconv;
        oconv << "The unknown found int the input file are not the ones expected by the problem: \n";
        MoReFEM::Utilities::PrintContainer<>::Do(tuple_unknown_list, oconv);
        oconv << " were expected but those that were in the input file were: \n";
        MoReFEM::Utilities::PrintContainer<MoReFEM::Utilities::PrintPolicyNS::Key>::Do(input_file_unknown_list, oconv);
        return oconv.str();
    }


} // namespace


/// @} // addtogroup FiniteElementGroup
