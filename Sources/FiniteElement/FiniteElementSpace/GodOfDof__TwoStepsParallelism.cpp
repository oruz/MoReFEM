/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <vector>
// IWYU pragma: no_include <__tree>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {

        /*!
         * \brief Helper class which stores SOME of the content of the GodOfDof prepartitioned file.
         *
         * Some date from this file are extracted directly by the functions that need them (for instance which key
         * depends directly from a numbering subset unique id).
         */
        struct PrepartitionedDataFileContent
        {
            PrepartitionedDataFileContent(LuaOptionFile& god_of_dof_prepartitioned_data,
                                          std::size_t rank,
                                          std::size_t Nprocessor);

            //! Program-wise index of the first \a NodeBearer on the current rank.
            std::size_t node_bearer_first_program_wise_index{};

            //! The \a NodeBearer list as indicated in the partition file: first letter stands for the interface, and
            //! related integer is the program-wise index for that interface. This list encompasses the \a NodeBearer on
            //! the local processor with the exception of the ghost ones.
            std::vector<std::string> interface_per_node_bearer;

            //! Same as \a interface_per_node_bearer for the ghost ones.
            std::vector<std::string> interface_per_ghost_node_bearer;

            //! List of program-wise indexes of ghost \a NodeBearer (these aren't continous
            //! contrary to the ones for processor-wise, for which just \a first_program_wise_index is enough)
            std::vector<NodeBearerNS::program_wise_index_type> ghost_node_bearer_index_list;

            //! Mpi rank for each ghost \a NodeBearer
            std::vector<std::size_t> ghost_node_bearer_mpi_rank_list;

            /*!
             * \brief Keep track of all the processors that ghost a given \a NodeBearer
             *
             * Key is the program-wise index of processor-wise \a NodeBearer, value is the list of all the processors
             * that ghost it.
             */
            std::unordered_map<NodeBearerNS::program_wise_index_type, std::set<std::size_t>>
                ghost_processor_per_node_bearer;
        };


        std::string MatchExceptionErrorMsg(std::string_view filename, std::string_view node_bearer_shorthand);


        struct MatchException : public Exception
        {


            /*!
             * \brief Constructor with simple message.
             *
             * \param[in] msg Message.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            explicit MatchException(std::string_view filename,
                                    std::string_view node_bearer_shorthand,
                                    const char* invoking_file,
                                    int invoking_line);

            //! Destructor
            virtual ~MatchException() override;

            //! \copydoc doxygen_hide_copy_constructor
            MatchException(const MatchException& rhs) = default;
        };


        struct FetchResult
        {

            FetchResult(NodeBearerNS::program_wise_index_type a_node_bearer_index, bool a_is_ghost)
            : node_bearer_index(a_node_bearer_index), is_ghost(a_is_ghost)
            { }

            const NodeBearerNS::program_wise_index_type node_bearer_index;

            const bool is_ghost;
        };


        //! Class which purpose is to help reassign the original \a NodeBearer program-wise index, using to do so the
        //! underlying interface index.
        class Match
        {
          public:
            //! Constructor.
            //! \param[in] filename Path to the Lua option file which contains the god of dof prepartitioned data.
            Match(std::string_view filename, const PrepartitionedDataFileContent& prepartitioned_data_file_content);

            /*!
             * \brief Fetch the \a NodeBearer program-wise index that should be associated to the given \a Interface.
             *
             * \return A \a FetchResult object.
             */
            FetchResult Fetch(const Interface& interface) const;

          private:
            //! Do the treatment over processor-wise \a NodeBearer list.
            void ProcessProcessorWise(const std::vector<std::string>& selected_node_bearer_list);

            //! Do the treatment over ghost \a NodeBearer list.
            void ProcessGhost(const std::vector<std::string>& selected_node_bearer_list,
                              const std::vector<NodeBearerNS::program_wise_index_type>& ghost_node_bearer_index_list);

            //! Return the proper list of interface depending on the chosen nature.
            const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                     NodeBearerNS::program_wise_index_type>&
            GetInterfaceMatch(InterfaceNS::Nature nature) const;

            //! 'Convert' a shorthand into a interface nature and its associated index.
            void ConvertShortHand(const std::string& shorthand,
                                  char& interface_type,
                                  ::MoReFEM::InterfaceNS::program_wise_index_type& interface_index) const;

            //! Whether the \a NodeBearer is a ghost or not.
            //! The way to identify this is the index:
            //! If it is in [first_program_wise_index_,  first_program_wise_index_ + Nprocessor_wise_node_bearer_[, it
            //! is a processor-wise If not it is a ghost.
            bool IsGhost(NodeBearerNS::program_wise_index_type node_bearer_index) const noexcept;

          private:
            //! Program-wise index for the first processor-wise \a NodeBearer (by construct their index is incremented
            //! and therefore all are contiguous).
            const NodeBearerNS::program_wise_index_type first_program_wise_index_on_processor_;

            //! Number of processor-wise \a NodeBearer.
            const std::size_t Nprocessor_wise_node_bearer_;

            //! Filename of the LuaOptionFile.
            std::string_view filename_;

            //! Key: the program-wise index of the \a Vertex.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                coords_matching_;

            //! Key: the program-wise index of the \a Edge.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                edge_matching_;

            //! Key: the program-wise index of the \a Face.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                face_matching_;

            //! Key: the program-wise index of the \a Volume.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                volume_matching_;
        };


        /*!
         * \brief Split the \a NodeBearers read into processor-wise and ghost ones and renumber them to  match original
         * numbering.
         *
         * \param[in] node_bearer_before_processing The \a NodeBearer list reconstructed from partitioned data before
         * renumbering - this list contains both processor-wise and ghost ones. \param[out]
         * processor_wise_node_bearer_list The \a NodeBearer handled directly by current rank, sort the same way as in
         * original partition. \param[out] ghost_node_bearer_list Same for ghosts.
         */
        void ProcessNodeBearer(const Wrappers::Mpi& mpi,
                               const NodeBearer::vector_shared_ptr& node_bearer_before_processing,
                               const PrepartitionedDataFileContent& prepartitioned_data_file_content,
                               const Match& match,
                               NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            assert(processor_wise_node_bearer_list.empty());
            assert(ghost_node_bearer_list.empty());

            const auto& interface_per_node_bearer = prepartitioned_data_file_content.interface_per_node_bearer;
            const auto& ghost_node_bearer_index_list = prepartitioned_data_file_content.ghost_node_bearer_index_list;
            const auto& ghost_node_bearer_mpi_rank_list =
                prepartitioned_data_file_content.ghost_node_bearer_mpi_rank_list;

            const auto& ghost_processor_per_node_bearer =
                prepartitioned_data_file_content.ghost_processor_per_node_bearer;

            const auto Nprocessor_wise_node_bearer =
                static_cast<NodeBearer::vector_shared_ptr::difference_type>(interface_per_node_bearer.size());

            assert(node_bearer_before_processing.size() >= interface_per_node_bearer.size());
            const auto Nghost_node_bearer = ghost_node_bearer_index_list.size();

            assert(Nghost_node_bearer == node_bearer_before_processing.size() - interface_per_node_bearer.size());

            // reserve/resize here is not a mistake: we do not proceed the same way for each of them.
            processor_wise_node_bearer_list.reserve(static_cast<std::size_t>(Nprocessor_wise_node_bearer));

            ghost_node_bearer_list.resize(Nghost_node_bearer, nullptr);

            const auto mpi_rank = mpi.GetRank<std::size_t>();

            for (auto& node_bearer_ptr : node_bearer_before_processing)
            {
                assert(!(!node_bearer_ptr));
                auto& node_bearer = *node_bearer_ptr;

                decltype(auto) interface = node_bearer.GetInterface();

                const auto result = match.Fetch(interface);
                node_bearer.SetProgramWiseIndex(result.node_bearer_index);

                if (result.is_ghost)
                {
                    // Put the \a NodeBearer in the exact same position it was in the original run.
                    auto it = std::find(ghost_node_bearer_index_list.cbegin(),
                                        ghost_node_bearer_index_list.cend(),
                                        result.node_bearer_index);

                    assert(it != ghost_node_bearer_index_list.cend());
                    const auto position = static_cast<std::size_t>(it - ghost_node_bearer_index_list.cbegin());
                    assert(ghost_node_bearer_list[position] == nullptr);
                    ghost_node_bearer_list[position] = node_bearer_ptr;
                    node_bearer.SetProcessor(ghost_node_bearer_mpi_rank_list[position]);
                } else
                {
                    node_bearer.SetProcessor(mpi_rank);
                    processor_wise_node_bearer_list.push_back(node_bearer_ptr);

                    auto it = ghost_processor_per_node_bearer.find(node_bearer.GetProgramWiseIndex());

                    if (it != ghost_processor_per_node_bearer.cend())
                    {
                        const auto& ghost_processor_list = it->second;

                        for (const auto processor : ghost_processor_list)
                            node_bearer.SetGhost(processor);
                    }
                }
            }

            std::sort(processor_wise_node_bearer_list.begin(),
                      processor_wise_node_bearer_list.end(),
                      [](const auto& lhs_ptr, const auto& rhs_ptr)
                      {
                          assert(!(!lhs_ptr));
                          assert(!(!rhs_ptr));
                          return lhs_ptr->GetProgramWiseIndex() < rhs_ptr->GetProgramWiseIndex();
                      });

            assert(processor_wise_node_bearer_list.size() == interface_per_node_bearer.size());

            assert(std::none_of(ghost_node_bearer_list.cbegin(),
                                ghost_node_bearer_list.cend(),
                                Utilities::IsNullptr<NodeBearer::shared_ptr>));
        }


    } // namespace


    void GodOfDof::InitFromPreprocessedDataHelper(LuaOptionFile& god_of_dof_prepartitioned_data)
    {
        auto match_interface_node_bearer = CreateNodeBearers();
        SetUpNodeBearersFromPrepartitionedData(god_of_dof_prepartitioned_data);
        CreateNodesAndDofs(match_interface_node_bearer);
    }


    void GodOfDof::SetUpNodeBearersFromPrepartitionedData(LuaOptionFile& god_of_dof_prepartitioned_data)
    {
        PrepartitionedDataFileContent prepartitioned_data_file_content(
            god_of_dof_prepartitioned_data, GetMpi().GetRank<std::size_t>(), GetMpi().Nprocessor<std::size_t>());

        Match match(god_of_dof_prepartitioned_data.GetFilename(), prepartitioned_data_file_content);

        {
            NodeBearer::vector_shared_ptr processor_wise_node_bearer_list, ghost_node_bearer_list;

            decltype(auto) unsort_node_bearer_list = GetProcessorWiseNodeBearerList();
            // < When the function is called this accessor does not contain yet what is on the tag:
            // < ghost are still there.

            ProcessNodeBearer(GetMpi(),
                              unsort_node_bearer_list,
                              prepartitioned_data_file_content,
                              match,
                              processor_wise_node_bearer_list,
                              ghost_node_bearer_list);

            GetNonCstProcessorWiseNodeBearerList().swap(processor_wise_node_bearer_list);
            GetNonCstGhostNodeBearerList().swap(ghost_node_bearer_list);
        }
    }


    namespace // anonymous
    {


        PrepartitionedDataFileContent::PrepartitionedDataFileContent(LuaOptionFile& god_of_dof_prepartitioned_data,
                                                                     const std::size_t rank,
                                                                     const std::size_t Nprocessor)
        {
            assert(rank < Nprocessor);

            god_of_dof_prepartitioned_data.Read(
                "node_bearer_first_program_wise_index", "", node_bearer_first_program_wise_index, __FILE__, __LINE__);
            god_of_dof_prepartitioned_data.Read(
                "interface_per_node_bearer", "", interface_per_node_bearer, __FILE__, __LINE__);
            god_of_dof_prepartitioned_data.Read(
                "interface_per_ghost_node_bearer", "", interface_per_ghost_node_bearer, __FILE__, __LINE__);
            god_of_dof_prepartitioned_data.Read(
                "ghost_node_bearer_indexes", "", ghost_node_bearer_index_list, __FILE__, __LINE__);

            god_of_dof_prepartitioned_data.Read(
                "ghost_node_bearer_mpi_rank", "", ghost_node_bearer_mpi_rank_list, __FILE__, __LINE__);

            {
                ghost_processor_per_node_bearer.max_load_factor(Utilities::DefaultMaxLoadFactor());

                for (auto proc = 0ul; proc < Nprocessor; ++proc)
                {
                    if (rank == proc)
                        continue;

                    std::ostringstream oconv;
                    oconv << "ghosted_processor_wise_node_bearer_on_processor_" << proc;

                    std::vector<NodeBearerNS::program_wise_index_type> ghosted_processor_wise_node_bearer_for_processor;

                    god_of_dof_prepartitioned_data.Read(
                        oconv.str(), "", ghosted_processor_wise_node_bearer_for_processor, __FILE__, __LINE__);


                    for (const auto& node_bearer_index : ghosted_processor_wise_node_bearer_for_processor)
                        ghost_processor_per_node_bearer[node_bearer_index].insert(proc);
                }
            }


            if (interface_per_ghost_node_bearer.size() != ghost_node_bearer_index_list.size())
            {
                std::ostringstream oconv;
                oconv << "Invalid prepartitioned file (" << god_of_dof_prepartitioned_data.GetFilename()
                      << "): the "
                         "number of items in interface_per_ghost_node_bearer is not the same as the one in "
                         "ghost_node_bearer_indexes"
                         " (respectively "
                      << interface_per_ghost_node_bearer.size() << " and " << ghost_node_bearer_index_list.size()
                      << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            if (interface_per_ghost_node_bearer.size() != ghost_node_bearer_mpi_rank_list.size())
            {
                std::ostringstream oconv;
                oconv << "Invalid prepartitioned file (" << god_of_dof_prepartitioned_data.GetFilename()
                      << "): the "
                         "number of items in interface_per_ghost_node_bearer is not the same as the one in "
                         "ghost_node_bearer_mpi_rank_list (respectively "
                      << interface_per_ghost_node_bearer.size() << " and " << ghost_node_bearer_mpi_rank_list.size()
                      << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        }


        Match::Match(std::string_view filename, const PrepartitionedDataFileContent& prepartitioned_data_file_content)
        : first_program_wise_index_on_processor_(prepartitioned_data_file_content.node_bearer_first_program_wise_index),
          Nprocessor_wise_node_bearer_(prepartitioned_data_file_content.interface_per_node_bearer.size()),
          filename_(filename)
        {
            coords_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            edge_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            face_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            volume_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());

            ProcessProcessorWise(prepartitioned_data_file_content.interface_per_node_bearer);
            ProcessGhost(prepartitioned_data_file_content.interface_per_ghost_node_bearer,
                         prepartitioned_data_file_content.ghost_node_bearer_index_list);
        }


        void
        Match::ConvertShortHand(const std::string& shorthand,
                                char& interface_type,
                                ::MoReFEM::InterfaceNS::program_wise_index_type& interface_program_wise_index) const
        {
            if (shorthand.empty())
                throw MatchException(filename_, shorthand, __FILE__, __LINE__);

            std::istringstream iconv(shorthand);

            iconv >> interface_type;

            std::size_t tmp;
            iconv >> tmp;

            if (!iconv)
                throw MatchException(filename_, shorthand, __FILE__, __LINE__);

            interface_program_wise_index = static_cast<::MoReFEM::InterfaceNS::program_wise_index_type>(tmp);
        }


        void Match::ProcessProcessorWise(const std::vector<std::string>& node_bearer_list)
        {
            auto current_node_bearer_program_wise_index{ first_program_wise_index_on_processor_ };

            for (const auto& node_bearer_shorthand : node_bearer_list)
            {
                char interface_type{};
                ::MoReFEM::InterfaceNS::program_wise_index_type interface_program_wise_index{ 0ul };
                ConvertShortHand(node_bearer_shorthand, interface_type, interface_program_wise_index);

                switch (interface_type)
                {
                case 'V':
                    coords_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'E':
                    edge_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'F':
                    face_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'G':
                    volume_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                default:
                    throw MatchException(filename_, node_bearer_shorthand, __FILE__, __LINE__);
                }

                ++current_node_bearer_program_wise_index;
            }
        }


        void Match::ProcessGhost(const std::vector<std::string>& ghost_node_bearer_list,
                                 const std::vector<NodeBearerNS::program_wise_index_type>& ghost_node_bearer_index_list)
        {
            assert(ghost_node_bearer_list.size() == ghost_node_bearer_index_list.size()
                   && "Should never happen: already tested by an exception beforehand");

            auto it = ghost_node_bearer_index_list.cbegin();

            for (const auto& node_bearer_shorthand : ghost_node_bearer_list)
            {
                char interface_type{};
                ::MoReFEM::InterfaceNS::program_wise_index_type interface_program_wise_index{};
                ConvertShortHand(node_bearer_shorthand, interface_type, interface_program_wise_index);

                const auto current_node_bearer_program_wise_index = *it;

                switch (interface_type)
                {
                case 'V':
                    coords_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'E':
                    edge_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'F':
                    face_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'G':
                    volume_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                default:
                    throw MatchException(filename_, node_bearer_shorthand, __FILE__, __LINE__);
                }
                ++it;
            }

            assert(it == ghost_node_bearer_index_list.cend());
        }


        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                 NodeBearerNS::program_wise_index_type>&
        Match::GetInterfaceMatch(InterfaceNS::Nature nature) const
        {
            switch (nature)
            {
            case InterfaceNS::Nature::vertex:
                return coords_matching_;
            case InterfaceNS::Nature::edge:
                return edge_matching_;
            case InterfaceNS::Nature::face:
                return face_matching_;
            case InterfaceNS::Nature::volume:
                return volume_matching_;
            case InterfaceNS::Nature::none:
            case InterfaceNS::Nature::undefined:
                assert(false);
                exit(EXIT_FAILURE);
            }

            // Stupid line to please gcc...
            return volume_matching_;
        }


        bool Match::IsGhost(NodeBearerNS::program_wise_index_type node_bearer_index) const noexcept
        {
            if (node_bearer_index < first_program_wise_index_on_processor_)
                return true;

            if (node_bearer_index >= first_program_wise_index_on_processor_
                                         + NodeBearerNS::program_wise_index_type(Nprocessor_wise_node_bearer_))
                return true;

            return false;
        }


        FetchResult Match::Fetch(const Interface& interface) const
        {
            decltype(auto) interface_list = GetInterfaceMatch(interface.GetNature());

            const auto it = interface_list.find(interface.GetProgramWiseIndex());

            if (it == interface_list.cend())
                throw Exception("Unable to match the recreated NodeBearer to one of the pre-existing one before "
                                "partitioning",
                                __FILE__,
                                __LINE__);

            FetchResult result(it->second, IsGhost(it->second));
            return result;
        }


        MatchException::~MatchException() = default;


        MatchException::MatchException(std::string_view filename,
                                       std::string_view node_bearer_shorthand,
                                       const char* invoking_file,
                                       int invoking_line)
        : Exception(MatchExceptionErrorMsg(filename, node_bearer_shorthand), invoking_file, invoking_line)
        { }


        std::string MatchExceptionErrorMsg(std::string_view filename, std::string_view node_bearer_shorthand)
        {
            std::ostringstream oconv;
            oconv << "Invalid node bearer shorthand in " << filename << ": '" << node_bearer_shorthand
                  << "' does not follow the expected format (which is a letter among { V, E, F, G } followed "
                     "by an integer.)";
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
