### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractMpi.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractMpi.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/CoordsMatching/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FEltSpace/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Output/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Partition/SourceList.cmake)
