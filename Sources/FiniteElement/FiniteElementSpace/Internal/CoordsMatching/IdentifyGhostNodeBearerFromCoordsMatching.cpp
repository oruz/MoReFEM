//! \file
//
//
//  IdentifyGhostNodeBearerFromCoordsMatching.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 20/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/IdentifyGhostNodeBearerFromCoordsMatching.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


    namespace // anonymous
    {


        /*!
         * \brief Extract the \a NodeBearer that are NOT processor-wise.
         *
         * \a  GodOfDof::IdentifyGhostNodeBearerFromCoordsMatching is meant to be called:
         * - before the reduction of \a NodeBearer list
         * - but after the processor for each \a NodeBearer has been assigned
         * (to put in a nutshell, the repartition has been decided but not yet applied at the list level).
         *
         * I want to identify in GodOfDof::IdentifyGhostNodeBearerFromCoordsMatching() the \a NodeBearer that are
         * required to set up \a FromCoordsMatching but that would have been missed otherwise; only ones that are not on
         * processor--wise may fit this bill, hence this function to filter out the processor-wise ones.
         */
        NodeBearer::vector_shared_ptr
        ExtractNonProcessorWiseNodeBearer(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                                          std::size_t mpi_rank);


    } // namespace


    Internal::NodeBearerNS::node_bearer_per_coords_index_list_type
    IdentifyGhostNodeBearerFromCoordsMatching(const GodOfDof& god_of_dof,
                                              const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching& vertex_matching)
    {
        Internal::NodeBearerNS::node_bearer_per_coords_index_list_type ret;

        if (god_of_dof.GetUniqueId() == vertex_matching.GetSourceMeshId())
        {
            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) target_god_of_dof = god_of_dof_manager.GetGodOfDof(vertex_matching.GetTargetMeshId());

            assert(target_god_of_dof.HasInitBeenCalled()
                   && "It is assumed target one has been built "
                      "first (in InitEachGodOfDof())");

            decltype(auto) target_processor_wise_node_bearer_list = target_god_of_dof.GetProcessorWiseNodeBearerList();

            const auto mpi_rank = god_of_dof.GetMpi().GetRank<std::size_t>();

            NodeBearer::vector_shared_ptr non_processor_wise_non_bearer_list =
                ExtractNonProcessorWiseNodeBearer(god_of_dof.GetProcessorWiseNodeBearerList(),
                                                  mpi_rank); // at the time of this function call all \a NodeBearer
                                                             // are still there.

            const auto source_node_bearer_per_coords_list =
                Internal::NodeBearerNS::GenerateNodeBearerPerCoordsList(non_processor_wise_non_bearer_list);

            for (const auto& node_bearer_ptr : target_processor_wise_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));

                decltype(auto) interface = node_bearer_ptr->GetInterface();

                decltype(auto) interface_coords_list =
                    interface.ComputeVertexCoordsIndexList<::MoReFEM::CoordsNS::index_enum::from_mesh_file>();

                const auto source_coords_index = vertex_matching.FindSourceIndex(interface_coords_list);

                auto it = source_node_bearer_per_coords_list.find(source_coords_index);

                if (it != source_node_bearer_per_coords_list.cend())
                {
                    auto [it2, is_properly_inserted] = ret.insert(*it);
                    assert(is_properly_inserted);
                    static_cast<void>(is_properly_inserted);
                }
            }
        } // if (god_of_dof.GetUniqueId() == vertex_matching.GetSourceMeshId())

        return ret;
    }

    namespace // anonymous
    {


        NodeBearer::vector_shared_ptr
        ExtractNonProcessorWiseNodeBearer(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                                          std::size_t mpi_rank)
        {
            NodeBearer::vector_shared_ptr ret(program_wise_node_bearer_list.size());

            auto logical_end = std::copy_if(program_wise_node_bearer_list.cbegin(),
                                            program_wise_node_bearer_list.cend(),
                                            ret.begin(),
                                            [mpi_rank](const NodeBearer::shared_ptr& node_bearer_ptr)
                                            {
                                                return node_bearer_ptr->GetProcessor() != mpi_rank;
                                            });

            ret.erase(logical_end, ret.end());
            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::GodOfDofNS


/// @} // addtogroup FiniteElementGroup
