/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline const FEltSpace& CoordsMatchingInterpolator::GetSourceFEltSpace() const noexcept
            {
                return source_felt_space_;
            }


            inline const NumberingSubset& CoordsMatchingInterpolator::GetSourceNumberingSubset() const noexcept
            {
                return source_numbering_subset_;
            }


            inline const FEltSpace& CoordsMatchingInterpolator::GetTargetFEltSpace() const noexcept
            {
                return target_felt_space_;
            }


            inline const NumberingSubset& CoordsMatchingInterpolator::GetTargetNumberingSubset() const noexcept
            {
                return target_numbering_subset_;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HXX_
