/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp"


#include <optional>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"

#include "Geometry/Interpolator/CoordsMatching.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<class SectionT>
            void CoordsMatchingInterpolatorManager::Create(
                const SectionT& section,
                std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>&& vertex_matching_informations)
            {
                if (vertex_matching_informations == std::nullopt)
                    throw Exception("CoordsMatchingInterpolator can't be defined without an InterpolationFile; "
                                    "please check the definition of your input data tuple!",
                                    __FILE__,
                                    __LINE__);

                namespace ipl = Internal::InputDataNS;

                decltype(auto) source_felt_space_index =
                    ipl::ExtractLeaf<typename SectionT::SourceFEltSpaceIndex>(section);
                decltype(auto) target_felt_space_index =
                    ipl::ExtractLeaf<typename SectionT::TargetFEltSpaceIndex>(section);

                decltype(auto) source_numbering_subset_index =
                    ipl::ExtractLeaf<typename SectionT::SourceNumberingSubsetIndex>(section);
                decltype(auto) target_numbering_subset_index =
                    ipl::ExtractLeaf<typename SectionT::TargetNumberingSubsetIndex>(section);

                Create(section.GetUniqueId(),
                       vertex_matching_informations.value(),
                       source_felt_space_index,
                       source_numbering_subset_index,
                       target_felt_space_index,
                       target_numbering_subset_index);
            }


            inline const auto& CoordsMatchingInterpolatorManager::GetStorage() const noexcept
            {
                return list_;
            }


            inline auto& CoordsMatchingInterpolatorManager::GetNonCstStorage() noexcept
            {
                return list_;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_
