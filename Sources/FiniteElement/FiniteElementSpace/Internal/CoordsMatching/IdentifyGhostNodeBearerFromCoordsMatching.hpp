//! \file
//
//
//  IdentifyGhostNodeBearerFromCoordsMatching.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 20/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_IDENTIFY_GHOST_NODE_BEARER_FROM_COORDS_MATCHING_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_IDENTIFY_GHOST_NODE_BEARER_FROM_COORDS_MATCHING_HPP_

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    /*!
     * \brief Prepare a list of \a NodeBearer that will be needed to define the \a FromCoordsMatching operator.
     *
     * \attention The goal is to identify the ones that would be left aside otherwise. For this reason, I keep only those that aren't on current processor  -
     * the reduction has not yet occurred but I already have the information of which will be handled processor-wise.
     *
     * \internal In fact, *more* \a NodeBearer than the list below is actually required: the ones connected to then within a \a LocalFEltSpace
     * shall also be required so that the integrity of the data is preserved (without that rebuilding fromn
     * prepartitioned data would fail).
     *
     * \param[in] god_of_dof The \a GodOfDof for which the additional \a NodeBearer might be computed (only if its unique id matches
     * \a vertex_matching's \a GetSourceMeshId().
     * \param[in] vertex_matching The geometric match between \a Coords of source and target meshes.
     *
     * \return List of *non* processor-wise \a NodeBearer that are a direct match for a \a FromCoordsMatching operator on the current rank.
     */
    Internal::NodeBearerNS::node_bearer_per_coords_index_list_type IdentifyGhostNodeBearerFromCoordsMatching(
        const GodOfDof& god_of_dof,
        const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching& vertex_matching);


} // namespace MoReFEM::Internal::GodOfDofNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_IDENTIFY_GHOST_NODE_BEARER_FROM_COORDS_MATCHING_HPP_
