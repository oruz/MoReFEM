/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    CoordsMatchingInterpolator ::CoordsMatchingInterpolator(std::size_t unique_id,
                                                            const FEltSpace& source_felt_space,
                                                            const NumberingSubset& source_numbering_subset,
                                                            const FEltSpace& target_felt_space,
                                                            const NumberingSubset& target_numbering_subset)
    : unique_id_parent(unique_id), source_felt_space_(source_felt_space),
      source_numbering_subset_(source_numbering_subset), target_felt_space_(target_felt_space),
      target_numbering_subset_(target_numbering_subset)
    {
        assert(source_felt_space.GetUniqueId() != target_felt_space.GetUniqueId());
        assert(source_numbering_subset_.GetUniqueId() != target_numbering_subset.GetUniqueId());
    }


    const std::string& CoordsMatchingInterpolator::ClassName()
    {
        static std::string ret("CoordsMatchingInterpolator");
        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup
