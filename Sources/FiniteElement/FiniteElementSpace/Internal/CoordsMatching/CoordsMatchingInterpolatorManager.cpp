/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp" // IWYU pragma: associated


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            CoordsMatchingInterpolatorManager::~CoordsMatchingInterpolatorManager() = default;


            const std::string& CoordsMatchingInterpolatorManager::ClassName()
            {
                static std::string ret("CoordsMatchingInterpolatorManager");
                return ret;
            }


            void CoordsMatchingInterpolatorManager::Create(
                const std::size_t unique_id,
                const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching& vertex_matching,
                std::size_t source_felt_space_index,
                std::size_t source_numbering_subset_index,
                std::size_t target_felt_space_index,
                std::size_t target_numbering_subset_index)
            {
                decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);


                decltype(auto) source_god_of_dof = god_of_dof_manager.GetGodOfDof(vertex_matching.GetSourceMeshId());
                decltype(auto) target_god_of_dof = god_of_dof_manager.GetGodOfDof(vertex_matching.GetTargetMeshId());

                namespace ipl = Utilities::InputDataNS;

                decltype(auto) source_felt_space = source_god_of_dof.GetFEltSpace(source_felt_space_index);
                decltype(auto) target_felt_space = target_god_of_dof.GetFEltSpace(target_felt_space_index);

                decltype(auto) source_numbering_subset =
                    source_god_of_dof.GetNumberingSubset(source_numbering_subset_index);
                decltype(auto) target_numbering_subset =
                    target_god_of_dof.GetNumberingSubset(target_numbering_subset_index);

                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                CoordsMatchingInterpolator* buf = new CoordsMatchingInterpolator(
                    unique_id, source_felt_space, source_numbering_subset, target_felt_space, target_numbering_subset);

                auto&& ptr = CoordsMatchingInterpolator::const_unique_ptr(buf);

                assert(ptr->GetUniqueId() == unique_id);

                auto&& pair = std::make_pair(unique_id, std::move(ptr));

                auto insert_return_value = list_.insert(std::move(pair));

                if (!insert_return_value.second)
                    throw Exception(
                        "Two CoordsMatchingInterpolator objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id) + ").",
                        __FILE__,
                        __LINE__);
            }


            const CoordsMatchingInterpolator&
            CoordsMatchingInterpolatorManager::GetCoordsMatchingInterpolator(std::size_t unique_id) const
            {
                const auto& list = GetStorage();

                auto it = list.find(unique_id);
                assert(it != list.cend());

                assert(!(!(it->second)));

                return *(it->second);
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
