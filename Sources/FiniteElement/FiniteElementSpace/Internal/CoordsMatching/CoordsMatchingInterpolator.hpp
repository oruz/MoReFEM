/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>

#include "Utilities/Containers/UnorderedMap.hpp" // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp"       // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::Internal::FEltSpaceNS { class CoordsMatchingInterpolatorManager; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief An internal class used to init correctly a \a FromCoordsMatching interpolator.
             *
             * This class just retains the \a FEltSpace and \a NumberingSubset related to both the source and target
             meshes of the interpolation.

             */
            class CoordsMatchingInterpolator
            : public ::MoReFEM::Crtp::UniqueId<CoordsMatchingInterpolator, UniqueIdNS::AssignationMode::manual>
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = CoordsMatchingInterpolator;

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const self>;

                //! Friendship to manager.
                friend CoordsMatchingInterpolatorManager;

                //! Convenient alias to parent.
                using unique_id_parent =
                    ::MoReFEM::Crtp::UniqueId<CoordsMatchingInterpolator, UniqueIdNS::AssignationMode::manual>;

                //! Returns the name of the class.
                static const std::string& ClassName();

              private:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] unique_id Unique identifier of the \a InputDataNS::CoordsMatchingInterpolator
                 * considered.
                 * \param[in] source_felt_space Source \a FEltSpace.
                 * \param[in] source_numbering_subset Source \a NumberingSubset .
                 * \param[in] target_felt_space Target \a FEltSpace.
                 * \param[in] target_numbering_subset Target \a NumberingSubset .
                 */
                explicit CoordsMatchingInterpolator(std::size_t unique_id,
                                                    const FEltSpace& source_felt_space,
                                                    const NumberingSubset& source_numbering_subset,
                                                    const FEltSpace& target_felt_space,
                                                    const NumberingSubset& target_numbering_subset);


              public:
                //! Destructor.
                ~CoordsMatchingInterpolator() = default;

                //! \copydoc doxygen_hide_copy_constructor
                CoordsMatchingInterpolator(const CoordsMatchingInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                CoordsMatchingInterpolator(CoordsMatchingInterpolator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                CoordsMatchingInterpolator& operator=(const CoordsMatchingInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                CoordsMatchingInterpolator& operator=(CoordsMatchingInterpolator&& rhs) = delete;

                ///@}

              public:
                //! Source \a FEltSpace considered.
                const FEltSpace& GetSourceFEltSpace() const noexcept;

                //! Source \a NumberingSubset considered.
                const NumberingSubset& GetSourceNumberingSubset() const noexcept;

                //! Target \a FEltSpace considered.
                const FEltSpace& GetTargetFEltSpace() const noexcept;

                //! Target \a NumberingSubset considered.
                const NumberingSubset& GetTargetNumberingSubset() const noexcept;


              private:
                //! Source \a FEltSpace considered.
                const FEltSpace& source_felt_space_;

                //! Source \a NumberingSubset considered.
                const NumberingSubset& source_numbering_subset_;

                //! Target \a FEltSpace considered.
                const FEltSpace& target_felt_space_;

                //! Target \a NumberingSubset considered.
                const NumberingSubset& target_numbering_subset_;
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_HPP_
