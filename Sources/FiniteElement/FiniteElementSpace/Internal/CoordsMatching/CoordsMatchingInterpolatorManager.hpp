/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <optional>
#include <unordered_map>

#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp"                        // IWYU pragma: export

#include "Core/InputData/Instances/Interpolator/CoordsMatchingInterpolator.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief This class is used to create and retrieve \a CoordsMatchingInterpolator
             * objects.
             *
             * CoordsMatchingInterpolator objects get private constructor and can only be created
             * through this class.
             */
            class CoordsMatchingInterpolatorManager : public Utilities::Singleton<CoordsMatchingInterpolatorManager>
            {

              public:
                /*!
                 * \brief Returns the name of the class (required for some Singleton-related errors).
                 *
                 * \return Name of the class.
                 */
                static const std::string& ClassName();


                /*!
                 * \brief Base type of CoordsMatchingInterpolator as input parameter.
                 */
                using input_data_type = ::MoReFEM::InputDataNS::BaseNS::CoordsMatchingInterpolator;

              public:
                /*!
                 * \brief Create a new CoordsMatchingInterpolator object from the data of the input
                 * parameter file.
                 *
                 * This method will be called from all GodOfDofs, but only the once that encompass relevant \a FEltSpace
                 * is allowed to proceed.
                 *
                 * \param[in] section Relevant section of the input data file (interpreted a as \a
                 * InputDataNS::BaseNS: CoordsMatchingInterpolator).
                 * \copydoc doxygen_hide_coords_matching_arg
                 *
                 * \tparam SectionT Type pf \a section.
                 */
                template<class SectionT>
                void Create(const SectionT& section,
                            std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>&& coords_matching);

                //! Fetch the object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{CoordsMatchingInterpolator}
                const CoordsMatchingInterpolator& GetCoordsMatchingInterpolator(std::size_t unique_id) const;

              private:
                //! Access to the storage.
                const auto& GetStorage() const noexcept;

                //! Access to the storage.
                auto& GetNonCstStorage() noexcept;

                /*!
                 * \brief Create from a finite element space and a numbering subset.
                 *
                 * \param[in] unique_id Unique identifier to give to the \a
                 * CoordsMatchingInterpolator object being created.
                 * \copydoc doxygen_hide_coords_matching_arg
                 * \param[in] source_felt_space Source \a FEltSpace.
                 * \param[in] source_numbering_subset Source \a NumberingSubset .
                 * \param[in] target_felt_space Target \a FEltSpace.
                 * \param[in] target_numbering_subset Target \a NumberingSubset .
                 */
                void Create(std::size_t unique_id,
                            const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                            std::size_t source_felt_space,
                            std::size_t source_numbering_subset,
                            std::size_t target_felt_space,
                            std::size_t target_numbering_subset);


              private:
                //! \name Singleton requirements.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] strategy This one is just there to signal an issue if current class is attempted to run in
                 * a run from prepartitioned data: current class is EXTREMELY specific and can't be initialised from
                 * prepartitioned data (it could, but this would involve storing loads of data... whereas it's cheaper
                 * to store for instance the \a CoordsMatching interpolation matrix that current class helps to
                 * compute).
                 */
                CoordsMatchingInterpolatorManager() = default;

                //! Destructor.
                virtual ~CoordsMatchingInterpolatorManager() override;

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<CoordsMatchingInterpolatorManager>;
                ///@}


              private:
                //! Store the \a CoordsMatchingInterpolatorManager per their unique id.
                //! \internal The nature of the storage is for conveniency: we could have stored the objects in a vector
                //! as the key is also what is returned if you use \a GetUniqueId() method on the second member of the
                //! pair.
                std::unordered_map<std::size_t, CoordsMatchingInterpolator::const_unique_ptr> list_;
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_
