//! \file
//
//
//  OutputDirectoryStorage.hxx
//  MoReFEM
//
//  Created by sebastien on 19/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <unordered_map>


namespace MoReFEM::Internal::GodOfDofNS
{


    inline const std::string& OutputDirectoryStorage::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


    inline const std::unordered_map<std::size_t, std::string>&
    OutputDirectoryStorage ::GetOutputDirectoryPerNumberingSubset() const noexcept
    {
        return output_directory_per_numbering_subset_;
    }


} // namespace MoReFEM::Internal::GodOfDofNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_
