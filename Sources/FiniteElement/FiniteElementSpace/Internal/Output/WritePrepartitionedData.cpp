//! \file
//
//
//  WritePrepartitionedData.cpp
//  MoReFEM
//
//  Created by sebastien on 04/05/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#include <algorithm>
#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <ostream>
#include <set>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


    namespace // anonymous
    {


        std::vector<std::string> GenerateInterfaceList(const NodeBearer::vector_shared_ptr& node_bearer_list);

        // Useful only for ghosts: for processor-wise they are continuous so just the index of the first one is enough.
        std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type>
        GenerateIndexList(const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

        // Useful only for ghosts.
        std::vector<std::size_t> GenerateMpiRankList(const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

        /*!
         * \brief For each processor-wise \a NodeBearer, keep track of the other processors that are ghosting it.
         *
         * \return Key is the rank of all processors, values are the program-wise indexes of the processor-wise \a
         * NodeBearer that are ghosted on the processor which index is given as key.
         */
        std::vector<std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type>>
        GenerateMpiGhostProcessorList(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                      std::size_t Nprocessor);

        // Generate the lists of program-wise indexes for \a numbering subset.
        std::vector<std::size_t>
        GenerateProgramWiseDofIndexesForNumberingSubset(const GodOfDof& god_of_dof,
                                                        const NumberingSubset& numbering_subset);

        //! Extract in \a out the program-wise indexes of elements of \a dof_list that are WITHIN \a NumberingSubset .
        void GenerateProgramWiseDofList(const NumberingSubset& numbering_subset,
                                        const Dof::vector_shared_ptr& dof_list,
                                        std::vector<std::size_t>& out);


    } // namespace


    void WritePrepartitionedData(const GodOfDof& god_of_dof, const ::MoReFEM::FilesystemNS::Directory& mesh_directory)
    {
        const auto partition_file = mesh_directory.AddFile("god_of_dof_data.lua");

        std::ofstream out;

        ::MoReFEM::FilesystemNS::File::Create(out, partition_file, __FILE__, __LINE__);

        {
            decltype(auto) node_bearer_list = god_of_dof.GetProcessorWiseNodeBearerList();
            assert(!node_bearer_list.empty());

            out << "\n-- Program-wise index of the first Node Bearer on the current rank" << std::endl;
            out << "node_bearer_first_program_wise_index = " << node_bearer_list.front()->GetProgramWiseIndex()
                << std::endl;

            out << "\n-- Nature and program-wise index of the underlying interface for each processor-wise NodeBearer ("
                << node_bearer_list.size() << ')' << std::endl;
            out << "interface_per_node_bearer = ";
            Utilities::PrintContainer<>::Do(GenerateInterfaceList(node_bearer_list),
                                            out,
                                            ::MoReFEM::PrintNS::Delimiter::separator("', '"),
                                            ::MoReFEM::PrintNS::Delimiter::opener("{ '"),
                                            ::MoReFEM::PrintNS::Delimiter::closer("' }"));

            const auto& mpi = god_of_dof.GetMpi();

            decltype(auto) ghost_node_bearer_list = god_of_dof.GetGhostNodeBearerList();

            out << "\n-- Nature and program-wise index of the underlying interface for each ghost NodeBearer."
                << std::endl;
            out << "interface_per_ghost_node_bearer = ";
            Utilities::PrintContainer<>::Do(GenerateInterfaceList(ghost_node_bearer_list),
                                            out,
                                            ::MoReFEM::PrintNS::Delimiter::separator("', '"),
                                            ::MoReFEM::PrintNS::Delimiter::opener("{ '"),
                                            ::MoReFEM::PrintNS::Delimiter::closer("' }"));

            out << std::endl;

            out << "\n-- Program-wise index of each ghost NodeBearer." << std::endl;
            out << "ghost_node_bearer_indexes = ";
            Utilities::PrintContainer<>::Do(GenerateIndexList(ghost_node_bearer_list),
                                            out,
                                            ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                            ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                            ::MoReFEM::PrintNS::Delimiter::closer(" }"));

            out << std::endl;

            out << "\n-- Mpi rank of each ghost NodeBearer." << std::endl;
            out << "ghost_node_bearer_mpi_rank = ";
            Utilities::PrintContainer<>::Do(GenerateMpiRankList(ghost_node_bearer_list),
                                            out,
                                            ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                            ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                            ::MoReFEM::PrintNS::Delimiter::closer(" }"));
            out << std::endl;


            {
                decltype(auto) Nproc = mpi.Nprocessor<std::size_t>();

                const auto ghost_processor_list_per_processor = GenerateMpiGhostProcessorList(node_bearer_list, Nproc);
                assert(ghost_processor_list_per_processor.size() == Nproc);

                const auto rank = mpi.GetRank<std::size_t>();

                for (auto proc = 0ul; proc < Nproc; ++proc)
                {
                    if (proc == rank)
                        continue;

                    out << "\n-- List of the current rank processor-wise NodeBearer (represented by their "
                           "program-wise index) ghosted on processor "
                        << proc << std::endl;
                    out << "ghosted_processor_wise_node_bearer_on_processor_" << proc << " = ";
                    Utilities::PrintContainer<>::Do(ghost_processor_list_per_processor[proc],
                                                    out,
                                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                    ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                    ::MoReFEM::PrintNS::Delimiter::closer(" }\n"));
                }
            }

            out << "\n-- For each numbering subset, the list of the program-wise index of its dof." << std::endl;
            out << "-- We are considered here the program-wise index INSIDE the NumberingSubset, which may differ "
                << "from the internal one defined regardless of the numbering subsets." << std::endl;

            decltype(auto) numbering_subset_list = god_of_dof.GetNumberingSubsetList();

            for (const auto& numbering_subset_ptr : numbering_subset_list)
            {
                assert(!(!numbering_subset_ptr));
                const auto& numbering_subset = *numbering_subset_ptr;

                out << "program_wise_dof_index_for_numbering_subset_" << numbering_subset.GetUniqueId() << " = ";
                Utilities::PrintContainer<>::Do(
                    GenerateProgramWiseDofIndexesForNumberingSubset(god_of_dof, numbering_subset),
                    out,
                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                    ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                    ::MoReFEM::PrintNS::Delimiter::closer(" }\n"));

                out << std::endl;
            }
        }
    }


    namespace // anonymous
    {


        std::vector<std::string> GenerateInterfaceList(const NodeBearer::vector_shared_ptr& node_bearer_list)
        {
            std::vector<std::string> interface_list(node_bearer_list.size());

            std::transform(node_bearer_list.cbegin(),
                           node_bearer_list.cend(),
                           interface_list.begin(),
                           [](const auto& node_bearer_ptr)
                           {
                               assert(!(!node_bearer_ptr));
                               return ShortHand(node_bearer_ptr->GetInterface());
                           });

            return interface_list;
        }


        std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type>
        GenerateIndexList(const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type> index_list(ghost_node_bearer_list.size());

            std::transform(ghost_node_bearer_list.cbegin(),
                           ghost_node_bearer_list.cend(),
                           index_list.begin(),
                           [](const auto& node_bearer_ptr)
                           {
                               assert(!(!node_bearer_ptr));
                               return node_bearer_ptr->GetProgramWiseIndex();
                           });

            return index_list;
        }


        std::vector<std::size_t> GenerateMpiRankList(const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            std::vector<std::size_t> index_list(ghost_node_bearer_list.size());

            std::transform(ghost_node_bearer_list.cbegin(),
                           ghost_node_bearer_list.cend(),
                           index_list.begin(),
                           [](const auto& node_bearer_ptr)
                           {
                               assert(!(!node_bearer_ptr));
                               return node_bearer_ptr->GetProcessor();
                           });

            return index_list;
        }


        std::vector<std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type>>
        GenerateMpiGhostProcessorList(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                      const std::size_t Nprocessor)
        {
            std::vector<std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type>> ret(Nprocessor);

            for (const auto& node_bearer_ptr : processor_wise_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                decltype(auto) ghost_proc_list = node_bearer_ptr->GetGhostProcessorList();

                for (const auto ghost_proc : ghost_proc_list)
                {
                    assert(ghost_proc < Nprocessor);
                    ret[ghost_proc].push_back(node_bearer.GetProgramWiseIndex());
                }
            }

            return ret;
        }


        void GenerateProgramWiseDofList(const NumberingSubset& numbering_subset,
                                        const Dof::vector_shared_ptr& dof_list,
                                        std::vector<std::size_t>& out)
        {
            for (const auto& dof_ptr : dof_list)
            {
                assert(!(!dof_ptr));
                if (dof_ptr->IsInNumberingSubset(numbering_subset))
                    out.push_back(dof_ptr->GetProgramWiseIndex(numbering_subset));
            }
        }


        std::vector<std::size_t>
        GenerateProgramWiseDofIndexesForNumberingSubset(const GodOfDof& god_of_dof,
                                                        const NumberingSubset& numbering_subset)
        {
            std::vector<std::size_t> ret;

            const auto capacity_guess = god_of_dof.NprocessorWiseDof(numbering_subset) + 20ul; // arbitrary to avoid
                                                                                               // number of memory
                                                                                               // allocation steps.
            ret.reserve(capacity_guess);

            {
                GenerateProgramWiseDofList(numbering_subset, god_of_dof.GetProcessorWiseDofList(), ret);

                GenerateProgramWiseDofList(numbering_subset, god_of_dof.GetGhostDofList(), ret);
            }

            assert(ret.size() >= god_of_dof.NprocessorWiseDof(numbering_subset));

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::GodOfDofNS
