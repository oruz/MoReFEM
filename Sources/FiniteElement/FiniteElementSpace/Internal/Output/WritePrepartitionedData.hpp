//! \file
//
//
//  WritePrepartitionedData.hpp
//  MoReFEM
//
//  Created by sebastien on 04/05/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HPP_

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal::GodOfDofNS
    {


        /*!
         * \brief Write prepartitioned data related to \a GodOfDof.
         *
         * \param[in] god_of_dof \a GodOfDof for which data are written.
         * \param[in] mesh_directory Directory in which the file will be created; remember there is exactly one \a
         * GodOfDof per \a Mesh.
         */
        void WritePrepartitionedData(const GodOfDof& god_of_dof,
                                     const ::MoReFEM::FilesystemNS::Directory& mesh_directory);


    } // namespace Internal::GodOfDofNS


} // namespace MoReFEM


#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HPP_
