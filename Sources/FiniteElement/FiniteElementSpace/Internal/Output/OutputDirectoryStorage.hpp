//! \file
//
//
//  OutputDirectoryStorage.hpp
//  MoReFEM
//
//  Created by sebastien on 19/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HPP_

#include <memory>
#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>
#include <cstddef> // IWYU pragma: keep
#include <unordered_map>

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{

    //! Whether we want true output directory or the one in which rank is replace by a wildcard.
    enum class wildcard_for_rank
    {
        yes,
        no
    };


    /*!
     * \brief Facility to store the paths to the output directories related to a \a GodOfDof.
     */
    class OutputDirectoryStorage
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = OutputDirectoryStorage;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] output_directory The output directory in which all outputs are written. One purpose of this class
         * is to manage more finely some subdirectories of this one. This class does not create it and expects it
         * to already exist (except if wildcard_for_rank is set to yes).
         * \param[in] numbering_subset_list List of \a NumberingSubset relevant to the \a GodOfDof.
         * \param[in] is_wildcard If wildcard_for_rank::yes, the rank number is replaced by '*' in \a output_directory.
         * For instance, '/foo/.../Rank_0/...'' is replaced by '/foo/.../Rank_STAR/...' where STAR is '*'. An exception
         * is thrown if 'Rank_' is not found in \a output_directory.
         */
        explicit OutputDirectoryStorage(const std::string& output_directory,
                                        const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                        wildcard_for_rank is_wildcard = wildcard_for_rank::no);

        //! Destructor.
        ~OutputDirectoryStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        OutputDirectoryStorage(const OutputDirectoryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OutputDirectoryStorage(OutputDirectoryStorage&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OutputDirectoryStorage& operator=(const OutputDirectoryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OutputDirectoryStorage& operator=(OutputDirectoryStorage&& rhs) = delete;

        ///@}

      public:
        //! Path to the global output directory.
        const std::string& GetOutputDirectory() const noexcept;

        /*!
         * \brief Returns the name of the subfolder related to a given numbering subset.
         *
         * For instance
         *
         * \code
         * *path_to_output_directory* /NumberingSubset*i*
         * \endcode
         *
         * where i is the unique id of the numbering subset.
         *
         * \param[in] numbering_subset Numbering subset for which output folder is sought.
         *
         * \return Name of the subfolder related to a given numbering subset.
         */
        const std::string& GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const;

      private:
        //! Accessor to \a output_directory_per_numbering_subset_.
        const std::unordered_map<std::size_t, std::string>& GetOutputDirectoryPerNumberingSubset() const noexcept;

      private:
        //! Path to the global output directory.
        std::string output_directory_;

        /*!
         * \brief For each \a NumberingSubset, store the path to the related subdirectory.
         *
         * This is stored rather than recomputed on the fly to avoid reallocating memory each time a call is needed.
         *
         * - Key: unique id of the \a NumberingSubset (as given by GetUniqueId() method).
         * - Value: path to the dedicated
         */
        std::unordered_map<std::size_t, std::string> output_directory_per_numbering_subset_;
    };


} // namespace MoReFEM::Internal::GodOfDofNS


#include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HPP_
