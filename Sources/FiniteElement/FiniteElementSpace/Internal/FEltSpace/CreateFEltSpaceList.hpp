//! \file
//
//
//  CreateFEltSpaceList.hpp
//  MoReFEM
//
//  Created by sebastien on 29/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Create all the \a FEltSpace objects from the input data file.
     *
     * \internal <b><tt>[internal]</tt></b> Should not be called outside of InitAllGodOfDofs().
     * \endinternal
     *
     * \copydoc doxygen_hide_input_data_arg
     * \return The list of  all \a FEltSpace detailed in the input
     * data file and store them in this container which key is the index of the \a GodOfDof (which are
     * not yet created when this function is called - hence the index rather than the pointer of the object).
     */
    template<class InputDataT>
    std::map<std::size_t, FEltSpace::vector_unique_ptr> CreateFEltSpaceList(const InputDataT& input_data);


} // namespace MoReFEM::Internal::FEltSpaceNS


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HPP_
