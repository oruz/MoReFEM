/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Mpi/Mpi.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp"


namespace MoReFEM
{
    class Domain;
}


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            Storage::Storage(const ::MoReFEM::Wrappers::Mpi& mpi,
                             LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_type,
                             LocalFEltSpacePerRefLocalFEltSpace&& ghost_felt_list_per_type)
            : ::MoReFEM::Crtp::CrtpMpi<Storage>(mpi),
              internal_storage_(Impl::InternalStorage(mpi, std::move(felt_list_per_type))),
              ghost_internal_storage_(Impl::InternalStorage(mpi, std::move(ghost_felt_list_per_type)))
            {
                constexpr auto factor = Utilities::DefaultMaxLoadFactor();
                GetNonCstFEltStoragePerDomain<RoleOnProcessor::processor_wise>().max_load_factor(factor);
                GetNonCstFEltStoragePerDomain<RoleOnProcessor::ghost>().max_load_factor(factor);
            }


            void Storage::SetReducedData(LocalFEltSpacePerRefLocalFEltSpace&& processor_wise_content,
                                         LocalFEltSpacePerRefLocalFEltSpace&& ghost_content)
            {
                internal_storage_.GetNonCstFEltListPerRefLocalFEltSpace() = std::move(processor_wise_content);
                ghost_internal_storage_.GetNonCstFEltListPerRefLocalFEltSpace() = std::move(ghost_content);

                processor_wise_felt_storage_per_domain_.clear();
                assert(ghost_felt_storage_per_domain_.empty());
            }


            LocalFEltSpacePerRefLocalFEltSpace
            Storage ::ComputeFEltListForDomain(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                               const Domain& domain) const
            {
                LocalFEltSpacePerRefLocalFEltSpace ret;

                for (const auto& pair : full_content)
                {
                    auto& ref_felt_space_ptr = pair.first;
                    const auto& complete_local_felt_space_list = pair.second;

                    LocalFEltSpace::per_geom_elt_index local_felt_space_list;
                    local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

                    for (const auto& local_felt_space_pair : complete_local_felt_space_list)
                    {
                        const auto& local_felt_space_ptr = local_felt_space_pair.second;
                        assert(!(!local_felt_space_ptr));

                        const auto& local_felt_space = *local_felt_space_ptr;

                        if (IsLocalFEltSpaceInDomain(local_felt_space, domain))
                            local_felt_space_list.insert(
                                { local_felt_space.GetGeometricElt().GetIndex(), local_felt_space_ptr });
                    }

                    if (!local_felt_space_list.empty())
                    {
                        auto&& copy_ref_felt_space_ptr =
                            std::make_unique<Internal::RefFEltNS::RefLocalFEltSpace>(*ref_felt_space_ptr);
                        ret.push_back({ std::move(copy_ref_felt_space_ptr), local_felt_space_list });
                    }
                }

                return ret;
            }


            bool Storage::IsEmpty() const noexcept
            {
                return GetStorage<RoleOnProcessor::processor_wise>().IsEmpty()
                       && GetStorage<RoleOnProcessor::ghost>().IsEmpty();
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
