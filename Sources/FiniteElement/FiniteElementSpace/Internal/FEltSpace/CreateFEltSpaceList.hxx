//! \file
//
//
//  CreateFEltSpaceList.hxx
//  MoReFEM
//
//  Created by sebastien on 29/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::FEltSpaceNS
{


    template<class InputDataT>
    std::map<std::size_t, FEltSpace::vector_unique_ptr> CreateFEltSpaceList(const InputDataT& input_data)
    {
        std::map<std::size_t, FEltSpace::vector_unique_ptr> ret;

        auto create = [&ret](const auto& section) -> void
        {
            namespace ipl = Internal::InputDataNS;

            using section_type = std::decay_t<decltype(section)>;

            const auto unique_id = section.GetUniqueId();

            decltype(auto) god_of_dof_index = ipl::ExtractLeaf<typename section_type::GodOfDofIndex>(section);
            decltype(auto) domain_index = ipl::ExtractLeaf<typename section_type::DomainIndex>(section);

            auto&& extended_unknown_list = MoReFEM::Internal::FEltSpaceNS::ExtractExtendedUnknownList(section);

            decltype(auto) god_of_dof_ptr =
                GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDofPtr(god_of_dof_index);

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            try
            {
                const auto& domain = domain_manager.GetDomain(domain_index, __FILE__, __LINE__);

                auto felt_space_ptr =
                    std::make_unique<FEltSpace>(god_of_dof_ptr, domain, unique_id, std::move(extended_unknown_list));

                ret[god_of_dof_index].emplace_back(std::move(felt_space_ptr));
            }
            catch (const Exception& e)
            {
                std::ostringstream oconv;
                oconv << "Ill-defined finite element space " << unique_id << ": " << e.GetRawMessage();
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        };


        using input_data_tuple_iteration = Internal::InputDataNS::
            TupleIteration<typename InputDataT::Tuple, 0, std::tuple_size<typename InputDataT::Tuple>::value>;

        input_data_tuple_iteration ::template ActIfSection<::MoReFEM::InputDataNS::BaseNS::FEltSpace>(
            input_data.GetTuple(), create);

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_
