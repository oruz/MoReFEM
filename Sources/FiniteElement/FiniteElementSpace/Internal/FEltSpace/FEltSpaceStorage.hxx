/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <mutex>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Utilities/Mpi/Mpi.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"


#include "Geometry/Domain/Domain.hpp"


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    template<RoleOnProcessor RoleOnProcessorT>
    inline const LocalFEltSpacePerRefLocalFEltSpace& Storage::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
    {
        return GetStorage<RoleOnProcessorT>().GetLocalFEltSpacePerRefLocalFEltSpace();
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline LocalFEltSpacePerRefLocalFEltSpace& Storage::GetNonCstLocalFEltSpacePerRefLocalFEltSpace() noexcept
    {
        return GetNonCstStorage<RoleOnProcessorT>().GetNonCstFEltListPerRefLocalFEltSpace();
    }


    template<>
    inline const Impl::InternalStorage& Storage::GetStorage<RoleOnProcessor::processor_wise>() const noexcept
    {
        return internal_storage_;
    }


    template<>
    inline const Impl::InternalStorage& Storage::GetStorage<RoleOnProcessor::ghost>() const noexcept
    {
        return ghost_internal_storage_;
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline Impl::InternalStorage& Storage::GetNonCstStorage() noexcept
    {
        return const_cast<Impl::InternalStorage&>(GetStorage<RoleOnProcessorT>());
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::unordered_map<std::size_t, Impl::InternalStorage>&
    Storage ::GetNonCstFEltStoragePerDomain() const noexcept
    {
        if constexpr (RoleOnProcessorT == RoleOnProcessor::processor_wise)
            return processor_wise_felt_storage_per_domain_;
        else
            return ghost_felt_storage_per_domain_;
    }


    template<RoleOnProcessor RoleOnProcessorT>
    const LocalFEltSpacePerRefLocalFEltSpace& Storage::GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const
    {
        // Check whether the domain is already known in the class.
        auto& felt_storage_for_domain = AddOrFetchDomainStorage<RoleOnProcessorT>(domain, GetMpi());

        return felt_storage_for_domain.GetLocalFEltSpacePerRefLocalFEltSpace();
    }


    template<RoleOnProcessor RoleOnProcessorT>
    Impl::InternalStorage& Storage::AddOrFetchDomainStorage(const Domain& domain,
                                                            const ::MoReFEM::Wrappers::Mpi& mpi) const
    {
        decltype(auto) full_content = GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>();
        const auto domain_unique_id = domain.GetUniqueId();
        decltype(auto) storage_per_domain = GetNonCstFEltStoragePerDomain<RoleOnProcessorT>();
        auto it = storage_per_domain.find(domain_unique_id);

        {
            std::lock_guard<std::mutex> lock(GetMutex());

            if (it == storage_per_domain.cend())
            {
                auto&& pair = std::make_pair(
                    domain_unique_id, Impl::InternalStorage(mpi, ComputeFEltListForDomain(full_content, domain)));

                auto check = storage_per_domain.emplace(std::move(pair));

                assert(check.second
                       && "The id was supposed not to exist, otherwise the test above would have fail "
                          "and we wouldn't be in this block.");

                it = check.first;
            }
        }

        assert(it != storage_per_domain.cend());
        return it->second;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_STORAGE_HXX_
