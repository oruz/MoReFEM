/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp" // IWYU pragma: export

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Return the domain that is associated to a given finite element space.
             *
             * \internal <b><tt>[internal]</tt></b> A factory function is used here due to the \a UniqueIdT template
             * parameter: C++ grammar doesn't allow an explicit template parameter in a non-template class constructor.
             * \endinternal
             *
             * \copydoc doxygen_hide_input_data_arg
             *
             * \tparam UniqueIdT Unique id of the finite element space from which the domain is extracted.
             * This unique id is the one used in the input data file; it is for instance 5 for block tagged
             * FiniteElementSpace5.
             * \tparam InputDataT Type of \a input_data.
             *
             * \return The full fledged domain object. It must have been built beforehand by the Domain manager.
             */
            template<std::size_t UniqueIdT, class InputDataT>
            Domain& ExtractDomain(const InputDataT& input_data);


            /*!
             * \brief Return the list of unknowns that is associated to a given finite element space and their
             * associated numbering subset.
             *
             *
             * \param[in] section The input parameter section currently considered (e.g. Unknown<5>).
             * \tparam SectionT Type of \a section.
             *
             * \return The list of unknowns. It must have been built beforehand by the Domain manager.
             */
            template<class SectionT>
            ExtendedUnknown::vector_const_shared_ptr ExtractExtendedUnknownList(const SectionT& section);


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_
