/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>

#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<std::size_t I>
            const Internal::RefFEltNS::RefLocalFEltSpace*
            FindRefLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& container)
            {
                for (const auto& pair : container)
                {
                    const auto& ref_local_felt_space_ptr = pair.first;
                    assert(!(!ref_local_felt_space_ptr));

                    decltype(auto) ref_geom_elt = ref_local_felt_space_ptr->GetRefGeomElt();
                    const auto ref_geom_elt_id = EnumUnderlyingType(ref_geom_elt.GetIdentifier());

                    if (ref_geom_elt_id == I)
                        return ref_local_felt_space_ptr.get();
                }

                return nullptr;
            }


            namespace Impl
            {


                inline const LocalFEltSpacePerRefLocalFEltSpace&
                InternalStorage::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
                {
                    return felt_list_per_ref_felt_space_;
                }


                inline LocalFEltSpacePerRefLocalFEltSpace&
                InternalStorage::GetNonCstFEltListPerRefLocalFEltSpace() noexcept
                {
                    return const_cast<LocalFEltSpacePerRefLocalFEltSpace&>(GetLocalFEltSpacePerRefLocalFEltSpace());
                }


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_
