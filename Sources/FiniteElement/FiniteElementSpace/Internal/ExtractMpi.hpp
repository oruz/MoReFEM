//! \file
//
//
//  ExtractMpi.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 16/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXTRACT_MPI_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXTRACT_MPI_HPP_


#ifndef NDEBUG


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    /*!
     * \brief Extract the \a Mpi object from a given \a GeometricElt.
     *
     * \internal This is purely for debug purposes: contrary to other classes, a \a GeometricElt has little business storing this information... but it can
     * be retrieved and is useful for some internal sanity checks.
     *
     * \param[in] geom_elt \a GeometricElt which rank is sought.
     *
     * \return \a Mpi object.
     */
    const ::MoReFEM::Wrappers::Mpi& ExtractMpi(const GeometricElt& geom_elt);


} // namespace MoReFEM::Internal


#endif // NDEBUG

#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXTRACT_MPI_HPP_
