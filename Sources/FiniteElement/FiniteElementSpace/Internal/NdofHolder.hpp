/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 14:51:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>

#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Structure that holds the number of dofs for different configurations.
             *
             * This structure is an helper structure to be used by \a GodOfDof and \a FEltSpace.
             */
            class NdofHolder final
            {
              public:
                //! Alias for unique_ptr.
                using const_unique_ptr = std::unique_ptr<const NdofHolder>;

                /*!
                 * \brief Constructor used in \a GodOfDof.
                 *
                 * Compute the number of dofs (both program- and processor-wise).
                 *
                 * \param[in] program_wise_node_bearer_list Node bearer list before processor-wise reduction.
                 * \param[in] mpi_rank Rank of the current processor.
                 * \param[in] numbering_subset_list List of \a NumberingSubset.
                 * \param[in] Nprogram_wise_dof In the case of running with pre-partitioned data, we need to force feed
                 * the number of program-wise values. Otherwise leave it empty and compute them from the data available
                 * (this call occurs before partitioning so the whole information is still there). \param[in]
                 * Nprogram_wise_dof_per_numbering_subset Same as \a Nprogram_wise_dof for each \a NumberingSubset.
                 * Please notice that if one is optional the other must be as well.
                 *
                 */
                NdofHolder(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                           const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                           const std::size_t mpi_rank,
                           std::optional<std::size_t> Nprogram_wise_dof = std::nullopt,
                           std::optional<std::map<std::size_t, std::size_t>> Nprogram_wise_dof_per_numbering_subset =
                               std::nullopt);

                /*!
                 * \brief Constructor used in \a FEltSpace.
                 *
                 * \copydoc doxygen_hide_mpi_param
                 * \param[in] processor_wise_dof_list \a Dof list after processor-wise reduction (ghost excluded).

                 * \param[in] numbering_subset_list List of \a NumberingSubset.
                 *
                 */
                NdofHolder(const ::MoReFEM::Wrappers::Mpi& mpi,
                           const Dof::vector_shared_ptr& processor_wise_dof_list,
                           const NumberingSubset::vector_const_shared_ptr& numbering_subset_list);

                //! Destructor.
                ~NdofHolder() = default;

                //! \copydoc doxygen_hide_copy_constructor
                NdofHolder(const NdofHolder& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                NdofHolder(NdofHolder&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                NdofHolder& operator=(const NdofHolder& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                NdofHolder& operator=(NdofHolder&& rhs) = delete;

                //! Get the number of program-wise dof.
                std::size_t NprogramWiseDof() const noexcept;

                //! Get the number of processor-wise dof (ghost excluded).
                std::size_t NprocessorWiseDof() const noexcept;

                //! Get the number of processor-wise dof (ghost excluded) within a \a numbering_subset.
                //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
                std::size_t NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

                //! Get the number of program-wise dof within a \a numbering_subset.
                //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
                std::size_t NprogramWiseDof(const NumberingSubset& numbering_subset) const;

              public:
                //! Accessor to the number of program-wise dof per numbering subset.
                const std::map<std::size_t, std::size_t>& NprogramWiseDofPerNumberingSubset() const noexcept;

              private:
                //! Accessor to the number of processor-wise dof per numbering subset.
                const std::map<std::size_t, std::size_t>& NprocessorWiseDofPerNumberingSubset() const noexcept;

              private:
                //! Number of program-wise dof.
                std::size_t Nprogram_wise_dof_ = 0ul;

                //! Number of processor-wise dof (ghost excluded).
                std::size_t Nprocessor_wise_dof_ = 0ul;

                /*!
                 * \brief Number of processor-wise dof for each numbering subset.
                 *
                 * . Key is numbering subset id.
                 * . Value is the number of dofs.
                 */
                std::map<std::size_t, std::size_t> Nprocessor_wise_dof_per_numbering_subset_;


                /*!
                 * \brief Number of program-wise dof for each numbering subset.
                 *
                 * . Key is numbering subset id.
                 * . Value is the number of dofs.
                 */
                std::map<std::size_t, std::size_t> Nprogram_wise_dof_per_numbering_subset_;
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_
