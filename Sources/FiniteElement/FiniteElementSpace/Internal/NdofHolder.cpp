/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 14:51:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>
#include <type_traits>
#include <utility>
#include <vector>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            NdofHolder::NdofHolder(
                const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                const std::size_t mpi_rank,
                std::optional<std::size_t> Nprogram_wise_dof,
                std::optional<std::map<std::size_t, std::size_t>> Nprogram_wise_dof_per_numbering_subset)
            {
                assert(Nprogram_wise_dof.has_value() == Nprogram_wise_dof_per_numbering_subset.has_value()
                       && "Either the program-wise is completely force-fed (for two steps parallelism) or it is "
                          "completely computed; no point doing half of it");

                // Perform the computation disregarding the numbering subsets.
                {
                    assert(!numbering_subset_list.empty());


                    for (const auto& node_bearer_ptr : program_wise_node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        const auto& node_bearer = *node_bearer_ptr;

                        auto Ndof = node_bearer.Ndof();
                        Nprogram_wise_dof_ += Ndof;

                        if (node_bearer.GetProcessor() == mpi_rank)
                            Nprocessor_wise_dof_ += Ndof;
                    }
                }

                {

                    // Now do it for each numbering subset.
                    for (const auto& numbering_subset_ptr : numbering_subset_list)
                    {
                        assert(!(!numbering_subset_ptr));
                        const auto& numbering_subset = *numbering_subset_ptr;
                        std::size_t local_Nprocessor_wise_dof = 0ul;
                        std::size_t local_Nprogram_wise_dof = 0ul;

                        for (const auto& node_bearer_ptr : program_wise_node_bearer_list)
                        {
                            assert(!(!node_bearer_ptr));
                            const auto& node_bearer = *node_bearer_ptr;

                            const auto Ndof = node_bearer.Ndof(numbering_subset);
                            local_Nprogram_wise_dof += Ndof;

                            if (node_bearer.GetProcessor() == mpi_rank)
                                local_Nprocessor_wise_dof += Ndof;
                        }

                        {
                            auto&& pair = std::make_pair(numbering_subset.GetUniqueId(), local_Nprocessor_wise_dof);
                            auto check = Nprocessor_wise_dof_per_numbering_subset_.insert(std::move(pair));

                            static_cast<void>(check);
                            assert(check.second && "A numbering subset should be inserted only once!");
                        }

                        {
                            auto&& pair = std::make_pair(numbering_subset.GetUniqueId(), local_Nprogram_wise_dof);
                            auto check = Nprogram_wise_dof_per_numbering_subset_.insert(std::move(pair));

                            static_cast<void>(check);
                            assert(check.second && "A numbering subset should be inserted only once!");
                        }
                    }
                }

                // Special case for two-steps parallelism: the code can't compute the full humber of program-wise dofs
                // and needs the values computed in the original run.
                if (Nprogram_wise_dof.has_value())
                {
                    Nprogram_wise_dof_ = Nprogram_wise_dof.value();
                    Nprogram_wise_dof_per_numbering_subset_ = Nprogram_wise_dof_per_numbering_subset.value();
                }
            }


            NdofHolder::NdofHolder(const ::MoReFEM::Wrappers::Mpi& mpi,
                                   const Dof::vector_shared_ptr& processor_wise_dof_list,
                                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list)
            {
                assert(IsConsistentOverRanks(mpi, numbering_subset_list));

                Nprocessor_wise_dof_ = processor_wise_dof_list.size();

                Nprogram_wise_dof_ = mpi.AllReduce(Nprocessor_wise_dof_, ::MoReFEM::Wrappers::MpiNS::Op::Sum);

                mpi.Barrier();

                const auto begin = processor_wise_dof_list.cbegin();
                const auto end = processor_wise_dof_list.cend();

                for (const auto& numbering_subset_ptr : numbering_subset_list)
                {
                    assert(!(!numbering_subset_ptr));
                    const auto& numbering_subset = *numbering_subset_ptr;

                    const auto Nprocessor_wise_dof_for_numbering_subset =
                        std::count_if(begin,
                                      end,
                                      [&numbering_subset](const auto& dof_ptr)
                                      {
                                          assert(!(!dof_ptr));
                                          return dof_ptr->IsInNumberingSubset(numbering_subset);
                                      });

                    auto Nprogram_wise_dof_for_numbering_subset =
                        mpi.AllReduce(Nprocessor_wise_dof_for_numbering_subset, ::MoReFEM::Wrappers::MpiNS::Op::Sum);

                    mpi.Barrier();

                    const auto numbering_subset_id = numbering_subset.GetUniqueId();

                    {
                        auto [it, check] = Nprocessor_wise_dof_per_numbering_subset_.insert(
                            { numbering_subset_id, Nprocessor_wise_dof_for_numbering_subset });

                        assert(check);
                        static_cast<void>(check);
                        static_cast<void>(it);
                    }

                    {
                        auto [it, check] = Nprogram_wise_dof_per_numbering_subset_.insert(
                            { numbering_subset_id, Nprogram_wise_dof_for_numbering_subset });

                        assert(check);
                        static_cast<void>(check);
                        static_cast<void>(it);
                    }
                }
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
