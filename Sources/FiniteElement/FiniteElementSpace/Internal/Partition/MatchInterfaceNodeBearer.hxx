/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 23 Dec 2013 11:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATCH_INTERFACE_NODE_BEARER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATCH_INTERFACE_NODE_BEARER_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Partition/Impl/InterfaceSpecialization.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<class OrientedInterfaceT>
            void MatchInterfaceNodeBearer ::CreateNodeBearerList(
                const GeometricElt& geom_elt,
                NodeBearer::vector_shared_ptr& node_bearer_list_for_current_god_of_dof,
                NodeBearer::vector_shared_ptr& node_bearer_list_for_current_local_felt_space,
                std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                    interface_node_bearer_list)
            {
                using HelperType = Impl::InterfaceSpecialization<OrientedInterfaceT>;

                const std::size_t Ninterface = HelperType::Ninterface(geom_elt);
                const auto& oriented_interface_list = HelperType::GetInterfaceList(geom_elt);
                assert(oriented_interface_list.size() == Ninterface);

                std::map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>
                    new_node_bearer_list; // key is the index of the Interface object.

                auto nature = OrientedInterfaceT::StaticNature();

                // Iterate through all interfaces, and create or retrieve the associated node.
                for (std::size_t i = 0ul; i < Ninterface; ++i)
                {
                    auto oriented_interface_ptr = oriented_interface_list[i];
                    assert(!(!oriented_interface_ptr));

                    OrientedInterfaceT& oriented_interface = *oriented_interface_ptr;

                    const auto interface_index = oriented_interface.GetProgramWiseIndex();

                    // Does the node on interface already exist? (might have been created by another finite elt).
                    auto it = interface_node_bearer_list.find(interface_index);

                    NodeBearer::shared_ptr node_bearer_ptr(nullptr);

                    const bool is_new_node_bearer = (it == interface_node_bearer_list.cend());

                    if (is_new_node_bearer)
                    {
                        node_bearer_ptr = HelperType::CreateNodeBearer(oriented_interface_ptr);
                        auto pair = std::make_pair(interface_index, node_bearer_ptr);
                        interface_node_bearer_list.insert(pair);
                        new_node_bearer_list.insert(pair);
                    } else
                        node_bearer_ptr = it->second;

                    // Add the current node bearer to the list of nodes bearers only if it isn't already there
                    // (from an unknown previously dealt with for instance).
                    // Node bearer index is not yet assigned; so we use to check whether the NodeBearer is already in
                    // the current finite element the interface (nature/index couple should be unique).
                    if (std::find_if(node_bearer_list_for_current_local_felt_space.cbegin(),
                                     node_bearer_list_for_current_local_felt_space.cend(),
                                     [interface_index, nature](const NodeBearer::shared_ptr& current_node_bearer_ptr)
                                     {
                                         assert(!(!current_node_bearer_ptr));

                                         auto current_nature = current_node_bearer_ptr->GetNature();

                                         return current_nature == nature
                                                && current_node_bearer_ptr->GetInterface().GetProgramWiseIndex()
                                                       == interface_index;
                                     })
                        == node_bearer_list_for_current_local_felt_space.cend())
                    {
                        node_bearer_list_for_current_local_felt_space.push_back(node_bearer_ptr);
                    }
                }

                // Update node_bearer_list_ and set a temporary NodeBearer index (it is changed after partitioning).
                ::MoReFEM::NodeBearerNS::program_wise_index_type program_wise_index{
                    node_bearer_list_for_current_god_of_dof.size()
                };

                for (const auto& pair : new_node_bearer_list)
                {
                    auto& node_bearer_ptr = pair.second;
                    node_bearer_ptr->SetProgramWiseIndex(program_wise_index++);
                    node_bearer_list_for_current_god_of_dof.push_back(node_bearer_ptr);
                }
            }


            template<class OrientedInterfaceT>
            void MatchInterfaceNodeBearer ::AddInterfaceNodeList(
                const GeometricElt& geom_elt,
                const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                    interface_node_bearer_list,
                std::map<LocalNodeNS::index_type, Node::shared_ptr>& node_for_current_finite_element)
            {
                using HelperType = Impl::InterfaceSpecialization<OrientedInterfaceT>;

                const std::size_t Ninterface = HelperType::Ninterface(geom_elt);
                const auto& oriented_interface_list = HelperType::GetInterfaceList(geom_elt);
                assert(oriented_interface_list.size() == Ninterface);

                const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

                const auto& extended_unknown = ref_felt.GetExtendedUnknown();
                const auto& unknown = extended_unknown.GetUnknown();
                decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();
                const auto& numbering_subset_ptr = extended_unknown.GetNumberingSubsetPtr();

                const auto Ncomponent = ref_felt.Ncomponent();

                std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> interface_index_list;
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> coords_index_list;

                for (const auto& interface : oriented_interface_list)
                {
                    interface_index_list.push_back(interface->GetProgramWiseIndex());
                    coords_index_list.push_back(interface->GetVertexCoordsList().back()->GetIndexFromMeshFile());
                }


                // Iterate through all interfaces, and create or retrieve the associated node.
                for (std::size_t i = 0ul; i < Ninterface; ++i)
                {
                    auto oriented_interface_ptr = oriented_interface_list[i];
                    assert(!(!oriented_interface_ptr));

                    OrientedInterfaceT& oriented_interface = *oriented_interface_ptr;

                    typename HelperType::NodeListType local_node_on_interface_list =
                        HelperType::GetNodeList(basic_ref_felt, oriented_interface, i);

                    assert(!local_node_on_interface_list.empty());

                    const ::MoReFEM::InterfaceNS::program_wise_index_type interface_index =
                        oriented_interface.GetProgramWiseIndex();

                    // Does the node on interface already exist? (might have been created by another finite elt).
                    auto it = interface_node_bearer_list.find(interface_index);

                    assert(it != interface_node_bearer_list.cend());

                    auto& node_bearer_ptr = it->second;

                    // Check whether the node list exists within the node bearer, and if not create it.
                    auto&& node_list = node_bearer_ptr->GetNodeList(unknown, shape_function_label);

                    if (node_list.empty())
                    {
                        CreateNodeList(local_node_on_interface_list,
                                       extended_unknown,
                                       Ncomponent,
                                       *node_bearer_ptr,
                                       node_for_current_finite_element);
                    } else
                    {
                        // Case in which the Node already exist; just report properly its instances
                        // to the current finite element.
                        const std::size_t Nnode = node_list.size();

                        assert(node_list.size() == local_node_on_interface_list.size());

                        for (std::size_t j = 0ul; j < Nnode; ++j)
                        {
                            const auto& local_node_ptr = local_node_on_interface_list[j];
                            assert(!(!local_node_ptr));
                            auto& node_ptr = node_list[j];
                            assert(!(!node_ptr));
                            node_ptr->RegisterNumberingSubset(numbering_subset_ptr);

                            node_for_current_finite_element.insert({ local_node_ptr->GetIndex(), node_ptr });
                        }
                    }
                }
            }


            template<class LocalNodeOnInterfaceT>
            void MatchInterfaceNodeBearer ::CreateNodeList(
                const LocalNodeOnInterfaceT& local_node_on_interface_list,
                const ExtendedUnknown& extended_unknown,
                const Advanced::ComponentNS::index_type Ncomponent,
                NodeBearer& node_bearer,
                std::map<LocalNodeNS::index_type, Node::shared_ptr>& node_for_current_finite_element)
            {
                for (const auto& local_node_ptr : local_node_on_interface_list)
                {
                    auto&& node_ptr = node_bearer.AddNode(extended_unknown, Ncomponent.Get());

                    node_for_current_finite_element.insert({ local_node_ptr->GetIndex(), std::move(node_ptr) });
                }
            }


            inline const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
            MatchInterfaceNodeBearer::GetVertexNodeBearerList() const noexcept
            {
                return vertex_node_bearer_list_;
            }


            inline const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
            MatchInterfaceNodeBearer::GetEdgeNodeBearerList() const noexcept
            {
                return edge_node_bearer_list_;
            }


            inline const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
            MatchInterfaceNodeBearer::GetFaceNodeBearerList() const noexcept
            {
                return face_node_bearer_list_;
            }


            inline const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
            MatchInterfaceNodeBearer::GetVolumeNodeBearerList() const noexcept
            {
                return volume_node_bearer_list_;
            }


            // clang-format off
            inline const std::unordered_map
            <
                GeometricElt::shared_ptr,
                NodeBearer::vector_shared_ptr
            >&
            MatchInterfaceNodeBearer::GetBoundaryConditionsData() const noexcept
            // clang-format on
            {
                return boundary_conditions_data_;
            }


            inline const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type&
            MatchInterfaceNodeBearer::GetCoordsMatchingData() const noexcept
            {
                return coords_matching_data_;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATCH_INTERFACE_NODE_BEARER_HXX_
