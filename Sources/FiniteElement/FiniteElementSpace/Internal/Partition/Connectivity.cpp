/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{

    namespace // anonymous
    {


        /*!
         * \brief Fill the \a connectivity with the informations from a given \a felt_space.
         *
         * \param node_bearer_filter  A functor (or lambda) which indicates which \a NodeBearer should be kept in \a connectivity.
         * The most generic case is to keep \a NodeBearer on the current processor, but for the sake of
         * \a CoordsMatching I sometime needs another rule.
         */
        template<class FunctorT>
        void ComputeNodeBearerConnectivityInFEltSpace(const FEltSpace& felt_space,
                                                      connectivity_type& connectivity,
                                                      FunctorT&& node_bearer_filter);

        /*!
         * \brief Helper function of ComputeNodeBearerConnectivityInFEltSpace which tracks all the key \a NodeBearer.
         *
         * To fill the \a connectivity space, we need to iterate through the \a LocalFEltSpace to identify which values
         * must be connected to a given key. However, there is a difficulty as some of these \a LocalFEltSpace might be
         * handled by another rank. So this helper function is called twice: once for processor-wise \a LocalFEltSpace
         * and another one for the ghost ones.
         *
         * \param node_bearer_filter  A functor (or lambda) which indicates which \a NodeBearer should be kept in \a connectivity.
         * The most generic case is to keep \a NodeBearer on the current processor, but for the sake of
         * \a CoordsMatching I sometime needs another rule.
         */
        template<RoleOnProcessor RoleOnProcessorT, class FunctorT>
        void IterateThroughLocalFEltSpace(const FEltSpace& felt_space,
                                          connectivity_type& connectivity,
                                          FunctorT&& node_bearer_filter);


        /*!
         * \brief Sort and eliminate duplicates.
         *
         * \param[in,out] connectivity Key is the \a NodeBearer for which we track connectivity, value the list of
         * connected \a NodeBearer. In output, values are sort (according to \a NodeBearer ordering relationship) and
         * duplicates are removed.
         * \param[in] do_keep_self_connexion If 'yes', the key \a NodeBearer is kept in the
         * list. We need to be able to provide both version to comply with interface of third party libraries.
         */
        void CleanUpConnectivity(connectivity_type& connectivity, KeepSelfConnexion do_keep_self_connexion);


    } // namespace


    connectivity_type ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                                    const std::size_t Nprocessor_wise_node_bearer,
                                                    KeepSelfConnexion do_keep_self_connexion)
    {
        connectivity_type connectivity;
        connectivity.max_load_factor(Utilities::DefaultMaxLoadFactor());
        connectivity.reserve(Nprocessor_wise_node_bearer);

        decltype(auto) any_felt_space = felt_space_list.back();
        assert(!(!any_felt_space));
        const auto mpi_rank = any_felt_space->GetMpi().GetRank<std::size_t>();

        auto is_on_local_processor = [mpi_rank](const NodeBearer::shared_ptr& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            return node_bearer_ptr->GetProcessor() == mpi_rank;
        };

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            ComputeNodeBearerConnectivityInFEltSpace(*felt_space_ptr, connectivity, is_on_local_processor);
        }

        // Reorder properly elements and get rid of duplicates.
        CleanUpConnectivity(connectivity, do_keep_self_connexion);

        assert("In both occasion this function is called, we are in measure to provide the exact size "
               "of the connectivity, thus avoiding possibly expensive rehashing calls."
               && connectivity.size() == Nprocessor_wise_node_bearer);

        return connectivity;
    }


    NodeBearer::vector_shared_ptr ComputeConnectedNodeBearers(
        const FEltSpace::vector_unique_ptr& felt_space_list,
        const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& node_bearer_list_to_keep)
    {
        connectivity_type connectivity;
        connectivity.max_load_factor(Utilities::DefaultMaxLoadFactor());
        connectivity.reserve(node_bearer_list_to_keep.size());

        auto node_bearer_filter = [&node_bearer_list_to_keep](const NodeBearer::shared_ptr& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;

            for (const auto& [coords_index_list, node_bearer_to_keep_ptr] : node_bearer_list_to_keep)
            {
                assert(!(!node_bearer_to_keep_ptr));
                if (*node_bearer_to_keep_ptr == node_bearer)
                    return true;
            }

            return false;
        };

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            ComputeNodeBearerConnectivityInFEltSpace(*felt_space_ptr, connectivity, node_bearer_filter);
        }

        assert(connectivity.size() == node_bearer_list_to_keep.size());

        NodeBearer::vector_shared_ptr ret;

        for (const auto& [node_bearer_ptr, connected_node_bearer_list] : connectivity)
        {
            ret.push_back(node_bearer_ptr);

            for (const auto& connected_node_bearer_ptr : connected_node_bearer_list)
            {
                assert(!(!connected_node_bearer_ptr));
                ret.push_back(connected_node_bearer_ptr);
            }
        }

        Utilities::EliminateDuplicate(ret,
                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

        return ret;
    }


    namespace // anonymous
    {


        template<RoleOnProcessor RoleOnProcessorT, class FunctorT>
        void IterateThroughLocalFEltSpace(const FEltSpace& felt_space,
                                          connectivity_type& connectivity,
                                          FunctorT&& node_bearer_filter)

        {
            decltype(auto) felt_list_per_type = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>();

            for (const auto& [ref_local_felt_space_ptr, local_felt_space_list_per_geom_index] : felt_list_per_type)
            {
                static_cast<void>(ref_local_felt_space_ptr);

                for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_list_per_geom_index)
                {
                    static_cast<void>(geom_elt_index);
                    assert(!(!local_felt_space_ptr));
                    const auto& local_felt_space = *local_felt_space_ptr;

                    const auto& node_bearers_in_current_local_felt_space = local_felt_space.GetNodeBearerList();

                    assert(local_felt_space.GetGeometricElt().GetIndex() == geom_elt_index);

                    for (const auto& node_bearer_ptr : node_bearers_in_current_local_felt_space)
                    {
                        if (!node_bearer_filter(node_bearer_ptr))
                            continue;

                        // Use of C++ 17 "structure binding".
                        // The following code works whether the \a node_bearer_ptr is already in the
                        // unordered_map or not.
                        auto [it, is_new_element] = connectivity.insert({ node_bearer_ptr, {} });
                        static_cast<void>(is_new_element);
                        auto& second_member = it->second;

                        std::copy(node_bearers_in_current_local_felt_space.cbegin(),
                                  node_bearers_in_current_local_felt_space.cend(),
                                  std::back_inserter(second_member));
                    }
                }
            }
        }


        template<class FunctorT>
        void ComputeNodeBearerConnectivityInFEltSpace(const FEltSpace& felt_space,
                                                      connectivity_type& connectivity,
                                                      FunctorT&& node_bearer_filter)
        {
            // Iterate through all node_bearers and determine the list of connected node_bearers.
            // In this first approach, a same node_bearer might be listed twice in the list.
            {
                IterateThroughLocalFEltSpace<RoleOnProcessor::processor_wise>(
                    felt_space, connectivity, node_bearer_filter);

                IterateThroughLocalFEltSpace<RoleOnProcessor::ghost>(felt_space, connectivity, node_bearer_filter);
            }
        }


        void CleanUpConnectivity(connectivity_type& connectivity, KeepSelfConnexion do_keep_self_connexion)
        {
            for (auto& item : connectivity)
            {
                auto& second_member = item.second;

                // There is in all likelihood duplicates in second member; remove them!
                Utilities::EliminateDuplicate(second_member,
                                              Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

                if (do_keep_self_connexion == KeepSelfConnexion::no)
                {
                    // Remove also self-connexion (harmful for instance if fed to Parmetis library, and trivial to
                    // add back anyway if you need it somewhere).
                    auto first_member = item.first;

                    auto it = std::find(second_member.begin(), second_member.end(), first_member);

                    if (it != second_member.end())
                        second_member.erase(it);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup
