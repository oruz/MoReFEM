/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_PARTITION_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_PARTITION_HPP_

#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Compute the required elements to perform the partition; reduction is NOT DONE HERE!
             *
             * \param[in,out] node_bearer_list Node bearer list is still the same length in output (reduction not yet
             * done) but its elements are sort differently: those to be on first processor comes first, then those on
             * second one, and so on... Program-wise numbering is also applied: each of them gets as program-wise index
             * its position in the new vector.
             * \copydetails doxygen_hide_mpi_param
             * \param[in] felt_space_list List of
             * finite element spaces in the god of dof that called present function.
             */
            void PreparePartition(const ::MoReFEM::Wrappers::Mpi& mpi,
                                  const FEltSpace::vector_unique_ptr& felt_space_list,
                                  NodeBearer::vector_shared_ptr& node_bearer_list);


            /*!
             * \brief Reduce the node bearers to the processor-wise values in the list.
             *
             * \internal <b><tt>[internal]</tt></b>  NodeBearer objects aren't destroyed at this stage: they are still
             * needed. The ones not useful on current processor will be destroyed shortly after this call, before the
             * end of GodOfDof::Init().
             * \endinternal
             *
             * \param[in] mpi_rank Rank of the current processor.
             * \param[in,out] node_bearer_list In input, the full list of node bearers. In output, the processor-wise
             * list of node bearers.
             */
            void ReduceNodeBearerList(std::size_t mpi_rank, NodeBearer::vector_shared_ptr& node_bearer_list);


            /*!
             * \brief Give to each processor-wise \a NodeBearer the list of processors which ghost them.
             *
             * \copydoc doxygen_hide_mpi_param
             * \param[in,out] processor_wise_node_bearer_list List of processor-wise \a NodeBearer. This is also an
             * output parameter as these objects may have their content modified: the number of processors that ghost
             * them may be added (it is the only change)
             * \param[in] ghost_node_bearer_list List of ghost \a NodeBearer,
             * which will be shared with the other ranks.
             */
            void BroadcastGhostNodeBearerIndexList(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                   const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                                   const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/Partition/Partition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_PARTITION_HPP_
