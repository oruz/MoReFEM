/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Nov 2014 17:24:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_REDUCE_TO_PROCESSOR_WISE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_REDUCE_TO_PROCESSOR_WISE_HPP_


#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
//#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::Internal::FEltSpaceNS { class AssignGeomEltToProcessor; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Reduce to processor-wise most data (finite elements in each finite element space, geometric
             * mesh region) and computes the list og ghost node bearers.
             *
             * \attention the list of processor-wise node bearers is not computed here because it already was
             * when the pattern of the global matrix was computed.
             *
             * \internal <b><tt>[internal]</tt></b> The only reason there is a class that encompass the two static
             * methods is privilege access: I wanted to forbid the public call of
             * FEltStorage::GetNonCstLocalFEltSpacePerRefLocalFEltSpace(). However
             * such a data was required; so FEltStorage holds a friendship to present class to circumvent that.
             * \endinternal
             */
            struct ReduceToProcessorWise
            {


                /*!
                 * \brief Static method that performs the actual work.
                 *
                 * The enclosing class is there just to provide a simple object to befriend!
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] assign_geom_elt_to_processor The object which was used to determine which rank is in
                 * charge of each \a GeometricElt. \param[in,out] felt_space_list The list of finite elt spaces to
                 * reduce to processor-wise data. \param[in,out] mesh In input the mesh as read initially. In output,
                 * the reduced mesh that includes only GeometricElts on the local processor. \param[in]
                 * processor_wise_node_bearer_list The list of processor-wise node bearers, computed previously.
                 * \param[in,out] ghost_node_bearer_list The list of ghost node bearers, i.e. the ones that were in
                 * processor-wise finite elements but not in processor-wise node bearer list. It can also be an input
                 * parameter: if several finite element spaces are involved the same list us kept for all of them.
                 * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the
                 * initialisation phase.
                 */
                static void Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                                    const FEltSpace::vector_unique_ptr& felt_space_list,
                                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                    Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
                                    NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                    Mesh& mesh);
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_REDUCE_TO_PROCESSOR_WISE_HPP_
