### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AssignGeomEltToProcessor.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MatchInterfaceNodeBearer.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Partition.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/ReduceToProcessorWise.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AssignGeomEltToProcessor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MatchInterfaceNodeBearer.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MatchInterfaceNodeBearer.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Partition.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Partition.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ReduceToProcessorWise.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
