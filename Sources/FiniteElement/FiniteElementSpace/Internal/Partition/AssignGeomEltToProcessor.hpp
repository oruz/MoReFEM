/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 May 2014 15:59:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_ASSIGN_GEOM_ELT_TO_PROCESSOR_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_ASSIGN_GEOM_ELT_TO_PROCESSOR_HPP_

#include <cstddef> // IWYU pragma: keep
#include <optional>
#include <set>
#include <unordered_map>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;
    class LocalFEltSpace;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Object which holds the final choice for the rank that will bear a \a LocalFEltSpace and the other
             * processors that nust keep a ghost track of it.
             */
            struct ProcessorData
            {

                //! The rank which will handle mainly the \a GeomElt.
                std::size_t rank;

                //! Other processors that may need data from the \a LocalFEltSpace (i.e. that manages part of the \a
                //! NodeBearer of the \a LocalFEltSpace ).
                std::set<std::size_t> ghost_rank_list;
            };


            /*!
             * \brief Helper class that decides which processor(s) are concerned for each \a GeometricElt .
             *
             * Or \a LocalFEltSpace - there is a bijection between those).
             *
             * There is one such object per \a Mesh, created locally just for the reduction phase.
             */
            class AssignGeomEltToProcessor final
            {
              public:
                /*!
                 * \brief Constructor.
                 *
                 * \param[in] god_of_dof \a GodOfDof which \a LocalFEltSpace  will be assigned to ranks.
                 * \param[in] Nprocessor Number of processor in the mpi scheme.
                 * \param[in] match_interface_node_bearer Helper object in \a GodOfDof initialization phase.
                 */
                explicit AssignGeomEltToProcessor(const GodOfDof& god_of_dof,
                                                  std::size_t Nprocessor,
                                                  MatchInterfaceNodeBearer& match_interface_node_bearer);

                //! Destructor.
                ~AssignGeomEltToProcessor() = default;

                //! \copydoc doxygen_hide_copy_constructor
                AssignGeomEltToProcessor(const AssignGeomEltToProcessor& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                AssignGeomEltToProcessor(AssignGeomEltToProcessor&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                AssignGeomEltToProcessor& operator=(const AssignGeomEltToProcessor& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                AssignGeomEltToProcessor& operator=(AssignGeomEltToProcessor&& rhs) = delete;

                //! Number of finite elements on \a processor.
                //! \param[in] processor Processor for which the tally is done.
                std::size_t NlocalFEltSpace(std::size_t processor) const;


                /*!
                 * \brief Get the \a ProcessorData related to the given \a local_felt_space.
                 *
                 * \internal In fact under the hood the namesalke method for \a geom_elt will be called.
                 *
                 * \param[in] local_felt_space The \a LocalFEltSpace considered; its underlying \a GeometricElt will be
                 * used.
                 *
                 * \return The sought \a ProcessorData.
                 */
                const ProcessorData& GetProcessorData(const LocalFEltSpace& local_felt_space) const;

                /*!
                 * \brief Get the \a ProcessorData related to the given \a geom_elt.
                 *
                 * \param[in] geom_elt \a GeometricElt which \a ProcessortData information we seek.
                 *
                 * \attention Remember this class is internal and should be used if you know exactly what you're doing.
                 * It won't handle well (except in debug mode through an assert) an attempt to run it  on an invalid \a
                 * geom_elt - typically in MoReFEM we keep after reduction only the \a GeomElt that are of use for the
                 * finite element work - so some \a GeometricElt from the original mesh might not be contained inside.
                 *
                 * \return The sought \a ProcessorData.
                 */
                const ProcessorData& GetProcessorData(const GeometricElt& geom_elt) const;

                //! Convenient alias.
                using processor_for_each_geom_elt_iterator = std::unordered_map<std::size_t, ProcessorData>::iterator;

                /*!
                 * \brief Tells whether a given \a GeometricElt is on current processor or not.
                 *
                 * \internal This is intended to be used once the object is fully built.
                 *
                 * \copydoc doxygen_hide_mpi_param
                 * \param[in] geom_elt \a GeometricElt under investigation.
                 *
                 * \return True if \a geom_elt is handled by current rank (processor-wise only - ghost \a GeometricElt
                 * should return false).
                 */
                bool IsProcessorWise(const ::MoReFEM::Wrappers::Mpi& mpi, const GeometricElt& geom_elt) const;

                /*!
                 * \brief Tells whether a given \a GeometricElt is a ghost on current processor or not.
                 *
                 * \internal This is intended to be used once the object is fully built.
                 *
                 * \copydoc doxygen_hide_mpi_param
                 * \param[in] geom_elt \a GeometricElt under investigation.
                 *
                 * \return True if \a geom_elt is handled as a ghost by current rank.
                 */
                bool IsGhost(const ::MoReFEM::Wrappers::Mpi& mpi, const GeometricElt& geom_elt) const;


              private:
                /*!
                 * \brief Determine which processor takes the current finite element group.
                 *
                 * \param[in] local_felt_space Finite element group considered.
                 *
                 * The assignment rule is the following:
                 * - If there is a Volumic node bearer in the LocalFEltSpace, take its processor.
                 * - If not, assign the finite element group to the processor in charge of most of its nodes.
                 * - In case there is a tie, choose the processor with the most ghost nodes at this stage.
                 */
                void AssignProcessorForCurrentLocalFEltSpace(const LocalFEltSpace& local_felt_space);

                /*!
                 * \brief Return the number of node bearers on each processor.
                 *
                 * \param[in] node_bearer_list List of node bearers to consider.
                 * \return How \a node_list is partitionned among processors. Index of this vector
                 * is the processor involved, the value the number of nodes from \a node_list that
                 * are handled by said processor.
                 */
                std::vector<std::size_t>
                NnodeBearerPerProcessor(const NodeBearer::vector_shared_ptr& node_bearer_list) const;

                //! Returns the number of processors.
                std::size_t Nprocessor() const;

                /*!
                 * \brief Check whether the choice of processor for the given \a local_felt_space has already be done.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace under consideration.
                 *
                 * \return If so, returns the iterator to the correct element in storage.
                 *
                 * \internal Not const as I want a non constant iterator as return value.
                 */
                std::optional<processor_for_each_geom_elt_iterator>
                IsAlreadyAttributed(const LocalFEltSpace& local_felt_space) noexcept;


                /*!
                 * \brief Returns the choice of processor in the case in which there is a \a NodeBearer on a volume.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace under consideration.
                 *
                 * \return If true, return the rank of the processor which holds this \a NodeBearer. If not, return
                 * std::nullopt.
                 */
                std::optional<std::size_t> IsVolumicNodeBearer(const LocalFEltSpace& local_felt_space) const noexcept;


                /*!
                 * \brief Add to the internal container the data related to current \a LocalFEltSpace - which was not
                 * present so far.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace under consideration.
                 * \param[in] rank
                 */
                void NewLocalFEltSpace(const LocalFEltSpace& local_felt_space, std::size_t rank);


                /*!
                 * \brief Update an existing \a ProcessorData object by (possibly) adding the ghost ranks required
                 * by the \a local_felt_space in the \a FEltSpace currently considered.
                 *
                 * If this method is called, it means ANOTHER \a FEltSpace already dealt with the current
                 * underlying \a GeomElt - but this doesn't mean this other \a FEltSpace made the same decisions
                 * regarding repartition.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace under consideration.
                 * \param[in] iterator Iterator to the data already associated to the underlying \a GeometricElt.
                 * These data may be updated by current method.
                 */
                void Update(const LocalFEltSpace& local_felt_space, processor_for_each_geom_elt_iterator iterator);


                /*!
                 * \brief Determine the main rank for a \a LocalFEltSpace in a non-trivial case.
                 *
                 * \param[in] node_bearer_list List of \a NodeBearer associated to a specific \a LocalFEltSpace.
                 *
                 * \return Rank to which the \a LocalFEltSpace is assigned.
                 */
                std::size_t DetermineMainRank(const NodeBearer::vector_shared_ptr& node_bearer_list) const;

                /*!
                 * \brief Determine the main rank for a \a LocalFEltSpace in a non-trivial case.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace which rank is sought.
                 *
                 * \return Rank to which the \a LocalFEltSpace is assigned.
                 */
                std::size_t DetermineMainRank(const LocalFEltSpace& local_felt_space) const;


                /*!
                 * \brief Cover the very specific case of \a GeometricElt boundary conditions, that might not be
                 * considered yet in general case.
                 *
                 * \param[in] match_interface_node_bearer Helper object in \a GodOfDof initialization phase.
                 */
                void AssignBoundaryConditionGeometricElts(const MatchInterfaceNodeBearer& match_interface_node_bearer);

                /*!
                 * \brief Cover the very specific case of \a FromCoordsMatching operator, which requires to store supplementary (ghost) \a NodeBearer
                 * and \a GeometricElt.
                 *
                 * \param[in] mesh \a Mesh that will have to store some supplementary \a GeometricElt.
                 * \param[in] match_interface_node_bearer Helper object in \a GodOfDof initialization phase.
                 */
                void AssignFromCoordsMatchingGeometricElts(const Mesh& mesh,
                                                           const MatchInterfaceNodeBearer& match_interface_node_bearer);


                /*!
                 * \brief Determine on which ranks a special \a geom_elt is involved.
                 *
                 * This method is not meant to be called for "regular" \a GeometricElt (i.e. those that were assigned
                 * directly from the specifications of the partitioning): it concerns some \a GeometricElt that would
                 * not be kept on current processor from this choice but are nonetheless needed for some reason on the
                 * current processor.
                 *
                 * Two such cases do exist at the time this comment is written:
                 * - \a GeometricElt which is kept because one \a NodeBearer related to the \a LocalFEltSpace
                 * counterpart must be kept on the processor.
                 * - \a GeometricElt which is kept because one \a NodeBearer related to the \a LocalFEltSpace
                 * counterpart is required by a \a FromCoordsMatching operator.
                 *
                 * \param[in] geom_elt \a GeometricElt for which the computation is performed.
                 * \param[in] geom_elt_node_bearer_list List of \a NodeBearer that are related to the \a GeometricElt.
                 */
                void
                DetermineRanksForSpecialGeometricElt(const GeometricElt& geom_elt,
                                                     const NodeBearer::vector_shared_ptr& geom_elt_node_bearer_list);

              private:
                //! How the finite element groups managed so far are shared among processors.
                std::vector<std::size_t> Nlocal_felt_space_per_processor_;


                /*!
                 * \brief Processor chosen for each geometric element (and its related local felt space).
                 *
                 * This container is useful to ensure a given geometric element is truly handled by the same processor
                 * (without it each finite element space could yield a different decision).
                 *
                 * Key is the index of the \a GeometricElt.
                 * Value is the related information: the rank that primarily handles the \a GeometricElt and the other
                 * ranks that have to ghost it.
                 */
                std::unordered_map<std::size_t, ProcessorData> processor_for_each_geom_elt_;
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_ASSIGN_GEOM_ELT_TO_PROCESSOR_HPP_
