/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 23 Dec 2013 11:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <map>
#include <memory>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hpp"
#include "FiniteElement/FiniteElement/Internal/FElt.hpp"
#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class DirichletBoundaryCondition; }
namespace MoReFEM { class Mesh; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            MatchInterfaceNodeBearer::MatchInterfaceNodeBearer()
            {
                vertex_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                edge_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                face_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                volume_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }


            void MatchInterfaceNodeBearer::AddNodeBearerList(
                const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                LocalFEltSpace& local_felt_space)
            {
                NodeBearer::vector_shared_ptr node_bearer_for_current_local_felt_space;

                const auto& ref_felt_list = ref_felt_space.GetRefFEltList();

                const auto& geom_elt = local_felt_space.GetGeometricElt();

                for (const auto& ref_felt_ptr : ref_felt_list)
                {
                    assert(!(!ref_felt_ptr));
                    const auto& ref_felt = *ref_felt_ptr;

                    local_felt_space.AddFElt(ref_felt); // \todo #1571 Should be done in an explicit method!!!

                    const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

                    if (basic_ref_felt.AreNodesOnVertices())
                    {
                        CreateNodeBearerList<Vertex>(geom_elt,
                                                     node_bearer_for_current_god_of_dof,
                                                     node_bearer_for_current_local_felt_space,
                                                     vertex_node_bearer_list_);
                    }

                    // Same for edges.
                    if (basic_ref_felt.AreNodesOnEdges())
                    {
                        CreateNodeBearerList<OrientedEdge>(geom_elt,
                                                           node_bearer_for_current_god_of_dof,
                                                           node_bearer_for_current_local_felt_space,
                                                           edge_node_bearer_list_);
                    }

                    // Same for faces.
                    if (basic_ref_felt.AreNodesOnFaces())
                    {
                        CreateNodeBearerList<OrientedFace>(geom_elt,
                                                           node_bearer_for_current_god_of_dof,
                                                           node_bearer_for_current_local_felt_space,
                                                           face_node_bearer_list_);
                    }

                    // Same for volume.
                    if (basic_ref_felt.AreNodesOnVolume())
                    {
                        CreateNodeBearerList<Volume>(geom_elt,
                                                     node_bearer_for_current_god_of_dof,
                                                     node_bearer_for_current_local_felt_space,
                                                     volume_node_bearer_list_);
                    }

                } // for (auto ref_felt_ptr : ref_felt_list)

                assert(!node_bearer_for_current_local_felt_space.empty());
                local_felt_space.SetNodeBearerList(std::move(node_bearer_for_current_local_felt_space));
            }


            void MatchInterfaceNodeBearer::ComputeNodeList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                                           LocalFEltSpace& local_felt_space)
            {
                const auto& ref_felt_list = ref_felt_space.GetRefFEltList();

                const auto& geom_elt = local_felt_space.GetGeometricElt();

                for (const auto& ref_felt_ptr : ref_felt_list)
                {
                    assert(!(!ref_felt_ptr));
                    const auto& ref_felt = *ref_felt_ptr;

                    auto& felt = local_felt_space.GetNonCstFElt(ref_felt.GetExtendedUnknown());

                    const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

                    // Key is local node index.
                    std::map<LocalNodeNS::index_type, Node::shared_ptr> node_for_current_finite_element;

                    if (basic_ref_felt.AreNodesOnVertices())
                    {
                        AddInterfaceNodeList<Vertex>(
                            geom_elt, ref_felt, GetVertexNodeBearerList(), node_for_current_finite_element);
                    }

                    // Same for edges.
                    if (basic_ref_felt.AreNodesOnEdges())
                    {
                        AddInterfaceNodeList<OrientedEdge>(
                            geom_elt, ref_felt, GetEdgeNodeBearerList(), node_for_current_finite_element);
                    }

                    // Same for faces.
                    if (basic_ref_felt.AreNodesOnFaces())
                    {
                        AddInterfaceNodeList<OrientedFace>(
                            geom_elt, ref_felt, GetFaceNodeBearerList(), node_for_current_finite_element);
                    }

                    // Same for volume.
                    if (basic_ref_felt.AreNodesOnVolume())
                    {
                        AddInterfaceNodeList<Volume>(
                            geom_elt, ref_felt, GetVolumeNodeBearerList(), node_for_current_finite_element);
                    }

                    assert(node_for_current_finite_element.size() == basic_ref_felt.NlocalNode());

                    for (const auto& pair : node_for_current_finite_element)
                        felt.AddNode(pair.second);

                } // for (auto ref_felt_ptr : ref_felt_list)
            }


            namespace // anonymous
            {


                template<class OrientedInterfaceContainerT>
                void AddNodeBearerOnBoundaryCondition(
                    const OrientedInterfaceContainerT& interface_list,
                    const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                        node_bearer_on_interface_type_list,
                    NodeBearer::vector_shared_ptr& node_bearer_list);


            } // namespace


            NodeBearer::vector_shared_ptr MatchInterfaceNodeBearer ::ComputeNodeBearerListOnBoundary(
                const Mesh& mesh,
                const DirichletBoundaryCondition& boundary_condition,
                std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr>& bc_data) const
            {
                const auto geom_elt_list =
                    Internal::BoundaryConditionNS::ComputeGeometricEltList(mesh, boundary_condition);

                NodeBearer::vector_shared_ptr ret;

                for (const auto& geom_elt_ptr : geom_elt_list)
                {
                    assert(!(!geom_elt_ptr));
                    const auto& geom_elt = *geom_elt_ptr;

                    NodeBearer::vector_shared_ptr node_bearer_list_for_current_geom_elt;

                    AddNodeBearerOnBoundaryCondition(
                        geom_elt.GetVertexList(), GetVertexNodeBearerList(), node_bearer_list_for_current_geom_elt);

                    if (!edge_node_bearer_list_.empty())
                    {
                        AddNodeBearerOnBoundaryCondition(geom_elt.GetOrientedEdgeList(),
                                                         GetEdgeNodeBearerList(),
                                                         node_bearer_list_for_current_geom_elt);
                    }

                    if (!face_node_bearer_list_.empty())
                    {
                        AddNodeBearerOnBoundaryCondition(geom_elt.GetOrientedFaceList(),
                                                         GetFaceNodeBearerList(),
                                                         node_bearer_list_for_current_geom_elt);
                    }

                    const auto [it, is_properly_inserted] =
                        bc_data.insert({ geom_elt_ptr, node_bearer_list_for_current_geom_elt });
                    assert(is_properly_inserted);
                    static_cast<void>(is_properly_inserted);

                    std::copy(node_bearer_list_for_current_geom_elt.cbegin(),
                              node_bearer_list_for_current_geom_elt.cend(),
                              std::back_inserter(ret));
                }

                Utilities::EliminateDuplicate(ret,
                                              Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());
                return ret;
            }


            namespace // anonymous
            {


                /*!
                 * \brief Remove from the NodeBearer list those that are neither processor-wise nor ghost.
                 *
                 * \param[in] processor_wise_node_bearer_list The list of processor-wise \a NodeBearer for the
                 * considered \a GodOfDof. \param[in] ghost_node_bearer_list The list of ghost\a NodeBearer for the
                 * considered \a GodOfDof. \param[in,out] node_bearer_per_interface_list The list of \a NodeBearer per
                 * interface, which will be reduced to only relevant ones.
                 */
                void FilterOutNonRelevantNodeBearers(
                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                    std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                        node_bearer_per_interface_list);


            } // namespace


            void MatchInterfaceNodeBearer::Reduce(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                                  const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
            {
                assert(std::is_sorted(processor_wise_node_bearer_list.cbegin(),
                                      processor_wise_node_bearer_list.cend(),
                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));

                assert(std::is_sorted(ghost_node_bearer_list.cbegin(),
                                      ghost_node_bearer_list.cend(),
                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));

                FilterOutNonRelevantNodeBearers(
                    processor_wise_node_bearer_list, ghost_node_bearer_list, vertex_node_bearer_list_);
                FilterOutNonRelevantNodeBearers(
                    processor_wise_node_bearer_list, ghost_node_bearer_list, edge_node_bearer_list_);
                FilterOutNonRelevantNodeBearers(
                    processor_wise_node_bearer_list, ghost_node_bearer_list, face_node_bearer_list_);
                FilterOutNonRelevantNodeBearers(
                    processor_wise_node_bearer_list, ghost_node_bearer_list, volume_node_bearer_list_);
            }


            void MatchInterfaceNodeBearer::SetBoundaryConditionData(
                std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr>&& bc_data)
            {
                assert(boundary_conditions_data_.empty());
                boundary_conditions_data_ = std::move(bc_data);
            }


            void MatchInterfaceNodeBearer::SetFromCoordsMatchingNodeBearers(
                const ::MoReFEM::Wrappers::Mpi& mpi,
                Internal::NodeBearerNS::node_bearer_per_coords_index_list_type&& value)
            {
                coords_matching_data_ = std::move(value);

                const auto mpi_rank = mpi.GetRank<std::size_t>();

                for (auto& [coords_list, node_bearer_ptr] : coords_matching_data_)
                {
                    assert(!(!node_bearer_ptr));
                    node_bearer_ptr->SetGhost(mpi_rank);
                }
            }


            namespace // anonymous
            {


                void FilterOutNonRelevantNodeBearers(
                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                    std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                        node_bearer_per_interface_list)
                {
                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> indexes_to_delete;
                    indexes_to_delete.reserve(node_bearer_per_interface_list.size());

                    const auto begin_proc = processor_wise_node_bearer_list.cbegin();
                    const auto end_proc = processor_wise_node_bearer_list.cend();

                    const auto begin_ghost = ghost_node_bearer_list.cbegin();
                    const auto end_ghost = ghost_node_bearer_list.cend();

                    for (const auto& [interface_index, node_bearer_ptr] : node_bearer_per_interface_list)
                    {
                        assert(!(!node_bearer_ptr));

                        if (std::binary_search(begin_proc,
                                               end_proc,
                                               node_bearer_ptr,
                                               Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()))
                            continue;

                        if (std::binary_search(begin_ghost,
                                               end_ghost,
                                               node_bearer_ptr,
                                               Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()))
                            continue;

                        indexes_to_delete.push_back(interface_index);
                    }

                    for (const auto& index : indexes_to_delete)
                        node_bearer_per_interface_list.erase(index);
                }


                template<class OrientedInterfaceContainerT>
                void AddNodeBearerOnBoundaryCondition(
                    const OrientedInterfaceContainerT& interface_list,
                    const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
                        node_bearer_on_interface_type_list,
                    NodeBearer::vector_shared_ptr& node_bearer_list)
                {
                    for (const auto& interface_ptr : interface_list)
                    {
                        assert(!(!interface_ptr));

                        auto it_node_bearer =
                            node_bearer_on_interface_type_list.find(interface_ptr->GetProgramWiseIndex());

                        // This might happen in models that use different shape functions on different parts of the
                        // mesh.
                        if (it_node_bearer == node_bearer_on_interface_type_list.cend())
                            continue;

                        const auto& node_bearer_ptr = it_node_bearer->second;
                        assert(!(!node_bearer_ptr));

                        node_bearer_list.push_back(node_bearer_ptr);
                    }
                }


            } // namespace


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
