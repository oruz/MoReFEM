/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 19 Aug 2014 14:27:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Partition/Impl/InterfaceSpecialization.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HXX_
