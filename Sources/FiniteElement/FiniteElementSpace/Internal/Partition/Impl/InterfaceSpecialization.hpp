/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Oct 2014 12:28:00 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {


                /*!
                 * \brief Helper struct that acts as a dispatcher depending on the type of the interface considered.
                 *
                 * \copydetails doxygen_hide_oriented_interface_tparam
                 */
                template<class OrientedInterfaceT>
                struct InterfaceSpecialization;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                template<>
                struct InterfaceSpecialization<Vertex>
                {
                    /*!
                     * There is only one LocalNode for Vertex, so it isn't stored as a vector in BasicRefFElt.
                     * However, for genericity of AddInterfaceNodeList() function I need it as  vector; there
                     * is therefore creation and copy of a vector with exactly one element inside.
                     *
                     */
                    using NodeListType = Advanced::LocalNode::vector_const_shared_ptr;

                    using InterfaceListType = const Vertex::vector_shared_ptr&;

                    static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

                    static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                    const Vertex& vertex,
                                                    std::size_t local_index);

                    static NodeBearer::shared_ptr CreateNodeBearer(const Vertex::shared_ptr& vertex_ptr);

                    static std::size_t Ninterface(const GeometricElt& geom_elt);
                };


                template<>
                struct InterfaceSpecialization<OrientedEdge>
                {
                    using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;

                    using InterfaceListType = const OrientedEdge::vector_shared_ptr&;

                    static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                    const OrientedEdge& edge,
                                                    std::size_t local_index);

                    static NodeBearer::shared_ptr CreateNodeBearer(const OrientedEdge::shared_ptr& edge_ptr);

                    static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

                    static std::size_t Ninterface(const GeometricElt& geom_elt);
                };


                template<>
                struct InterfaceSpecialization<OrientedFace>
                {
                    using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;

                    using InterfaceListType = const OrientedFace::vector_shared_ptr&;


                    static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                    const OrientedFace& face,
                                                    std::size_t local_index);

                    static NodeBearer::shared_ptr CreateNodeBearer(const OrientedFace::shared_ptr& face_ptr);

                    static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);


                    static std::size_t Ninterface(const GeometricElt& geom_elt);
                };


                template<>
                struct InterfaceSpecialization<Volume>
                {

                    using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;


                    /*!
                     * There is at most one interface for Volume, so it isn't stored as a vector in GeometricElt.
                     * However, for genericity of AddInterfaceNodeList() function I need it as  vector; there
                     * is therefore creation and copy of a vector with 0 or 1 element inside.
                     *
                     */
                    using InterfaceListType = Volume::vector_shared_ptr;


                    static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                    const Volume& volume,
                                                    std::size_t local_index);


                    static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

                    static NodeBearer::shared_ptr CreateNodeBearer(const Volume::shared_ptr& volume_ptr);


                    static std::size_t Ninterface(const GeometricElt& geom_elt);
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/Partition/Impl/InterfaceSpecialization.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_IMPL_x_INTERFACE_SPECIALIZATION_HPP_
