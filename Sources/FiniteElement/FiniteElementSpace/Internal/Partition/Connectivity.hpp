/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_CONNECTIVITY_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_CONNECTIVITY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            //! Enum class to make the call to ComputeNodeConnectivity() more readable than with a mere boolean.
            enum class KeepSelfConnexion
            {
                yes,
                no
            };


            /*!
             * \brief Convenient alias.
             *
             * \internal <b><tt>[internal]</tt></b> Unordered map use the address and not the NodeBearer object in its
             * hash table.
             * \endinternal
             */
            using connectivity_type = std::unordered_map<NodeBearer::shared_ptr, NodeBearer::vector_shared_ptr>;


            /*!
             * \brief Compute the connectivity between \a NodeBearer.
             *
             * In MoReFEM, parallelism ensures that all nodes and dofs located on a same \a Interface are
             * managed by the same processor; that's why we need first to define a connectivity between
             * \a NodeBearer prior to the processor reduction.
             *
             * \param[in] felt_space_list List of all finite element spaces within the god of dof (current
             * function is called once per \a GodOfDof.
             * \param[in] Nprocessor_wise_node_bearer Number of processor-wise node bearers.
             * \param[in] KeepSelfConnexion Whether we keep auto-association or not for each node bearer.
             *
             * \return For each node bearer, the list of connected node bearers.
             */
            connectivity_type ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                                            const std::size_t Nprocessor_wise_node_bearer,
                                                            KeepSelfConnexion KeepSelfConnexion);

            /*!
             * \brief Given a list of \a NodeBearer, determine the list of connected \a NodeBearer that should be kept to maintain integrity.
             *
             * \param[in] felt_space_list List of \a FEltSpace within a \a GodOfDof.
             * \param[in] node_bearer_list_to_keep List of \a NodeBearer that are for some reason tagged as needed to be kept (the
             * current use of this is for instance for the purposes of \a FromCoordsMatching interpolator).
             *
             * \return A list of \a NodeBearer that needs to be kept to preserve the integrity of the data structure.  By construct all the \a NodeBearer
             * from \a node_bearer_list_to_keep are there. At this point, no filtering concerning processor-wise or
             * ghost.
             */
            NodeBearer::vector_shared_ptr ComputeConnectedNodeBearers(
                const FEltSpace::vector_unique_ptr& felt_space_list,
                const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& node_bearer_list_to_keep);


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_CONNECTIVITY_HPP_
