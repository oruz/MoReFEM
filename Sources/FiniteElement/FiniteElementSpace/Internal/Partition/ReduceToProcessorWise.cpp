/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Nov 2014 17:24:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp" // IWYU pragma: keep
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/AssignGeomEltToProcessor.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/ReduceToProcessorWise.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace // anonymous
            {


                /*!
                 * \brief Sift through \a list_to_process and filter out elements that were already in \a
                 * processor_wise_list
                 */
                template<class T>
                void FilterOutProcessorWise(const typename T::vector_shared_ptr& processor_wise_list,
                                            typename T::vector_shared_ptr& list_to_process);

#ifndef NDEBUG
                /*!
                 * \brief Various sanity checks upon processor_wise_node_bearer_list and ghost_node_bearer_list.
                 *
                 * \return Always true: return value is just a trick so that the function may be put inside an assert, but in case of failure there are asserts
                 * in the function definition which will be triggered.
                 */
                bool AreNodeBearerListsValid(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                             const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

#endif // NDEBUG


            } // namespace


            /*!
             * \brief Reduce the mesh object to its components on the local processor.
             *
             * \internal This is a struct solely to help friendship declaration in Mesh.
             * This frienship is also the reason it is not in the anonymous namespace!
             * \endinternal
             */
            struct ReduceMesh
            {


                /*!
                 * \brief Static method that actually does all the work.
                 *
                 * \param[in] felt_space_list List of all finite element space. For each of them, the list
                 * of geometric elements used will be extracted and put in the list of elements to keep.
                 * \param[in] processor_wise_node_bearer_list List of processor-wise node bearers.
                 * \param[in,out] mesh The Mesh to be reduced to processor-wise data.
                 */
                static void Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                    const LocalFEltSpace::vector_shared_ptr& ghost_local_felt_space_list,
                                    Mesh& mesh);
            };


            void ReduceToProcessorWise ::Perform(
                const ::MoReFEM::Wrappers::Mpi& mpi,
                const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                const FEltSpace::vector_unique_ptr& felt_space_list,
                const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
                NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                Mesh& mesh)
            {
                assert(ghost_node_bearer_list.empty());

                const std::size_t mpi_rank = mpi.GetRank<std::size_t>();

                LocalFEltSpace::vector_shared_ptr ghost_local_felt_space_list;

                for (const auto& felt_space_ptr : felt_space_list)
                {
                    assert(!(!felt_space_ptr));
                    auto& felt_space = *felt_space_ptr;
                    felt_space.Reduce(assign_geom_elt_to_processor);

                    ExtractLocalFEltSpaceList<RoleOnProcessor::ghost>(felt_space, ghost_local_felt_space_list);
                }

                {
                    auto connectivity = ComputeNodeBearerConnectivity(felt_space_list,
                                                                      processor_wise_node_bearer_list.size(),
                                                                      Internal::FEltSpaceNS::KeepSelfConnexion::yes);

                    for (auto& [processor_wise_node_bearer_ptr, connected_node_bearer_list] : connectivity)
                    {
                        for (const auto& connected_node_bearer_ptr : connected_node_bearer_list)
                        {
                            assert(!(!connected_node_bearer_ptr));
                            if (connected_node_bearer_ptr->GetProcessor() != mpi_rank)
                                ghost_node_bearer_list.push_back(connected_node_bearer_ptr);
                        }
                    }

                    Utilities::EliminateDuplicate(ghost_node_bearer_list,
                                                  Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                                  Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());
                }

                {
                    decltype(auto) fvm_node_bearer_list = match_interface_node_bearer.GetCoordsMatchingData();

                    auto node_bearer_to_keep = ComputeConnectedNodeBearers(felt_space_list, fvm_node_bearer_list);

                    std::copy_if(node_bearer_to_keep.cbegin(),
                                 node_bearer_to_keep.cend(),
                                 std::back_inserter(ghost_node_bearer_list),
                                 [mpi_rank](const auto& node_bearer_ptr)
                                 {
                                     assert(!(!node_bearer_ptr));
                                     return node_bearer_ptr->GetProcessor() != mpi_rank;
                                 });

                    Utilities::EliminateDuplicate(ghost_node_bearer_list,
                                                  Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                                  Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());
                }

                assert(AreNodeBearerListsValid(processor_wise_node_bearer_list, ghost_node_bearer_list));

                // Boundary condition: keep only processor-wise and ghost NodeBearer
                {
                    const auto& boundary_condition_list =
                        DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();

                    for (auto& boundary_condition_ptr : boundary_condition_list)
                    {
                        assert(!(!boundary_condition_ptr));
                        boundary_condition_ptr->Reduce(processor_wise_node_bearer_list, ghost_node_bearer_list);
                    }
                }

                match_interface_node_bearer.Reduce(processor_wise_node_bearer_list, ghost_node_bearer_list);

                ReduceMesh::Perform(mpi,
                                    assign_geom_elt_to_processor,
                                    processor_wise_node_bearer_list,
                                    ghost_node_bearer_list,
                                    ghost_local_felt_space_list,
                                    mesh);
            }


            namespace // anonymous
            {


                void ComputeCoordsListFromNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                         Coords::vector_shared_ptr& reduced_coords_list)
                {
                    for (const auto& node_bearer_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));

                        decltype(auto) interface = node_bearer_ptr->GetInterface();

                        decltype(auto) vertex_coords_on_interface = interface.GetVertexCoordsList();

                        // #248 I assume here Coords are only vertices.
                        std::copy(vertex_coords_on_interface.cbegin(),
                                  vertex_coords_on_interface.cend(),
                                  std::back_inserter(reduced_coords_list));
                    }

                    Utilities::EliminateDuplicate(reduced_coords_list,
                                                  Utilities::PointerComparison::Less<Coords::shared_ptr>(),
                                                  Utilities::PointerComparison::Equal<Coords::shared_ptr>());
                }


#ifndef NDEBUG
                /*!
                 * \brief Various sanity checks upon processor_wise_node_bearer_list and ghost_node_bearer_list.
                 */
                bool AreNodeBearerListsValid(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                             const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
                {
                    assert(std::is_sorted(processor_wise_node_bearer_list.cbegin(),
                                          processor_wise_node_bearer_list.cend(),
                                          Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));

                    assert(std::is_sorted(ghost_node_bearer_list.cbegin(),
                                          ghost_node_bearer_list.cend(),
                                          Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));

                    NodeBearer::vector_shared_ptr intersection;

                    std::set_intersection(processor_wise_node_bearer_list.cbegin(),
                                          processor_wise_node_bearer_list.cend(),
                                          ghost_node_bearer_list.cbegin(),
                                          ghost_node_bearer_list.cend(),
                                          std::back_inserter(intersection),
                                          Utilities::PointerComparison::Less<NodeBearer::shared_ptr>());

                    assert(intersection.empty());

                    return true;
                }

#endif // NDEBUG


            } // namespace


            void
            ReduceMesh ::Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                                 const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                 const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                 const LocalFEltSpace::vector_shared_ptr& ghost_local_felt_space_list,
                                 Mesh& mesh)
            {
                GeometricElt::vector_shared_ptr processor_wise_geom_elt_list;
                GeometricElt::vector_shared_ptr ghost_geom_elt_list;

                {
                    decltype(auto) program_wise_geom_elt_list =
                        mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>(); // not a mistake - true before
                                                                                     // reduction

                    processor_wise_geom_elt_list.reserve(program_wise_geom_elt_list.size());

                    std::copy_if(program_wise_geom_elt_list.cbegin(),
                                 program_wise_geom_elt_list.cend(),
                                 std::back_inserter(processor_wise_geom_elt_list),
                                 [&mpi, &assign_geom_elt_to_processor](const auto& geom_elt_ptr)
                                 {
                                     assert(!(!geom_elt_ptr));
                                     return assign_geom_elt_to_processor.IsProcessorWise(mpi, *geom_elt_ptr);
                                 });

                    std::copy_if(program_wise_geom_elt_list.cbegin(),
                                 program_wise_geom_elt_list.cend(),
                                 std::back_inserter(ghost_geom_elt_list),
                                 [&mpi, &assign_geom_elt_to_processor](const auto& geom_elt_ptr)
                                 {
                                     assert(!(!geom_elt_ptr));
                                     return assign_geom_elt_to_processor.IsGhost(mpi, *geom_elt_ptr);
                                 });
                }

                Coords::vector_shared_ptr processor_wise_coords_list;
                Coords::vector_shared_ptr ghost_coords_list;

                ComputeCoordsListFromNodeBearerList(processor_wise_node_bearer_list, processor_wise_coords_list);
                ComputeCoordsListFromNodeBearerList(ghost_node_bearer_list, ghost_coords_list);

                FilterOutProcessorWise<Coords>(processor_wise_coords_list, ghost_coords_list);

                NodeBearer::vector_shared_ptr node_bearer_in_local_felt_space_ghost_list;

                for (const auto& local_felt_space_ptr : ghost_local_felt_space_list)
                {
                    assert(!(!local_felt_space_ptr));

                    decltype(auto) node_bearer_list = local_felt_space_ptr->GetNodeBearerList();

                    for (const auto& node_bearer_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        node_bearer_in_local_felt_space_ghost_list.push_back(node_bearer_ptr);

                        const auto& node_bearer = *node_bearer_ptr;

                        decltype(auto) coords_list = node_bearer.GetInterface().GetVertexCoordsList();

                        for (const auto& coords_ptr : coords_list)
                        {
                            assert(!(!coords_ptr));

                            if (!std::binary_search(processor_wise_coords_list.cbegin(),
                                                    processor_wise_coords_list.cend(),
                                                    coords_ptr,
                                                    Utilities::PointerComparison::Less<Coords::shared_ptr>()))
                                ghost_coords_list.push_back(coords_ptr);
                        }
                    }
                }

                Utilities::EliminateDuplicate(node_bearer_in_local_felt_space_ghost_list,
                                              Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

                FilterOutProcessorWise<NodeBearer>(processor_wise_node_bearer_list,
                                                   node_bearer_in_local_felt_space_ghost_list);

                Utilities::EliminateDuplicate(ghost_coords_list,
                                              Utilities::PointerComparison::Less<Coords::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<Coords::shared_ptr>());

                mesh.ShrinkToProcessorWise(mpi,
                                           processor_wise_geom_elt_list,
                                           ghost_geom_elt_list,
                                           std::move(processor_wise_coords_list),
                                           std::move(ghost_coords_list));
            }


            namespace // anonymous
            {


                template<class T>
                void FilterOutProcessorWise(const typename T::vector_shared_ptr& processor_wise_list,
                                            typename T::vector_shared_ptr& list_to_process)
                {
                    if (list_to_process.empty()) // the assert below could fail if this condition is not cleared
                                                 // out first.
                        return;

                    assert(processor_wise_list.data() != list_to_process.data());

                    Utilities::EliminateDuplicate(list_to_process,
                                                  Utilities::PointerComparison::Less<typename T::shared_ptr>(),
                                                  Utilities::PointerComparison::Equal<typename T::shared_ptr>());

                    const auto proc_wise_list_begin = processor_wise_list.cbegin();
                    const auto proc_wise_list_end = processor_wise_list.cend();

                    const auto it_partition = std::partition(
                        list_to_process.begin(),
                        list_to_process.end(),
                        [&proc_wise_list_begin, &proc_wise_list_end](const auto& object)
                        {
                            return !std::binary_search(proc_wise_list_begin,
                                                       proc_wise_list_end,
                                                       object,
                                                       Utilities::PointerComparison::Less<typename T::shared_ptr>());
                        });

                    list_to_process.erase(it_partition, list_to_process.end());

                    // Partition doesn't keep the ordering (stable parition would have but would have been more
                    // expensive)... so put it back!
                    std::sort(list_to_process.begin(),
                              list_to_process.end(),
                              Utilities::PointerComparison::Less<typename T::shared_ptr>());
                }


            } // namespace


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
