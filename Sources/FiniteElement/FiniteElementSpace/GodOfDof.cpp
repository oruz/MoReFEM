/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    GodOfDof::GodOfDof(const Wrappers::Mpi& mpi, Mesh& mesh)
    : Crtp::CrtpMpi<GodOfDof>(mpi), unique_id_parent(mesh.GetUniqueId()), mesh_(mesh)
    { }


    const std::string& GodOfDof::ClassName()
    {
        static std::string ret("GodOfDof");
        return ret;
    }


    void GodOfDof::PrintDofInformation(const NumberingSubset& numbering_subset, std::ostream& out) const
    {
        const auto& node_bearer_list = GetProcessorWiseNodeBearerList();

        out << "Ndof (processor_wise) = " << NprocessorWiseDof(numbering_subset) << std::endl;
        out << "# First column: program-wise index." << std::endl;
        out << "# Second column: processor-wise index." << std::endl;
        out << "# Third column: the interface upon which the dof is located. Look at the interface file in same "
               "directory to relate it to the initial mesh."
            << std::endl;
        out << "# Fourth column: unknown and component involved." << std::endl;
        out << "# Fifth column: shape function label." << std::endl;

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;

            std::ostringstream oconv;
            oconv << node_bearer.GetNature() << ' ' << node_bearer.GetInterface().GetProgramWiseIndex();

            const std::string nature(oconv.str());

            const auto& node_list = node_bearer.GetNodeList();

            for (auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;

                if (!node.IsInNumberingSubset(numbering_subset))
                    continue;

                const auto& unknown = node.GetUnknown();

                const std::string unknown_name(unknown.GetName());

                const auto& dof_list = node.GetDofList();

                const std::size_t Ndof_in_node = dof_list.size();

                const auto& shape_function_label = node.GetShapeFunctionLabel();

                for (std::size_t i = 0ul; i < Ndof_in_node; ++i)
                {
                    const auto& dof_ptr = dof_list[i];
                    assert(!(!dof_ptr));

                    out << dof_ptr->GetProgramWiseIndex(numbering_subset) << ';';
                    out << dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset) << ';';
                    out << nature << ';';
                    out << unknown_name << ' ' << i << ';';
                    out << shape_function_label;
                    out << std::endl;
                }
            }
        }
    }


    void PrintNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list,
                             const std::size_t rank,
                             std::ostream& out)
    {
        std::vector<NodeBearerNS::program_wise_index_type> indexes;
        indexes.reserve(node_bearer_list.size());

        std::vector<std::size_t> on_proc;
        on_proc.reserve(node_bearer_list.size());

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            indexes.push_back(node_bearer_ptr->GetProgramWiseIndex());
            on_proc.push_back(node_bearer_ptr->GetProcessor());
        }

        std::ostringstream oconv;
        oconv << "Node bearers on processor " << rank << " -> [";

        Utilities::PrintContainer<>::Do(indexes,
                                        out,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener(oconv.str()),
                                        PrintNS::Delimiter::closer("]"));
    }


    auto GodOfDof::GetIteratorFEltSpace(std::size_t felt_space_index) const
    {
        const auto& felt_space_list = GetFEltSpaceList();

        return std::find_if(felt_space_list.cbegin(),
                            felt_space_list.cend(),
                            [felt_space_index](const auto& felt_space_ptr)
                            {
                                assert(!(!felt_space_ptr));
                                return felt_space_ptr->GetUniqueId() == felt_space_index;
                            });
    }


    bool GodOfDof::IsFEltSpace(std::size_t felt_space_index) const
    {
        return GetIteratorFEltSpace(felt_space_index) != GetFEltSpaceList().cend();
    }


    const FEltSpace& GodOfDof::GetFEltSpace(std::size_t felt_space_index) const
    {
        auto it = GetIteratorFEltSpace(felt_space_index);

        assert(it != GetFEltSpaceList().cend() && "If not something went awry in GodOfDof initialization!");

        return *(*it);
    }


    const NumberingSubset::vector_const_shared_ptr& GodOfDof::ComputeNumberingSubsetList()
    {
        const auto& felt_space_list = GetFEltSpaceList();

        assert(numbering_subset_list_.empty() && "Should be initialized only once!");

        for (const auto& felt_space_ptr : felt_space_list)
        {
            decltype(auto) numbering_subset_list_in_felt_space = felt_space_ptr->GetNumberingSubsetList();

            std::move(numbering_subset_list_in_felt_space.begin(),
                      numbering_subset_list_in_felt_space.end(),
                      std::back_inserter(numbering_subset_list_));
        }

        Utilities::EliminateDuplicate(numbering_subset_list_,
                                      Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

        assert(!numbering_subset_list_.empty() && "There must be at least one...");

        return numbering_subset_list_;
    }


    const Wrappers::Petsc::MatrixPattern&
    GodOfDof::GetMatrixPattern(const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& column_numbering_subset) const
    {
        auto it = std::find_if(matrix_pattern_per_numbering_subset_.cbegin(),
                               matrix_pattern_per_numbering_subset_.cend(),
                               [&row_numbering_subset, &column_numbering_subset](const auto& item)
                               {
                                   assert(!(!item));
                                   return item->GetRowNumberingSubset() == row_numbering_subset
                                          && item->GetColumnNumberingSubset() == column_numbering_subset;
                               });

        assert(it != matrix_pattern_per_numbering_subset_.cend());
        assert(!(!*it));
        return (*it)->GetPattern();
    }


    const NumberingSubset::const_shared_ptr& GodOfDof::GetNumberingSubsetPtr(std::size_t unique_id) const
    {
        const auto& numbering_subset_list = GetNumberingSubsetList();

        auto it = std::find_if(numbering_subset_list.cbegin(),
                               numbering_subset_list.cend(),
                               [unique_id](const auto& numbering_subset_ptr)
                               {
                                   assert(!(!numbering_subset_ptr));
                                   return numbering_subset_ptr->GetUniqueId() == unique_id;
                               });

        // \todo #608 Should be an exception (currently assert is fine but for some model in which subsets
        // may be monolithic or not it's not that sure...)
        assert(Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance(__FILE__, __LINE__).DoExist(unique_id)
               && "The requested numbering subset is not even defined in the InputData tuple!");

        assert(it != numbering_subset_list.cend()
               && "Numbering subset was not found in the GodOfDof but do exists in the InputData tuple; "
                  "the most likely explanation is that no FEltSpace in the GodOfDof is actually using it.");

        assert(!(!*it));

        return *it;
    }


    void GodOfDof::PrepareOutput(const Internal::Parallelism* parallelism)
    {
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        decltype(auto) mpi = GetMpi();

        // First create all the relevant folders; make sure this is done before any processor attempts to create a file.
        {
            for (const auto& numbering_subset_ptr : numbering_subset_list)
            {
                assert(!(!numbering_subset_ptr));
                const auto& numbering_subset = *numbering_subset_ptr;

                // Create the subfolder to store numbering subset data.
                // Root processor is in charge of it; other processors must wait until it's done.
                auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);

                if (!Advanced::FilesystemNS::DirectoryNS::DoExist(subfolder)) // \todo #497
                    Advanced::FilesystemNS::DirectoryNS::Create(subfolder, __FILE__, __LINE__);
            }
        }

        mpi.Barrier();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            // Create the subfolder to store numbering subset data.
            // Root processor is in charge of it; other processors must wait until it's done.
            auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);

            // Report the dof informations.
            std::ostringstream oconv;
            oconv << subfolder << "/dof_information.hhdata";
            std::ofstream out;
            FilesystemNS::File::Create(out, oconv.str(), __FILE__, __LINE__);
            PrintDofInformation(numbering_subset, out);
        }

        // Write if required the parallelism strategy.
        if (parallelism != nullptr)
        {
            const auto parallelism_strategy = parallelism->GetParallelismStrategy();

            decltype(auto) mesh = GetMesh();

            switch (parallelism_strategy)
            {
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::precompute:
            {
                FilesystemNS::Directory mesh_subdir(
                    parallelism->GetDirectory(), "Mesh_" + std::to_string(mesh.GetUniqueId()), __FILE__, __LINE__);

                Advanced::MeshNS::WritePrepartitionedData partition_data_facility(
                    mesh, mesh_subdir, mesh.GetInitialFormat());

                Internal::GodOfDofNS::WritePrepartitionedData(*this, mesh_subdir);

                break;
            }
            case Advanced::parallelism_strategy::none:
            case Advanced::parallelism_strategy::run_from_preprocessed:
            case Advanced::parallelism_strategy::parallel_no_write:
                break;
            }
        }
    }


    void GodOfDof::ClearTemporaryData() const noexcept
    {
        const auto& felt_list = GetFEltSpaceList();

        for (const auto& felt_ptr : felt_list)
        {
            assert(!(!felt_ptr));
            felt_ptr->ClearTemporaryData();
        }
    }


#ifndef NDEBUG
    void AssertMatrixRespectPattern(const GodOfDof& god_of_dof,
                                    const GlobalMatrix& matrix,
                                    const char* invoking_file,
                                    int invoking_line)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        try
        {
            decltype(auto) pattern =
                god_of_dof.GetMatrixPattern(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

            const auto Nrow = static_cast<std::size_t>(matrix.GetProcessorWiseSize(invoking_file, invoking_line).first);

            if (Nrow != pattern.Nrow())
                throw Exception("Number of rows in pattern and matrix is not the same!", invoking_file, invoking_line);

            decltype(auto) iCsr = pattern.GetICsr();
            decltype(auto) jCsr = pattern.GetJCsr();

            assert(iCsr.size() == Nrow + 1 && "This would highlight a bug within MatrixPattern class");

            auto current_jcsr_index = 0ul;

            decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

            std::vector<PetscInt> position_list_in_matrix;
            std::vector<PetscScalar> value_list_in_matrix;

            decltype(auto) row_numbering_subset = matrix.GetRowNumberingSubset();


            Dof::vector_shared_ptr processor_wise_dof_list_in_row_numbering_subset;


            std::copy_if(processor_wise_dof_list.cbegin(),
                         processor_wise_dof_list.cend(),
                         std::back_inserter(processor_wise_dof_list_in_row_numbering_subset),
                         [&row_numbering_subset](const auto& dof_ptr)
                         {
                             assert(!(!dof_ptr));
                             return dof_ptr->IsInNumberingSubset(row_numbering_subset);
                         });

            for (auto row_index = 0ul; row_index < Nrow; ++row_index)
            {
                position_list_in_matrix.clear();
                value_list_in_matrix.clear();

                const auto it =
                    std::find_if(processor_wise_dof_list_in_row_numbering_subset.cbegin(),
                                 processor_wise_dof_list_in_row_numbering_subset.cend(),
                                 [&row_numbering_subset, row_index](const auto& dof_ptr)
                                 {
                                     assert(!(!dof_ptr));
                                     return dof_ptr->GetProcessorWiseOrGhostIndex(row_numbering_subset) == row_index;
                                 });
                assert(it != processor_wise_dof_list.cend());
                assert(!(!(*it)));

                const auto program_wise_index = static_cast<PetscInt>((*it)->GetProgramWiseIndex(row_numbering_subset));

                matrix.GetRow(
                    program_wise_index, position_list_in_matrix, value_list_in_matrix, invoking_file, invoking_line);

                const auto Nvalue_in_pattern_row = static_cast<std::size_t>(
                    iCsr[static_cast<std::size_t>(row_index + 1)] - iCsr[static_cast<std::size_t>(row_index)]);

                std::vector<PetscInt> position_list_in_pattern;

                for (auto j = 0ul; j < Nvalue_in_pattern_row; ++j)
                {
                    assert(current_jcsr_index + j < jCsr.size());
                    position_list_in_pattern.push_back(static_cast<PetscInt>(jCsr[current_jcsr_index + j]));
                }


                current_jcsr_index += Nvalue_in_pattern_row;

                if (!std::includes(position_list_in_pattern.cbegin(),
                                   position_list_in_pattern.cend(),
                                   position_list_in_matrix.cbegin(),
                                   position_list_in_matrix.cend()))
                {
                    PrintNumberingSubset("matrix", matrix);

                    std::ostringstream oconv;
                    oconv << "Position in the actual matrix must match those defined in the pattern! Not the case "
                             "for row "
                          << program_wise_index << " (program-wise numbering) on processor " << mpi.GetRank<int>();

                    Utilities::PrintContainer<>::Do(position_list_in_matrix,
                                                    oconv,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("\n\t- Position in matrix -> ["),
                                                    PrintNS::Delimiter::closer("]"));

                    Utilities::PrintContainer<>::Do(position_list_in_pattern,
                                                    oconv,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("\t- Position in pattern -> ["),
                                                    PrintNS::Delimiter::closer("]"));

                    throw Exception(oconv.str(), invoking_file, invoking_line);
                }
            }
        }
        catch (const Exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
#endif // NDEBUG


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
