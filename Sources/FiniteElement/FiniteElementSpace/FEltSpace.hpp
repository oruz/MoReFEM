/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:45:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <memory>
#include <vector>

#include "Utilities/Mpi/Mpi.hpp" // IWYU pragma: export
#include "Utilities/Mutex/Mutex.hpp"
#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hpp"        // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp" // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"             // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"                 // IWYU pragma: export
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp" // IWYU pragma: export

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::ConformInterpolatorNS::LagrangianNS { class LocalLagrangianInterpolator; }
namespace MoReFEM::Internal::FEltSpaceNS { class AssignGeomEltToProcessor; }
namespace MoReFEM::Internal::FEltSpaceNS { struct ReduceToProcessorWise; }

namespace MoReFEM::ConformInterpolatorNS::LagrangianNS
{
    
    template<class DerivedT, class ElementaryInterpolatorT>
    class LagrangianInterpolator; // IWYU pragma: keep
}

namespace MoReFEM::ParameterNS::Policy
{
    template<ParameterNS::Type TypeT, std::size_t Ndim>
    class AtDof; // IWYU pragma: keep
}

namespace MoReFEM
{
    template<ParameterNS::Type TypeT, std::size_t Ndim>
    class ParamAtDof; // IWYU pragma: keep
}


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief The class in charge of most of the interation with nodes, dofs and unknowns.
     *
     *
     * \warning The actual content of the FEltSpace is not filled until its GodOfDof Init() method has been called!
     * All finite element spaces must therefore be defined at this specific stage...
     */
    class FEltSpace final : public Crtp::CrtpMpi<FEltSpace>,
                            public Crtp::Mutex<FEltSpace>,
                            public Crtp::UniqueId<FEltSpace, UniqueIdNS::AssignationMode::manual>
    {
      public:
        //! Name of the class.
        static const std::string& ClassName();

        //! Alias over unique_ptr.
        using unique_ptr = std::unique_ptr<FEltSpace>;

        //! Alias over a vector of unique_ptr.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Convenient alias to one of the parent.
        using unique_id_parent = Crtp::UniqueId<FEltSpace, UniqueIdNS::AssignationMode::manual>;

        /*!
         * Friendship to a private class, which needs to use GetNonCstFEltStorage().
         *
         * I do not want this method to be public: this access is quite insecure!
         */
        friend struct Internal::FEltSpaceNS::ReduceToProcessorWise;

        //! Friendship to helper class during partitioning.
        friend class Internal::FEltSpaceNS::AssignGeomEltToProcessor;


        //! Friendship to interpolator to allow access to RefLocalFEltSpace.
        template<class DerivedT, class ElementaryInterpolatorT>
        friend class ConformInterpolatorNS::LagrangianNS::LagrangianInterpolator;


        //! Friendship to interpolator to allow access to RefLocalFEltSpace.
        friend class ConformInterpolatorNS::LagrangianNS::LocalLagrangianInterpolator;

        //! Friendship to GodOfDof (which stores the FEltSpace defined on its related mesh).
        friend GodOfDof;

        //! Friendship to AtDof parameter policy, which requires access to \a BasicRefFElt \a ShapeFunction() method.
        template<ParameterNS::Type TypeT, std::size_t Ndim>
        friend class ParameterNS::Policy::AtDof;


        //! Friendship to AtDof parameter policy, which requires access to \a BasicRefFElt \a ShapeFunction() method.
        template<ParameterNS::Type TypeT, std::size_t Ndim>
        friend class ParamAtDof;

      public:
        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] domain Geometric domain upon which the finite element space is defined.
         * \param[in] god_of_dof_ptr Shared pointer to the enclosing god of dof; a weak pointer will be kept inside
         * current class.
         * \param[in] unique_id Unique identifier of the finite element space. This is under this identifier that
         * the finite element space is defined in the input data file.
         * \param[in] extended_unknown_list List of couples (\a Unknown, \a NumberingSubset) considered within the
         * finite element space.
         */
        explicit FEltSpace(const std::shared_ptr<const GodOfDof>& god_of_dof_ptr,
                           const Domain& domain,
                           std::size_t unique_id,
                           ExtendedUnknown::vector_const_shared_ptr&& extended_unknown_list);

        //! Destructor.
        ~FEltSpace() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FEltSpace(FEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FEltSpace(FEltSpace&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FEltSpace& operator=(FEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FEltSpace& operator=(FEltSpace&& rhs) = delete;

        ///@}


        /*!
         * \brief Init the local2global array for each combination LocalFEltSpace/numbering subset.
         *
         * This method must be called before \a ComputeLocal2Global() one, which actually uses up what is computed
         * here to compute the local2global array related to a subset of unknowns.
         *
         * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
         */
        void InitLocal2Global(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global);

        /*!
         * \brief Use the output of the finite element space to move a mesh.
         *
         * Relevant only for numbering subsets for which do_move_mesh is true.
         *
         * \param[in] vector Vector which values will be used to move the coords. The numbering subset used is
         * derived from this vector as well.
         *
         * The mesh moved here is the one covered by the god of dof that includes the finite element space.
         */
        void MoveMesh(const GlobalVector& vector) const;


        /*!
         * \brief Exactly the same as MoveMesh except that the displacement is applied upon the initial position
         * of the mesh.
         *
         * \param[in] vector Vector which values will be used to move the coords. The numbering subset used is
         * derived from this vector as well.
         */
        void MoveMeshFromInitialPosition(const GlobalVector& vector) const;

      private:
        /*!
         * \brief Reduce the \a LocalFEltSpace to processor-wise and ghost.
         *
         * \attention The \a Dof are NOT yet reduced here; this is done just a bit later. To avoid ambiguity, the \a Dof
         * lists are cleared in current method; it has to be reinitiated once the reduction is fully perfomed for the \a
         * GodOfDof. You shouldn't have to bother with this unless you're a library developer and meddling with \a
         * GodOfDof initialization - the only moment this method should be used.
         *
         * \param[in] assign_geom_elt_to_processor An helper object which keeps track of the choices for all \a
         * GeometricElt. This helper object is used for all \a FEltSpace of a given \a GodOfDof - meaning that the
         * processor choice for a given \a GeometricElt is the same for all the \a LocalFEltSpace to which it is
         * related.
         */
        void Reduce(const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor);


      public:
        /*!
         * \class doxygen_hide_compute_local_2_global_method
         *
         * \brief Compute the local2global array required by a given global variational operator.
         *
         * This method is expected to be called only from a GlobalVariationalOperator or a child class of it;
         * it is public solely to account for the cases we need to call it within definitions of a child of
         * GlobalVariationalOperator class.
         *
         * \a InitLocal2Global() must have been called beforehand.
         *
         * \param[in] do_compute_processor_wise_local_2_global Whether local -> global will be required for processor-
         * wise dof indexes. This really depends on the operators considered in
         *
         * \internal <b><tt>[internal]</tt></b> Despite its constness, this method actually modifies one aspect of the
         * FEltSpace: the local2global array is computed and stored within a deep class which specific attribute is
         * mutable. The reason for this is that I would have to renounce to the constness of FEltSpace elsewhere
         * otherwise only for an operation that happens in the very beginning of the program, in the initialization
         * phase.
         * \endinternal
         */


        /*!
         * \copydoc doxygen_hide_compute_local_2_global_method
         *
         * \param[in] extended_unknown_list List of unknowns handled by the global variational operator which called
         * the present method. This list is splitted per numbering subset, and for each of them a local2global array is
         * computed.
         */
        void ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                 DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const;

        /*!
         * \copydoc doxygen_hide_compute_local_2_global_method
         *
         * \param[in] extended_unknown_ptr Couple (\a Unknown, \a NumberingSubset) for which local -> global array must
         * be computed.
         */
        void ComputeLocal2Global(ExtendedUnknown::const_shared_ptr&& extended_unknown_ptr,
                                 DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const;

        //! Return true if the FEltSpace doesn't include any finite element.
        bool IsEmpty() const noexcept;


      private:
        /*!
         * \brief Compute the maximal order of the finite elements present in the \a FEltSpace.
         *
         * \return Maximal order of the finite elements present in the \a FEltSpace
         */
        std::size_t ComputeMaxOrderFElt() const;

      private:
        /*!
         * \brief Compute the list of dofs from the one available in the GodOfDof.
         *
         * \param[in] dof_unique_id_list List of all dofs unique ids involved in the finite element space before the
         * partitioning occurs. All those still present will be put into \a dof_list_ container.
         *
         * \internal <b><tt>[internal]</tt></b> Should only be called in GodOfDof::Init().
         * \endinternal
         */
        void ComputeDofList(const std::vector<std::size_t>& dof_unique_id_list);


        /*!
         * \brief Fill the local finite element space list.
         *
         * \copydoc doxygen_hide_tparam_role_on_processor
         *
         * \param[in] mesh \a Mesh considered.
         * \param[in] domain \a Domain considered.
         * \param[in] ref_local_felt_space \a RefLocalFEltSpace for which the list is created.
         * \param[out] local_felt_space_list The \a LocalFEltSpace for each \a GeometricElt.
         *
         * \internal This is a method rather than an anonymous function solely to 'solve' the friendship of
         * LocalFEltSpace constructor.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t FillLocalFEltSpaceList(const Mesh& mesh,
                                           const Domain& domain,
                                           const Internal::RefFEltNS::RefLocalFEltSpace& ref_local_felt_space,
                                           LocalFEltSpace::per_geom_elt_index& local_felt_space_list) const;

        /*!
         * \brief Clear both processor-wise and ghost \a Dof lists.
         *
         * This should be called only immediatly after data reduction.
         */
        void ClearDofLists();

      public:
        /*!
         * \brief Get the list of local finite element spaces sort per finite element type.
         *
         * \copydoc doxygen_hide_tparam_role_on_processor
         */
        template<RoleOnProcessor RoleOnProcessorT>
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;


        /*!
         * \brief Get the list of local finite element spaces sort per finite element type for a given \a domain.
         *
         * \param[in] domain \a Domain used as filter.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const;

      private:
        /*!
         * \brief Get the list of local finite element spaces sort for a given \a RefLocalFEltSpace.
         *
         * \param[in] ref_felt_space \a RefLocalFEltSpace used as gilter.
         */
        const LocalFEltSpace::per_geom_elt_index&
        GetLocalFEltSpaceList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space) const;

        /*!
         * \brief Sift through the \a GodOfDof  lists and fill the lists of processor-wise and ghost \a Dof, and also
         * the \a NdofHolder object.
         */
        void ComputeDofList();

      public:
        /*!
         * \brief Get the local felt space that matches a given \a geometric_elt.
         *
         * \param[in] geometric_elt \a GeometricElt used as filter.
         */
        const LocalFEltSpace& GetLocalFEltSpace(const GeometricElt& geometric_elt) const;

        /*!
         * \brief Get the list of local finite element spaces sort for a given \a ref_geom_elt.
         *
         * \param[in] ref_geom_elt \a RefGeomElt used as filter.
         */
        const LocalFEltSpace::per_geom_elt_index& GetLocalFEltSpaceList(const RefGeomElt& ref_geom_elt) const;

        /*!
         * \brief Get the list of processor-wise dofs covered by the finite element space.
         *
         */
        const Dof::vector_shared_ptr& GetProcessorWiseDofList() const noexcept;

        //! Get the list of ghost dofs covered by the finite element space.
        const Dof::vector_shared_ptr& GetGhostDofList() const noexcept;

        //! Returns the number of processor-wise \a Dof,
        std::size_t NprocessorWiseDof() const noexcept;

        //! Returns the number of ghost \a Dof,
        std::size_t NghostDof() const noexcept;

        /*!
         * \brief Returns the number of processor-wise \a Dof for given \a numbering_subset.
         *
         * \param[in] numbering_subset \a NumberingSubset for which the tally is requested.
         *
         * \return Number of processor-wise \a Dof in the current \a FEltSpace for the chosen \a NumberingSubset.
         */
        std::size_t NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Returns the number of program-wise \a Dof for given \a numbering_subset.
         *
         * \param[in] numbering_subset \a NumberingSubset for which the tally is requested.
         *
         * \return Number of program-wise \a Dof in the current \a FEltSpace for the chosen \a NumberingSubset.
         */
        std::size_t NprogramWiseDof(const NumberingSubset& numbering_subset) const;


        /*!
         * \brief Get the list of processor-wise dofs covered by the finite element space and inside given
         * numbering subset.
         *
         * \attention The list doesn't exist and is returned by value; in most cases filtering on the namesake above
         * will be much more efficient.
         *
         * \param[in] numbering_subset NumberingSubset considered.
         *
         * \return List of processor-wise dofs covered by the finite element space and inside given
         * numbering subset.
         *
         */
        Dof::vector_shared_ptr GetProcessorWiseDofList(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Check whether \a numbering_subset is covered in the finite element space.
         *
         * \param[in] numbering_subset \a NumberingSubset being investigated.
         *
         * \return True if \a numbering_subset is covered by the finit element space.
         */
        bool DoCoverNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Returns the dimension considered within the FEltSpace.
        std::size_t GetDimension() const noexcept;

        //! Returns the dimension of the mesh (read from the GodOfDof).
        std::size_t GetMeshDimension() const;

        //! Access to the enclosing GodOfDof.
        std::shared_ptr<const GodOfDof> GetGodOfDofFromWeakPtr() const;

        //! List of all the pairs of unknowns and numbering subset.
        const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList() const noexcept;

        //! Get the numbering subset associated to \a unknown.
        //! \param[in] unknown \a Unknown which associated \a NumberingSubset is sought.
        const NumberingSubset& GetNumberingSubset(const Unknown& unknown) const;

        //! Get the numbering subset associated to \a unknown.
        //! \param[in] unknown \a Unknown which associated \a NumberingSubset is sought.
        const NumberingSubset::const_shared_ptr& GetNumberingSubsetPtr(const Unknown& unknown) const;

        //! Get the pair unknown/numbering subset associated to the given unknown.
        //! \param[in] unknown \a Unknown which associated \a NumberingSubset is sought.
        const ExtendedUnknown& GetExtendedUnknown(const Unknown& unknown) const;

        //! Get the pair unknown/numbering subset associated to the given unknown as a smart pointer.
        //! \param[in] unknown \a Unknown which associated \a ExtendedUnknown is sought.
        ExtendedUnknown::const_shared_ptr GetExtendedUnknownPtr(const Unknown& unknown) const;

        //! Get the list of numbering subset.
        const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const noexcept;

        //! Clear the temporary data used to build properly the Internal::FEltNS::Local2GlobalStorage objects.
        void ClearTemporaryData() const noexcept;

        /*!
         * \brief Constant accessor to the list of default quadrature rules to use in operators for which no
         * rule has been specified.
         */
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

        /*!
         * \brief Constant accessor to the list of default quadrature rules to use in operators for which no
         * rule has been specified.
         */
        const QuadratureRulePerTopology* GetQuadratureRulePerTopologyRawPtr() const noexcept;

        //! Domain upon which \a FEltSpace is defined. By construct its dimension is contrived (it can't overlap
        //! several).
        const Domain& GetDomain() const noexcept;

      private:
        /*!
         * \brief Get the reference finite element space related to a given \a ref_geom_elt.
         *
         * \internal <b><tt>[internal]</tt></b> This method assumes there is one; it is checked by an assert in
         * debug mode.
         * \endinternal
         *
         * \param[in] ref_geom_elt Reference geometric element.
         *
         * \return Reference finite element space related to a given \a ref_geom_elt.
         */
        const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace(const RefGeomElt& ref_geom_elt) const;


        //! \non_cst_accessor{list of processor-wise \a Dof}
        Dof::vector_shared_ptr& GetNonCstProcessorWiseDofList() noexcept;

        //! \non_cst_accessor{list of ghost \a Dof}
        Dof::vector_shared_ptr& GetNonCstGhostDofList() noexcept;

      private:
        /*!
         * \brief Prepare the finite element list from the mesh and the domain.
         *
         * \internal <b><tt>[internal]</tt></b> This method is called in the constructor and should not be called again
         * afterwards.
         * \endinternal
         *
         * \param[in] mesh Mesh upon which the GodOfDof in which the finite element space
         * is defined is built.
         * \param[in] domain The domain that delimits the whole finite element space.
         */
        void SetFEltList(const Mesh& mesh, const Domain& domain);

        /*!
         * \brief Compute data required for movemesh if relevant.
         *
         * This is done for each numbering subset that features DoMoveMesh() == true.
         *
         * The data set here is movemesh_helper_data_, which allows to match the dof considered to the actual vertices
         * of the mesh.
         *
         * \attention Broaden this when P2 geometry is reimplemented. \todo #248
         */
        void SetMovemeshData();


      private:
        //! Get access to the object in charge of finite element storage.
        const Internal::FEltSpaceNS::Storage& GetFEltStorage() const noexcept;

        //! Get non constant access to the object in charge of finite element storage.
        Internal::FEltSpaceNS::Storage& GetNonCstFEltStorage() noexcept;

#ifndef NDEBUG
        //! Assert GodOfDof has been properly initialized.
        void AssertGodOfDofInitialized() const;
#endif // NDEBUG

        //! Access to useful data when a mesh must be moved according to (felt_space, numbering subset) data.
        const std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper>& GetMovemeshHelperStorage() const noexcept;

        //! Access to useful data when a mesh must be moved according to (felt_space, numbering subset) data.
        std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper>& GetNonCstMovemeshHelperStorage() noexcept;

        //! Access to the movemesh helper related to a given vector.
        //! \param[in] vector \a GlobalVector which content should be used to move the mesh.
        const Internal::FEltSpaceNS::MovemeshHelper& GetMovemeshHelper(const GlobalVector& vector) const noexcept;

        //! Accessor to NdofHolder.
        const Internal::FEltSpaceNS::NdofHolder& GetNdofHolder() const noexcept;

      private:
        /*!
         * \brief Object in charge of storing the finite elements in an efficient way so that access is as
         * fast as possible.
         *
         * This can noticeably store the list of finite elements restricted on a smaller domain that the one
         * upon which the whole finite element space is built.
         *
         */
        Internal::FEltSpaceNS::Storage::const_unique_ptr felt_storage_ = nullptr;

        //! Dimension considered within the FEltSpace.
        std::size_t dimension_ = NumericNS::UninitializedIndex<std::size_t>();

        //! Weak pointer to the enclosing God of dof.
        std::weak_ptr<const GodOfDof> god_of_dof_;

        //! Domain upon which \a FEltSpace is defined. By construct its dimension is contrived (it can't overlap
        //! several).
        const Domain& domain_;

        /*!
         * \brief List of all the pairs of unknowns and numbering subset.
         *
         * GodOfDof is not enough here: same unknown might have been numbered with another \a NumberingSubset in another
         * finite element space.
         */
        const ExtendedUnknown::vector_const_shared_ptr extended_unknown_list_;

        /*!
         * \brief List of numbering subsets considered.
         *
         * \internal <b><tt>[internal]</tt></b> Could have been computed from \a unknown_storage_, but it is convenient
         * to get it stored independently.
         * \endinternal
         *
         */
        const NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

        /*!
         * \brief Data that helps to move efficiently a mesh.
         *
         * Key is the unique id of a numbering subset.
         * Values gives away the list of dofs for each vertex.
         *
         * Relevant only for numbering subsets for which do_move_mesh is true.
         */
        std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper> movemesh_helper_data_;

        //! List of processor-wise dofs (ghost excluded).
        Dof::vector_shared_ptr dof_list_;

        //! List of ghost dofs.
        Dof::vector_shared_ptr ghost_dof_list_;

        //! Objects that counts the number of dofs in several configurations.
        Internal::FEltSpaceNS::NdofHolder::const_unique_ptr Ndof_holder_ = nullptr;

#ifndef NDEBUG
        //! Helper data attribute to check some methods are not called unduly before the \a Dof lists are computed.
        bool are_dof_list_computed_ = false;
#endif // NDEBUG

        //! List of default quadrature rules to use in operators for which no rule has been specified.
        QuadratureRulePerTopology::const_unique_ptr default_quadrature_rule_per_topology_ = nullptr;
    };


    /*!
     * \brief Extract the list of all \a LocalFEltSpace present in the \a felt_space.
     *
     * \copydoc doxygen_hide_tparam_role_on_processor
     *
     * \param[in] felt_space The \a FEltSpace from which the \a LocalFEltSpace are extracted.
     * \param[in,out] list The list of \a LocalFEltSpace. The list might not be empty on input: for instance this
     * funciton may be used to aggregate a list encompassing several \a FEltSpace, with one call for each.
     *
     */
    template<RoleOnProcessor RoleOnProcessorT>
    void ExtractLocalFEltSpaceList(const FEltSpace& felt_space, LocalFEltSpace::vector_shared_ptr& list);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/FEltSpace.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HPP_
