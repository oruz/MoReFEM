/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class MoReFEMDataT>
    void GodOfDof::Init(const MoReFEMDataT& morefem_data,
                        FEltSpace::vector_unique_ptr&& a_felt_space_list,
                        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                        const FilesystemNS::Directory& output_directory,
                        const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching)
    {
        BeginInitialization(output_directory, std::move(a_felt_space_list));

        if constexpr (!MoReFEMDataT::HasParallelismField())
        {
            assert(morefem_data.GetParallelismPtr() == nullptr);
            StandardInit(morefem_data, coords_matching);
        } else
        {
            const auto parallelism_ptr = morefem_data.GetParallelismPtr();

            const auto parallelism_strategy = parallelism_ptr->GetParallelismStrategy();

            switch (parallelism_strategy)
            {
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::precompute:
            case Advanced::parallelism_strategy::none:
            case Advanced::parallelism_strategy::parallel_no_write:
            {
                StandardInit(morefem_data, coords_matching);

                break;
            }
            case Advanced::parallelism_strategy::run_from_preprocessed:
            {
                InitFromPreprocessedData(morefem_data);

                break;
            }
            }
        }

        FinalizeInitialization(do_consider_proc_wise_local_2_global, morefem_data.GetParallelismPtr());
    }


    template<class MoReFEMDataT>
    void GodOfDof::InitFromPreprocessedData(const MoReFEMDataT& morefem_data)
    {
        namespace ipl = Utilities::InputDataNS;

        decltype(auto) prepartitioned_data_dir_str =
            ipl::Extract<InputDataNS::Parallelism::Directory>::Path(morefem_data.GetInputData());

        decltype(auto) mpi = GetMpi();

        FilesystemNS::Directory prepartitioned_data_dir(
            mpi, prepartitioned_data_dir_str, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        FilesystemNS::Directory mesh_subdir(
            prepartitioned_data_dir, "Mesh_" + std::to_string(GetUniqueId()), __FILE__, __LINE__);

        const auto god_of_dof_prepartitioned_data_file = mesh_subdir.AddFile("god_of_dof_data.lua");

        LuaOptionFile god_of_dof_prepartitioned_data(god_of_dof_prepartitioned_data_file, __FILE__, __LINE__);

        InitFromPreprocessedDataHelper(god_of_dof_prepartitioned_data);
    }


    template<class MoReFEMDataT>
    void GodOfDof ::StandardInit(const MoReFEMDataT& morefem_data,
                                 const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching)
    {
        static_cast<void>(morefem_data);
        auto match_interface_node_bearer = InitNodeBearers();

        Reduce(match_interface_node_bearer, coords_matching);
        CreateNodesAndDofs(match_interface_node_bearer);
    }


    inline const Mesh& GodOfDof::GetMesh() const noexcept
    {
        return mesh_;
    }


    inline Mesh& GodOfDof::GetNonCstMesh() noexcept
    {
        return mesh_;
    }


    inline const FEltSpace::vector_unique_ptr& GodOfDof::GetFEltSpaceList() const noexcept
    {
        return felt_space_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetProcessorWiseNodeBearerList() const noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseNodeBearerList() noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstGhostNodeBearerList() noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetGhostNodeBearerList() const noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline std::size_t GodOfDof::NprogramWiseDof() const noexcept
    {
        return GetNdofHolder().NprogramWiseDof();
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetProcessorWiseDofList() const noexcept
    {
        return processor_wise_dof_list_;
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetGhostDofList() const noexcept
    {
        return ghost_dof_list_;
    }


    inline std::size_t GodOfDof::NprocessorWiseDof() const noexcept
    {
        assert(GetNdofHolder().NprocessorWiseDof() == processor_wise_dof_list_.size());
        return GetNdofHolder().NprocessorWiseDof();
    }


    inline std::size_t GodOfDof::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprocessorWiseDof(numbering_subset);
    }


    inline std::size_t GodOfDof::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprogramWiseDof(numbering_subset);
    }


    inline std::size_t GodOfDof::NprocessorWiseNodeBearer() const noexcept
    {
        return processor_wise_node_bearer_list_.size();
    }


    inline GodOfDof::shared_ptr GodOfDof::GetSharedPtr()
    {
        return shared_from_this();
    }


#ifndef NDEBUG
    inline bool GodOfDof::HasInitBeenCalled() const
    {
        return has_init_been_called_;
    }
#endif // NDEBUG


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseDofList() noexcept
    {
        return processor_wise_dof_list_;
    }


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstGhostDofList() noexcept
    {
        return ghost_dof_list_;
    }


    inline const NumberingSubset::vector_const_shared_ptr& GodOfDof::GetNumberingSubsetList() const noexcept
    {
        assert(!numbering_subset_list_.empty());
        assert(std::is_sorted(numbering_subset_list_.cbegin(),
                              numbering_subset_list_.cend(),
                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>()));

        return numbering_subset_list_;
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const std::string& GodOfDof::GetOutputDirectory() const noexcept
    {
        if constexpr (is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::no)
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectory();
        } else
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectory();
        }
    }


    inline const Internal::FEltSpaceNS::NdofHolder& GodOfDof::GetNdofHolder() const noexcept
    {
        assert(!(!Ndof_holder_));
        return *Ndof_holder_;
    }


    inline const Wrappers::Petsc::MatrixPattern&
    GodOfDof ::GetMatrixPattern(const NumberingSubset& numbering_subset) const
    {
        return GetMatrixPattern(numbering_subset, numbering_subset);
    }


    inline const NumberingSubset& GodOfDof::GetNumberingSubset(std::size_t unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }


#ifndef NDEBUG
    inline DoConsiderProcessorWiseLocal2Global GodOfDof::GetDoConsiderProcessorWiseLocal2Global() const
    {
        return do_consider_proc_wise_local_2_global_;
    }

#endif // NDEBUG


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition,
                                           GlobalMatrix& matrix) const
    {
        // These lines are because currently the ghosts dofs are present in the dof list, but for some
        // operations upon the matrices it is not allowed with the default behaviour of MAT_NO_OFF_PROC_ZERO_ROWS.
        // See in the upcoming refactoring of boundary conditions (#1581) if we need to keep this or if we can do things
        // more cleverly.
        PetscBool no_ghost_rows_allowed;
        matrix.GetOption(MAT_NO_OFF_PROC_ZERO_ROWS, &no_ghost_rows_allowed, __FILE__, __LINE__);
        matrix.SetOption(MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_FALSE, __FILE__, __LINE__);

        switch (BoundaryConditionMethodT)
        {
        case BoundaryConditionMethod::pseudo_elimination:
            ApplyPseudoElimination(boundary_condition, matrix);
            break;
        case BoundaryConditionMethod::penalization:
            ApplyPenalization(boundary_condition, matrix);
            break;
        }

        matrix.SetOption(MAT_NO_OFF_PROC_ZERO_ROWS, no_ghost_rows_allowed, __FILE__, __LINE__);
    }


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition,
                                           GlobalVector& vector) const
    {
        switch (BoundaryConditionMethodT)
        {
        case BoundaryConditionMethod::pseudo_elimination:
            ApplyPseudoElimination(boundary_condition, vector);
            break;
        case BoundaryConditionMethod::penalization:
            ApplyPenalization(boundary_condition, vector);
            break;
        }
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const std::string&
    GodOfDof ::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        if constexpr (is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::yes)
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        } else
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        }
    }


    inline const std::map<std::size_t, std::size_t>& GodOfDof::NprogramWiseDofPerNumberingSubset() const noexcept
    {
        return GetNdofHolder().NprogramWiseDofPerNumberingSubset();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
