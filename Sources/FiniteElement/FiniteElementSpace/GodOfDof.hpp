/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <map>
#include <memory>
#include <optional>

#include "Utilities/Mpi/Mpi.hpp"           // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp" // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp" // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"                         // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"      // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp" // TMP #1571
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hpp"            // IWYU pragma: export
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM
{
    
    class GodOfDofManager; // IWYU pragma: keep
    class GlobalMatrix;
    class GlobalVector;
    class GlobalDiagonalMatrix;
    class LuaOptionFile;
    class Mesh;
}

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }
namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS { class Model; }
namespace MoReFEM::Internal::FEltSpaceNS { class AssignGeomEltToProcessor; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    //! Whether output directory must be created or not.
    enum class create_output_dir
    {
        yes,
        no
    };


    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Object in charge of all the dofs related to a given \a Mesh.
     *
     * There is exactly one \a GodOfDof per \a Mesh; both objects share the same unique id. For instance
     * if in the input data file there is
     * \verbatim
        Mesh2 = { ... }
     \endverbatim
     * a \a Mesh AND a \a GodOfDof with an identifier of 2 are created.
     *
     * \a GodOfDof is also the place where all finite element spaces related to a given mesh are stored.
     */
    class GodOfDof final : public std::enable_shared_from_this<GodOfDof>,
                           public Crtp::CrtpMpi<GodOfDof>,
                           public Crtp::UniqueId<GodOfDof, UniqueIdNS::AssignationMode::manual>
    {

      public:
        //! Alias to shared pointer. It is a shared_pointer and not unique_ptr here as weak_pointers will be used in
        //! FEltSpace.
        using shared_ptr = std::shared_ptr<GodOfDof>;

        //! Alias to shared pointer. It is a shared_pointer and not unique_ptr here as weak_pointers will be used in
        //! FEltSpace.
        using const_shared_ptr = std::shared_ptr<const GodOfDof>;

        //! Convenient alias to one of the parent.
        using unique_id_parent = Crtp::UniqueId<GodOfDof, UniqueIdNS::AssignationMode::manual>;

        //! Name of the class.
        static const std::string& ClassName();

        //! Friendship.
        friend class GodOfDofManager;

        //! Friendship to test class,
        friend class TestNS::LoadPrepartitionedGodOfDofNS::Model;

        /// \name Special members.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * \internal <b><tt>[internal]</tt></b> It is private as construction should be driven by friend
         * GodOfDofManager. \endinternal
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] mesh Mesh upon which the \a GodOfDof is built.
         */
        explicit GodOfDof(const Wrappers::Mpi& mpi, Mesh& mesh);

      public:
        //! Destructor.
        ~GodOfDof() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GodOfDof(const GodOfDof& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GodOfDof(GodOfDof&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GodOfDof& operator=(const GodOfDof& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GodOfDof& operator=(GodOfDof&& rhs) = delete;


        ///@}


      public:
        /*!
         * \class doxygen_hide_coords_matching_arg
         *
         * \param[in] coords_matching If there is a \a FromCoordsMatching operator, the association between \a Coords of both meshes
         * involved.
         */


        /*!
         * \brief Fully init the GodOfDof.
         *
         * This means among others:
         * - Partition between processors the problem if code is run in parallel.
         * - Set the list of finite elements in each finite element space.
         * - Set the list of nodes here.
         *
         * \copydoc doxygen_hide_morefem_data_param
         * \param[in] felt_space_list List of all \a FEltSpace to consider in the \a GodOfDof.
         * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] output_directory Output directory for data specific to the mesh covered by the \a GodOfDof.
         * \copydoc doxygen_hide_coords_matching_arg
         */
        template<class MoReFEMDataT>
        void Init(const MoReFEMDataT& morefem_data,
                  FEltSpace::vector_unique_ptr&& felt_space_list,
                  DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                  const FilesystemNS::Directory& output_directory,
                  const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching);


        //! Get the mesh object.
        const Mesh& GetMesh() const noexcept;

        /*!
         * \brief Get access to the \a unique_id -th finite element space.
         *
         * It assumes the finite element space does exist in the GodOfDof; this can be checked beforehand with
         * IsFEltSpace(index).
         * \unique_id_param_in_accessor{FEltSpace}
         *
         * \return Returns the \a unique_id -th finite element space.
         */
        const FEltSpace& GetFEltSpace(std::size_t unique_id) const;

        //! Whether the finite element space \a unique_id is in the current \a GodOfDof.
        //! \unique_id_param_in_accessor{FEltSpace}
        bool IsFEltSpace(std::size_t unique_id) const;

        //! Get the list of FEltSpace.
        const FEltSpace::vector_unique_ptr& GetFEltSpaceList() const noexcept;

        //! Returns the list of node bearers present on local processor.
        const NodeBearer::vector_shared_ptr& GetProcessorWiseNodeBearerList() const noexcept;

        //! Returns the list of ghost node bearers.
        const NodeBearer::vector_shared_ptr& GetGhostNodeBearerList() const noexcept;

        //! Returns the number of processor-wise dofs (excluding ghosts).
        std::size_t NprocessorWiseDof() const noexcept;

        //! Returns the number of processor-wise dofs (excluding ghosts) within a given numbering subset.
        //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
        std::size_t NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

        //! Get the number of program-wise dofs.
        std::size_t NprogramWiseDof() const noexcept;

        //! Returns the number of program-wise dofs within a given numbering subset.
        //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
        std::size_t NprogramWiseDof(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Get the list of all processor-wise dofs.
         */
        const Dof::vector_shared_ptr& GetProcessorWiseDofList() const noexcept;

        /*!
         * \brief Get the list of all ghost dofs.
         */
        const Dof::vector_shared_ptr& GetGhostDofList() const noexcept;

        //! List of all numbering subsets, computed from the finite element spaces.
        const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const noexcept;


        //! Fetch the matrix pattern which rows are numbered by \a row_numbering_subset, and columns by \a
        //! column_numbering_subset. \param[in] row_numbering_subset \a NumberingSubset used for the rows. \param[in]
        //! column_numbering_subset \a NumberingSubset used for the columns.
        const Wrappers::Petsc::MatrixPattern& GetMatrixPattern(const NumberingSubset& row_numbering_subset,
                                                               const NumberingSubset& column_numbering_subset) const;


        //! Shorthand function for a matrix described only by one \a numbering_subset (square matrix).
        //! \param[in] numbering_subset Same \a NumberingSubset used for the rows and the columns.
        const Wrappers::Petsc::MatrixPattern& GetMatrixPattern(const NumberingSubset& numbering_subset) const;


        /*!
         * \brief Return the list of the dofs involved in an essential boundary condition.
         *
         * The dofs may be processor-wise or ghosts; as usual ghost are packed together at the end of the container.
         *
         * \attention There is a return-by-value as it is currently used only in State constructor; store that
         * somewhere if it is needed by something else!
         *
         * \param[in] numbering_subset Only dofs that belongs to this \a NumberingSubset are requested.
         *
         * \return List of dofs encompassed by the boundary conditions relared to \a numbering_subset.
         */
        Dof::vector_shared_ptr GetBoundaryConditionDofList(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Yield a shared_ptr to the current object.
         *
         * This requires that at least one shared_ptr to this object exists (see behaviour of
         * std::enable_shared_from_this for more details).
         *
         * \return Return shared_from_this().
         */
        GodOfDof::shared_ptr GetSharedPtr();


#ifndef NDEBUG
        /*!
         * \brief Whether Init() has already been called or not.
         *
         * Finite element spaces are fully initialized with the call to Init(), which among other things fill them
         * and partition the mesh. They must not be defined after this call; current debug attribute is there
         * to ensure it doesn't happen.
         *
         * \return Whether Init() has already been called or not.
         */
        bool HasInitBeenCalled() const;
#endif // NDEBUG

        //! Print the informations for each dof.
        //! \param[in] numbering_subset \a NumberingSubset for which dof informations will be printed.
        //! \copydoc doxygen_hide_stream_inout
        void PrintDofInformation(const NumberingSubset& numbering_subset, std::ostream& stream) const;

        //! Get the numbering subset which id is \a unique_id.
        //! \param[in] unique_id Unique identifier of the sought \a NumberingSubset (as returned by
        //! NumberingSubset::GetUniqueId()).
        const NumberingSubset& GetNumberingSubset(std::size_t unique_id) const;

        //! Get the numbering subset which id is \a unique_id as a smart pointer.
        //! \param[in] unique_id Unique identifier of the sought \a NumberingSubset (as returned by
        //! NumberingSubset::GetUniqueId()).
        const NumberingSubset::const_shared_ptr& GetNumberingSubsetPtr(std::size_t unique_id) const;

        //! Get the output directory.
        template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard = Internal::GodOfDofNS::wildcard_for_rank::no>
        const std::string& GetOutputDirectory() const noexcept;


        /*!
         * \brief Returns the name of the subfolder related to a given numbering subset.
         *
         * For instance
         *
         * \code
         * *path_to_output_directory* /NumberingSubset*i*
         * \endcode
         *
         * where i is the unique id of the numbering subset.
         *
         * \param[in] numbering_subset Numbering subset for which output folder is sought.
         *
         * \return Name of the subfolder related to a given numbering subset.
         */
        template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard = Internal::GodOfDofNS::wildcard_for_rank::no>
        const std::string& GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Clear the temporary data used to build properly the Internal::FEltNS::Local2GlobalStorage objects.
        void ClearTemporaryData() const noexcept;

        //! Apply the boundary condition method given in template argument.
        //! \tparam BoundaryConditionMethodT Method used to enforce the boundary condition.
        //! \param[in] boundary_condition \a DirichletBoundaryCondition to be applied.
        //! \param[in,out] matrix \a GlobalMatrix upon which the boundary condition will be enforced.
        template<BoundaryConditionMethod BoundaryConditionMethodT>
        void ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        //! Apply the boundary condition method given in template argument.
        //! \tparam BoundaryConditionMethodT Method used to enforce the boundary condition.
        //! \param[in] boundary_condition \a DirichletBoundaryCondition to be applied.
        //! \param[in,out] vector \a GlobalVector upon which the boundary condition will be enforced.
        template<BoundaryConditionMethod BoundaryConditionMethodT>
        void ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;


        /*!
         * \brief Apply one boundary condition by pseudo-elimination on a matrix.
         *
         * Relevant numbering subset is read from the matrix object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] matrix Matrix upon which the condition is applied.
         */
        void ApplyPseudoElimination(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        /*!
         * \brief Apply one boundary condition by pseudo-elimination on a vector.
         *
         * Relevant numbering subset is read from the vector object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] vector Vector upon which the condition is applied.
         */
        void ApplyPseudoElimination(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;

        /*!
         * \brief Apply one boundary condition by penalization on a matrix.
         *
         * Relevant numbering subset is read from the matrix object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] matrix Matrix upon which the condition is applied.
         */
        void ApplyPenalization(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const;

        /*!
         * \brief Apply one boundary condition by penalization on a vector.
         *
         * Relevant numbering subset is read from the vector object.
         *
         * \param[in] boundary_condition Boundary condition to be applied.
         * \param[in,out] vector Vector upon which the condition is applied.
         */
        void ApplyPenalization(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const;


        /*!
         * \brief Non constant access to the mesh object.
         *
         * \attention Access is public solely for the needs of FEltSpace::Movemesh(). You shouldn't call this accessor
         * at all unless you are modifying the core of MoReFLoEM; during the development of a model you shouldn't
         * use this one at all and prefer the constant accessor.
         *
         * \return \a Mesh on top of which the GodOfDof has been built.
         */
        Mesh& GetNonCstMesh() noexcept;


      private:
        //! Non constant access to the list of node bearers.
        NodeBearer::vector_shared_ptr& GetNonCstProcessorWiseNodeBearerList() noexcept;

        //! Non constant access to the list of ghost node bearers.
        NodeBearer::vector_shared_ptr& GetNonCstGhostNodeBearerList() noexcept;


        /*!
         * \brief Sort and reindex the \a NodeBearer so that they match what is expected for the parallel run.
         *
         * This method is expected to be called in a run from prepartitioned data, just after the \a NodeBearer have
         * been created from the reduced data. At this stage before the call, we get a list of \a NodeBearers, but we
         * don't know yet:
         * - Whether a given \a NodeBearer is processor-wise or ghost (by construct no other option!)
         * - Their indexes.
         *
         * These informations are properly filled by the method.
         *
         * \param[in] god_of_dof_prepartitioned_data The \a LuaOptionFile which contains the data required to
         * reconstruct properly the \a GodOfDof in a run fron preprocessed data. It should really be seen as an input
         * only - the fact it is not const is due to the constraints of the \a LuaOptionFile implementation.
         */
        void SetUpNodeBearersFromPrepartitionedData(LuaOptionFile& god_of_dof_prepartitioned_data);


        /*!
         * \brief Iterator to the finite element space which index is \a unique_id.
         *
         * Iterator might be the end of the list; this method is only for internal purposes.
         *
         * \param[in] unique_id Unique identifier of the \a FEltSpace (noticeably used in the input data file to
         * tag the finite element space).
         *
         * \return Iterator to the finite element space which index is \a unique_id.
         */
        auto GetIteratorFEltSpace(std::size_t unique_id) const;


      private:
        /*!
         * \brief Reduce to processor-wise data.
         *
         * \param[in] assign_geom_elt_to_processor The object which was used to determine which rank is in charge of
         * each \a GeometricElt.
         * \copydoc doxygen_hide_coords_matching_arg
         */
        void Reduce(const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                    const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching);

        /*!
         * \brief Create all the  \a NodeBearer objects.
         *
         * \internal And only those - their content (\a Node and \a Dof) is created later, after the reduction in
         * parallel mode.
         *
         * \return An helper object which is used during initialization.
         */
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer CreateNodeBearers();


        /*!
         * \brief Create  all the \a Nodes by iterating through all \a LocalFEltSpace of \a FEltSpace.
         *
         * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the initialisation
         * phase.
         *
         * \internal This method is expected to be called AFTER the reduction process occurred. After the call, all
         * relevant \a Node are created and they are properly sorted as processor-wise or ghost.
         *
         * \internal Don't call this method directly: it is automatically called in CreateNodesAndDofs().
         *
         */
        void CreateNodes(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


        /*!
         * \brief Compute all the indexes related to all of the \a Dof.
         *
         * This includes the indexes for each relevant \a NumbberingSubset.
         */
        void ComputeDofIndexes();


        /*!
         * \brief Reduce to processor-wise the finite elements in each finite element space, the mesh and computes
         * the ghost node bearers.
         *
         * \param[in] assign_geom_elt_to_processor The object which was used to determine which rank is in charge of
         * each \a GeometricElt. \param[in,out] match_interface_node_bearer Helper object which stores data useful for
         * the initialisation phase.
         *
         * The only quantity not yet reduced is the dofs involved in the boundary conditions.
         */
        void ReduceToProcessorWise(const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
                                   Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);

        //! Returns the number of node bearers.
        std::size_t NprocessorWiseNodeBearer() const noexcept;

        /*!
         * \brief Set for each concerned dof the associated value from a Dirichlet boundary condition.
         *
         * \internal <b><tt>[internal]</tt></b> Boundary conditions have not been discussed deeply yet and so
         * essential ones are still handled à la Felisce (which is not necessarily a bad thing here).
         * However, we depart much for the handling of natural ones.
         * \endinternal
         */
        void SetBoundaryConditions();

        // \todo #1581
        // void ExperimentalDetermineBCNodeBearer(const DirichletBoundaryCondition& boundary_condition);

        //! Get the list of all processor-wise dofs (ghost excluded).
        Dof::vector_shared_ptr& GetNonCstProcessorWiseDofList() noexcept;

        //! Get the list of all ghost dofs.
        Dof::vector_shared_ptr& GetNonCstGhostDofList() noexcept;

        /*!
         * \brief Iterate through all finite element spaces and compute the list of all numbering subsets.
         *
         * \return The generated list.
         */
        const NumberingSubset::vector_const_shared_ptr& ComputeNumberingSubsetList();


      private:
        /*!
         * \brief Prepare the subfolders for each numbering_subset.
         *
         * \param[in] parallelism Object which holds the parallelism strategy to use. or some of them, additional
         * informations will be written on disk.
         */
        void PrepareOutput(const Internal::Parallelism* parallelism);

        //! Accessor to NdofHolder.
        const Internal::FEltSpaceNS::NdofHolder& GetNdofHolder() const noexcept;
        /*!
         * \brief First part of the Init() method, that begins to initialize stuff but has not yet begun the reduction
         * to processor-wise.
         *
         * It is hence decoupled because this part may be compiled, contrary to the second one.
         *
         */
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer InitNodeBearers();


        /*!
         * \brief Third part of the Init() method, which mostly settles the data reduction to processor-size.
         *
         * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the initialisation
         * phase.
         */
        void CreateNodesAndDofs(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


        /*!
         * \brief Reduce to processor-wise and ghost data..
         *
         * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the initialisation
         * phase.
         * \copydoc doxygen_hide_coords_matching_arg
         */
        void Reduce(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
                    const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching);

        /*!
         * \brief First method to be called in an initialization process (\a Init()).
         *
         * This one:
         * - Set the list of \a FEltSpace
         * - Prepare the adequate output directories.
         * - Set the list of  \a NumberingSubset (through computation from the \a felt_space_list.
         *
         * \param[in] felt_space_list List of all \a FEltSpace to consider in the \a GodOfDof.
         * \param[in] output_directory Output directory for data specific to the mesh covered by the \a GodOfDof.
         *
         * <b>Prerequisite:</b> none - this method can be called at the very beginning of the initialization process.
         */
        void BeginInitialization(const FilesystemNS::Directory& output_directory,
                                 FEltSpace::vector_unique_ptr&& felt_space_list);


        /*!
         * \brief The method called by \a Init() for all parallelism strategy save  "run_from_preprocessed_data".
         *
         * \copydoc doxygen_hide_morefem_data_param
         * \copydoc doxygen_hide_coords_matching_arg
         *
         * <b>Prerequisite:</b> \a FEltSpace and output directories must have been initialized.
         *
         */
        template<class MoReFEMDataT>
        void StandardInit(const MoReFEMDataT& morefem_data,
                          const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching);


        /*!
         * \brief The method called by \a Init() when the parallelism strategy is "run_from_preprocessed_data"
         *
         * \copydoc doxygen_hide_morefem_data_param
         *
         * <b>Prerequisite:</b> \a FEltSpace and output directories must have been initialized.
         *
         */
        template<class MoReFEMDataT>
        void InitFromPreprocessedData(const MoReFEMDataT& morefem_data);

        /*!
         * \brief The method that does the heavy bulk of \a InitFromPreprocessedData - once the correct data have been
         * extracted from the input data file.
         *
         * The perk is that this method is compiled, leaving only a very restrained part in the \a
         * InitFromPreprocessedData method.
         *
         * \param[in] god_of_dof_prepartioned_data The \a LuaOptionFile which contains the data required to reconstruct
         * properly the \a GodOfDo in a run fron preprocessed data. It should really be seen as an input only - the fact
         * it is not const is due to the constraints of the \a LuaOptionFile implementation.
         *
         */
        void InitFromPreprocessedDataHelper(LuaOptionFile& god_of_dof_prepartioned_data);

        /*!
         * \brief Finalize the initialization of the \a GodOfDof (through \a Init() call).
         *
         * \copydoc doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] parallelism Object which holds the parallelism strategy to use. or some of them, additional
         * informations will be written on disk.
         */
        void FinalizeInitialization(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                                    const Internal::Parallelism* parallelism);


      public:
        //! Number of program-wise dofs per \a NumberingSubset.
        //! You shouldn't need this: it is useful only for reloading data for two-steps parallelism.
        const std::map<std::size_t, std::size_t>& NprogramWiseDofPerNumberingSubset() const noexcept;


      public:
#ifndef NDEBUG
        /*!
         * \brief Whether local2global arrays for processor-wise indexes might be required or not.
         *
         * yes doesn't mean they are already computed, but is a mandatory prerequisite should they be required.
         *
         * \return yes if local2global arrays for processor-wise indexes are to be considered.
         */
        DoConsiderProcessorWiseLocal2Global GetDoConsiderProcessorWiseLocal2Global() const;
#endif // NDEBUG

      private:
        //! List of all finite element spaces related to the mesh covered by GodOfDof.
        FEltSpace::vector_unique_ptr felt_space_list_;

        //! List of all numbering subsets, computed from the finite element spaces.
        NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

        //! Mesh covered by the current object.
        Mesh& mesh_;

        //! List of all node bearers. Each one is present only once here; ghost aren't present in this one.
        NodeBearer::vector_shared_ptr processor_wise_node_bearer_list_;

        //! List of all ghost node bearers, that is all nodes required for calculation but not hosted by local
        //! processor.
        NodeBearer::vector_shared_ptr ghost_node_bearer_list_;

        //! Objects that counts the number of dofs in several configurations.
        Internal::FEltSpaceNS::NdofHolder::const_unique_ptr Ndof_holder_ = nullptr;

        //! Object in charge of storing output directories.
        Internal::GodOfDofNS::OutputDirectoryStorage::const_unique_ptr output_directory_storage_ = nullptr;

        //! Object in charge of storing output directories with wildcarfs instead of rank id (for use in output files).
        Internal::GodOfDofNS::OutputDirectoryStorage::const_unique_ptr output_directory_wildcard_storage_ = nullptr;

        /*!
         * \brief List of processor-wise dofs.
         *
         * Ghost are excluded and handled in ghost_dof_list_.
         *
         * \internal <b><tt>[internal]</tt></b> This information is redundant with the one from node_per_felt_,
         * but it allows to use a slicker interface (direct loop instead of three imbricated ones). // \todo #258
         * \endinternal
         */
        Dof::vector_shared_ptr processor_wise_dof_list_;

        /*!
         * \brief List of processor-wise dofs.
         *
         * \internal <b><tt>[internal]</tt></b> This information is redundant with the one from node_per_felt_,
         * but it allows to use a slicker interface (direct loop instead of three imbricated ones).
         * \endinternal
         */
        Dof::vector_shared_ptr ghost_dof_list_;

        //! CSR Pattern of the matrix.
        Internal::FEltSpaceNS::MatrixPattern::vector_const_unique_ptr matrix_pattern_per_numbering_subset_;

        //! Path of the file listing the time iterations and the related files.
        std::string time_iteration_file_;

#ifndef NDEBUG
        /*!
         * \brief Whether Init() has already been called or not.
         *
         * Finite element spaces are fully initialized with the call to Init(), which among other things fill them
         * and partition the mesh. They must not been defined after this call; current debug attribute is there
         * to ensure it doesn't happen.
         */
        bool has_init_been_called_ = false;

        /*!
         * \brief Whether local2global arrays for processor-wise indexes might be required or not.
         *
         * yes doesn't mean they are already computed, but is a mandatory prerequisite should they be required.
         */
        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global_ =
            DoConsiderProcessorWiseLocal2Global::no;

#endif // NDEBUG
    };


    /*!
     * \brief Print the ids of node bearers on the local processor.
     *
     * \param[in] node_bearer_list List of node bearers to print.
     * \param[in] rank Rank of the current processor.
     * \copydoc doxygen_hide_stream_inout
     */
    void PrintNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list,
                             std::size_t rank,
                             std::ostream& stream = std::cout);


    /*!
     * \brief Allocate the underlying (parallel) pattern of a matrix.
     *
     * GlobalMatrix constructor only defines the related numbering subsets, but the Petsc matrix isn't yet allocated.
     *
     * Purpose of the current function is to finish the work and allocate the pattern of the matrix, possibly over
     * several processors in parallelism is involved.
     *
     * \param[in] god_of_dof God of dof upon which both numbering subsets of \a matrix are defined.
     * \param[in,out] matrix Matrix being allocated. On input, respective numbering subsets for rows and columns
     * are already defined; \a god_of_dof will use this information to properly allocate the underlying Petsc matrix.
     */
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalMatrix& matrix);


    /*!
     *
     * \copydoc MoReFEM::AllocateGlobalMatrix()
     */
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalDiagonalMatrix& matrix);


    /*!
     * \brief Allocate the underlying (parallel) pattern of a vector.
     *
     * GlobalVector constructor only defines the related numbering subset, but the Petsc vectors isn't yet allocated.
     *
     * Purpose of the current function is to finish the work and allocate the pattern of the vector, possibly over
     * several processors in parallelism is involved.
     *
     * \param[in] god_of_dof God of dof upon which numbering subset of \a vector are defined.
     * \param[in,out] vector Vector being allocated. On input, numbering subset is already defined; \a god_of_dof will
     * use this information to properly allocate the underlying Petsc vector.
     */
    void AllocateGlobalVector(const GodOfDof& god_of_dof, GlobalVector& vector);


#ifndef NDEBUG
    /*!
     * \copydoc doxygen_hide_assert_matrix_respect_pattern_function
     *
     * \param[in] god_of_dof GodOfDof onto which the matrix is defined.
     */
    void AssertMatrixRespectPattern(const GodOfDof& god_of_dof,
                                    const GlobalMatrix& matrix,
                                    const char* invoking_file,
                                    int invoking_line);
#endif // NDEBUG


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/GodOfDof.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HPP_
