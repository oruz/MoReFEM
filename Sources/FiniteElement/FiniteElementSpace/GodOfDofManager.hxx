/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class MeshSectionT>
    void GodOfDofManager::Create(const MeshSectionT& section, const Wrappers::Mpi& mpi)
    {
        const auto unique_id = section.GetUniqueId();

        auto& mesh = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetNonCstMesh(unique_id);

        Create(mpi, mesh);
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(std::size_t unique_id) const
    {
        return *(GetGodOfDofPtr(unique_id));
    }


    inline GodOfDof& GodOfDofManager::GetNonCstGodOfDof(std::size_t unique_id)
    {
        return const_cast<GodOfDof&>(GetGodOfDof(unique_id));
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(const Mesh& mesh) const
    {
        return GetGodOfDof(mesh.GetUniqueId());
    }


    inline const auto& GodOfDofManager::GetStorage() const noexcept
    {
        return list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
