/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:45:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<RoleOnProcessor RoleOnProcessorT>
    inline const LocalFEltSpacePerRefLocalFEltSpace& FEltSpace::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
    {
        return GetFEltStorage().GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>();
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline const LocalFEltSpacePerRefLocalFEltSpace&
    FEltSpace::GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const
    {
        return GetFEltStorage().GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>(domain);
    }


    inline const Dof::vector_shared_ptr& FEltSpace::GetProcessorWiseDofList() const noexcept
    {
        assert(are_dof_list_computed_
               && "If not, you may be in the moment where finite element space has been "
                  "reduced but the dofs were not recomputed (you are probably meddling with GodOfDof initialization - "
                  "if not issue a ticket so that library developer may investigate");
        return dof_list_;
    }


    inline const Dof::vector_shared_ptr& FEltSpace::GetGhostDofList() const noexcept
    {
        assert(are_dof_list_computed_
               && "If not, you may be in the moment where finite element space has been "
                  "reduced but the dofs were not recomputed (you are probably meddling with GodOfDof initialization - "
                  "if not issue a ticket so that library developer may investigate");

        return ghost_dof_list_;
    }


    inline Dof::vector_shared_ptr& FEltSpace::GetNonCstProcessorWiseDofList() noexcept
    {
        assert(!are_dof_list_computed_ && "The only case it should be called is the initialization!");
        return dof_list_;
    }


    inline Dof::vector_shared_ptr& FEltSpace::GetNonCstGhostDofList() noexcept
    {
        assert(!are_dof_list_computed_ && "The only case it should be called is the initialization!");
        return ghost_dof_list_;
    }


    inline std::size_t FEltSpace::GetDimension() const noexcept
    {
        return dimension_;
    }


    inline const Internal::FEltSpaceNS::Storage& FEltSpace::GetFEltStorage() const noexcept
    {
        assert(!(!felt_storage_));
#ifndef NDEBUG
        AssertGodOfDofInitialized();
#endif // NDEBUG

        return *felt_storage_;
    }


    inline Internal::FEltSpaceNS::Storage& FEltSpace::GetNonCstFEltStorage() noexcept
    {
        return const_cast<Internal::FEltSpaceNS::Storage&>(GetFEltStorage());
    }


    inline std::shared_ptr<const GodOfDof> FEltSpace::GetGodOfDofFromWeakPtr() const
    {
        assert(!god_of_dof_.expired());
        return god_of_dof_.lock();
    }


    inline const ExtendedUnknown::vector_const_shared_ptr& FEltSpace::GetExtendedUnknownList() const noexcept
    {
        return extended_unknown_list_;
    }


    inline const NumberingSubset& FEltSpace::GetNumberingSubset(const Unknown& unknown) const
    {
        return GetExtendedUnknown(unknown).GetNumberingSubset();
    }


    inline const NumberingSubset::const_shared_ptr& FEltSpace::GetNumberingSubsetPtr(const Unknown& unknown) const
    {
        return GetExtendedUnknown(unknown).GetNumberingSubsetPtr();
    }


    inline const ExtendedUnknown& FEltSpace::GetExtendedUnknown(const Unknown& unknown) const
    {
        return *GetExtendedUnknownPtr(unknown);
    }


    inline const NumberingSubset::vector_const_shared_ptr& FEltSpace::GetNumberingSubsetList() const noexcept
    {
        assert(std::none_of(numbering_subset_list_.cbegin(),
                            numbering_subset_list_.cend(),
                            Utilities::IsNullptr<NumberingSubset::const_shared_ptr>));

        return numbering_subset_list_;
    }


    inline const LocalFEltSpace::per_geom_elt_index&
    FEltSpace::GetLocalFEltSpaceList(const RefGeomElt& ref_geom_elt) const
    {
        const auto& ref_local_felt_space = GetRefLocalFEltSpace(ref_geom_elt);
        return GetLocalFEltSpaceList(ref_local_felt_space);
    }


    inline void
    FEltSpace ::ComputeLocal2Global(ExtendedUnknown::const_shared_ptr&& unknown_ptr,
                                    DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const
    {
        ExtendedUnknown::vector_const_shared_ptr buf{ std::move(unknown_ptr) };
        return ComputeLocal2Global(buf, do_compute_processor_wise_local_2_global);
    }


    inline const std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper>&
    FEltSpace ::GetMovemeshHelperStorage() const noexcept
    {
        return movemesh_helper_data_;
    }


    inline std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper>&
    FEltSpace ::GetNonCstMovemeshHelperStorage() noexcept
    {
        return const_cast<std::map<std::size_t, Internal::FEltSpaceNS::MovemeshHelper>&>(GetMovemeshHelperStorage());
    }


    inline void FEltSpace::MoveMesh(const GlobalVector& vector) const
    {
        GetMovemeshHelper(vector).Movemesh(vector, Internal::FEltSpaceNS::MovemeshHelper::From::current_mesh);
    }


    inline void FEltSpace::MoveMeshFromInitialPosition(const GlobalVector& vector) const
    {
        GetMovemeshHelper(vector).Movemesh(vector, Internal::FEltSpaceNS::MovemeshHelper::From::initial_mesh);
    }


    inline const QuadratureRulePerTopology& FEltSpace::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!default_quadrature_rule_per_topology_));
        return *default_quadrature_rule_per_topology_;
    }


    inline const QuadratureRulePerTopology* FEltSpace::GetQuadratureRulePerTopologyRawPtr() const noexcept
    {
        assert(!(!default_quadrature_rule_per_topology_));
        return default_quadrature_rule_per_topology_.get();
    }


    inline const Domain& FEltSpace::GetDomain() const noexcept
    {
        return domain_;
    }


    template<RoleOnProcessor RoleOnProcessorT>
    std::size_t FEltSpace::FillLocalFEltSpaceList(const Mesh& mesh,
                                                  const Domain& domain,
                                                  const Internal::RefFEltNS::RefLocalFEltSpace& ref_local_felt_space,
                                                  LocalFEltSpace::per_geom_elt_index& local_felt_space_list) const
    {
        assert(local_felt_space_list.empty());

        constexpr auto bag_role_type = RoleOnProcessorT == RoleOnProcessor::processor_wise
                                           ? RoleOnProcessorPlusBoth::processor_wise
                                           : RoleOnProcessorPlusBoth::ghost;

        decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<bag_role_type>();

        decltype(auto) ref_geom_elt = ref_local_felt_space.GetRefGeomElt();

        {
            // Do this check: GetSubsetGeometricEltList() throws an exception if there are no \a GeometricElt that fits
            // the bill.
            const auto ref_geom_elt_it = std::find_if(ref_geom_elt_list.cbegin(),
                                                      ref_geom_elt_list.cend(),
                                                      [&ref_geom_elt](const auto& item_ptr)
                                                      {
                                                          assert(!(!item_ptr));
                                                          return *item_ptr == ref_geom_elt;
                                                      });

            if (ref_geom_elt_it == ref_geom_elt_list.cend())
                return 0ul;
        }

        auto geometric_elt_range = mesh.GetSubsetGeometricEltList<RoleOnProcessorT>(ref_geom_elt);

        assert(geometric_elt_range.second > geometric_elt_range.first);

        for (auto it = geometric_elt_range.first; it != geometric_elt_range.second; ++it)
        {
            const auto& geometric_elt_ptr = *it;
            assert(!(!geometric_elt_ptr));
            const auto& geometric_elt = *geometric_elt_ptr;
            assert(geometric_elt.GetIdentifier() == ref_geom_elt.GetIdentifier());

            if (domain.IsGeometricEltInside(geometric_elt))
            {
                auto&& ptr = new LocalFEltSpace(ref_local_felt_space, geometric_elt_ptr);
                local_felt_space_list.insert({ geometric_elt.GetIndex(), LocalFEltSpace::shared_ptr(ptr) });
            }
        }

        return static_cast<std::size_t>(geometric_elt_range.second - geometric_elt_range.first);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    void ExtractLocalFEltSpaceList(const FEltSpace& felt_space, LocalFEltSpace::vector_shared_ptr& list)
    {
        decltype(auto) local_felt_space_in_storage =
            felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>();

        for (const auto& [ref_local_felt_space_ptr, local_felt_space_per_geom_elt_index] : local_felt_space_in_storage)
        {
            for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_per_geom_elt_index)
                list.push_back(local_felt_space_ptr);
        }

        Utilities::EliminateDuplicate(list,
                                      Utilities::PointerComparison::Less<LocalFEltSpace::shared_ptr>(),
                                      Utilities::PointerComparison::Equal<LocalFEltSpace::shared_ptr>());
    }


    inline std::size_t FEltSpace::NprocessorWiseDof() const noexcept
    {
        const auto ret = GetNdofHolder().NprocessorWiseDof();
        assert(ret == GetProcessorWiseDofList().size());
        return ret;
    }


    inline std::size_t FEltSpace::NghostDof() const noexcept
    {
        return GetGhostDofList().size();
    }


    inline std::size_t FEltSpace::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprocessorWiseDof(numbering_subset);
    }


    inline std::size_t FEltSpace::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprogramWiseDof(numbering_subset);
    }


    inline const Internal::FEltSpaceNS::NdofHolder& FEltSpace::GetNdofHolder() const noexcept
    {
        assert(are_dof_list_computed_);
        assert(!(!Ndof_holder_));
        return *Ndof_holder_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_
