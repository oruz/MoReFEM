/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <iterator>
#include <memory>
#include <numeric>
#include <optional>
#include <set>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Core/Enum.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/IdentifyGhostNodeBearerFromCoordsMatching.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/AssignGeomEltToProcessor.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Partition.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/ReduceToProcessorWise.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    void GodOfDof::BeginInitialization(const FilesystemNS::Directory& output_directory,
                                       FEltSpace::vector_unique_ptr&& felt_space_list)
    {
        assert(!HasInitBeenCalled() && "Must be called only once per GodOfDof!");

#ifndef NDEBUG
        has_init_been_called_ = true;

        assert(std::none_of(
            felt_space_list.cbegin(), felt_space_list.cend(), Utilities::IsNullptr<FEltSpace::unique_ptr>));
#endif // NDEBUG

        felt_space_list_ = std::move(felt_space_list);

        decltype(auto) numbering_subset_list = ComputeNumberingSubsetList();

        output_directory_storage_ =
            std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(output_directory, numbering_subset_list);

        output_directory_wildcard_storage_ = std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(
            output_directory, numbering_subset_list, Internal::GodOfDofNS::wildcard_for_rank::yes);
    }


    namespace // anonymous
    {


        void CreateNodeBearerListHelper(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                        NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                                        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


        void AddNodeFromStorage(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


        void ProduceDofList(const NodeBearer::vector_shared_ptr& node_bearer_list, Dof::vector_shared_ptr& dof_list);


    } // namespace


    void GodOfDof::CreateNodes(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        decltype(auto) felt_space_list = GetFEltSpaceList();

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));

            AddNodeFromStorage(felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>(),
                               match_interface_node_bearer);

            // This one may really be needed when we are working from preprocessed data (in a normal run the
            // method is called before reduction and therefore the first argument is empty).
            AddNodeFromStorage(felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>(),
                               match_interface_node_bearer);
        }
    }


    Internal::FEltSpaceNS::MatchInterfaceNodeBearer GodOfDof::InitNodeBearers()
    {
        const auto& mpi = GetMpi();

        // Create the nodes for all finite element spaces.
        auto match_interface_node_bearer = CreateNodeBearers();

        auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList();

        // Compute the required elements to perform the partition. Parmetis call for instance is done there.
        // Node bearer list is still the same length in output (reduction not yet done) but its elements are sort
        // differently: those to be on first processor comes first, then those on second one, and so on...
        // Program-wise numbering is also applied: each of them gets as program-wise index its position in the new
        // vector.
        if (mpi.Nprocessor<int>() > 1)
        {
            Internal::FEltSpaceNS::PreparePartition(mpi, GetFEltSpaceList(), node_bearer_list);
        } else
        {
            for (auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                node_bearer_ptr->SetProcessor(0ul);
            }
        }

        return match_interface_node_bearer;
    }


    void GodOfDof::Reduce(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
                          const std::optional<MeshNS::InterpolationNS::CoordsMatching>& coords_matching)
    {
        decltype(auto) mpi = GetMpi();
        const auto rank = mpi.GetRank<std::size_t>();

        // If there is a CoordsMatching operator, list the \a NodeBearer that should be kept as ghost - some of them
        // would have been dropped otherwise.
        Internal::NodeBearerNS::node_bearer_per_coords_index_list_type ghost_from_coords_matching_node_bearer_list;
        if ((mpi.Nprocessor<int>() > 1) && coords_matching.has_value())
        {
            ghost_from_coords_matching_node_bearer_list =
                Internal::GodOfDofNS::IdentifyGhostNodeBearerFromCoordsMatching(*this, coords_matching.value());

            match_interface_node_bearer.SetFromCoordsMatchingNodeBearers(
                mpi, std::move(ghost_from_coords_matching_node_bearer_list));
        }

        // Assign a processor for each \a GeometricElt.
        Internal::FEltSpaceNS::AssignGeomEltToProcessor assign_geom_elt_to_processor(
            *this, mpi.Nprocessor<std::size_t>(), match_interface_node_bearer);

        // Reduce the list of node bearers to the ones present on processor.
        // Pattern computation will correctly address those not in the list as it will query finite elements to find
        // the adequate columns to fill with non-zero.
        Internal::FEltSpaceNS::ReduceNodeBearerList(rank, GetNonCstProcessorWiseNodeBearerList());

        // - Reduce the list of finite elements in each finite element space to the processor-wise ones only.
        // - Compute the ghost node bearers for each finite element space.
        // - Compute the reduced mesh, limited to processor-wise geometric elements.
        if (mpi.Nprocessor<int>() > 1)
            ReduceToProcessorWise(assign_geom_elt_to_processor, match_interface_node_bearer);
    }


    void GodOfDof::CreateNodesAndDofs(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        decltype(auto) mpi = GetMpi();

        decltype(auto) felt_space_list = GetFEltSpaceList();
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        CreateNodes(match_interface_node_bearer);

        ProduceDofList(node_bearer_list, GetNonCstProcessorWiseDofList());

        ProduceDofList(GetGhostNodeBearerList(), GetNonCstGhostDofList());

        Ndof_holder_ =
            std::make_unique<Internal::FEltSpaceNS::NdofHolder>(mpi, GetProcessorWiseDofList(), numbering_subset_list);

        ComputeDofIndexes();

        matrix_pattern_per_numbering_subset_ = Internal::FEltSpaceNS::ComputeMatrixPattern::Perform(
            felt_space_list, node_bearer_list, numbering_subset_list, GetNdofHolder());

        {
            for (const auto& felt_space_ptr : felt_space_list)
                felt_space_ptr->ComputeDofList();
        }

        {
            const auto& boundary_condition_list =
                DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();

            for (auto& boundary_condition_ptr : boundary_condition_list)
            {
                assert(!(!boundary_condition_ptr));
                boundary_condition_ptr->ComputeDofList();
            }
        }
    }


    namespace // anonymous
    {


        //! Print informations about the partition that has been performed.
        void PrintInformations(const FEltSpace::vector_unique_ptr& felt_space_list,
                               const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                               const std::size_t Nprogram_wise_dof,
                               const std::size_t Nprocessor_wise_dof,
                               const Wrappers::Mpi& mpi);


    } // namespace


    void GodOfDof::FinalizeInitialization(DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                                          const Internal::Parallelism* parallelism)
    {
#ifndef NDEBUG
        do_consider_proc_wise_local_2_global_ = do_consider_proc_wise_local_2_global;
#endif // NDEBUG

        decltype(auto) mpi = GetMpi();

        decltype(auto) felt_space_list = GetFEltSpaceList();
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();

        // Finish the reduction to processor-wise: remove dof in boundary conditions that are not processor-wise.
        if (mpi.Nprocessor<int>() > 1)
        {
            // Reduce boundary conditions to processor-wise and ghost ones.
            auto& bc_list = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();

            for (auto& bc_ptr : bc_list)
            {
                assert(!(!bc_ptr));
                auto& bc = *bc_ptr;

                // God of dof should only reduce its own boundary conditions.
                if (bc.GetDomain().GetMeshIdentifier() == GetUniqueId())
                    bc.ShrinkToProcessorWise(*this);
            }
        }

        PrintInformations(
            felt_space_list, node_bearer_list, GetGhostNodeBearerList(), NprogramWiseDof(), NprocessorWiseDof(), mpi);

        // Init the local2global for each local finite element space in each finite element space.
        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            felt_space_ptr->InitLocal2Global(do_consider_proc_wise_local_2_global);
        }

        // Prepare if relevant the data to compute movemeshes in finite element spaces.
        const auto mesh_dimension = GetMesh().GetDimension();

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            auto& felt_space = *felt_space_ptr;

            // It makes little sense to move only contours of the mesh!
            if (mesh_dimension != felt_space.GetDimension())
                continue;

            felt_space.SetMovemeshData();
        }

        SetBoundaryConditions();

        // Prepare the output directories.
        PrepareOutput(parallelism);

#ifndef NDEBUG
        if (mpi.Nprocessor<int>() == 1)
            assert(NprocessorWiseDof() == NprogramWiseDof());
        else
        {
            assert(mpi.AllReduce(NprocessorWiseDof(), Wrappers::MpiNS::Op::Sum) == NprogramWiseDof());
            //            assert(mpi.AllReduce(NnodeBearer(), Wrappers::MpiNS::Op::Sum) == Nprogram_wise_node_bearer);
        }
#endif // NDEBUG
    }


    Internal::FEltSpaceNS::MatchInterfaceNodeBearer GodOfDof::CreateNodeBearers()
    {
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer ret;

        const auto& felt_space_list = GetFEltSpaceList();

        {
            auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList(); // no processor-wise or ghost at this stage
                                                                             // - current call is expected to be before
                                                                             // reduction.
            assert(node_bearer_list.empty());


            for (const auto& felt_space_ptr : felt_space_list)
            {
                assert(!(!felt_space_ptr));

                CreateNodeBearerListHelper(
                    felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>(),
                    node_bearer_list,
                    ret);

                // This one may really be needed when we are working from preprocessed data (in a normal run the
                // method is called before reduction and therefore the first argument is empty).
                CreateNodeBearerListHelper(
                    felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>(),
                    node_bearer_list,
                    ret);
            }
        }

        // Constitute here the list of indexes concerned by BC.
        // \todo #619 This is rather awkward: it would be better to set completely the boundary condition
        // after the reduction to processor-wise, at the beginning of the method SetBoundaryCondition().
        // However it means severing the dependancy here upon 'ret'; actually only two data attributes are
        // used in the method ComputeNodeBearerListOnBoundary() and it should be relatively easy to avoid this.
        const auto& mesh = GetMesh();

        const auto& boundary_condition_list =
            DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();

        std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr> bc_data;

        for (auto& boundary_condition_ptr : boundary_condition_list)
        {
            assert(!(!boundary_condition_ptr));
            auto& boundary_condition = *boundary_condition_ptr;

            boundary_condition.SetNodeBearerList(
                ret.ComputeNodeBearerListOnBoundary(mesh, boundary_condition, bc_data));
        }

        ret.SetBoundaryConditionData(std::move(bc_data));

        return ret;
    }


    namespace // anonymous
    {


        std::size_t FirstProgramWiseIndexOnRank(const Wrappers::Mpi& mpi,
                                                const Internal::FEltSpaceNS::NdofHolder& Ndof_holder,
                                                const NumberingSubset& numbering_subset);

        void FillGhostDofProgramWiseIndexes(const Wrappers::Mpi& mpi,
                                            const NumberingSubset& numbering_subset,
                                            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                            const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

        /*!
         * \brief The class holding the method that will actually compute the dof indexes for all the nodes of a list.
         *
         * There is a struct to ease friendship declaration.
         *
         */
        struct ComputeDofIndexesHelper final
        {

            enum class in_numbering_subset
            {
                yes,
                no
            };

            /*!
             * \brief Static method that does the actual work.
             *
             * \tparam TypeDofIndexT Whether we are computing program_wise or processor (or ghost)-wise indexes.
             * \tparam DoProduceDofListT Whether \a complete_dof_list is filled or not.
             *
             * \param[in] node_bearer_list List of node bearers for which dof indexes are computed. It is expected the
             * structure of the node bearers (its nodes, the exact number of dofs) is already initialized. \param[in]
             * dof_numbering_scheme When considering a vectorial unknown, this argument reflects the choice of
             * numbering. Two possibilities at the moment: components of a same Node are given contiguous indexes or on
             * the contrary all dofs related to a same component (for instance displacement/x) are contiguous, then all
             * of the following component. \param[in,out] current_dof_index In input, the starting point of the
             * numbering. In output, the highest dof index attributed. The reason for this is simply for processor-wise
             * and ghosts: there are two consecutive calls, the first on node on processor, the second on ghost nodes,
             * and the numbering of ghosts is expected to begin where the one of node ended. \param[in]
             * numbering_subset_ptr Pointer to a numbering subset if computation is performed for a numbering subset. If
             * not, put nullptr and no numbering subset filtering will be applied. \param[out] complete_dof_list List of
             * all dofs created, in the order of their creation. Only if DoProduceDofListT is yes.
             */
            template<in_numbering_subset IndexInNumberingSubsetT, RoleOnProcessor RoleOnProcessorT>
            static void Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                DofNumberingScheme dof_numbering_scheme,
                                std::size_t& current_dof_index,
                                const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                                std::optional<std::size_t> first_program_wise_index = std::nullopt);
        };


    } // namespace


    void GodOfDof::ComputeDofIndexes()
    {
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();
        decltype(auto) ghost_node_bearer_list = GetGhostNodeBearerList();
        const auto& numbering_subset_list = GetNumberingSubsetList();

        decltype(auto) Ndof_holder = GetNdofHolder();
        decltype(auto) mpi = GetMpi();

        {
            std::size_t current_processor_wise_or_ghost_dof_index = 0ul;

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::no,
                                              RoleOnProcessor::processor_wise>(
                node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                nullptr);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::no, RoleOnProcessor::ghost>(
                ghost_node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                nullptr);
        }

        assert(IsConsistentOverRanks(mpi, numbering_subset_list));

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            std::size_t current_processor_wise_or_ghost_dof_index = 0ul;

            const auto first_program_wise_index_on_rank =
                FirstProgramWiseIndexOnRank(mpi, Ndof_holder, *numbering_subset_ptr);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::yes,
                                              RoleOnProcessor::processor_wise>(
                node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                numbering_subset_ptr,
                first_program_wise_index_on_rank);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::yes,
                                              RoleOnProcessor::ghost>(ghost_node_bearer_list,
                                                                      DofNumberingScheme::contiguous_per_node,
                                                                      current_processor_wise_or_ghost_dof_index,
                                                                      numbering_subset_ptr,
                                                                      first_program_wise_index_on_rank);


            FillGhostDofProgramWiseIndexes(mpi, numbering_subset, node_bearer_list, ghost_node_bearer_list);
        }
    }


    void GodOfDof ::ReduceToProcessorWise(
        const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        const auto& mpi = GetMpi();

        assert(GetMpi().Nprocessor<int>() > 1 && "Should not be called otherwise!");
        assert(GetGhostNodeBearerList().empty());

        Internal::FEltSpaceNS::ReduceToProcessorWise::Perform(mpi,
                                                              assign_geom_elt_to_processor,
                                                              GetFEltSpaceList(),
                                                              GetProcessorWiseNodeBearerList(),
                                                              match_interface_node_bearer,
                                                              GetNonCstGhostNodeBearerList(),
                                                              GetNonCstMesh());

        Internal::FEltSpaceNS::BroadcastGhostNodeBearerIndexList(
            mpi, GetProcessorWiseNodeBearerList(), GetGhostNodeBearerList());
    }


    namespace // anonymous
    {


        void PrintInformations(const FEltSpace::vector_unique_ptr& felt_space_list,
                               const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                               const std::size_t Nprogram_wise_dof,
                               const std::size_t Nprocessor_wise_dof,
                               const Wrappers::Mpi& mpi)
        {
            std::size_t Nfelt = 0;

            {
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    const auto& felt_list_per_type =
                        felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

                    for (const auto& pair : felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;

                        Nfelt += local_felt_space_list.size();
                    }
                }
            }

            const std::size_t rank = mpi.GetRank<std::size_t>();

            if (rank == 0)
                std::cout << "Total (program-wise) number of dofs = " << Nprogram_wise_dof << std::endl;

            std::cout << "On processor " << rank << " -> " << Nfelt << " finite elements, "
                      << processor_wise_node_bearer_list.size() << " node_bearers, " << Nprocessor_wise_dof << " dofs, "
                      << ghost_node_bearer_list.size() << " ghost node_bearers." << std::endl;
        }


        std::size_t FirstProgramWiseIndexOnRank(const Wrappers::Mpi& mpi,
                                                const Internal::FEltSpaceNS::NdofHolder& Ndof_holder,
                                                const NumberingSubset& numbering_subset)
        {
            const auto Nprocessor_wise_index_per_rank =
                mpi.CollectFromEachProcessor(Ndof_holder.NprocessorWiseDof(numbering_subset));

            const auto begin = Nprocessor_wise_index_per_rank.cbegin();

            return std::accumulate(
                begin, begin + mpi.GetRank<decltype(Nprocessor_wise_index_per_rank)::difference_type>(), 0ul);
        }


        std::vector<std::size_t> GenerateGhostedDofIndexList(const NodeBearer& processor_wise_node_bearer,
                                                             const NumberingSubset& numbering_subset)
        {
            assert(processor_wise_node_bearer.IsGhosted() && "If not function shouldn't have been called!");

            decltype(auto) node_list = processor_wise_node_bearer.GetNodeList();

            std::vector<std::size_t> ret;

            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;

                if (!node.IsInNumberingSubset(numbering_subset))
                    continue;

                decltype(auto) dof_list = node.GetDofList();

                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    ret.push_back(dof_ptr->GetProgramWiseIndex(numbering_subset));
                }
            }

            return ret;
        }


        std::vector<std::vector<std::size_t>>
        ComputeDofIndexListToSend(const Wrappers::Mpi& mpi,
                                  const NumberingSubset& numbering_subset,
                                  const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();
            std::vector<std::vector<std::size_t>> ret(Nprocessor);

            for (const auto& node_bearer_ptr : processor_wise_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                if (!node_bearer.IsGhosted())
                    continue;

                const auto dof_index_list = GenerateGhostedDofIndexList(node_bearer, numbering_subset);

                decltype(auto) ghost_processor_list = node_bearer.GetGhostProcessorList();

                const auto begin = dof_index_list.cbegin();
                const auto end = dof_index_list.cend();

                for (const auto processor : ghost_processor_list)
                {
                    assert(processor < ret.size());
                    auto& full_dof_index_list = ret[processor];
                    std::copy(begin, end, std::back_inserter(full_dof_index_list));
                }
            }
            return ret;
        }


        std::vector<Dof::vector_shared_ptr>
        ComputeRecipientDofList(const Wrappers::Mpi& mpi,
                                const NumberingSubset& numbering_subset,
                                const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();

            std::vector<Dof::vector_shared_ptr> ret(Nprocessor);

            for (const auto& node_bearer_ptr : ghost_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                assert(node_bearer.GetProcessor() != mpi.GetRank<std::size_t>());

                if (!node_bearer.IsInNumberingSubset(numbering_subset))
                    continue;

                decltype(auto) node_list = node_bearer.GetNodeList();

                Dof::vector_shared_ptr recipient_dof_list_for_node_bearer;

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    if (!node.IsInNumberingSubset(numbering_subset))
                        continue;

                    decltype(auto) dof_list = node.GetDofList();

                    std::copy(
                        dof_list.cbegin(), dof_list.cend(), std::back_inserter(recipient_dof_list_for_node_bearer));
                }

                assert(!recipient_dof_list_for_node_bearer.empty());

                const auto processor = node_bearer.GetProcessor();

                assert(processor < ret.size());
                auto& recipient_dof_list = ret[processor];

                std::copy(recipient_dof_list_for_node_bearer.cbegin(),
                          recipient_dof_list_for_node_bearer.cend(),
                          std::back_inserter(recipient_dof_list));
            }

            assert(mpi.GetRank<std::size_t>() < ret.size());
            assert(ret[mpi.GetRank<std::size_t>()].empty());

            return ret;
        }


        void FillGhostDofProgramWiseIndexes(const Wrappers::Mpi& mpi,
                                            const NumberingSubset& numbering_subset,
                                            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                            const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();
            const auto current_rank = mpi.GetRank<std::size_t>();

            std::vector<std::vector<std::size_t>> send_dof_index_per_processor =
                ComputeDofIndexListToSend(mpi, numbering_subset, processor_wise_node_bearer_list);

            std::vector<Dof::vector_shared_ptr> recipient_dof_list_per_processor =
                ComputeRecipientDofList(mpi, numbering_subset, ghost_node_bearer_list);

            assert(send_dof_index_per_processor.size() == Nprocessor);
            assert(recipient_dof_list_per_processor.size() == Nprocessor);
            assert(recipient_dof_list_per_processor[current_rank].empty());

            std::vector<std::size_t> Ndof_index_to_send_per_processor(send_dof_index_per_processor.size());

            std::transform(send_dof_index_per_processor.cbegin(),
                           send_dof_index_per_processor.cend(),
                           Ndof_index_to_send_per_processor.begin(),
                           [](const auto& vector)
                           {
                               return vector.size();
                           });

            mpi.Barrier();

            for (auto sender_rank = 0ul; sender_rank < Nprocessor; ++sender_rank)
            {
                std::vector<std::size_t> Ndof_per_processor_for_sender_rank;

                if (sender_rank == current_rank)
                    Ndof_per_processor_for_sender_rank = Ndof_index_to_send_per_processor;
                else
                    Ndof_per_processor_for_sender_rank.resize(Nprocessor);

                mpi.Broadcast(Ndof_per_processor_for_sender_rank, sender_rank);
                assert(current_rank < Ndof_per_processor_for_sender_rank.size());

                const auto& recipient_dof_list = recipient_dof_list_per_processor[sender_rank];

                const auto Nrecipient_dof_from_sender_proc = recipient_dof_list.size();

                if (Ndof_per_processor_for_sender_rank[current_rank] != Nrecipient_dof_from_sender_proc)
                {
                    std::ostringstream oconv;
                    oconv << mpi.GetRankPreffix()
                          << " Discrepancy between the number of ghost dofs that are "
                             "primarily handled by processor "
                          << sender_rank << " - " << recipient_dof_list_per_processor[sender_rank].size()
                          << " were expected but " << Ndof_per_processor_for_sender_rank[current_rank]
                          << " were "
                             "received."
                          << std::endl;

                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                assert(send_dof_index_per_processor.size() == Nprocessor);

                for (auto recipient_rank = 0ul; recipient_rank < Nprocessor; ++recipient_rank)
                {
                    if (recipient_rank == sender_rank)
                        continue;

                    if (current_rank == sender_rank)
                    {
                        if (!send_dof_index_per_processor[recipient_rank].empty())
                        {
                            mpi.SendContainer(recipient_rank, send_dof_index_per_processor[recipient_rank]);
                        }
                    } else if (current_rank == recipient_rank)
                    {
                        if (Nrecipient_dof_from_sender_proc > 0)
                        {
                            auto dof_index_list =
                                mpi.Receive<std::size_t>(sender_rank, Nrecipient_dof_from_sender_proc);

                            assert(recipient_dof_list.size() == Nrecipient_dof_from_sender_proc);
                            assert(recipient_dof_list.size() == dof_index_list.size());

                            for (auto i = 0ul; i < Nrecipient_dof_from_sender_proc; ++i)
                                recipient_dof_list[i]->SetProgramWiseIndex(numbering_subset, dof_index_list[i]);
                        }
                    }
                }
            }
        }


        void ProduceDofList(const NodeBearer::vector_shared_ptr& node_bearer_list, Dof::vector_shared_ptr& dof_list)
        {
            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                const auto& node_list = node_bearer.GetNodeList();

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    const auto& node_dof_list = node.GetDofList();

                    for (auto& dof_ptr : node_dof_list)
                    {
                        assert(!(!dof_ptr));
                        dof_list.push_back(dof_ptr);
                    }
                }
            }
        }


        void CreateNodeBearerListHelper(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                        NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                                        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
        {
            for (const auto& [ref_local_felt_space_ptr, local_felt_space_list] : felt_storage)
            {
                assert(!(!ref_local_felt_space_ptr));
                const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                for (const auto& local_felt_space_pair : local_felt_space_list)
                {
                    const auto& local_felt_space_ptr = local_felt_space_pair.second;
                    assert(!(!local_felt_space_ptr));
                    auto& local_felt_space = *local_felt_space_ptr;

                    match_interface_node_bearer.AddNodeBearerList(
                        ref_local_felt_space, node_bearer_for_current_god_of_dof, local_felt_space);
                }
            }
        }


        void AddNodeFromStorage(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
        {
            for (const auto& [ref_local_felt_space_ptr, local_felt_space_list] : felt_storage)
            {
                assert(!(!ref_local_felt_space_ptr));
                const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                for (const auto& local_felt_space_pair : local_felt_space_list)
                {
                    const auto& local_felt_space_ptr = local_felt_space_pair.second;
                    assert(!(!local_felt_space_ptr));
                    auto& local_felt_space = *local_felt_space_ptr;

                    match_interface_node_bearer.ComputeNodeList(ref_local_felt_space, local_felt_space);
                }
            }
        }


        template<ComputeDofIndexesHelper::in_numbering_subset IndexInNumberingSubsetT, RoleOnProcessor RoleOnProcessorT>
        void ComputeDofIndexesHelper ::Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                               DofNumberingScheme dof_numbering_scheme,
                                               std::size_t& current_dof_index,
                                               const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                                               std::optional<std::size_t> first_program_wise_index)
        {
            assert(dof_numbering_scheme == DofNumberingScheme::contiguous_per_node
                   && "Other schemes not yet implemented (need to be discussed in code meeting).");

#ifndef NDEBUG
            if constexpr (IndexInNumberingSubsetT == in_numbering_subset::yes)
                assert(!(!numbering_subset_ptr));
            else
                assert(numbering_subset_ptr == nullptr);
#endif // NDEBUG

            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                const auto& node_list = node_bearer.GetNodeList();

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    // Filter out nodes not encompassed by numbering subset if this quantity is
                    // relevant (otherwise it is nullptr...)
                    if constexpr (IndexInNumberingSubsetT == in_numbering_subset::yes)
                    {
                        assert(!(!numbering_subset_ptr));
                        if (!node.IsInNumberingSubset(*numbering_subset_ptr))
                            continue;
                    }

                    const auto& dof_list = node.GetDofList();

                    for (auto& dof_ptr : dof_list)
                    {
                        assert(!(!dof_ptr));

                        auto& dof = *dof_ptr;

                        switch (IndexInNumberingSubsetT)
                        {
                        case in_numbering_subset::yes:
                            dof.SetProcessorWiseOrGhostIndex(*numbering_subset_ptr, current_dof_index);

                            if (RoleOnProcessorT == RoleOnProcessor::processor_wise)
                                dof.SetProgramWiseIndex(*numbering_subset_ptr,
                                                        first_program_wise_index.value() + current_dof_index);

                            break;
                        case in_numbering_subset::no:
                            dof.SetInternalProcessorWiseOrGhostIndex(current_dof_index);
                            break;
                        }

                        switch (dof_numbering_scheme)
                        {
                        case DofNumberingScheme::contiguous_per_node:
                            current_dof_index++;
                            break;
                        }
                    }
                }
            }
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
