/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Geometry/Mesh/Mesh.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"


namespace MoReFEM
{


    GodOfDofManager::~GodOfDofManager() = default;


    void GodOfDofManager::Create(const Wrappers::Mpi& mpi, Mesh& mesh)
    {
        const auto unique_id = mesh.GetUniqueId();

        auto raw_ptr = new GodOfDof(mpi, mesh);

        auto ptr = GodOfDof::shared_ptr(raw_ptr);

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto insert_return_value = list_.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two god of dof objects can't share the same unique identifier! (namely "
                                + std::to_string(unique_id) + ").",
                            __FILE__,
                            __LINE__);
    }


    const std::string& GodOfDofManager::ClassName()
    {
        static std::string ret("GodOfDofManager");
        return ret;
    }


    GodOfDofManager::GodOfDofManager()
    {
        list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    GodOfDof::shared_ptr GodOfDofManager::GetGodOfDofPtr(std::size_t unique_id) const
    {
        auto it = list_.find(unique_id);

        assert(it != list_.cend());
        assert(!(!(it->second)));

        return it->second;
    }


    void ClearGodOfDofTemporaryData()
    {
        const auto& storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        for (const auto& pair : storage)
        {
            auto& god_of_dof_ptr = pair.second;
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;
            god_of_dof.ClearTemporaryData();
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
