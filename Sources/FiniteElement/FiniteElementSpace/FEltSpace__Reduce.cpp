//! \file
//
//
//  FEltSpace__Reduce.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 21/10/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <set>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp" // IWYU pragma: associated
#include "FiniteElement/FiniteElementSpace/Internal/Partition/AssignGeomEltToProcessor.hpp"
#include "FiniteElement/Nodes_and_dofs/Dof.hpp"


namespace MoReFEM
{


    void FEltSpace::Reduce(const Internal::FEltSpaceNS::AssignGeomEltToProcessor& geom_elt_repartition)
    {
        assert(ghost_dof_list_.empty() && "Should be empty before this call!"); // due to the debug variable to check
                                                                                // dofs are cleared the accessor can't
                                                                                // be used here.

        decltype(auto) felt_storage = GetNonCstFEltStorage();
        assert(felt_storage.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>().empty());

        decltype(auto) mpi = GetMpi();
        const auto rank = mpi.GetRank<std::size_t>();

        LocalFEltSpacePerRefLocalFEltSpace processor_wise_list;
        LocalFEltSpacePerRefLocalFEltSpace ghost_list;

        decltype(auto) program_wise_list =
            felt_storage.GetNonCstLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();
        // < not a typo: in sequential construction - the only one during which current method should be called! -
        // < everything is built completely on each processor.
        assert(felt_storage.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>().empty());

        for (const auto& [ref_local_felt_space_ptr, local_felt_space_per_geom_elt_index] : program_wise_list)
        {
            LocalFEltSpace::per_geom_elt_index processor_wise_local_felt_space_list;
            processor_wise_local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

            auto ghost_wise_local_felt_space_list = processor_wise_local_felt_space_list;

            for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_per_geom_elt_index)
            {
                assert(!(!local_felt_space_ptr));
                const auto& local_felt_space = *local_felt_space_ptr;
                assert(geom_elt_index == local_felt_space.GetGeometricElt().GetIndex());

                // Determine to which processor the local finite element space should be attributed.
                const auto processor_data_for_local_felt_space =
                    geom_elt_repartition.GetProcessorData(local_felt_space);

                if (processor_data_for_local_felt_space.rank == rank)
                    processor_wise_local_felt_space_list.insert({ geom_elt_index, local_felt_space_ptr });
                else
                {
                    auto it = processor_data_for_local_felt_space.ghost_rank_list.find(rank);
                    if (it != processor_data_for_local_felt_space.ghost_rank_list.cend())
                        ghost_wise_local_felt_space_list.insert({ geom_elt_index, local_felt_space_ptr });
                }
            }

            processor_wise_list.push_back(
                std::make_pair(ref_local_felt_space_ptr, std::move(processor_wise_local_felt_space_list)));

            ghost_list.push_back(std::make_pair(ref_local_felt_space_ptr, std::move(ghost_wise_local_felt_space_list)));
        }

        felt_storage.SetReducedData(std::move(processor_wise_list), std::move(ghost_list));


        ClearDofLists();
    }


} // namespace MoReFEM
