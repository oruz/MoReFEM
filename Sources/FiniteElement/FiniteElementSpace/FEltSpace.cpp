/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:45:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp" // IWYU pragma: associated
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"  // IWYU pragma: keep
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        std::size_t ExtractDimensionFromDomain(const Domain& domain)
        {
            if (!domain.IsConstraintOn<DomainNS::Criterion::dimension>())
            {
                std::ostringstream oconv;
                oconv << "By design, a finite element space covers one and only one dimension; current underlying "
                         "domain cover doesn't get any dimension restriction.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }


            if (domain.GetDimensionList().size() != 1u)
            {
                std::ostringstream oconv;
                oconv << "By design, a finite element space covers one and only one dimension; current underlying "
                         "domain cover "
                      << domain.GetDimensionList().size() << " different dimensions.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            return domain.GetDimensionList().back();
        }


        NumberingSubset::vector_const_shared_ptr
        ComputeNumberingSubsetList(const ExtendedUnknown::vector_const_shared_ptr& unknown_list)
        {
            NumberingSubset::vector_const_shared_ptr ret;

            for (const auto& extended_unknown_ptr : unknown_list)
            {
                assert(!(!extended_unknown_ptr));
                ret.push_back(extended_unknown_ptr->GetNumberingSubsetPtr());
            }

            Utilities::EliminateDuplicate(ret);

            return ret;
        }


        void IterateThroughLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& local_felt_space_storage,
                                          const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                          Dof::vector_shared_ptr& dof_list)
        {
            for (const auto& [ref_local_felt_space_ptr, local_felt_space_list_per_geom_elt_index] :
                 local_felt_space_storage)
            {
                for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_list_per_geom_elt_index)
                {
                    assert((!(!local_felt_space_ptr)));
                    decltype(auto) local_felt_space = *local_felt_space_ptr;

                    for (const auto& extended_unknown_ptr : extended_unknown_list)
                    {
                        assert(!(!extended_unknown_ptr));
                        decltype(auto) extended_unknown = *extended_unknown_ptr;
                        decltype(auto) unknown = extended_unknown.GetUnknown();
                        decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();

                        const auto& node_bearer_list = local_felt_space.GetNodeBearerList();

                        for (const auto& node_bearer_ptr : node_bearer_list)
                        {
                            assert(!(!node_bearer_ptr));
                            decltype(auto) node_bearer = *node_bearer_ptr;
                            const auto& node_list = node_bearer.GetNodeList(unknown, shape_function_label);

                            for (const auto& node_ptr : node_list)
                            {
                                assert(!(!node_ptr));
                                const auto& node_dof_list = node_ptr->GetDofList();

                                for (const auto& dof_ptr : node_dof_list)
                                {
                                    assert(!(!dof_ptr));
                                    dof_list.push_back(dof_ptr);
                                }
                            }
                        }
                    }
                }
            }
        }


    } // namespace


    const std::string& FEltSpace::ClassName()
    {
        static std::string ret("FEltSpace");
        return ret;
    }


    FEltSpace::FEltSpace(const std::shared_ptr<const GodOfDof>& god_of_dof_ptr,
                         const Domain& domain,
                         const std::size_t unique_id,
                         ExtendedUnknown::vector_const_shared_ptr&& extended_unknown_list)
    : Crtp::CrtpMpi<FEltSpace>(god_of_dof_ptr->GetMpi()), unique_id_parent(unique_id), felt_storage_(nullptr),
      dimension_(ExtractDimensionFromDomain(domain)), god_of_dof_(god_of_dof_ptr), domain_(domain),
      extended_unknown_list_(std::move(extended_unknown_list)),
      numbering_subset_list_(ComputeNumberingSubsetList(GetExtendedUnknownList()))
    {
        assert(!(!god_of_dof_ptr));

        SetFEltList(god_of_dof_ptr->GetMesh(), domain);

        if (IsEmpty() && god_of_dof_ptr->GetMpi().IsRootProcessor())
        {
            std::ostringstream oconv;
            oconv << "[WARNING] Finite element space " << GetUniqueId()
                  << " does not seem to store any finite element; "
                     "the most likely explanation is that the domain upon which it is built is ill-formed.";
            std::cout << oconv.str();
        }

        // Beware: some operations are not possible at this stage as the FEltSpace is actually initialized completely
        // along with full initialization of its GodOfDof, which is not complete when current constructor is called.
        // That's the reason for instance default_quadrature_rule_per_topology_ is defined in subsequent
        // InitLocal2Global() rather than here.
    }


    void FEltSpace::InitLocal2Global(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global)
    {
        const auto& felt_storage = GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        const auto& numbering_subset_list = GetNumberingSubsetList();

        for (const auto& pair : felt_storage)
        {
            const auto& local_felt_space_list = pair.second;

            for (auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                local_felt_space_ptr->InitLocal2Global(numbering_subset_list,
                                                       do_consider_processor_wise_local_2_global);
            }
        }

        const auto max_order = ComputeMaxOrderFElt();

        default_quadrature_rule_per_topology_ =
            std::make_unique<QuadratureRulePerTopology>(DEFAULT_DEGREE_OF_EXACTNESS, max_order);
    }


    void
    FEltSpace::ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                   DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const
    {
        assert(!extended_unknown_list.empty());

#ifndef NDEBUG
        if (do_compute_processor_wise_local_2_global == DoComputeProcessorWiseLocal2Global::yes)
        {
            assert(GetGodOfDofFromWeakPtr()->GetDoConsiderProcessorWiseLocal2Global()
                       == DoConsiderProcessorWiseLocal2Global::yes
                   && "Unable to compute local2global if pre-initialization has not been performed.");
        }

        assert(std::none_of(extended_unknown_list.cbegin(),
                            extended_unknown_list.cend(),
                            Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

#endif // NDEBUG

        const auto& felt_storage = GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        for (const auto& pair : felt_storage)
        {
            const auto& local_felt_space_list = pair.second;

            for (auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                local_felt_space_ptr->ComputeLocal2Global(extended_unknown_list,
                                                          do_compute_processor_wise_local_2_global);
            }
        }
    }


    void FEltSpace::SetFEltList(const Mesh& mesh, const Domain& domain)
    {
        const auto felt_space_dimension = GetDimension();
        const auto bag = mesh.BagOfEltType<RoleOnProcessorPlusBoth::both>(felt_space_dimension);

        const auto& unknown_storage = GetExtendedUnknownList();

        LocalFEltSpacePerRefLocalFEltSpace felt_list_per_type;
        LocalFEltSpacePerRefLocalFEltSpace ghost_felt_list_per_type;

        for (const auto& ref_geom_elt_ptr : bag)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;

            if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                continue;

            auto&& ref_local_felt_space_ptr = std::make_unique<const Internal::RefFEltNS::RefLocalFEltSpace>(
                ref_geom_elt_ptr, unknown_storage, mesh.GetDimension(), felt_space_dimension);

            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            LocalFEltSpace::per_geom_elt_index local_felt_space_list;
            local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

            LocalFEltSpace::per_geom_elt_index ghost_local_felt_space_list;
            ghost_local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

            auto Ngeometric_elt_in_range = FillLocalFEltSpaceList<RoleOnProcessor::processor_wise>(
                mesh, domain, ref_local_felt_space, local_felt_space_list);

            Ngeometric_elt_in_range += FillLocalFEltSpaceList<RoleOnProcessor::ghost>(
                mesh, domain, ref_local_felt_space, ghost_local_felt_space_list);

            static_cast<void>(Ngeometric_elt_in_range);
            assert(Ngeometric_elt_in_range >= local_felt_space_list.size()); // equal if no domain restriction applied.

            if (local_felt_space_list.empty()) // might happen if for instance RefGeomElt not considered in the domain.
                continue;

            if (!ghost_local_felt_space_list.empty())
            {
                // \todo REMAINING Awkward; use a shared_ptr instead!
                auto copy_ref_local_felt_space_ptr =
                    std::make_unique<const Internal::RefFEltNS::RefLocalFEltSpace>(*ref_local_felt_space_ptr);

                ghost_felt_list_per_type.push_back(
                    std::make_pair(std::move(copy_ref_local_felt_space_ptr), ghost_local_felt_space_list));
            }


            felt_list_per_type.push_back(std::make_pair(std::move(ref_local_felt_space_ptr), local_felt_space_list));
        }

        felt_storage_ = std::make_unique<Internal::FEltSpaceNS::Storage>(
            GetMpi(), std::move(felt_list_per_type), std::move(ghost_felt_list_per_type));
    }


#ifndef NDEBUG
    void FEltSpace::AssertGodOfDofInitialized() const
    {
        assert(GetGodOfDofFromWeakPtr()->HasInitBeenCalled()
               && "Finite element and node list not yet existing; GodOfDof::Init() must be called first!");
    }

#endif // NDEBUG


    ExtendedUnknown::const_shared_ptr FEltSpace::GetExtendedUnknownPtr(const Unknown& unknown) const
    {
        const auto& unknown_storage = GetExtendedUnknownList();
        auto it = std::find_if(unknown_storage.cbegin(),
                               unknown_storage.cend(),
                               [&unknown](const auto& extended_unknown_ptr)
                               {
                                   assert(!(!extended_unknown_ptr));
                                   return extended_unknown_ptr->GetUnknown() == unknown;
                               });

        assert(it != unknown_storage.cend()
               && "Your unknown is probably not present in the \a FEltSpace upon which "
                  "the operator is defined.");

        return *it;
    }


    void FEltSpace::ClearTemporaryData() const noexcept
    {
        const auto& storage = GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        for (const auto& pair : storage)
        {
            const auto& local_felt_space_list = pair.second;

            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));
                local_felt_space_ptr->ClearTemporaryData();
            }
        }
    }


    namespace // anonymous
    {


        auto IteratorLocalFEltSpacePair(const FEltSpace& felt_space, const RefGeomElt& ref_geom_elt)
        {
            const auto& full_storage =
                felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();

            auto it =
                std::find_if(full_storage.cbegin(),
                             full_storage.cend(),
                             [ref_geom_elt_id](const auto& pair)
                             {
                                 const auto& current_ref_felt_space = pair.first;
                                 assert(!(!current_ref_felt_space));
                                 // Two ref felt spaces are equal if they are related to the same ref geometric element.
                                 return current_ref_felt_space->GetRefGeomElt().GetIdentifier() == ref_geom_elt_id;
                             });

            assert(it != full_storage.cend());
            return it;
        }


    } // namespace


    const LocalFEltSpace::per_geom_elt_index&
    FEltSpace ::GetLocalFEltSpaceList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space) const
    {
        auto it = IteratorLocalFEltSpacePair(*this, ref_felt_space.GetRefGeomElt());
        return it->second;
    }


    const Internal::RefFEltNS::RefLocalFEltSpace& FEltSpace::GetRefLocalFEltSpace(const RefGeomElt& ref_geom_elt) const
    {
        auto it = IteratorLocalFEltSpacePair(*this, ref_geom_elt);
        assert(!(!it->first));
        return *(it->first);
    }


    std::size_t FEltSpace::GetMeshDimension() const
    {
        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();
        return god_of_dof_ptr->GetMesh().GetDimension();
    }


    namespace // anonymous
    {


        /*!
         * \brief Helper to ComputeDofList.
         *
         * \param[in] felt_space_dof_unique_id_list Unique ids of all the dofs involved in the finite element space,
         * regardless of partitioning.
         * \param[in] processor_or_ghost_wise_dof_list Processor- OR ghost- wise dof list in the whole GodOfDof.
         * \return Processor- OR ghost- dofs that belongs to the finite element space.
         */
        Dof::vector_shared_ptr ComputeDofListHelper(const std::vector<std::size_t>& felt_space_dof_unique_id_list,
                                                    const Dof::vector_shared_ptr& processor_or_ghost_wise_dof_list)
        {
            Dof::vector_shared_ptr ret;
            assert(std::is_sorted(processor_or_ghost_wise_dof_list.cbegin(),
                                  processor_or_ghost_wise_dof_list.cend(),
                                  Utilities::PointerComparison::Less<Dof::shared_ptr>()));

            const auto begin = felt_space_dof_unique_id_list.cbegin();
            const auto end = felt_space_dof_unique_id_list.cend();

            for (const auto& dof_ptr : processor_or_ghost_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto unique_id = dof_ptr->GetUniqueId();

                if (std::binary_search(begin, end, unique_id))
                    ret.push_back(dof_ptr);
            }

            std::sort(ret.begin(), ret.end(), Utilities::PointerComparison::Less<Dof::shared_ptr>());

            return ret;
        }


    } // namespace


    void FEltSpace::ComputeDofList(const std::vector<std::size_t>& dof_unique_id_list)
    {
        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();

        assert(!are_dof_list_computed_);

        dof_list_ = ComputeDofListHelper(dof_unique_id_list, god_of_dof_ptr->GetProcessorWiseDofList());

        ghost_dof_list_ = ComputeDofListHelper(dof_unique_id_list, god_of_dof_ptr->GetGhostDofList());

#ifndef NDEBUG
        are_dof_list_computed_ = true;
#endif // NDEBUG

        Ndof_holder_ = std::make_unique<Internal::FEltSpaceNS::NdofHolder>(
            GetMpi(), dof_list_, god_of_dof_ptr->GetNumberingSubsetList());
    }


    const LocalFEltSpace& FEltSpace::GetLocalFEltSpace(const GeometricElt& geometric_elt) const
    {
        const auto& ref_geom_elt = geometric_elt.GetRefGeomElt();

        const auto& local_felt_list = GetLocalFEltSpaceList(ref_geom_elt);

        const auto it = local_felt_list.find(geometric_elt.GetIndex());
        assert(it != local_felt_list.cend()
               && "If this happens, there's likely a discrepancy somewhere in an operator "
                  "between \a FEltSpace and for instance a ParameterAtDof defined on a wholly different mesh.");

        const auto& local_felt_space_ptr = it->second;

        assert(!(!local_felt_space_ptr));

        return *local_felt_space_ptr;
    }


    Dof::vector_shared_ptr FEltSpace::GetProcessorWiseDofList(const NumberingSubset& numbering_subset) const
    {
        assert(are_dof_list_computed_);

        decltype(auto) complete_list = GetProcessorWiseDofList();

        Dof::vector_shared_ptr ret;
        ret.reserve(complete_list.size());
        const auto end = complete_list.cend();

        std::copy_if(complete_list.cbegin(),
                     end,
                     std::back_inserter(ret),
                     [&numbering_subset](const auto& dof_ptr)
                     {
                         assert(!(!dof_ptr));
                         return dof_ptr->IsInNumberingSubset(numbering_subset);
                     });

        ret.shrink_to_fit();

        return ret;
    }


    void FEltSpace::SetMovemeshData()
    {
        assert(GetDimension() == GetMeshDimension() && "It makes little sense to move only contours of the mesh!");

        const auto& numbering_subset_list = GetNumberingSubsetList();

        decltype(auto) extended_unknown_list = GetExtendedUnknownList();
        const auto begin_extended_unknown_list = extended_unknown_list.cbegin();
        const auto end_extended_unknown_list = extended_unknown_list.cend();

        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

#ifndef NDEBUG
        decltype(auto) felt_space_processor_wise_dof_list = GetProcessorWiseDofList();
        decltype(auto) felt_space_ghost_dof_list = GetGhostDofList();
#endif // NDEBUG

        auto& movemesh_helper_data = GetNonCstMovemeshHelperStorage();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            if (!numbering_subset.DoMoveMesh())
                continue;

            // Determine which unknown provides the actual displacement to move the mesh.
            auto find_unknown = [&numbering_subset](const auto& ptr)
            {
                assert(!(!ptr));
                return ptr->GetNumberingSubset() == numbering_subset;
            };

            auto it = std::find_if(begin_extended_unknown_list, end_extended_unknown_list, find_unknown);

            assert(it != end_extended_unknown_list);
            assert(std::count_if(begin_extended_unknown_list, end_extended_unknown_list, find_unknown) == 1);
            const auto& extended_unknown = (*(*it));
            assert(extended_unknown.GetNature() == UnknownNS::Nature::vectorial && "Unknown should be a displacement!");

            // Create an helper object which retains all relevant informations to perform the move during the course
            // of the time iterations.
            auto&& helper_per_numbering_subset =
                std::make_pair(numbering_subset.GetUniqueId(),
                               Internal::FEltSpaceNS::MovemeshHelper(god_of_dof,
                                                                     extended_unknown
#ifndef NDEBUG
                                                                     ,
                                                                     felt_space_processor_wise_dof_list,
                                                                     felt_space_ghost_dof_list
#endif // NDEBUG
                                                                     ));

            auto check = movemesh_helper_data.emplace(std::move(helper_per_numbering_subset));
            static_cast<void>(check);
            assert(check.second && "There should be only one entry per numbering subset.");

        } // for (const auto& numbering_subset_ptr : numbering_subset_list)
    }


    const Internal::FEltSpaceNS::MovemeshHelper& FEltSpace::GetMovemeshHelper(const GlobalVector& vector) const noexcept
    {
        const auto& movemesh_helper_data = GetMovemeshHelperStorage();

        const auto& numbering_subset = vector.GetNumberingSubset();
        const auto it = movemesh_helper_data.find(numbering_subset.GetUniqueId());
        assert(it != movemesh_helper_data.cend());

        return it->second;
    }


    bool FEltSpace::DoCoverNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) list = GetNumberingSubsetList();

        for (const auto& item : list)
        {
            assert(!(!item));
            if (*item == numbering_subset)
                return true;
        }

        return false;
    }


    std::size_t FEltSpace::ComputeMaxOrderFElt() const
    {
        decltype(auto) storage = GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        std::size_t ret = 0ul;

        for (const auto& item : storage)
        {
            const auto& ref_local_felt_space_ptr = item.first;
            assert(!(!ref_local_felt_space_ptr));
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            decltype(auto) ref_felt_list = ref_local_felt_space.GetRefFEltList();

            for (const auto& ref_felt : ref_felt_list)
            {
                assert(!(!ref_felt));

                decltype(auto) basic_ref_felt = ref_felt->GetBasicRefFElt();

                const auto order = basic_ref_felt.GetOrder();

                if (order > ret)
                    ret = order;
            }
        }

        return ret;
    }


    bool FEltSpace::IsEmpty() const noexcept
    {
        // I do not use the accessor here as it also checkts the GodOfDof is fully built... and I want to use this
        // method during GodOfDof initialization.
        assert(!(!felt_storage_));
        return felt_storage_->IsEmpty();
    }


    void FEltSpace::ClearDofLists()
    {
        dof_list_.clear();
        ghost_dof_list_.clear();
#ifndef NDEBUG
        are_dof_list_computed_ = false;
#endif // NDEBUG
    }


    void FEltSpace::ComputeDofList()
    {
        auto& processor_wise_dof_list = GetNonCstProcessorWiseDofList();
        auto& ghost_dof_list = GetNonCstGhostDofList();

        decltype(auto) extended_unknown_list = GetExtendedUnknownList();

        Dof::vector_shared_ptr non_sorted_dof_list;

        {
            // First iterate through all \a LocalFEltSpace and catch all \a Dofs.
            // We don't want to separate processor-wise and ghost at this stage - this work has already been done
            // in \a GodOfDof, and we don't want to duplicate it here!
            IterateThroughLocalFEltSpace(GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>(),
                                         extended_unknown_list,
                                         non_sorted_dof_list);

            IterateThroughLocalFEltSpace(GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>(),
                                         extended_unknown_list,
                                         non_sorted_dof_list);

            Utilities::EliminateDuplicate(non_sorted_dof_list,
                                          Utilities::PointerComparison::Less<Dof::shared_ptr>(),
                                          Utilities::PointerComparison::Equal<Dof::shared_ptr>());
        }

        {
            // Now we will sort it, using the results from \a GodOfDof. We will use here the list of ghosts rather
            // than the list of processor-wise - both would work but as the ghost is expected to be much smaller
            // the look-up will be much faster.
            const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();

            decltype(auto) ghost_god_of_dof_list = god_of_dof_ptr->GetGhostDofList();

            const auto begin = ghost_god_of_dof_list.cbegin();
            const auto end = ghost_god_of_dof_list.cend();

            assert(std::is_sorted(begin, end, Utilities::PointerComparison::Less<Dof::shared_ptr>()));

            for (const auto& dof_ptr : non_sorted_dof_list)
            {
                assert(!(!dof_ptr));

                if (std::binary_search(begin, end, dof_ptr, Utilities::PointerComparison::Less<Dof::shared_ptr>()))
                    ghost_dof_list.push_back(dof_ptr);
                else
                    processor_wise_dof_list.push_back(dof_ptr);
            }
        }

        Utilities::EliminateDuplicate(processor_wise_dof_list,
                                      Utilities::PointerComparison::Less<Dof::shared_ptr>(),
                                      Utilities::PointerComparison::Equal<Dof::shared_ptr>());

        Utilities::EliminateDuplicate(ghost_dof_list,
                                      Utilities::PointerComparison::Less<Dof::shared_ptr>(),
                                      Utilities::PointerComparison::Equal<Dof::shared_ptr>());

#ifndef NDEBUG
        {
            const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();

            decltype(auto) processor_wise_god_of_dof_list = god_of_dof_ptr->GetProcessorWiseDofList();

            assert(std::is_sorted(processor_wise_god_of_dof_list.cbegin(),
                                  processor_wise_god_of_dof_list.cend(),
                                  Utilities::PointerComparison::Less<Dof::shared_ptr>()));

            const auto begin = processor_wise_dof_list.cbegin();
            const auto end = processor_wise_dof_list.cend();

            assert(std::all_of(begin,
                               end,
                               [&processor_wise_god_of_dof_list](const auto& dof_ptr)
                               {
                                   return std::binary_search(processor_wise_god_of_dof_list.cbegin(),
                                                             processor_wise_god_of_dof_list.cend(),
                                                             dof_ptr,
                                                             Utilities::PointerComparison::Less<Dof::shared_ptr>());
                               }));
        }
#endif // NDEBUG


#ifndef NDEBUG
        are_dof_list_computed_ = true;
#endif // NDEBUG

        {
            const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();

            Ndof_holder_ = std::make_unique<Internal::FEltSpaceNS::NdofHolder>(
                GetMpi(), GetProcessorWiseDofList(), god_of_dof_ptr->GetNumberingSubsetList());
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
