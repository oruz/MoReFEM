//
//  used-but-marked-unused.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if !defined(__apple_build_version__) && __clang_major__ >= 9
PRAGMA_DIAGNOSTIC(ignored "-Wused-but-marked-unused")
#endif
#endif // __clang__
