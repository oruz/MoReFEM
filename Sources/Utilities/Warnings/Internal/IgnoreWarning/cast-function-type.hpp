//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef MOREFEM_GCC
#if __GNUC__ >= 8
PRAGMA_DIAGNOSTIC(ignored "-Wcast-function-type")
#endif
#endif // MOREFEM_GCC
