//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wredundant-parens")
#endif // __clang__
