### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/cast-function-type.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/comma.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/covered-switch-default.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/deprecated-dynamic-exception-spec.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/deprecated.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/disabled-macro-expansion.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/extra-semi-stmt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/missing-prototypes.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/newline-eof.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/non-virtual-dtor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/redundant-parens.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/reorder.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/reserved-id-macro.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/shadow-field-in-constructor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/shadow-field.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/shorten-64-to-32.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/sometimes-uninitialized.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/suggest-destructor-override.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/suggest-override.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/undef.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/unused-local-typedef.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/unused-template.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/used-but-marked-unused.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/weak-vtables.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/zero-as-null-pointer-constant.hpp"
)

