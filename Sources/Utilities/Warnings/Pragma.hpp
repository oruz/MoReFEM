/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 27 Feb 2014 14:09:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_WARNINGS_x_PRAGMA_HPP_
#define MOREFEM_x_UTILITIES_x_WARNINGS_x_PRAGMA_HPP_


#ifdef SONARQUBE_4_MOREFEM
#define PRAGMA_DIAGNOSTIC(tok)
#else // SONARQUBE_4_MOREFEM

// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Helper macros.
// ============================


//! Helper of PRAGMA_DIAGNOSTIC.
#define STR_EXPAND(tok) #tok

/*!
 * \brief STR transform into a string litteral the tok argument.
 *
 * Why we need STR_EXPAND is honestly a bit blurry for me, but we really need it.
 */
#define STR(tok) STR_EXPAND(tok)


/*!
 * \brief Helper to define PRAGMA_DIAGNOSTIC.
 *
 * It will choose the syntax to employ depending on the compiler used (clang and gcc supported at the moment).
 *
 * See
 * http://nadeausoftware.com/articles/2012/10/c_c_tip_how_detect_compiler_name_and_version_using_compiler_predefined_macros
 * for the macro used to detect each compiler.
 */
#ifdef __clang__
#define PREPARE_PRAGMA_STRING(tok) clang diagnostic tok
#else
#define MOREFEM_GCC // there is no macro to identify gcc, so I introduce one...
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#if GCC_VERSION >= 40600
#define PREPARE_PRAGMA_STRING(tok) GCC diagnostic tok
#else
#define PREPARE_PRAGMA_STRING(tok) message("Diagnostic pragmas not activated; requires gcc 4.6 or clang.")
#endif // __GNUC__
#endif // __clang__


// ============================
// Helper macros.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


/*!
 * \brief Apply a pragma diagnostic directive.
 *
 * \param[in] tok Token given to the macro. It is not a string litteral yet; do no put double quotes around this
 argument.
 *
 * \code
 * PRAGMA_DIAGNOSTIC(push)
 * PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
 * PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")
 * #include "parmetis.h"
 * PRAGMA_DIAGNOSTIC(pop)
 *
 * \endcode
 *
 * The advantage over a direct call to _Pragma is that the compiler is automatically handled.
 *
 * Without that the line would be
 *
 \verbatim
 #pragma clang diagnostic ignored "-Wconversion"
 \endverbatim
 *
 * for clang and
 *
 \verbatim
 #pragma GCC diagnostic ignored "-Wconversion"
 \endverbatim
 *
 * for GCC, with a directive condition to rule out between both.
 */
#define PRAGMA_DIAGNOSTIC(tok) _Pragma(STR(PREPARE_PRAGMA_STRING(tok)))

#endif // SONARQUBE_4_MOREFEM

/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_WARNINGS_x_PRAGMA_HPP_
