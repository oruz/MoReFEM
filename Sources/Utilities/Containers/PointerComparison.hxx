/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 1 Jul 2013 12:44:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HXX_

// IWYU pragma: private, include "Utilities/Containers/PointerComparison.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace PointerComparison
        {


            template<class T>
            bool Less<T>::operator()(comparison_type lhs, comparison_type rhs) const
            {
                static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(),
                              "T must behaves like a pointer!");


                // First handle cases with nullptr involved.
                // Rule is nullptr is the lowest value possible.
                if (!lhs || !rhs)
                {
                    if (!lhs && !rhs)
                        return false;

                    if (!lhs)
                        return true;

                    if (!rhs)
                        return false;
                }

                return *lhs < *rhs;
            }


            template<class T>
            bool Greater<T>::operator()(comparison_type lhs, comparison_type rhs) const
            {
                static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(),
                              "T must behaves like a pointer!");

                // Return false if both are equal.
                if (Equal<T>()(lhs, rhs))
                    return false;

                // If not equal the result is the opposite of the one of Less.
                return !Less<T>()(lhs, rhs);
            }


            template<class T>
            bool Equal<T>::operator()(comparison_type lhs, comparison_type rhs) const
            {
                static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(),
                              "T must behaves like a pointer!");

                // Handle nullptr cases
                if (!lhs || !rhs)
                {
                    // If both are nullptr consider them equal.
                    if (!lhs && !rhs)
                        return true;

                    return false;
                }

                return *lhs == *rhs;
            }


        } // namespace PointerComparison


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HXX_
