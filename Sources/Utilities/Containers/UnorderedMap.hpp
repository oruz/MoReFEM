/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Apr 2014 14:47:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HPP_

#include <cstddef> // IWYU pragma: keep
#include <functional>


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Value of std::unordered_map::max_load_factor() that is recommended in "The C++ Standard Library"
         * by N. Josuttis.
         *
         * \return 0.7 (either that or .8 was suggested).
         */
        constexpr float DefaultMaxLoadFactor() noexcept
        {
            return 0.7f;
        }


        /*!
         * \brief Function lifted from Boost (http://www.boost.org) to combine several hash keys together.
         *
         * Apparently this formula stems from Knuth's work.
         *
         * For instance to yield a hash key for a std::pair<std::size_t, std::size_t>:
         *
         * \code
         * std::pair<std::size_t, std::size_t> pair;
         * ...
         * std::size_t key = std::hash<std::size_t>()(pair.first);
         * Utilities::HashCombine(key, pair.second);
         * \endcode
         *
         * \tparam T Type for which a std::hash specialization already exists.
         *
         * \param[in,out] seed In input current value of the hash; in output modified value.
         * \param[in] value New element which hash key is combined to the \a seed to provide a new has key.
         *
         * \return \a seed after the modification.
         */
        template<class T>
        std::size_t HashCombine(std::size_t& seed, const T& value);


        /*!
         * \brief Struct used to generate a hash key for a container with a direct accessor.
         *
         * \internal It is defined as a struct rather than a free function to make it easily usable as
         * std::unordered_map third argument. \endinternal
         */
        struct ContainerHash
        {


            /*!
             * \brief Combine all values of a container into a single hash key.
             *
             * \tparam ContainerT A container type. It must define a 'value_type' alias, a direct iterator and a size()
             * method.
             *
             * \param[in] container Container for which a hash key is sought.
             *
             * \return Hash key for the container.
             */
            template<class ContainerT>
            std::size_t operator()(const ContainerT& container) const;
        };


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Containers/UnorderedMap.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HPP_
