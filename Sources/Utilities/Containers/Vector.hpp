/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Apr 2014 14:47:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_VECTOR_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_VECTOR_HPP_


#include <algorithm>
#include <vector>


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \class doxygen_hide_utilities_eliminate_duplicate
         *
         * \brief Sort a std::vector and delete the possible duplicates.
         *
         * \tparam T Type of the data stored within the std::vector.
         * \param[in,out] vector Vector which should be sort and from which duplicates should be removed.
         */

        /*!
         * \copydoc doxygen_hide_utilities_eliminate_duplicate
         *
         * This version uses default comparison (less) and equality (==) predicates.
         */
        template<class T>
        void EliminateDuplicate(std::vector<T>& vector)
        {
            std::sort(vector.begin(), vector.end());
            vector.erase(std::unique(vector.begin(), vector.end()), vector.end());
        }


        /*!
         * \copydoc doxygen_hide_utilities_eliminate_duplicate
         *
         * \tparam ComparePredicateT Functor used to compare two values of the vector.
         * \tparam EqualPredicateT Predicate used to determine two quantities are equal.
         *
         * \param[in] comp Binary functor used to sort two values.
         * \param[in] equal Predicate to use to determine whether two values are equal.
         */
        template<class T, class ComparePredicateT, class EqualPredicateT>
        void EliminateDuplicate(std::vector<T>& vector, ComparePredicateT comp, EqualPredicateT equal)
        {
            std::sort(vector.begin(), vector.end(), comp);
            vector.erase(std::unique(vector.begin(), vector.end(), equal), vector.end());
        }


        /*!
         * \brief A slightly extended static_cast.
         *
         * \tparam SourceT Original type.
         * \tparam TargetT Type to which we want to cast.
         *
         * This function also handles std::vector: you can cast a std::vector<int> into a std::vector<std::size_t>
         * for instance. No other extensions are provided for other containers as there are currently no need for it.
         *
         */
        template<class SourceT, class TargetT>
        struct StaticCast
        {


            //! Function that does the actual work.
            //! \param[in] source Value which is to be cast into a \a TargetT.
            static TargetT Perform(SourceT source)
            {
                return static_cast<TargetT>(source);
            }
        };


        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Case when types is to be cast into itself.
        template<class T>
        struct StaticCast<T, T>
        {

            static T Perform(T source)
            {
                return source;
            }
        };


        //! Specialization for std::vector.
        template<class T, class U>
        struct StaticCast<std::vector<T>, std::vector<U>>
        {

            static std::vector<U> Perform(const std::vector<T>& source)
            {
                std::vector<U> ret(source.size());

                std::transform(source.cbegin(),
                               source.cend(),
                               ret.begin(),
                               [](T value)
                               {
                                   return static_cast<U>(value);
                               });

                return ret;
            }
        };

        //! Case in which the vector is to be cast into its original type, in which case nothing is done.
        template<class T>
        struct StaticCast<std::vector<T>, std::vector<T>>
        {
          private:
            using type = std::vector<T>;

          public:
            static type Perform(type source)
            {
                return source;
            }
        };
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_VECTOR_HPP_
