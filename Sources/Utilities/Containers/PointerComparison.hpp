/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 23 Jun 2013 22:20:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HPP_


#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Utilities
    {


        namespace PointerComparison
        {


            /*!
             * \class doxygen_hide_pointer_comp_type
             *
             * \brief Use reference if the type being compared is a shared or unique pointer.
             *
             * For the shared pointer it's only a matter of efficiency, for the unique one comparison couldn't occur
             * otherwise.
             */


            /*!
             * \brief The equivalent of std::less<> for raw or shared pointers: except that what is compared
             * is the underlying object rather than just the address of the pointer.
             *
             * For instance:
             * \code
             * std::vector<std::shared_ptr<Foo>> my_list;
             * ... fill the list ...
             * std::sort(my_list.begin(), my_list.end(), PointerComparison::Less<std::shared_ptr<Foo>>());
             * \endcode
             *
             * will use the std::less<Foo> to decide which is the order (and not compile if there is no such order).
             *
             * On the contrary,
             *
             * \code
             * std::sort(my_list.begin(), my_list.end(), std::less<std::shared_ptr<Foo>>);
             * \endcode
             *
             * would sort the elements of the list according to their address; it would work each time but the
             * ordering would change at each run of the code (which could lead to very tricky bugs to solve, as they
             * appear only once in a while for specific orderings...).
             */
            template<class T>
            struct Less
            {


                //! \copydoc doxygen_hide_pointer_comp_type
                // clang-format off
                using comparison_type =
                    std::conditional_t
                    <
                        Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                        const T&,
                        T
                    >;
                // clang-format on


                //! Overload operator() so that Less might be used as a functor.
                //! \copydoc doxygen_hide_lhs_rhs_arg
                bool operator()(comparison_type lhs, comparison_type rhs) const;
            };


            //! See Less for explanation.
            template<class T>
            struct Greater
            {


                //! \copydoc doxygen_hide_pointer_comp_type
                // clang-format off
                using comparison_type =
                    std::conditional_t
                    <
                        Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                        const T&,
                        T
                    >;
                // clang-format on

                //! Overload operator() so that Great might be used as a functor.
                //! \copydoc doxygen_hide_lhs_rhs_arg
                bool operator()(comparison_type lhs, comparison_type rhs) const;
            };


            //! See Less for explanation.
            template<class T>
            struct Equal
            {


                //! \copydoc doxygen_hide_pointer_comp_type
                using comparison_type =
                    // clang-format off
                    std::conditional_t
                    <
                        Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                        const T&,
                        T
                    >;
                // clang-format on

                //! Overload operator() so that Less might be used as a functor.
                //! \copydoc doxygen_hide_lhs_rhs_arg
                bool operator()(comparison_type lhs, comparison_type rhs) const;
            };


            /*!
             * \brief A std::set that stores pointers and use the pointer comparisons.
             */
            template<class T, class CompareT = Less<T>>
            using Set = std::set<T, CompareT>;


            /*!
             * \brief A std::map that stores pointers and use the pointer comparisons.
             */
            template<class KeyT, class ValueT, class CompareT = Less<KeyT>>
            using Map = std::map<KeyT, ValueT, CompareT>;


        } // namespace PointerComparison


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Containers/PointerComparison.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_POINTER_COMPARISON_HPP_
