### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Array.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Array.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Delimiter.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/EnumClass.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PointerComparison.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PointerComparison.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Print.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Print.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Sort.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Tuple.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/UnorderedMap.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/UnorderedMap.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/PrintPolicy/SourceList.cmake)
