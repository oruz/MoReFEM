//! \file
//
//
//  Print.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_

// IWYU pragma: private, include "Utilities/Containers/Internal/Print.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::PrintNS
{


    template<class PrintPolicyT>
    SeparatorFacility<PrintPolicyT>::SeparatorFacility(std::ostream& stream,
                                                       ::MoReFEM::PrintNS::Delimiter::separator separator)
    : stream_(stream), separator_(separator)
    { }


    template<class PrintPolicyT, class T>
    SeparatorFacility<PrintPolicyT>& operator<<(SeparatorFacility<PrintPolicyT>& facility, const T& value)
    {
        if (facility.is_first_)
            facility.is_first_ = false;
        else
            facility.stream_ << facility.separator_.Get();

        PrintPolicyT::Do(facility.stream_, value);

        return facility;
    }


    template<class StreamT, std::size_t Index, std::size_t Max, class TupleT>
    void PrintTupleHelper<StreamT, Index, Max, TupleT>::Print(StreamT& stream,
                                                              const TupleT& t,
                                                              ::MoReFEM::PrintNS::Delimiter::separator separator)
    {
        using EltTupleType = typename std::tuple_element<Index, TupleT>::type;

        const auto quote = Utilities::String::IsString<EltTupleType>::value ? "\"" : "";

        stream << quote << std::get<Index>(t) << quote
               << (Index + 1 == Max ? ::MoReFEM::PrintNS::Delimiter::separator("") : separator).Get();
        PrintTupleHelper<StreamT, Index + 1, Max, TupleT>::Print(stream, t, separator);
    };


    template<class StreamT, std::size_t Max, class TupleT>
    void PrintTupleHelper<StreamT, Max, Max, TupleT>::Print(StreamT&,
                                                            const TupleT&,
                                                            ::MoReFEM::PrintNS::Delimiter::separator)
    {
        // Do nothing!
    }


} // namespace MoReFEM::Internal::PrintNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_
