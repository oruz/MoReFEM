/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 3 Jan 2018 22:18:50 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HXX_

// IWYU pragma: private, include "Utilities/Containers/Internal/Sort.hpp"


#include <cassert>
#include <tuple>


namespace MoReFEM
{


    namespace Internal
    {


        namespace SortNS
        {


            template<class T, class UnaryT>
            bool IsEqual(const T& lhs, const T& rhs)
            {
                return UnaryT::Value(lhs) == UnaryT::Value(rhs);
            }


            template<class T, int IndexT, class TupleT>
            bool SortHelper<T, IndexT, TupleT>::Compare(const T& lhs, const T& rhs)
            {
                enum
                {
                    size = std::tuple_size<TupleT>::value
                };

                using Criterion = typename std::tuple_element<size - 1 - IndexT, TupleT>::type;

                if (!IsEqual<T, Criterion>(lhs, rhs))
                {
                    return typename Criterion::StrictOrderingOperator()(Criterion::Value(lhs), Criterion::Value(rhs));
                }

                return SortHelper<T, IndexT - 1, TupleT>::Compare(lhs, rhs);
            }


            template<class T, class TupleT>
            bool SortHelper<T, 0, TupleT>::Compare(const T& lhs, const T& rhs)
            {
                enum
                {
                    size = std::tuple_size<TupleT>::value
                };

                using Criterion = typename std::tuple_element<size - 1, TupleT>::type;

                if (!IsEqual<T, Criterion>(lhs, rhs))
                    return typename Criterion::StrictOrderingOperator()(Criterion::Value(lhs), Criterion::Value(rhs));

                // False is returned here as we consider the sorting criterion
                // expects a strict ordering criterion: typically STL functions
                // expect < and not <=.
                // So when \a lhs and \a rhs are completely equal for all criteria,
                // we do not want to return true (which would mean there is
                // actually a difference in one of the sorting criterion).
                return false;
            }


        } // namespace SortNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_SORT_HXX_
