/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Aug 2013 13:36:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_HPP_

#include <cstddef>    // IWYU pragma: keep
#include <functional> // for std::mem_fn
#include <tuple>

#include "Utilities/Containers/Internal/TupleHelper.hpp"
#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Utilities
    {


        namespace Tuple
        {


            /*!
             ** \brief Find the index of a given type in a tuple (used here as a typelist)
             **
             ** If none found, value is NumericNS::UninitializedIndex<std::size_t>, otherwise the index is returned.
             **
             **
             ** If a same structure appears several time in the tuple (which doesn't make
             ** much sense for a typelist), the index of the last occurrence is returned
             ** (not the most logical but I had to workaround with C++ standard limitations)
             **
             ** \tparam T Class looked at in the tuple
             ** \tparam TupleT std::tuple which is investigated
             *
             * NOTE: Since C++ 14, std::get provides basically the same functionality. However, there are two
             * restrictions current class doesn't have, the second of which is problematic for our use:
             * - std::get<> fails to compile if a same type appears twice in the tuple, whereas here the first
             * instance is returned with IndexOf. In our use std::get<> behavior would be as good (if not better)
             * than our current implementation.
             * - std::get<> fails to compile if T doesn't exist in the tuple. In some cases we would like to handle
             * it more smoothly hence IndexOf that in this case just returns -1.
             */
            template<class T, class TupleT>
            class IndexOf
            {

              private:
                //! Value of the last index to consider in \a TupleT.
                enum
                {
                    last_index = std::tuple_size<TupleT>::value - 1
                };

              public:
                //! Current value of the index.
                enum
                {
                    value = Internal::Tuple::IndexOfHelper<T, TupleT, last_index>::value
                };
            };


            /*!
             * \brief Check the same type is not present twice in \a TupleT.
             *
             * When tuples are used as a typelist, in some cases we want to be sure the same parameter isn't present
             * twice.
             *
             * Perform() method of this class checks that at compilation time.
             *
             * \code
             * AssertNoDuplicate<*tuple to check*>::Perform();
             * \endcode
             *
             * \internal <b><tt>[internal]</tt></b> This method might lead to a compilation error due to the important
             * number of recursions involved as soon as the tuple is lengthy enough. You might have to increase the
             * limit through compilation option -ftemplate-depth = *the value you want to put*. \endinternal
             */
            template<class TupleT>
            class AssertNoDuplicate
            {

              private:
                //! Store internally the size of the tuple.
                enum
                {
                    tuple_size = std::tuple_size<TupleT>::value,
                };

              public:
                //! Method that does the actual work.
                static void Perform()
                {
                    if (tuple_size >= 2)
                        Internal::Tuple::AssertNoDuplicateHelper<TupleT, 0, tuple_size>::Perform();
                }
            };


            /*!
             * \brief Returns true if all the containers yield the same size, false otherwise.
             *
             * \tparam TupleT Tuple of all the containers considered.
             *
             * \param[in] tuple All containers considered.
             *
             * \return True if all the containers yield the same size.
             */
            template<class TupleT>
            bool AreEqualLength(const TupleT& tuple)
            {
                enum
                {
                    size = std::tuple_size<TupleT>::value - 1
                };
                return Internal::Tuple::AreEqualLengthHelper<TupleT, 0, size - 1>::Check(tuple);
            }


            /*!
             * \brief Helper class to help unpack tuple arguments.
             *
             * BEWARE: this function works only for free functions; to use it upon methods resort to
             * CallMethodWithArgumentsFromTuple.
             *
             * When using C++ 11 variadic templates (i.e. templates which number of arguments can vary) it is often very
             * useful to pack all these arguments within a std::tuple, for instance if they are given along to another
             * function.
             *
             * Template variadic are used in places not directly accessible to even an advanced developer (i.e. one able
             * to define its own operators); so the purpose is that the function this developer has to use remains
             * straight in its arguments; for instance: \code void MyLocalOperator::ComputeEltArray(double coefficient,
             *                                       const std::vector<double>&
             * additional_arg_specific_for_this_operator); \end
             *
             * is much more natural to define than:
             *
             * \code
             * void MyLocalOperator::ComputeEltArray(double coefficient,
             *                                       const std::tuple<const std::vector<double>&>& tuple);
             * \end
             *
             * So the purpose of current function is to carry out the 'conversion' from the tuple to the list of
             * arguments.
             *
             * The way to do this is to call the following function; in the low-level code in the library that will
             * actually calls the ComputeEltArray for all operators there will be a call:
             *
             * \code
             * std::tuple<const std::vector<double>> tuple; // defined in the variadic template call
             * ...
             * Utilities::Tuple::CallFunctionWithArgumentsFromTuple<void>(MyLocalOperator::ComputeEltArray, tuple);
             * \endcode
             *
             * This code is very complicated and should be used only at very low level. I'm not its author (it is beyond
             * my metaprogramming skill); it was inspired by a solution in a StackOverflow forum:
             * http://stackoverflow.com/questions/10766112/c11-i-can-go-from-multiple-args-to-tuple-but-can-i-go-from-tuple-to-multiple
             * I have just made it more generic by adding the possibility of a return type rather than just using
             * void functions.
             *
             * \param[in] f The function.
             * \param[in] tuple The arguments to pass to the method.
             *
             * \return The returned value (except is ReturnTypeT is void)
             */
            template<typename ReturnTypeT, typename FunctionPrototypeT, typename TupleT>
            inline ReturnTypeT CallFunctionWithArgumentsFromTuple(FunctionPrototypeT f, TupleT&& tuple)
            {
                using ttype = typename std::decay_t<TupleT>;
                return Internal::Tuple::CallFunctionWithArgumentsFromTuple<
                    ReturnTypeT,
                    FunctionPrototypeT,
                    TupleT,
                    0 == std::tuple_size<ttype>::value,
                    std::tuple_size<ttype>::value>::Perform(f, std::forward<TupleT>(tuple));
            }


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Specialization of the namesake function for void return type.
            template<typename FunctionPrototypeT, typename TupleT>
            inline void CallFunctionWithArgumentsFromTuple(FunctionPrototypeT f, TupleT&& tuple)
            {

                using ttype = typename std::decay_t<TupleT>;
                Internal::Tuple::CallFunctionWithArgumentsFromTuple<
                    void,
                    FunctionPrototypeT,
                    TupleT,
                    0 == std::tuple_size<ttype>::value,
                    std::tuple_size<ttype>::value>::Perform(f, std::forward<TupleT>(tuple));
            }
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Same as \a CallFunctionWithArgumentsFromTuple for methods.
             *
             * \param[in] object The object which method is called.
             * \param[in] f The method.
             * \param[in] tuple The arguments to pass to the method.
             *
             * \return The returned value (except is ReturnTypeT is void)
             */
            template<typename ReturnTypeT, typename ObjectT, typename FunctionPrototypeT, typename TupleT>
            inline ReturnTypeT CallMethodWithArgumentsFromTuple(ObjectT& object, FunctionPrototypeT f, TupleT&& tuple)
            {
                using ttype = typename std::decay_t<TupleT>;
                return Internal::Tuple::CallMethodWithArgumentsFromTuple<
                    ReturnTypeT,
                    FunctionPrototypeT,
                    TupleT,
                    0 == std::tuple_size<ttype>::value,
                    std::tuple_size<ttype>::value>::Perform(object, f, std::forward<TupleT>(tuple));
            }


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Specialization of the namesake function for void return type.
            template<typename ObjectT, typename FunctionPrototypeT, typename TupleT>
            inline void CallMethodWithArgumentsFromTuple(ObjectT& object, FunctionPrototypeT f, TupleT&& tuple)
            {

                using ttype = typename std::decay_t<TupleT>;
                Internal::Tuple::CallMethodWithArgumentsFromTuple<
                    void,
                    FunctionPrototypeT,
                    TupleT,
                    0 == std::tuple_size<ttype>::value,
                    std::tuple_size<ttype>::value>::Perform(object, f, std::forward<TupleT>(tuple));
            }
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief The purpose of this struct is to return a new tuple which includes the decayed types of \a
             * original_tuple.
             *
             * For instance,
             * \code
             * using original_tuple = std::tuple<const Foo&, int&&, double>;
             * Tuple::Decay<original_tuple>::type decayed_tuple = std::make_tuple(Foo(), 5, 7.4); // yields <Foo, int,
             * double> \endcode
             */
            template<class OriginalTupleT>
            struct Decay
            {

                static_assert(IsSpecializationOf<std::tuple, OriginalTupleT>::value,
                              "Compilation error if OriginalTupleT is not a std::tuple.");

                //! Resulting type.
                using type = typename Internal::Tuple::
                    Decay<OriginalTupleT, std::tuple<>, 0, std::tuple_size<OriginalTupleT>::value>::type;
            };


            /*!
             * \brief Check whether \a TupleT gets an element ItemT for which OperatorT<T, ItemT>() is true.
             *
             * \tparam TupleT std::tuple being looked at.
             * \tparam T Type sought in the tuple.
             * \tparam OperatorT Template operator an element of the tuple has to satisfy for the struct to yield
             * a type compatible with true.
             *
             * This code is inspired from:
             * https://stackoverflow.com/questions/25958259/how-do-i-find-out-if-a-tuple-contains-a-type
             *
             * HasType yields a value compatible with true if for at least one element ItemT OperatorT<T, ItemT>() is
             * true (see std::disjunction for more details).
             */
            template<class T, template<class, class> class OperatorT, class TupleT>
            struct HasType;


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Stop condition.
            // ============================


            template<class T, template<class, class> class OperatorT, class... Us>
            struct HasType<T, OperatorT, std::tuple<Us...>> : std::disjunction<OperatorT<T, Us>...>
            { };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace Tuple


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_HPP_
