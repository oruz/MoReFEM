//! \file
//
//
//  Associative.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HPP_

#include <cassert>
#include <memory>
#include <vector>

#include "Utilities/String/Traits.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{

    /*!
     * \brief Enum class to decide the format to adopt.
     *
     * Two are proposed so far:
     * - Default: the format is (key, value), i.e.:
     *      * Opening by '('
     *      * Closing by ')'
     *      * Separation by ", ".
     * - Lua: the format is [key] = value (or ['key'] = value if is_key_string is set to yes.
     */
    enum class associative_format
    {
        Default,
        Lua
    };

    /*!
     * \brief Policy to handle the an associative container.
     *
     * \tparam FormatT Format to use for the output. It is rather rigid now; it is easy to make it more customizable
     * if need be (for instance default one could use different opening, closing or separation characters).
     */
    template<associative_format FormatT = associative_format::Default>
    struct Associative
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


#include "Utilities/Containers/PrintPolicy/Associative.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HPP_
