//! \file
//
//
//  Pointer.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HPP_


#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy to handle the content of a container of (eventually smart) pointers.
     *
     * Pointers are dereferenced before printing.
     *
     * This policy assumes the container is not associative.
     */
    struct Pointer
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


#include "Utilities/Containers/PrintPolicy/Pointer.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_POINTER_HPP_
