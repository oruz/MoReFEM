//! \file
//
//
//  Normal.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HPP_

#include <ostream>


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy to handle the 'normal' case, i.e. when elements inside the container are directly displayable
     * with operator <<.
     *
     * This policy assumes the container is not associative.
     */
    struct Normal
    {

      public:
        /*!
         * \class doxygen_hide_print_policy_do
         *
         * \brief Static method in charge of the actual work.
         *
         * \param[in,out] stream The stream onto which the container is printed.
         * \param[in] element Element of the container which is to be displayed.
         *
         * \tparam Type of the element to be displayed.
         */

        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


#include "Utilities/Containers/PrintPolicy/Normal.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HPP_
