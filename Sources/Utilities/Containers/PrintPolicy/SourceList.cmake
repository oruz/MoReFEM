### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Associative.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Associative.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Key.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Key.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Normal.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Normal.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Pointer.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Pointer.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Variant.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Variant.hxx"
)

