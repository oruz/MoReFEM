//! \file
//
//
//  Associative.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HXX_

// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Key.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class ElementTypeT>
    void Key::Do(std::ostream& stream, ElementTypeT&& element)
    {
        stream << element.first;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HXX_
