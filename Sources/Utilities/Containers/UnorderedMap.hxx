/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Nov 2016 11:38:45 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HXX_

// IWYU pragma: private, include "Utilities/Containers/UnorderedMap.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Utilities
    {


        template<class T>
        inline std::size_t HashCombine(std::size_t& seed, const T& value)
        {
            seed ^= std::hash<T>()(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
            return seed;
        }


        template<class ContainerT>
        std::size_t ContainerHash::operator()(const ContainerT& container) const
        {
            assert(!container.empty());

            using type = typename ContainerT::value_type;

            std::size_t ret = std::hash<type>()(container[0]);

            const auto size = container.size();

            for (auto i = 1ul; i < size; ++i)
                HashCombine(ret, container[i]);

            return ret;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_UNORDERED_MAP_HXX_
