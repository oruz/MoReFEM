/*!
 */


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_

// IWYU pragma: private, include "Utilities/Containers/Print.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Utilities
    {

        template<class PrintPolicyT>
        template<class ContainerT, typename StreamT>
        void PrintContainer<PrintPolicyT>::Do(const ContainerT& container,
                                              StreamT& stream,
                                              PrintNS::Delimiter::separator separator,
                                              PrintNS::Delimiter::opener opener,
                                              PrintNS::Delimiter::closer closer)
        {
            std::ostringstream oconv;
            oconv.copyfmt(stream);
            oconv << opener.Get();

            Internal::PrintNS::SeparatorFacility<PrintPolicyT> separator_facility(oconv, separator);

            for (const auto& element : container)
                separator_facility << element;

            oconv << closer.Get();
            stream << oconv.str();
        }


        template<class PrintPolicyT>
        template<std::size_t N, class ContainerT, typename StreamT>
        void PrintContainer<PrintPolicyT>::Nelt(const ContainerT& container,
                                                StreamT& stream,
                                                PrintNS::Delimiter::separator separator,
                                                PrintNS::Delimiter::opener opener,
                                                PrintNS::Delimiter::closer closer)
        {
            std::ostringstream oconv;
            oconv.copyfmt(stream);
            oconv << opener.Get();

            Internal::PrintNS::SeparatorFacility<PrintPolicyT> separator_facility(oconv, separator);

            auto it = container.cbegin();
            auto end = it;
            std::advance(end, static_cast<typename ContainerT::difference_type>(std::min(N, container.size())));

            for (; it != end; ++it)
                separator_facility << *it;

            oconv << closer.Get();
            stream << oconv.str();
        }


        template<class TupleT, typename StreamT>
        void PrintTuple(const TupleT& tuple,
                        StreamT& stream,
                        PrintNS::Delimiter::separator separator,
                        PrintNS::Delimiter::opener opener,
                        PrintNS::Delimiter::closer closer)
        {
            std::ostringstream oconv;
            oconv.copyfmt(stream);
            oconv << opener.Get();
            enum
            {
                size = std::tuple_size<TupleT>::value
            };
            Internal::PrintNS::PrintTupleHelper<StreamT, 0, size, TupleT>::Print(oconv, tuple, separator);
            oconv << closer.Get();
            stream << oconv.str();
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HXX_
