/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Jun 2013 10:41:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <iostream> // mandatory due to std::cout as default parameter
#include <map>
#include <sstream>
#include <string_view>
#include <tuple>
#include <variant>

#include "Utilities/Containers/Delimiter.hpp"          // IWYU pragma: export
#include "Utilities/Containers/Internal/Print.hpp"     // IWYU pragma: export
#include "Utilities/Containers/PrintPolicy/Normal.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"                 // IWYU pragma: export
#include "Utilities/String/Traits.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Print the content of a container (that might be associative or not - see \a PrintPolicyT).
         *
         * \tparam PrintPolicyT A policy which determines how the printing of the element of the container behaves.
         * Several policies are proposed in Utilities/Containers/PrintPolicy directory:
         * - 'Normal': which prints a non-associative container which elements may be displayed directory with
         * operator<<.
         * - 'Variant': same as Normal, except that if type is std::variant a visitor is used to print it properly.
         * - 'Pointer': for a non-associative container of (smart) pointers; the element is dereferenced before being
         * printed.
         * - 'Associative': for an associative container; each key/value is printed under format (key, value) (i.e.
         * parenthesis as open/close and ", " as separator).
         * - 'Key': prints only the keys of an associative containers.
         */
        template<class PrintPolicyT = PrintPolicyNS::Normal>
        struct PrintContainer
        {


            /*!
             * \class doxygen_hide_print_container_common_arg
             *
             * \tparam StreamT Type of output stream considered
             * \tparam ContainerT Type of the container to be displayed (also see class template parameter
             * \a PrintPolicyT for more details).
             *
             * \param[in,out] stream Output stream in which container will be displayed
             * \param[in] container Container displayed
             * \param[in] separator Separator between two entries of the contained
             * \param[in] opener Prefix used while displaying the container
             * \param[in] closer Suffix used while displaying the container
             */


            /*!
             * \brief Function to actually print the content of a container.
             *
             * \internal I am using struct/static method only to deal more finely with default template parameters:
             * we want the container type to be inferred automatically but the policy to use to be customizable.
             * \endinternal
             *
             * \copydoc doxygen_hide_print_container_common_arg
             *
             * In most cases templates parameters can be determined implicitly at compile time:
             * \code
             * std::vector<double> foo { 1., 2., 3., 10., 42. };
             * std::ostringstream oconv;
             * PrintContainer<>::Do(foo, oconv, " ", "---", "---")
             * \endcode
             * This code yields:
             *   ---1. 2. 3. 10. 42.---
             */
            template<class ContainerT, typename StreamT = std::ostream>
            static void Do(const ContainerT& container,
                           StreamT& stream = std::cout,
                           PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                           PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                           PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));


            /*!
             * \brief Function to print the \a N first elements of a container (or the whole container if N is greater
             * than its size).
             *
             * \copydoc doxygen_hide_print_container_common_arg
             * \tparam N Maximum number of elements to display.
             */
            template<std::size_t N, class ContainerT, typename StreamT = std::ostream>
            static void Nelt(const ContainerT& container,
                             StreamT& stream = std::cout,
                             PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                             PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                             PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));
        };


        /*!
         * \brief Print the content of a tuple or a pair.
         *
         * Inspired by Nicolai M. Josuttis "The C++ standard library" page 74.
         *
         * \tparam StreamT Type of output stream considered
         *
         * \param[in,out] stream Output stream in which tuple content will be displayed. All tuple elements must
         * define operator<<.
         * \param[in] tuple Tuple which content is  displayed.
         * \param[in] separator Separator between two entries of the tuple.
         * \param[in] opener Prefix used while displaying the tuple.
         * \param[in] closer Suffix used while displaying the tuple.
         */
        template<class TupleT, typename StreamT = std::ostream>
        void PrintTuple(const TupleT& tuple,
                        StreamT& stream = std::cout,
                        PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                        PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                        PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Containers/Print.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_
