//! \file
//
//
//  Delimiter.hpp
//  MoReFEM
//
//  Created by sebastien on 06/07/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_DELIMITER_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_DELIMITER_HPP_

#include <string_view>

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM::PrintNS::Delimiter
{


    //! Strong type for the type of the string that opens a container (e.g. "[ " for "[ 0, 1, 2, 3 ]"
    using opener = StrongType<std::string_view, struct OpenerTag>;

    //! Strong type for the type of the string that separates itemsr (e.g. ", " for "[ 0, 1, 2, 3 ]"
    using separator = StrongType<std::string_view, struct SeparatorTag>;

    //! Strong type for the type of the string that closes a container (e.g. " ]" for "[ 0, 1, 2, 3 ]"
    using closer = StrongType<std::string_view, struct CloserTag>;


} // namespace MoReFEM::PrintNS::Delimiter


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_DELIMITER_HPP_
