//! \file
//
//
//  HasMember.hpp
//  MoReFEM
//
//  Created by sebastien on 19/06/2019.
//  Copyright © 2019 Inria. All rights reserved.
//
// This code is lifted and slightly adapted from StackOverflow answer https://stackoverflow.com/a/16867422;
// I have simplified as I do not need the entire complexity of the code. Finding this code was a boon: many references
// could be found but most do not pass basic tests I wrote (for instance many versions are dumbfounded if the sought
// function is overloaded).

#ifndef MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_INTERNAL_x_HAS_MEMBER_HPP_
#define MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_INTERNAL_x_HAS_MEMBER_HPP_


namespace MoReFEM::Internal::HasMemberNS
{


    // ================================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ================================


    /*
     - Multiple inheritance forces ambiguity of member names.
     - SFINAE is used to make aliases to member names.
     - Expression SFINAE is used in just one generic HAS_MEMBER that can accept
     any alias we pass it.
     */

    // Variadic to force ambiguity of class members.  C++11 and up.
    template<typename... Args>
    struct Ambiguate : public Args...
    { };

    template<typename A, typename = void>
    struct GotType : std::false_type
    { };

    template<typename A>
    struct GotType<A> : std::true_type
    {
        typedef A type;
    };

    template<typename T, T>
    struct SignatureCheck : std::true_type
    { };

    template<typename Alias, typename AmbiguitySeed>
    struct HasMember
    {
        template<typename C>
        static char (&f(decltype(&C::value)))[1];
        template<typename C>
        static char (&f(...))[2];

        // Make sure the member name is consistently spelled the same.
        static_assert((sizeof(f<AmbiguitySeed>(nullptr)) == 1) // 0 instead of nullptr in original answer
                      ,
                      "Member name specified in AmbiguitySeed is different from member name specified in Alias, "
                      "or wrong Alias/AmbiguitySeed has been specified.");

        static bool const value = sizeof(f<Alias>(nullptr)) == 2; // 0 instead of nullptr in original answer
    };


    // ================================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ================================

} // namespace MoReFEM::Internal::HasMemberNS


#endif // MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_INTERNAL_x_HAS_MEMBER_HPP_
