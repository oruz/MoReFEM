//! \file
//
//
//  HasMember.hpp
//  MoReFEM
//
//  Created by sebastien on 19/06/2019.
//  Copyright © 2019 Inria. All rights reserved.
//
// This code is lifted and slightly adapted from StackOverflow answer https://stackoverflow.com/a/16867422;
// I have simplified as I do not need the entire complexity of the code. Finding this code was a boon: many references
// could be found but most do not pass basic tests I wrote (for instance many versions are dumbfounded if the sought
// function is overloaded).
// The major modification is a tiny static_assert to exclude properly POD and enums as template parameters.

#ifndef MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_HAS_MEMBER_HPP_
#define MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_HAS_MEMBER_HPP_

#include "Utilities/HasMember/Internal/HasMember.hpp"

/*!
 * \class doxygen_hide_create_member_check_macro
 *
 * \brief Check for any member with given name, whether var, func, class, union or  enum.
 *
 * Usage:
 * - First and foremost call the macro CREATE_MEMBER_CHECK for the name of member you seek. Make sure to do so outside
 * of a function definition!
 * - Then you may use HAS_MEMBER_## member_name<T>::value where T is the class evaluated (space added to avoid Doxygen
 * warning).
 * The best is probably to provide an example:
 *
 * \code
 CREATE_MEMBER_CHECK(size);

 HAS_MEMBER_size<std::vector<int>>::value; // should evaluate to true
 HAS_MEMBER_size<std::true_type>::value; // should evaluate to false
 \endcode
 *
 */

//! \copydoc doxygen_hide_create_member_check_macro
//!  \param[in] member The member for which HAS_MEMBER is generated. \a size in the example above.
#define CREATE_MEMBER_CHECK(member)                                                                                    \
                                                                                                                       \
    template<typename T, typename = std::true_type>                                                                    \
    struct Alias_##member;                                                                                             \
                                                                                                                       \
    template<typename T>                                                                                               \
    struct Alias_##member<                                                                                             \
        T,                                                                                                             \
        std::integral_constant<bool, MoReFEM::Internal::HasMemberNS::GotType<decltype(&T::member)>::value>>            \
    {                                                                                                                  \
        static const decltype(&T::member) value;                                                                       \
    };                                                                                                                 \
                                                                                                                       \
    struct AmbiguitySeed_##member                                                                                      \
    {                                                                                                                  \
        char member;                                                                                                   \
    };                                                                                                                 \
                                                                                                                       \
    template<typename T>                                                                                               \
    struct HAS_MEMBER_##member                                                                                         \
    {                                                                                                                  \
        static_assert(std::is_class<T>(), "Not working for POD types or enums");                                       \
                                                                                                                       \
        static const bool value = MoReFEM::Internal::HasMemberNS::HasMember<                                           \
            Alias_##member<MoReFEM::Internal::HasMemberNS::Ambiguate<T, AmbiguitySeed_##member>>,                      \
            Alias_##member<AmbiguitySeed_##member>>::value;                                                            \
    }

#endif // MOREFEM_x_UTILITIES_x_HAS_MEMBER_x_HAS_MEMBER_HPP_
