/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 May 2014 11:56:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_TIME_KEEP_x_TIME_KEEP_HPP_
#define MOREFEM_x_UTILITIES_x_TIME_KEEP_x_TIME_KEEP_HPP_

#include <chrono>
#include <fstream> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Class used to profile crudely (through prints) the code.
     *
     * In 'normal' release mode, this class should be kept at minimum: just keep in memory the starting date, and
     * write the total elapsed time at the end of the simulation.
     * If you define macro MOREFEM_EXTENDED_TIME_KEEP, an additional method named PrintTimeElapsed() is made
     * available so that prints can be added to profile finely time elapsed between each call of this method.
     */
    class TimeKeep : public Utilities::Singleton<TimeKeep>
    {
      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

#ifdef MOREFEM_EXTENDED_TIME_KEEP
        /*!
         * \brief Print the time elapsed since Init() has been called, in milliseconds.
         *
         * \param[in] label Specify whatever you want to specify where the call occurs.
         */
        void PrintTimeElapsed(const std::string& label);
#endif // MOREFEM_EXTENDED_TIME_KEEP

        //! Return the time elapsed since the beginning of the simulation.
        //! \return Time elapsed since the beginning of the simulation, under the format HH::MM::SS.
        std::string TimeElapsedSinceBeginning() const;

        /*!
         * \brief At the end of the program, write in the stream the time elapsed since beginning.
         *
         * Should be called at the end of a Model.
         */
        void PrintEndProgram();


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in,out] stream Stream upon which time informations will be written.
         */
        explicit TimeKeep(std::ofstream&& stream);

        //! Destructor.
        virtual ~TimeKeep() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<TimeKeep>;
        ///@}


      private:
        //! Stream into which the prints are logged.
        std::ofstream stream_;

        //! Time at which Init() has been called.
        const std::chrono::time_point<std::chrono::steady_clock> init_time_;

        //! Time of the previous call to PrintTimeElapsed.
        std::chrono::time_point<std::chrono::steady_clock> previous_call_time_;
    };


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_TIME_KEEP_x_TIME_KEEP_HPP_
