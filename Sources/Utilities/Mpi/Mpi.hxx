/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 16:17:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_MPI_x_MPI_HXX_
#define MOREFEM_x_UTILITIES_x_MPI_x_MPI_HXX_

// IWYU pragma: private, include "Utilities/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT>
        CrtpMpi<DerivedT>::CrtpMpi(const Wrappers::Mpi& mpi) : mpi_(mpi)
        { }


        template<class DerivedT>
        const Wrappers::Mpi& CrtpMpi<DerivedT>::GetMpi() const noexcept
        {
            return mpi_;
        }


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_MPI_x_MPI_HXX_
