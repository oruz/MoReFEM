/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 16:17:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_
#define MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_


#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief This CRTP class provides to its derived class a Mpi object and an accessor to it.
         */
        template<class DerivedT>
        class CrtpMpi
        {
          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydetails doxygen_hide_mpi_param
             */
            explicit CrtpMpi(const Wrappers::Mpi& mpi);

            //! Destructor.
            ~CrtpMpi() = default;

            //! \copydoc doxygen_hide_copy_constructor
            CrtpMpi(const CrtpMpi& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            CrtpMpi(CrtpMpi&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            CrtpMpi& operator=(const CrtpMpi& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            CrtpMpi& operator=(CrtpMpi&& rhs) = delete;

            ///@}

            //! Read-only access to underlying \a Mpi object.
            //! \return Constant accessor to underlying Mpi object.
            const Wrappers::Mpi& GetMpi() const noexcept;

          private:
            //! Mpi object.
            const Wrappers::Mpi& mpi_;
        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Mpi/Mpi.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_
