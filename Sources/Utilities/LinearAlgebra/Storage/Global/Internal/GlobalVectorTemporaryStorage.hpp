/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HPP_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HPP_

#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>


namespace MoReFEM
{


    namespace GlobalLinearAlgebraNS
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================

        template<class GlobalVectorTemporaryStorageT>
        class GlobalVectorTemporaryAccess;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


    } // namespace GlobalLinearAlgebraNS


    namespace Internal
    {


        namespace GlobalLinearAlgebraNS
        {


            /*!
             * \brief Class which actually stores the \a GlobalVector needed in GlobalVectorTemporary CRTP.
             *
             * \copydetails doxygen_hide_global_vector_temporary_manager_class
             */
            template<class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
            class GlobalVectorTemporaryStorage
            {

              public:
                //! \copydoc doxygen_hide_alias_self
                using self = GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>;

                //! Alias to unique ptr.
                using unique_ptr = std::unique_ptr<self>;

                //! Friendship to the class that provides the RAII access.
                friend GlobalVectorTemporaryAccess<self>;

                //! Alias to \a GlobalVectorT.
                using vector_type = GlobalVectorT;

                //! Alias to a type that help disambiguate some method calls.
                using identifier_type = typename std::integral_constant<std::size_t, IdentifierT>::type;

              public:
                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] model Work vector will be built from this \a GlobalVector: it will retain same
                 * \a NumberingSubset and \a GodOfDof.
                 */
                explicit GlobalVectorTemporaryStorage(const GlobalVectorT& model);

                //! Destructor.
                ~GlobalVectorTemporaryStorage() = default;

                //! \copydoc doxygen_hide_copy_constructor
                GlobalVectorTemporaryStorage(const GlobalVectorTemporaryStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GlobalVectorTemporaryStorage(GlobalVectorTemporaryStorage&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GlobalVectorTemporaryStorage& operator=(const GlobalVectorTemporaryStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GlobalVectorTemporaryStorage& operator=(GlobalVectorTemporaryStorage&& rhs) = delete;

                ///@}


              private:
                /*!
                 * \brief Determine an index that points to a \a GlobalVector currently unused.
                 *
                 * \return Index that will provide a currently unused \a GlobalVector (it's up to
                 * GlobalVectorTemporaryAccess to handle it).
                 */
                std::size_t DeterminedUnusedIndex() noexcept;


              private:
                //! Storage.
                typename GlobalVectorT::template array_unique_ptr<N> storage_;

                //! Holds a 1 if the value is currently used somewhere. Released when
                std::bitset<N> usage_;
            };


        } // namespace GlobalLinearAlgebraNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HPP_
