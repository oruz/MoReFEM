/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HPP_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalLinearAlgebraNS
        {


            /*!
             * \brief Class which actually provides acess to a temporary \a GlobalVector needed in GlobalVectorTemporary
             * scheme.
             *
             * \copydetails doxygen_hide_global_vector_temporary_manager_class
             *
             * \attention The class is actually never called directly as the alias provided in GlobalVectorTemporary
             * is much more convenient; hence its 'internal' status.
             */
            template<class WorkVariableT>
            class GlobalVectorTemporaryAccess
            {


              public:
                /*!
                 * \brief Constructor.
                 *
                 * \param[in] work_variable \a WorkVariable object which stores all the needed work variables.
                 *
                 * This is the main constructor... but not the easiest to use; the template one provide a much
                 * more satisfying interface (which does not require for instance to know in the call the value of
                 * \a N.
                 */
                explicit GlobalVectorTemporaryAccess(WorkVariableT& work_variable);

                /*!
                 * \brief Template constructor.
                 *
                 * \param[in] data The data structure for which the work variable is defined. For instance
                 * DataNS::Monolithic, DataNS::FluidVelocity, etc... This structure must provide a private
                 * GetNonCstTemporaryGlobalVectorManager() which returns a reference to an internal \a WorkVariable and
                 * a friendship to current class.
                 */
                template<class DataT>
                GlobalVectorTemporaryAccess(const DataT& data);

                //! Destructor.
                ~GlobalVectorTemporaryAccess();

                //! \copydoc doxygen_hide_copy_constructor
                GlobalVectorTemporaryAccess(const GlobalVectorTemporaryAccess& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GlobalVectorTemporaryAccess(GlobalVectorTemporaryAccess&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GlobalVectorTemporaryAccess& operator=(const GlobalVectorTemporaryAccess& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GlobalVectorTemporaryAccess& operator=(GlobalVectorTemporaryAccess&& rhs) = delete;

                //! Accessor to the vector.
                typename WorkVariableT::vector_type& GetNonCstVector() noexcept;


              private:
                /*!
                 * \brief The \a WorkVariable class which will provide the current \a GlobalVector.
                 */
                WorkVariableT& work_variable_;

                /*!
                 * \brief Index in \a work_variable_ \a storage_ data attribute of the \a GlobalVector to use.
                 *
                 */
                const std::size_t vector_index_;
            };


        } // namespace GlobalLinearAlgebraNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryAccess.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HPP_
