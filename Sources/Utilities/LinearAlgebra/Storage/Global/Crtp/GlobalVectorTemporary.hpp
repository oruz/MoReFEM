/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HPP_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryAccess.hpp"
#include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryStorage.hpp"


namespace MoReFEM
{


    namespace GlobalLinearAlgebraNS
    {


        /*!
         * \class doxygen_hide_global_vector_temporary_manager_class
         *
         * \brief Gives to a \a DerivedT class access to one or several unnamed \a GlobalVector for temporary storage.
         *
         * The point is that assigning a \a GlobalVector is rather expensive, and sometimes you need one for
         * a brief moment - for instance you might want to compute a vector which is used only in a subsequent
         * matrix/vector product.
         *
         * A naive way to handle this would be to create directly a \a GlobalVector within DerivedT, which could be used
         * whenever you need a placeholder vector for temporary computation. However, it is not very secure:
         *
         * \code
         *
         * void Foo(const MyData& my_data)
         * {
         *      decltype(auto) work = my_data.GetNonCstWork();
         *       ...
         * }
         *
         * MyData my_data (...) // we assume MyData, the class which needs a temporary \a GlobalVector, is built
         * properly here.
         *
         *
         *
         * {
         *      decltype(auto) work = my_data.GetNonCstWork();
         *      ...
         *      Foo();
         *      ...
         *      work.View(); // work use now the value used in Foo(), which was not what was intended.
         * }
         *
         * \endcode
         *
         * The present class aims to circumvent this issue: it stores N vectors, and when you need one you require
         * access through an object (RAII is used). Code above would be now:
         *
         * \code
         *
         * void Foo(const MyData& my_data)
         * {
         *      MyData::global_vector_temporary_manager temporary_vector(my_data);
         *      decltype(auto) contribution = my_data.GetNonCstVector();
         *       ...
         * }
         *
         * MyData my_data (...) // we assume MyData, the class which needs a temporary \a GlobalVector, is built
         properly here.
         *
         * {
         *      MyData::global_vector_temporary_manager temporary_vector(my_data);
         *      decltype(auto) contribution = my_data.GetNonCstVector();
         *      ...
         *      Foo(); // in here another vector is assigned, provided N is big enough... If not the program fails.
         *      ...
         *      contribution.View(); // contribution is really the local one asked above, regardless of what Foo() did.
         * }
         *
         * \endcode\
         *
         * The two lines are necessary for the call, as RAII is used (i.e. the temporary vector might be used elsewhere
         * as soon as temporary_vector local object is destroyed).
         *
         * So to put in a nutshell, \a GlobalVector are defined and do exist as long as the \a DerivedT object, but
         * their access is strictly controlled so that we're sure the same temporary might not be used at two locations
         * at the same time.
         *
         * \tparam DerivedT The class upon which Crtp is applied.
         * \tparam GlobalVectorT Should be 'GlobalVector' in MoReFEM (templatized only because GlobalVector is not
         * known yet in Utilities library).
         * \tparam N Number of temporary \a GlobalVector the class needs. If not high enough the program will assert
         * (in the example above, if N = 1 you would get this assert).
         * \tparam IdentifierT Crtp provides access to an object that might include many vectors, but all of them are
         * expected to follow the same pattern. However, in somes cases you might want to use temporaries for two kind
         * of vectors (see DataNS::FluidPressure in Poromechanics model, that use temporary vectors defined on two
         * different meshes). In this case, you might inherit twice from current class, changing this IdentifierT
         * parameter to distinguish them both. Calls of methods needs in this case to specify explicitly the parent,
         * for instance:
         * \code
         *  using global_vector_temporary_solid_parent =
         GlobalLinearAlgebraNS::GlobalVectorTemporary<Porosity, GlobalVector, 1ul,
         EnumUnderlyingType(MeshIndex::solid)>;
         * global_vector_temporary_solid_parent::InitGlobalVectorTemporary(...);
         *
         * ...
         * global_vector_temporary_solid_parent::global_vector_temporary_manager access_solid_vector(...);
         * ...
         * \endcode
         *
         * \attention If a same \a DerivedT uses up more than 1 GlobalVectorTemporary, it must define in its interface
         * for each GlobalVectorTemporary:
         * \code
         * using parent_1::GetNonCstTemporaryGlobalVectorManager;
         * using parent_2::GetNonCstTemporaryGlobalVectorManager;
         * \endcode
         *
         * \attention It is essential to call InitGlobalVectorTemporary() before any use: this method correctly
         initialize
         * all the \a GlobalVector following the structure of a given global vector.
         */

        //! \copydoc doxygen_hide_global_vector_temporary_manager_class
        template<class DerivedT, class GlobalVectorT, std::size_t N, std::size_t IdentifierT = 0>
        class GlobalVectorTemporary
        {

          public:
            //! Object that stores work vectors with the same characteristics as current_ one.
            using global_vector_temporary_manager_storage =
                Internal::GlobalLinearAlgebraNS::GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>;

            /*!
             * \class doxygen_hide_work_variable_usage
             *
             * For instance:
             * \code
             * Monolithic::global_vector_temporary_manager work(monolithic_data);
             * auto& work_variable = work.GetNonCstVector();
             * \endcode
             */

            /*!
             * \brief Useful alias to use a work variable.
             *
             * \copydoc doxygen_hide_work_variable_usage
             */
            using global_vector_temporary_manager =
                Internal::GlobalLinearAlgebraNS::GlobalVectorTemporaryAccess<global_vector_temporary_manager_storage>;

            //! Init work variables.
            //! \param[in] vector An already assigned global vector which will acts as a template for all the global
            //! vectors to assign throughout the code.
            void InitGlobalVectorTemporary(const GlobalVectorT& vector);

            /*!
             * \brief Call the object that store the \a N \a GlobalVector defined for temporary usage.
             *
             * \param[in] placeholder To enable disambiguation when
             * a same \a DerivedT class uses up several \a GlobalVectorTemporary.
             *
             * \return Reference to the relevant \a global_vector_temporary_manager.
             */
            global_vector_temporary_manager_storage& GetNonCstTemporaryGlobalVectorManager(
                typename std::integral_constant<std::size_t, IdentifierT>::type placeholder) const noexcept;

          private:
            /*!
             * \brief Structure that stores work vectors.
             *
             * There are N vectors inside this object, all sharing the same internal build (CSR pattern, etc...)
             */
            mutable typename global_vector_temporary_manager_storage::unique_ptr storage_ = nullptr;
        };


    } // namespace GlobalLinearAlgebraNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/LinearAlgebra/Storage/Global/Crtp/GlobalVectorTemporary.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HPP_
