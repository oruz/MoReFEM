/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HXX_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HXX_

// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Global/Crtp/GlobalVectorTemporary.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace GlobalLinearAlgebraNS
    {


        template<class DerivedT, class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
        void GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::InitGlobalVectorTemporary(
            const GlobalVectorT& model)
        {
            storage_ = std::make_unique<global_vector_temporary_manager_storage>(model);
        }


        template<class DerivedT, class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
        inline typename GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::
            global_vector_temporary_manager_storage&
            GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::GetNonCstTemporaryGlobalVectorManager(
                typename std::integral_constant<std::size_t, IdentifierT>::type placeholder) const noexcept
        {
            static_cast<void>(placeholder);
            assert(!(!storage_) && "Make sure InitGlobalVectorTemporary() was properly called!");
            return *storage_;
        }


    } // namespace GlobalLinearAlgebraNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_CRTP_x_GLOBAL_VECTOR_TEMPORARY_HXX_
