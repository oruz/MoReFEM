/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Mar 2015 14:57:33 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HPP_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief CRTP to give access to \a NlocalVectorT local vectors.
         *
         * \tparam DerivedT Name of the base class for which the CRTP is deployed.
         * \tparam NlocalVectorT Number of local vectors to add.
         *
         * It is advised to declare in \a DerivedT an enum class to tag the local vectors, e.g.:
         * \code
         *  enum class LocalVectorIndex : std::size_t
         *  {
         *      new_contribution = 0,
         *      local_rhs,
         *      dW,
         *      ...
         *  };
         * \endcode
         */

        template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT = LocalVector>
        class LocalVectorStorage
        {

          public:
            //! Return the number of local vectors.
            //! \return Number of local vectors.
            static constexpr std::size_t Size();


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * Does nothing: the computation of the dimension in the constructor of DerivedT proved to be unreadable.
             * So InitLocalVectorStorage() must absolutely be called to set appropriately the dimensions.
             */
            explicit LocalVectorStorage() = default;


          protected:
            //! Destructor.
            ~LocalVectorStorage() = default;

            //! \copydoc doxygen_hide_copy_constructor
            LocalVectorStorage(const LocalVectorStorage& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            LocalVectorStorage(LocalVectorStorage&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            LocalVectorStorage& operator=(const LocalVectorStorage& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            LocalVectorStorage& operator=(LocalVectorStorage&& rhs) = default;


            ///@}


          public:
            /*!
             * \brief This method must be called in the constructor of DerivedT, once the dimensions have been
             * computed.
             *
             * For safety reasons, vector is filled with 0 to avoid undefined behaviour if no value is actually given.
             *
             * \param[in] vectors_dimension For each \a LocalVector stored this way, the integer is its size.
             */
            void InitLocalVectorStorage(const std::array<std::size_t, NlocalVectorT>& vectors_dimension);

            /*!
             * \brief Access to the \a IndexT -th local vector.
             *
             * \return Access to the \a IndexT -th local vector.
             *
             * The idea is to use it as such:
             * \code
             * auto& local_vector = GetLocalVector<0>();
             * ... (use local_vector variable in the following)
             * \endcode
             *
             * \internal <b><tt>[internal]</tt></b> This method is const becaue we might want to use local vectors in
             * const methods of DerivedT; as local vectors are bound to be used within a single method they are
             * declared as mutable.
             * \endinternal
             */
            template<std::size_t IndexT>
            LocalVectorT& GetLocalVector() const;


          private:
            //! Local vectors stored.
            mutable std::array<LocalVectorT, NlocalVectorT> vector_list_;
        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HPP_
