/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Mar 2015 14:57:33 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_
#define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_

// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
        constexpr std::size_t LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::Size()
        {
            return NlocalVectorT;
        }


        template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
        void LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::InitLocalVectorStorage(
            const std::array<std::size_t, NlocalVectorT>& vectors_dimension)
        {
            for (std::size_t i = 0ul; i < NlocalVectorT; ++i)
            {
                auto& vector = vector_list_[i];
                vector.resize({ static_cast<std::size_t>(vectors_dimension[i]) });
                vector.fill(0.);
            }
        }


        template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
        template<std::size_t IndexT>
        inline LocalVectorT& LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::GetLocalVector() const
        {
            static_assert(IndexT < NlocalVectorT, "Check index is within bounds!");
            return vector_list_[IndexT];
        }


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_VECTOR_STORAGE_HXX_
