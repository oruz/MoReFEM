//! \file
//
//
//  Now.hpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_DATETIME_x_NOW_HPP_
#define MOREFEM_x_UTILITIES_x_DATETIME_x_NOW_HPP_

#include <string> // IWYU pragma: export
// IWYU pragma: no_include <iosfwd>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Write the current date in format 'YYYY-MM-DD_HH:MM:SS'.
         *
         * \param[in] mpi The mpi facility. Current function is there to tag output directories, so we do not want a
         * mismatcch among ranks due to slightly different time. So root processor transmits its time to all ranks.
         *
         * \return The string at the format specified above.
         *
         * \internal I could probably have used std::chrono library, but I'm not very familiar with it and lots of
         * features (including some that I think would do in a breeze what this function does) are set to appear in
         * C++20. \endinternal
         */
        std::string Now(const Wrappers::Mpi& mpi);


    } // namespace Utilities


} // namespace MoReFEM


#endif // MOREFEM_x_UTILITIES_x_DATETIME_x_NOW_HPP_
