//! \file
//
//
//  ReadBinaryFile.cpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <cstring> // required by gcc for std::memcpy.
#include <fstream>
#include <string>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/OutputFormat/ReadBinaryFile.hpp"


namespace MoReFEM::Advanced
{


    std::vector<double>
    ReadSimpleBinaryFile(const std::string& binary_file, const char* invoking_file, int invoking_line, double epsilon)
    {
        std::vector<double> ret;

        std::ifstream in;
        FilesystemNS::File::Read(in, binary_file, invoking_file, invoking_line);

        if (in)
        {
            // Get length of file.
            std::ifstream::pos_type block_size_helper;
            block_size_helper = in.seekg(0, std::ifstream::end).tellg();
            in.seekg(0, std::ifstream::beg);

            const auto block_size = static_cast<std::size_t>(block_size_helper);

            std::vector<char> buffer(block_size);

            // Read data as a block.
            in.read(buffer.data(), block_size_helper);

            if (!in)
                throw Exception("Unable to read file " + binary_file, invoking_file, invoking_line);

            in.close();

            // Buffer contains the entire file. Convert bytes back into doubles.
            const auto Ndouble_values = block_size / sizeof(double);
            ret.resize(Ndouble_values);
            std::memcpy(ret.data(), buffer.data(), block_size);

            for (auto& item : ret)
            {
                if (std::fabs(item) <= epsilon)
                    item = 0.;
            }
        }

        return ret;
    }


} // namespace MoReFEM::Advanced
