/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HXX_
#define MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HXX_

// IWYU pragma: private, include "Utilities/OutputFormat/OutputFormat.hpp"


#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        inline binary_or_ascii OutputFormat::IsBinaryOutput() const noexcept
        {
            return binary_output_;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HXX_
