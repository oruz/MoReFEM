/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HPP_
#define MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HPP_

#include <iosfwd>
#include <memory>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp"  // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Provides a way to specify the output format of the solution vectors corresponding to each
         * unknown.
         *
         * This singleton acts as a global variable in order to store the boolean value related to the format
         * of the output. It's only purpose is to propagate this information throughout the whole code.
         * \tparam  binary_output_ is set to false by default, which outputs solution files in ascii format.
         * If is set to true (through Result::OutputBinary) then the solution vectors are written in binary format.
         *
         */
        class OutputFormat final : public Utilities::Singleton<OutputFormat>
        {

          public:
            //! \copydoc doxygen_hide_alias_self
            using self = OutputFormat;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

          private:
            //! \name Singleton requirements.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] binary_output True to write resulting vectors in binary, false to write them in ascii.
             */
            explicit OutputFormat(bool binary_output);

            //! Destructor.
            virtual ~OutputFormat() override;

            //! Friendship declaration to Singleton template class (to enable call to constructor).
            friend class Utilities::Singleton<OutputFormat>;

            //! Name of the class.
            static const std::string& ClassName();


            ///@}


          public:
            //! Output format for solution vectors.
            binary_or_ascii IsBinaryOutput() const noexcept;

          private:
            //! Used to define whether the output is ascii or binary.
            binary_or_ascii binary_output_ = binary_or_ascii::ascii;
        };


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/OutputFormat/OutputFormat.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_OUTPUT_FORMAT_HPP_
