//! \file
//
//
//  ReadBinaryFile.hpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_READ_BINARY_FILE_HPP_
#define MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_READ_BINARY_FILE_HPP_

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include <vector>

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::Advanced
{


    /*!
     * \brief Read the content of a binary file and load it into a std::vector.
     *
     * This function is intended to work only with very basic binary files, for which double values were dumped
     * directly within the binary file.
     *
     * \param[in] binary_file The file which content is sought.
     * \param[in] epsilon Values read that are below this threshold are replaced by 0.
     * \copydoc doxygen_hide_invoking_file_and_line
     *
     * \return The content of the file on disk as double values.
     */
    std::vector<double> ReadSimpleBinaryFile(const std::string& binary_file,
                                             const char* invoking_file,
                                             int invoking_line,
                                             double epsilon = NumericNS::DefaultEpsilon<double>());


} // namespace MoReFEM::Advanced


#endif // MOREFEM_x_UTILITIES_x_OUTPUT_FORMAT_x_READ_BINARY_FILE_HPP_
