/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include "Utilities/OutputFormat/OutputFormat.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        OutputFormat::~OutputFormat() = default;


        OutputFormat::OutputFormat(bool binary_output)
        : binary_output_(binary_output ? binary_or_ascii::binary : binary_or_ascii::ascii)
        { }


        const std::string& OutputFormat::ClassName()
        {
            static std::string ret("OutputFormat");
            return ret;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
