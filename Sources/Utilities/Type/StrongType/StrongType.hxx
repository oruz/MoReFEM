/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz on 26/02/2020.
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/StrongType.hpp"


namespace MoReFEM
{


    template<class T, class Parameter, template<typename> class... Skills>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(T const& value) : value_(value)
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    template<class T_>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(
        T&& value,
        // clang-format off
                                                    std::enable_if_t
                                                    <
                                                        !std::is_reference<T_>{},
                                                        std::nullptr_t
                                                    >)
    // clang-format on
    : value_(std::move(value))
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    template<class T_>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(
        std::enable_if_t<typename almost_self<T_>::enable_default_constructor{}, std::nullptr_t>)
    : value_{}
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    inline T const& StrongType<T, Parameter, Skills...>::Get() const noexcept
    {
        return value_;
    }


    template<class T, class Parameter, template<typename> class... Skills>
    inline T& StrongType<T, Parameter, Skills...>::Get() noexcept
    {
        return value_;
    }


    template<class T, class ParameterT, template<typename> class... Skills>
    std::ostream& operator<<(std::ostream& stream, const StrongType<T, ParameterT, Skills...>& object)
    {
        object.Print(stream);
        return stream;
    }


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HXX_
