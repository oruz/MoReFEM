/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz on 26/02/2020.
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HPP_

#include <iostream>
#include <type_traits>
#include <utility>

namespace MoReFEM
{


    /*!
     * \brief Abstract class used to define a StrongType, which allows more expressive code and ensures that the order
     * of arguments of the same underlying type is respected at call sites.
     *
     * Adapted from https://github.com/joboccara/NamedType/
     *
     * \tparam T Type of the parameter.
     * \tparam Parameter This is a phantom type used to specialize the type deduced for SrongType, it is not actually
     * used in the implementation. \tparam Skills Optional arguments to grant additional functionalities to the created
     * StrongType (see below and
     * https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/ post).
     *
     * As an example we could create a class Rectangle with strong types to differentiate its width from its length as:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag>;
     * using Height = StrongType<double, struct HeightTag>;
     *
     * class StrongRectangle
     * {
     * public:
     *     StrongRectangle (Width width, Height height) : width_(width.Get()), height_(height.Get()) {}
     *     double getWidth() const {return width_;}
     *     double getHeight() const {return height_;}
     *
     * private:
     *     double width_;
     *     double height_;
     * };
     * \endcode
     *
     * At the calling site we would have:
     *
     * \code
     * StrongRectangle rectangle((Width(5.0)), (Height((2.0))));
     * \endcode
     *
     * Note that the extra parenthesis are only required for constructors due to the most vexing parse.
     *
     * As for \a Skills: a StrongType does not allow direct operations upon its instances. For instance the following
     * code is invalid:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag>;
     * Width w1(5.);
     * Width w2(10.);
     * Width sum = w1 + w2; // does not compile!
     * \endcode
     *
     * The new arguments enable to add support for it; to do so we provide a new Crtp (called Addable here) which grants
     * support for operator+:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag, Addable>;
     * Width w1(5.);
     * Width w2(10.);
     * Width sum = w1 + w2; // ok!
     * \endcode
     *
     * The list of possible options is in the 'Skills' subdirectory; you may define your own if needed (it is basically
     * a Crtp). Several may be added in the declaration:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag, Addable, Incrementable>;
     * \endcode
     */
    template<class T, class Parameter, template<typename> class... Skills>
    class StrongType : public Skills<StrongType<T, Parameter, Skills...>>...
    {
      private:
        //! An alias to the class type... except the \a T parameter is not fixed. Useful for one constructor which is
        //! defined only if specific conditions are met (through std::enable_if_t). This code was written before C++ 20
        //! was mainstream enough to be used within the code; its 'require' would of course greatly simplified such
        //! constructor definitions and this trick wouldn't be required at all.
        template<class T_>
        using almost_self = StrongType<T_, Parameter, Skills...>;


      public:
        //! Traits to indicate the object is a strong type.
        static constexpr bool IsStrongType{ true };


        //! Convenient alias.
        using underlying_type = T;

        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         */
        explicit constexpr StrongType(T const& value);

        /*!
         * \brief Move constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         * \tparam T_ Artifical template parameter to ensure that SFINAE applies in case \a T  is  a reference.
         *
         */
        template<class T_ = T>
        explicit constexpr StrongType(T&& value, std::enable_if_t<!std::is_reference<T_>{}, std::nullptr_t> = nullptr);

        /*!
         * \brief Default constructor, which is defined only when enable_default_constructor is defined.
         *
         * This should happen only when skill \a DefaultConstructible is chosen.
         *
         * \tparam T_ Artifical template parameter to ensure that SFINAE applies.
         *
         */
        template<class T_ = T>
        explicit constexpr StrongType(
            std::enable_if_t<typename almost_self<T_>::enable_default_constructor{}, std::nullptr_t> = nullptr);

        //! Destructor.
        ~StrongType() = default;

        //! \copydoc doxygen_hide_copy_constructor
        StrongType(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        StrongType(StrongType&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        StrongType& operator=(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        StrongType& operator=(StrongType&& rhs) = default;


        ///@}

      public:
        //! Constant accessor to the underlying value held by the strong type.
        T const& Get() const noexcept;

        //! Non constant accessor to the underlying value held by the strong type.
        T& Get() noexcept;

      private:
        //! Underlying value held by the strong type.
        T value_;
    };


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * This one works only if a \a Print(std::ostream&) method exists in \a StrongType class; skill \a Printable defines
     * such a method.
     */
    template<class T, class ParameterT, template<class> class... Skills>
    std::ostream& operator<<(std::ostream& stream, const StrongType<T, ParameterT, Skills...>& rhs);


    /*!
     * \brief Facility to determine whether a class \a T is a \a StrongType or not.
     *
     * \internal Utilities::IsSpecializationOf doesn't work with \a StrongType and my metaprogramming skill is not high enough to figure out how
     * to make it work in a reasonable time hence this dedicated one.
     */
    template<typename T>
    struct IsStrongType : std::false_type
    { };

    template<class T, class Parameter, template<typename> class... Skills>
    struct IsStrongType<StrongType<T, Parameter, Skills...>> : std::true_type
    { };


} // namespace MoReFEM


namespace std
{


    /*!
     * \brief Provide hash function for \a StrongType only if enabled explicitly in \a Skills.
     */
    template<typename T, typename Parameter, template<typename> class... Skills>
    struct hash<::MoReFEM::StrongType<T, Parameter, Skills...>>
    {

        //! Alias to the \a StrongType considered.
        using type = ::MoReFEM::StrongType<T, Parameter, Skills...>;

        //! Activated only is is_hashable token is defined (SFINAE)
        //! See https://www.fluentcpp.com/2017/05/30/implementing-a-hash-function-for-strong-types/.
        using check_is_hashable = typename std::enable_if<type::is_hashable, void>::type;

        /*!
         * \brief Operator which does the bulk of the work and returns the hash.
         *
         * \param[in] x Strong-typed value for which a hash is rquested.
         *
         * \return Hash of the value hidden within the \a StrongType.
         */
        size_t operator()(::MoReFEM::StrongType<T, Parameter, Skills...> const& x) const
        {
            return std::hash<T>()(x.Get());
        }
    };


} // namespace std


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Type/StrongType/StrongType.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_STRONG_TYPE_HPP_
