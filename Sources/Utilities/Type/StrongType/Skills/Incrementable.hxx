//! \file
//
//
//  Incrementable.hxx
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Incrementable.hpp"


namespace MoReFEM::StrongTypeNS
{


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator+=(StrongTypeT const& other)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        underlying.Get() += other.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator++()
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        ++underlying.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT Incrementable<StrongTypeT>::operator++(int)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        StrongTypeT temp = underlying;
        ++underlying.Get();
        return temp;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator-=(StrongTypeT const& other)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        underlying.Get() -= other.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator--()
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        --underlying.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT Incrementable<StrongTypeT>::operator--(int)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        StrongTypeT temp = underlying;
        --underlying.Get();
        return temp;
    }


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HXX_
