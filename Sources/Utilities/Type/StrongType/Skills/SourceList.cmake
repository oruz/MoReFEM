### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Addable.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Addable.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/AsMpiDatatype.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Comparable.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Comparable.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/DefaultConstructible.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Divisible.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Divisible.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Hashable.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Incrementable.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Incrementable.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Printable.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Printable.hxx"
)

