//! \file
//
//
//  Incrementable.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HPP_


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator+ is enabled for the \a
     * StrongType.
     */
    template<typename StrongTypeT>
    struct Incrementable
    {

        /*!
         * \brief Operator+= for the \a StrongTypeT.
         *
         * \param[in] rhs Second member of the += operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT& operator+=(StrongTypeT const& rhs);

        /*!
         * \brief Operator++ (prefix increment) for the \a StrongTypeT.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT& operator++();

        /*!
         * \brief Operator++ (postfix increment) for the \a StrongTypeT.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator++(int);

        /*!
         * \brief Operator-= for the \a StrongTypeT.
         *
         * \param[in] rhs Second member of the -= operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT& operator-=(StrongTypeT const& rhs);

        /*!
         * \brief Operator-- (prefix increment) for the \a StrongTypeT.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT& operator--();

        /*!
         * \brief Operator-- (postfix increment) for the \a StrongTypeT.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator--(int);
    };


} // namespace MoReFEM::StrongTypeNS


#include "Utilities/Type/StrongType/Skills/Incrementable.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_INCREMENTABLE_HPP_
