//! \file
//
//
//  Addable.hpp
//  MoReFEM
//
//  Created by sebastien on 26/05/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HPP_

#include <type_traits>

namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator% is enabled for the \a
     * StrongType.
     */
    template<typename StrongTypeT>
    struct Divisible
    {


        /*!
         * \brief Operator/ for the \a StrongTypeT.
         *
         * \param[in] other Second member of the / operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator/(StrongTypeT const& other) const;

        /*!
         * \brief Operator% for the \a StrongTypeT.
         *
         * \param[in] other Second member of the % operator.
         *
         * This operator is defined only for integral types, as % yields a compilation error for floating-point types.
         *
         * \tparam StrongTypeT_ Artificial type introduced to use SFINAE here; never use it with something other than \a StrongTypeT !
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        template<class StrongTypeT_ = StrongTypeT>
        std::enable_if_t<std::is_integral_v<typename StrongTypeT_::underlying_type>, StrongTypeT>
        operator%(StrongTypeT const& other) const;
    };


} // namespace MoReFEM::StrongTypeNS


#include "Utilities/Type/StrongType/Skills/Divisible.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HPP_
