//! \file
//
//
//  Addable.hxx
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Addable.hpp"


namespace MoReFEM::StrongTypeNS
{


    template<class StrongTypeT>
    StrongTypeT Addable<StrongTypeT>::operator+(StrongTypeT const& other) const
    {
        return StrongTypeT(static_cast<StrongTypeT const&>(*this).Get() + other.Get());
    }


    template<class StrongTypeT>
    StrongTypeT Addable<StrongTypeT>::operator-(StrongTypeT const& other) const
    {
        return StrongTypeT(static_cast<StrongTypeT const&>(*this).Get() - other.Get());
    }


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HXX_
