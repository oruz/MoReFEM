//! \file
//
//
//  Addable.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HPP_

#include <iosfwd>


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator<< is enabled for the \a
     * StrongType.
     *
     * It assumes the type hidden in \a StrongTypeT provides itself an overload to operator<</
     */
    template<typename StrongTypeT>
    struct Printable
    {


        //! Print the strong type on stteam \a os.
        //! \param[in,out] os Stream onto which the strong type will be written.
        void Print(std::ostream& os) const;
    };


} // namespace MoReFEM::StrongTypeNS


#include "Utilities/Type/StrongType/Skills/Printable.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HPP_
