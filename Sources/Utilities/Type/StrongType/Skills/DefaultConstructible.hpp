//! \file
//
//
//  DefaultConstructible.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DEFAULT_CONSTRUCTIBLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DEFAULT_CONSTRUCTIBLE_HPP_

#include <type_traits>


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType, default constructor is enabled.
     *
     */
    template<typename StrongTypeT>
    struct DefaultConstructible
    {


        //! Type which is defined in this class only - and will be used indirectly to enable a specific \a StrongType
        //! constructor.
        using enable_default_constructor = std::true_type;
    };


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DEFAULT_CONSTRUCTIBLE_HPP_
