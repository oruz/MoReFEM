//! \file
//
//
//  Addable.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_HASHABLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_HASHABLE_HPP_


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType, a hash specialization is provided
     * (using the underlying type hash)
     *
     * See https://www.fluentcpp.com/2017/05/30/implementing-a-hash-function-for-strong-types/ for more explanation.
     */
    template<typename StrongTypeT>
    struct Hashable
    {


        //! Token: the heavy work is done in std::hash specialization in \a StrongType, which is enabled only if there
        //! is such a token.
        static constexpr bool is_hashable = true;
    };


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_HASHABLE_HPP_
