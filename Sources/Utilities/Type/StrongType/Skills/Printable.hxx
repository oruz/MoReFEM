//! \file
//
//
//  Addable.hxx
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Printable.hpp"


namespace MoReFEM::StrongTypeNS
{


    template<class StrongTypeT>
    void Printable<StrongTypeT>::Print(std::ostream& stream) const
    {
        stream << static_cast<const StrongTypeT&>(*this).Get();
    }


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_PRINTABLE_HXX_
