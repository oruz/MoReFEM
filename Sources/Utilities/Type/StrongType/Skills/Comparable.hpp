//! \file
//
//
//  Addable.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_COMPARABLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_COMPARABLE_HPP_


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator< and == are enabled for the \a
     * StrongType.
     *
     * \todo #1553 Use spaceship operator when C++ 20 is out.
     *
     */
    template<typename StrongTypeT>
    struct Comparable
    {

        /*!
         * \brief Operator< for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        bool operator<(StrongTypeT const& other) const noexcept;


        /*!
         * \brief Operator== for the \a StrongTypeT.
         *
         * \param[in] other Second member of the operator.
         *
         * \return The result.
         */
        bool operator==(StrongTypeT const& other) const noexcept;

        /*!
         * \brief Operator> for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        bool operator>(StrongTypeT const& other) const noexcept;

        /*!
         * \brief Operator<= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        bool operator<=(StrongTypeT const& other) const noexcept;


        /*!
         * \brief Operator>= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        bool operator>=(StrongTypeT const& other) const noexcept;


        /*!
         * \brief Operator!= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the operator.
         *
         * \return The result.
         */
        bool operator!=(StrongTypeT const& other) const noexcept;
    };


} // namespace MoReFEM::StrongTypeNS


#include "Utilities/Type/StrongType/Skills/Comparable.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_COMPARABLE_HPP_
