/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_HXX_
#define MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_HXX_

// IWYU pragma: private, include "Utilities/Environment/Environment.hpp"
#include <cstdlib>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/String/Traits.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        template<class T>
        std::string
        Environment::GetEnvironmentVariable(T&& variable, const char* invoking_file, int invoking_line) const
        {
            static_assert(Utilities::String::IsString<T, Utilities::String::CharAllowed::no>::value);

            // First check internally.
            const auto it = values_.find(variable);

            if (it != values_.cend())
                return it->second;

            // If not found, see if shell defines it.
            char* value = nullptr;

            if constexpr (std::is_same<std::decay_t<T>, std::string>())
                value = std::getenv(variable.c_str());
            else
                value = std::getenv(variable);

            if (value == nullptr)
            {
                std::ostringstream oconv;
                oconv << "Environment variable " << variable << " is not defined!";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            return std::string(value);
        }


        template<class T>
        inline bool Environment::DoExist(T&& variable) const
        {
            static_assert(Utilities::String::IsString<T, Utilities::String::CharAllowed::no>::value);

            const auto it = values_.find(variable);

            if (it != values_.cend())
                return true;

            if constexpr (std::is_same<std::decay_t<T>, std::string>())
                return std::getenv(variable.c_str()) != nullptr;
            else
                return std::getenv(variable) != nullptr;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_HXX_
