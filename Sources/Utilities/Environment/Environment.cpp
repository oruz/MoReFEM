/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <cassert>
#include <cstdlib>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/String/String.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        Environment::~Environment() = default;


        Environment::Environment()
        {
            values_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        }


        const std::string& Environment::ClassName()
        {
            static std::string ret("Environment");
            return ret;
        }


        std::string Environment::SubstituteValues(std::string string) const
        {

            auto begin_env_pos = string.find("${");
            auto end_env_pos = string.find("}");

            while (end_env_pos != std::string::npos && begin_env_pos < end_env_pos)
            {
                std::string environment_variable_name_with_braces =
                    string.substr(begin_env_pos, end_env_pos - begin_env_pos + 1ul);

                std::string environment_variable_name =
                    environment_variable_name_with_braces.substr(2, environment_variable_name_with_braces.size() - 3ul);

                auto environment_variable_value = GetEnvironmentVariable(environment_variable_name, __FILE__, __LINE__);

                Utilities::String::Replace(environment_variable_name_with_braces, environment_variable_value, string);

                // Look out for other environment variables to substitute.
                begin_env_pos = string.find("${");
                end_env_pos = string.find("}");
            }

            return string;
        }


        void Environment::SetEnvironmentVariable(std::pair<std::string, std::string> variable,
                                                 const char* invoking_file,
                                                 int invoking_line)
        {
            const auto& key = variable.first;

            if (DoExist(key))
            {
                std::ostringstream oconv;
                oconv << "The environment variable " << key << " is already defined ";

                if (std::getenv(key.c_str()) != nullptr)
                    oconv << " in your shell.";
                else
                    oconv << " in MoReFEM internals. Make sure to set it only once";

                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            const auto check = values_.insert(std::move(variable));
            assert(check.second);
            static_cast<void>(check);
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
