### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Exception.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/GracefulExit.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/PrintAndAbort.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Exception.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GracefulExit.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PrintAndAbort.hpp"
)

