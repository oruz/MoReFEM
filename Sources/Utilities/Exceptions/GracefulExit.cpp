/*!
//
// \file
//

// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <sstream>

#include "Utilities/Exceptions/GracefulExit.hpp"


namespace MoReFEM::ExceptionNS
{


    GracefulExit::GracefulExit(const char* invoking_file, int invoking_line)
    {
        std::ostringstream oconv;
        oconv << "A graceful exit of the program was required at file " << invoking_file << " and line "
              << invoking_line << ". The program will therefore end and return EXIT_SUCCESS.";

        what_message_ = oconv.str();
    }


    GracefulExit::~GracefulExit() = default;


    const char* GracefulExit::what() const noexcept
    {
        return what_message_.c_str();
    }


} // namespace MoReFEM::ExceptionNS


/// @} // addtogroup UtilitiesGroup
