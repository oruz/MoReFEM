/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <iostream>
#include <sstream> // IWYU pragma: keep
#include <type_traits>

#include "Utilities/Exceptions/Exception.hpp"


namespace // anonymous
{


    //! Call this function to set the message displayed by the exception.
    void SetWhatMessage(const std::string& msg, std::string& what_message, const char* invoking_file, int invoking_line)
    {
        std::ostringstream stream;
        stream << "Exception found at ";
        stream << invoking_file << ", line " << invoking_line << ": ";
        stream << msg;
        what_message = stream.str();
    }


} // namespace


namespace MoReFEM
{


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : std::exception(), raw_message_(msg)
    {
        SetWhatMessage(msg, what_message_, invoking_file, invoking_line);
    }


    Exception::~Exception() noexcept = default;


    const char* Exception::what() const noexcept
    {
        return what_message_.c_str();
    }


    void ThrowBeforeMain(Exception&& exception)
    {
        std::cerr << "ERROR before main(): " << exception.what() << std::endl;

        // Throw anyway in case one of the exception appears within main() function; however it
        // is foreseen to be called in functions BEFORE the main, and hence result in abortion.
        throw std::move(exception);
    }


    const std::string& Exception::GetRawMessage() const noexcept
    {
        return raw_message_;
    }


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
