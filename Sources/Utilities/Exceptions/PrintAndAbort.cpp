/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 4 Mar 2015 10:51:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <iostream>
#include <sstream>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Warnings/Pragma.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        void PrintAndAbort(const Wrappers::Mpi& mpi, const std::string& exception_message)
        {

            // Trick to ensure there is no interleaving between processors: use a ostringstream so that only one
            // operation is given to std::cout. std::cout is used rather than std::cerr because it is buffered
            // (std::cerr here could lead to interleaving).
            std::ostringstream oconv;
            oconv << mpi.GetRankPreffix() << " Exception caught: " << exception_message << std::endl;
            oconv << "Program has therefore been terminated." << std::endl;
            std::cout << oconv.str();

            // Call MPI_Abort to shut down all process.
            // I followed a recommendation of
            // http://www.ucs.cam.ac.uk/docs/course-notes/unix-courses/MPI/files/mphil/talk-01b.pdf and use it on
            // MPI_COMM_WORLD whatever the communicator in mpi local variable.
            PRAGMA_DIAGNOSTIC(push)
            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
            PRAGMA_DIAGNOSTIC(pop)
        }


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
