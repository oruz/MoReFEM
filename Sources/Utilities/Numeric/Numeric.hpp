/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Apr 2014 14:47:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_
#define MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_


#include <cassert>
#include <cmath>
#include <limits>
#include <type_traits>
#include <vector>


namespace MoReFEM
{


    namespace NumericNS
    {


        /*!
         * \brief When an  index is not known at object constructor, better assign a controlled dumb value
         * than not knowing what the compiler decided.
         *
         * In most cases (typically for instance the initialization of a class attribute) it is a good idea to
         * use it along with decltype:
         *
         * \code
         * MyClass::MyClass()
         * : index_(MoReFEM::NumericNS::UninitializedIndex<decltype(index_)>())
         * { }
         *
         * \endcode
         *
         * Doing so makes the code robust to a change of type: if some day index_ becomes a std::size_t rather than
         * an int the default value will keep being the greatest value it can reach.
         *
         * \return Known value when an index is not properly initialized (currently it is the highest possible value for
         * \a T).
         */
        template<class T>
        constexpr T UninitializedIndex() noexcept;


        /*!
         * \class doxygen_hide_pow_like_operator
         *
         * \note std::pow() is more versatile but far less efficient than a simple call to operator*. Defining
         * a function is not just laziness: if value is for instance computed on the spot it is done only once;
         * consider for instance Operator(f(5)): f(5) is computed only once!
         *
         * \tparam T Type of the value; it is assumed operator* is defined for this type.
         *
         * \param[in] value Value upon which the operation is performed.
         *
         */

        /*!
         * \brief Computes the square of a value.
         *
         * \copydoc doxygen_hide_pow_like_operator
         *
         * \return value * value.
         */
        template<class T>
        constexpr T Square(T value) noexcept;


        /*!
         * \brief Computes the cube of a value.
         *
         * \copydoc doxygen_hide_pow_like_operator
         *
         * \return value * value * value.
         */
        template<class T>
        constexpr T Cube(T value) noexcept;


        /*!
         * \brief Computes the 4th power of a value.
         *
         * \copydoc doxygen_hide_pow_like_operator
         *
         * \return value * value * value * value.
         */
        template<class T>
        constexpr T PowerFour(T value) noexcept;


        /*!
         * \brief Returns value if it is positive or 0 otherwise.
         *
         * \tparam T Type of the value considered; it must both be initialized with 0 and comes with an operator<.
         *
         * \param[in] value Value upon which the operation is performed.
         *
         * \return value if value > 0, 0 otherwise.
         */
        template<class T>
        constexpr T AbsPlus(T value) noexcept;


        /*!
         * \brief Returns -1, 0 or 1 depending on the sign of tge value.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * \tparam T Type of the value; 0, 1 and -1 must be convertible to this type and operator< must be defined for
         * it.
         *
         * \return -1 if negative, 1 if positive, 0 if null (as determined by the \a IsZero() function).
         */
        template<class T>
        constexpr T Sign(T value) noexcept;


        /*!
         * \brief A special version of \a Sign which considers 0 as positive.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * \tparam T Type of the value; 0, 1 and -1 must be convertible to this type and operator< must be defined for
         * it.
         *
         * This is directly lifted from HeartLab code; TrueSign is like the sign except zero is counted as positive.
         *
         * \return -1 if negative, 1 if positive or zero (as determined by the \a IsZero function).
         */
        template<class T>
        constexpr T TrueSign(T value) noexcept;

        /*!
         * \brief Heaviside function.
         *
         * \param[in] value Value which sign is evaluated.
         *
         * \tparam T A floating-point type.
         *
         * \return 0 if negative, 1 if positive, 0.5 if null (as determined by the \a IsZero function).
         */
        template<class T>
        constexpr T Heaviside(T value) noexcept;


        /*!
         * \brief Returns a floating-point value that is small enough for most purposes.
         *
         * Currently set to 1.e-15 is used here.
         * \note We use a function for this so that there are no magic number in the code and the same value
         * is used everywhere.
         *
         * \tparam T Type for which the value is required; a double must be convertible into this type.
         *
         * \return 1.e-15.
         */
        template<class T>
        constexpr std::enable_if_t<std::is_floating_point<T>::value, T> DefaultEpsilon() noexcept;


        /*!
         * \brief Check whether a value is close enough to zero to be able to be considered as 0.
         *
         * \tparam T Floating point type considered (float, double or long double).
         *
         * \param[in] value Value that is tested.
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         *
         * \return True if \a value might be considered as null or not.
         */
        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool> IsZero(T value,
                                                                        T epsilon = DefaultEpsilon<T>()) noexcept;

        /*!
         * \brief Check whether a value is 0.
         *
         * \tparam T Integral type considered.
         *
         * \param[in] value Value that is tested.
         *
         * \internal For an integral type such a function is trivial but it might be useful from a metaprogramming standpoint.
         *
         * \return True if \a value might be considered as null or not.
         */
        template<class T>
        std::enable_if_t<std::is_integral<T>::value, bool> IsZero(T value) noexcept;


        /*!
         * \brief Check whether a value is close enough to another.
         *
         * \tparam T Floating point type considered (float, double or long double).
         *
         * \param[in] lhs Lhs value.
         * \param[in] rhs Rhs value.
         * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
         * is there if you want to play with it.
         *
         * \return True if \a lhs and \a rhs are deemed to be close enough to be considered equal.
         */
        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool>
        AreEqual(T lhs, T rhs, T epsilon = DefaultEpsilon<T>()) noexcept;


        /*!
         * \brief Checks whether a quantity is finite and defined, and if not say so explicitly.
         *
         * \param[in] value The value being addressed.
         *
         * \return True if the value is neither inf nor nan.
         */
        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool> IsNumber(T value) noexcept;


    } // namespace NumericNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Numeric/Numeric.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HPP_
