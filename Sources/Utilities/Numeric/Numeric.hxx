/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 11 Dec 2014 10:51:50 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HXX_
#define MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HXX_

// IWYU pragma: private, include "Utilities/Numeric/Numeric.hpp"

#include "Utilities/Miscellaneous.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"


namespace MoReFEM
{


    namespace NumericNS
    {


        template<class T>
        constexpr T UninitializedIndex() noexcept
        {
            if constexpr (std::is_integral<T>())
                return std::numeric_limits<T>::max();
            else // specialization of StrongType; needs to be more explicit...
                return T(std::numeric_limits<typename T::underlying_type>::max());
        }


        template<class T>
        constexpr T Square(T value) noexcept
        {
            static_assert(std::is_arithmetic<T>());
            return value * value;
        }


        template<class T>
        constexpr T Cube(T value) noexcept
        {
            return value * value * value;
        }


        template<class T>
        constexpr T PowerFour(T value) noexcept
        {
            return value * value * value * value;
        }


        template<class T>
        constexpr T AbsPlus(T value) noexcept
        {
            constexpr T zero = static_cast<T>(0);

            if (value < zero)
                return zero;
            else
                return value;
        }


        template<class T>
        constexpr T Sign(T value) noexcept
        {
            constexpr T zero = static_cast<T>(0);
            constexpr T minus_one = static_cast<T>(-1);
            constexpr T one = static_cast<T>(1);

            if (IsZero(value))
                return zero;
            else if (value < zero)
                return minus_one;
            else
                return one;
        }


        template<class T>
        constexpr T TrueSign(T value) noexcept
        {
            constexpr T zero = static_cast<T>(0);
            constexpr T minus_one = static_cast<T>(-1);
            constexpr T one = static_cast<T>(1);

            if (value < zero)
                return minus_one;
            else
                return one;
        }


        template<class T>
        constexpr T Heaviside(T value) noexcept
        {
            static_assert(std::is_floating_point<T>(), "Heaviside doesn't make any sense for integers.");

            constexpr T zero = static_cast<T>(0);
            constexpr T one_half = static_cast<T>(1. / 2.);
            constexpr T one = static_cast<T>(1.);

            if (value < zero)
                return zero;
            else if (IsZero(value))
                return one_half;
            else
                return one;
        }


        template<class T>
        constexpr std::enable_if_t<std::is_floating_point<T>::value, T> DefaultEpsilon() noexcept
        {
            return static_cast<T>(1.e-15);
        }


        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool> IsZero(T value, T epsilon) noexcept
        {
            return std::fabs(value) < epsilon;
        }


        template<class T>
        std::enable_if_t<std::is_integral<T>::value, bool> IsZero(T value) noexcept
        {
            return value == static_cast<T>(0);
        }


        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool> AreEqual(T lhs, T rhs, T epsilon) noexcept
        {
            return IsZero(lhs - rhs, epsilon);
        }


        template<class T>
        std::enable_if_t<std::is_floating_point<T>::value, bool> IsNumber(T value) noexcept
        {
            return (std::isnan(value) || std::isinf(value)) ? false : true;
        }


    } // namespace NumericNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_NUMERIC_x_NUMERIC_HXX_
