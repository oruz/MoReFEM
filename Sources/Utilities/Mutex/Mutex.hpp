/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 13:53:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HPP_
#define MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HPP_

#include <cassert>
#include <memory>
#include <mutex>


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief This CRTP class defines a mutex each object of the DerivedT class.
         *
         */
        template<class DerivedT>
        class Mutex
        {

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Mutex() = default;

            //! Destructor.
            ~Mutex() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Mutex(const Mutex& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Mutex(Mutex&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            Mutex& operator=(const Mutex& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Mutex& operator=(Mutex&& rhs) = delete;

            ///@}

            //! Get access to the mutex.
            //! \return Non constant reference to std::mutex underlying object.
            std::mutex& GetMutex() const;


          private:
            /*!
             * \brief Mutex object.
             *
             * This follows item 16 of Scott Meyers's "Effective Modern C++", which advises to make const member
             * functions thread safe.
             *
             * However, it is here enclosed in a unique_ptr as mutex doesn't seem to be movable (current implementations
             * of both clang and gcc don't define the move constructor and the move assignation; the copy is deleted).
             */
            mutable std::unique_ptr<std::mutex> mutex_ = std::make_unique<std::mutex>();
        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Mutex/Mutex.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HPP_
