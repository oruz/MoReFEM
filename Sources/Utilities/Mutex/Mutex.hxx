/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 13:53:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HXX_
#define MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HXX_

// IWYU pragma: private, include "Utilities/Mutex/Mutex.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT>
        inline std::mutex& Mutex<DerivedT>::GetMutex() const
        {
            assert(!(!mutex_));
            return *mutex_;
        }


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_MUTEX_x_MUTEX_HXX_
