/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 11:39:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_
#define MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_

// IWYU pragma: private, include "Utilities/String/String.hpp"

#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string_view>


namespace MoReFEM
{


    namespace Utilities
    {


        namespace String
        {


            inline void Strip(std::string& string, std::string_view char_to_strip)
            {
                StripLeft(string, char_to_strip);
                StripRight(string, char_to_strip);
            }


            inline bool StartsWith(std::string_view string, std::string_view sequence)
            {
                return 0 == string.compare(0, sequence.size(), sequence);
            }


            template<class StringT>
            std::string Repeat(const StringT& string, std::size_t times)
            {
                std::ostringstream oconv;

                for (std::size_t i = 0; i < times; ++i)
                    oconv << string;

                return oconv.str();
            }


        } // namespace String


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_
