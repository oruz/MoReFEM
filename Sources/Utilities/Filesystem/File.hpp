/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Jul 2013 14:26:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HPP_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HPP_

#include <fstream>
#include <iosfwd>
#include <string> // IWYU pragma: keep
#include <vector>


namespace MoReFEM
{


    namespace FilesystemNS
    {


        namespace File
        {


            /*! \class doxygen_hide_openmode
             *
             * \param[in] openmode Open mode given to the stream (for instance to specify you want binary output).
             * It follows the exact same mask syntax as what you would use directly in std::ofstream or std::ofstream.
             */


            /*! \class doxygen_hide_filesystem_thread_safe
             *
             * \attention This function is syntactic sugar for file or directory manipulation and has no pretension to
             * handle by itself thread safety.
             */


            /*!
             * \brief A wrapper over std::remove that throws an exception in case the operation fails.
             *
             * It also takes a std::string instead of a const char*.
             *
             * \param[in] filename Path to the file to remove.
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Remove(const std::string& filename, const char* invoking_file, int invoking_line);


            /*!
             * \brief A wrapper over ofstream::open that throws an exception in case the operation fails.
             *
             * \param[in] filename Name of the file to be created.
             * \param[out] out Stream associated to the file.
             * \copydoc doxygen_hide_openmode
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Create(std::ofstream& out,
                        const std::string& filename,
                        const char* invoking_file,
                        int invoking_line,
                        std::ios_base::openmode openmode = std::ios_base::out);


            /*!
             * \brief A wrapper over ifstream::open that throws an exception in case the operation fails.
             *
             * \param[in] filename Name of the file to be read.
             * \param[out] stream Stream associated to the file.
             * \copydoc doxygen_hide_invoking_file_and_line
             * \copydoc doxygen_hide_openmode
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Read(std::ifstream& stream,
                      const std::string& filename,
                      const char* invoking_file,
                      int invoking_line,
                      std::ios_base::openmode openmode = std::ios_base::in);


            /*!
             * \brief A wrapper over ifstream::open with std::ios::append that throws an exception in case the operation
             * fails.
             *
             * \param[in] filename Name of the file to be loaded and modified.
             * \param[out] stream Stream associated to the file.
             * \copydoc doxygen_hide_openmode
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Append(std::ofstream& stream,
                        const std::string& filename,
                        const char* invoking_file,
                        int invoking_line,
                        std::ios_base::openmode openmode = std::ios_base::out);


            /*!
             * \brief Check whether \a filename exists or not.
             *
             * \param[in] filename Path of the filename to check.
             *
             * \return True if it does exist.
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            bool DoExist(const std::string& filename);


            /*!
             * \brief Extract the extension from a filename.
             *
             * This is done crudely: an extension is all that is beyong the last '.' met in the filename. If none,
             * empty string is returned.
             *
             * \param[in] filename Filename from which extension is extracted.
             *
             * \return Extension (of empty string if none).
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            std::string Extension(const std::string& filename);


            //! Convenient enum class for copy.
            enum class autocopy
            {
                yes,
                no
            };

            //! Convenient enum class for copy.
            enum class fail_if_already_exist
            {
                yes,
                no
            };

            //! Copy

            /*!
             * \brief Copy a file.
             *
             * \param[in] source Path of the source file.
             * \param[in] target Path of the target file.
             * \param[in] do_fail_if_already_exist If yes, the target must not exist yet.
             * \param[in] allow_autocopy If no, an exception is thrown is source is the same path as target. If yes,
             * nothing is done. The reason for this is that I'm not sure underlying library Yuni can cope with this
             * case. It will be replaced by future STL filesystem library that should be shipped along with C++17.
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Copy(const std::string& source,
                      const std::string& target,
                      fail_if_already_exist do_fail_if_already_exist,
                      autocopy allow_autocopy,
                      const char* invoking_file,
                      int invoking_line);


            /*!
             * \brief Concatenate two files.
             *
             * \param[in] file_list List of files to concatenate.
             * \param[out] amalgated_file File into which the amalgated files are written.
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * All files must be valid (or in a valid path for output one); if not an exception is raised.
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            void Concatenate(const std::vector<std::string>& file_list,
                             const std::string& amalgated_file,
                             const char* invoking_file,
                             int invoking_line);


            /*!
             * \brief Check two files share exactly the same content.
             *
             * \copydoc doxygen_hide_lhs_rhs_arg
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * This function checks both files do exist first.
             *
             * This code is lifted from https://stackoverflow.com/questions/6163611/compare-two-files with
             * very small adaptation; http://www.cplusplus.com/forum/general/94032/ provides a Boost-dependent
             * implementation which is seemingly more efficient.
             *
             * Upcoming STL filesystem library doesn't seem to provide the functionality; currently it is used only
             * for a test with very small files involved. So if this is deemed to be used more often and on much bigger
             * file some profiling would be interesting here!
             *
             * \return True if the files share exactly the same content.
             *
             * \copydoc doxygen_hide_filesystem_thread_safe
             */
            bool
            AreEquals(const std::string& lhs, const std::string& rhs, const char* invoking_file, int invoking_line);


        } // namespace File


    } // namespace FilesystemNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HPP_
