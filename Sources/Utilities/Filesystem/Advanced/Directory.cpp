/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 14:58:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <filesystem>
#include <sstream>
#include <string>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"


namespace MoReFEM::Advanced::FilesystemNS::DirectoryNS
{


    bool DoExist(const std::string& folder)
    {
        // is_directory also checks it does exist.
        return std::filesystem::is_directory(std::filesystem::path(folder));
    }


    void Create(const std::string& folder, const char* invoking_file, int invoking_line)
    {
        std::filesystem::path folder_path(folder);

        if (DoExist(folder))
            throw Exception("Folder " + folder + " already exists!", invoking_file, invoking_line);

        std::filesystem::create_directories(folder_path);
    }


    void Remove(const std::string& folder, const char* invoking_file, int invoking_line)
    {
        if (!DoExist(folder))
            throw Exception("Folder " + folder + " doesn't exist!", invoking_file, invoking_line);

        std::filesystem::remove_all(std::filesystem::path(folder));
    }


} // namespace MoReFEM::Advanced::FilesystemNS::DirectoryNS


/// @} // addtogroup UtilitiesGroup
