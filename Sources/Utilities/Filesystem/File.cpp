/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Jul 2013 14:26:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <algorithm>
#include <array>
#include <cstddef> // IWYU pragma: keep
#include <cstdio>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM
{


    namespace FilesystemNS
    {


        namespace // anonymous
        {


            void CheckFilenameNotEmpty(const std::string& filename, const char* invoking_file, int invoking_line)
            {
                if (filename.empty())
                    throw Exception("Trying to operate on an empty string!", invoking_file, invoking_line);
            }


        } // namespace


        namespace File
        {


            void Remove(const std::string& filename, const char* invoking_file, int invoking_line)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);

                int err = std::remove(filename.c_str());

                if (err)
                    throw Exception("Unable to remove " + filename, invoking_file, invoking_line);
            }


            void Create(std::ofstream& out,
                        const std::string& filename,
                        const char* invoking_file,
                        int invoking_line,
                        std::ios_base::openmode openmode)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                out.open(filename, openmode);

                if (!out)
                    throw Exception("Unable to create file " + filename, invoking_file, invoking_line);
            }


            void Read(std::ifstream& in,
                      const std::string& filename,
                      const char* invoking_file,
                      int invoking_line,
                      std::ios_base::openmode openmode)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);
                in.open(filename, openmode);

                if (!in)
                    throw Exception("Unable to read file " + filename, invoking_file, invoking_line);
            }


            void Append(std::ofstream& inout,
                        const std::string& filename,
                        const char* invoking_file,
                        int invoking_line,
                        std::ios_base::openmode openmode)
            {
                CheckFilenameNotEmpty(filename, invoking_file, invoking_line);

                inout.open(filename, openmode | std::ios::app);

                if (!inout)
                    throw Exception("Unable to read/modify file \"" + filename + "\".", invoking_file, invoking_line);
            }


            bool DoExist(const std::string& filename)
            {
                // Not the most efficient; 'struct stat' is probably better.
                std::ifstream in;

                in.open(filename);

                if (!in)
                    return false;

                return true;
            }


            std::string Extension(const std::string& filename)
            {
                auto pos = filename.rfind('.');

                if (pos == std::string::npos)
                    return "";

                return filename.substr(pos + 1);
            }


            void Copy(const std::string& source,
                      const std::string& target,
                      fail_if_already_exist do_fail_if_already_exist,
                      autocopy allow_autocopy,
                      const char* invoking_file,
                      int invoking_line)
            {
                // Handle separately the case source and target are identical: I'm not positive Yuni handles it
                // properly.
                if (source == target)
                {
                    switch (allow_autocopy)
                    {
                    case autocopy::yes:
                        return;
                    case autocopy::no:
                        throw Exception(
                            "Trying to copy file \"" + source + "\" onto itself!", invoking_file, invoking_line);
                    }
                }

                const auto copy_option = do_fail_if_already_exist == fail_if_already_exist::yes
                                             ? std::filesystem::copy_options::none
                                             : std::filesystem::copy_options::overwrite_existing;

                std::filesystem::path source_path(source);
                std::filesystem::path target_path(target);

                std::filesystem::copy_file(source_path, target_path, copy_option);
            }


            void Concatenate(const std::vector<std::string>& file_list,
                             const std::string& amalgated_file,
                             const char* invoking_file,
                             int invoking_line)
            {
                std::ofstream out;

                Create(out, amalgated_file, invoking_file, invoking_line);

                std::ifstream in;
                std::string line;

                for (const auto& input_file : file_list)
                {
                    Read(in, input_file, invoking_file, invoking_line);

                    while (getline(in, line))
                        out << line << std::endl;

                    in.close();
                }
            }


            bool AreEquals(const std::string& lhs, const std::string& rhs, const char* invoking_file, int invoking_line)
            {
                std::ifstream lhs_stream, rhs_stream;

                // Load the stream, and check both exists.
                Read(lhs_stream, lhs, invoking_file, invoking_line);
                Read(rhs_stream, rhs, invoking_file, invoking_line);

                if (lhs == rhs) // if exact same file content is the same!
                    return true;

                std::ifstream::pos_type size1, size2;

                size1 = lhs_stream.seekg(0, std::ifstream::end).tellg();
                lhs_stream.seekg(0, std::ifstream::beg);

                size2 = rhs_stream.seekg(0, std::ifstream::end).tellg();
                rhs_stream.seekg(0, std::ifstream::beg);

                if (size1 != size2)
                    return false;

                constexpr auto block_size = 4096ul;
                auto remaining = static_cast<std::streamsize>(size1);

                while (remaining)
                {
                    std::array<char, block_size> buffer1, buffer2;

                    const auto size = std::min(static_cast<std::streamsize>(block_size), remaining);

                    lhs_stream.read(buffer1.data(), size);
                    rhs_stream.read(buffer2.data(), size);

                    if (0 != memcmp(buffer1.data(), buffer2.data(), static_cast<std::size_t>(size)))
                        return false;

                    remaining -= size;
                }

                return true;
            }


        } // namespace File


    } // namespace FilesystemNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
