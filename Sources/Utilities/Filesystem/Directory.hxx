//! \file
//
//
//  Directory.hxx
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_

// IWYU pragma: private, include "Utilities/Filesystem/Directory.hpp"

#include <cassert>
#include <optional>
#include <sstream>
#include <vector>

#include "Utilities/Containers/Print.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FilesystemNS
{


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         const StringT& subdirectory,
                         const char* invoking_file,
                         int invoking_line,
                         std::optional<behaviour> directory_behaviour)
    : mpi_(parent_directory.GetMpiPtr()),
      directory_behaviour_(directory_behaviour.has_value() ? directory_behaviour.value()
                                                           : parent_directory.GetBehaviour()),
      with_rank_(parent_directory.IsWithRank())
    {
        Internal::FilesystemNS::CheckForSubdirectoryConstructor(parent_directory, invoking_file, invoking_line);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath() << subdirectory << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         const std::vector<StringT>& subdirectories,
                         const char* invoking_file,
                         int invoking_line,
                         std::optional<behaviour> directory_behaviour)
    : mpi_(parent_directory.GetMpiPtr()),
      directory_behaviour_(directory_behaviour.has_value() ? directory_behaviour.value()
                                                           : parent_directory.GetBehaviour()),
      with_rank_(parent_directory.IsWithRank())
    {
        Internal::FilesystemNS::CheckForSubdirectoryConstructor(parent_directory, invoking_file, invoking_line);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath();
        Utilities::PrintContainer<>::Do(subdirectories,
                                        oconv,
                                        PrintNS::Delimiter::separator("/"),
                                        PrintNS::Delimiter::opener(""),
                                        PrintNS::Delimiter::closer(""));

        oconv << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    inline const std::string& Directory::GetPath() const noexcept
    {
        return path_;
    }


    inline const Wrappers::Mpi& Directory::GetMpi() const noexcept
    {
        assert(IsMpi());
        return *mpi_;
    }


    inline bool Directory::IsMpi() const noexcept
    {
        return mpi_ != nullptr;
    }


    inline const Wrappers::Mpi* Directory::GetMpiPtr() const noexcept
    {
        return mpi_; // might be nullptr
    }


    inline behaviour Directory::GetBehaviour() const noexcept
    {
        return directory_behaviour_;
    }


    inline Directory::operator const std::string&() const noexcept
    {
        return GetPath();
    }


    inline bool Directory::IsWithRank() const noexcept
    {
        return with_rank_;
    }


} // namespace MoReFEM::FilesystemNS


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
