//! \file
//
//
//  Directory.hpp
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HPP_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <optional>
#include <string_view>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"             // IWYU pragma: export
#include "Utilities/Exceptions/Exception.hpp"          // IWYU pragma: export
#include "Utilities/Filesystem/Advanced/Directory.hpp" // IWYU pragma: export


namespace MoReFEM::FilesystemNS
{


    /*!
     * \class doxygen_hide_directory_behaviour_desc
     *
     * <ul>
     * <li> overwrite: Remove the pre-existing one and recreate it.</li>
     * <li> quit: Quit the program if the directory exists.</li>
     * <li> read: this mode expects the directory to exist and throws an exception otherwise.</li>
     * <li> create: create a new directory - it is expected here it doesn't exist yet (and an exception is thrown if it
     * does). </li>
     * <li> create_if_necessary: create the directory if it doesn't exist yet
     * <li> ask: Ask the end user if he wants to override or not. If he chooses not to do so, the program ends.
     * Mpi is properly handled (the interface is properly rerouted to root processor which is the sole able to
     * communicate with stdin).</li>
     * <li> ignore: Do nothing - whether the directory exist or not. This might be useful if you want to set the
     * behaviour later; I would nonetheless not recommend using this possibility much as we lose part of the appeal of
     * using a class such as \a Directory doing so.</li>
     * </ul>
     */


    /*!
     * \brief Enum class to determine how to handle the case
     *
     * \copydoc doxygen_hide_directory_behaviour_desc
     */
    enum class behaviour
    {
        overwrite,
        ask,
        quit,
        read,
        create,
        create_if_necessary,
        ignore
    };

    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, behaviour rhs);


    /*!
     * \brief Whether a subdirectory with 'Rank_' + * rank id *  must be added to the directory or not.
     *
     * This should be the standard case for output directories, but it is obviously not relevant for most of the
     * input ones...
     */
    enum class add_rank
    {
        no,
        yes
    };


    /*!
     * \class doxygen_hide_directory_behaviour_param
     *
     * \param[in] directory_behaviour Behaviour of the directory, among:
     *
     * \copydoc doxygen_hide_directory_behaviour_desc
     */


    /*!
     * \brief Class to manage directories and ensure they behave as expected (check path already exists or if not
     * whether it should create it or not, etc...)
     *
     */
    class Directory
    {

      public:
        //! Alias to self.
        using self = Directory;

        //! Smart pointer to hold it.
        using unique_ptr = std::unique_ptr<self>;

        //! Smart pointer to hold it.
        using const_unique_ptr = std::unique_ptr<const self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         * \copydoc doxygen_hide_mpi_param
         * \copydoc doxygen_hide_directory_behaviour_param
         * \param[in] path Unix path of the directory.
         * \param[in] do_add_rank Whether a "Rank_" part is added to the path.
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        explicit Directory(const Wrappers::Mpi& mpi,
                           const std::string& path,
                           behaviour directory_behaviour,
                           const char* invoking_file,
                           int invoking_line,
                           add_rank do_add_rank = add_rank::yes);


        /*!
         * \brief Simple constructor: no mpi involved - so no rank whatsoever added.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         * \copydoc doxygen_hide_directory_behaviour_param
         * \param[in] path Unix path of the directory.
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        explicit Directory(const std::string& path,
                           behaviour directory_behaviour,
                           const char* invoking_file,
                           int invoking_line);


        /*!
         * \class doxygen_hide_subdirectory_constructor
         *
         * \param[in] parent_directory The directory into which the new one is created (or read - depends on behaviour).
         * The constructed directory takes the same attributes than its parent directory - except obviously for the
         * path. \copydoc doxygen_hide_invoking_file_and_line \param[in] directory_behaviour Directory behaviour; if \a
         * nullopt the behaviour of the parent directory is used (you should stick with this choice most of the time),
         *
         * \tparam StringT Type of \a subdirectory, which may actually be anything for which operator<< has been
         * overloaded (avoid nonetheless values with spaces inside...)
         */

        /*!
         * \brief Constructor of a \a Directory which is a subdirectory on an already existing \a parent_directory.
         *
         * \param[in] subdirectory Name of the subdirectory relative to \a parent_directory.
         * \copydetails doxygen_hide_subdirectory_constructor
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        template<class StringT>
        explicit Directory(const Directory& parent_directory,
                           const StringT& subdirectory,
                           const char* invoking_file,
                           int invoking_line,
                           std::optional<behaviour> directory_behaviour = std::nullopt);

        /*!
         * \brief Constructor of a \a Directory which is several layers of directories inside an already existing
         * \a parent_directory.
         *
         * \param[in] layers Names of the layers of subdirectories. For instance if { "Foo", "Bar"}, it will create
         * inside \a parent_directory the subdirectory "Foo/Bar/".
         * \copydetails doxygen_hide_subdirectory_constructor
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        template<class StringT>
        explicit Directory(const Directory& parent_directory,
                           const std::vector<StringT>& layers,
                           const char* invoking_file,
                           int invoking_line,
                           std::optional<behaviour> directory_behaviour = std::nullopt);

        //! Destructor.
        ~Directory() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Directory(const Directory& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Directory(Directory&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Directory& operator=(const Directory& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Directory& operator=(Directory&& rhs) = delete;

        ///@}

        //! Get the underlying Unix path.
        const std::string& GetPath() const noexcept;

        //! Conversion operator which does the same as \a GetPath().
        operator const std::string&() const noexcept;

        /*!
         * \brief Define path to a file in the directory.
         *
         * \param[in] filename Name of the file.
         *
         * \return Full path of the file.
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        std::string AddFile(std::string_view filename) const;

        //! Returns the behaviour.
        behaviour GetBehaviour() const noexcept;

        /*!
         * \brief Changes the behaviour
         *
         * \param[in] new_behaviour As written on the tag...
         *
         * \attention You should be cautious before using this one; this was introduced to soften the behaviour (i.e.
         * overwrite -> read) for some tests which create first directories and then use some post-processing on them.
         */
        void SetBehaviour(behaviour new_behaviour);

        //! Accessor to Mpi object (if relevant).
        const Wrappers::Mpi& GetMpi() const noexcept;

        //! Whether \a mpi is considered or not.
        bool IsMpi() const noexcept;

        //! Whether a rank was given in construction.
        bool IsWithRank() const noexcept;

      public:
        /*!
         * \brief Modify current directory so that it points to a subdirectory.
         *
         * \attention It is NOT the preferred way to create a subdirectory: it is much better to create another
         * \a Directory object with the proper constructor to do so. However, it is handy as it cut short the code
         * for testing models outputs and comparing them to the stored expected results.
         *
         * \param[in] subdirectory Name of the subdirectory layer that will hence be pointed by current object.
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        void AddSubdirectory(std::string_view subdirectory, const char* invoking_file, int invoking_line);

      private:
        /*!
         * \brief Helper method used in both constructors.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        void Construct(const char* invoking_file, int invoking_line) const;

        /*!
         * \brief Helper method for the 'ask' behaviour.
         *
         * This behaviour requires inter-processor communication: each rank must tell root processor whether the
         * directory already exist or not, and if some do root processor must ask the user what to do. Then
         * it musts transmit back answers to each rank, which then must take action.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         */

        void CollectAnswer(const char* invoking_file, int invoking_line) const;

        //! Accessor to Mpi pointer - required to define some constructors.
        const Wrappers::Mpi* GetMpiPtr() const noexcept;

      private:
        //! Path of the directory.
        std::string path_;

        //! \a Mpi object.
        const Wrappers::Mpi* const mpi_;

        /*!
         * \brief What to do at the construction of the \a Directory object.
         *
         * Options are:
         *
         * \copydoc doxygen_hide_directory_behaviour_desc
         */
        behaviour directory_behaviour_;

        //! Whether the rank was given in construction.
        const bool with_rank_;
    };


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const Directory& rhs);


    /*!
     * \brief Create a \a Directory which contains path related to \a rank - where rank is NOT necessarily the one
     * held by mpi object.
     *
     * \attention Should only be used on mpi root processor!
     *
     * This is only for some specific functions and should be used with great care; behaviour is set to 'read'
     * and should be changed with even greater care...
     *
     * \param[in] root_directory The directory matching the root rank.
     * \param[in] rank Rank for which the new directory is required.
     * \copydoc doxygen_hide_invoking_file_and_line
     *
     * \return The directory related to the chosen \a rank argument. It is created with the 'read' behaviour - and
     * this one should changed with great care! (or better left unchanged).
     */
    Directory
    GetRankDirectory(const Directory& root_directory, std::size_t rank, const char* invoking_file, int invoking_line);


} // namespace MoReFEM::FilesystemNS


namespace MoReFEM::Internal::FilesystemNS
{


    /*!
     * \brief Helper function for subdirectory constructors.
     *
     * \param[in] parent_directory The directory from which a subdirectory is to be constructed.
     * \copydoc doxygen_hide_invoking_file_and_line
     */
    void CheckForSubdirectoryConstructor(const ::MoReFEM::FilesystemNS::Directory& parent_directory,
                                         const char* invoking_file,
                                         int invoking_line);


} // namespace MoReFEM::Internal::FilesystemNS


#include "Utilities/Filesystem/Directory.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HPP_
