//! \file
//
//
//  Directory.hpp
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_GET_RANK_DIRECTORY_HPP_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_GET_RANK_DIRECTORY_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Filesystem/Directory.hpp"


namespace MoReFEM::Internal::FilesystemNS
{


    /*!
     * \brief Create a \a Directory which contains path related to \a rank - where rank is NOT necessarily the one
     * held by mpi object.
     *
     * \attention Should only be used on mpi root processor!
     *
     * This is only for some specific functions and should be used with great care; behaviour is set to 'read'
     * and should be changed with even greater care...
     *
     * \param[in] root_directory The directory matching the root rank.
     * \param[in] rank Rank for which the new directory is required.
     * \copydoc doxygen_hide_invoking_file_and_line
     *
     * \return The directory related to the chosen \a rank argument. It is created with the 'read' behaviour - and
     * this one should changed with great care! (or better left unchanged).
     */
    ::MoReFEM::FilesystemNS::Directory GetRankDirectory(const ::MoReFEM::FilesystemNS::Directory& root_directory,
                                                        std::size_t rank,
                                                        const char* invoking_file,
                                                        int invoking_line);


} // namespace MoReFEM::Internal::FilesystemNS


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_GET_RANK_DIRECTORY_HPP_
