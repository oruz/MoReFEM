//! \file
//
//
//  LuaDumpStack.hpp
//  MoReFEM
//
//  Created by sebastien on 26/06/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HXX_
#define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HXX_

// IWYU pragma: private, include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hpp"


#include <cassert>
#include <cstdlib>
#include <iostream>
#include <tuple>
#include <type_traits>

#include "Utilities/Miscellaneous.hpp"
#include "Utilities/Type/PrintTypeName.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    template<typename T>
    void PushOnStack(lua_State* state, T value)
    {
        if constexpr (Utilities::IsSpecializationOf<std::tuple, T>())
        {
            // See https://stackoverflow.com/questions/16387354/template-tuple-calling-a-function-on-each-element.
            std::apply(
                [state](auto... x)
                {
                    (..., PushOnStack(state, x));
                },
                value);
        } else
        {
            if constexpr (std::is_same<T, bool>())
                lua_pushboolean(state, value);
            else if constexpr (std::is_same<T, std::string>())
                lua_pushstring(state, value.c_str());
            else if constexpr (std::is_integral<T>())
                lua_pushinteger(state, value);
            else if constexpr (std::is_floating_point<T>())
                lua_pushnumber(state, value);
            else
            {
                std::cerr << "PushingOnStack type " << GetTypeName<T>() << " was not foreseen..." << std::endl;
                assert(false);
                exit(EXIT_FAILURE);
            }
        }
    }


    template<typename T>
    T PullFromStack(lua_State* state, int index)
    {
        if constexpr (std::is_same<T, bool>())
            return lua_toboolean(state, index);
        else if constexpr (std::is_same<T, std::string>())
            return lua_tostring(state, index);
        else if constexpr (std::is_integral<T>())
            return static_cast<T>(lua_tointeger(state, index));
        else if constexpr (std::is_floating_point<T>())
            return static_cast<T>(lua_tonumber(state, index));
        else
        {
            std::cerr << "PullFromStack type " << GetTypeName<T>() << " was not foreseen..." << std::endl;
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::LuaNS


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HXX_
