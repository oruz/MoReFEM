//! \file
//
//
//  LuaDumpStack.cpp
//  MoReFEM
//
//  Created by sebastien on 26/06/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
#include <sstream>
#include <string>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    namespace // anonymous
    {


        /*!
         * \brief Sift through the sections/subsections and so forth to put them on Lua stack.
         *
         *
         * \param[in] full_name The full name with all the layers (e.g. VolumicMass.nature)
         * \param[in] entry_name Name of an entry that is accessible from the entry
         currently on top of the stack (e.g. nature).
         */
        void WalkDown(const std::string& full_name, const std::string& entry_name, lua_State* state);


    } // namespace


    void LuaStackDump(lua_State* state, std::ostream& stream)
    {
        stream << "Content of Lua stack is: " << std::endl;

        int i;
        int top = lua_gettop(state);

        for (i = 1; i <= top; i++) /* repeat for each level */
        {
            int t = lua_type(state, i);

            stream << "\t- ";

            switch (t)
            {
            case LUA_TSTRING: /* strings */
                stream << lua_tostring(state, i) << std::endl;
                break;

            case LUA_TBOOLEAN: /* booleans */
                stream << (lua_toboolean(state, i) ? "true" : "false") << std::endl;
                break;

            case LUA_TNUMBER: /* numbers */
                stream << lua_tonumber(state, i) << std::endl;
                break;

            default: /* other values */
                stream << lua_typename(state, t) << std::endl;
                break;
            }
        }
    }


    void PutOnStack(lua_State* state, const std::string& name)
    {
        assert(!name.empty()); // \todo Note: was handled in Ops but very likely not relevant for us.

        const auto end = name.find(".");
        assert(end != 0ul);

        // No section involved: the parameter was directly defined in root level.
        if (end == std::string::npos)
        {
            lua_getglobal(state, name.c_str());
        } else
        {
            lua_getglobal(state, name.substr(0, end).c_str());

            assert(end < name.size());
            assert(name[end] == '.');
            WalkDown(name, name.substr(end + 1).c_str(), state);
        }
    }


    namespace // anonymous
    {


        void WalkDown(const std::string& full_name, const std::string& name, lua_State* state)
        {
            assert(!name.empty());

            if (lua_isnil(state, -1))
            {
                std::ostringstream oconv;
                oconv << "Invalid Lua file: unable to find field '" << full_name << "'.";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            // The sub-entries are introduced with ".".
            const auto end = name.find(".");

            if (end == std::string::npos)
            {
                lua_pushstring(state, name.c_str());
                lua_gettable(state, -2);
            } else
            {
                assert(end < name.size());
                assert(name[end] == '.');
                // One step down.
                {
                    lua_pushstring(state, name.substr(0, end).c_str());
                    lua_gettable(state, -2);
                    WalkDown(name, name.substr(end + 1).c_str(), state);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::LuaNS
