/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Mar 2018 15:22:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <cassert>
#include <cctype> // IWYU pragma: keep
// IWYU pragma: no_include <_ctype.h>
// IWYU pragma: no_include <__tree>
#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        //! Defines 'value_in' for the user. It checks whether an element is in a table.
        void AddValueIn(lua_State* lua_state);


        //! Structure which holds the name of the data or section found and the position of the companion equal sign.
        struct EntryOrSection
        {
            EntryOrSection(const std::string& name, std::size_t position);

            std::string name;

            std::size_t equal_position;
        };


        struct Section
        {

            std::string name;

            std::size_t min;

            std::size_t max;
        };


        /*!
         * \brief Parse the file to extract all the keys in it.
         *
         * \internal Lua is not involved at all here; it is done in pure C++ by parsing manually the file.
         * \endinternal
         *
         * The point of this class is to compute all the keys found in the input file.
         */
        class ExtractKeysFromFile
        {
          public:
            //! Constructor.
            //! \param[in] file Path to the input Lua file.
            ExtractKeysFromFile(const std::string& path);


            std::vector<std::string> GetEntryKeyList() const;

          private:
            //! Interpret the file as a big string; comments are excluded from it.
            //! Reason fo this is that by former implementation (prior to #1468) didn't do that this way and I ended
            //! up with side effects depending whether a brace ended on the same line as another or not.
            void FileAsString();

            //! Compute the positions of the relevant '{', '}' and '='.
            //! At this point map are correctly filtered out but not vectors (it is done at a further step).
            void ComputeDelimiterPositions();

            //! Extract the names of the data and the section (not knowing yet which is which).
            void ComputeEntryOrSectionNames();

            //! Pair braces together (identify which close the block opened by another).
            std::vector<std::pair<std::size_t, std::size_t>> PairBraces();

            //! Identify the sections (so far we had a list of names which might have been sections or entries).
            void IdentifySections(const std::vector<std::pair<std::size_t, std::size_t>>& pair_braces_list);

          private:
            const std::string& file_;

            std::string content_;

            // Convention: we do not count equals which last seen character was a ']'
            // Because '=' in maps (such as { [1] = 0., [3] = 14.5, [4] = -31.231 }} ) shouldn't be considered to
            // determine keywords.
            std::vector<std::size_t> equal_position_list_;

            // convention: '{' positions are stored as positive values.
            // '}' positions are stored as negative values..
            std::vector<int> brace_position_list_;

            std::vector<EntryOrSection> entry_or_section_list_;

            std::vector<Section> section_list_;
        };


        struct ReadNameBackward
        {

            ReadNameBackward(const std::string& content, std::size_t pos);

            //! Check whether the character read is either a letter, a underscore and possibly a letter (only if
            //! letter_already_read_ is false for the latter)
            bool Test(const char c);

            std::string name;
            bool letter_already_read_ = false;
        };


        //! Check the list of keys from the option file doesn't include any redundancy (and if so throw an exception).
        void ThrowIfRedundancy(const std::string& filename, const std::vector<std::string>& entry_key_list);


    } // namespace


    LuaOptionFile ::LuaOptionFile(const std::string& filename, const char* invoking_file, int invoking_line)
    : state_(luaL_newstate()), filename_(filename)
    {
        decltype(auto) state = GetNonCstLuaState();
        luaL_openlibs(state);

        AddValueIn(state);

        // Clean-up if an exception is thrown.
        try
        {
            Open(filename, invoking_file, invoking_line);
        }
        catch (...)
        {
            lua_close(state_);
            state_ = nullptr;
            throw;
        }
    }


    LuaOptionFile::~LuaOptionFile()
    {
        lua_close(state_);
        state_ = nullptr;
    }


    void LuaOptionFile::Open(const std::string& filename, const char* invoking_file, int invoking_line)
    {
        ExtractKeysFromFile entry_keys_list(filename);
        SetEntryKeyList(entry_keys_list.GetEntryKeyList());

        if (luaL_dofile(state_, filename.c_str()))
        {
            std::ostringstream oconv;
            oconv << "Open(string, bool) " << lua_tostring(state_, -1);
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


    void LuaOptionFile::ClearStack()
    {
        lua_pop(state_, lua_gettop(state_));
    }


    bool LuaOptionFile::IsKey(std::string_view entry_name) const
    {
        decltype(auto) list = GetEntryKeyList();

        const auto end = list.cend();

        return std::find(list.cbegin(), end, entry_name) == end;
    }


    void LuaOptionFile::SetEntryKeyList(std::vector<std::string>&& entry_key_list)
    {
        assert(entry_key_list_.empty() && "Should only be called once.");

        ThrowIfRedundancy(GetFilename(), entry_key_list);
        entry_key_list_ = std::move(entry_key_list);
    }


    namespace // anonymous
    {


        void AddValueIn(lua_State* lua_state)
        {
            std::string code = "function value_in(v, table)\
            for _, value in ipairs(table) do        \
            if v == value then                  \
            return true                     \
            end                                 \
            end                                     \
            return false                            \
            end";

            if (luaL_dostring(lua_state, code.c_str()))
                throw Exception(lua_tostring(lua_state, -1), __FILE__, __LINE__);
        }


        bool ReadNameBackward::Test(const char c)
        {
            if (c == '_')
                return true;

            if (letter_already_read_)
                return isalpha(c);
            else
            {
                if (isalpha(c))
                {
                    letter_already_read_ = true;
                    return true;
                }
                return isalnum(c);
            }
        }


        ReadNameBackward::ReadNameBackward(const std::string& content, std::size_t pos)
        {
            while (Test(content[pos]) && pos > 0)
            {
                name.push_back(content[pos]);
                --pos;
            }

            if (pos == 0)
                name.push_back(content[pos]);

            std::reverse(name.begin(), name.end());
        }


        ExtractKeysFromFile::ExtractKeysFromFile(const std::string& file) : file_(file)
        {
            FileAsString();

            ComputeDelimiterPositions();

            ComputeEntryOrSectionNames();

            auto pair_list = PairBraces();

            IdentifySections(pair_list);
        }


        void ExtractKeysFromFile::FileAsString()
        {
            std::ifstream stream;
            FilesystemNS::File::Read(stream, file_, __FILE__, __LINE__);

            constexpr auto npos = std::string::npos;

            std::ostringstream oconv;

            std::string line;
            while (getline(stream, line))
            {
                auto pos = line.find("--");

                if (pos != npos)
                    line.erase(pos, npos);

                oconv << line;
            }

            content_ = oconv.str();
        }


        EntryOrSection::EntryOrSection(const std::string& a_name, std::size_t a_position)
        : name(a_name), equal_position(a_position)
        { }


        void ExtractKeysFromFile::ComputeDelimiterPositions()
        {
            // Using a copy to avoid ambiguities with <= and >= symbols.
            auto copy = content_;
            Utilities::String::Replace("<=", "LE", copy);
            Utilities::String::Replace(">=", "GE", copy);

            const auto size = copy.size();

            bool is_last_non_space_character_close_bracket = false;

            int inside_map = 0;

            for (auto i = 0ul; i < size; ++i)
            {
                char current_char = copy[i];

                // We want to avoid taking in the process symbols <= and >=
                if (current_char == '=')
                {
                    if (!is_last_non_space_character_close_bracket)
                        equal_position_list_.push_back(i);
                    else
                        ++inside_map;

                    if (inside_map == 1)
                        brace_position_list_.pop_back(); // cancel the last opening brace.
                } else if (current_char == '{')
                    brace_position_list_.push_back(static_cast<int>(i));
                else if (current_char == '}')
                {
                    if (inside_map)
                        inside_map = 0;
                    else
                        brace_position_list_.push_back(-static_cast<int>(i));
                }
                if (current_char == ']')
                    is_last_non_space_character_close_bracket = true;
                else if (current_char == ' ')
                    ;
                else
                    is_last_non_space_character_close_bracket = false;
            }
        }


        void ExtractKeysFromFile::ComputeEntryOrSectionNames()
        {
            // For each equal, grab the name beforehand (sections will be reconstructed later).

            for (auto equal_pos : equal_position_list_)
            {
                auto pos = equal_pos;

                while (content_[pos] == ' ' && pos > 0)
                    --pos;

                if (pos == 0)
                {
                    std::ostringstream oconv;
                    oconv << "Unable to find name before the first equal sign in file " << file_;
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }


                // Keep doing it while reconstruct the name backwards.
                while (!std::isalnum(content_[pos]) && pos > 0)
                    --pos;

                ReadNameBackward read_name_backward(content_, pos);

                auto& name = read_name_backward.name;

                if (name.empty())
                {
                    std::ostringstream oconv;
                    oconv << "Unable to figure out a variable name in file " << file_;
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }


                entry_or_section_list_.emplace_back(EntryOrSection(name, equal_pos));
            }

            assert(entry_or_section_list_.size() == equal_position_list_.size());
        }


        std::vector<std::pair<std::size_t, std::size_t>> ExtractKeysFromFile::PairBraces()
        {
            if (!brace_position_list_.empty()) // the emptiness case might happen for VERY simple cases...
            {
                if (brace_position_list_.front() < 0)
                {
                    std::ostringstream oconv;
                    oconv << "First brace found in the file " << file_ << " is a closing one!";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                if (brace_position_list_.back() > 0)
                {
                    std::ostringstream oconv;
                    oconv << "Last brace found in the file " << file_ << " is an opening one!";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }

            // Check there are an equal number of opening and closing braces.
            auto opening_brace_lbd = [](auto i)
            {
                return i > 0;
            };

            auto closing_brace_lbd = [](auto i)
            {
                return i < 0;
            };

            const auto Nopening_brace =
                std::count_if(brace_position_list_.cbegin(), brace_position_list_.cend(), opening_brace_lbd);

            const auto Nclosing_brace =
                std::count_if(brace_position_list_.cbegin(), brace_position_list_.cend(), closing_brace_lbd);

            if (Nopening_brace != Nclosing_brace)
            {
                std::ostringstream oconv;
                oconv << "Unbalanced braces in the file " << file_ << ": there is " << Nopening_brace << " '{'"
                      << " and " << Nclosing_brace << " '}' [the ones related to maps or in comments aren't counted].";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }


            // Pair the braces together.
            std::vector<std::pair<std::size_t, std::size_t>> pair_list;

            auto copy = brace_position_list_;

            while (!copy.empty())
            {
                if (copy.front() < 0)
                {
                    std::ostringstream oconv;
                    oconv << "Unbalanced braces in file " << file_;
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                // Find first negative value.
                auto it = std::find_if(copy.cbegin(), copy.cend(), closing_brace_lbd);

                assert(it != copy.cend());
                assert(it != copy.cbegin());

                pair_list.emplace_back(
                    std::make_pair(static_cast<std::size_t>(*(it - 1)), static_cast<std::size_t>(-*it)));

                copy.erase(it);
                --it;
                copy.erase(it);
            }

            return pair_list;
        }

        void
        ExtractKeysFromFile ::IdentifySections(const std::vector<std::pair<std::size_t, std::size_t>>& pair_braces_list)
        {
            std::vector<std::pair<std::size_t, std::size_t>> filtered_pair_list;
            assert(entry_or_section_list_.size() >= pair_braces_list.size());

            for (const auto& pair : pair_braces_list)
            {
                const auto opening = pair.first;
                const auto closing = pair.second;

                for (const auto equal_pos : equal_position_list_)
                {
                    if (equal_pos > opening && equal_pos < closing)
                    {
                        filtered_pair_list.push_back(pair);
                        break;
                    }
                }
            }

            // Each brace pair is associated to the closer equal before it.
            const auto Npair = filtered_pair_list.size();

            for (auto i = 0ul; i < Npair; ++i)
            {
                const auto opening_brace = filtered_pair_list[i].first;

                // Using standard iterators instead of reverse ones as I want to use erase and conversion with
                // normal iterators is shall we say headache prone...
                auto it = entry_or_section_list_.end();
                --it;
                auto end = entry_or_section_list_.begin();
                --end;

                for (; it != end; --it)
                {
                    if (it->equal_position < opening_brace)
                        break;
                }

                assert(it != end);

                Section section;

                section.min = opening_brace;
                section.max = filtered_pair_list[i].second;
                section.name = it->name;

                section_list_.push_back(section);
                entry_or_section_list_.erase(it);
            }

            std::sort(section_list_.begin(),
                      section_list_.end(),
                      [](const auto& lhs, const auto& rhs)
                      {
                          return lhs.min < rhs.min;
                      });
        }


        std::vector<std::string> ExtractKeysFromFile::GetEntryKeyList() const
        {
            std::vector<std::string> ret;

            for (const auto& name : entry_or_section_list_)
            {
                std::ostringstream oconv;

                for (const auto& section : section_list_)
                {
                    if (name.equal_position > section.min && name.equal_position < section.max)
                        oconv << section.name << '.';
                }

                oconv << name.name;
                ret.push_back(oconv.str());
            }

            return ret;
        }


        void ThrowIfRedundancy(const std::string& filename, const std::vector<std::string>& entry_key_list)
        {
            std::set<std::string> check_unique_key_helper(entry_key_list.cbegin(), entry_key_list.cend());

            if (entry_key_list.size() != check_unique_key_helper.size())
            {
                std::ostringstream oconv;

                const auto begin = entry_key_list.cbegin();
                const auto end = entry_key_list.cend();

                std::vector<std::string> duplicate_list;

                for (const auto& item : check_unique_key_helper)
                {
                    if (std::count(begin, end, item) > 1)
                        duplicate_list.push_back(item);
                }

                oconv << "Error while interpreting Lua file " << filename << ": following entries were duplicated:\n";
                Utilities::PrintContainer<>::Do(duplicate_list,
                                                oconv,
                                                PrintNS::Delimiter::separator("\n\t- "),
                                                PrintNS::Delimiter::opener("\t- "),
                                                PrintNS::Delimiter::closer("\n"));

                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
