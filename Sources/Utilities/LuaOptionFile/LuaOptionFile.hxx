/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Mar 2018 15:22:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_
#define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_

// IWYU pragma: private, include "Utilities/LuaOptionFile/LuaOptionFile.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
// IWYU pragma: no_include <__nullptr>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

#include "Utilities/InputData/LuaFunction.hpp"
#include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hpp"
#include "Utilities/Type/PrintTypeName.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LuaOptionFile; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace // anonymous
    {


        constexpr auto most_recent_in_stack = -1;


    } // namespace


    inline lua_State* LuaOptionFile::GetNonCstLuaState() noexcept
    {
        assert(!(!state_));
        return state_;
    }


    template<class T, bool TolerateMissingFieldT, class VariantSelectorT>
    bool LuaOptionFile::Read(const std::string& entry_name,
                             const std::string& constraint,
                             T& ret,
                             const char* invoking_file,
                             int invoking_line,
                             VariantSelectorT&& variant_selector)
    {
        Internal::LuaNS::PutOnStack(state_, entry_name);

        bool was_properly_read{ false };

        try
        {
            if (lua_isnil(state_, most_recent_in_stack))
            {
                decltype(auto) filename = GetFilename();
                std::ostringstream oconv;
                oconv << "Couldn't read entry '" << entry_name << "' in file '" << filename << "'.";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            if constexpr (Utilities::IsSpecializationOf<std::vector, T>())
                ReadVector(entry_name, constraint, invoking_file, invoking_line, ret, std::move(variant_selector));
            else if constexpr (Utilities::IsSpecializationOf<std::map, T>())
                ReadMap(entry_name, constraint, invoking_file, invoking_line, ret, std::move(variant_selector));
            else if constexpr (std::is_same_v<std::nullptr_t, T>)
                ret = nullptr;
            else
            {
                Convert(most_recent_in_stack, entry_name, ret, invoking_file, invoking_line, variant_selector);
                CheckConstraint(entry_name, constraint, invoking_file, invoking_line);
            }

            was_properly_read = true;
        }
        catch (const Exception&)
        {
            if (!TolerateMissingFieldT)
                throw;
        }

        ClearStack();

        return was_properly_read;
    }


    template<class T, class StringT, class VariantSelectorT>
    void LuaOptionFile::Convert(int index_in_lua_stack,
                                StringT&& entry_name,
                                T& ret,
                                const char* invoking_file,
                                int invoking_line,
                                const VariantSelectorT& variant_selector)
    {
        if constexpr (Utilities::IsSpecializationOf<std::variant, T>())
        {
            const auto lbd = [this, index_in_lua_stack, entry_name, invoking_file, invoking_line](auto& value) -> T
            {
                using type = std::decay_t<decltype(value)>;

                type selected_value;

                this->Convert<type>(
                    index_in_lua_stack, entry_name, selected_value, invoking_file, invoking_line, nullptr);

                return selected_value;
            };

            ret = variant_selector;
            ret = std::visit(lbd, ret);
        } else if constexpr (std::is_same<T, bool>())
        {
            if (!lua_isboolean(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name
                                    + " is not a bool. Error is probably in the Read<>() type given in "
                                      "the call.",
                                invoking_file,
                                invoking_line);

            ret = static_cast<bool>(lua_toboolean(state_, index_in_lua_stack));
        } else if constexpr (std::is_same<T, std::string>())
        {
            if (!lua_isstring(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name
                                    + " is not a string. Error is probably in the Read<>() type given in "
                                      "the call.",
                                invoking_file,
                                invoking_line);

            ret = static_cast<std::string>(lua_tostring(state_, index_in_lua_stack));
        } else if constexpr (std::is_integral<T>())
        {
            if (!lua_isinteger(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name
                      << " is not an integer. Error is probably in the Read<>() type given "
                         "in the call.";

                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            ret = static_cast<T>(lua_tointeger(state_, index_in_lua_stack));
        } else if constexpr (std::is_floating_point<T>())
        {
            if (!lua_isnumber(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name
                      << " is not a floating point. Error is probably in the Read<>() type "
                         "given in the call.";

                throw Exception(oconv.str(), invoking_file, invoking_line);
            }

            ret = static_cast<T>(lua_tonumber(state_, index_in_lua_stack));
        } else if constexpr (IsStrongType<T>())
        {
            typename T::underlying_type tmp;

            this->Convert<typename T::underlying_type>(
                index_in_lua_stack, entry_name, tmp, invoking_file, invoking_line, nullptr);
            ret = T(tmp);
        } else if constexpr (std::is_same<T, std::nullptr_t>()) // might happen to ignore a field for instance.
            ret = nullptr;
        else if constexpr (Utilities::IsSpecializationOf<Utilities::InputDataNS::LuaFunction, T>())
        {
            std::string function;

            this->Convert<std::string>(index_in_lua_stack, entry_name, function, invoking_file, invoking_line, nullptr);
            ret = T(function);
        } else if constexpr (Utilities::IsSpecializationOf<std::map, T>())
        {
            ReadMap(entry_name,
                    "", // no constraint checked when called from a variant (at least so far)
                    invoking_file,
                    invoking_line,
                    ret,
                    variant_selector);
        } else
        {
            static_cast<void>(index_in_lua_stack);
            static_cast<void>(ret);
            static_cast<void>(invoking_file);
            static_cast<void>(invoking_line);

            std::cerr << "Conversion into " << GetTypeName<T>() << " was not foreseen..." << std::endl;
            assert(false && "Conversion not foreseen...");
            exit(EXIT_FAILURE);
        }
    }


    template<class StringT>
    void LuaOptionFile::CheckConstraint(StringT&& name,
                                        const std::string& constraint,
                                        const char* invoking_file,
                                        int invoking_line)
    {
        if (constraint == "")
            return;

        std::ostringstream oconv;
        oconv << "function lua_opt_file_check_constraint(v)\nreturn " << constraint
              << "\nend\nlua_opt_file_result = lua_opt_file_check_constraint(" << name << ')';

        if (luaL_dostring(state_, oconv.str().c_str()))
        {
            oconv.str("");
            oconv << "Error in CheckConstraint while checking " << name << ":\n  "
                  << lua_tostring(state_, most_recent_in_stack);

            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        Internal::LuaNS::PutOnStack(state_, "lua_opt_file_result");

        assert(lua_isboolean(state_, most_recent_in_stack));
        const bool result = static_cast<bool>(lua_toboolean(state_, most_recent_in_stack));

        if (!result)
        {
            oconv.str("");
            oconv << "Constraint \"" << constraint << "\" is not fulfilled for \"" << name << "\"!";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


    template<class T, class VariantSelectorT>
    void LuaOptionFile::ReadVector(const std::string& entry_name,
                                   const std::string& constraint,
                                   const char* invoking_file,
                                   int invoking_line,
                                   T& vector,
                                   VariantSelectorT&& variant_selector)
    {
        if (!lua_istable(state_, most_recent_in_stack))
            throw Exception("Error in Read: entry " + entry_name + " is not a table.", invoking_file, invoking_line);

        using element_type = typename T::value_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);

        std::size_t index = 0ul;
        static_cast<void>(index);

        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            element_type element;

            if constexpr (std::is_same<VariantSelectorT, std::nullptr_t>())
                Convert(most_recent_in_stack, entry_name, element, invoking_file, invoking_line, variant_selector);
            else
            {
                using type = std::decay_t<decltype(variant_selector)>;
                static_assert(Utilities::IsSpecializationOf<std::vector, type>());
                assert(index < variant_selector.size());
                Convert(
                    most_recent_in_stack, entry_name, element, invoking_file, invoking_line, variant_selector[index++]);
            }

            vector.push_back(element);

            lua_pop(state_, 3);
        }

        const auto Nelem = vector.size();
        for (auto i = 0ul; i < Nelem; ++i)
            CheckConstraint(entry_name + "[" + std::to_string(i + 1) + "]", constraint, invoking_file, invoking_line);
    }


    template<class T, class VariantSelectorT>
    void LuaOptionFile::ReadMap(const std::string& entry_name,
                                const std::string& constraint,
                                const char* invoking_file,
                                int invoking_line,
                                T& map,
                                VariantSelectorT&& variant_selector)
    {
        if (!lua_istable(state_, most_recent_in_stack))
            throw Exception("Error in Read: entry " + entry_name + " is not an associative container.",
                            invoking_file,
                            invoking_line);

        using key_type = typename T::key_type;
        using mapped_type = typename T::mapped_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);
        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            key_type key;
            mapped_type element;

            Convert(-2, entry_name, key, invoking_file, invoking_line, variant_selector);
            Convert(most_recent_in_stack, entry_name, element, invoking_file, invoking_line, variant_selector);

            const auto check = map.insert(std::make_pair(key, element));

            if (!check.second)
                throw Exception("Error while reading entry \"" + entry_name + "\": same key read twice!",
                                invoking_file,
                                invoking_line);

            lua_pop(state_, 3);
        }

        for (const auto& elem : map)
        {
            std::ostringstream oconv;

            if (std::is_same<key_type, std::string>())
                oconv << entry_name << "[\"" << elem.first << "\"]";
            else
                oconv << entry_name << "[" << elem.first << "]";

            CheckConstraint(oconv.str(), constraint, invoking_file, invoking_line);
        }
    }


    inline const std::string& LuaOptionFile::GetFilename() const noexcept
    {
        assert("Probably missing call to Open()!" && !filename_.empty());
        return filename_;
    }


    inline const std::vector<std::string>& LuaOptionFile::GetEntryKeyList() const noexcept
    {
        return entry_key_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HXX_
