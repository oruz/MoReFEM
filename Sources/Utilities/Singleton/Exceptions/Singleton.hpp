/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 17:41:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_
#define MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_

#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export // IWYU pragma: export


namespace MoReFEM
{


    namespace Utilities
    {


        namespace ExceptionNS
        {


            namespace Singleton
            {


                //! This exception is thrown if a call is made after singleton destruction.
                class DeadReference final : public MoReFEM::Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] name Name of the \a Singleton for which the exception was thrown.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit DeadReference(const std::string& name, const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    DeadReference(const DeadReference& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    DeadReference(DeadReference&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    DeadReference& operator=(const DeadReference& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    DeadReference& operator=(DeadReference&& rhs) = default;


                    //! Destructor
                    virtual ~DeadReference() override;
                };


                /*!
                 * This exception is thrown when Singleton<T>::GetInstance(__FILE__, __LINE__) is called while singleton
                 * has not yet been created.
                 */
                class NotYetCreated final : public MoReFEM::Exception
                {
                  public:
                    /*!
                     * \brief Constructor
                     *
                     * \param[in] name Name of the \a Singleton for which the exception was thrown.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit NotYetCreated(const std::string& name, const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    NotYetCreated(const NotYetCreated& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    NotYetCreated(NotYetCreated&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    NotYetCreated& operator=(const NotYetCreated& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    NotYetCreated& operator=(NotYetCreated&& rhs) = default;

                    //! Destructor
                    virtual ~NotYetCreated() override;
                };


            } // namespace Singleton


        } // namespace ExceptionNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_
