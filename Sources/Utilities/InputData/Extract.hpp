//! \file
//
//
//  Extract.hpp
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Enum.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"  // IWYU pragma: export


namespace MoReFEM::Utilities::InputDataNS
{


    /*!
     * \brief A convenient helper to extract information from Base class.
     *
     * The problem with Base class is that the syntax is really hideous; for instance a derived class
     * from base (named IPL for the example below) must use the following syntax to extract a value
     * named Component:
     *
     * \code
     * using Type = InputData::BoundaryCondition::Component;
     * auto component_value = this->template ReadHelper<Type>();
     * \endcode
     *
     * This template syntax is quite unusual and many C++ developers don't know it; so the helper class
     * hids that and the same quantity may be extracted with:
     * \code
     * using Type = InputData::BoundaryCondition::Component;
     * auto component_value = Extract<Type>::Value();
     * \endcode
     *
     * This class can also be used to read Lua functions:
     * \code
     * decltype(auto) fct = Extract<InputData::Force::Surfacic>::Value(input_data);
     * std::cout << "Value = " << fct(0, 1, 2, 3) << std::endl;
     * \endcode
     */
    template<class ObjectT>
    struct Extract
    {

        //! Convenient alias over the return type.
        using return_type = typename ObjectT::return_type;

        /*!
         * \brief Return the value of the input parameter.
         *
         * \tparam InputDataT The object which holds the information about input data file content.
         * \tparam CountAsUsedT At the end of the program, we may sum up the input data that have not been
         * used. A call to this method tally them as used unless 'no' is specified here.
         * \copydoc doxygen_hide_input_data_arg
         *
         * \return Value of the input parameter.
         */
        template<class InputDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
        static typename ConstRefOrValue<return_type>::type Value(const InputDataT& input_data);


        /*!
         * \brief Return a string that stands for a path; only additional operation is to replace environment
         * variables by their value.
         *
         * For instance "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
         *
         * \param[in] input_data Object which holds all relevant input data for the model considered.
         *
         * \return Path related to \a ObjectT.
         */
        template<class InputDataT>
        static std::string Path(const InputDataT& input_data);


        /*!
         * \brief Read the number of elements when the object is a vector.
         *
         * If not a vector, a compilation error will occur as size() method won't be defined.
         * \param[in] input_data Object which holds all relevant input data for the model considered.
         *
         *
         * \return Number of elements hold by \a ObjectT.

         */
        template<class InputDataT>
        static std::size_t Number(const InputDataT& input_data);

        /*!
         * \brief Return the \a index -th value of a vector.
         *
         * If the input parameter is not a vector, subscript operator will yield an error at compile time.
         *
         * \param[in] input_data Object which holds all relevant input data for the model considered.
         * \param[in] index Index of the element sought in the vector read in the \a input_data.
         *
         * \return Value of the \a index -th element of \a ObjectT.
         */
        template<class InputDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
        static decltype(auto) Subscript(const InputDataT& input_data, std::size_t index);
    };


} // namespace MoReFEM::Utilities::InputDataNS


#include "Utilities/InputData/Extract.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_
