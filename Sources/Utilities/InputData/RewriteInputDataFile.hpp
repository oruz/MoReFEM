//! \file
//
//
//  RewriteInputDataFile.hpp
//  MoReFEM
//
//  Created by sebastien on 25/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HPP_

#include <iostream>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Base.hpp"


namespace MoReFEM
{


    /*!
     * \brief Rewrite the input data file that was used to load the input data in the first place.
     *
     * Doing so might be useful to make sure the comments are up-to-date: if the documentation of the fields has changed
     * you will get the latest version this way. It will also trim fields that were in fact not used in the model.
     *
     * \copydoc doxygen_hide_input_data_arg
     */
    template<class InputDataT>
    void RewriteInputDataFile(const InputDataT& input_data);


} // namespace MoReFEM


#include "Utilities/InputData/RewriteInputDataFile.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HPP_
