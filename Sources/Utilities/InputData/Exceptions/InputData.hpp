/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 15:14:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HPP_

#include <sstream>
#include <string_view>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export // IWYU pragma: export


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace ExceptionNS
            {


                //! Generic class
                class Exception : public MoReFEM::Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] msg Message
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    Exception(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    Exception(Exception&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    Exception& operator=(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    Exception& operator=(Exception&& rhs) = default;

                    //! Destructor
                    virtual ~Exception() override;
                };


                /*!
                 * \brief Thrown when an input parameter appears twice in the input data file.
                 */
                class DuplicateInInputFile final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] filename Name of the input file from which input parameters were read.
                     * \param[in] section Section in the input file.
                     * \param[in] variable_list All variables defined in the section of this input file.
                     * When exception is thrown we know there are duplicates but we till don't know
                     * which; it will be determined here.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit DuplicateInInputFile(const std::string& filename,
                                                  const std::string& section,
                                                  const std::vector<std::string>& variable_list,
                                                  const char* invoking_file,
                                                  int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    DuplicateInInputFile(const DuplicateInInputFile& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    DuplicateInInputFile(DuplicateInInputFile&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    DuplicateInInputFile& operator=(const DuplicateInInputFile& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    DuplicateInInputFile& operator=(DuplicateInInputFile&& rhs) = default;


                    //! Destructor
                    virtual ~DuplicateInInputFile() override;
                };


                /*!
                 * \brief Thrown when a same key (section/name) appears twice in the tuple elements.
                 */
                class DuplicateInTuple final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] key The key found at least twice.
                     * Key is section.name is section is not empty and name otherwise.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit DuplicateInTuple(const std::string& key, const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    DuplicateInTuple(const DuplicateInTuple& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    DuplicateInTuple(DuplicateInTuple&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    DuplicateInTuple& operator=(const DuplicateInTuple& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    DuplicateInTuple& operator=(DuplicateInTuple&& rhs) = default;


                    //! Destructor
                    virtual ~DuplicateInTuple() override;
                };


                /*!
                 * \brief Thrown when an input parameter in the input data file is not related to an
                 * element in the tuple list.
                 */
                class UnboundInputData final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] filename Name of the input file from which input parameters were read.
                     * \param[in] section Section in the input file.
                     * \param[in] variable Variable read in the input file.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit UnboundInputData(const std::string& filename,
                                              std::string_view section,
                                              std::string_view variable,
                                              const char* invoking_file,
                                              int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    UnboundInputData(const UnboundInputData& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    UnboundInputData(UnboundInputData&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    UnboundInputData& operator=(const UnboundInputData& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    UnboundInputData& operator=(UnboundInputData&& rhs) = default;

                    //! Destructor
                    virtual ~UnboundInputData() override;
                };


                /*!
                 * \brief Thrown when an input parameter in the input data file is not related to an
                 * element in the tuple list.
                 *
                 * \tparam SubTupleT Tuple type which includes several input parameters which vectors should
                 * have been the same size.
                 *
                 */
                template<class SubTupleT>
                class MismatchedVectorSize final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit MismatchedVectorSize(const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    MismatchedVectorSize(const MismatchedVectorSize& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    MismatchedVectorSize(MismatchedVectorSize&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    MismatchedVectorSize& operator=(const MismatchedVectorSize& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    MismatchedVectorSize& operator=(MismatchedVectorSize&& rhs) = default;

                    //! Destructor
                    virtual ~MismatchedVectorSize() override;
                };


                /*!
                 * \brief Thrown when MPI has not been initialized properly.
                 *
                 */
                class MpiNotInitialized final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit MpiNotInitialized(const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    MpiNotInitialized(const MpiNotInitialized& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    MpiNotInitialized(MpiNotInitialized&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    MpiNotInitialized& operator=(const MpiNotInitialized& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    MpiNotInitialized& operator=(MpiNotInitialized&& rhs) = default;

                    //! Destructor
                    virtual ~MpiNotInitialized() override;
                };


                /*!
                 * \brief Thrown when a folder specified in the input file doesn't exist whereas it was
                 * expected to.
                 *
                 */
                class FolderDoesntExist final : public Exception
                {
                  public:
                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] folder Name of the folder.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit FolderDoesntExist(const std::string& folder, const char* invoking_file, int invoking_line);

                    //! \copydoc doxygen_hide_copy_constructor
                    FolderDoesntExist(const FolderDoesntExist& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    FolderDoesntExist(FolderDoesntExist&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    FolderDoesntExist& operator=(const FolderDoesntExist& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    FolderDoesntExist& operator=(FolderDoesntExist&& rhs) = default;

                    //! Destructor
                    virtual ~FolderDoesntExist() override;
                };


            } // namespace ExceptionNS


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Exceptions/InputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HPP_
