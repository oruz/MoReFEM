//! \file
//
//
//  RewriteInputDataFile.hxx
//  MoReFEM
//
//  Created by sebastien on 25/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HXX_

// IWYU pragma: private, include "Utilities/InputData/RewriteInputDataFile.hpp"


namespace MoReFEM
{


    template<class InputDataT>
    void RewriteInputDataFile(const InputDataT& input_data)
    {
        decltype(auto) original_file = input_data.GetInputFile();
        decltype(auto) backup_file = original_file + ".bak";

        FilesystemNS::File::Copy(original_file,
                                 backup_file,
                                 FilesystemNS::File::fail_if_already_exist::no,
                                 FilesystemNS::File::autocopy::no,
                                 __FILE__,
                                 __LINE__);

        try
        {
            FilesystemNS::File::Remove(original_file, __FILE__, __LINE__);
            Utilities::InputDataNS::Write(input_data, original_file);
        }
        catch (const Exception& e)
        {
            std::cerr << "Exception caught: " << e.what() << std::endl;
            std::cerr << "File " << original_file
                      << " will be put back in its original state and exception will be"
                         " thrown again."
                      << std::endl;

            FilesystemNS::File::Copy(backup_file,
                                     original_file,
                                     FilesystemNS::File::fail_if_already_exist::no,
                                     FilesystemNS::File::autocopy::no,
                                     __FILE__,
                                     __LINE__);

            throw;
        }

        std::cout << "File " << original_file
                  << " has been updated; a copy of the former version has been made "
                     "(with an additional .bak extension). Please review it before validating it! "
                  << std::endl;
        std::cout
            << "It is possible that some fields have disappeared: if there was in the data file a field which "
               "was not considered at all (i.e. had no match in the InputData tuple) they were in fact unused and were "
               "dropped here."
            << std::endl;
    }


} // namespace MoReFEM


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_REWRITE_INPUT_DATA_FILE_HXX_
