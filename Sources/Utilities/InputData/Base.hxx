/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HXX_

// IWYU pragma: private, include "Utilities/InputData/Base.hpp"
#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            Base<DerivedT, TupleT, AllowInvalidLuaFileT>::Base(const std::string& filename,
                                                               const Wrappers::Mpi& mpi,
                                                               DoTrackUnusedFields do_track_unused_fields)
            : mpi_parent(mpi), input_data_file_(filename)
            {
                static_cast<void>(do_track_unused_fields);

                int is_initialized;

                MPI_Initialized(&is_initialized);

                if (!is_initialized)
                    throw ExceptionNS::MpiNotInitialized(__FILE__, __LINE__);

                static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value,
                              "Template argument is expected to be a std::tuple.");

                CheckNoDuplicateKeysInTuple();

                LuaOptionFile lua_option_file(filename, __FILE__, __LINE__);

                if constexpr (AllowInvalidLuaFileT == Utilities::InputDataNS::allow_invalid_lua_file::no)
                    CheckUnboundInputData(filename, lua_option_file, do_track_unused_fields);

                // Fill from the file all the objects in tuple_.
                Internal::InputDataNS::FillTuple<AllowInvalidLuaFileT>(this, lua_option_file, tuple_);
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            Base<DerivedT, TupleT, AllowInvalidLuaFileT>::~Base()
            { }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            constexpr std::size_t Base<DerivedT, TupleT, AllowInvalidLuaFileT>::Size()
            {
                return std::tuple_size<TupleT>::value;
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            inline const std::string& Base<DerivedT, TupleT, AllowInvalidLuaFileT>::GetInputFile() const
            {
                assert(!input_data_file_.empty());
                return input_data_file_;
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            const auto& Base<DerivedT, TupleT, AllowInvalidLuaFileT>::GetTuple() const
            {
                return tuple_;
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class InputDataT, CountAsUsed CountAsUsedT>
            typename Utilities::ConstRefOrValue<typename InputDataT::return_type>::type
            Base<DerivedT, TupleT, AllowInvalidLuaFileT>::ReadHelper() const
            {
                const InputDataT* item_ptr = nullptr;

                constexpr bool found = tuple_iteration::template Find<InputDataT>();
                static_assert(found,
                              "InputData not defined in the tuple! To find out which in the admittedly poor "
                              "error message, seek the class name before 'ReadHelper' in the error message below.");

                tuple_iteration::template ExtractValue<InputDataT>(tuple_, item_ptr);

                assert(!(!item_ptr)
                       && "If the item is not defined in the tuple static assert two lines "
                          "earlier should have been triggered");

                const auto& item = *item_ptr;

                if (CountAsUsedT == CountAsUsed::yes)
                    item.SetAsUsed();

                return item.GetTheValue();
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class InputDataT, CountAsUsed CountAsUsedT>
            typename Utilities::ConstRefOrValue<typename InputDataT::return_type::value_type>::type
            Base<DerivedT, TupleT, AllowInvalidLuaFileT>::ReadHelper(std::size_t index) const
            {
                const auto& buf = ReadHelper<InputDataT, CountAsUsedT>();
                assert(index < buf.size());
                return buf[index];
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class InputDataT>
            std::string Base<DerivedT, TupleT, AllowInvalidLuaFileT>::ReadHelperPath() const
            {
                decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
                return environment.SubstituteValues(ReadHelper<InputDataT, CountAsUsed::yes>());
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class InputDataT, CountAsUsed CountAsUsedT>
            std::size_t Base<DerivedT, TupleT, AllowInvalidLuaFileT>::ReadHelperNumber() const
            {
                return ReadHelper<InputDataT, CountAsUsedT>().size();
            }


            template<class TupleT>
            void CreateDefaultInputFile(const std::string& path)
            {
                std::ofstream out;
                FilesystemNS::File::Create(out, path, __FILE__, __LINE__);

                out << "-- Comment lines are introduced by \"--\".\n";
                out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";

                Internal::InputDataNS::PrepareDefaultEntries<TupleT>(out);
            }


            template<class InputData>
            void Write(const InputData& input_data, const std::string& path)
            {
                std::ofstream out;
                FilesystemNS::File::Create(out, path, __FILE__, __LINE__);

                out << "-- Comment lines are introduced by \"--\".\n";
                out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";

                Internal::InputDataNS::PrintContent(input_data, out);
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            void Base<DerivedT, TupleT, AllowInvalidLuaFileT>::CheckNoDuplicateKeysInTuple() const
            {
                // Check there are no type duplicated in the tuple.
                Utilities::Tuple::AssertNoDuplicate<TupleT>::Perform();

                // Check there are no duplicated keys in the tuple (two different types that share the same key for
                // instance).
                std::unordered_set<std::string> buf;
                tuple_iteration::CheckNoDuplicateKeysInTuple(buf);
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            void Base<DerivedT, TupleT, AllowInvalidLuaFileT>::CheckUnboundInputData(
                const std::string& filename,
                LuaOptionFile& lua_option_file,
                DoTrackUnusedFields do_track_unused_fields) const
            {
                // Check there are no items undefined in the tuple.
                if (do_track_unused_fields == DoTrackUnusedFields::yes)
                {
                    decltype(auto) entry_key_list = lua_option_file.GetEntryKeyList();

                    for (decltype(auto) entry_key : entry_key_list)
                    {
                        const auto pos = entry_key.rfind(".");

                        std::string_view section_name, variable;

                        if (pos == std::string::npos)
                        {
                            section_name = "";
                            variable = entry_key;
                        } else
                        {
                            section_name = std::string_view(entry_key.data(), pos);
                            variable = std::string_view(&entry_key.at(pos + 1), entry_key.size() - pos - 1);
                        }

                        if (!tuple_iteration::DoMatchIdentifier(section_name, variable))
                            throw ExceptionNS::UnboundInputData(filename, section_name, variable, __FILE__, __LINE__);
                    }
                }
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class SubTupleT>
            void Base<DerivedT, TupleT, AllowInvalidLuaFileT>::EnsureSameLength() const
            {
                enum
                {
                    size = std::tuple_size<SubTupleT>::value
                };
                std::size_t length;
                Internal::InputDataNS::ThroughSubtuple<SubTupleT, 0, size, TupleT>::EnsureSameLength(tuple_, length);
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            void Base<DerivedT, TupleT, AllowInvalidLuaFileT>::PrintUnused(std::ostream& out) const
            {
                // First gather the data from all processors: it is possible (though unlikely) than a given
                // input item is read only in some of the processors.
                std::vector<bool> stats;
                std::vector<std::string> identifiers;

                tuple_iteration::IsUsed(tuple_, identifiers, stats);

                assert(identifiers.size() == stats.size());

                // Then reduce it on the root processor.
                const auto& mpi = mpi_parent::GetMpi();

                auto&& reduced_stats = mpi.ReduceOnRootProcessor(stats, Wrappers::MpiNS::Op::LogicalOr);
                assert(reduced_stats.size() == stats.size());

                if (mpi.IsRootProcessor()
                    && !std::all_of(reduced_stats.cbegin(),
                                    reduced_stats.cend(),
                                    [](bool arg)
                                    {
                                        return arg;
                                    }))
                {
                    out << "Note about InputData: some of the input data weren't "
                           "actually used by the program:"
                        << std::endl;

                    const auto size = identifiers.size();

                    for (std::size_t i = 0ul; i < size; ++i)
                    {
                        if (!reduced_stats[i])
                            out << "\t - " << identifiers[i] << '\n';
                    }
                }
            }


            // clang-format off
            template
            <
                class DerivedT,
                class TupleT,
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT
            >
            // clang-format on
            template<class ItemT>
            constexpr bool Base<DerivedT, TupleT, AllowInvalidLuaFileT>::Find()
            {
                return tuple_iteration::template Find<ItemT>();
            }


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HXX_
