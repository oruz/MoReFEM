/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_

#include <variant>

#include "Utilities/HasMember/HasMember.hpp"
#include "Utilities/InputData/Enum.hpp" // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/InputData/LuaFunction.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"
#include "Utilities/String/String.hpp"


//! \copydoc doxygen_hide_create_member_check_macro
//! We want to generate the macro HAS_MEMBER_Selector.
CREATE_MEMBER_CHECK(Selector);


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            namespace Impl
            {


                /*!
                 * \brief Helper class to set the value of an InputData.
                 *
                 * \tparam TupleEltTypeT Current element of the input parameter tuple which value is set.
                 */
                template<class TupleEltTypeT>
                struct ElementType
                {


                    /*!
                     * \class doxygen_hide_input_data_element_type_args
                     *
                     * \param[in] element Current tuple element which value is set.
                     * \param[in] fullname Name of the key in Lua file, with its possible sections included. E.g.
                     * "domain4.unique_id".
                     * \copydoc doxygen_hide_input_data_arg
                     * \param[in] lua_option_file All the values read from the input parameter
                     * file are stored within this \a LuaOptionFile object. Not constant due to LuaOptionFile interface.
                     */

                    /*!
                     * \brief Set the value of \a element from the values read by \a LuaOptionFile in the input
                     * parameter file.
                     *
                     * \copydoc doxygen_hide_tparam_allow_invalid_lua_file
                     * \copydoc doxygen_hide_input_data_element_type_args
                     */
                    template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
                    static void Set(TupleEltTypeT& element,
                                    const InputDataT* input_data,
                                    const std::string& fullname,
                                    LuaOptionFile& lua_option_file);
                };


            } // namespace Impl


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Internal/TupleIteration/Impl/TupleItem.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_
