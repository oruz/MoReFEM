/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            // ============================
            // Find()
            // ============================


            template<class TupleEltTypeT>
            constexpr bool SectionOrLeaf<TupleEltTypeT, Nature::leaf, TupleEltTypeT>::Find()
            {
                return true;
            }


            template<class TupleEltTypeT>
            constexpr bool SectionOrLeaf<TupleEltTypeT, Nature::section, TupleEltTypeT>::Find()
            {
                return true;
            }


            template<class TupleEltTypeT, class InputDataT>
            constexpr bool SectionOrLeaf<TupleEltTypeT, Nature::leaf, InputDataT>::Find()
            {
                return false;
            }


            template<class TupleEltTypeT, class InputDataT>
            constexpr bool SectionOrLeaf<TupleEltTypeT, Nature::section, InputDataT>::Find()
            {
                return tuple_iteration::template Find<InputDataT>();
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            template<class InputDataT>
            constexpr bool TupleIteration<TupleT, Index, Length>::Find()
            {
                using enriched_section_or_leaf = SectionOrLeaf<tuple_elt_type, GetNature(), InputDataT>;

                constexpr bool found_in_current = enriched_section_or_leaf::Find();

                if constexpr (found_in_current)
                    return true;
                else
                    return next_item::template Find<InputDataT>();
            }


            template<class TupleT, std::size_t Length>
            template<class InputDataT>
            constexpr bool TupleIteration<TupleT, Length, Length>::Find()
            {
                return false;
            }


            // ============================
            // ExtractValue()
            // ============================


            template<class TupleEltTypeT>
            void
            SectionOrLeaf<TupleEltTypeT, Nature::leaf, TupleEltTypeT>::ExtractValue(const TupleEltTypeT& item,
                                                                                    const TupleEltTypeT*& leaf_if_found)
            {
                leaf_if_found = &item;
            }


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section, TupleEltTypeT>::ExtractValue(
                const TupleEltTypeT& item,
                const TupleEltTypeT*& leaf_if_found)
            {
                static_cast<void>(item);
                static_cast<void>(leaf_if_found);
                static_assert(std::is_same<std::false_type, TupleEltTypeT>::value,
                              "Extract() methods from InputData shouldn't act on a section, only "
                              "on leafs.");
            }


            template<class TupleEltTypeT, class InputDataT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf, InputDataT>::ExtractValue(const TupleEltTypeT& item,
                                                                                      const InputDataT*& leaf_if_found)
            {
                static_cast<void>(item);
                static_cast<void>(leaf_if_found);
            }


            template<class TupleEltTypeT, class InputDataT>
            void
            SectionOrLeaf<TupleEltTypeT, Nature::section, InputDataT>::ExtractValue(const TupleEltTypeT& item,
                                                                                    const InputDataT*& leaf_if_found)
            {
                constexpr bool is_leaf_in_current_section = tuple_iteration::template Find<InputDataT>();

                if constexpr (is_leaf_in_current_section)
                {
                    tuple_iteration::ExtractValue(item.GetSectionContent(), leaf_if_found);
                }
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            template<class InputDataT>
            void TupleIteration<TupleT, Index, Length>::ExtractValue(const TupleT& tuple,
                                                                     const InputDataT*& leaf_if_found)
            {
                using enriched_section_or_leaf = SectionOrLeaf<tuple_elt_type, GetNature(), InputDataT>;

                constexpr bool found_in_current = enriched_section_or_leaf::Find();

                if constexpr (found_in_current)
                {
                    const auto& item = std::get<Index>(tuple);
                    enriched_section_or_leaf::ExtractValue(item, leaf_if_found);
                } else
                {
                    next_item::template ExtractValue<InputDataT>(tuple, leaf_if_found);
                }
            }


            template<class TupleT, std::size_t Length>
            template<class InputDataT>
            void TupleIteration<TupleT, Length, Length>::ExtractValue(const TupleT&, const InputDataT*&)
            { }

            // ============================
            // Fill()
            // ============================


            // clang-format off
            template
            <
                Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT,
                class TupleT,
                class InputDataT
            >
            // clang-format on
            void FillTuple(const InputDataT* input_data, LuaOptionFile& lua_option_file, TupleT& tuple)
            {
                enum
                {
                    size = std::tuple_size<TupleT>::value
                };

                TupleIteration<TupleT, 0, size>::template Fill<AllowInvalidLuaFileT>(
                    input_data, lua_option_file, tuple);
            }


            template<class TupleEltTypeT>
            template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::Fill(const InputDataT* input_data,
                                                                  LuaOptionFile& lua_option_file,
                                                                  TupleEltTypeT& item)
            {
                // Read in the input data file the whole identifier, which is 'section.name' or 'name'
                // if no section is provided.
                const std::string& full_name = TupleEltTypeT::GetIdentifier();

                Impl::ElementType<TupleEltTypeT>::template Set<AllowInvalidLuaFileT>(
                    item, input_data, full_name, lua_option_file);
            }


            template<class TupleEltTypeT>
            template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::Fill(const InputDataT* input_data,
                                                                     LuaOptionFile& lua_option_file,
                                                                     TupleEltTypeT& item)
            {
                tuple_iteration::template Fill<AllowInvalidLuaFileT>(
                    input_data, lua_option_file, item.GetNonCstSectionContent());
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
            void TupleIteration<TupleT, Index, Length>::Fill(const InputDataT* input_data,
                                                             LuaOptionFile& lua_option_file,
                                                             TupleT& tuple)
            {
                static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value,
                              "TupleT should be a std::tuple.");

                // If current item is a section, go deeper inside to find nested input data.
                auto& element = std::get<Index>(tuple);

                section_or_leaf::template Fill<AllowInvalidLuaFileT>(input_data, lua_option_file, element);

                // Recursion.
                next_item::template Fill<AllowInvalidLuaFileT>(input_data, lua_option_file, tuple);
            }


            template<class TupleT, std::size_t Length>
            template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
            void TupleIteration<TupleT, Length, Length>::Fill(const InputDataT*, LuaOptionFile&, TupleT&)
            {
                // Do nothing.
            }


            // ============================
            // PrepareDefaultEntries()
            // ============================


            template<class TupleT>
            void PrepareDefaultEntries(std::ostream& out)
            {
                enum
                {
                    size = std::tuple_size<TupleT>::value
                };

                std::vector<std::pair<std::string, std::string>> block_per_identifier;

                // Extract all input data from \a TupleT (including those enclosed in sections) and
                // generate their content which is stored in a map.
                TupleIteration<TupleT, 0, size>::template PrepareEntry<print_default_value::yes, std::nullptr_t>(
                    block_per_identifier, nullptr);

                Impl::PrintInFile(block_per_identifier, out);
            }


            template<class InputDataT>
            void PrintContent(const InputDataT& input_data, std::ostream& stream)
            {
                using tuple = typename InputDataT::Tuple;
                constexpr auto size = std::tuple_size<tuple>::value;

                std::vector<std::pair<std::string, std::string>> block_per_identifier;

                // Extract all input data from \a TupleT (including those enclosed in sections) and
                // generate their content which is stored in a map.
                TupleIteration<tuple, 0, size>::template PrepareEntry<print_default_value::if_none_found>(
                    block_per_identifier, &input_data);

                Impl::PrintInFile(block_per_identifier, stream);
            }


            template<class TupleEltTypeT>
            void UseDefaultValueIfPossible(std::ostream& oconv)
            {
                if (TupleEltTypeT::DefaultValue().empty())
                    oconv << "No default value was provided!";
                else
                    oconv << TupleEltTypeT::DefaultValue();
            }


            template<class TupleEltTypeT>
            template<print_default_value DoPrintDefaultValueT, class InputDataT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::PrepareEntry(
                std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                const InputDataT* input_data)
            {
                std::ostringstream oconv;

                const std::string& indent =
                    Impl::GetIndentPlaceholder(); // will be replaced later by the correct indentation.
                const auto indent_comment = indent + "-- ";

                oconv << "\n";

                oconv << indent_comment;

                constexpr std::size_t max_length = 105;

                if (TupleEltTypeT::Description().empty())
                    oconv << "No description was provided!";
                else
                    oconv << Utilities::String::Reformat(TupleEltTypeT::Description(), max_length, indent_comment);

                oconv << indent_comment << "Expected format: "
                      << Utilities::String::Reformat(
                             Traits::Format<typename TupleEltTypeT::return_type>::Print(indent_comment),
                             max_length,
                             indent_comment);

                if (!TupleEltTypeT::Constraint().empty())
                    oconv << indent_comment << "Constraint: " << TupleEltTypeT::Constraint() << '\n';

                oconv << indent << TupleEltTypeT::NameInFile() << " = ";

                if constexpr (DoPrintDefaultValueT == print_default_value::yes)
                {
                    assert(input_data == nullptr);
                    static_cast<void>(input_data);
                    UseDefaultValueIfPossible<TupleEltTypeT>(oconv);
                } else
                {
                    assert(input_data != nullptr);

                    const TupleEltTypeT* value = nullptr;

                    using tuple = typename InputDataT::Tuple;
                    enum
                    {
                        tuple_size = std::tuple_size<tuple>::value
                    };
                    using tuple_iteration = MoReFEM::Internal::InputDataNS::TupleIteration<tuple, 0, tuple_size>;

                    tuple_iteration::template ExtractValue<TupleEltTypeT>(input_data->GetTuple(), value);
                    assert(!(!value));

                    if (value->HasValue())
                        Internal::PrintPolicyNS::LuaFormat::Do(oconv, value->GetTheValue());
                    else
                        UseDefaultValueIfPossible<TupleEltTypeT>(oconv);
                }

                block_per_identifier.emplace_back(std::make_pair(TupleEltTypeT::GetIdentifier(), oconv.str()));
            }


            template<class TupleEltTypeT>
            template<print_default_value DoPrintDefaultValueT, class InputDataT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::PrepareEntry(
                std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                const InputDataT* input_data)
            {
                tuple_iteration::template PrepareEntry<DoPrintDefaultValueT>(block_per_identifier, input_data);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            template<print_default_value DoPrintDefaultValueT, class InputDataT>
            void TupleIteration<TupleT, Index, Length>::PrepareEntry(
                std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                const InputDataT* input_data)
            {
                section_or_leaf::template PrepareEntry<DoPrintDefaultValueT>(block_per_identifier, input_data);

                // Recursion.
                next_item::template PrepareEntry<DoPrintDefaultValueT>(block_per_identifier, input_data);
            }


            template<class TupleT, std::size_t Length>
            template<print_default_value DoPrintDefaultValueT, class InputDataT>
            void TupleIteration<TupleT, Length, Length>::PrepareEntry(
                std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                const InputDataT* input_data)
            {
                // Do nothing.
                static_cast<void>(block_per_identifier);
                static_cast<void>(input_data);
            }


            // ============================
            // PrintKeys()
            // ============================


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::PrintKeys(std::ostream& out)
            {
                out << '\t' << TupleEltTypeT::GetIdentifier() << '\n';
            }


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::PrintKeys(std::ostream& out)
            {
                tuple_iteration::PrintKeys(out);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            void TupleIteration<TupleT, Index, Length>::PrintKeys(std::ostream& out)
            {
                // If current item is a section, go deeper inside to find nested input data.
                section_or_leaf::PrintKeys(out);

                // Recursion.
                next_item::PrintKeys(out);
            }


            template<class TupleT, std::size_t Length>
            void TupleIteration<TupleT, Length, Length>::PrintKeys(std::ostream&)
            {
                // Do nothing.
            }


            // ============================
            // Unused tracking
            // ============================


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::IsUsed(const TupleEltTypeT& item,
                                                                    std::vector<std::string>& identifier_list,
                                                                    std::vector<bool>& usage_statistics)
            {
                identifier_list.push_back(TupleEltTypeT::GetIdentifier());
                usage_statistics.push_back(item.IsUsed());
            }


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::IsUsed(const TupleEltTypeT& item,
                                                                       std::vector<std::string>& identifier_list,
                                                                       std::vector<bool>& usage_statistics)
            {
                tuple_iteration::IsUsed(item.GetSectionContent(), identifier_list, usage_statistics);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            void TupleIteration<TupleT, Index, Length>::IsUsed(const TupleT& tuple,
                                                               std::vector<std::string>& identifier_list,
                                                               std::vector<bool>& usage_statistics)
            {
                const auto& element = std::get<Index>(tuple);

                // If current item is a section, go deeper inside to find nested input data.
                section_or_leaf::IsUsed(element, identifier_list, usage_statistics);

                // Recursion.
                next_item::IsUsed(tuple, identifier_list, usage_statistics);
            }


            template<class TupleT, std::size_t Length>
            void TupleIteration<TupleT, Length, Length>::IsUsed(const TupleT& tuple,
                                                                std::vector<std::string>& identifier_list,
                                                                std::vector<bool>& usage_statistics)
            {
                // Do nothing.
                static_cast<void>(tuple);
                static_cast<void>(identifier_list);
                static_cast<void>(usage_statistics);
            }


            // ============================
            // Duplicates.
            // ============================

            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::CheckNoDuplicateKeysInTuple(
                std::unordered_set<std::string>& end_items_keys)
            {
                decltype(auto) key = TupleEltTypeT::GetIdentifier();

                if (end_items_keys.find(key) != end_items_keys.cend())
                    throw Utilities::InputDataNS::ExceptionNS::DuplicateInTuple(key, __FILE__, __LINE__);

                end_items_keys.insert(key);
            }


            template<class TupleEltTypeT>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::CheckNoDuplicateKeysInTuple(
                std::unordered_set<std::string>& end_items_keys)
            {
                tuple_iteration::CheckNoDuplicateKeysInTuple(end_items_keys);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            void
            TupleIteration<TupleT, Index, Length>::CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& keys)
            {
                // If current item is a section, go deeper inside to find nested input data.
                section_or_leaf::CheckNoDuplicateKeysInTuple(keys);

                // Recursion.
                next_item::CheckNoDuplicateKeysInTuple(keys);
            }

            template<class TupleT, std::size_t Length>
            void TupleIteration<TupleT, Length, Length>::CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>&)
            {
                // Do nothing.
            }


            // ============================
            // Lookup functions.
            // ============================


            template<class TupleEltTypeT>
            bool SectionOrLeaf<TupleEltTypeT, Nature::leaf>::DoMatchIdentifier(std::string_view section,
                                                                               std::string_view variable)
            {


                return variable == TupleEltTypeT::NameInFile()
                       && section == TupleEltTypeT::enclosing_section::GetFullName();
            }


            template<class TupleEltTypeT>
            bool SectionOrLeaf<TupleEltTypeT, Nature::section>::DoMatchIdentifier(std::string_view section,
                                                                                  std::string_view variable)

            {
                return tuple_iteration::DoMatchIdentifier(section, variable);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            bool TupleIteration<TupleT, Index, Length>::DoMatchIdentifier(std::string_view section,
                                                                          std::string_view variable)
            {
                const bool do_match = section_or_leaf::DoMatchIdentifier(section, variable);

                if (do_match)
                    return true;

                return next_item::DoMatchIdentifier(section, variable);
            }


            template<class TupleT, std::size_t Length>
            bool TupleIteration<TupleT, Length, Length>::DoMatchIdentifier(std::string_view, std::string_view)
            {
                return false;
            }


            // ============================
            // Whether section or leaf is considered.
            // ============================

            template<class TupleT, std::size_t Index, std::size_t Length>
            constexpr Nature TupleIteration<TupleT, Index, Length>::GetNature() noexcept
            {
                return tuple_elt_type::GetNature();
            }


            // ============================
            // ActIfSection()
            // ============================


            template<class TupleEltTypeT>
            template<class SectionTypeT, class ActionT, typename... Args>
            void SectionOrLeaf<TupleEltTypeT, Nature::leaf>::ActIfSection(const TupleEltTypeT&, ActionT, Args&&...)
            {
                // Do nothing.
            }


            template<class TupleEltTypeT>
            template<class SectionTypeT, class ActionT, typename... Args>
            void SectionOrLeaf<TupleEltTypeT, Nature::section>::ActIfSection(const TupleEltTypeT& item,
                                                                             ActionT action,
                                                                             Args&&... args)
            {
                if constexpr (std::is_base_of<SectionTypeT, TupleEltTypeT>())
                    action(item);

                tuple_iteration::template ActIfSection<SectionTypeT>(
                    item.GetSectionContent(), action, std::forward<Args>(args)...);
            }


            template<class TupleT, std::size_t Index, std::size_t Length>
            template<class SectionTypeT, class ActionT, typename... Args>
            void
            TupleIteration<TupleT, Index, Length>::ActIfSection(const TupleT& tuple, ActionT action, Args&&... args)
            {
                static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value,
                              "TupleT should be a std::tuple.");

                // If current item is a section, go deeper inside to find nested input data.
                auto& element = std::get<Index>(tuple);

                section_or_leaf::template ActIfSection<SectionTypeT>(element, action, std::forward<Args>(args)...);

                // Recursion.
                next_item::template ActIfSection<SectionTypeT>(tuple, action, std::forward<Args>(args)...);
            }


            template<class TupleT, std::size_t Length>
            template<class SectionTypeT, class ActionT, typename... Args>
            void TupleIteration<TupleT, Length, Length>::ActIfSection(const TupleT&, ActionT, Args&&...)
            {
                // Do nothing.
            }


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HXX_
