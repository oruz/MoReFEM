/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <string> // IWYU pragma: keep
#include <vector>
// IWYU pragma:  no_include <iosfwd>

#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            namespace Traits
            {


                std::string Format<std::string>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "\"VALUE\"";
                };


                std::string Format<std::vector<std::string>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "{\"VALUE1\", \"VALUE2\", ...}";
                };


                std::string Format<bool>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "'true' or 'false' (without the quote)";
                };


                std::string Format<std::vector<bool>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "{\"VALUE1\", \"VALUE2\", ...} where values are either 'true' or 'false' (without the "
                           "quote)";
                };


            } // namespace Traits


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
