/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_

#include <iosfwd>
#include <map>
#include <variant>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Utilities::InputDataNS { template <class T> struct LuaFunction; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            namespace Traits
            {


                /*!
                 * \brief Class specializations used in CreateDefaultInputFile() to write the expected
                 * format for a given input parameter.
                 */
                template<class return_type>
                struct Format
                {
                    //! Print the expected format associated to \a return_type.
                    //! \param[in] indent_comment Unused for most specializations, but required to make the ones
                    //! that span several lines good-looking.
                    static std::string Print(const std::string& indent_comment);
                };


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================


                template<>
                struct Format<std::string>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class T>
                struct Format<std::vector<T>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<>
                struct Format<std::vector<std::string>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class T>
                struct Format<Utilities::InputDataNS::LuaFunction<T>>
                {
                    static std::string Print(const std::string& indent_comment, bool none_desc = true);
                };


                template<>
                struct Format<bool>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<>
                struct Format<std::vector<bool>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class T, class U>
                struct Format<std::map<T, U>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class... Args>
                struct Format<std::variant<Args...>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace Traits


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_
