/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_


#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <list>
#include <map>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "Utilities/Containers/Tuple.hpp" // IWYU pragma: export
#include "Utilities/InputData/Enum.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"                   // IWYU pragma: export
#include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hpp"         // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/Impl/Print.hpp"     // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/Impl/TupleItem.hpp" // IWYU pragma: export
#include "Utilities/String/String.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            //! Enum class used in some methods that might either print default value or a value stored elsewhere.
            enum class print_default_value
            {
                yes,
                no,
                if_none_found
            };


            //! Convenient alias to avoid repeating endlessly namespaces.
            using Nature = Utilities::InputDataNS::Nature;


            /*!
             * \brief The struct in charge of depth management.
             *
             * The tuple upon which TupleIteration is expected to work may contain twio kind of items:
             * -Leafs, which values are read fron the input file and stored.
             * - Sections, that in turn may contain other sections and/or leafs.
             *
             * The purpose of current class is to handle each of these cases. If this is a section, another
             * TupleIteration is called to deal with tuple related to this section. Once it's done, work upon the first
             * tuple is resumed.
             *
             * \tparam TupleEltTypeT SectionOrLeaf should be called only by TupleIteration template class;
             * each of this call is on one of its tuple element, which type is given by current template parameter.
             * \tparam NatureT Whether the current tuple element is a leaf or a section.
             * \tparam InputDataTypeT Used only when a given type is sought within the tuple; otherwise
             * just take the same as \a TupleEltTypeT. Only the Find() and ExtractValue() static methods are defined in
             * the case \a TupleEltTypeT and \a InputDataTypeT differ.
             *
             * \internal <b><tt>[internal]</tt></b> Due to the circularity involved here, this class must be in the same
             * file as TupleIteration template class. Definition of specializations occurs after the declaration of
             * TupleIteration.
             * \endinternal
             */
            template<class TupleEltTypeT, Nature NatureT, class InputDataTypeT = TupleEltTypeT>
            struct SectionOrLeaf;


            /*!
             * \brief An helper class used to perform some operations recursively for all elements of \a TupleT
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input data.
             * \tparam Index Index of the current element considered in the tuple.
             * \tparam Length Number of elements in the tuple. Recursion ends when Index == Length.
             */
            template<class TupleT, std::size_t Index, std::size_t Length>
            struct TupleIteration
            {


                /*!
                 * \brief Extract the value of the \a Index -th element in the \a TupleT tuple.
                 *
                 * \tparam InputDataT Type of the input paramater considered.
                 *
                 * \param[in] tuple Tuple from which the input data are extracted.
                 * \param[out] leaf_if_found Value of the leaf if it is found. If not, an exception is
                 * thrown.
                 */
                template<class InputDataT>
                static void ExtractValue(const TupleT& tuple, const InputDataT*& leaf_if_found);


                /*!
                 * \brief Static method which looks for \a InputDataT within \a TupleT.
                 *
                 * \tparam InputDataT Input data that is sought in the tuple. It can't be a section!
                 *
                 * \return True when current element of the tuple (at \a Index position) is \a InputDataT.
                 *
                 */
                template<class InputDataT>
                static constexpr bool Find();


                /*!
                 * \brief Set the content of the tuple.
                 *
                 * \copydoc doxygen_hide_tparam_allow_invalid_lua_file
                 * \copydoc doxygen_hide_input_data_arg
                 * \param[in] lua_option_file \a LuaOptionFile object that gives the requested informations.
                 * \param[in,out] tuple The tuple into which the information is stored.
                 */
                template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
                static void Fill(const InputDataT* input_data, LuaOptionFile& lua_option_file, TupleT& tuple);


                /*!
                 * \brief Prepareentry for a given input item/.
                 *
                 * \tparam DoPrintDefaultValueT If yes, print the default value for each tuple item. If false, print
                 * the value from \a input_data
                 * \param[in,out] block_per_identifier List of key/default value for input data items.
                 * \param[in] input_data Should be nullptr if DoPrintDefaultValueT is yes. Otherwise, object which
                 * values are to be printed.
                 */
                template<print_default_value DoPrintDefaultValueT, class InputDataT>
                static void PrepareEntry(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                         const InputDataT* input_data);

                /*!
                 * \brief Print the list of keys found in the tuple.
                 *
                 * \copydoc doxygen_hide_stream_inout
                 */
                static void PrintKeys(std::ostream& stream);


                /*!
                 * \brief Collect statistics about usage of input items.
                 *
                 * \param[in] tuple Tuple considered.
                 * \param[in,out] identifier_list List of all identifiers met, whether they are used or not.
                 * Depth is taken into account here, so this list might be bigger than tuple size if sections are
                 * involved in the tuple considered.
                 * \param[in,out] usage_statistics This vector is the same size as \a identifier_list and is read
                 * alongside it; it tells whether the item was used or not.
                 */
                static void IsUsed(const TupleT& tuple,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);


                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] keys List of keys found so far. New key will be added if the exception wasn't
                 * triggered first.
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& keys);

                /*!
                 * \brief Helper recursive function that looks which tuple match a key found in the input file.
                 *
                 * \param[in] section Section read in the input file.
                 * \param[in] variable Name of the variable read in the input file.
                 *
                 * \return Whether \a variable is in the tuple or not.
                 */
                static bool DoMatchIdentifier(std::string_view section, std::string_view variable);


                /*!
                 * \brief Do something only if current item consider in the tuple is a section.
                 *
                 * \param[in] tuple Tuple considered.
                 * \param[in] action Functor applied to the section.
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleT& tuple, ActionT action, Args&&... args);


              private:
                //! Convenient alias.
                using tuple_elt_type = typename std::tuple_element<Index, TupleT>::type;

                //! Whether the current element is a leaf or a section.
                static constexpr Nature GetNature() noexcept;

                //! Alias to the next element in the tuple.
                using next_item = TupleIteration<TupleT, Index + 1, Length>;

                //! Convenient alias.
                using section_or_leaf = SectionOrLeaf<tuple_elt_type, GetNature()>;
            };


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Stopping condition of TupleIteration.
            // ============================


            template<class TupleT, std::size_t Length>
            struct TupleIteration<TupleT, Length, Length>
            {

                template<class InputDataT>
                static void ExtractValue(const TupleT&, const InputDataT*&);


                template<class InputDataT>
                static constexpr bool Find();


                template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
                static void Fill(const InputDataT* input_data, LuaOptionFile&, TupleT&);


                template<print_default_value DoPrintDefaultValueT, class InputDataT>
                static void PrepareEntry(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                         const InputDataT* input_data);


                static void PrintKeys(std::ostream& stream);

                static void IsUsed(const TupleT& tuple,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>&);


                static bool DoMatchIdentifier(std::string_view, std::string_view);

                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleT& tuple, ActionT action, Args&&... args);
            };


            // ============================
            // Stopping condition of TupleIteration.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // SectionOrLeaf was hacked into MoReFEM to introduce section management but is pointlessly
            // complicated; it should be refactored at some point to make it clearer and also allow a more direct
            // handle on sections (you can't manipulate directly a section in MoReFEM models).
            // I don't therefore take the time required to redocument all of this.
            // ============================


            template<class TupleEltTypeT, class InputDataT>
            struct SectionOrLeaf<TupleEltTypeT, Nature::section, InputDataT>
            {

                static void ExtractValue(const TupleEltTypeT& item, const InputDataT*& leaf_if_found);


                static constexpr bool Find();

                using section_tuple = typename TupleEltTypeT::section_content_type;

                using tuple_iteration = TupleIteration<section_tuple, 0, std::tuple_size<section_tuple>::value>;
            };


            template<class TupleEltTypeT, class InputDataT>
            struct SectionOrLeaf<TupleEltTypeT, Nature::leaf, InputDataT>
            {

                static void ExtractValue(const TupleEltTypeT& item, const InputDataT*& leaf_if_found);

                static constexpr bool Find();
            };


            //! Specialization of \a SectionOrLeaf for sections.
            template<class TupleEltTypeT>
            struct SectionOrLeaf<TupleEltTypeT, Nature::section, TupleEltTypeT>
            {


                static constexpr bool Find();


                static void ExtractValue(const TupleEltTypeT& item, const TupleEltTypeT*& leaf_if_found);


                template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
                static void Fill(const InputDataT* input_data, LuaOptionFile& lua_option_file, TupleEltTypeT& item);


                static void PrintKeys(std::ostream& stream);


                static void IsUsed(const TupleEltTypeT& item,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] end_items_keys List of keys found so far for end items (sections keys are
                 * not considered). New key will be added if the exception wasn't triggered first.
                 *
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& end_items_keys);


                using section_tuple = typename TupleEltTypeT::section_content_type;

                static bool DoMatchIdentifier(std::string_view, std::string_view);

                template<print_default_value DoPrintDefaultValueT, class InputDataT>
                static void PrepareEntry(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                         const InputDataT* input_data);

                using tuple_iteration = TupleIteration<section_tuple, 0, std::tuple_size<section_tuple>::value>;

                /*!
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleEltTypeT& item, ActionT action, Args&&... args);
            };


            //! Specialization of \a SectionOrLeaf for leafs.
            template<class TupleEltTypeT>
            struct SectionOrLeaf<TupleEltTypeT, Nature::leaf, TupleEltTypeT>
            {
                static constexpr bool Find();

                static void ExtractValue(const TupleEltTypeT& item, const TupleEltTypeT*& leaf_if_found);

                template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT, class InputDataT>
                static void Fill(const InputDataT* input_data, LuaOptionFile& lua_option_file, TupleEltTypeT& item);

                static void PrintKeys(std::ostream& stream);


                static void IsUsed(const TupleEltTypeT& item,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] end_items_keys List of keys found so far for end items (sections keys are
                 * not considered). New key will be added if the exception wasn't triggered first.
                 *
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& end_items_keys);

                static bool DoMatchIdentifier(std::string_view, std::string_view);

                template<print_default_value DoPrintDefaultValueT, class InputDataT>
                static void PrepareEntry(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                         const InputDataT* input_data);

                /*!
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleEltTypeT& item, ActionT action, Args&&... args);
            };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            /*!
             * \brief Function that will recursively fill the values read in the input file for each input item.
             *
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input items.
             * \tparam InputDataT Type of \a input_data.
             * \copydoc doxygen_hide_tparam_allow_invalid_lua_file
             *
             * \copydoc doxygen_hide_input_data_arg
             * \param[in] lua_option_file \a LuaOptionFile object.
             * \param[in,out] tuple Should be the data attribute tuple_ nested in Base class.
             */
            template<Utilities::InputDataNS::allow_invalid_lua_file AllowInvalidLuaFileT,
                     class TupleT,
                     class InputDataT>
            void FillTuple(const InputDataT* input_data, LuaOptionFile& lua_option_file, TupleT& tuple);


            /*!
             * \brief Prepare the entries to put in the default input file.
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input items.
             *
             * \copydoc doxygen_hide_stream_inout
             */
            template<class TupleT>
            void PrepareDefaultEntries(std::ostream& stream);


            /*!
             * \brief Print the content of \a input_data onto the \a stream.
             *
             * \copydoc doxygen_hide_input_data_arg
             * \copydoc doxygen_hide_stream_inout
             */
            template<class InputDataT>
            void PrintContent(const InputDataT& input_data, std::ostream& stream);


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_
