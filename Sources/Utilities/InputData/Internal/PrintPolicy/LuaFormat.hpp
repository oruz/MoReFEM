//! \file
//
//
//  LuaFormat.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HPP_

#include <iomanip>
#include <sstream>
#include <type_traits>
#include <variant>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/InputData/LuaFunction.hpp"
#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export
#include "Utilities/Type/PrintTypeName.hpp"


namespace MoReFEM::Internal::PrintPolicyNS
{


    /*!
     * \brief Policy to handle the printing of input data onto a stream.
     *
     * This is implemented very similarly as the policies of PrintContainer... because I am using this very facility
     * to print content of a vector!
     *
     * This function is implemented with recursivity: it is for instance possible to print a vector of heterogeneous
     * values (in a std::variant in this case).
     *
     * The point of this policy is to allow to print in a Lua file all the different types accepted in the Lua
     * interface. The policy has been classified as 'Internal' as it is very specific and is unlikely to be of use
     * directly to a end-user, contrary to the other policies provided in Utilities/Containers/PrintPolicy directory.
     */
    struct LuaFormat
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Internal::PrintPolicyNS


#include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HPP_
