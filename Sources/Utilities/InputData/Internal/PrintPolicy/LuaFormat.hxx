//! \file
//
//
//  LuaFormat.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hpp"


namespace MoReFEM::Internal::PrintPolicyNS
{


    template<class ElementTypeT>
    void LuaFormat::Do(std::ostream& stream, ElementTypeT&& entry)
    {
        using type = std::decay_t<ElementTypeT>;

        if constexpr (Utilities::IsSpecializationOf<std::variant, type>())
        {
            std::visit(
                [&stream](auto&& arg)
                {
                    LuaFormat::Do(stream, arg);
                },
                entry);
        } else if constexpr (Utilities::IsSpecializationOf<Utilities::InputDataNS::LuaFunction, type>())
        {
            stream << "\n[[\n";
            stream << entry.GetString();
            stream << "]]";
        } else if constexpr (std::is_same<type, std::string>())
        {
            stream << "'" << entry << "'";
        } else if constexpr (std::is_arithmetic<type>())
        {
            if constexpr (std::is_same<type, bool>())
                stream << std::boolalpha << entry;
            else
                stream << std::setprecision(15) << entry;
        } else if constexpr (Utilities::IsSpecializationOf<std::vector, type>())
        {
            // Trick here: current class is used as policy to that the choices here will be applied as well on
            // each element of the vector.
            Utilities::PrintContainer<LuaFormat>::Do(entry,
                                                     stream,
                                                     ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                     ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                     ::MoReFEM::PrintNS::Delimiter::closer(" }"));

        } else if constexpr (Utilities::IsSpecializationOf<std::map, type>())
        {
            // Map can only called on the end of the recursion, contrary to vector.
            using policy =
                ::MoReFEM::Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;
            Utilities::PrintContainer<policy>::Do(entry,
                                                  stream,
                                                  ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                  ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                  ::MoReFEM::PrintNS::Delimiter::closer("} "));
        } else if constexpr (std::is_same<std::nullptr_t,
                                          type>()) // might happen: the 'ignore' parameters proceed that way
        {
            stream << 0;
        } else
        {
            std::cerr << "Type not handled: " << ::MoReFEM::GetTypeName<type>() << std::endl;
            assert(false && "Case not foreseen...");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_PRINT_POLICY_x_LUA_FORMAT_HXX_
