### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/ConvertEntryToString/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ExtractLeaf/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ManualParsing/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/PrintPolicy/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Subtuple/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TupleIteration/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Wrapper/SourceList.cmake)
