/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Dec 2016 15:47:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_LEAF_x_EXTRACT_LEAF_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_LEAF_x_EXTRACT_LEAF_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"


// IWYU pragma: private, include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            // clang-format off
            template
            <
                class LeafNameT,
                class DerivedT,
                class EnclosingSectionT,
                CountAsUsed CountAsUsedT
            >
            // clang-format on
            decltype(auto)
            ExtractLeaf(const Utilities::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
            {
                const auto& section_content_tuple = section.GetSectionContent();

                using tuple_type = std::decay_t<decltype(section_content_tuple)>;

                const auto& leaf =
                    std::get<Utilities::Tuple::IndexOf<LeafNameT, tuple_type>::value>(section_content_tuple);

                if (CountAsUsedT == CountAsUsed::yes)
                    leaf.SetAsUsed();

                return leaf.GetTheValue();
            }


            // clang-format off
            template
            <
                class LeafNameT,
                class DerivedT,
                class EnclosingSectionT,
                CountAsUsed CountAsUsedT
            >
            // clang-format on
            std::string
            ExtractPathLeaf(const Utilities::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
            {
                auto path_read = ExtractLeaf<LeafNameT>(section);
                decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);

                return environment.SubstituteValues(path_read);
            }


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_LEAF_x_EXTRACT_LEAF_HXX_
