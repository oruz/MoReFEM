//! \file
//
//
//  LuaState.hxx
//  MoReFEM
//
//  Created by sebastien on 11/09/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/Wrapper/LuaState.hpp"


#include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    inline const std::string& LuaState::GetString() const noexcept
    {
        return content_;
    }


    inline lua_State* LuaState::GetInternal() const noexcept
    {
        return state_;
    }


} // namespace MoReFEM::Internal::LuaNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HXX_
