//! \file
//
//
//  LuaState.cpp
//  MoReFEM
//
//  Created by sebastien on 11/09/2019.
// Copyright © 2019 Inria. All rights reserved.
//


#include <sstream>

#include <cstddef> // IWYU pragma: keep
#include <new>
#include <string>

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"
#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/InputData/Internal/Wrapper/LuaState.hpp"
#include "Utilities/Warnings/Pragma.hpp"


namespace MoReFEM::Internal::LuaNS
{


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content);


    } // namespace


    LuaState::~LuaState()
    {
        if (state_ != nullptr) // the condition is not trivial: see the move assignment operator for instance to
                               // convince yourself...
            lua_close(state_);
    }


    LuaState::LuaState() : state_(luaL_newstate())
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp" // IWYU pragma: keep
        if (state_ == NULL)
            throw std::bad_alloc();
        PRAGMA_DIAGNOSTIC(pop)
    }


    LuaState::LuaState(const std::string& content) : LuaState()
    {
        content_ = content;
        OpenState(state_, content_);
    }


    LuaState::LuaState(const LuaState& rhs) : LuaState()
    {
        content_ = rhs.content_;
        OpenState(state_, content_);
    }


    LuaState::LuaState(LuaState&& rhs) : LuaState()
    {
        content_ = rhs.content_;

        OpenState(state_, rhs.content_);

        if (rhs.state_ != nullptr)
        {
            lua_close(rhs.state_);
            rhs.state_ = nullptr;
        }

        rhs.content_.clear();
    }


    LuaState& LuaState::operator=(const LuaState& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            content_ = rhs.content_;
            OpenState(state_, content_);
        }

        return *this;
    }


    LuaState& LuaState::operator=(LuaState&& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            OpenState(state_, rhs.content_);
            content_ = rhs.content_;

            if (rhs.state_ != nullptr)
            {
                lua_close(rhs.state_);
                rhs.state_ = nullptr;
            }

            rhs.content_ = "";
        }

        return *this;
    }


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content)
        {
            luaL_openlibs(state);

            if (!content.empty())
            {
                std::ostringstream oconv;
                oconv << "f = " << content; // function is arbitrarily called f.

                if (luaL_dostring(state, oconv.str().c_str()))
                    throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                                    "it was: \n"
                                        + content + "\n",
                                    __FILE__,
                                    __LINE__);
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::LuaNS
