//! \file
//
//
//  LuaState.hpp
//  MoReFEM
//
//  Created by sebastien on 11/09/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HPP_

#include <iosfwd>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    /*!
     * \brief An internal class to handle the underlying \a lua_State attribute in \a LuaFunction template class.
     *
     * MoReFEM needs to be able to somehow copy the Lua state because:
     * - Each LuaFunction holds its own Lua stack.
     * - Due to the complex construction of the \a InputData (especially so due to the fact most if not all
     * \a LuaFunction object occur in std::variant so that they are an option among others).
     *
     * What we do in Lua remains extremely simple: we initialize properly the state, and perform some operations on
     * Lua stack when operator() is called. However, there doesn't seem to be easy way to copy the stack. We solve
     * this by... not copying it!
     *
     * Each time this object is copied or moved, the new copy is initialized properly with a blank state; only the
     * string which contains the expression of the Lua function is kept.
     */
    class LuaState
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LuaState;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        //! Default constructor.
        explicit LuaState();

        /*!
         * \brief Constructor with content.
         *
         * \param[in]  content String with the definition of the function in Lua C API.
         *
         * For instance:
         \verbatim
         function (x, y, z)
         return 3. * x + y - z
         end
         \endverbatim
         */
        explicit LuaState(const std::string& content);

        //! Destructor.
        ~LuaState();

        /*!
         * \class doxygen_hide_lua_state_copy_move
         *
         * \attention Not an usual copy/move operation: the internal Lua state is not copied/moved (and we don't care!)
         * See class comment for a more detailed explanation.
         */


        //! \copydoc doxygen_hide_copy_constructor
        //! \copydoc doxygen_hide_lua_state_copy_move
        LuaState(const LuaState& rhs);

        //! \copydoc doxygen_hide_move_constructor
        //! \copydoc doxygen_hide_lua_state_copy_move
        LuaState(LuaState&& rhs);

        //! \copydoc doxygen_hide_copy_affectation
        //! \copydoc doxygen_hide_lua_state_copy_move
        LuaState& operator=(const LuaState& rhs);

        //! \copydoc doxygen_hide_move_affectation
        //! \copydoc doxygen_hide_lua_state_copy_move
        LuaState& operator=(LuaState&& rhs);

        ///@}

      public:
        /*!
         * \brief String with the definition of the function in Lua C API.
         *
         * For instance:
         \verbatim
         function (x, y, z)
         return 3. * x + y - z
         end
         \endverbatim
         */
        const std::string& GetString() const noexcept;

        //! Get the internal Lua state.
        lua_State* GetInternal() const noexcept;

      private:
        //! Encapsulated lua state.
        lua_State* state_ = nullptr;

        /*!
         * \brief String with the definition of the function in Lua C API.
         *
         * For instance:
         \verbatim
         function (x, y, z)
            return 3. * x + y - z
         end
         \endverbatim
         */
        std::string content_ = "";
    };


} // namespace MoReFEM::Internal::LuaNS


#include "Utilities/InputData/Internal/Wrapper/LuaState.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRAPPER_x_LUA_STATE_HPP_
