/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/Subtuple/Subtuple.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            template<class SubTupleT, std::size_t Index, std::size_t Length, class TupleT>
            void ThroughSubtuple<SubTupleT, Index, Length, TupleT>::EnsureSameLength(const TupleT& tuple,
                                                                                     std::size_t length)
            {
                static_assert(index_in_tuple != -1, "An element of the sub-tuple couldn't be found in the tuple.");


                if (length != std::get<index_in_tuple>(tuple).GetTheValue().size())
                    throw Utilities::InputDataNS::ExceptionNS::MismatchedVectorSize<SubTupleT>(__FILE__, __LINE__);

                ThroughSubtuple<SubTupleT, Index + 1, Length, TupleT>::EnsureSameLength(tuple, length);
            }


            template<class SubTupleT, std::size_t Length, class TupleT>
            void ThroughSubtuple<SubTupleT, 0, Length, TupleT>::EnsureSameLength(const TupleT& tuple,
                                                                                 std::size_t& length)
            {
                length = std::get<index_in_Tuple>(tuple).GetTheValue().size();
                ThroughSubtuple<SubTupleT, 1, Length, TupleT>::EnsureSameLength(tuple, length);
            }


            template<class SubTupleT, std::size_t Length, class TupleT>
            void ThroughSubtuple<SubTupleT, Length, Length, TupleT>::EnsureSameLength(const TupleT&, std::size_t)
            {
                // Do nothing.
            }


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HXX_
