### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Enum.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Base.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Base.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Extract.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Extract.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/RewriteInputDataFile.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/RewriteInputDataFile.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
