/*!
 */


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_

// IWYU pragma: private, include "Utilities/InputData/LuaFunction.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            template<typename ReturnTypeT, typename... Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction()
            { }


            template<typename ReturnTypeT, typename... Args>
            LuaFunction<ReturnTypeT(Args...)>::LuaFunction(const std::string& content)
            : state_(Internal::LuaNS::LuaState(content))
            {
                decltype(auto) state = GetInternalState();

                std::ostringstream oconv;
                oconv << "f = " << content; // function is arbitrarily called f.

                if (luaL_dostring(state, oconv.str().c_str()))
                    throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                                    "it was: \n"
                                        + content + "\n",
                                    __FILE__,
                                    __LINE__);
            }


            template<typename ReturnTypeT, typename... Args>
            inline ReturnTypeT LuaFunction<ReturnTypeT(Args...)>::operator()(Args... args) const
            {
                decltype(auto) state = GetInternalState();

                if (state == nullptr)
                {
                    assert(state_.GetString() == "");
                    std::ostringstream oconv;
                    oconv << "You are attempting to compute the value of a Lua function which was not properly "
                             "initialized.";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                Internal::LuaNS::PutOnStack(state, "f");

                Internal::LuaNS::PushOnStack(state, std::forward_as_tuple(args...));

                //  int n = lua_gettop(state_);

                const auto size = static_cast<int>(sizeof...(args));

                if (lua_pcall(state, size, LUA_MULTRET, 0) != 0)
                {
                    std::ostringstream oconv;
                    oconv << "Failure while trying to compute the function with Lua. FYI the Lua stack was at the time "
                             "of the call: \n";
                    Internal::LuaNS::LuaStackDump(state, oconv);

                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                /* retrieve result */
                if (!lua_isnumber(state, -1))
                    throw Exception("Function `f' must return a number!", __FILE__, __LINE__);

                ReturnTypeT ret = Internal::LuaNS::PullFromStack<ReturnTypeT>(state, -1);

                lua_pop(state, 1);

                return ret;
            }


            template<typename ReturnTypeT, typename... Args>
            inline const std::string& LuaFunction<ReturnTypeT(Args...)>::GetString() const noexcept
            {
                return state_.GetString();
            }


            template<typename ReturnTypeT, typename... Args>
            inline lua_State* LuaFunction<ReturnTypeT(Args...)>::GetInternalState() const noexcept
            {
                return state_.GetInternal();
            }


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HXX_
