/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Aug 2015 15:24:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HPP_

#include <string>

#include "Utilities/InputData/Enum.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace Crtp
            {


                /*!
                 * \brief Any section in the input data file list tuple should derive from this.
                 *
                 * \tparam EnclosingSectionT Type of the enclosing section. Choose NoEnclosingSection if the section is
                 * at root level.
                 */
                template<class DerivedT, class EnclosingSectionT>
                struct Section
                {

                    //! Specifies the nature is 'section'.
                    static constexpr Nature GetNature() noexcept;


                    /*!
                     * \brief Full name of the section, including names of enclosing section(s) if relevant.
                     *
                     * Separator is a '.', so a section might be for instance 'Foo.Bar'.
                     *
                     * \return Full name of the section.
                     */
                    static const std::string& GetFullName();


                    /*!
                     * \brief Identifier is an alias to GetFullName()
                     *
                     * \internal <b><tt>[internal]</tt></b> Both are necessary as included struct get their own
                     * 'Identifier()', whereas they do not define any GetName().
                     * \endinternal
                     *
                     * \return Full name of the section.
                     */
                    static const std::string& GetIdentifier() noexcept;


                    /*!
                     * \brief Accessor to the tuple that lists the content of the section.
                     *
                     * \internal <b><tt>[internal]</tt></b> auto is used on purpose here: CRTP can't read a type from
                     * DerivedT.
                     * \endinternal
                     *
                     * \return Tuple that lists the content of the section.
                     */
                    const auto& GetSectionContent() const noexcept;

                    /*!
                     * \brief Non constant accessor to the tuple that lists the content of the section.
                     *
                     * \internal <b><tt>[internal]</tt></b> auto is used on purpose here: CRTP can't read a type from
                     * DerivedT. \endinternal
                     *
                     * \return Tuple that lists the content of the section.
                     */
                    auto& GetNonCstSectionContent() noexcept;
                };


            } // namespace Crtp


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Crtp/Section.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HPP_
