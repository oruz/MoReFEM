/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Aug 2015 15:24:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HXX_

// IWYU pragma: private, include "Utilities/InputData/Crtp/Section.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace Crtp
            {


                template<class DerivedT, class EnclosingSectionT>
                constexpr Nature Section<DerivedT, EnclosingSectionT>::GetNature() noexcept
                {
                    return Nature::section;
                }


                template<class DerivedT, class EnclosingSectionT>
                const std::string& Section<DerivedT, EnclosingSectionT>::GetIdentifier() noexcept
                {
                    return DerivedT::GetName();
                }


                namespace Impl
                {


                    template<class DerivedT, class EnclosingSectionT>
                    struct GenerateFullSectionName
                    {
                        static std::string Perform();
                    };


                    template<class DerivedT>
                    struct GenerateFullSectionName<DerivedT, NoEnclosingSection>
                    {
                        static std::string Perform();
                    };


                    template<class DerivedT>
                    std::string GenerateFullSectionName<DerivedT, NoEnclosingSection>::Perform()
                    {
                        return DerivedT::GetName();
                    };


                    template<class DerivedT, class EnclosingSectionT>
                    std::string GenerateFullSectionName<DerivedT, EnclosingSectionT>::Perform()
                    {
                        return EnclosingSectionT::GetFullName() + "." + DerivedT::GetName();
                    };


                } // namespace Impl


                template<class DerivedT, class EnclosingSectionT>
                const std::string& Section<DerivedT, EnclosingSectionT>::GetFullName()
                {
                    static std::string ret = Impl::GenerateFullSectionName<DerivedT, EnclosingSectionT>::Perform();

                    return ret;
                }


                template<class DerivedT, class EnclosingSectionT>
                const auto& Section<DerivedT, EnclosingSectionT>::GetSectionContent() const noexcept
                {
                    return static_cast<const DerivedT&>(*this).section_content_;
                }


                template<class DerivedT, class EnclosingSectionT>
                auto& Section<DerivedT, EnclosingSectionT>::GetNonCstSectionContent() noexcept
                {
                    return static_cast<DerivedT&>(*this).section_content_;
                }


            } // namespace Crtp


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_SECTION_HXX_
