/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Aug 2013 10:53:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HXX_

// IWYU pragma: private, include "Utilities/InputData/Crtp/Data.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace Crtp
            {


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                const std::string& InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::GetEnclosingSection()
                {
                    static std::string ret = std::is_same<EnclosingSectionT, NoEnclosingSection>()
                                                 ? ""
                                                 : EnclosingSectionT::GetFullName() + ".";
                    return ret;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                const std::string& InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::GetIdentifier()
                {
                    static std::string ret = GetEnclosingSection() + DerivedT::NameInFile();
                    return ret;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                void InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::SetValue(return_type value)
                {
                    value_ = std::move(value);
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                constexpr Nature InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::GetNature() noexcept
                {
                    return Nature::leaf;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline typename Utilities::ConstRefOrValue<ReturnTypeT>::type
                InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::GetTheValue() const
                {
                    assert(value_.has_value());
                    return value_.value();
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline bool InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::HasValue() const noexcept
                {
                    return value_.has_value();
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline void InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::SetAsUsed() const noexcept
                {
                    is_used_ = true;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline bool InputData<DerivedT, EnclosingSectionT, ReturnTypeT>::IsUsed() const noexcept
                {
                    return is_used_;
                }


            } // namespace Crtp


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HXX_
