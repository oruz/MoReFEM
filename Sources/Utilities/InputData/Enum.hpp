//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ENUM_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ENUM_HPP_

#include <iosfwd>


namespace MoReFEM::Utilities::InputDataNS
{


    /*!
     * \brief Placeholder class to use as one of the template parameter for sections and end parameters at root level.
     *
     * Should be used as Crtp::Section or Crtp::InputData template argument.
     */
    struct NoEnclosingSection
    {


        //! Returns empty string.
        static const std::string& GetName();

        //! Returns empty string.
        static const std::string& GetFullName();
    };


    //! Enum to tell whether current item of input parameter list is a section or a leaf.
    enum class Nature
    {
        section = 0,
        leaf
    };


    /*!
     * \brief An enum class that will basically act as a boolean.
     *
     * InputData::Base class takes into account which input parameters are actually used; the
     * way to do so is to set a bit in a bitset when some methods are called. However, sometimes these
     * methods might be called to some purposes that do not truly mean the input parameter is used.
     *
     * For instance, if we check at the beginning of the program that several input vectors are the same
     * length it doesn't mean all of them are actually used, hence the following enum to be able to
     * specify the method not to set the bit for this specific use.
     *
     */
    enum class CountAsUsed
    {
        no,
        yes
    };


    /*!
     * \brief Enum class to indicate whether the Lua file may not match the expected content.
     *
     * When ' yes'  is chosen:
     * - A field found in the file might not match an element of the input data tuple.
     * - A tuple might not be present properly in the Lua file.
     *
     * 'no' is rather obviously the go-to choice for most cases; the only circumstance in which we might want to loosen
     * the constraint is when we run a program that aims to update an existing Lua file.
     *
     */
    enum class allow_invalid_lua_file
    {
        no,
        yes
    };


    /*!
     * \class doxygen_hide_tparam_allow_invalid_lua_file
     *
     * \tparam AllowInvalidLuaFileT The only case in  which  'yes' makes sense is if you're running a program dedicated to
     * update an existing Lua file.
     */


} // namespace MoReFEM::Utilities::InputDataNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ENUM_HPP_
