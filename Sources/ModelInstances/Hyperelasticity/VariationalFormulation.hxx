/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "ModelInstances/Hyperelasticity/VariationalFormulation.hpp"

#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation ::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation::GetMassOperator() const noexcept
        {
            assert(!(!mass_operator_));
            return *mass_operator_;
        }


        inline const VariationalFormulation::StiffnessOperatorType&
        VariationalFormulation::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperator() const noexcept
        {
            assert(!(!surfacic_force_operator_));
            return *surfacic_force_operator_;
        }


        inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
        {
            assert(!(!vector_stiffness_residual_));
            return *vector_stiffness_residual_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorVelocityAtNewtonIteration() const noexcept
        {
            assert(!(!vector_velocity_at_newton_iteration_));
            return *vector_velocity_at_newton_iteration_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorVelocityAtNewtonIteration() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorVelocityAtNewtonIteration());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixMassPerSquareTimeStep() const noexcept
        {
            assert(!(!matrix_mass_per_square_time_step_));
            return *matrix_mass_per_square_time_step_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixMassPerSquareTimeStep() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
        {
            assert(!(!matrix_tangent_stiffness_));
            return *matrix_tangent_stiffness_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForce() const noexcept
        {
            assert(!(!vector_surfacic_force_));
            return *vector_surfacic_force_;
        }

        inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForce() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorSurfacicForce());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
        {
            assert(!(!vector_current_displacement_));
            return *vector_current_displacement_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentVelocity() const noexcept
        {
            assert(!(!vector_current_velocity_));
            return *vector_current_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointPosition() const noexcept
        {
            assert(!(!vector_midpoint_position_));
            return *vector_midpoint_position_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointPosition() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointPosition());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointVelocity() const noexcept
        {
            assert(!(!vector_midpoint_velocity_));
            return *vector_midpoint_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorDiffDisplacement() const noexcept
        {
            assert(!(!vector_diff_displacement_));
            return *vector_diff_displacement_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorDiffDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorDiffDisplacement());
        }


        inline const Solid& VariationalFormulation::GetSolid() const noexcept
        {
            assert(!(!solid_));
            return *solid_;
        }


        inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const noexcept
        {
            return displacement_numbering_subset_;
        }


    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_VARIATIONAL_FORMULATION_HXX_
