/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <map>
#include <ostream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "ModelInstances/Hyperelasticity/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation::VariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& displacement_numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          displacement_numbering_subset_(displacement_numbering_subset)
        {
            assert(time_manager.IsTimeStepConstant() && "Current instantiation relies on this assumption!");
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();

            parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            parent::AllocateSystemVector(displacement_numbering_subset);

            const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
            vector_surfacic_force_ = std::make_unique<GlobalVector>(system_rhs);
            vector_displacement_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
            vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

            vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
            vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
            matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
        }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));

            decltype(auto) domain_full_mesh =
                DomainManager::GetInstance(__FILE__, __LINE__)
                    .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            decltype(auto) input_data = morefem_data.GetInputData();

            solid_ =
                std::make_unique<Solid>(input_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

            DefineStaticOperators(input_data);
        }


        void VariationalFormulation::DefineStaticOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            const auto& felt_space_force = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::force));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;

            hyperelastic_law_parent::Create(GetSolid());

            stiffness_operator_ =
                std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                        displacement_ptr,
                                                        displacement_ptr,
                                                        GetSolid(),
                                                        GetTimeManager(),
                                                        hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                        nullptr,
                                                        nullptr);

            decltype(auto) domain_force = DomainManager::GetInstance(__FILE__, __LINE__)
                                              .GetDomain(EnumUnderlyingType(DomainIndex::force), __FILE__, __LINE__);

            using parameter_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>;

            force_parameter_ = InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type>(
                "Surfacic force", domain_force, input_data);

            if (force_parameter_ != nullptr)
            {
                surfacic_force_operator_ =
                    std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(
                        felt_space_force, displacement_ptr, *force_parameter_);
            }
        }


        void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
        {
            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                break;
            case StaticOrDynamic::dynamic_:
                UpdateDynamicVectorsAndMatrices(evaluation_state);
                break;
            }

            AssembleOperators(evaluation_state);
        }


        void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state)
        {
            //    UpdateDisplacementAtNewtonIteration();

            UpdateVelocityAtNewtonIteration(evaluation_state);

            auto& midpoint_position = GetNonCstVectorMidpointPosition();

            midpoint_position.Copy(GetVectorCurrentDisplacement(), __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., evaluation_state, midpoint_position, __FILE__, __LINE__);

            midpoint_position.Scale(0.5, __FILE__, __LINE__);

            midpoint_position.UpdateGhosts(__FILE__, __LINE__);

            auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();

            midpoint_velocity.Copy(evaluation_state, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity, __FILE__, __LINE__);

            midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);

            midpoint_velocity.UpdateGhosts(__FILE__, __LINE__);

            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state)
        {
            auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

            if (GetSnes().GetSnesIteration(__FILE__, __LINE__) == 0)
            {
                velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity(), __FILE__, __LINE__);
            } else
            {
                const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();

                auto& diff_displacement = GetNonCstVectorDiffDisplacement();

                diff_displacement.Copy(evaluation_state, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement, __FILE__, __LINE__);
                diff_displacement.Scale(2. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
                velocity_at_newton_iteration.Copy(diff_displacement, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(
                    -1., velocity_previous_time_iteration, velocity_at_newton_iteration, __FILE__, __LINE__);
            }

            velocity_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
        {
            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                AssembleNewtonStaticOperators(evaluation_state);
                break;
            case StaticOrDynamic::dynamic_:
                AssembleNewtonDynamicOperators();
                break;
            }
        }


        void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(evaluation_state));
            }

            const std::size_t newton_iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);

            if (newton_iteration == 0ul)
            {
                auto& vector_surfacic_force = GetNonCstVectorSurfacicForce();

                vector_surfacic_force.ZeroEntries(__FILE__, __LINE__);

                GlobalVectorWithCoefficient vec(vector_surfacic_force, 1.);

                const double time = parent::GetTimeManager().GetTime();

                GetSurfacicForceOperator().Assemble(std::make_tuple(std::ref(vec)), time);
            }
        }


        void VariationalFormulation::AssembleNewtonDynamicOperators()
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                const GlobalVector& displacement_vector = GetVectorMidpointPosition();

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(displacement_vector));
            }
        }


        void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
        {
            UpdateVectorsAndMatrices(evaluation_state);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticResidual(residual);
                break;
            case StaticOrDynamic::dynamic_:
                ComputeDynamicResidual(evaluation_state, residual);
                break;
            }

            ApplyEssentialBoundaryCondition(residual);
        }


        void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
        {
            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForce(), residual, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeDynamicResidual(const GlobalVector& evaluation_state,
                                                            GlobalVector& residual)
        {
            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            const auto& time_manager = GetTimeManager();
            const double time_step = time_manager.GetTimeStep();
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            auto& diff_displacement = GetNonCstVectorDiffDisplacement();
            diff_displacement.Copy(evaluation_state, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(-time_step, velocity_previous_time_iteration, diff_displacement, __FILE__, __LINE__);

            Wrappers::Petsc::MatMultAdd(
                GetMatrixMassPerSquareTimeStep(), diff_displacement, residual, residual, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeTangent(const GlobalVector& evaluation_state,
                                                    GlobalMatrix& tangent,
                                                    GlobalMatrix& preconditioner)
        {
            static_cast<void>(evaluation_state);
            static_cast<void>(preconditioner);

            switch (GetTimeManager().GetStaticOrDynamic())
            {
            case StaticOrDynamic::static_:
                ComputeStaticTangent(tangent);
                break;
            case StaticOrDynamic::dynamic_:
                ComputeDynamicTangent(tangent);
                break;
            }

            ApplyEssentialBoundaryCondition(tangent);
        }


        void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& jacobian)
        {
            jacobian.ZeroEntries(__FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), jacobian, __FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeDynamicTangent(GlobalMatrix& jacobian)
        {
            jacobian.ZeroEntries(__FILE__, __LINE__);

#ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixTangentStiffness(), jacobian);
            AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), jacobian);
#endif // NDEBUG

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(
                1., GetMatrixMassPerSquareTimeStep(), jacobian, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(0.5, GetMatrixTangentStiffness(), jacobian, __FILE__, __LINE__);
        }


        void VariationalFormulation::PrepareDynamicRuns()
        {
            DefineDynamicOperators();

            AssembleDynamicOperators();

            UpdateForNextTimeStep();
        }


        void VariationalFormulation::DefineDynamicOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume, displacement_ptr, displacement_ptr);
        }


        void VariationalFormulation::AssembleDynamicOperators()
        {
            const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();

            const double time_step = GetTimeManager().GetTimeStep();

            const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);

            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(), mass_coefficient);

            GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }


        void VariationalFormulation::UpdateForNextTimeStep()
        {
            UpdateDisplacementBetweenTimeStep();

            UpdateVelocityBetweenTimeStep();

            // ComputeGuessForNextTimeStep();
        }


        void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
        {
            GetNonCstVectorCurrentDisplacement().Copy(
                GetSystemSolution(GetDisplacementNumberingSubset()), __FILE__, __LINE__);

            GetNonCstVectorCurrentDisplacement().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::UpdateVelocityBetweenTimeStep()
        {
            GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration(), __FILE__, __LINE__);

            GetNonCstVectorCurrentVelocity().UpdateGhosts(__FILE__, __LINE__);
        }


        void VariationalFormulation::ComputeGuessForNextTimeStep()
        {
            const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
            auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

            Wrappers::Petsc::AXPY(
                GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution, __FILE__, __LINE__);

            system_solution.UpdateGhosts(__FILE__, __LINE__);
        }


    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM
