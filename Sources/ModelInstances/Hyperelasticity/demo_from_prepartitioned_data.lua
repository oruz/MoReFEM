-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {


	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0,


	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,


	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.5

} -- transient

NumberingSubset1 = {


	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
	name = 'displacement',


	-- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a 
	-- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation. 
	-- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a 
	-- displacement. 
	-- Expected format: 'true' or 'false' (without the quote)
	do_move_mesh = false

} -- NumberingSubset1

Unknown1 = {


	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = 'displacement',


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'scalar', 'vectorial'})
	nature = 'vectorial'

} -- Unknown1

Mesh1 = {


	-- Path of the mesh file to use. 
	-- IMPORTANT: if run from preprocessed data, this path won't be used (the already cut mesh will be used 
	-- instead). 
	-- Expected format: "VALUE"
	mesh = '${MOREFEM_ROOT}/Data/Mesh/Bar.mesh',


	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = 'Medit',


	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,


	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1

} -- Mesh1

Domain1 = {


	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_index = { 1 },


	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = {  },


	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },


	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  LuaOptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain1

Domain2 = {


	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_index = { 1 },


	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 3 },


	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },


	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  LuaOptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain2

Domain3 = {


	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_index = { 1 },


	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },


	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 2 },


	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  LuaOptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain3

Domain4 = {


	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_index = { 1 },


	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },


	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },


	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  LuaOptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain4

EssentialBoundaryCondition1 = {


	-- Name of the boundary condition (must be unique).
	-- Expected format: "VALUE"
	name = 'clamped',


	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = 'Comp123',


	-- Name of the unknown addressed by the boundary condition.
	-- Expected format: "VALUE"
	unknown = 'displacement',


	-- Values at each of the relevant component.
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 0, 0, 0 },


	-- Index of the domain onto which essential boundary condition is defined.
	-- Expected format: VALUE
	domain_index = 4,


	-- Whether the values of the boundary condition may vary over time.
	-- Expected format: 'true' or 'false' (without the quote)
	is_mutable = false

} -- EssentialBoundaryCondition1

FiniteElementSpace1 = {


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,


	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 2,


	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = { 'displacement' },


	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' },


	-- List of the numbering subset to use for each unknown;
	-- Expected format: { VALUE1, VALUE2, ...}
	numbering_subset_list = { 1 }

} -- FiniteElementSpace1

FiniteElementSpace2 = {


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,


	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 3,


	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = { 'displacement' },


	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' },


	-- List of the numbering subset to use for each unknown;
	-- Expected format: { VALUE1, VALUE2, ...}
	numbering_subset_list = { 1 }

} -- FiniteElementSpace2

Petsc1 = {


	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-08,


	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,


	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,


	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',


	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-05,


	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-08,


	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'Mumps'

} -- Petsc1

TransientSource1 = {


	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = { 'constant', 'constant', 'constant' },


	-- The values of the vectorial parameter; expected format is a table (opening = '{', closing = '} and 
	-- separator = ',') and each item depends on the nature specified at the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix. 
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 0, 300, 0 }

} -- TransientSource1

Solid = {

	VolumicMass = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',


		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix. 
		-- Expected format: see the variant description...
		value = 1

	}, -- VolumicMass

	HyperelasticBulk = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',


		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix. 
		-- Expected format: see the variant description...
		value = 1750000

	}, -- HyperelasticBulk

	Kappa1 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',


		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix. 
		-- Expected format: see the variant description...
		value = 500

	}, -- Kappa1

	Kappa2 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',


		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix. 
		-- Expected format: see the variant description...
		value = 403346.153846154

	}, -- Kappa2


	-- If the displacement induced by the applied loading is strong enoughit can lead to inverted elements: 
	-- some vertices of the element are movedsuch that the volume of the finite element becomes negative. This 
	-- meansthat the resulting deformed mesh is no longer valid. This parameter enables acomputationally 
	-- expensive test on all quadrature points to check that the volumeof all finite elements remains positive 
	-- throughout the computation. 
	-- Expected format: 'true' or 'false' (without the quote)
	CheckInvertedElements = false

} -- Solid

Parallelism = {


	-- What should be done for a parallel run. There are 4 possibilities: 
	--  'Precompute': Precompute the data for a later parallel run and stop once it's done. 
	--  'ParallelNoWrite': Run the code in parallel without using any pre-processed data and do not write down 
	-- the processed data. 
	--  'Parallel': Run the code in parallel without using any pre-processed data and write down the processed 
	-- data. 
	--  'RunFromPreprocessed': Run the code in parallel using pre-processed data. 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})
	policy = 'RunFromPreprocessed',


	-- Directory in which parallelism data will be written or read (depending on the policy).
	-- Expected format: "VALUE"
	directory = '${MOREFEM_PREPARTITIONED_DATA_DIR}/MidpointHyperelasticity'

} -- Parallelism

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_RESULT_DIR}/MidpointHyperelasticity',


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,


	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

