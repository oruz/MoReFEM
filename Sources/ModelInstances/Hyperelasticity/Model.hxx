/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/Hyperelasticity/Model.hpp"

#include <cassert>
#include <iosfwd>

#include "ModelInstances/Hyperelasticity/VariationalFormulation.hpp"

namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("MidpointHyperelasticity");
            return name;
        }


        inline const VariationalFormulation& Model::GetVariationalFormulation() const
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
        {
            return const_cast<VariationalFormulation&>(GetVariationalFormulation());
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_MODEL_HXX_
