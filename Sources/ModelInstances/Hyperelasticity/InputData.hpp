/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_DATA_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement = 1
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement = 1
        };

        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            full_mesh = 1,
            volume = 2,
            force = 3,
            dirichlet = 4,
        };

        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            clamped = 1
        };

        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            volume = 1,
            force = 2
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::clamped)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::CheckInvertedElements,

            InputDataNS::Parallelism,
            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace MidpointHyperelasticityNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_DATA_HPP_
