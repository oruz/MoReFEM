//! \file
//
//
//  main_update_lua_file.cpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include "Model/Main/MainUpdateLuaFile.hpp"

#include "ModelInstances/Heat/Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    return ModelNS::MainUpdateLuaFile<HeatNS::HeatModel>(argc, argv);
}
