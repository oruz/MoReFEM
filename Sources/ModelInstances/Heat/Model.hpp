/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HPP_

#include "Core/Enum.hpp"                             // IWYU pragma: export
#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export

#include "Model/Model.hpp" // IWYU pragma: export

#include "ModelInstances/Heat/InputData.hpp"              // IWYU pragma: export
#include "ModelInstances/Heat/VariationalFormulation.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace HeatNS
    {


        //! Model that implements the heat problem in static and dynamic (backward Euler for time integration)
        class HeatModel final
        : public MoReFEM::Model<HeatModel, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
        {


          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Alias to self.
            using self = HeatModel;

            //! Convenient alias to parent.
            using parent = MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

          public:
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<HeatModel>;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            explicit HeatModel(const morefem_data_type& morefem_data);

            //! Destructor.
            ~HeatModel() = default;

            //! \copydoc doxygen_hide_copy_constructor
            HeatModel(const HeatModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            HeatModel(HeatModel&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            HeatModel& operator=(const HeatModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            HeatModel& operator=(HeatModel&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize() const;


          private:
            //! Non constant access to the underlying VariationalFormulation object.
            HeatVariationalFormulation& GetNonCstVariationalFormulation();

            //! Access to the underlying VariationalFormulation object.
            const HeatVariationalFormulation& GetVariationalFormulation() const;

          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}


          private:
            //! Underlying variational formulation.
            HeatVariationalFormulation::unique_ptr variational_formulation_;
        };


    } // namespace HeatNS


} // namespace MoReFEM


#include "ModelInstances/Heat/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HPP_
