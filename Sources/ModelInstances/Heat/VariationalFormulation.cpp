/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <functional>
#include <map>
#include <ostream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"

#include "ModelInstances/Heat/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace HeatNS
    {


        HeatVariationalFormulation::~HeatVariationalFormulation() = default;


        HeatVariationalFormulation::HeatVariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          numbering_subset_(numbering_subset)
        { }


        void HeatVariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            using Diffusion = InputDataNS::Diffusion;

            decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                        .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            decltype(auto) input_data = morefem_data.GetInputData();

            diffusion_tensor_ =
                InitScalarParameterFromInputData<Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>>(
                    "Diffusion tensor", domain, input_data);
            transfert_coefficient_ = InitScalarParameterFromInputData<Diffusion::TransfertCoefficient>(
                "Transfert coefficient", domain, input_data);

            density_ = InitScalarParameterFromInputData<Diffusion::Density>("Density", domain, input_data);

            if (!GetDiffusionTensor().IsConstant())
                throw Exception("Current heat model is restricted to a constant diffusion tensor.", __FILE__, __LINE__);

            if (!GetTransfertCoefficient().IsConstant())
                throw Exception(
                    "Current heat model is restricted to a constant transfert coefficient.", __FILE__, __LINE__);

            if (!GetDiffusionDensity().IsConstant())
                throw Exception(
                    "Current heat model is restricted to a constant diffusion density.", __FILE__, __LINE__);

            DefineOperators(input_data);

            if (GetStaticOrDynamic() == StaticOrDynamic::static_case)
                RunStaticCase();
            else
                PrepareDynamicRuns(input_data);
        }


        void HeatVariationalFormulation::RunStaticCase()
        {
            AssembleStaticOperators();
            AssembleTransientOperators();

            const NumberingSubset& numbering_subset = GetNumberingSubset();

            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset,
                                                                                                 numbering_subset);
            SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset, __FILE__, __LINE__);
        }


        void HeatVariationalFormulation::DefineOperators(const InputData& input_data)
        {
            const GodOfDof& god_of_dof = GetGodOfDof();
            const FEltSpace& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const FEltSpace& felt_space_dim_N_minus_1_Neumann =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::neumann));
            const FEltSpace& felt_space_dim_N_minus_1_Robin =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::robin));

            const auto& temperature_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                              .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::temperature));

            decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                        .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;


            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::volumic_source)>;

                volumic_source_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("Volumic source", domain, input_data);

                if (volumic_source_parameter_ != nullptr)
                {
                    volumic_source_operator_ = std::make_unique<source_operator_type>(
                        felt_space_highest_dimension, temperature_ptr, *volumic_source_parameter_);
                }
            } // namespace Utilities::InputDataNS;

            conductivity_operator_ =
                std::make_unique<GVO::GradPhiGradPhi>(felt_space_highest_dimension, temperature_ptr, temperature_ptr);

            if (run_case_ == StaticOrDynamic::dynamic_case)
                capacity_operator_ =
                    std::make_unique<GVO::Mass>(felt_space_highest_dimension, temperature_ptr, temperature_ptr);


            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::neumann_boundary_condition)>;

                neumann_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("Neumann boundary condition", domain, input_data);

                if (neumann_parameter_ != nullptr)
                {
                    neumann_operator_ = std::make_unique<source_operator_type>(
                        felt_space_dim_N_minus_1_Neumann, temperature_ptr, *neumann_parameter_);
                }
            }

            robin_bilinear_part_operator_ =
                std::make_unique<GVO::Mass>(felt_space_dim_N_minus_1_Robin, temperature_ptr, temperature_ptr);

            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::robin_boundary_condition)>;

                robin_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("Robin boundary condition", domain, input_data);

                if (robin_parameter_ != nullptr)
                {
                    robin_linear_part_operator_ = std::make_unique<source_operator_type>(
                        felt_space_dim_N_minus_1_Robin, temperature_ptr, *robin_parameter_);
                }
            }
        }


        void HeatVariationalFormulation::AllocateMatricesAndVectors()
        {
            const NumberingSubset& numbering_subset = GetNumberingSubset();

            parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
            parent::AllocateSystemVector(numbering_subset);

            const GlobalMatrix& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
            const GlobalVector& system_rhs = GetSystemRhs(numbering_subset);

            vector_current_volumic_source_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_neumann_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_robin_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_conductivity_ = std::make_unique<GlobalMatrix>(system_matrix);

            if (run_case_ == StaticOrDynamic::dynamic_case)
                matrix_capacity_ = std::make_unique<GlobalMatrix>(system_matrix);

            matrix_robin_ = std::make_unique<GlobalMatrix>(system_matrix);
        }


        void HeatVariationalFormulation::AssembleStaticOperators()
        {
            const auto& numbering_subset = GetNumberingSubset();

            auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);

            {
                const double transfer_coefficient = GetTransfertCoefficient().GetConstantValue();
                GlobalMatrixWithCoefficient matrix(GetNonCstMatrixRobin(), transfer_coefficient);
                GetRobinBilinearPartOperator().Assemble(std::make_tuple(std::ref(matrix)));
            }

            {
                const double diffusion_tensor = GetDiffusionTensor().GetConstantValue();
                GlobalMatrixWithCoefficient matrix(system_matrix, diffusion_tensor);
                GetConductivityOperator().Assemble(std::make_tuple(std::ref(matrix)));
            }

#ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixRobin(), system_matrix);
#endif // NDEBUG
            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixRobin(), system_matrix, __FILE__, __LINE__);
        }


        void HeatVariationalFormulation::AssembleTransientOperators()
        {
            const double transfert_coefficient = GetTransfertCoefficient().GetConstantValue();
            const double time = GetTimeManager().GetTime();

            // Put all the vectors to zero
            GetNonCstVectorCurrentVolumicSource().ZeroEntries(__FILE__, __LINE__);
            GetNonCstVectorCurrentNeumann().ZeroEntries(__FILE__, __LINE__);
            GetNonCstVectorCurrentRobin().ZeroEntries(__FILE__, __LINE__);

            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentNeumann(), 1.);
                GetNeumannOperator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentRobin(), transfert_coefficient);
                GetRobinLinearPartOperator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            {
                GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentVolumicSource(), 1.);
                GetVolumicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), time);
            }

            GlobalVector& rhs = GetNonCstSystemRhs(GetNumberingSubset());

            rhs.Copy(GetVectorCurrentVolumicSource(), __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(1., GetVectorCurrentNeumann(), rhs, __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., GetVectorCurrentRobin(), rhs, __FILE__, __LINE__);
        }


        void HeatVariationalFormulation::PrepareDynamicRuns(const InputData& input_data)
        {
            // Assemble once and for all the system matrix in dynamic case; intermediate matrices used
            // to compute rhs at each time iteration are also computed there.
            AssembleStaticOperators();
            AssembleCapacityOperator();
            ComputeDynamicMatrices();

            const GodOfDof& god_of_dof = GetGodOfDof();

            {
                const NumberingSubset& numbering_subset = GetNumberingSubset();
                const FEltSpace& felt_space_highest_dimension =
                    god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
                const Unknown& temperature = UnknownManager::GetInstance(__FILE__, __LINE__)
                                                 .GetUnknown(EnumUnderlyingType(UnknownIndex::temperature));

                constexpr auto index = EnumUnderlyingType(InitialConditionIndex::temperature_initial_condition);

                InitializeVectorSystemSolution<index>(
                    input_data, numbering_subset, temperature, felt_space_highest_dimension);
            }
        }


        void HeatVariationalFormulation::AssembleCapacityOperator()
        {
            const double density = GetDiffusionDensity().GetConstantValue();

            const double capacity_coefficient = density / GetTimeManager().GetTimeStep();

            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixCapacityPerTimeStep(), capacity_coefficient);


            GetCapacityOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }


        void HeatVariationalFormulation::ComputeDynamicMatrices()
        {
            const NumberingSubset& numbering_subset = GetNumberingSubset();
            auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);
            const GlobalMatrix& capacity_matrix = GetMatrixCapacityPerTimeStep();

            {
#ifndef NDEBUG
                AssertSameNumberingSubset(capacity_matrix, system_matrix);
#endif // NDEBUG

                Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., capacity_matrix, system_matrix, __FILE__, __LINE__);
            }
        }


        void HeatVariationalFormulation::ComputeDynamicSystemRhs()
        {
            const NumberingSubset& numbering_subset = GetNumberingSubset();

            GetNonCstSystemSolution(numbering_subset).UpdateGhosts(__FILE__, __LINE__);

            AssembleTransientOperators();

            GlobalVector& rhs = GetNonCstSystemRhs(numbering_subset);
            Wrappers::Petsc::MatMultAdd(
                GetMatrixCapacityPerTimeStep(), GetSystemSolution(numbering_subset), rhs, rhs, __FILE__, __LINE__);
        }


        void HeatVariationalFormulation::SetStaticOrDynamic(StaticOrDynamic value)
        {
            run_case_ = value;
        }


    } // namespace HeatNS


} // namespace MoReFEM
