/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HPP_

#include <utility>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "ModelInstances/Heat/InputData.hpp"


namespace MoReFEM
{


    namespace HeatNS
    {


        //! Variational formulaton to solve for a heat problem with Dirichlet, Neumann and Robin BC.
        class HeatVariationalFormulation final
        : public MoReFEM::VariationalFormulation<HeatVariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>
        {
          private:
            //! Alias to the parent class.
            using parent =
                MoReFEM::VariationalFormulation<HeatVariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;


          public:
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<HeatVariationalFormulation>;

            //! Alias to source operator type.
            using source_operator_type = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar>;

            //! Alias to source parameter type.
            using source_parameter_type = ScalarParameter<>;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

          public:
            /// \name Special members.
            ///@{

            //! \copydoc doxygen_hide_varf_constructor
            //! \param[in] numbering_subset The only \a NumberingSubset used in this variational formulation.
            explicit HeatVariationalFormulation(
                const morefem_data_type& morefem_data,
                const NumberingSubset& numbering_subset,
                TimeManager& time_manager,
                const GodOfDof& god_of_dof,
                DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~HeatVariationalFormulation() override;

            //! \copydoc doxygen_hide_copy_constructor
            HeatVariationalFormulation(const HeatVariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            HeatVariationalFormulation(HeatVariationalFormulation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            HeatVariationalFormulation& operator=(const HeatVariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            HeatVariationalFormulation& operator=(HeatVariationalFormulation&& rhs) = delete;

            ///@}


            //! At each time iteration, compute the system Rhs.
            void ComputeDynamicSystemRhs();

            //! Accessor to the capacity matrix per time step.
            const GlobalMatrix& GetMatrixCapacityPerTimeStep() const;


          private:
            //! Run the static case.
            void RunStaticCase();

            /*!
             * \brief Prepare dynamic runs.
             *
             * For instance for dynamic iterations the system matrix is always the same; compute it once and for all
             * here.
             *
             * StaticOrDynamic rhs is what changes between two time iterations, but to compute it the same matrices are
             * used at each time iteration; they are also computed there. \copydoc doxygen_hide_input_data_arg
             */
            void PrepareDynamicRuns(const InputData& input_data);


            /*!
             * \brief Assemble method for all the static operators except the capacity.
             */
            void AssembleStaticOperators();

            /*!
             * \brief Assemble method for all the transient operators (Source, Neumann and Robin).
             */
            void AssembleTransientOperators();

            /*!
             * \brief Assemble method for the capacity operator.
             */
            void AssembleCapacityOperator();


            //! Compute all the matrices required for dynamic calculation.
            void ComputeDynamicMatrices();


          public:
            //! Enum class to decide to run either the static or dynamic problem.
            enum class StaticOrDynamic
            {
                static_case,
                dynamic_case
            };


            //! Getter for the run case.
            StaticOrDynamic GetStaticOrDynamic() const;


            //! Setter for the run case.
            //! \param[in] value Value to be set.
            void SetStaticOrDynamic(StaticOrDynamic value);

            /*!
             * \brief Get the only numbering subset relevant for this VariationalFormulation.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return The only relevant \a NumberingSubset.
             */
            const NumberingSubset& GetNumberingSubset() const;


          private:
            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const morefem_data_type& morefem_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();


            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}


          private:
            /*!
             * \brief Define the properties of all the global variational operators involved.
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            void DefineOperators(const InputData& input_data);

            //! Get the volumic source operator.
            const source_operator_type& GetVolumicSourceOperator() const noexcept;

            //! Get the conductivity operator.
            const GlobalVariationalOperatorNS::GradPhiGradPhi& GetConductivityOperator() const noexcept;

            //! Get the capacity operator.
            const GlobalVariationalOperatorNS::Mass& GetCapacityOperator() const noexcept;

            //! Get the Neumann Operator
            const source_operator_type& GetNeumannOperator() const noexcept;

            //! Get the Robin bilinear part operator.
            const GlobalVariationalOperatorNS::Mass& GetRobinBilinearPartOperator() const noexcept;

            //! Get the Robin linear part operator.
            const source_operator_type& GetRobinLinearPartOperator() const noexcept;

          private:
            /// \name Accessors to vectors and matrices specific to the heat problem.
            ///@{

            //! Accessor.
            const GlobalVector& GetVectorCurrentVolumicSource() const;

            //! Accessor.
            GlobalVector& GetNonCstVectorCurrentVolumicSource();

            //! Accessor.
            const GlobalVector& GetVectorCurrentNeumann() const;

            //! Accessor.
            GlobalVector& GetNonCstVectorCurrentNeumann();

            //! Accessor.
            const GlobalVector& GetVectorCurrentRobin() const;

            //! Accessor.
            GlobalVector& GetNonCstVectorCurrentRobin();

            //! Accessor.
            const GlobalMatrix& GetMatrixConductivity() const;

            //! Accessor.
            GlobalMatrix& GetNonCstMatrixConductivity();

            //! Accessor.
            GlobalMatrix& GetNonCstMatrixCapacityPerTimeStep();

            //! Accessor.
            const GlobalMatrix& GetMatrixRobin() const;

            //! Accessor.
            GlobalMatrix& GetNonCstMatrixRobin();


            ///@}

            /// \name Material parameters.
            ///@{

            //! Diffusion density,
            const ScalarParameter<>& GetDiffusionDensity() const;

            //! Diffusion tranfert coefficient.
            const ScalarParameter<>& GetTransfertCoefficient() const;

            //! Diffusion tensor.
            const ScalarParameter<>& GetDiffusionTensor() const;

            ///@}

          private:
            //! Accessor on the temperature temperature pair.
            const UnknownPair& GetTemperatureTemperaturePair() const noexcept;

          private:
            /// \name Global variational operators.
            ///@{


            //! Conductivity operator.
            GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr conductivity_operator_ = nullptr;

            //! Capacity operator
            GlobalVariationalOperatorNS::Mass::const_unique_ptr capacity_operator_ = nullptr;

            //! Volumic source operator.
            source_operator_type::const_unique_ptr volumic_source_operator_ = nullptr;

            //! Neumann operator.
            source_operator_type::const_unique_ptr neumann_operator_ = nullptr;

            //! Robin bilinear part operator (int(Gamma)(h*u*v)).
            GlobalVariationalOperatorNS::Mass::const_unique_ptr robin_bilinear_part_operator_ = nullptr;

            //! Robin linear part operator (int(Gamma)(h*uc*v)).
            source_operator_type::const_unique_ptr robin_linear_part_operator_ = nullptr;

            ///@}


            /// \name Parameters used to define TransientSource operators.
            ///@{

            //! Volumic source parameter.
            source_parameter_type::unique_ptr volumic_source_parameter_ = nullptr;

            //! Neumann parameter.
            source_parameter_type::unique_ptr neumann_parameter_ = nullptr;

            //! Robin parameter.
            source_parameter_type::unique_ptr robin_parameter_ = nullptr;

            ///@}


          private:
            /// \name Global vectors and matrices specific to the heat problem.
            ///@{

            //! current volumic source vector.
            GlobalVector::unique_ptr vector_current_volumic_source_ = nullptr;

            //! Neumann BC vector.
            GlobalVector::unique_ptr vector_current_neumann_ = nullptr;

            //! Robin BC vector.
            GlobalVector::unique_ptr vector_current_robin_ = nullptr;

            //! Matrix current conductivity.
            GlobalMatrix::unique_ptr matrix_conductivity_ = nullptr;

            //! Current capacity per time matrix.
            GlobalMatrix::unique_ptr matrix_capacity_ = nullptr;

            //! Current Robin matrix.
            GlobalMatrix::unique_ptr matrix_robin_ = nullptr;


            ///@}


          private:
            //! \name Material parameters.
            ///@{

            //! Diffusion tensor.
            ScalarParameter<>::unique_ptr diffusion_tensor_ = nullptr;

            //! Transfer coefficient .
            ScalarParameter<>::unique_ptr transfert_coefficient_ = nullptr;

            //! Diffusion Density.
            ScalarParameter<>::unique_ptr density_ = nullptr;

            ///@}


          private:
            //! Type of case to run read from the input data file.
            StaticOrDynamic run_case_ = StaticOrDynamic::static_case;

            //! Numbering subset covering the displacement in the main finite element space.
            const NumberingSubset& numbering_subset_;
        };


    } // namespace HeatNS


} // namespace MoReFEM


#include "ModelInstances/Heat/VariationalFormulation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HPP_
