add_library(MoReFEM4Elasticity_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Elasticity_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/Model.cpp" / 
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx" /
		"${CMAKE_CURRENT_LIST_DIR}/InputData.hpp" /
 		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx"
)

target_link_libraries(MoReFEM4Elasticity_lib
                      ${ALL_LOAD_BEGIN_FLAG}
                      ${MOREFEM_MODEL}
                      ${ALL_LOAD_END_FLAG})


add_executable(MoReFEM4Elasticity ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Elasticity
                      MoReFEM4Elasticity_lib)

apply_lto_if_supported(MoReFEM4Elasticity)                      


add_executable(MoReFEM4ElasticityUpdateLuaFile ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
target_link_libraries(MoReFEM4ElasticityUpdateLuaFile
                      ${MOREFEM_CORE})


add_executable(MoReFEM4ElasticityEnsightOutput ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
target_link_libraries(MoReFEM4ElasticityEnsightOutput
                      ${MOREFEM_POST_PROCESSING})

morefem_install(MoReFEM4Elasticity MoReFEM4Elasticity_lib MoReFEM4ElasticityEnsightOutput MoReFEM4ElasticityUpdateLuaFile)


add_test(ElasticityModel3D        
         MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3D PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3D-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3D-mpi PROPERTIES TIMEOUT 50)


add_test(ElasticityModel3D-mpi-from-prepartitioned-data
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_from_prepartitioned_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3D-mpi-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2D
         MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2D PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2D-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2D-mpi PROPERTIES TIMEOUT 50)


add_test(ElasticityModel2D-mpi-from-prepartitioned-data
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_from_prepartitioned_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2D-mpi-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Seq/Ascii/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3DEnsightOutput PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput-mpi
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3DEnsightOutput-mpi PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput-mpi-from-prepartitioned-data
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel3DEnsightOutput-mpi-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2DEnsightOutput PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput-mpi
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2DEnsightOutput-mpi PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput-mpi-from-prepartitioned-data
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Ascii
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Ascii)

set_tests_properties(ElasticityModel2DEnsightOutput-mpi-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3D-bin
         MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary.lua
     -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Binary
     -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3D-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3D-mpi-bin
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3D-mpi-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3D-mpi-bin-from-prepartitioned-data
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary_from_prepartitioned_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3D-mpi-bin-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2D-bin
         MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_binary.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel2D-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2D-mpi-bin
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_binary.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel2D-mpi-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2D-mpi-bin-from-prepartitioned-data
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Elasticity
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_binary_from_prepartitioned_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel2D-mpi-bin-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput-bin
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Seq/Binary/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3DEnsightOutput-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput-mpi-bin
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3DEnsightOutput-mpi-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel3DEnsightOutput-mpi-bin-from-prepartitioned-data
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary/Elasticity/3D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3DEnsightOutput-mpi-from-prepartitioned-data PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput-bin
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel2DEnsightOutput-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput-mpi-bin
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel2DEnsightOutput-mpi-bin PROPERTIES TIMEOUT 50)

add_test(ElasticityModel2DEnsightOutput-mpi-bin-from-prepartitioned-data
         MoReFEM4ElasticityEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary/Elasticity/2D/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4_FromPrepartitionedData/Binary
         -e MOREFEM_PREPARTITIONED_DATA_DIR=${MOREFEM_TEST_OUTPUT_DIR}/PrepartitionedData/Binary)

set_tests_properties(ElasticityModel3DEnsightOutput-mpi-bin-from-prepartitioned-data PROPERTIES TIMEOUT 50)

morefem_install(MoReFEM4ElasticityEnsightOutput)

add_executable(MoReFEM4ElasticityCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4ElasticityCheckResults
                       ${MOREFEM_TEST_TOOLS})

add_test(ElasticityCheckResults
         MoReFEM4ElasticityCheckResults
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}
         )

set_tests_properties(ElasticityCheckResults PROPERTIES TIMEOUT 50)
