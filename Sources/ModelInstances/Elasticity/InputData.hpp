/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"


namespace MoReFEM
{


    namespace ElasticityNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            highest_dimension = 1,
            neumann = 2,
            dirichlet,
            full_mesh
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            neumann = 2
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            solid_displacement = 1
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            monolithic = 1
        };


        //! \copydoc doxygen_hide_source_enum
        enum class SourceIndex
        {
            volumic = 1,
            surfacic = 2
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            sole = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::solid_displacement)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Solid::PlaneStressStrain,

            InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic)>,
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>,

            InputDataNS::Parallelism,
            InputDataNS::Result
        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace ElasticityNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_
