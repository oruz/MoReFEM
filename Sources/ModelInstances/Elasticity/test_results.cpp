/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE model_elasticity

#include <iomanip>
#include <sstream>
#include <string_view>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string_view seq_or_par, std::string_view dimension, std::string_view ascii_or_bin);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(seq_3d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_from_prepartitioned_data, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_from_prepartitioned_data, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_bin_from_prepartitioned_data, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_2d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_bin_from_prepartitioned_data, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "2D", "Binary");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string_view seq_or_par, std::string_view dimension, std::string_view ascii_or_bin)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(root_dir));
        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(output_dir));

        std::ostringstream oconv;
        oconv << root_dir << "/Sources/ModelInstances/Elasticity/ExpectedResults/" << ascii_or_bin << '/' << dimension;

        const std::string ref_dir_path = oconv.str();

        oconv.str("");
        oconv << output_dir << '/' << seq_or_par << '/' << ascii_or_bin << "/Elasticity/" << dimension << "/Rank_0";
        const std::string obtained_dir_path = oconv.str();


        FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        FilesystemNS::Directory obtained_dir(obtained_dir_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        if (seq_or_par != "Mpi4_FromPrepartitionedData")
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);


        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            /* BOOST_REQUIRE_NO_THROW */ (
                TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__));

        ref_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);

        /* BOOST_REQUIRE_NO_THROW */ (
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */ (
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__));

        for (auto i = 0ul; i <= 5; ++i)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            /* BOOST_REQUIRE_NO_THROW */ (
                TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__, 1.e-11));
        }
    }


} // namespace
