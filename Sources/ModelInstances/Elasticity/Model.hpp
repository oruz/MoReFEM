/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HPP_

#include "Core/Enum.hpp"                             // IWYU pragma: export
#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export

#include "Model/Model.hpp" // IWYU pragma: export

#include "ModelInstances/Elasticity/InputData.hpp"              // IWYU pragma: export
#include "ModelInstances/Elasticity/VariationalFormulation.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace ElasticityNS
    {


        //! \copydoc doxygen_hide_simple_model
        class ElasticityModel final
        : public Model<ElasticityModel, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>
        {


          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Alias to self.
            using self = ElasticityModel;

            //! Convenient alias to parent.
            using parent = Model<ElasticityModel, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>;

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

            //! Variational formulation.
            using variational_formulation_type = VariationalFormulationElasticity;


          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            ElasticityModel(const morefem_data_type& morefem_data);

            //! Destructor.
            ~ElasticityModel() = default;

            //! \copydoc doxygen_hide_copy_constructor
            ElasticityModel(const ElasticityModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            ElasticityModel(ElasticityModel&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            ElasticityModel& operator=(const ElasticityModel& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            ElasticityModel& operator=(ElasticityModel&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();

            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize() const;


          private:
            //! Non constant access to the underlying VariationalFormulation object.
            variational_formulation_type& GetNonCstVariationalFormulation() noexcept;

            //! Access to the underlying VariationalFormulation object.
            const variational_formulation_type& GetVariationalFormulation() const noexcept;


            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep() const;


            ///@}

          private:
            //! Underlying variational formulation.
            variational_formulation_type::unique_ptr variational_formulation_;
        };


    } // namespace ElasticityNS


} // namespace MoReFEM


#include "ModelInstances/Elasticity/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HPP_
