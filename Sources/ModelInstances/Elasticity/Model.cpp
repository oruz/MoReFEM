/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <type_traits>
#include <vector>

#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/Elasticity/Model.hpp"


namespace MoReFEM
{


    namespace ElasticityNS
    {


        ElasticityModel::ElasticityModel(const morefem_data_type& morefem_data)
        : parent(morefem_data), variational_formulation_(nullptr)
        { }


        void ElasticityModel::SupplInitialize()
        {
            const auto& god_of_dof = this->GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            const auto& numbering_subset_ptr =
                god_of_dof.GetNumberingSubsetPtr(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            const auto& numbering_subset = *numbering_subset_ptr;

            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

                auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr("sole") };

                const auto& main_felt_space =
                    god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
                const auto& neumannn_felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::neumann));
                decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);
                const auto& unknown = unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::solid_displacement));

                variational_formulation_ = std::make_unique<variational_formulation_type>(morefem_data,
                                                                                          main_felt_space,
                                                                                          neumannn_felt_space,
                                                                                          unknown,
                                                                                          numbering_subset,
                                                                                          GetNonCstTimeManager(),
                                                                                          god_of_dof,
                                                                                          std::move(bc_list));
            }

            auto& formulation = this->GetNonCstVariationalFormulation();
            formulation.Init(morefem_data);

            const auto& mpi = this->GetMpi();

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "----------------------------------------------\n", mpi, __FILE__, __LINE__);

            formulation.RunStaticCase();
            formulation.WriteSolution(this->GetTimeManager(), numbering_subset);
            formulation.PrepareDynamicRuns();
        }


        void ElasticityModel::Forward()
        {
            auto& formulation = this->GetNonCstVariationalFormulation();

            // Only Rhs is modified at each time iteration; compute it and solve the system.
            formulation.ComputeDynamicSystemRhs();
            const auto& numbering_subset = formulation.GetNumberingSubset();

            if (GetTimeManager().NtimeModified() == 1)
            {
                formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                    numbering_subset, numbering_subset);
                formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset, __FILE__, __LINE__);
            } else
            {
                formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                                                      numbering_subset);
                formulation.SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset, __FILE__, __LINE__);
            }
        }


        void ElasticityModel::SupplFinalizeStep()
        {
            // Update quantities for next iteration.
            auto& formulation = this->GetNonCstVariationalFormulation();
            const auto& numbering_subset = formulation.GetNumberingSubset();

            formulation.WriteSolution(this->GetTimeManager(), numbering_subset);
            formulation.UpdateDisplacementAndVelocity();
        }


        void ElasticityModel::SupplFinalize() const
        { }


        void ElasticityModel::SupplInitializeStep() const
        { }


    } // namespace ElasticityNS


} // namespace MoReFEM
