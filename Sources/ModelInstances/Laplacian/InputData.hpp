/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_DATA_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };

        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            monolithic = 1
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            pressure = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            full_mesh = 1,
            volume = 2,
            dirichlet = 3
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            first = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            volume = 1
        };

        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class SourceIndexList : std::size_t
        {
            volumic_source = 1
        };


        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            initial_condition = 1
        };

        //! \copydoc doxygen_hide_input_data_tuple
        // clang-format off
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>,

            InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>,

            InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::initial_condition)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result

        >;
        // clang-format on


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace LaplacianNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_INPUT_DATA_HPP_
