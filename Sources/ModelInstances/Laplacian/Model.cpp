/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 22 Nov 2017 13:30:46 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <type_traits>

#include "Utilities/InputData/Extract.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "ModelInstances/Laplacian/Model.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace LaplacianNS
    {


        Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            const GodOfDof& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            const NumberingSubset& numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

                auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr("dirichlet") };

                variational_formulation_ = std::make_unique<VariationalFormulation>(
                    morefem_data, GetNonCstTimeManager(), god_of_dof, std::move(bc_list));
            }

            auto& variational_formulation = GetNonCstVariationalFormulation();

            variational_formulation.Init(morefem_data);
            variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize() const
        { }


    } // namespace LaplacianNS


} // namespace MoReFEM
