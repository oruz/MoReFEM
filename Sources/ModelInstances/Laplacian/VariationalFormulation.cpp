/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 22 Nov 2017 13:30:46 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ModelInstances/Laplacian/VariationalFormulation.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        VariationalFormulation::~VariationalFormulation() = default;


        VariationalFormulation ::VariationalFormulation(
            const morefem_data_type& morefem_data,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list))
        { }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            decltype(auto) input_data = morefem_data.GetInputData();
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            const auto& pressure =
                UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknown(EnumUnderlyingType(UnknownIndex::pressure));

            constexpr auto initial_condition_index = EnumUnderlyingType(InitialConditionIndex::initial_condition);

            InitializeVectorSystemSolution<initial_condition_index>(
                input_data, monolithic_numbering_subset, pressure, felt_space_volume);

            DefineOperators(input_data);
            AssembleStaticOperators();

            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
            auto& time_keeper = TimeKeep::GetInstance(__FILE__, __LINE__);
            time_keeper.PrintTimeElapsed("Before solving with factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

            if (GetMpi().IsRootProcessor())
                std::cout << "Before solving with factorization." << std::endl;

            SolveLinear<IsFactorized::no>(monolithic_numbering_subset, monolithic_numbering_subset, __FILE__, __LINE__);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
            time_keeper.PrintTimeElapsed("After solving with factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

            if (GetMpi().IsRootProcessor())
                std::cout << "After solving with factorization." << std::endl;

            std::size_t Nsolve = 20;

            for (std::size_t i = 0; i < Nsolve; ++i)
            {
                SolveLinear<IsFactorized::yes>(
                    monolithic_numbering_subset, monolithic_numbering_subset, __FILE__, __LINE__);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("After solving without factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP
            }
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            parent::AllocateSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            parent::AllocateSystemVector(monolithic_numbering_subset);
        }


        void VariationalFormulation::DefineOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));

            decltype(auto) volume_domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                               .GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);

            const auto& pressure = UnknownManager::GetInstance(__FILE__, __LINE__)
                                       .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));

            namespace GVO = GlobalVariationalOperatorNS;

            grad_grad_operator_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume, pressure, pressure);

            namespace IPL = Utilities::InputDataNS;


            {
                using parameter_type =
                    InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>;

                volumic_source_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("Volumic source", volume_domain, input_data);

                if (volumic_source_parameter_ != nullptr)
                {
                    volumic_source_operator_ =
                        std::make_unique<source_operator_type>(felt_space_volume, pressure, *volumic_source_parameter_);
                }
            } // namespace Utilities::InputDataNS;
        }


        void VariationalFormulation::AssembleStaticOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            auto& system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
            auto& time_keeper = TimeKeep::GetInstance(__FILE__, __LINE__);
#endif // MOREFEM_EXTENDED_TIME_KEEP

            {
                GlobalMatrixWithCoefficient matrix(system_matrix, 1.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("Before assembling laplacian operator.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                if (GetMpi().IsRootProcessor())
                    std::cout << "Before assembling laplacian operator." << std::endl;

                GetGradGradOperator().Assemble(std::make_tuple(std::ref(matrix)));
#ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("After assembling laplacian operator.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                if (GetMpi().IsRootProcessor())
                    std::cout << "After assembling laplacian operator." << std::endl;
            }

            if (volumic_source_parameter_ != nullptr)
            {
                auto& rhs = GetNonCstSystemRhs(monolithic_numbering_subset);

                {
                    GlobalVectorWithCoefficient vector(rhs, 1.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                    time_keeper.PrintTimeElapsed("Before assembling rhs.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                    if (GetMpi().IsRootProcessor())
                        std::cout << "Before assembling rhs." << std::endl;

                    GetVolumicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), 0.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                    time_keeper.PrintTimeElapsed("After assembling rhs.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                    if (GetMpi().IsRootProcessor())
                        std::cout << "After assembling rhs." << std::endl;
                }
            }
        }


    } // namespace LaplacianNS


} // namespace MoReFEM
