/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 10:45:44 +0100
// Copyright (c) Inria. All rights reserved.
//
*/
#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"
#include "ModelInstances/Laplacian/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::LaplacianNS;


int main(int argc, char** argv)
{
    std::vector<std::size_t> numbering_subset_id_list{ EnumUnderlyingType(NumberingSubsetIndex::monolithic) };

    std::vector<std::string> unknown_list{ "pressure" };

    return ModelNS::MainEnsightOutput<LaplacianNS::InputData>(
        argc, argv, EnumUnderlyingType(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
