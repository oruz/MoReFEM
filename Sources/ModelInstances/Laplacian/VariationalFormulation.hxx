/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 22 Nov 2017 13:30:46 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "ModelInstances/Laplacian/VariationalFormulation.hpp"

#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
        VariationalFormulation::GetGradGradOperator() const noexcept
        {
            assert(!(!grad_grad_operator_));
            return *grad_grad_operator_;
        }


        inline const VariationalFormulation::source_operator_type&
        VariationalFormulation::GetVolumicSourceOperator() const noexcept
        {
            assert(!(!volumic_source_operator_));
            return *volumic_source_operator_;
        }


    } // namespace LaplacianNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HXX_
