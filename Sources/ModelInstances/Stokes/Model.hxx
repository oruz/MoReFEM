/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 4 Mar 2014 18:27:50 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/Stokes/Model.hpp"

#include <cassert>
#include <iosfwd>

#include "ModelInstances/Stokes/Model.hpp"


namespace MoReFEM
{


    namespace StokesNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("Stokes");
            return name;
        }


        inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline const VariationalFormulation& Model::GetVariationalFormulation() const
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return true; // no dynamic run in our very simple test case!
        }


        inline void Model::SupplInitializeStep()
        { }


        inline bool Model::IsMonolithic() const
        {
            return is_monolithic_;
        }


    } // namespace StokesNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HXX_
