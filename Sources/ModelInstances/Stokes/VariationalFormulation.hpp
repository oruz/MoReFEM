/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 13:43:40 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HPP_

#include "Core/LinearAlgebra/GlobalVector.hpp" // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp" // IWYU pragma: export

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"     // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp" // IWYU pragma: export
#else                                                                    // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hpp" // IWYU pragma: export
#endif                                                                   // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp" // IWYU pragma: export
#include "FormulationSolver/VariationalFormulation.hpp"        // IWYU pragma: export

#include "ModelInstances/Stokes/InputData.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace StokesNS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>,
          public Crtp::VolumicAndSurfacicSource<VariationalFormulation,
                                                ParameterNS::Type::vector,
                                                EnumUnderlyingType(SourceIndex::volumic),
                                                EnumUnderlyingType(SourceIndex::surfacic),
                                                ParameterNS::TimeDependencyNS::None>
        {

          private:
            //! Alias to the parent class.
            using parent =
                MoReFEM::VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver)>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;


          public:
            //! Alias to self.
            using self = VariationalFormulation;

            //! Alias to shared pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to the convenient flavor of \a ScalarParameter.
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

          public:
            /// \name Special members.
            ///@{

            //! \copydoc doxygen_hide_varf_constructor
            //! \param[in] velocity_numbering_subset \a NumberingSubset used to keep track of velocity dofs.
            //! \param[in] pressure_numbering_subset \a NumberingSubset used to keep track of pressure dofs. Might be
            //! the same if monolithic resolution is attempted (way less efficient but if you want to...)
            //! \param[in] uzawa_coupling_term ad hoc term if monolithic formulation is used; not used otherwise.
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& velocity_numbering_subset,
                                            const NumberingSubset& pressure_numbering_subset,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                            const double uzawa_coupling_term);

            //! Destructor.
            ~VariationalFormulation() override;

            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulation(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulation(VariationalFormulation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

            ///@}


            //! Run the static case.
            void RunStaticCase();


            /*!
             * \brief Get the numbering subset associated to the velocity variable.
             *
             * Might be the same as the one for pressure for monolithic management.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return \a NumberingSubset in charge of the velocity unknown.
             */
            const NumberingSubset& GetVelocityNumberingSubset() const;

            /*!
             * \brief Get the numbering subset associated to the pressure variable.
             *
             * Might be the same as the one for velocity for monolithic management.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return \a NumberingSubset in charge of the pressure unknown.
             */
            const NumberingSubset& GetPressureNumberingSubset() const;


          private:
            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const morefem_data_type& morefem_data);

            /*!
             * \brief Allocate the global matrices and vectors specific to the considered problem.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}

            /*!
             * \brief Define the properties of all the global variational operators involved.
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            void DefineOperators(const InputData& input_data);


            /// \name Accessors to vectors and matrices specific to the non monolithic Stokes problem.
            ///@{

            //! Get the term in which force is involved in Uzawa scheme.
            const GlobalVector& GetForceVector() const;

            //! Get the term in which force is involved in Uzawa scheme.
            GlobalVector& GetNonCstForceVector();

            //! Get the pressure-related term in new velocity computation.
            const GlobalVector& GetPressureTermInVelocityComputation() const;

            //! Non constant access to the pressure-related term in new velocity computation.
            GlobalVector& GetNonCstPressureTermInVelocityComputation();

            //! Non constant access to the pressure-related term in new velocity computation.
            GlobalVector& GetNonCstPressureIncrement();


            ///@}

            /*!
             * \brief Assemble method relevant for the static case.
             */
            void AssembleStaticCase();

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
            //! Get the Stokes operator.
            const GlobalVariationalOperatorNS::ScalarDivVectorial& GetScalarDivVectorialOperator() const noexcept;

            //! Get the velocity stiffness operator.
            const GlobalVariationalOperatorNS::GradPhiGradPhi& GetVelocityStiffnessOperator() const noexcept;

#else // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

            //! Get the Stokes operator.
            const GlobalVariationalOperatorNS::Stokes& GetStokesOperator() const noexcept;

#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS


            //! Get  the viscosity of the fluid.
            const scalar_parameter& GetFluidViscosity() const;

          private:
            /// \name Method specific to the monolithic case.
            ///@{

            //! Monolithic solve.
            void MonolithicSolve();


            ///@}


          private:
            /// \name Method specific to the non-monolithic case.
            ///@{


            //! Call the Uzawa method.
            void UzawaSolve();

            //! Get the value of the factor involved in the computation of the new pressure.
            double GetCouplingTerm() const;


            //! Compute the new velocity in Uzawa scheme.
            void ComputeNewVelocity();

            //! Compute the new pressure in Uzawa scheme.
            void ComputeNewPressure();


            ///@}


            //! Whether the model is monolithic or not.
            bool IsMonolithic() const;

          private:
            /// \name Global variational operators.

            ///@{


#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
            //! Psi div operator.
            GlobalVariationalOperatorNS::ScalarDivVectorial::const_unique_ptr scalar_div_vectorial_operator_ = nullptr;

            //! Velocity stiffness operator.
            GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr velocity_stiffness_operator_ = nullptr;

#else // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

            //! Stokes operator.
            GlobalVariationalOperatorNS::Stokes::const_unique_ptr stokes_operator_ = nullptr;
#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS


            ///@}


            //! Numbering subset covering the velocity in the main finite element space.
            const NumberingSubset& velocity_numbering_subset_;

            /*!
             * \brief Numbering subset covering the pressure in the main finite element space.
             *
             * Might be the same as the one for velocity (in which case we consider monolithic problem).
             */
            const NumberingSubset& pressure_numbering_subset_;


          private:
            //! Factor involved in the computation of the new pressure.
            const double uzawa_coupling_term_;

            /// \name Data attributes specific to the non-monolithic case.
            ///@{

            //! Vector that intervenes in Uzawa scheme.
            GlobalVector::unique_ptr force_vector_ = nullptr;

            //! Pressure-related term in new velocity computation.
            GlobalVector::unique_ptr pressure_term_in_velocity_computation_ = nullptr;

            //! Helper vector used in the computation of the new pressure.
            GlobalVector::unique_ptr pressure_increment_ = nullptr;

            ///@}


            //! \name Material parameters.
            ///@{

            //! Viscosity of the fluid.
            scalar_parameter::unique_ptr fluid_viscosity_ = nullptr;


            ///@}
        };


    } // namespace StokesNS


} // namespace MoReFEM


#include "ModelInstances/Stokes/VariationalFormulation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HPP_
