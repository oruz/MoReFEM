/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 13:43:40 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "ModelInstances/Stokes/VariationalFormulation.hpp"

#include <cassert>

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"     // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp" // IWYU pragma: export
#else                                                                    // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hpp" // IWYU pragma: export
#endif                                                                   // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

#include "FormulationSolver/Internal/Snes/SnesInterface.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace StokesNS
    {


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        inline const GlobalVariationalOperatorNS::ScalarDivVectorial&
        VariationalFormulation ::GetScalarDivVectorialOperator() const noexcept
        {
            assert(!(!scalar_div_vectorial_operator_));
            return *scalar_div_vectorial_operator_;
        }


        inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
        VariationalFormulation::GetVelocityStiffnessOperator() const noexcept
        {
            assert(!(!velocity_stiffness_operator_));
            return *velocity_stiffness_operator_;
        }


#else // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        inline const GlobalVariationalOperatorNS::Stokes& VariationalFormulation ::GetStokesOperator() const noexcept
        {
            assert(!(!stokes_operator_));
            return *stokes_operator_;
        }

#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        inline const VariationalFormulation::scalar_parameter& VariationalFormulation::GetFluidViscosity() const
        {
            assert(!(!fluid_viscosity_));
            return *fluid_viscosity_;
        }


        inline const NumberingSubset& VariationalFormulation::GetVelocityNumberingSubset() const
        {
            return velocity_numbering_subset_;
        }


        inline const NumberingSubset& VariationalFormulation::GetPressureNumberingSubset() const
        {
            return pressure_numbering_subset_;
        }


        inline double VariationalFormulation::GetCouplingTerm() const
        {
            return uzawa_coupling_term_;
        }


        inline const GlobalVector& VariationalFormulation::GetForceVector() const
        {
            assert(!(!force_vector_));
            return *force_vector_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstForceVector()
        {
            return const_cast<GlobalVector&>(GetForceVector());
        }


        inline const GlobalVector& VariationalFormulation::GetPressureTermInVelocityComputation() const
        {
            assert(!(!pressure_term_in_velocity_computation_));
            return *pressure_term_in_velocity_computation_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstPressureTermInVelocityComputation()
        {
            return const_cast<GlobalVector&>(GetPressureTermInVelocityComputation());
        }


        inline GlobalVector& VariationalFormulation::GetNonCstPressureIncrement()
        {
            assert(!(!pressure_increment_));
            return *pressure_increment_;
        }


        inline bool VariationalFormulation::IsMonolithic() const
        {
            return GetVelocityNumberingSubset() == GetPressureNumberingSubset();
        }


    } // namespace StokesNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_VARIATIONAL_FORMULATION_HXX_
