/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HPP_

#include <iosfwd> // IWYU pragma: export
#include <memory> // IWYU pragma: export
#include <vector> // IWYU pragma: export

#include "Core/Enum.hpp"                             // IWYU pragma: export
#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Model/Model.hpp" // IWYU pragma: export

#include "ModelInstances/Stokes/InputData.hpp"              // IWYU pragma: export
#include "ModelInstances/Stokes/VariationalFormulation.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace StokesNS
    {


        //! \copydoc doxygen_hide_simple_model
        class Model final : public MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>
        {

          private:
            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::no>;

          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_morefem_data_param
             */
            Model(const morefem_data_type& morefem_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            // \internal <b><tt>[internal]</tt></b> noreturn Only because case is restrained to static iteration; remove
            // it when dynamic implemented.
            [[noreturn]] void Forward();

            /*!
             * \brief Initialise a dynamic step.
             *
             * \internal <b><tt>[internal]</tt></b> noreturn Only because case is restrained to static iteration; remove
             * it when dynamic implemented. \endinternal
             */
            [[noreturn]] void SupplFinalizeStep();

            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize() const;


          private:
            //! Non constant access to the underlying VariationalFormulation object.
            VariationalFormulation& GetNonCstVariationalFormulation();

            //! Access to the underlying VariationalFormulation object.
            const VariationalFormulation& GetVariationalFormulation() const;


            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}

            //! Whether the model is monolithic.
            bool IsMonolithic() const;


          private:
            //! Underlying variational formulation.
            VariationalFormulation::unique_ptr variational_formulation_ = nullptr;

            //! Whether the Stokes is monolithic or not.
            bool is_monolithic_ = true;
        };


    } // namespace StokesNS


} // namespace MoReFEM


#include "ModelInstances/Stokes/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_MODEL_HPP_
