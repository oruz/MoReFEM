/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include "Model/Main/Main.hpp"

#include "ModelInstances/Stokes/Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    return MoReFEM::ModelNS::Main<StokesNS::Model>(argc, argv);
}
