/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HPP_

#include <iosfwd> // IWYU pragma: export
#include <memory> // IWYU pragma: export
#include <vector> // IWYU pragma: export

#include "Utilities/Mpi/Mpi.hpp"           // IWYU pragma: export
#include "Utilities/TimeKeep/TimeKeep.hpp" // IWYU pragma: export

#include "Core/Enum.hpp"                             // IWYU pragma: export
#include "Core/InputData/Instances/Crtp/Section.hpp" // IWYU pragma: export

#include "Model/Model.hpp" // IWYU pragma: export

#include "ModelInstances/RivlinCube/InputData.hpp"              // IWYU pragma: export
#include "ModelInstances/RivlinCube/VariationalFormulation.hpp" // IWYU pragma: export


namespace MoReFEM
{


    namespace RivlinCubeNS
    {


        //! \copydoc doxygen_hide_simple_model
        class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
        {

          private:
            //! \copydoc doxygen_hide_alias_self
            using self = Model;

            //! Convenient alias.
            using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

          public:
            //! Return the name of the model.
            static const std::string& ClassName();

            //! Friendship granted to the base class so this one can manipulates private methods.
            friend parent;

          public:
            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            Model(const morefem_data_type& input_data);

            //! Destructor.
            ~Model() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Model(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Model(Model&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Model& operator=(const Model& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Model& operator=(Model&& rhs) = delete;

            ///@}


            /// \name Crtp-required methods.
            ///@{


            /*!
             * \brief Initialise the problem.
             *
             * This initialisation includes the resolution of the static problem.
             */
            void SupplInitialize();


            //! Manage time iteration.
            void Forward();

            /*!
             * \brief Additional operations to finalize a dynamic step.
             *
             * Base class already update the time for next time iterations.
             */
            void SupplFinalizeStep();


            /*!
             * \brief Initialise a dynamic step.
             *
             */
            void SupplFinalize();

          private:
            //! Non constant access to the underlying VariationalFormulation object.
            VariationalFormulation& GetNonCstVariationalFormulation();

            //! Access to the underlying VariationalFormulation object.
            const VariationalFormulation& GetVariationalFormulation() const;


          private:
            //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
            bool SupplHasFinishedConditions() const;


            /*!
             * \brief Part of InitializedStep() specific to Elastic model.
             *
             * As there are none, the body of this method is empty.
             */
            void SupplInitializeStep();


            ///@}

          private:
            //! Underlying variational formulation.
            VariationalFormulation::unique_ptr variational_formulation_;
        };


    } // namespace RivlinCubeNS


} // namespace MoReFEM


#include "ModelInstances/RivlinCube/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HPP_
