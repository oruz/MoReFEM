/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <utility>

#include "Utilities/Mpi/Mpi.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/Solver/Solver.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"

#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/FollowingPressure.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "ModelInstances/RivlinCube/InputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::GlobalVariationalOperatorNS { struct DisplacementTag; }
namespace MoReFEM::Internal::SolverNS { template <class VariationalFormulationT> struct SnesInterface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    namespace RivlinCubeNS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation<VariationalFormulation,
                                                 EnumUnderlyingType(SolverIndex::solver),
                                                 enable_non_linear_solver::yes>,
          public FormulationSolverNS::HyperelasticLaw<VariationalFormulation, HyperelasticLawNS::MooneyRivlin>
        {
          private:
            //! \copydoc doxygen_hide_alias_self
            using self = VariationalFormulation;

            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation<VariationalFormulation,
                                                           EnumUnderlyingType(SolverIndex::solver),
                                                           enable_non_linear_solver::yes>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Alias to hyperelastic parent.
            using hyperelastic_law_parent =
                FormulationSolverNS::HyperelasticLaw<VariationalFormulation, HyperelasticLawNS::MooneyRivlin>;

            //! Alias to hyperelastic law used.
            using hyperelastic_law_type = HyperelasticLawNS::MooneyRivlin;

            //! Alias to hyperelastic policy.
            using hyperelasticity_policy = GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                HyperelasticityPolicyNS ::Hyperelasticity<hyperelastic_law_type>;

            //! Alias to viscoelastic policy.
            using ViscoelasticityPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None;

            //! Alias to acgtive stress policy.
            using InternalVariablePolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None;

            //! Alias to the type of the stiffness operator to use.
            using StiffnessOperatorType = GlobalVariationalOperatorNS::
                SecondPiolaKirchhoffStressTensor<hyperelasticity_policy, ViscoelasticityPolicy, InternalVariablePolicy>;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

            //! Strong type for displacement global vectors.
            using DisplacementGlobalVector =
                StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

            //! Friendship to the class which implements the prototyped functions required by Petsc Snes algorithm.
            friend struct Internal::SolverNS::SnesInterface<self>;

          public:
            //! Class name.
            static const std::string& ClassName();

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

          public:
            /// \name Special members.
            ///@{

            //! \copydoc doxygen_hide_varf_constructor
            //! \param[in] numbering_subset The only \a NumberingSubset used in this variational formulation.
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& numbering_subset,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() override;

            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulation(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulation(VariationalFormulation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

            ///@}

            /*!
             * \brief Get the only numbering subset relevant for this VariationalFormulation.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return Only \a NumberingSubset relevant for the variational formulation.
             */
            const NumberingSubset& GetNumberingSubset() const;

            //! Indicates at the end of the simulation if the tangent was quadratic.
            void WasTangentQuadratic();

          private:
            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const morefem_data_type& morefem_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}


          private:
            /*!
             * \brief Assemble method for all the static operators.
             *
             * \copydoc doxygen_hide_evaluation_state_arg
             */
            void AssembleStaticOperators(const GlobalVector& evaluation_state);


            //! \copydoc doxygen_hide_compute_tangent
            void
            ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);

            //! \copydoc doxygen_hide_compute_residual
            void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

            //! Check if the tangent is quadratic at each newton iteration.
            //! \param[in] rhs Right-hand side vector.
            void TangentCheck(GlobalVector& rhs);

          private:
            /*!
             * \brief Define the properties of all the global variational operators involved.
             */
            void DefineOperators();

            //! Get the hyperelastic stiffness operator.
            const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

            //! Get the following pressure operator.
            const GlobalVariationalOperatorNS::FollowingPressure<ParameterNS::TimeDependencyNS::None>&
            GetFollowingPressureOperator() const noexcept;

          private:
            /// \name Global variational operators.
            ///@{

            //! Stiffness operator.
            StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

            //! Following pressure operator.
            GlobalVariationalOperatorNS::FollowingPressure<ParameterNS::TimeDependencyNS::None>::const_unique_ptr
                following_pressure_operator_ = nullptr;

            ///@}


          private:
            /// \name Accessors to the global vectors and matrices managed by the class.
            ///@{

            //! Accessor.
            const GlobalVector& GetVectorStiffnessResidual() const noexcept;

            //! Accessor.
            GlobalVector& GetNonCstVectorStiffnessResidual();

            //! Accessor.
            const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

            //! Accessor.
            GlobalMatrix& GetNonCstMatrixTangentStiffness();

            //! Accessor.
            const GlobalVector& GetVectorFollowingPressureResidual() const noexcept;

            //! Accessor.
            GlobalVector& GetNonCstVectorFollowingPressureResidual();

            //! Accessor.
            const GlobalMatrix& GetMatrixTangentFollowingPressure() const noexcept;

            //! Accessor.
            GlobalMatrix& GetNonCstMatrixTangentFollowingPressure();

            ///@}

            //! Applied pressure.
            const ScalarParameter<>& GetStaticPressure() const noexcept;

            //! Access to the solid.
            const Solid& GetSolid() const noexcept;

          private:
            //! Constant accessor to the file in which the tangent quadratic verification is printed.
            const std::string& GetTangentQuadraticVerificationFile() const noexcept;

          private:
          private:
            //! Accessor on the displacement displacement pair.
            const UnknownPair& GetDisplacementDisplacementPair() const noexcept;

          private:
            /// \name Global vectors and matrices specific to the problem.
            ///@{

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

            //! Matrix tangent following pressure.
            GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_following_pressure_residual_ = nullptr;

            //! Matrix tangent following pressure.
            GlobalMatrix::unique_ptr matrix_tangent_following_pressure_ = nullptr;

            ///@}

          private:
            /// \name Numbering subsets used in the formulation.
            ///@{

            //! Only relevant \a NumberingSubset
            const NumberingSubset& numbering_subset_;

            ///@}


          private:
            //! Material parameters of the solid.
            Solid::const_unique_ptr solid_ = nullptr;

            //! Applied pressure.
            ScalarParameter<>::unique_ptr static_pressure_ = nullptr;

          private:
            //! Needed to verify if tangent is quadratic.
            double rhs_norm_previous_iter_;

            //! Path to the directory that stores the energy file.
            std::string tangent_quadratic_verification_file_;

            //! Maximum string size of the time.
            int stew_time_size_;

            //! Maximum string size of the iterations.
            int stew_iteration_size_;

            //! Indicates if the tangent was quadratic over the static resolution.
            bool tangent_not_quadratic_in_static_ = false;

            //! Indicates if the tangent was not quadratic at another iteration than the first in the static resolution.
            bool tangent_not_quadratic_at_other_than_first_iteration_in_static_ = false;

            //! Enables to print in the console the warnings at each iterations.
            bool print_tangent_warning_ = false;
        };


    } // namespace RivlinCubeNS


} // namespace MoReFEM


#include "ModelInstances/RivlinCube/VariationalFormulation.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_
