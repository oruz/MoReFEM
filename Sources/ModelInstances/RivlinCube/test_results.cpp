/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#define BOOST_TEST_MODULE model_rivlin_cube

#include <iosfwd>
#include <string>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& geometry);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential_hexahedra, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "Hexahedra");
}


BOOST_FIXTURE_TEST_CASE(mpi4_hexahedra, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "Hexahedra");
}


BOOST_FIXTURE_TEST_CASE(sequential_tetrahedra, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "Tetrahedra");
}


BOOST_FIXTURE_TEST_CASE(mpi4_tetrahedra, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "Tetrahedra");
}

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& geometry)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(root_dir));
        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(output_dir));

        std::string ref_dir_path = root_dir + "/Sources/ModelInstances/RivlinCube/ExpectedResults/" + geometry;
        std::string obtained_dir_path = output_dir + std::string("/") + seq_or_par + std::string("/RivlinCube/")
                                        + geometry + std::string("/Rank_0");

        FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        FilesystemNS::Directory obtained_dir(obtained_dir_path, FilesystemNS::behaviour::read, __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        TestNS::CompareEnsightFiles(ref_dir, obtained_dir, "solid_displacement.00000.scl", __FILE__, __LINE__);
    }


} // namespace
