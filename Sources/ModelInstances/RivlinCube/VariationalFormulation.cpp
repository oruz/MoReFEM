/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iomanip>
#include <map>
#include <ostream>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint/ForUnknownList.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"

#include "ModelInstances/RivlinCube/VariationalFormulation.hpp"

namespace MoReFEM
{


    namespace RivlinCubeNS
    {


        VariationalFormulation::~VariationalFormulation() = default;


        const std::string& VariationalFormulation::ClassName()
        {
            static std::string ret = "Rivlin cube variational formulation";
            return ret;
        }


        VariationalFormulation::VariationalFormulation(
            const morefem_data_type& morefem_data,
            const NumberingSubset& numbering_subset,
            TimeManager& time_manager,
            const GodOfDof& god_of_dof,
            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data, time_manager, god_of_dof, std::move(boundary_condition_list)),
          numbering_subset_(numbering_subset)
        { }


        void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                        .GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            const auto& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));

            decltype(auto) input_data = morefem_data.GetInputData();

            solid_ = std::make_unique<Solid>(
                input_data, domain, felt_space_highest_dimension.GetQuadratureRulePerTopology());

            static_pressure_ = InitScalarParameterFromInputData<InputDataNS::Source::StaticPressure>(
                "StaticPressure", domain, input_data);

            {
                const auto& numbering_subset = GetNumberingSubset();

                const auto& displacement = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknown(EnumUnderlyingType(UnknownIndex::displacement));

                constexpr auto index = EnumUnderlyingType(InitialConditionIndex::displacement_initial_condition);

                InitializeVectorSystemSolution<index>(
                    input_data, numbering_subset, displacement, felt_space_highest_dimension);
            }


            namespace IPL = Utilities::InputDataNS;

            tangent_quadratic_verification_file_ = parent::GetResultDirectory() + "/tangent_quadratic_verification.dat";

            const auto& mpi = GetMpi();

            if (mpi.IsRootProcessor())
            {
                if (FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                {
                    FilesystemNS::File::Remove(GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);
                }

                if (!FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                {
                    std::ofstream stream;
                    FilesystemNS::File::Create(stream, GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);

                    stream << "Time | Newton iteration | Residual norm at current iteration | Previous residual norm "
                              "square\n\n";
                    stream << "===========================================\n";
                    stream << "Static\n";
                    stream << "===========================================\n";

                    namespace ipl = Utilities::InputDataNS;
                    using ip_petsc = InputDataNS::Petsc<1>; //#892 hardcoded.
                    const std::size_t max_iteration = IPL::Extract<typename ip_petsc::MaxIteration>::Value(input_data);

                    std::stringstream temp;
                    temp << max_iteration;
                    stew_iteration_size_ = static_cast<int>(temp.str().size());

                    using TimeManager = InputDataNS::TimeManager;
                    double time_max = IPL::Extract<TimeManager::TimeMax>::Value(input_data);

                    temp.clear();
                    temp.str(std::string());
                    temp << (GetTimeManager().GetTimeStep() + time_max);
                    stew_time_size_ = static_cast<int>(temp.str().size());
                }
            }

            mpi.Barrier();

            DefineOperators();
        }


        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& numbering_subset = GetNumberingSubset();

            parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
            parent::AllocateSystemVector(numbering_subset);

            const auto& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
            const auto& system_rhs = GetSystemRhs(numbering_subset);

            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
            vector_following_pressure_residual_ = std::make_unique<GlobalVector>(system_rhs);

            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
            matrix_tangent_following_pressure_ = std::make_unique<GlobalMatrix>(system_matrix);
        }


        void VariationalFormulation::DefineOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const auto& felt_space_surface_pressure =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_pressure));

            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__)
                                               .GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;

            hyperelastic_law_parent::Create(GetSolid());

            stiffness_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_highest_dimension,
                                                                          displacement_ptr,
                                                                          displacement_ptr,
                                                                          GetSolid(),
                                                                          GetTimeManager(),
                                                                          GetHyperelasticLawPtr());

            following_pressure_operator_ =
                std::make_unique<GVO::FollowingPressure<ParameterNS::TimeDependencyNS::None>>(
                    felt_space_surface_pressure, displacement_ptr, displacement_ptr, GetStaticPressure());
        }


        void VariationalFormulation::AssembleStaticOperators(const GlobalVector& evaluation_state)
        {
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                DisplacementGlobalVector(evaluation_state));
            }

            {
                auto& matrix_tangent_following_pressure = GetNonCstMatrixTangentFollowingPressure();
                auto& vector_following_pressure_residual = GetNonCstVectorFollowingPressureResidual();

                matrix_tangent_following_pressure.ZeroEntries(__FILE__, __LINE__);
                vector_following_pressure_residual.ZeroEntries(__FILE__, __LINE__);

                GlobalMatrixWithCoefficient mat(matrix_tangent_following_pressure, 1.);
                GlobalVectorWithCoefficient vec(vector_following_pressure_residual, 1.);

                GetFollowingPressureOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                        evaluation_state);
            }
        }


        void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
        {
            AssembleStaticOperators(evaluation_state);

            Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY(1., GetVectorFollowingPressureResidual(), residual, __FILE__, __LINE__);

            ApplyEssentialBoundaryCondition(residual);

            TangentCheck(residual);
        }


        void VariationalFormulation::ComputeTangent(const GlobalVector& evaluation_state,
                                                    GlobalMatrix& tangent,
                                                    GlobalMatrix& preconditioner)
        {
            static_cast<void>(evaluation_state);
            static_cast<void>(preconditioner);
            assert("For this model, we are in the most generic case in which tangent is used as preconditioner."
                   && tangent.Internal() == preconditioner.Internal());

#ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
            AssertSameNumberingSubset(GetMatrixTangentFollowingPressure(), tangent);
#endif // NDEBUG

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(
                1., GetMatrixTangentFollowingPressure(), tangent, __FILE__, __LINE__);

            ApplyEssentialBoundaryCondition(tangent);
        }


        void VariationalFormulation::TangentCheck(GlobalVector& rhs)
        {
            // Verifying if the tangent is quadratic.
            const double rhs_norm_iter = rhs.Norm(NORM_2, __FILE__, __LINE__);

            std::size_t iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);

            if (iteration == 0)
            {
                rhs_norm_previous_iter_ = 0;
            }

            if (!(iteration == 0))
            {
                if (rhs_norm_iter > rhs_norm_previous_iter_ * rhs_norm_previous_iter_)
                {
                    tangent_not_quadratic_in_static_ = true;

                    if (iteration > 1)
                        tangent_not_quadratic_at_other_than_first_iteration_in_static_ = true;

                    if (GetMpi().IsRootProcessor())
                    {
                        if (FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                        {
                            std::ofstream inout;
                            FilesystemNS::File::Append(
                                inout, GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);

                            inout << GetTimeManager().GetTime();
                            inout << " | ";
                            inout << std::setw(stew_iteration_size_) << iteration;
                            inout << std::scientific << std::setprecision(12) << " | " << rhs_norm_iter;
                            inout << " | " << rhs_norm_previous_iter_ * rhs_norm_previous_iter_;
                            inout << std::endl;
                        }
                    }

                    GetMpi().Barrier();

                    if (print_tangent_warning_)
                    {
                        Wrappers::Petsc::PrintMessageOnFirstProcessor(
                            "\n===========================================\n", GetMpi(), __FILE__, __LINE__);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor(
                            "[WARNING] Tangent is not quadratic at iteration %d.\n",
                            GetMpi(),
                            __FILE__,
                            __LINE__,
                            iteration);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("Residual norm at current iteration    %1.12e\n",
                                                                      GetMpi(),
                                                                      __FILE__,
                                                                      __LINE__,
                                                                      rhs_norm_iter);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("Previous residual norm square         %1.12e\n",
                                                                      GetMpi(),
                                                                      __FILE__,
                                                                      __LINE__,
                                                                      rhs_norm_previous_iter_
                                                                          * rhs_norm_previous_iter_);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor(
                            "===========================================\n\n", GetMpi(), __FILE__, __LINE__);
                    }
                }
            }

            rhs_norm_previous_iter_ = rhs_norm_iter;
        }

        void VariationalFormulation::WasTangentQuadratic()
        {
            if (tangent_not_quadratic_in_static_)
            {
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "\n===========================================\n", GetMpi(), __FILE__, __LINE__);
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "[WARNING] In the static resolution the tangent was not quadratic.\n",
                    GetMpi(),
                    __FILE__,
                    __LINE__);

                if (tangent_not_quadratic_at_other_than_first_iteration_in_static_)
                {
                    Wrappers::Petsc::PrintMessageOnFirstProcessor(
                        "[WARNING] The tangent was not quadratic at another step than the first one.\n",
                        GetMpi(),
                        __FILE__,
                        __LINE__);
                }

                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "===========================================\n", GetMpi(), __FILE__, __LINE__);
            }

            if (tangent_not_quadratic_in_static_)
            {
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "\n===========================================\n", GetMpi(), __FILE__, __LINE__);
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "[WARNING] Informations about the tangent have been printed in %s\n",
                    GetMpi(),
                    __FILE__,
                    __LINE__,
                    GetTangentQuadraticVerificationFile().c_str());
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "===========================================\n\n", GetMpi(), __FILE__, __LINE__);
            }
        }


    } // namespace RivlinCubeNS


} // namespace MoReFEM
