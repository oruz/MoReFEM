/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 10:45:44 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"
#include "ModelInstances/RivlinCube/Model.hpp"

using namespace MoReFEM;
using namespace MoReFEM::RivlinCubeNS;


int main(int argc, char** argv)
{
    std::vector<std::size_t> numbering_subset_id_list{ EnumUnderlyingType(NumberingSubsetIndex::monolithic) };

    std::vector<std::string> unknown_list{ "solid_displacement" };

    return ModelNS::MainEnsightOutput<RivlinCubeNS::InputData>(
        argc, argv, EnumUnderlyingType(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
