/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <type_traits>

#include "Utilities/InputData/Base.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "ModelInstances/RivlinCube/Model.hpp"


namespace MoReFEM
{


    namespace RivlinCubeNS
    {


        Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            const auto& numbering_subset = god_of_dof.GetNumberingSubset(1);
            decltype(auto) morefem_data = parent::GetMoReFEMData();

            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

                auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr("face1"),
                                   bc_manager.GetDirichletBoundaryConditionPtr("face2"),
                                   bc_manager.GetDirichletBoundaryConditionPtr("face3") };

                variational_formulation_ = std::make_unique<VariationalFormulation>(
                    morefem_data, numbering_subset, GetNonCstTimeManager(), god_of_dof, std::move(bc_list));
            }

            auto& formulation = GetNonCstVariationalFormulation();
            const auto& mpi = GetMpi();

            formulation.Init(morefem_data);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "----------------------------------------------\n", mpi, __FILE__, __LINE__);


            // Solve the system.
            formulation.SolveNonLinear(numbering_subset, numbering_subset, __FILE__, __LINE__);
            formulation.WriteSolution(GetTimeManager(), numbering_subset);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }


        void Model::SupplFinalize()
        {
            auto& formulation = GetNonCstVariationalFormulation();
            formulation.WasTangentQuadratic();
        }


    } // namespace RivlinCubeNS


} // namespace MoReFEM
