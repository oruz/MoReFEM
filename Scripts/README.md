__IMPORTANT:__ Scripts are clearly are aimed at developers: if you're just a MoReFEM user which wants to run an existing model you don't need to look further in this README.

For developers, I have tried to structure them so that you know which ones may be useful for you and which one are only used by the integration manager.

# Important scripts

## init_morefem.py

_Frequency:_ once after the repository was cloned.

This script installs properly the commit hook and help the user set up its gitconfig if none was present.

For macOS users, the script also set up the XCode templates that allows to create an empty shell for different types of MoReFEM file (skeleton of a Model class, of a main, etc...)

## header_guards.py

_Frequency:_ ideally each time a new header file is created, and at least before emitting a merge request.

This script sieves all the header files in the repository and check the header guards are the expected ones (to ensure unicity the header guard is generated from its location in the repository).

It is advised to run this with a clean git state and to check with `git diff` the modifications are relevant: the script is rather brutal (it rewrites the files for which the header guard was not correct).


## generate_cmake_source_list.py

_Frequency:_ each time a new file is added.

This script generates in most of the directories a file named `SourceList.cmake` which is truly a CMakeLists.txt named differently to indicate it was generated automatically.

## For macOS users only

### update_xcode_template.py

_Frequency:_ each time XCode templates have been modified (so not that much).

For macOS users, copy the code templates from the MoReFEM repository in the location XCode expects them. You should run this command each time these templates are modified so that the new version is taken into account.


# Third party tools

## count_number_of_lines.py

A Python script that uses up [cloc](https://github.com/AlDanial/cloc) under the hood to compute number of lines statistics for each module.

## run_cpp_check.py

This script assumes cppcheck is installed and available in your path. This script is a thin wrapper which calls cppcheck with few default options.



# Miscellaneous

## MeshGeneration/1D_Ensight_Mesh.py

A utility to generate a 1D mesh representing a bar wiath arbitrary length and number of discretization points.


## update_markdown_toc.py

_Frequency:_ each time you worked on a Markdown file.

This script updates the table of contents of the markdown files. It assumes markdown-toc is installed on your computer.


## Internal

### commit_hook,py

The (executable) commit hook that is installed by `init_morefem.py` script. 

### FileHeader.py

_Frequency:_ never except for the integration manager.

A rather crude script to add informations in each header files and also the Doxygen groups.