#!/bin/bash

set -e

# Run a script to check connection first.
echo "===================================================================================="
echo "Run a sort of dry-run to check connection to Sonarqube server is alright."
echo "===================================================================================="

bash -c 'Scripts/CI/Sonarqube/check_sonarqube_server.sh'


echo "===================================================================================="
echo "Now run the standard sonar-scanner command."
echo "===================================================================================="

# create the sonarqube config file
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=38a5102208b9b34fb90fdc89358289239488da30

sonar.links.homepage=$CI_PROJECT_URL
sonar.links.scm=$CI_REPOSITORY_URL
sonar.scm.disabled=true
sonar.scm.provider=git

sonar.projectKey=m3disim:morefem:develop
sonar.projectDescription=MoReFEM finite element library
sonar.projectVersion=0.9

sonar.sources=Sources/Utilities,Sources/ThirdParty,Sources/Core,Sources/Geometry,Sources/FiniteElement,Sources/FormulationSolver,Sources/Model,Sources/ModelInstances,Sources/OperatorInstances,Sources/Operators,Sources/ParameterInstances,Sources/Parameters,Sources/PostProcessing

sonar.exclusions=**/test_results.cpp,**/*.py,Sources/ThirdParty/Scripts/cloc-*.pl

sonar.lang.patterns.c++=**/*.cpp,**/*.hpp,**/*.hxx

sonar.cxx.includeDirectories=Sources,/usr/include,/usr/lib64/clang/11/include,/usr/include/c++/v1,/opt/Boost/include,/opt/Libmeshb/include,/opt/Lua/include,/opt/Openblas/include,/opt/Openmpi/include,/opt/Parmetis/include,/opt/Petsc/include,/opt/Xtensor/include,/opt/Tclap/include

sonar.sourceEncoding=UTF-8

sonar.cxx.defines=__x86_64__\nSONARQUBE_4_MOREFEM
sonar.cxx.errorRecoveryEcheck_sonarqube_server.sh nabled=true
sonar.cxx.clangsa.reportPath=build_4_sonarqube/analyzer_reports/*/*.plist
sonar.cxx.cppcheck.reportPath=morefem-cppcheck.xml
sonar.cxx.rats.reportPath=morefem-rats.xml

EOF

echo "sonar-project.properties file written."

# run sonar analysis
STARTTIME=$(date +%s)
sonar-scanner -X 2>&1 | tee sonar.log
ENDTIME=$(date +%s)

echo "Output written in sonar.log; analysis performed in $(($ENDTIME - $STARTTIME)) seconds."

failure_found=`grep "EXECUTION FAILURE" sonar.log | wc -l`

if [ ${failure_found} -ge 1 ]; then
  echo "Execution failure for Sonar scanner."
  exit 1
fi

success_found=`grep "EXECUTION SUCCESS" sonar.log | wc -l`

if [ ${success_found} -ne 1 ]; then
  echo "Please check the sonar scanner log: no success was reported (or the way it is reported has been changed...)."
  exit 1
fi

# Left out in sonar.cxx.includeDirectories to be a tad more faster:
# /opt/Boost/include,/opt/Libmeshb/include,/opt/Lua/include,/opt/Openblas/include,/opt/Openmpi/include,/opt/Parmetis/include,/opt/Petsc/include,/opt/Xtensor/include,/opt/Tclap/include
