import argparse
import os
import sys


class FindWarningInIWYULog():
    """
    Interpret the IWYU log and count how many warnings were found.

    """

    def __init__(self, log):
        """
        @param log The file which contains the result of the ninja command.
        """
        with open(log) as fd:
            self.__file_with_warnings = [ self.__extract_filename(line) for line in fd if self.__extract_filename(line) ]


    def __extract_filename(self, line):
        """
        Investigate a line and see if it mentions a file for which IWYU recommends modifications.
        """
        if "should add these lines" in line:
            return line.split("should add these lines")[0].strip()

        return None


    @property
    def Nwarning(self):
        """Return the total number of warnings found."""
        return len(self.__file_with_warnings)

    def IsPerfect(self):
        """Returns true if no warning were found."""
        if self.__file_with_warnings:
            return False

        return True

    @property
    def ProblematicFileList(self):
        """Return all the files fo which there was an IWYU issue.."""
        return self.__file_with_warnings


class ParseLog():
    """Just a facility to call InterpretLog from command line.

    return The error code to be transmitted to sys.exit().
    """

    def __init__(self, args):
        parser = argparse.ArgumentParser(
            description='Interpret the output of the ninja command tailored for IWYU case (see in ExternalTools/IWYU/README.md for more details).', \
            formatter_class=argparse.RawDescriptionHelpFormatter)

        parser.add_argument(
            '--log',
            required=True,
            help= 'Path to the log file to analyze.'
        )

        parsed_args = parser.parse_args(args)

        result = FindWarningInIWYULog(parsed_args.log)

        if result.IsPerfect():
            self.__error_code = 0
        else:
            print(f"{result.Nwarning} warning(s) were found; files concerned will be written in iwyu_files_to_fix.txt.")
            self.__error_code = os.EX_SOFTWARE

        with open("iwyu_files_to_fix.txt", "w") as fd:
            fd.write("\n".join(result.ProblematicFileList))


    def error_code(self):
        return self.__error_code


if __name__ == "__main__":
    sys.exit(ParseLog(sys.argv[1:]).error_code())
