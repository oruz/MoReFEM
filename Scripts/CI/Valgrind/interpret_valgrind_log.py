import re
import argparse

class Leak:
    """A class which stores how many leaks were found and the amount of memory lost."""
    
    def __init__(self, line):
        """
        param[in] line The line in scrutiny; its expected format is something like:
        ==1371==    definitely lost: 0 bytes in 0 blocks
        """
        print(line)

        assert(line.count("==") == 2)        
        splitted = line.split(":")
        assert(len(splitted) == 2)
        
        numbers = re.findall('(.*) bytes in (.*) blocks', splitted[1])[0]

        # Valgrind use american notation for floating point...
        numbers = [item.replace(",", "") for item in numbers] 
        
        assert(len(numbers) == 2)
        
        (self.__Nbytes, self.__Nblocks) = map(int, numbers)

    @property
    def Nbytes(self):
        return self.__Nbytes

    @property
    def Nblocks(self):
        return self.__Nblocks


class interpret_valgrind_log:
    """Open a file expected to be in the Valgrind format and extract the number of leaks found."""
    
    def __init__(self, log):
        
        self.__definitely_lost = None
        self.__indirectly_lost = None
        self.__possibly_lost = None
        self.__still_reachable = None
        self.__suppressed = None
        
        with open(log, "r") as myfile:
            
            # Skip all lines until the line indicating the sumary is to be printed is met:
            for line in myfile:
                if "LEAK SUMMARY" in line:
                    break
            
            for line in myfile:
                
                if "definitely lost:" in line:
                    assert(self.__definitely_lost is None)
                    self.__definitely_lost = Leak(line)
                if "indirectly lost:" in line:
                    assert(self.__indirectly_lost is None)
                    self.__indirectly_lost = Leak(line)
                if "possibly lost:" in line:
                    assert(self.__possibly_lost is None)
                    self.__possibly_lost = Leak(line)
                if "still reachable:" in line:
                    assert(self.__still_reachable is None)
                    self.__still_reachable = Leak(line)                    
                if "suppressed:" in line and "ERROR SUMMARY" not in line:
                    assert(self.__suppressed is None)
                    self.__suppressed = Leak(line)
                    
        assert(self.__definitely_lost is not None)
        assert(self.__indirectly_lost is not None)
        assert(self.__possibly_lost is not None)
        assert(self.__still_reachable is not None)
        assert(self.__suppressed is not None)
                    
    @property
    def definitely_lost(self):
        return self.__definitely_lost

    @property
    def indirectly_lost(self):
        return self.__indirectly_lost

    @property
    def possibly_lost(self):
        return self.__possibly_lost

    @property
    def still_reachable(self):
        return self.__still_reachable

    @property
    def suppressed(self):
        return self.__suppressed
        
    
    def only_indirectly_lost_and_suppressed(self):
        """Returns True if the only leaks identified are indirectly lost and suppressed ones."""
        if self.definitely_lost.Nbytes != 0:
            return False
        if self.possibly_lost.Nbytes != 0:
            return False
        if self.still_reachable.Nbytes != 0:
            return False
        return True
            
if __name__ == "__main__":
    # For debug purpose only; interpret_valgrind_log is at the moment written to be run from RunValgrind class.
    parser = argparse.ArgumentParser(
            description='Check whether there were leaks identified by Valgrind.',
            formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
            '--log',
            required=True,
            help=
            'Valgrind output to be analysed.'
        )

    args = parser.parse_args()

    interpret_valgrind_log(args.log)



