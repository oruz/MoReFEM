import subprocess
import os
import sys

utilities_abspath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "Utilities")
sys.path.append(utilities_abspath)

from substitute_env_variable import SubstituteEnvVariable


class ComputeCodeStats:
    """
    Use the `cloc` (https://github.com/AlDanial/cloc/releases) program several times to generate statistics
    for each module of MoReFEM.
    
    Consolidated stats are generated at the end, by amalgamating the module reports. There are two versions: one with
    everything and one wihout the tests and model instances.
    """
    
    def __init__(self, output_directory):
        
        '''
        Compute the statistics about MoReFEM.
        \param morefem_src Path to the Sources/ directory of MoReFEM.
        \param output_directory Directory into which output and work files are written.
        '''
        self.__output_directory = SubstituteEnvVariable(output_directory)
    
        if not os.path.exists(self.__output_directory):
            os.makedirs(self.__output_directory)
    
        # MoReFEM root folder is twice removed from the one this script is found.
        self.__morefem_src = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "Sources")
        self.__morefem_src = os.path.normpath(self.__morefem_src)
        
        subdirectory_list = os.listdir(self.__morefem_src)

        subdirectory_list = [ subdir for subdir in subdirectory_list  \
                            if os.path.isdir(os.path.join(self.__morefem_src, subdir))  \
                            and not subdir.startswith("build") \
                            and not subdir.startswith(".") ]
    
        self.__cloc_exec = os.path.join(self.__morefem_src, "ThirdParty", "Scripts", "cloc-1.80.pl")
    
        self.__CreateExcludeFile()
    
        for subdir in subdirectory_list:
           self.__ComputeStatsForSubdir(subdir)
            
        
        # Amalgamate reports from all folders:
        print("======= AMALGAMATE REPORTS =======")
        self.__Amalgamate(subdirectory_list, "reports_all.txt")
        
        # Remove tests and model instances
        subdirectory_list.remove("ModelInstances")
        subdirectory_list.remove("Test")
        
        self.__Amalgamate(subdirectory_list, "reports_no_test_models.txt")

        
        
    def __CreateExcludeFile(self):
        """Create the exclusion file with the paths of all directories to ignore."""
        
        self.__exclude_file = os.path.join(self.__output_directory, "exclude_list.txt")
        
        FILE_exclude = open(self.__exclude_file, 'w')
        
        FILE_exclude.write("{folder}\n".format(folder = os.path.join(self.__morefem_src, "ThirdParty", "Source")))
        FILE_exclude.write("{folder}\n".format(folder = os.path.join(self.__morefem_src, "ThirdParty", "Scripts")))
        
        FILE_exclude.close()
    

    def __ComputeStatsForSubdir(self, subdirectory):
        """
        Compute the stats for the sources in 'subdirectory'; they will be written in a namesake directory of 
        'self.__output_directory'.
        """
        print("======= COMPUTE STATISTICS FOR {} =======".format(subdirectory))
        
        subdir = os.path.join(self.__output_directory, subdirectory)
        
        if not os.path.exists(subdir):
            os.mkdir(subdir)
        
        cmd = (self.__cloc_exec, \
               os.path.join(self.__morefem_src, subdirectory), \
               "--force-lang=C++,hxx", \
               "--force-lang=C++,doxygen", \
               "--force-lang=CMake,cmake", \
               "--exclude-ext=hhdata", \
               "--exclude-ext=scl", \
               '--exclude-list-file', \
                self.__exclude_file, \
                "--fullpath ", \
                "--not-match-d='htmlcov'", \
                '--ignored', \
                os.path.join(subdir, 'ignored.txt'), \
                '--counted', \
                os.path.join(subdir, 'files_considered.txt'), \
                "--report-file ", \
                os.path.join(subdir, 'report.txt'), \
               )
               
        #print(' '.join(cmd))
               
        subprocess.Popen(' '.join(cmd), shell = True).communicate()
        
    def __Amalgamate(self, list_dir, report_name):
        """Amalgamate several report files into one.
        
        \param[in] list_dir List of subdirectories of self.__morefem_src for which we track the reports. 
        Only the subdirectory name is expected, not the full path (e.g. 'Utilities').
        \param[in] report_name Name of the report file. It will be written in self.__output_directory.
        """
        report_list = []
        for subdir in list_dir:
            report_list.append(os.path.join(self.__output_directory, subdir, "report.txt"))
        
        cmd = (self.__cloc_exec, \
               "--sum-reports ", \
               " ".join(report_list), \
               "--report-file ", \
               os.path.join(self.__output_directory, report_name)
               )
            
        #print(' '.join(cmd))
            
        subprocess.Popen(' '.join(cmd), shell = True).communicate()

if __name__ == "__main__":    
    ComputeCodeStats('/tmp/MoReFEM/Stats')
