import os
import sys
import re
import argparse
from collections import Counter


class FindWarningInDoxygenLog():
    """Interpret the Doxygen log and count how many warnings were found.

    Doxygen became more severe after 1.8.13 and the count rose from 0 to thousands; I fixed some that seems pertinent
    (for instance missing function parameters documentation was spot on) but I remove here the return related warnings:
    
    \\code
    //! Return the value.
    double GetValue() const;
    \\endcode
    
    now triggers a warning as \\return is not defined. Fixing it in all the code would take days and is a bit on the 
    nose so I won't do it.
    """

    def __init__(self, log):
        
        self.__content = []
        
        ignore_return_list = ("warning: return type of", \
                              "model_tutorial.md",  # Doxygen tries unsuccessfully to interpret a Markdown link in 
                                                    # README.md; I don't want to fix it as it would broke Gitlab
                                                    # markdown interpreter
                             "argument 'closer' of command", # See #1471 for this one - hopefully may be removed at some point. Beware: it is for Linux VM - don't remove it uness checked properly on CI!                                
                             "Consider increasing DOT_GRAPH_MAX_NODES",
                             "Unexpected tag <dd>" # I am not happy with this one, but once again only seen on Linux...
                             # Maybe not required when Ubuntu will ship more recent Doxygen?
                             )
        
        with open(log) as FILE_in:
    
            for line in FILE_in:
                skip_line = False
                for item in ignore_return_list:
                    if item in line:
                        skip_line = True
                        
                if skip_line:
                    continue
                self.__content.append(line)
        
                
    def Nwarning(self):
        """Return the total number of warnings found."""
        return len(self.__content)
        
        
    def IsPerfect(self):
        """Returns true if no warning were found."""
        print(self.__content)
        
        if self.__content:
            return False
            
        return True
        
        
    @property
    def content(self):
        """Return all the lines related to kept documentation."""
        return self.__content
    
       
            
class ParseLog():
    """Just a facility to call FindWarningInDoxygenLog from command line.
    
    return The error code to be transmitted to sys.exit().
    """
    
    def __init__(self, args):
        parser = argparse.ArgumentParser(
            description='Check whether the Doxygen log given in input contains Doxygen warnings outside of those '
            'deliberately ignored.', \
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        parser.add_argument(
            '--log',
            required=True,
            help= 'Path to the Doxygen log file to analyze.'
        )
        
        parsed_args = parser.parse_args(args)
        
        result = FindWarningInDoxygenLog(parsed_args.log)
        
        if result.IsPerfect():
            self.__error_code = 0
        else:
            print("{} warnings were found and will be written in filtered_doxygen_log.txt.".format(result.Nwarning()))
            self.__error_code = os.EX_SOFTWARE
            
        with open("filtered_doxygen_log.txt", "w") as FILE_out:
            FILE_out.write("\n".join(result.content))
                        
            
    def error_code(self):
        return self.__error_code


if __name__ == "__main__":
    sys.exit(ParseLog(sys.argv[1:]).error_code())
    
            
