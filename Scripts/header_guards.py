import os
import sys
import tempfile

utilities_abspath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "Utilities")
sys.path.append(utilities_abspath)

from substitute_env_variable import SubstituteEnvVariable


class HeaderGuards():

    def __init__(self, project_name, directory, ignore_list):

        self._project_name_in_header_guard = self.GenerateHeaderGuardName(project_name)

        self._directory = SubstituteEnvVariable(directory)

        if not  os.path.exists(self._directory):
            print("[WARNING] Directory {0} not found; couldn't correct header guards inside.".format(self._directory))
        else:
            self.CorrectHeaderGuards(directory, ignore_list)


    def CorrectHeaderGuards(self, directory, ignore_list, header_extension_list = ("hpp", "hxx")):
        """Walk in directory and check all header guards are correct; if not propose a correction (git is there is the correction needs to be cancelled...)

        \param[in] directory Source directory of the code (e.g. 'MoReFEM').
        \param[in] ignore_list Paths that should be ignored while walking within directory. Path might be given absolutely or relatively to directory:
        ignore_list = (os.path.join("ThirdParty", "Source"), ) // Ok
        ignore_list = (os.path.join(directory, "ThirdParty", "Source"), ) // Ok

        \param[in] header_extension_list List of extension that are interpreted as a C++ header; do not specify the dot ('hpp', not '.hpp').
        """

        # First of all, identify whether there is an environment variable and substitute values there.
        length_main_dir = len(self._directory) + 1

        # Do the same for folders to ignore.
        modified_ignore_list = []

        for item in ignore_list:
            item = SubstituteEnvVariable(item)
            modified_ignore_list.append(item)


        for root, dirs, files in os.walk(self._directory):

            skip = False

            for item in modified_ignore_list:
                if root.endswith(item):
                    skip = True
                    dirs[:] = []

            # Skip files that are located within ignored folders.
            if skip:
                continue

            # Work on header files only.
            for myfile in files:
                extension = os.path.splitext(myfile)[1]

                if extension.startswith('.'):
                    extension = extension[1:]

                if extension not in header_extension_list:
                    continue

                self.CheckAndFixHeaderGuard(os.path.join(root[length_main_dir:], myfile))


    def CheckAndFixHeaderGuard(self, myfile):
        """Open header file and check whether header guards are correct or not.

        It is assumed here choice was properly made and it is truly a header file; if not it will fail to convert it and display the issue in an onscreen print.
        """
        header_guard = "{0}x_{1}".format(self._project_name_in_header_guard,
                                       self.GenerateHeaderGuardName(myfile))

        original_file = os.path.join(self._directory, myfile)
        rewritten_file = tempfile.NamedTemporaryFile(mode='w+t', delete = False)

        is_file_modified = False
        is_comment = False

        FILE_in = open(original_file)

        line = ""

        # Read until the # define line is found. It is assumed header guards comes first, before any other macro.
        while 1:
            line = FILE_in.readline()
            line = line.rstrip("\r\n\t ")

            # The first relevant line should be line with the first header guard
            # So line sbefore should be either empty or comments.
            if not line or line.startswith("//"):
                rewritten_file.write("{0}\n".format(line))
                continue

            if line.startswith("/*"):
                rewritten_file.write("{0}\n".format(line))
                is_comment = True
                continue

            # If not in comment it means the line should be the header guard; which
            # is handled just after the while loop
            if not is_comment:
                current_line = line
                break;

            if line.endswith("*/"):
                is_comment = False
                rewritten_file.write("{0}\n".format(line))

        # Analyse
        if not "ifndef" in line:
            print("Error in header guards for {0}: line beginning by ifndef not found at the beginning of the file.".format(myfile))
            return

        expected_line = "#ifndef {0}".format(header_guard)

        rewritten_file.write("{0}\n".format(expected_line))

        if expected_line != line:
            is_file_modified = True

        # Read the file again; skip empty lines and reach the line that must start with '# define'.
        while 1:
            line = FILE_in.readline()
            line = line.rstrip("\r\n\t ")

            if line:
               break

        # Line should be the "# define" one.
        if not "define" in line:
             print("Error in header guards for {0}: line beginning by define (after ifndef)  not found at the beginning of the file.".format(myfile))
             return

        expected_line = "#define {0}".format(header_guard)

        rewritten_file.write("{0}\n".format(expected_line))

        if expected_line != line:
            is_file_modified = True

        line_list = []

        # Only if the file ends with hxx: check it properly indicates to include-what-you-use it's the hpp file that
        # should be included.
        if myfile.endswith("hxx"):

            # Read the file again until a non empty line is found.
            while 1:
                line = FILE_in.readline()
                line = line.rstrip("\r\n\t ")

                if line:
                    break

            expected_line = "// IWYU pragma: private, include \"{}\"".format(myfile.replace("hxx", "hpp"))

            rewritten_file.write("\n{0}\n\n\n".format(expected_line))

            if expected_line != line:
                is_file_modified = True
                line_list.append(line)


        # Read the rest of the file and store it into a list of lines.
        for line in FILE_in:
            line = line.rstrip("\r\n\t ")
            line_list.append(line)

        # Ideally, penultimate line should be the one with #endif // HEADER_GUARD_NAME.
        Nline = len(line_list)

        endif_line_found = False

        index = Nline - 1

        while not endif_line_found:

            line = line_list[index]

            if line:
                if line.startswith("#endif"):
                    endif_line_found = True
                    expected_line = "#endif // {0}".format(header_guard)

                    if expected_line != line:
                        is_file_modified = True
                        line_list[index] = expected_line

            if index == 0:
                print("Error in header guards for {0}: #endif line not found.".format(myfile))
                return

            if not endif_line_found:
                index = index - 1

        # Write the content up to index in the new file, and add a single empty line at the end.
        for i in range(0, index + 1):
            rewritten_file.write("{0}\n".format(line_list[i]))

        # Write on screen whether the include file was modified.
        if is_file_modified:
            print("File {0} was modified for header guards.".format(myfile))

            os.remove(original_file)
            os.rename(rewritten_file.name, original_file)

    def GenerateHeaderGuardName(self, name):
        """Generate a unique name for header guards from the relative location to the source of the project.

        For instance, PictStock -> PICT_STOCK
        """
        assert(name)
        assert(str(name)[0].isalpha())

        ret = [str(name)[0]]

        for current in name[1:]:

            if current.isalpha():
                if current.islower():
                    ret.append(current.upper())
                else:
                    ret.extend(("_", current))
            elif current.isdigit():
                ret.append(current)
            elif current == ".":
                ret.append("_")
            else:
                # Virtually anything else should be converted to '_x'
                ret.append("_x")

        if ret[-1] != "_":
            ret.append("_")

        return "".join(ret)





if __name__ == "__main__":

    directory = "${HOME}/Codes/MoReFEM/CoreLibrary/Sources"
    ignore_list = (os.path.join("Sources", "Utilities", "Warnings", "Internal", "IgnoreWarning"), )

    HeaderGuards("Morefem", directory, ignore_list)
