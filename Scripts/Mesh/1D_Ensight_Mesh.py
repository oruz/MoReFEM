#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Command line below creates the file bar_1D_L1_N10.geo of a mesh of length 1m with 10 points of discretization.
# What is important is that the points are numbered in an order  (the natural one) such that MoReFEM will not renumbered them. 
# It is useful to test 1D Model and get in the hhdata files a solution that you can either load in matlab or 
# plot in pgfplot with ease as no renumbering is due.
# Command line : python 1D_Ensight_Mesh.py bar 1 10 


import numbers
import argparse

parser = argparse.ArgumentParser(
            description='Creates an Ensight file of a 1D bar of customizable length and number of discretiation points.\n'
            'What is important is that the points are numbered in an order  (the natural one) such that MoReFEM will not renumbered them. '
            'It is useful to test 1D Model and get in the hhdata files a solution that you can either load in Matlab '
'or plot in pgfplot with ease as no renumbering is due.',                               
            formatter_class=argparse.RawDescriptionHelpFormatter)
            

parser.add_argument('--length',
                    help = "Length of the bar in meters.",
                    required=True)
                    
parser.add_argument('--Ndiscretization_points',
                    help = "Number of discretization points to use.",
                    required=True)
                    
args = parser.parse_args()                    

if not args.Ndiscretization_points.isdigit():
    raise Exception("Number of discretization points is expected to be an integer!")
    
Npoints = int(args.Ndiscretization_points)

if Npoints <= 0:
    raise Exception("Number of discretization points is expected to be a positive integer!")
    

length = args.length

dx = float(length) / (float(Npoints) - 1)

mesh_name = "bar_1D_L{}_N{}".format(length, Npoints)

ensight_file = open(mesh_name + "_Ensight.geo", "w")

ensight_file.write("Geometry file\n")
ensight_file.write("Geometry file\n")
ensight_file.write("node id assign\n")
ensight_file.write("element id assign\n")
ensight_file.write("coordinates\n")
chars = list(args.Ndiscretization_points)
space = " " * (7 - len(chars) + 1)
ensight_file.write("{}{}\n".format(space, Npoints))

x = 0.

for i in range (0, Npoints):
    ensight_file.write(" %.5e %.5e %.5e\n"  % (x + i*dx, 0., 0.))
    
ensight_file.write("part 	1\n")
ensight_file.write("MeshLabel_1\n")
ensight_file.write("point\n")
ensight_file.write("       1\n")
ensight_file.write("       1\n")
ensight_file.write("part       2\n")
ensight_file.write("MeshLabel_2\n")
ensight_file.write("point\n")
ensight_file.write("       1\n")
chars = list(args.Ndiscretization_points)
space = " " * (7 - len(chars) + 1)
ensight_file.write("{}{}\n".format(space, Npoints))
ensight_file.write("part       3\n")
ensight_file.write("MeshLabel_3\n")
ensight_file.write("bar2\n")
chars = list(str(Npoints - 1))
space = " " * (7 - len(chars) + 1)
ensight_file.write(space + str((Npoints-1)) + "\n")

for i in range (1, Npoints):
    chars1 = list(str(i))
    chars2 = list(str(i+1))
    space1 = " " * (7 - len(chars1) + 1)
    space2 = " " * (7 - len(chars2) + 1)
    ensight_file.write(space1 + str(i) + space2 + str(i+1) + "\n") 