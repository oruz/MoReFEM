function vector_out = VectorRenumbering(vector_in, indices_out)

nDof = length(indices_out);

vector_out = zeros(nDof, 1);

for i=1:nDof
    vector_out(indices_out(i) + 1) = vector_in(i);
end

end