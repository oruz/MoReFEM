"""
Prepare the CMake command to configure the project, using to sdo so the settings used to compile the MoReFEM library.
"""

import argparse
import os
import subprocess


def cmake_command(args):
    """Generates the cmake command, and either run it or print it depending on no_run_command flag.
        
    \param[in] args: The parsed arguments to the command line.
    """
    cmd = "cmake -C {0}/cmake/PreCacheFile.cmake -DMoReFEM_DIR={0}/cmake {1} {2}".format(args.morefem_install_dir,
    args.cmake_args, args.root_directory)

    if args.no_run_command:
        print("The generated CMake command is:\n\n\t{}".format(cmd))
    else:
        subprocess.Popen(cmd, shell=True).communicate()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Wrapper over the CMake command.')
        
        
    parser.add_argument(
        '--morefem_install_dir',
        help=
        'The directory where MoReFEM was installed (through for instance "ninja install" command).'
    )
    
    parser.add_argument(
        '--root_directory',
        default="..",
        help=
        'The path to the root directory of the project (in which the base CMakeLists.txt should be)'
    )
        
    parser.add_argument(
        '--cmake_args',
        help=
        "Arguments you would like to give to cmake command, e.g. '-G Ninja'. Please use quotes or double quotes if there are several components. You should put there at least the relative path to the root folder."
    )
        
    parser.add_argument(
        '--no_run_command',
        action='store_true',
        help=
        "If this flag is set, the cmake command is just printed on screen, not run."
    ) 
        
        
    args = parser.parse_args()
    
    cmake_command(args)
    
    
    