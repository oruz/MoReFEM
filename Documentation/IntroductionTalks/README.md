Here are both talks given on the January, the 30th 2017 within the M3DISIM internal seminar.

Their goal is to give hints of the whereabouts of MoReFEM

Sebastien's talk is more of an overview, and deals with generic explanations (e.g. about mpi parallelism, C++ principles and so forth). You also get there useful command lines to get started (e.g. how to clone the sources or install the third party libraries).
Gautier's talk is much more practical: it explains how to set up a ModelInstance, using as example the case of hyperelasticity with midpoint scheme.
It also provides enlightening slides about the vector numbering, which are much more detailed than those in Sebastien's talk.


Each of these talks are present in 4 formats:

- The original Keynote one; which is the reference. Use it if you're using a Mac!
- An html format. The way to go if you want to see original presentation without a Mac at hand (especially if there are animations involved).
- A pdf with a huge number of slides to mimic the animations.
- A Powerpoint conversion. Not that bad, but some renderings are just awful.

__Update February 2018__:

New talk from Gautier added; this quick overview was prepared for the M3DISIM Séminaire d'Evaluation.

__Update July 2019__:

The talks themselves have been moved to the project