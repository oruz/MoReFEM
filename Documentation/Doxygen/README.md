There are three developer's guide present here:

## Basic developer's guide

This guide is for someone who intends to create a simple enough model, in which for instance all finite elements and all operators do already exist in the library. All the models given in ModelInstances qualify for this basic level!

## Advanced developer's guide

The following step is for developers already at ease with MoReFEM; in Advanced level you might for instance declare yourself a brand new operator.

Within the code, all the new content is embraced in the _Advanced_ namespace.

## Complete developer's guide

This guide is intended to developers of MoReFEM library itself and documents anything related to MoReFEM, including under-the-hood mechanisms. It should be reserved only to developer who tinkers with the core of the library and to no use to a developer writing a Model.

