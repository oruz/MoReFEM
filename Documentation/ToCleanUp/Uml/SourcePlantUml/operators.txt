@startuml

class DofManager {
    + DofManager(mpi, unknown_manager, global_operator_tuple)
    + Init(input_parameter_data, mesh)
    + DetermineDofProgramWiseIndexList()
    - Partition(cumulative_dof_index, mesh)
    - ReadBoundaryConditionList(input_parameter_data)
    - ComputeProcessorWiseAndGhostDofIndex()
    - SetFiniteElementList(input_parameter_data, mesh)
    - unknown_manager_
    - node_list_
    - ghost_node_list_
    - Nprogram_wise_dof_
    - processor_wise_dof_list_
    - ghosted_dof_list_
    - boundary_condition_list_
    - global_operator_tuple_
    
}

note bottom of DofManager : UnknownManager, Node and Dof are described on another UML.




class GlobalVariationalOperator {
   # GlobalVariationalOperator(input_parameter_data, unknown_list, matrix_or_vector_list, do_compute_quadrature_points)
   + ComputeLocal2Global()
   + AddFiniteElementForLocalVariationalOperator(geometric_subset, local_operator)
   .. Extract elementary values from a global vector ..
   #  ExtractFiniteElementValuesFrom(finite_element,global_vector)
   .. Store all finite elements per their associated local operator ..
   - finite_element_list_per_local_operator_
   - matrix_or_vector_properties_list_
   - unknown_list_
   - node_for_boundary_condition_
} 

note top of GlobalVariationalOperator : All actual global variational operators will inherit from\nThey may use additional arguments, such as the result from previous time iteration.


class LocalVariationalOperator {
    # LocalVariationalOperator(finite_element_type, elementary_data)
    .. Link the LocalVariationalOperator to a given FiniteElement ..
    + SetFiniteElement(finite_element)
    - finite_element_type_
    - elementary_data_

}

note top of LocalVariationalOperator : All actual local variational operators will inherit from


namespace Private {
class ElementaryData {
    + ElementaryData(finite_element_type, do_compute_quadrature_points, Ndof)
    .. Elementary matrix and/or vector in which a finite element computation are/is stored ..
    - matrix_ [if relevant]
    - vector_ [if relevant]
    .. For internal access only ..
    - finite_element_type_ 
    .. Various intermediate matrices and vectors ..
    .. Various cached geometric element data ..
    

}
}





namespace Private {
class GlobalMatrixOrVectorProperties {
    .. Depending on the operator, matrix, vector or both are present ..
    - matrix_ [if relevant]
    - matrix_coefficient_ [if relevant]
    - vector_ [if relevant]
    - vector_coefficient_ [if relevant]
    
    .. Specifies when it should be assembled ..
    - assembling_moment_

}
}


class FiniteElementType {
    + FiniteElementType(geometric_reference_element, unknown_list, quadrature_rule, shape_function_label)
    - geometric_reference_element_
    - reference_element_
    - unknown_list_
    - quadrature_rule_
    - shape_function_label_
    - Ndof_
    .. Various immutable local matrices and vectors at quadrature points ..
    
}


class FiniteElement {
    + FiniteElement(geometric_element)
    + ComputeLocal2Global()
    + SetNodeList(node_list)    
    - geometric_element_
    - node_list_
    - local_2_global_program_wise_
    - local_2_global_processor_wise_
        


}


note top of FiniteElement: FiniteElement can't access directly its most generic properties; \nFiniteElementType within LocalVariationalOperator gives away those data.\nIn most cases these generic data aren't needed.


DofManager "1" *-- "few" GlobalVariationalOperator : contains


class finite_element_list_per_local_operator_  << (A,orchid) >>
class matrix_or_vector_properties_list_ << (A,orchid) >>

'Extract the attribute finite_element_list_per_local_operator_'
class GlobalVariationalOperator o.. finite_element_list_per_local_operator_ 

'Link it to LocalVariationalOperator.'
finite_element_list_per_local_operator_ "1" *-- "few" class LocalVariationalOperator : contains

'Link it to FiniteElements.'
finite_element_list_per_local_operator_ "1" *-- "many" class FiniteElement : contains

'Extract the attribute matrix_or_vector_properties_list_'
class GlobalVariationalOperator o.. matrix_or_vector_properties_list_

'Link it to a GlobalMatrixOrVectorProperties'
matrix_or_vector_properties_list_ "1" *-- "one per associated matrix or vector" Private.GlobalMatrixOrVectorProperties 

'Extract the attributes from LocalVariationalOperator.'
class elementary_data_ << (A,orchid) >>
class finite_element_type_ << (A,orchid) >>

class LocalVariationalOperator o.. elementary_data_ 
class LocalVariationalOperator o.. finite_element_type_

elementary_data_ --  class Private.ElementaryData : is
finite_element_type_ -- class FiniteElementType : is


 
@enduml
