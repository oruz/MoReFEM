@startuml

abstract class RefFiniteElt {
    + RefFiniteElt()
    + Init<TopologyT>() 
    + NlocalNode() const 
    + GetLocalNode(topologic_index) const 
    + GetLocalNodeList() const 
    -- Access nodes on vertices --
    + GetLocalNodeOnVertex( topologic_vertex_index) const 
    + GetLocalNodeOnVertexList() const 
    -- Access nodes on edges --
    + AreNodesOnEdges() const 
    + GetLocalNodeOnEdgeList(topologic_edge_index, orientation) const 
    + GetLocalNodeOnEdgeList(topologic_edge_index) const 
    -- Access nodes on faces --
    + AreNodesOnFaces() const 
    + GetLocalNodeOnFaceList( topologic_face_index, orientation) const 
    + GetLocalNodeOnFaceList( topologic_face_index) const 
    -- Access nodes on volume --
    + AreNodesOnVolume() const 
    + GetLocalNodeOnVolumeList() const 
    -- Pure virtual methods --
    + {abstract} ShapeFunction(local_node_index, local_coords) const
    + {abstract} FirstDerivateShapeFunction(local_node_index, component, local_coords) const
    - {abstract} ComputeLocalNodeList()
    -- Miscellany --
    + Print(stream) const 
    + GetTopologyDimension() const 
    - NnodeOnVertex() const 
    - NnodeOnEdge() const 
    - NnodeOnFace() const    
    -- Private methods used in initialization --     
    - SortLocalNodeOnVertexList<TopologyT>() 
    - SetEdgeLocalNodeList(local_node_on_edge_list) 
    - SetFaceLocalNodeList(local_node_on_face_list)    
    -- Data attributes --
    - local_node_list_ 
    - local_node_on_vertex_list_ 
    - local_node_on_edge_storage_ 
    - local_node_on_face_storage_ 
    - local_node_on_volume_list_ 
    - topology_dimension_
}


class Spectral<TopologyT, NI, NJ, NK>{
    + Spectral()
    + {static} ShapeFunctionLabel()
    + ShapeFunction(local_node_index, local_coords) const
    + FirstDerivateShapeFunction(local_node_index, component, local_coords) const
    - ComputeLocalNodeList()
    - work_container_
}

namespace Private {


    class RefFiniteEltFactory << (S,#FF7700) Singleton >> {
        + {static} CreateOrGetInstance()
        + {static} GetInstance()
        - RefFiniteEltFactory()
        + {static} Name()
        + FetchRefFiniteElt(topology_name, shape_function_label) const
        + Register<RefFiniteEltInstanceT>()
        - ref_finite_elt_storage_
    }

} 



RefFiniteElt <|-- Spectral

Note bottom of Spectral: An example of RefFiniteElt instantiation.

Note bottom of Private.RefFiniteEltFactory { 
    The object in charge of registering and giving access to 
    all the known RefFiniteElt objects. A given RefFiniteElt
    (i.e. a couple topology/shape function label) is instantiated
    at most once.
}

Private.RefFiniteEltFactory "1" *-- "1" Spectral : registered in



@enduml