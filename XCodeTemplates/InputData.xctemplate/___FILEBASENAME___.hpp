//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

# include "Core/InputData/InputData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {
        
        
        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            // TODO: These are examples: adapt to your case!
            // See that there are no constraint upon numbering choice save that a same index can't be present twice.
            highest_dimension = 1,
            neumann = 2,
            dirichlet = 3,
            full_mesh = 10
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            // TODO: These are examples: adapt to your case!
            // See that there are no constraint upon numbering choice save that a same index can't be present twice.
            highest_dimension = 10,
            neumann = 20,
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            // TODO: These are examples: adapt to your case!
            // See that there are no constraint upon numbering choice save that a same index can't be present twice.
            velocity = 1,
            pressure = 2
        };
        
        
        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            // TODO: These are examples: adapt to your case!
            // See that there are no constraint upon numbering choice save that a same index can't be present twice.
            first = 1
        };



        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };
        
        
        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            monolithic = 1
        };
        


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,
            
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,
            
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,
        
            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>,
        
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,
            
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result
            // TODO: Add the problem-specific input parameters here!
        >;
        

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;

        //! \copydoc doxygen_hide_input_data_type
        using input_data_type = typename morefem_data_type::input_data_type;
    
    
    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM


#endif 
