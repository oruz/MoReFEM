//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace ___VARIABLE_problemName:identifier___NS
    {

        
        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }
        
        
    } // namespace ___VARIABLE_problemName:identifier___NS
    

} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
