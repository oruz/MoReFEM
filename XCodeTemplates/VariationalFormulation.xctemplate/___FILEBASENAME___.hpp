//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include <memory>
# include <vector>

# include "Utilities/InputData/LuaFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"

# include "ModelInstances/___VARIABLE_relativePath___/InputData.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation
        <
            VariationalFormulation,
            EnumUnderlyingType(SolverIndex::solver)
        >
        {
        private:
            
            //! \copydoc doxygen_hide_alias_self
            // \TODO This might seem a bit dumb but is actually very convenient for template classes.
            using self = VariationalFormulation;
            
            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation
            <
                self,
                EnumUnderlyingType(SolverIndex::solver)
            >;
            
            
            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;
            
        public:
            
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;
            
        public:
    
            /// \name Special members.
            ///@{
    
            //! \copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                            TimeManager& time_manager);
    
            //! Destructor.
            ~VariationalFormulation() override;
    
            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulation(const VariationalFormulation& rhs) = delete;
    
            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulation(VariationalFormulation&& rhs) = delete;
    
            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;
            
            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;
    
            ///@}
            
           
        private:
    
            /// \name CRTP-required methods.
            ///@{
    
            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const input_data_type& input_data);
    
            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();
    
            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

    
    
            ///@}
            
        private:
            
            
            /// \name Global variational operators.
            ///@{
            
            // \TODO Define there as data attributes the global operators involved in the formulation.
            // They should be defined as std::unique_ptr; accessors that returns a reference to them should also be
            // foreseen.
        
            ///@}
            
        private:
            
            /// \name Global vectors and matrices specific to the problem.
            ///@{
            
            // \TODO Define here the GlobalMatrix and GlobalVector objects into which the global operators are 
            // assembled.
            
            ///@}
            
        private:
            
        
            
    
        };
    
    
    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM


# include "ModelInstances/___VARIABLE_relativePath___/___FILEBASENAME___.hxx" // IWYU pragma: export


#endif 
