//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

#include <cstddef> // IWYU pragma: keep

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp" // \TODO Remove it for linear operators.
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp" // \TODO Remove it for bilinear operators.

# include "Operators/LocalVariationalOperator/___VARIABLE_operatorNature___LocalVariationalOperator.hpp"


namespace MoReFEM
{
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        class ___FILEBASENAMEASIDENTIFIER___ final
        : public ___VARIABLE_operatorNature___LocalVariationalOperator
        <
            LocalMatrix,
            LocalVector
        >,
        // << \TODO Remove matrix if linear operator and vector if bilinear one. You may change and specify a more
        // specific local type (symmetric matrix for instance        
        public Crtp::LocalMatrixStorage<___FILEBASENAMEASIDENTIFIER___, >// \TODO Specify here how many local matrices are required for the computation; remove it for linear operators.
        public Crtp::LocalVectorStorage<___FILEBASENAMEASIDENTIFIER___, >// \TODO Specify here how many local vectors are required for the computation; remove it for bilinear operators.
        {
            
        public:
            
            //! \copydoc doxygen_hide_alias_self
            // \TODO This might seem a bit dumb but is actually very convenient for template classes.
            using self = ___FILEBASENAMEASIDENTIFIER___;
            
            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;
            
            //! Returns the name of the operator.
            static const std::string& ClassName();
            
            //! Type of the elementary matrix.
            using matrix_type = LocalMatrix; // \TODO Same as template parameter
            
            //! Type of the elementary vector.
            using vector_type = LocalVector;
            
            //! Alias to variational operator parent.
            using parent = NonlinearLocalVariationalOperator
            <
                LocalMatrix,
                LocalVector
            >;
            
            //! Alias to the parent that provides LocalMatrixStorage.
            using matrix_parent = Crtp::LocalMatrixStorage<self, /*\TODO  Be consistent... */>;
            
            //! Alias to the parent that provides LocalVectorStorage.
            using vector_parent = Crtp::LocalVectorStorage<self, /*\TODO  Be consistent... */>;
            
            //! Rejuvenate alias from parent class.
            using elementary_data_type = typename parent::elementary_data_type;

            
            
        public:
            
            /// \name Special members.
            ///@{
            
            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * two unknowns (the first one vectorial and the second one scalar).
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             *
             * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
             * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
             * \endinternal
             */
            explicit ___FILEBASENAMEASIDENTIFIER___(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                        elementary_data_type&& elementary_data);
                                                    // \TODO Add if any same supplementary arguments as those given to GlobalVariationalOperator counterpart.
            
            
            //! Destructor.
            ~___FILEBASENAMEASIDENTIFIER___();
            
            //! \copydoc doxygen_hide_copy_constructor
            ___FILEBASENAMEASIDENTIFIER___(const ___FILEBASENAMEASIDENTIFIER___& rhs) = delete;
            
            //! \copydoc doxygen_hide_move_constructor
            ___FILEBASENAMEASIDENTIFIER___(___FILEBASENAMEASIDENTIFIER___&& rhs) = delete;
            
            //! \copydoc doxygen_hide_copy_affectation
            ___FILEBASENAMEASIDENTIFIER___& operator=(const ___FILEBASENAMEASIDENTIFIER___& rhs) = delete;
            
            //! \copydoc doxygen_hide_move_affectation
            ___FILEBASENAMEASIDENTIFIER___& operator=(___FILEBASENAMEASIDENTIFIER___&& rhs) = delete;
            
            ///@}
            
            
            // TODO By design this method does not take any argument; if you need extra data (e.g. solution from
            // former time iteration) you must foresee a data attribute which will be set by the global variational
            // operator's SetComputeEltArrayArguments().
            //! Compute the elementary vector.
            void ComputeEltArray();
            
        private:
            
            /// \name Useful indexes to fetch the work matrices and vectors.
            ///@{

            // TODO Remove if linear operator or if no local matrix of interest.
            // Number of values here must match the integer value given in constructor to Crtp::LocalMatrixStorage.
            //! Indexes of local matrices.
            enum class LocalMatrixIndex : std::size_t
            {
               your_first_local_matrix = 0,
               your_second_local_matrix, 
               ...
            };


            // TODO Remove if bilinear operator or if no local vector of interest.
            // Number of values here must match the integer value given in constructor to Crtp::LocalVectorStorage.
            //! Indexes of local vectors.
            enum class LocalVectorIndex : std::size_t
            {
               your_first_local_vector = 0,
               your_second_local_vector, 
               ...
            };
            ///@}
            

        };

        
        

        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


# include "Operators/LocalVariationalOperatorInstances/___VARIABLE_operatorNature___Form/___FILEBASENAME___.hxx" // IWYU pragma: export


#endif 
