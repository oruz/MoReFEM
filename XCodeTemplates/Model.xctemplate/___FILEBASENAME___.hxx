//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace ___VARIABLE_problemName:identifier___NS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("___VARIABLE_problemName:identifier___");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            // TODO: Put here any additional conditions that may trigger the end of the dynamic run.
            // If there is one, return true.
            // If there is none (i.e. the sole ending condition is to reach last time step) simply return false.
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        {
            // Base class InitializeStep() update the time for next iteration and print it on screen. If you
            // need additional operations, this is the place for them.
        }

     
    } // namespace ___VARIABLE_problemName:identifier___NS
    

} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
