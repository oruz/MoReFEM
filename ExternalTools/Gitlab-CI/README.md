The principle is that there is a gitlab-ci.yml file which is basically a list of other Yaml files included. The rationale behind that choice is that it enables to comment out easily parts of the CI, which is really useful in the dev cycle involving new CI builds. The drawback though is that CI-lint is more difficult to use than in the case of a unique Yaml file.

_common.yml_ encompasses the stuff that is used in all Yaml files (template to provide compilation and check warnings jobs for macos and linux, the jobs to handle Doxygen documentation, etc...) and thus should be included most of the time.

A verrou.yml file is currently not activated due to issues with the Docker image: on some architectures the Docker images refuse to run valfgrind, calling out an illegal instruction instead.

_generate_yaml.py_ was a script to generate former version of full gitlab-ci.yml file; it has been changed and is now handy to generate the Linux or macos versions of build and check.