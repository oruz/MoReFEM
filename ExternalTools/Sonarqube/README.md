SonarQube is mostly handled through CI, do the best to get a grasp of how it works is to have a peek at CI configuration.

However, sometimes the parsing of some files fail; in this case you should:

- Check whether there are intricated templates, and in this case try adding a space. The C++ plugin is not able for instance to deal with:

````
struct IsUniquePtr<const std::unique_ptr<T>>
````

but will gladly accept

````
struct IsUniquePtr<const std::unique_ptr<T> >
````

- Similarly, it may have a hard type parsing some (but not all) template with default parameters:

````
template<bool T = false>
std::string Check3();
````

A [ticket](https://github.com/SonarOpenCommunity/sonar-cxx/issues/2025) has been issued on the sonar-cxx project for this one.

- To better understand where the problem stems from, there is a tool named sslr-cxx-toolkit. You can download the latest version on [GitHub](https://github.com/SonarOpenCommunity/sonar-cxx/) and it is invoked through:

````
java -jar sslr-cxx-toolkit-x.x.x.xxxx.jar
````

It is rather crude but may nonetheless be rather helpful (beware though that if it is stuck it will stay that way - for some files I had to comment parts of them until it opens properly and the figure out reversely where the issue was).
