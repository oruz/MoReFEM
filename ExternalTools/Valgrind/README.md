Valgrind should be run only in a Linux environment, unless its developer manage to provide a working version in Mac (currently even a very basic function such as ls returns plenty of errors in Mac).

There are many leaks stemming from third party libraries; most of them are suppressed with the help of the files in this directory.

The way to call Valgrind is:

````
valgrind --gen-suppressions=all --show-leak-kinds=all --show-reachable=yes --track-origins=yes --leak-check=full --suppressions=~/Codes/MoReFEM/CoreLibrary/ExternalTools/Valgrind/openmpi.suppr --suppressions=~/Codes/MoReFEM/CoreLibrary/ExternalTools/Valgrind/libgomp.suppr --suppressions=~/Codes/MoReFEM/CoreLibrary/ExternalTools/Valgrind/libc.suppr  --suppressions=~/Codes/MoReFEM/CoreLibrary/ExternalTools/Valgrind/shared_runner.suppr *executable* *arguments*
````



The only remaining one in the last run was untraceable:

````
==1034== 48 bytes in 1 blocks are definitely lost in loss record 214 of 385
==1034==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==1034==    by 0xE419EC0: ???
==1034==    by 0xE41AC83: ???
==1034==    by 0xE419AAE: ???
==1034==    by 0xE4196DB: ???
==1034==    by 0xE41B93D: ???
==1034==    by 0xE41C38E: ???
==1034==    by 0xE41C3EA: ???
==1034==    by 0x1C2078FD: ???
==1034==    by 0xE43C879: ???
==1034==    by 0xE40C6CC: ???
==1034==    by 0xE3DD8E9: ???
==1034== 
````

# shared_runner

Some Valgrind warnings appears when we started using the shared runner in Gitlab CI; same image with same code don't yield them if run locally or from a CI virtual machine.