# How to set up include-what-you-use

Rather obviously, first install https://include-what-you-use.org/ on your computer...

Then following what is suggested on their (messy) [documentation](https://github.com/include-what-you-use/include-what-you-use/blob/master/README.md), we will create a specific build directory for it, named `build_iwyu` here.

````
mkdir build_iwyu
cd build_iwyu
````

Then we will call CMake with our usual script, providing several additional options:

````
python ../cmake/Scripts/configure_cmake.py --cache_file=../cmake/PreCache/macos_apple_clang.cmake --cmake_args="-DCMAKE_C_COMPILER=/usr/local/Cellar/llvm/11.0.0/bin/clang -DCMAKE_CXX_COMPILER=/usr/local/Cellar/llvm/11.0.0/bin/clang++ -G Ninja  -DCMAKE_CXX_INCLUDE_WHAT_YOU_USE='/usr/local/Cellar/include-what-you-use/0.15/libexec/bin/include-what-you-use;-Xiwyu;any;-Xiwyu;iwyu;-Xiwyu;args;-Xiwyu;--mapping_file=../ExternalTools/IncludeWhatYouUse/morefem.imp;-Xiwyu;--no_comments;-Xiwyu;--cxx17ns;-Xiwyu;max_line_length=120' -DMOREFEM_IGNORE_TESTS=True"
````

Few notes:
- Of course replace the paths to match your configuration:
  . The cache file
  . include-what-you-use path
  . clang and clang++ paths
- `-G Ninja` is a matter of taste: both `ninja` and `make` work with include-what-you-use.
- Make sure to use the vanilla clang and clang++, and not the Openmpi wrappers we usually used in the build of MoReFEM. The reason is that if you use Openmpi ones include-what-you-use will be stuck as the path to Openmpi includes is removed when those wrappers are used.
- MOREFEM_IGNORE_TESTS is a macro inside CMake build that is not publicized but disables most of the integration tests (only the test tools library and the tests related to Models remain).

# How to use it

Just invoke your build command (`ninja` or `make`). This will both compile the file and run include-what-you-use on it.

It behaves as a true build command: on repeated runs only the files modified will be recompiled / handled.
